﻿Imports System.Web
Imports System.Net

Namespace VBClasses


    Public Class Helper

        Public Function B2BAutoSendEmail(rentalid As String, emailaddress As String) As String

            If rentalid.Contains("/") = True Then
                rentalid = rentalid.Replace("/", "-")
            End If

            Dim webService As New PhoenixWebService()

            Dim HTTPProxy As String = System.Web.Configuration.WebConfigurationManager.AppSettings("Proxy")
            Dim byPassList As String = System.Web.Configuration.WebConfigurationManager.AppSettings("ByPassProxy")

            If HTTPProxy IsNot Nothing AndAlso HTTPProxy <> String.Empty Then
                Dim localProxy As New WebProxy(HTTPProxy, True, New String() {byPassList})
                webService.Proxy = localProxy
            End If

            Dim wsResponse As String = ""
            Try
                'if emailaddress is empty, then use agent email address
                wsResponse = webService.B2BCreateAndSendconfirmation(rentalid, emailaddress, (If(String.IsNullOrEmpty(emailaddress), False, True)))
                Dim auroraError As String = String.Empty
            Catch ex As Exception
                Return "ERROR:" + ex.Message
            End Try


            Return wsResponse

        End Function

        Public Function B2BAgentEmailAddress(b2bagent As String) As String


            Dim webService As New PhoenixWebService()

            Dim HTTPProxy As String = System.Web.Configuration.WebConfigurationManager.AppSettings("Proxy")
            Dim byPassList As String = System.Web.Configuration.WebConfigurationManager.AppSettings("ByPassProxy")

            If HTTPProxy IsNot Nothing AndAlso HTTPProxy <> String.Empty Then
                Dim localProxy As New WebProxy(HTTPProxy, True, New String() {byPassList})
                webService.Proxy = localProxy
            End If

            Dim wsResponse As String = ""
            Try
                wsResponse = webService.B2BAgentEmailAddress(b2bagent)
            Catch ex As Exception
                Return "ERROR:" + ex.Message
            End Try

            Return wsResponse
        End Function

        Public Function B2BAutoSendEmail(emailaddress As String, subject As String, content As String, b2bagent As String) As String

            If String.IsNullOrEmpty(content) Then
                Return "Error: Confirmation Html is empty"
            End If
            If String.IsNullOrEmpty(emailaddress) Then
                Return "Error: Email is empty"
            End If


            Dim webService As New PhoenixWebService()

            Dim HTTPProxy As String = System.Web.Configuration.WebConfigurationManager.AppSettings("Proxy")
            Dim byPassList As String = System.Web.Configuration.WebConfigurationManager.AppSettings("ByPassProxy")

            If HTTPProxy IsNot Nothing AndAlso HTTPProxy <> String.Empty Then
                Dim localProxy As New WebProxy(HTTPProxy, True, New String() {byPassList})
                webService.Proxy = localProxy
            End If

            Dim wsResponse As String = ""
            Try
                wsResponse = webService.B2BSendConfirmation(emailaddress, subject, content, b2bagent)
            Catch ex As Exception

                Return "ERROR:" + ex.Message
            End Try


            Return wsResponse

        End Function

    End Class


End Namespace