<%@ Application Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>
<script runat="server">       
    
    void Application_Start(object sender, EventArgs e) 
    {
        Application["ApplicationData"] = new THL.Booking.ApplicationData();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        //get reference to the source of the exception chain
        Exception ex = Server.GetLastError().GetBaseException();

        //log the details of the exception and page state to the
        /* TODO DEFine Event Logging */
        
        /*
        EventLog.WriteEntry("PhoenixWeb",
          "MESSAGE: " + ex.Message +
          "\nSOURCE: " + ex.Source +
          "\nFORM: " + Request.Form.ToString() +
          "\nQUERYSTRING: " + Request.QueryString.ToString() +
          "\nTARGETSITE: " + ex.TargetSite +
          "\nSTACKTRACE: " + ex.StackTrace,
          EventLogEntryType.Error);
        */
        
        //Insert optional email notification here..
        try
        {
            //-----------------------------------------------------------------------------------------------------
            //rev:mia MAY 31 2013  - PART OF APCOVER ENHANCEMENT. 
            //                        CREATE A LOGHANDLER FOR ALL EXCEPTIONS
            //-----------------------------------------------------------------------------------------------------
            string ErrorDetails = "message: "+ ex.Message + ", Source: " + ex.Source + ", StackTrace: " + ex.StackTrace + ", InnerException" + ex.InnerException;
            ErrorDetails = ErrorDetails + ", URLreferrer" + Request.UrlReferrer.ToString();
            THLDebug.LogError(ErrorTypes.Application, "Application_Error", ErrorDetails, "");
        }
        catch (Exception exTemp)
        {
            THLDebug.LogError(ErrorTypes.Application, "Application_Error:Inside Catch Block", exTemp.Message, "");
        }   
        
            
        Response.Redirect("Error.aspx");

    }

    void Session_Start(object sender, EventArgs e) 
    {
        Session["BookingStatus"] = THL.Booking.BookingStatus.Created;
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
