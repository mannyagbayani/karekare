<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Register.ascx.vb" Inherits="THLAuroraWebInterface.Register" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE class="normal" id="Table1" cellSpacing="0" cellPadding="2" width="550" border="0">
	<TR>
		<TD width="550" colSpan="3"><asp:label id="lblError" runat="server" CssClass="warning"></asp:label></TD>
	</TR>
	<TR bgColor="silver">
		<TD width="200">First Name</TD>
		<TD width="200" align="center"><asp:textbox id="txtFirstName" tabIndex="4" Width="200px" runat="server" MaxLength="64"></asp:textbox></TD>
		<td width="150" align="right"><asp:button id="btnRegister" tabIndex="9" runat="server" Text="Register" Width="80px"></asp:button></td>
	</TR>
	<TR bgColor="silver">
		<TD width="200">Surname</TD>
		<TD width="200" align="center"><asp:textbox id="txtSurname" tabIndex="5" Width="200px" runat="server" MaxLength="64"></asp:textbox></TD>
		<TD width="150"></TD>
	</TR>
	<TR bgColor="silver">
		<TD width="200">Email</TD>
		<TD width="200" align="center"><asp:textbox id="txtEmail" tabIndex="6" Width="200px" runat="server" MaxLength="64"></asp:textbox></TD>
		<TD width="150"></TD>
	</TR>
	<TR bgColor="silver">
		<TD width="200">Password</TD>
		<TD width="200" align="center"><asp:textbox id="txtPassword" tabIndex="7" Width="200px" runat="server" TextMode="Password" MaxLength="64"></asp:textbox></TD>
		<TD width="150"></TD>
	</TR>
	<TR bgColor="silver">
		<TD width="200">Confirm Password</TD>
		<TD width="200" align="center"><asp:textbox id="txtConfirmPassword" tabIndex="8" Width="200px" runat="server" TextMode="Password"
				MaxLength="64"></asp:textbox></TD>
		<TD width="150"></TD>
	</TR>
</TABLE>
