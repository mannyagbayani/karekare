'--------------------------------------------------------------------------------------
'   Class:          Register
'   Description:    Captures user registration info. Once user is registered an event will be 
'                   fired.
'--------------------------------------------------------------------------------------

Partial Class Register
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public Event NewUserCreated(ByVal email As String, ByVal password As String)
    Dim RetUsersRS As New THL_RetUsersRS

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblError.Text = ""
    End Sub

    Private Sub btnRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        ' The process of registering a new user involves
        ' 1. Logging in as anonymous user in order to get the correct pos details
        ' 2. Retrieving the anonymous users details inorder to get the default agent and agent list
        ' 3. Inserting the user with the correct agent and user entered details

        If ValidateForm() = True Then
            Dim RQ As New THL_LoginOkRQ
            Dim RS As New THL_LoginOkRS
            Dim customerPOS(0) As SourceType
            Dim mRequestorId As New UniqueID_Type

            ' Create an anonymous user POS
            customerPOS(0) = New SourceType
            customerPOS(0).AgentSine = "anonymous"
            customerPOS(0).ERSP_UserID = "anonymous"
            mRequestorId.ID = "WEBMAUI"
            mRequestorId.Type = "22"
            customerPOS(0).RequestorID = mRequestorId
            Session("POS") = customerPOS(0)

            RQ.EchoToken = "Token"
            RQ.email = "anonymous@thlrentals.com"
            RQ.Item = "anonymous"
            RQ.IsAgent = True
            RQ.TimeStamp = New Date
            RQ.Version = CDec(1.1)
            Try
                RS = Proxy.LoginOk(RQ)

            Catch ex As Exception
                lblError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
                Exit Sub
            End Try

            If Not IsNothing(RS.Errors) AndAlso RS.Errors.Length > 0 Then
                ' An error occured, display it
                Dim errorString As String
                Dim i As Integer
                For i = 0 To RS.Errors.Length - 1
                    errorString &= RS.Errors(i).Tag & "<br>"
                Next
                lblError.Text &= errorString
            Else
                Session("POS") = RS.POS(0)

                'Retrieve anonymous user details 
                RetrieveAnonymousUserDetails()

                'And insert new user
                InsertNewUser()

            End If

        End If


    End Sub

    Private Sub RetrieveAnonymousUserDetails()
        Dim RQ As New THL_RetUsersRQ


        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)} 'only pos required in request

        Try
            RetUsersRS = Proxy.RetUsers(RQ) 'all user data retrieved based on user passed in pos

        Catch ex As Exception
            lblError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try


        If IsNothing(RetUsersRS.Errors) Then

        Else
            'errors occurred
            Dim errorIndex As Integer
            For errorIndex = 0 To RetUsersRS.Errors.Length - 1
                lblError.Text &= RetUsersRS.Errors(errorIndex).Tag & "<br>"
            Next
        End If
    End Sub

    Private Sub InsertNewUser()

        Dim RQ As New THL_UpdateUserDetailsRQ
        Dim RS As New THL_UpdateUserDetailsRS

        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        RQ.UserInfo = New UserInfoType

        RQ.Mode = "I" ' New user            


        RQ.UserInfo.UserId = RQ.POS(0).ERSP_UserID
        RQ.UserInfo.FirstName = txtFirstName.Text
        RQ.UserInfo.LastName = txtSurname.Text
        RQ.UserInfo.PassWord = txtPassword.Text

        RQ.UserInfo.EmailAddress = txtEmail.Text
        RQ.UserInfo.DefAgentCode = RetUsersRS.Users(0).DefAgentCode ' Anonymous users def agent code

        RQ.UserInfo.Agents = RetUsersRS.Agents
        Try
            RS = Proxy.UpdateUserDetails(RQ)
        Catch ex As Exception
            lblError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
            Exit Sub
        End Try


        Dim sb As New System.Text.StringBuilder
        Dim i As Integer = 0

        If Not IsNothing(RS.Errors) AndAlso RS.Errors.Length > 0 Then
            ' There were errors
            For i = 0 To RS.Errors.Length - 1
                sb.Append(RS.Errors(0).Tag & "<br>")
            Next
            lblError.Text = sb.ToString
        Else ' All ok
            'user has been created sucessfully, now log them into the system
            RaiseEvent NewUserCreated(txtEmail.Text, txtPassword.Text)
        End If

    End Sub

    Private Function ValidateForm() As Boolean
        ValidateForm = True

        If txtFirstName.Text.Trim = "" Then
            lblError.Text &= "Please enter a First Name"
            ValidateForm = False
        End If

        If txtSurname.Text.Trim = "" Then
            lblError.Text &= "Please enter a Surname"
            ValidateForm = False
        End If

        If txtEmail.Text.Trim = "" Then
            lblError.Text &= "Please enter an Email address"
            ValidateForm = False
        End If

        If txtPassword.Text.Trim = "" Then
            lblError.Text &= "Please enter a password"
            ValidateForm = False

        ElseIf txtPassword.Text.Trim.Length < 5 Then
            lblError.Text &= "Password must be at least 5 characters"
            ValidateForm = False

        ElseIf txtPassword.Text.Trim <> txtConfirmPassword.Text.Trim Then
            lblError.Text &= "Passwords do not match"
            ValidateForm = False
        End If

        Return ValidateForm
    End Function
End Class
