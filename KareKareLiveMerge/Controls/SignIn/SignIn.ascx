<%@ Control Language="vb" AutoEventWireup="false" Codebehind="SignIn.ascx.vb" Inherits="THLAuroraWebInterface.SignIn" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE class="Normal" id="Table1" cellSpacing="0" cellPadding="2" width="550" border="0">
	<TR bgColor="silver">
		<TD width="270">Enter Your Email Address
		</TD>
		<TD width="200" align="center"><asp:textbox id="txtEmail" MaxLength="64" runat="server" Width="200px" tabIndex="1" Enabled="False"></asp:textbox></TD>
		<TD width="150" align="right"><asp:button id="btnLogin" tabIndex="3" runat="server" Width="80px" Text="Login" Enabled="False"></asp:button></TD>
	</TR>
	<TR bgColor="silver">
		<TD width="200">Password</TD>
		<TD width="200" align="center"><asp:textbox id="txtPassword" tabIndex="2" MaxLength="24" runat="server" Width="200px" TextMode="Password"
				Enabled="False"></asp:textbox></TD>
		<TD width="150"></TD>
	</TR>
	<TR>
		<TD colSpan="3"><asp:checkbox id="chkPassword" tabIndex="4" runat="server" Text="Forgot your password ? Enter your email address above and check this box then press login. Your password will be emailed to you."
				Enabled="False"></asp:checkbox></TD>
	</TR>
	<TR>
		<TD colSpan="3"><asp:label id="lblWarning" runat="server" Width="400" CssClass="warning"></asp:label></TD>
	</TR>
</TABLE>
