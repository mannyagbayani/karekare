Partial Class WebServiceDown
    Inherits System.Web.UI.UserControl
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA

#Region " Web Form Designer Generated Code "

#Region "Variables"

    Private m_WebServiceStatus As Boolean = False
    'Private m_ErrorMsg As String

#End Region

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'test the availability of the webservice

        Dim RS As THL_WebServiceStateRS
        Dim EventDateTime As String
        Dim Direction As String

        RS = Proxy.WebServiceState()

        'IF there were errors to the service then
        If Not IsNothing(RS.Errors) AndAlso RS.Errors.Length > 0 Then
            ' An error occured, display it!
            Dim sb As New System.Text.StringBuilder
            Dim i As Integer
            For i = 0 To RS.Errors.Length - 1
                sb.Append(RS.Errors(i).Tag)
            Next

            'to display error msg in the parent
            'm_ErrorMsg &= sb.ToString
            lblServiceState.ForeColor = System.Drawing.Color.Red
            lblServiceState.Text = sb.ToString
            Return
        End If

        'Get the datetime
        EventDateTime = RS.DateTime.ToString()
        'Get the direction 
        Direction = RS.State.ToString()

        'show the web status as available with the next scheduled downtime datetime
        'This is the visable default hardcoded
        lblServiceState.ForeColor = System.Drawing.Color.Black
        lblServiceState.Text = EventDateTime.ToString()

        If (Direction = "DOWN") Then

            'set the public variable
            m_WebServiceStatus = True
            'or
            'show the web status as unavailable with the forcasted uptime datetime
        ElseIf (Direction = "UP") Or (Direction = "NONE") Then

            'set the public variable
            m_WebServiceStatus = False

        End If

        'lblServiceState.Visible = True

    End Sub

#Region "Properties"
    Public ReadOnly Property WebServiceStatus() As Boolean
        Get
            Return m_WebServiceStatus
        End Get

    End Property

    'Public ReadOnly Property ErrorMsg() As String
    '    Get
    '        Return m_ErrorMsg
    '    End Get
    'End Property

#End Region

End Class
