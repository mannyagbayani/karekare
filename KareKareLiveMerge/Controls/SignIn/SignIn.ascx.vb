'--------------------------------------------------------------------------------------
'   Class:          SignIn
'   Description:    Logs the user in and if specified will return the user to the page they got
'                   kicked out off.
'--------------------------------------------------------------------------------------

Imports System.Xml.Serialization
Imports System.IO
'Imports THLRentals.OTA.Utils

Partial Class SignIn
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Private webservice As WebServiceDown

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Customer Confirmation Changes"
    ' Customer Confirmation Changes
    Public Property GrossModeEnabled() As Boolean
        Get
            If Session("GrossModeEnabled") IsNot Nothing Then
                Return CBool(Session("GrossModeEnabled"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("GrossModeEnabled") = value
        End Set
    End Property

    Public Property NettModeEnabled() As Boolean
        Get
            If Session("NettModeEnabled") IsNot Nothing Then
                Return CBool(Session("NettModeEnabled"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("NettModeEnabled") = value
        End Set
    End Property

    'Public Property GrossModeSelected() As Boolean
    '    Get
    '        If Session("GrossModeSelected") IsNot Nothing Then
    '            Return CBool(Session("GrossModeSelected"))
    '        Else
    '            Return False
    '        End If
    '    End Get
    '    Set(ByVal value As Boolean)
    '        Session("GrossModeSelected") = value
    '    End Set
    'End Property

    'Public Property NettModeSelected() As Boolean
    '    Get
    '        If Session("NettModeSelected") IsNot Nothing Then
    '            Return CBool(Session("NettModeSelected"))
    '        Else
    '            Return False
    '        End If
    '    End Get
    '    Set(ByVal value As Boolean)
    '        Session("NettModeSelected") = value
    '    End Set
    'End Property

    Public Property GrossNettDefault() As String
        Get
            If Session("GrossNettDefault") IsNot Nothing Then
                Return CStr(Session("GrossNettDefault"))
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As String)
            Session("GrossNettDefault") = value
        End Set
    End Property

    Public Property UserCanSeeCustomerConfirmations() As Boolean
        Get
            If Session("UserCanSeeCustomerConfirmations") IsNot Nothing Then
                Return CBool(Session("UserCanSeeCustomerConfirmations"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("UserCanSeeCustomerConfirmations") = value
        End Set
    End Property

    Public Property UserCanSeeGrossNett() As Boolean
        Get
            If Session("UserCanSeeGrossNett") IsNot Nothing Then
                Return CBool(Session("UserCanSeeGrossNett"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("UserCanSeeGrossNett") = value
        End Set
    End Property

    ' Customer Confirmation Changes
#End Region

#Region "Currency Options"

    Public Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("UserHasCurrencyOptions") = value
        End Set
    End Property

#End Region

    Public Property RedirectURL() As String
        Get
            Return CStr(ViewState("redirectUrl"))
        End Get
        Set(ByVal Value As String)
            ViewState("redirectUrl") = Value
        End Set
    End Property

    Public WriteOnly Property Email() As String
        Set(ByVal Value As String)
            txtEmail.Text = Value
        End Set
    End Property

    Public WriteOnly Property Password() As String
        Set(ByVal Value As String)
            txtPassword.Text = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here

        lblWarning.Text = ""

        'Enable the objects if the service is up
        'the default should be disabled

        webservice = CType(Me.Parent, WebServiceDown)

        If webservice.WebServiceStatus Then

            Me.btnLogin.Enabled = True
            Me.txtPassword.Enabled = True
            Me.txtEmail.Enabled = True
            Me.chkPassword.Enabled = True

        End If

        'webservicedown errors not displayed here
        'lblWarning.Text = webservice.ErrorMsg


        '#If DEBUG Then
        '       txtEmail.Text = "b2b@thlrentals.com"
        '      txtPassword.Text = "demo123"
        '#End If

    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Login()
    End Sub


    Public Sub Login()
        ' Allow sub to be public so password and email can be set and user logged in 
        ' without having to enter details

        Dim RQ As New THL_LoginOkRQ
        Dim RS As New THL_LoginOkRS
        ' Validate form and proceed to log user in (or email password if selected)
        If Validate() = True Then
            ' Proceed to login
            Try
                RQ.EchoToken = "Token"
                RQ.email = CStr(txtEmail.Text)

                If chkPassword.Checked = False Then
                    ' Only send password if Request Password box is not checked, otherwise an email will be generated and sent to the user

                    RQ.Item = txtPassword.Text.ToString

                    '#If DEBUG Then
                    '                   RQ.Item = "demo123"
                    '#End If

                Else
                    RQ.Item = CBool("true") ' True= send email to user
                End If

                RQ.IsAgent = True
                RQ.TimeStamp = New Date
                RQ.Version = CDec(1.1)


                General.SerializeLoginObject(RQ)
                RS = Proxy.LoginOk(RQ)

            Catch ex As Exception
                lblWarning.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
            End Try

            If Not IsNothing(RS.Errors) AndAlso RS.Errors.Length > 0 Then
                ' An error occured, display it!
                Dim sb As New System.Text.StringBuilder
                Dim i As Integer
                For i = 0 To RS.Errors.Length - 1
                    sb.Append(RS.Errors(i).Tag & "<br>")
                Next
                lblWarning.Text &= sb.ToString
            Else
                ' All ok
                If lblWarning.Text = "" Then
                    If chkPassword.Checked = False Then
                        ' Store users details 
                        Session("POS") = RS.POS(0)

                        ' Save Customer Conf Settings to session
                        Dim saUserOptions As String() = RS.POS(0).AgentDutyCode.Split("~"c)
                        ' 0 - Customer Confirmation Allowed? Yes/No
                        ' 1 - Gross+Nett Allowed? Yes/No
                        ' 2 - Gross/Nett/Both?  G,N
                        ' 3 - default option out of Gross/Nett?  G
                        ' Save Customer Conf Settings to session
                        UserCanSeeCustomerConfirmations = saUserOptions(0).ToUpper().Equals("YES")
                        UserCanSeeGrossNett = saUserOptions(1).ToUpper().Equals("YES")
                        GrossModeEnabled = saUserOptions(2).ToUpper().Contains("G")
                        NettModeEnabled = saUserOptions(2).ToUpper().Contains("N")
                        'GrossModeSelected = saUserOptions(3).ToUpper().Contains("G")
                        'NettModeSelected = saUserOptions(3).ToUpper().Contains("N")
                        GrossNettDefault = Left(saUserOptions(3).ToUpper().Trim(), 1)

                        ' currency changes
                        UserHasCurrencyOptions = saUserOptions(4).ToUpper().Trim().Equals("YES")
                        'CurrencyOptionsList = saUserOptions(5).ToUpper().Trim().Split(","c)
                        'DefaultCurrency = saUserOptions(6).ToUpper().Trim()
                        ' currency changes

                        ' And redirect user
                        ''rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
                        CreateCookieAndSession(RQ.email, RS.POS(0).RequestorID.ID)
                        If Me.RedirectURL = "" Then
                            ' Redirect to main page
                            Session("SessionId") = RS.EchoToken
                            Response.Redirect("QuotesAndBookings.aspx")
                        Else
                            Response.Redirect(Me.RedirectURL)
                        End If

                    Else
                        ' Don't redirect if user hasn't entered password
                        ' Display message that email has been sent 
                        lblWarning.Text &= "An email has been sent to the specified email address with your password"
                    End If
                End If
            End If

        End If

    End Sub
    Private Function Validate() As Boolean
        ' Check to see user input has meet all requirements
        Dim success As Boolean = True

        If txtEmail.Text.Trim = "" Then
            lblWarning.Text = "Please enter a Login" & "<br>"
            success = False
        End If

        If chkPassword.Checked = False Then
            If txtPassword.Text.Trim = "" Then
                success = False
                lblWarning.Text = "Please enter a Password" & "<br>"

            ElseIf txtPassword.Text.Trim.Length < 5 Then
                lblWarning.Text = "Password must be at least 5 characters" & "<br>"
                success = False
            End If
        End If

        Return success
    End Function

#Region "rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation"
    Private Sub CreateCookieAndSession(agentemailaddress As String, agentCode As String)
        Dim cookie As HttpCookie = New HttpCookie("agentinformation")
        cookie("emailaddress") = agentemailaddress
        cookie("agentcode") = agentCode
        cookie.Expires = DateTime.Now.AddDays(1)
        Session("agentinformation.emailaddress") = agentemailaddress
        Session("agentinformation.agentcode") = agentCode
        Session("agentinformation.message") = ""
        Response.Cookies.Add(cookie)
    End Sub
#End Region

    
End Class
