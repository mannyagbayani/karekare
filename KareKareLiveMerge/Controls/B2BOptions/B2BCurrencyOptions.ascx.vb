﻿Option Strict Off
Imports System.Text

Partial Public Class B2BCurrencyOptions
    '    Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

    Protected Proxy As New AuroraOTAProxy.Aurora_OTA

#Region "Properties"

    Public ReadOnly Property LoggedInUserIsSuperUser() As Boolean
        Get
            If Session("IsSuperUser") IsNot Nothing Then
                Return CBool(Session("IsSuperUser"))
            Else
                Return False
            End If
        End Get
    End Property

    Private Property UserData() As String
        Get
            Return CStr(Session("UserData"))
        End Get
        Set(ByVal value As String)
            Session("UserData") = value
        End Set
    End Property

    Public Property UserId() As String
        Get
            Return CStr(ViewState("UserId"))
        End Get
        Set(ByVal Value As String)
            ViewState("UserId") = Value
        End Set
    End Property

    Public Property RS() As THL_RetUsersRS
        Get
            Return CType(Session("THL_RetUsersRS"), THL_RetUsersRS)
        End Get
        Set(ByVal value As THL_RetUsersRS)
            Session("THL_RetUsersRS") = value
        End Set
    End Property

    Public Property MRU_ToCurrencyList() As ArrayList
        Get
            Return CType(Session("MRU_ToCurrencyList"), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("MRU_ToCurrencyList") = value
        End Set
    End Property

    Public Property MRU_RatesList() As ExchangeRatesTypeAgentCurr()
        Get
            Return CType(Session("MRU_RatesList"), ExchangeRatesTypeAgentCurr())
        End Get
        Set(ByVal value As ExchangeRatesTypeAgentCurr())
            Session("MRU_RatesList") = value
        End Set
    End Property

    Public Property MRU_UserSettingsTable() As DataTable
        Get
            Return CType(Session("MRU_UserSettingsTable"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            Session("MRU_UserSettingsTable") = value
        End Set
    End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            PopulateAgentDropDown()
            GetCurrencyDataForSelectedAgent(cmbAgent.SelectedValue)
        End If
    End Sub


    Sub PopulateAgentDropDown()
        Dim arrAgents As String()
        arrAgents = RS.Agents

        For Each sAgent As String In arrAgents
            Dim sCode, sName As String
            sCode = sAgent.Split("|"c)(0)

            ' see if there are exchange rates for this agent
            For Each oAgent As ExchangeRatesTypeAgent In RS.ExchangeRates
                If Trim(UCase(oAgent.Code)) = Trim(UCase(sCode)) Then
                    ' there are exchange rates for this agent
                    sName = sAgent.Split("|"c)(1)
                    cmbAgent.Items.Add(New ListItem(sName, sCode))
                End If
            Next

        Next
        SortDropDown(cmbAgent)
        cmbAgent.SelectedIndex = 0
    End Sub


    Private Sub SortDropDown(ByRef dd As DropDownList)
        Dim ar As ListItem()
        Dim i As Long = 0
        For Each li As ListItem In dd.Items
            ReDim Preserve ar(i)
            ar(i) = li
            i += 1
        Next
        Dim ar1 As Array = ar

        ar1.Sort(ar1, New ListItemComparer)
        dd.Items.Clear()
        dd.Items.AddRange(ar1)
    End Sub

    Private Class ListItemComparer 
        Implements IComparer

        Public Function Compare(ByVal x As Object, _
              ByVal y As Object) As Integer _
              Implements System.Collections.IComparer.Compare
            Dim a As ListItem = x
            Dim b As ListItem = y
            Dim c As New CaseInsensitiveComparer
            Return c.Compare(a.Text, b.Text)
        End Function
    End Class



    Sub GetCurrencyDataForSelectedAgent(ByVal SelectedAgentCode As String)
        For Each oAgent As ExchangeRatesTypeAgent In RS.ExchangeRates
            If oAgent.Code = SelectedAgentCode Then
                ' this is the selected agent
                Dim alToCurrencyList As New ArrayList
                Dim alToCurrencyList1 As New ArrayList

                For Each oRate As ExchangeRatesTypeAgentCurr In oAgent.Curr

                    If Not alToCurrencyList1.Contains(oRate.ToCode) Then
                        alToCurrencyList.Add(New String() {CStr(oRate.ToCode), CStr(oRate.ToId), CStr(oRate.ToDesc)})
                        alToCurrencyList1.Add(oRate.ToCode)
                    End If
                Next

                DrawCurrencyRateTable(alToCurrencyList, oAgent.Curr, False)
                ' mru populated
                CreateUserCurrencySettingsDataTable()
                ' bind grid
                grdCurrencySettings.DataSource = MRU_UserSettingsTable
                grdCurrencySettings.DataBind()
                ' generate scripts
                'GenerateScripts()
            Else
                Continue For
            End If
        Next
    End Sub

    Sub DrawCurrencyRateTable(ByVal ToCurrencyList As ArrayList, ByVal RatesList As ExchangeRatesTypeAgentCurr(), ByVal NewRatesMode As Boolean)

        ' back up MRU in session - start
        MRU_RatesList = RatesList
        MRU_ToCurrencyList = ToCurrencyList
        ' back up MRU in session - end

        Dim sbCurrencyRateTable As New StringBuilder
        Dim sbJavascriptIds As New StringBuilder
        Dim sbCurrencyIdsForJS As New StringBuilder

        Dim bDummyDataAdded As Boolean = False

        Dim sAUDCodeId As String = "61C1FBAA-06DA-497F-95AA-6958342AF2B8"
        Dim sNZDCodeId As String = "4F96F47F-EAAF-41ED-9F09-7686A6B6D4F4"

        Dim sAUDCurrDesc As String = "Aust Dollar"
        Dim sNZDCurrDesc As String = "New Zealand Dollar"

        Dim alFromCurrencies As New ArrayList
        Dim alToCurrencies As ArrayList = ToCurrencyList

        ' Get list of FROM Currencies (typically AUD, NZD)
        alFromCurrencies.Add(New String() {"AUD", sAUDCodeId, sAUDCurrDesc})
        alFromCurrencies.Add(New String() {"NZD", sNZDCodeId, sNZDCurrDesc})

        Dim bMissingNewRatesMode As Boolean = False

        For Each oExchRate As ExchangeRatesTypeAgentCurr In RatesList
            If Not NewRatesMode Then
                If oExchRate.Rate = "" OrElse CDbl(oExchRate.Rate) = 0.0 Then
                    ' this is a cue to create row
                    bMissingNewRatesMode = True
                End If
            End If

        Next

        With sbCurrencyRateTable
            For Each arrCurrencyFrom As String() In alFromCurrencies
                .Append("    <tr>")
                .Append("        <td class=""PriceAndAvailabilityRow"" colspan=""2"">")
                .Append("        </td>")
                .Append("        <td class=""PriceAndAvailabilityRow"">")
                .Append("            Country of Travel " & arrCurrencyFrom(0))
                .Append("        </td>")
                .Append("    </tr>")
                .Append("    <tr>")
                .Append("        <td  colspan=""2"">")
                .Append("            <b>Currency</b>")
                .Append("        </td>")
                .Append("        <td >")
                .Append("            <b>Rate for $1 " & arrCurrencyFrom(0) & "</b>")
                .Append("        </td>")
                .Append("    </tr>")

                Dim iCounter As Integer = 0

                For Each arrCurrencyTo As String() In alToCurrencies
                    .Append("    <tr class=""" & IIf(iCounter Mod 2 = 0, "evenRow", "oddRow").ToString() & """>")
                    .Append("        <td valign=""bottom"" align=""center"" >")
                    .Append("            <img src=""../Images/" & arrCurrencyTo(0) & ".png"" alt=""" & arrCurrencyTo(0) & """ style=""width: 40px; height: 30px"" />")
                    .Append("        </td>")
                    .Append("        <td >")
                    .Append(arrCurrencyTo(0) & " - " & arrCurrencyTo(2))
                    .Append("        </td>")
                    .Append("        <td >")
                    .Append("            <input type=""text"" maxlength=""10"" style=""width:100px;text-align:right""  " _
                                        & "  id=""txt" & arrCurrencyFrom(0) & "to" & arrCurrencyTo(0) & """ ")

                    '' <<TODO>> need a data table here for storing the rates
                    For Each oRow As ExchangeRatesTypeAgentCurr In RatesList
                        If oRow.FromCode = arrCurrencyFrom(0) _
                           AndAlso _
                           oRow.ToCode = arrCurrencyTo(0) _
                        Then
                            ' read the rate
                            If NewRatesMode OrElse (bMissingNewRatesMode AndAlso CDbl(oRow.Rate) = 0.0) Then
                                .Append("onkeypress=""return keyStrokeDecimal(event);"" onblur=""CheckContents(this,'');"" value="""" />")
                            Else
                                .Append(" READONLY tabindex=""-1"" ")
                                .Append("value=""" & CDbl(oRow.Rate).ToString("f6").ToString() & """ />")
                            End If




                        Exit For
                        End If
                    Next


                    .Append("        </td>")
                    .Append("    </tr>")
                    iCounter += 1

                    sbJavascriptIds.Append("txt" & arrCurrencyFrom(0) & "to" & arrCurrencyTo(0) & ",")
                    sbCurrencyIdsForJS.Append(arrCurrencyFrom(1) & "|" & arrCurrencyTo(1) & "|" & arrCurrencyFrom(0) & "|" & arrCurrencyTo(0) & ",")
                Next

            Next
        End With

        If sbCurrencyRateTable.ToString().Length > 0 Then
            sbCurrencyRateTable.Insert(0, "<table class=""ExchangeRate"" cellpadding=""0"" cellspacing=""0"">")
            sbCurrencyRateTable.Append("</table>")
        End If

        If sbJavascriptIds.Length > 0 Then
            sbJavascriptIds.Remove(sbJavascriptIds.Length - 1, 1)
            'If NewRatesMode Then
            btnSave.Attributes("onclick") = "return UpdatehdnCurrencyData('" & hdnCurrencyData.ClientID & "','" & sbJavascriptIds.ToString() & "','" & sbCurrencyIdsForJS.ToString() & "');"
            btnSave2.Attributes("onclick") = "return UpdatehdnCurrencyData('" & hdnCurrencyData.ClientID & "','" & sbJavascriptIds.ToString() & "','" & sbCurrencyIdsForJS.ToString() & "');"
            'End If
            hdnCurrencyData.Value = "" 'reset this
        End If

        If sbCurrencyIdsForJS.Length > 0 Then
            sbCurrencyIdsForJS.Remove(sbCurrencyIdsForJS.Length - 1, 1)
        End If

        ltrExchangeRate.Text = sbCurrencyRateTable.ToString()
    End Sub

    Protected Sub cmbAgent_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbAgent.SelectedIndexChanged
        ' load the rates required 
        ' load the user stuff
        GetCurrencyDataForSelectedAgent(cmbAgent.SelectedValue)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click, btnSave2.Click


        ' Dim RQ As New THL_UpdateUserDetailsRQ

        Dim RQ As New THL_updateB2BExchangeRateRQ

        RQ.B2BExchangeRates = New B2BExchangeRatesType
        RQ.B2BExchangeRates.Agent = New B2BExchangeRatesTypeAgent
        RQ.B2BExchangeRates.Agent.Code = cmbAgent.SelectedValue

        ' reset the error display
        lblError.Text = ""
        lblError.Style("display") = "none"
        Dim bExitSub As Boolean = False

        ' ------------XCHG------------------
        Dim bExchangeRateChagesMade As Boolean = False
        bExchangeRateChagesMade = (hdnCurrencyData.Value.Trim() <> String.Empty)

        Dim saCurrency As String()
        If Trim(hdnCurrencyData.Value) <> "" Then
            saCurrency = hdnCurrencyData.Value.Remove(hdnCurrencyData.Value.Length - 1, 1).Split(","c)
        Else
            lblError.Text = "Unable to read currency rates"
            lblError.Style("display") = "block"
            Exit Sub
        End If


        'Dim arrCurrency(saCurrency.Length) As B2BExchangeRatesTypeAgentCurr
        Dim alCurrency As New ArrayList

        For Each sCurrencyDetail As String In saCurrency

            Dim saDetails As String()
            saDetails = sCurrencyDetail.Split("|"c)

            Dim oCurrencyOption As New B2BExchangeRatesTypeAgentCurr

            oCurrencyOption.FromId = saDetails(0)
            oCurrencyOption.FromCode = saDetails(2)

            oCurrencyOption.ToId = saDetails(1)
            oCurrencyOption.ToCode = saDetails(3)

            oCurrencyOption.Rate = CDbl(saDetails(4))

            'arrCurrency(i) = oCurrencyOption
            alCurrency.Add(oCurrencyOption)

        Next

        Dim oTemp As New B2BExchangeRatesTypeAgentCurr ' temp object , only used for obtaining the type
        RQ.B2BExchangeRates.Agent.Curr = alCurrency.ToArray(oTemp.GetType)
        ' the exchange rate data is now stored


        'DrawCurrencyRateTable(MRU_ToCurrencyList,<<NEWRATESLIST>>,false)

        ' ------------XCHG------------------



        Dim alUserCurrencyData As New ArrayList


        Dim bNoChangesInCurrentRow As Boolean = True
        Dim bNoChangesOverall As Boolean = True
        ' -----------USERDATA---------------
        For Each RowItem As GridViewRow In grdCurrencySettings.Rows

            'RQ.UserInfo.AgentDutyCode 

            Dim chkCurrencyOptionEnabled As CheckBox = CType(RowItem.FindControl("chkCurrencyOptionEnabled"), CheckBox)
            Dim hdnCurrencyOptionEnabled As HiddenField = CType(RowItem.FindControl("hdnCurrencyOptionEnabled"), HiddenField)



            If CBool(hdnCurrencyOptionEnabled.Value) <> chkCurrencyOptionEnabled.Checked Then
                ' no change
                bNoChangesInCurrentRow = False
            End If

            Dim objDefaultAndSelect As DefaultAndSelectionUserControl
            objDefaultAndSelect = CType(RowItem.FindControl("objDefaultAndSelect"), DefaultAndSelectionUserControl)

            Dim sErrorMessage As String = ""

            If objDefaultAndSelect.HasChanged OrElse Not bNoChangesInCurrentRow OrElse (bExchangeRateChagesMade AndAlso RowItem.RowIndex = 0) Then
                bNoChangesInCurrentRow = False
                bNoChangesOverall = False
                If objDefaultAndSelect.IsValid(sErrorMessage) Then
                    ' valid
                    Dim sNewCurrencyList As String = ""
                    Dim sDefaultCurrency As String = ""
                    sNewCurrencyList = objDefaultAndSelect.GetCSVSelectedList
                    sDefaultCurrency = objDefaultAndSelect.GetDefault

                    Dim oUserB2BCurrData As New B2BUserInfoType

                    oUserB2BCurrData.UserId = CType(RowItem.FindControl("hdnUserId"), HiddenField).Value

                    For Each oUser As UserInfoType In RS.Users
                        If oUser.UserId = oUserB2BCurrData.UserId Then
                            oUserB2BCurrData.AgentDutyCode = oUser.AgentDutyCode
                            Exit For
                        End If
                    Next

                    Dim saB2BOptions As String() = oUserB2BCurrData.AgentDutyCode.Split("~"c)
                    saB2BOptions(4) = IIf(chkCurrencyOptionEnabled.Checked, "Yes", "No")
                    oUserB2BCurrData.AgentDutyCode = Join(saB2BOptions, "~")

                    'oUserB2BCurrData.CurrOptions = New CurrOptionsTypeAgent

                    Dim oCurrOptions As New CurrOptionsTypeAgent
                    oCurrOptions.code = cmbAgent.SelectedValue
                    oCurrOptions.currList = sNewCurrencyList
                    oCurrOptions.default = sDefaultCurrency

                    ' validation
                    Dim sUserEmail As String = RowItem.Cells(1).Text
                    If chkCurrencyOptionEnabled.Checked Then
                        If sNewCurrencyList.Trim().Equals(String.Empty) Then
                            lblError.Text += "Please tick currencies available for this user - " & sUserEmail & "<br/>"
                            lblError.Style("display") = "block"
                            'Exit Sub
                            bExitSub = True
                        End If
                        If sDefaultCurrency.Trim().Equals(String.Empty) Then
                            lblError.Text += "Please determine the default currency for this user - " & sUserEmail & "<br/>"
                            lblError.Style("display") = "block"
                            'Exit Sub
                            bExitSub = True
                        End If
                    End If
                    ' validation

                    oUserB2BCurrData.CurrOptions = New CurrOptionsTypeAgent(0) {oCurrOptions}

                    alUserCurrencyData.Add(oUserB2BCurrData)
                Else
                    ' invalid
                    lblError.Text = sErrorMessage
                    lblError.Style("display") = "block"
                    Exit Sub
                End If

            End If
            bNoChangesInCurrentRow = True
        Next

        If bExitSub Then
            Exit Sub
        End If

        ' update user data structure 
        Dim oTemp2 As New B2BUserInfoType ' temp object , only used for obtaining the type
        RQ.Users = alUserCurrencyData.ToArray(oTemp2.GetType())

        ' -----------USERDATA---------------

        Dim oRS As New THL_RetUsersRS

        If Not bNoChangesOverall OrElse bExchangeRateChagesMade Then
            ' oRS = Proxy.UpdateUserDetails(RQ)
            RQ.POS = New SourceType() {Session("POS")}

            oRS = Proxy.updateB2BExchangeRate(RQ)

            If oRS.Errors Is Nothing OrElse oRS.Errors.Length = 0 Then
                ' reload here
                RS = oRS
                GetCurrencyDataForSelectedAgent(cmbAgent.SelectedValue)
            Else
                lblError.Text = oRS.Errors(0).ShortText
                lblError.Style("display") = "block"
            End If
        End If


    End Sub

    Sub GetAgentData()
        Dim RS As THL_RetUsersRS = CType(General.DeserializeRetUsersRSObject(Me.UserData, RS), THL_RetUsersRS)
    End Sub

    Protected Sub btnCreateNewRates_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCreateNewRates.Click
        DrawCurrencyRateTable(MRU_ToCurrencyList, MRU_RatesList, True)
        btnSave.Enabled = True
    End Sub

    Sub CreateUserCurrencySettingsDataTable()
        ' we need list of all possible to currencies
        Dim dtUserCurrencySettings As New DataTable("UserCurrencySettings")
        dtUserCurrencySettings.Columns.Add("UserId")
        dtUserCurrencySettings.Columns.Add("UserEmail")
        dtUserCurrencySettings.Columns.Add("CurrencyOptionEnabled")
        dtUserCurrencySettings.Columns.Add("AgentCode")
        dtUserCurrencySettings.Columns.Add("CurrenciesSelected")
        dtUserCurrencySettings.Columns.Add("DefaultCurrency")

        For Each oUser As UserInfoType In RS.Users

            ' process only if this user is linked to the agent in the dropdown

            Dim bProcessThisUser As Boolean = False

            If oUser.Agents IsNot Nothing Then
                For Each sAgnCode As String In oUser.Agents
                    If UCase(Trim(sAgnCode.Split("|"c)(0))) = UCase(Trim(cmbAgent.SelectedValue)) Then
                        bProcessThisUser = True
                        Exit For
                    End If
                Next
            End If

            If bProcessThisUser Then

                Dim bCurrencyOptionEnabled As Boolean = False
                Dim sAgentCode As String = ""
                Dim sSelectedCurrencies As String = ""
                Dim sDefaultCurrency As String = ""

                Try
                    bCurrencyOptionEnabled = oUser.AgentDutyCode.Split("~"c)(4).ToUpper().Trim().Equals("YES")
                Catch ex As Exception
                End Try

                Try
                    sAgentCode = cmbAgent.SelectedValue.ToUpper().Trim()
                Catch ex As Exception
                End Try

                If oUser.CurrOptions IsNot Nothing AndAlso oUser.CurrOptions.Length > 0 Then
                    ' there are some options
                    For Each oOption As CurrOptionsTypeAgent In oUser.CurrOptions
                        If CStr(oOption.code).ToUpper().Trim() = sAgentCode Then
                            ' this is the agent
                            sSelectedCurrencies = CStr(oOption.currList)
                            sDefaultCurrency = CStr(oOption.default)
                            Exit For
                        End If
                    Next
                End If

                dtUserCurrencySettings.Rows.Add(oUser.UserId, oUser.EmailAddress, bCurrencyOptionEnabled, sAgentCode, sSelectedCurrencies, sDefaultCurrency)
            End If
        Next

        ' save to session
        MRU_UserSettingsTable = dtUserCurrencySettings
    End Sub

    Protected Sub grdCurrencySettings_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCurrencySettings.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            SetGridDropDowns(e.Row)
        End If

        ' hide the currency enabled column
        e.Row.Cells(0).Visible = False
        e.Row.Cells(2).Visible = False
        e.Row.Cells(3).Visible = False
        e.Row.Cells(4).Visible = False
    End Sub

    Sub SetGridDropDowns(ByVal RowItem As GridViewRow)
        ' then set up the drop downs
        Dim hdnCurrencySelection, hdnDefaultCurrency, hdnCurrencyOptionEnabled As System.Web.UI.WebControls.TableCell
        Dim hiddenhdnCurrencyOptionEnabled, hdnUserId As HiddenField
        Dim ltrScript As Literal

        Dim chkCurrencyOptionEnabled As CheckBox

        hdnCurrencyOptionEnabled = RowItem.Cells(2)
        hdnCurrencySelection = RowItem.Cells(3)
        hdnDefaultCurrency = RowItem.Cells(4)

        chkCurrencyOptionEnabled = CType(RowItem.FindControl("chkCurrencyOptionEnabled"), CheckBox)
        hiddenhdnCurrencyOptionEnabled = CType(RowItem.FindControl("hdnCurrencyOptionEnabled"), HiddenField)
        ltrScript = CType(RowItem.FindControl("ltrScript"), Literal)

        hiddenhdnCurrencyOptionEnabled.Value = hdnCurrencyOptionEnabled.Text

        hdnUserId = CType(RowItem.FindControl("hdnUserId"), HiddenField)
        hdnUserId.Value = RowItem.Cells(0).Text

        ' reset everything
        Dim dtSelectionData As New DataTable
        dtSelectionData.Columns.Add("ItemCode")
        dtSelectionData.Columns.Add("ItemName")
        dtSelectionData.Columns.Add("ItemDesc")
        dtSelectionData.Columns.Add("Default")
        dtSelectionData.Columns.Add("Selected")


        For Each saToCurrency As String() In MRU_ToCurrencyList
            Dim sItemCode, sItemName As String
            Dim bDefault, bSelected As Boolean

            sItemCode = saToCurrency(0).ToUpper().Trim()
            sItemName = saToCurrency(2).ToUpper().Trim()

            bSelected = hdnCurrencySelection.Text.ToUpper().ToString().Contains(saToCurrency(0).ToUpper().Trim())
            bDefault = sItemCode.Equals(hdnDefaultCurrency.Text.ToUpper().Trim())

            dtSelectionData.Rows.Add(sItemCode, sItemName, sItemCode & "-" & sItemName, bDefault, bSelected)
        Next

        Dim objDefaultAndSelect As DefaultAndSelectionUserControl
        objDefaultAndSelect = CType(RowItem.FindControl("objDefaultAndSelect"), DefaultAndSelectionUserControl)

        objDefaultAndSelect.DataSource = dtSelectionData
        objDefaultAndSelect.DataBind()

        If hdnCurrencyOptionEnabled.Text.Trim().Equals(String.Empty) Then
            chkCurrencyOptionEnabled.Checked = False
        Else
            chkCurrencyOptionEnabled.Checked = CBool(hdnCurrencyOptionEnabled.Text)
        End If

        If RowItem.RowIndex = 0 Then
            chkCurrencyOptionEnabled.Enabled = False ' disallow self disabling
        End If


    End Sub


    Protected Sub cmbAvailableCurrencies_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim rwRow As GridViewRow
        rwRow = CType(CType(sender, DropDownList).Parent.Parent, GridViewRow)

        Dim cmbAvailableCurrencies, cmbDefaultCurrency As DropDownList
        Dim cblSelectedCurrencies As CheckBoxList

        cmbAvailableCurrencies = CType(sender, DropDownList)
        cmbDefaultCurrency = CType(rwRow.FindControl("cmbDefaultCurrency"), DropDownList)

        cblSelectedCurrencies = CType(rwRow.FindControl("cblSelectedCurrencies"), CheckBoxList)

        cmbDefaultCurrency.Items.Add(New ListItem(cmbAvailableCurrencies.SelectedItem.Text, cmbAvailableCurrencies.SelectedItem.Value))
        cblSelectedCurrencies.Items.Add(New ListItem(cmbAvailableCurrencies.SelectedItem.Text, cmbAvailableCurrencies.SelectedItem.Value))

        cmbAvailableCurrencies.Items.RemoveAt(cmbAvailableCurrencies.SelectedIndex)
    End Sub

End Class