﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefaultAndSelectionUserControl.ascx.vb"
    Inherits="THLAuroraWebInterface.DefaultAndSelectionUserControl" %>
<asp:GridView AutoGenerateColumns="false" runat="server" ID="grdMain" Width="100%" CssClass="CleanGrid">
    <Columns>
        <asp:BoundField DataField="Default" />
        <asp:BoundField DataField="Selected" />
        <asp:TemplateField HeaderText="Default" ItemStyle-Width="50px">
            <ItemTemplate>
                <asp:RadioButton ID="radDefault" GroupName="DefaultGroup" runat="server" onclick="SelectMeOnly(this)"/>
                <asp:HiddenField ID="hdnItemCode" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Selected" ItemStyle-Width="50px">
            <ItemTemplate>
                <asp:CheckBox ID="chkSelected" runat="server" onclick="ClearMyRadio(this)" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="ItemName" />
        <asp:BoundField DataField="ItemCode" />
        <asp:BoundField DataField="ItemDesc"  />
    </Columns>
</asp:GridView>
<asp:HiddenField ID="hdnCollectionName" runat="server" />
<asp:HiddenField ID="hdnShowHeader" runat="server" />

