﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="B2BCurrencyOptions.ascx.vb"
    Inherits="THLAuroraWebInterface.B2BCurrencyOptions" %>
<%@ Register TagPrefix="uc" TagName="DefaultAndSelect" Src="~/Controls/B2BOptions/DefaultAndSelectionUserControl.ascx" %>

<script type="text/javascript">
    function keyStrokeDecimal(evt)
    { 
        if (!window.event) return true; // give up, its too hard to test for control keys in FF (IE doesnt pass them)

        evt = window.event;

        return !((evt.keyCode < 48 || evt.keyCode > 58) && (evt.keyCode != 46))
    }

    function trimDecimal(intData,noOfDec){ 
	    var MinusFlag = false
	    intData = "" + intData + ""
	    if(intData.substring(0,1)=="-")
		    MinusFlag = true
	    inumber=intData
	    number=intData
	    intData=""+number+""
	    number=""+number+""
	    numberArray=number.split(".")
	    if(intData=="-."){ return "-0.00"
	    }
	    if(intData=="-"){ return "-0.00"
	    }
	    if(intData=="."){ return "0.00"
	    }
	    if(numberArray.length==1){ if(inumber=="")
		    inumber=0
		    return ""+inumber+""+".00"
	    }
	    sInt=numberArray[0]
	    sFraction=""+numberArray[1].substring(0,noOfDec)+""
	    if(sFraction==""){ return ""+sInt+""+".00"
	    }
	    if(sInt.length ==1 ){ if(sInt=="-"){ if(sFraction.length==1)
	    sFraction= sFraction + '0'
	    if(sFraction=="")
	    return "-0.00"
	    return "-0." + sFraction
	    }
	    }
	    if(sInt==""){ if(sFraction.length==1)
	    sFraction= sFraction + '0'
	    return "0." + sFraction
	    }
	    if(sInt==0){ if(sFraction.length==1)
	    sFraction= sFraction + 0
	    return sInt + "." + sFraction
	    }
	    if(sFraction.length==1){ return sResult=sInt +"."+ sFraction+"0"
	    }
	    sResult=sInt +"."+ sFraction
	    return ""+sResult+""
    }

    function CheckContents(field,defaultValue)
    {
        if(isNaN(field.value) || parseFloat(field.value) <= 0.0)
        {
            if(defaultValue == null)
            {
                field.value="";
            }
            else
            {
                field.value=defaultValue;
            }                
        }
        else
        {
            if(isNaN(trimDecimal(parseFloat(field.value),6)) || trimDecimal(parseFloat(field.value),6) <= 0.0 || parseFloat(trimDecimal(field.value,6)) <= 0.0)
            {
                field.value = "";
            }
            else
            {
                field.value = trimDecimal(parseFloat(field.value),6);
            }
        }
    }

   
    function UpdatehdnCurrencyData(hdnCurrencyDataID,JavascriptIdList,CurrencyIdDetails)
    {
        var hdnCurrencyData = document.getElementById(hdnCurrencyDataID);
        var arrTextBoxes = JavascriptIdList.split(",");
        var arrCurrDetails = CurrencyIdDetails.split(",");
        var lblError = document.getElementById("<%=lblError.ClientID %>");
        
        for(var i=0;i<arrTextBoxes.length;i++)
        {
            var arrSubDetails = arrCurrDetails[i].split("|");
            if(!(document.getElementById(arrTextBoxes[i]) == null))
            {
                if( document.getElementById(arrTextBoxes[i]).value == "" || isNaN(parseFloat(document.getElementById(arrTextBoxes[i]).value)) )
                {
                    //setWarningShortMessage("Please enter a valid exchange rate for " + arrSubDetails[2] + " to " + arrSubDetails[3]);
                    lblError.innerHTML = "Please enter a valid exchange rate for " + arrSubDetails[2] + " to " + arrSubDetails[3];
                    // innerText wont work in lame old firefox :( it wont update the display at all
                    lblError.style.display = "block";
                    return false;
                }
                else
                {
                    hdnCurrencyData.value = hdnCurrencyData.value + arrCurrDetails[i] + "|" + document.getElementById(arrTextBoxes[i]).value + "," ;
                    lblError.style.display = "none";
                }
            }
        }
        return true;
    }
    
    function EnableDisableCurrencyOptions(chkCurrencyOptionEnabled)
    {
        EnableDisableAllChildren(chkCurrencyOptionEnabled.parentNode.parentNode.children[2],chkCurrencyOptionEnabled.checked);
    }
    
    function EnableDisableAllChildren(Element,EnableDisable)
    {
        try
        {
            Element.disabled = !EnableDisable;
            if(Element.children.length>0)
            {
                for(var i = 0;i<Element.children.length;i++)
                {
                    EnableDisableAllChildren(Element.children[i],EnableDisable)
                }
            }
        }
        catch(err)
        {}
    }
    
    
</script>

<table cellpadding="0" cellspacing="0" width="750px">
    <tr>
        <td class="CellHeading2" colspan="2" style="height: 20px">
            Select agent to maintain currency rates and user defaults
        </td>
    </tr>
    <tr>
        <td class="CellHeading2">
            <asp:DropDownList ID="cmbAgent" runat="server" AutoPostBack="true" />
        </td>
        <td class="CellHeading2">
            <asp:Button ID="btnCreateNewRates" runat="server" Text="Create New Rates" />
        </td>
    </tr>
    <tr>
        <td>
            <br />
            <b>Currency Rates</b>
            <asp:Literal ID="ltrExchangeRate" runat="server" />
            <asp:HiddenField ID="hdnCurrencyData" runat="server" />
            <br />
            <asp:Label ID="lblError" runat="server" CssClass="warning" Style="display: none"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="CellHeading2" style="height: 20px">
            Maintain currency options for users
        </td>
        <td class="CellHeading2" align="right">
            <asp:Button ID="btnSave2" runat="server" Text="Save" Width="100px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <br />
            <asp:GridView ID="grdCurrencySettings" runat="server" AutoGenerateColumns="False"
                Style="border: solid 1px black">
                <Columns>
                    <asp:BoundField DataField="UserId" HeaderText="UserId" HeaderStyle-CssClass="PriceAndAvailabilityRow" />
                    <asp:BoundField DataField="UserEmail" HeaderText="User Email" HeaderStyle-CssClass="PriceAndAvailabilityRow"
                        ItemStyle-BorderColor="Black" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                        HeaderStyle-BorderColor="Black" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" />
                    <asp:BoundField DataField="CurrencyOptionEnabled" HeaderText="CurrencyOptionEnabled"
                        HeaderStyle-CssClass="PriceAndAvailabilityRow" ItemStyle-BorderColor="Black"
                        ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px" HeaderStyle-BorderColor="Black"
                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" />
                    <asp:BoundField DataField="CurrenciesSelected" HeaderText="CurrenciesSelected" HeaderStyle-CssClass="PriceAndAvailabilityRow"
                        ItemStyle-BorderColor="Black" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                        HeaderStyle-BorderColor="Black" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" />
                    <asp:BoundField DataField="DefaultCurrency" HeaderText="DefaultCurrency" HeaderStyle-CssClass="PriceAndAvailabilityRow"
                        ItemStyle-BorderColor="Black" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                        HeaderStyle-BorderColor="Black" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" />
                    <asp:TemplateField HeaderText="Currency Option Enabled?" HeaderStyle-CssClass="PriceAndAvailabilityRow"
                        ItemStyle-BorderColor="Black" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                        HeaderStyle-BorderColor="Black" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnUserId" runat="server" />
                            <asp:HiddenField ID="hdnCurrencyOptionEnabled" runat="server" />
                            <asp:CheckBox ID="chkCurrencyOptionEnabled" runat="server" onclick="EnableDisableCurrencyOptions(this)" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="250px" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="PriceAndAvailabilityRow"
                        ItemStyle-BorderColor="Black" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                        HeaderStyle-BorderColor="Black" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                        <HeaderTemplate>
                            <table width="100%">
                                <tr>
                                    <td colspan="3">
                                        Currency Selection
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50px">
                                        Default
                                    </td>
                                    <td align="left" width="50px">
                                        Selected
                                    </td>
                                    <td align="left">
                                        Currency
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <uc:DefaultAndSelect ID="objDefaultAndSelect" runat="server" CollectionName="Currency"
                                ShowHeader="false" />
                            <%--<asp:Literal ID="ltrScript" runat="server"></asp:Literal>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <script type="text/javascript">
                try
                {
                    for(var i=2;i< <% =(grdCurrencySettings.Rows.Count+2).ToString() %>;i++)
                    {
                        var sCheckBoxId = "objB2BCurrencyOptions_grdCurrencySettings__ctl" + i.toString() + "_chkCurrencyOptionEnabled";
                        var sTableId = "objB2BCurrencyOptions_grdCurrencySettings__ctl" + i.toString() + "_objDefaultAndSelect_grdMain";
                        
                        var chkEnabled = document.getElementById(sCheckBoxId);
                        var tblCurrency = document.getElementById(sTableId);
                        
                        if(chkEnabled)
                        {
                            if(tblCurrency)
                            {
                                EnableDisableAllChildren(tblCurrency,chkEnabled.checked);
                            }                            
                        }
                    }
                }
                catch(err)
                {}
            </script>

        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td align="right">
            <br />
            <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
</table>
