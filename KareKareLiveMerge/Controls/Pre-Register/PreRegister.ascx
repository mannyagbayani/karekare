<%@ Control Language="vb" AutoEventWireup="false" Codebehind="PreRegister.ascx.vb" Inherits="THLAuroraWebInterface.PreRegister" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE cellSpacing="0" cellPadding="2" width="746" border="0">
	<TR class="RowHeading">
		<TD vAlign="top">Pre-Registration Details</TD>
		<TD></TD>
		<TD>
			<P>&nbsp;</P>
		</TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading"></TD>
		<TD>Booking Confirmation Number</TD>
		<TD><asp:textbox id="txtBookingConfirmNo" runat="server" Width="265px" ReadOnly="True"></asp:textbox></TD>
		<TD align="right"><FONT color="red">*
				<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter a Booking Confirmation Number"
					ControlToValidate="txtBookingConfirmNo"><</asp:requiredfieldvalidator></FONT></TD>
	</TR>
	<TR class="RowHeading">
		<TD>Personal Details</TD>
		<TD></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Title</TD>
		<TD><asp:dropdownlist id="ddlTitle" CssClass="NormalDDL" runat="server">
				<asp:ListItem Value="Dr">Dr</asp:ListItem>
				<asp:ListItem Value="Mr" Selected="True">Mr</asp:ListItem>
				<asp:ListItem Value="Mrs">Mrs</asp:ListItem>
				<asp:ListItem Value="Miss">Miss</asp:ListItem>
				<asp:ListItem Value="Ms">Ms</asp:ListItem>
				<asp:ListItem></asp:ListItem>
			</asp:dropdownlist></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px; HEIGHT: 20px"></TD>
		<TD style="WIDTH: 229px; HEIGHT: 20px">First Name</TD>
		<TD style="HEIGHT: 20px"><asp:textbox id="txtFirstName" runat="server" Width="265px"></asp:textbox></TD>
		<TD style="HEIGHT: 20px" align="right"><FONT color="red"><FONT color="red">*
					<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter your First Name"
						ControlToValidate="txtFirstName"><</asp:requiredfieldvalidator></FONT></FONT></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Surname</TD>
		<TD><asp:textbox id="txtSurname" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red"><FONT color="red">* </FONT></FONT>
			<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ErrorMessage="Please enter your Surname"
				ControlToValidate="txtSurname"><</asp:requiredfieldvalidator></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Date of Birth</TD>
		<TD>Day&nbsp;
			<asp:textbox id="txtDOBDay" runat="server" Width="33px" MaxLength="2"></asp:textbox>Month&nbsp;
			<asp:dropdownlist id="ddlDOBMonth" CssClass="NormalDDL" runat="server">
				<asp:ListItem Value="01">Jan</asp:ListItem>
				<asp:ListItem Value="02">Feb</asp:ListItem>
				<asp:ListItem Value="03">Mar</asp:ListItem>
				<asp:ListItem Value="04">Apr</asp:ListItem>
				<asp:ListItem Value="05">May</asp:ListItem>
				<asp:ListItem Value="06">Jun</asp:ListItem>
				<asp:ListItem Value="07">Jul</asp:ListItem>
				<asp:ListItem Value="08">Aug</asp:ListItem>
				<asp:ListItem Value="09">Sep</asp:ListItem>
				<asp:ListItem Value="10">Oct</asp:ListItem>
				<asp:ListItem Value="11">Nov</asp:ListItem>
				<asp:ListItem Value="12">Dec</asp:ListItem>
			</asp:dropdownlist>Year&nbsp;
			<asp:textbox id="txtDOBYear" runat="server" Width="52px" MaxLength="4"></asp:textbox></TD>
		<TD align="right"><FONT color="#ff0000"><FONT color="red">*
					<asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" ErrorMessage="Please enter a DOB Day"
						ControlToValidate="txtDOBDay"><</asp:requiredfieldvalidator><asp:requiredfieldvalidator id="Requiredfieldvalidator13" runat="server" ErrorMessage="Please enter a DOB Year"
						ControlToValidate="txtDOBYear"><</asp:requiredfieldvalidator></FONT></FONT></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 264px">Drivers Licence Details</TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Licence Number</TD>
		<TD><asp:textbox id="txtLicenenseNo" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red"><FONT color="red">*
					<asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter you License Number"
						ControlToValidate="txtLicenenseNo"><</asp:requiredfieldvalidator></FONT></FONT></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Expiry Date</TD>
		<TD>Day&nbsp;
			<asp:textbox id="txtExpiryDay" runat="server" Width="33px" MaxLength="2"></asp:textbox>Month&nbsp;
			<asp:dropdownlist id="ddlLicenceMonth" CssClass="NormalDDL" runat="server">
				<asp:ListItem Value="01">Jan</asp:ListItem>
				<asp:ListItem Value="02">Feb</asp:ListItem>
				<asp:ListItem Value="03">Mar</asp:ListItem>
				<asp:ListItem Value="04">Apr</asp:ListItem>
				<asp:ListItem Value="05">May</asp:ListItem>
				<asp:ListItem Value="06">Jun</asp:ListItem>
				<asp:ListItem Value="07">Jul</asp:ListItem>
				<asp:ListItem Value="08">Aug</asp:ListItem>
				<asp:ListItem Value="09">Sep</asp:ListItem>
				<asp:ListItem Value="10">Oct</asp:ListItem>
				<asp:ListItem Value="11">Nov</asp:ListItem>
				<asp:ListItem Value="12">Dec</asp:ListItem>
			</asp:dropdownlist>Year&nbsp;
			<asp:textbox id="txtExpiryYear" runat="server" Width="52px" MaxLength="4"></asp:textbox>&nbsp;&nbsp;</TD>
		<TD align="right"><FONT color="#ff0000"><FONT color="red">*
					<asp:requiredfieldvalidator id="Requiredfieldvalidator14" runat="server" ErrorMessage="Please enter an Expiry Day"
						ControlToValidate="txtExpiryDay"><</asp:requiredfieldvalidator><asp:requiredfieldvalidator id="Requiredfieldvalidator15" runat="server" ErrorMessage="Please enter an expiry Year"
						ControlToValidate="txtExpiryYear"><</asp:requiredfieldvalidator></FONT></FONT></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Issuing Authority</TD>
		<TD><asp:textbox id="txtAuthority" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red"><FONT color="red">*
					<asp:requiredfieldvalidator id="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter an Issuing Authority"
						ControlToValidate="txtAuthority"><</asp:requiredfieldvalidator></FONT></FONT></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Country of Issue</TD>
		<TD><asp:textbox id="txtCountryOfIssue" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red">*
				<asp:requiredfieldvalidator id="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter the Country Of Issue"
					ControlToValidate="txtCountryOfIssue"><</asp:requiredfieldvalidator></FONT></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 264px">Address Details</TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Address</TD>
		<TD><asp:textbox id="txtAddress" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red">*<FONT color="red"><FONT color="red">
						<asp:requiredfieldvalidator id="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter an Address"
							ControlToValidate="txtAddress"><</asp:requiredfieldvalidator></FONT></FONT></FONT></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Town/City</TD>
		<TD><asp:textbox id="txtTown" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red"><FONT color="red"><FONT color="red">*
						<asp:requiredfieldvalidator id="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter a Town" ControlToValidate="txtTown"><</asp:requiredfieldvalidator></FONT></FONT></FONT></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px; HEIGHT: 38px"></TD>
		<TD style="WIDTH: 229px; HEIGHT: 38px">Country</TD>
		<TD style="HEIGHT: 38px"><asp:textbox id="txtCountry" runat="server" Width="265px"></asp:textbox></TD>
		<TD style="HEIGHT: 38px" align="right"><FONT color="red"><FONT color="red"><FONT color="red">*
						<asp:requiredfieldvalidator id="RequiredFieldValidator10" runat="server" ErrorMessage="Please enter a Country"
							ControlToValidate="txtCountry"><</asp:requiredfieldvalidator></FONT></FONT></FONT></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 747px; HEIGHT: 19px" colSpan="3">Contact Details &nbsp;<FONT color="red">*</FONT>
			At least one of the following is required</TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Phone Number</TD>
		<TD><asp:textbox id="txtPhoneNumber" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Mobile Number</TD>
		<TD><asp:textbox id="txtMobile" runat="server" Width="265px"></asp:textbox></TD>
		<TD style="HEIGHT: 20px"></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Fax Number</TD>
		<TD><asp:textbox id="txtFax" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Email Address</TD>
		<TD><asp:textbox id="txtEmail" runat="server" Width="265px"></asp:textbox></TD>
		<TD style="HEIGHT: 26px"></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 264px">Passport&nbsp;Details&nbsp;&nbsp;&nbsp;</TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Passport Number</TD>
		<TD><asp:textbox id="txtPassportNumber" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red"><FONT color="red"><FONT color="red">*
						<asp:requiredfieldvalidator id="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter your Passport Number"
							ControlToValidate="txtPassportNumber"><</asp:requiredfieldvalidator></FONT></FONT></FONT></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Country of Issue</TD>
		<TD><asp:textbox id="txtPassportCountry" runat="server" Width="265px"></asp:textbox></TD>
		<TD align="right"><FONT color="red">*
				<asp:requiredfieldvalidator id="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter the Country Of Issue"
					ControlToValidate="txtCountryOfIssue"><</asp:requiredfieldvalidator></FONT></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 264px">Arrival&nbsp;Details
		</TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Arrival Method</TD>
		<TD><asp:dropdownlist id="ddlArivalMethod" CssClass="NormalDDL" runat="server" Width="82px">
				<asp:ListItem Value="Plane" Selected="True">Plane</asp:ListItem>
				<asp:ListItem Value="Boat">Boat</asp:ListItem>
				<asp:ListItem Value="Train">Train</asp:ListItem>
				<asp:ListItem Value="Ferry">Ferry</asp:ListItem>
				<asp:ListItem Value="Taxi">Taxi</asp:ListItem>
				<asp:ListItem></asp:ListItem>
			</asp:dropdownlist>&nbsp;</TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Flight/ship number</TD>
		<TD><asp:textbox id="txtArrFlightNumber" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Arrival Date &amp; Time</TD>
		<TD><asp:textbox id="txtArrivalDate" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Arrival Location</TD>
		<TD><asp:textbox id="txtArrLocation" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 264px">Departure&nbsp;Details
		</TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<tr class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Departure&nbsp;Method</TD>
		<TD><asp:dropdownlist id="DropDownList1" CssClass="NormalDDL" runat="server" Width="82px">
				<asp:ListItem Value="Plane" Selected="True">Plane</asp:ListItem>
				<asp:ListItem Value="Boat">Boat</asp:ListItem>
				<asp:ListItem Value="Train">Train</asp:ListItem>
				<asp:ListItem Value="Ferry">Ferry</asp:ListItem>
				<asp:ListItem Value="Taxi">Taxi</asp:ListItem>
				<asp:ListItem></asp:ListItem>
			</asp:dropdownlist></TD>
		<TD></TD>
	</tr>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Flight/ship number</TD>
		<TD><asp:textbox id="txtDepFlightNumber" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Departure&nbsp;&nbsp;Date &amp; Time</TD>
		<TD><asp:textbox id="txtDepDate" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Arrival Location</TD>
		<TD><asp:textbox id="txtDepLocation" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 264px">Special Requirements</TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px">Special Requirements</TD>
		<TD><asp:textbox id="txtSpecial" runat="server" Width="265px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="RowHeading">
		<TD style="WIDTH: 264px">Notes</TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px" vAlign="top">Notes</TD>
		<TD><asp:textbox id="txtNotes" runat="server" Width="265px" MaxLength="256" TextMode="MultiLine"
				Height="66px"></asp:textbox></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px"></TD>
		<TD></TD>
		<TD></TD>
	</TR>
	<TR class="normal">
		<TD class="CellHeading" style="WIDTH: 264px"></TD>
		<TD style="WIDTH: 229px"></TD>
		<TD align="right"><asp:button id="btnSave" runat="server" Width="80px" Text="Save"></asp:button></TD>
		<TD></TD>
	</TR>
	<TR>
		<TD class="CellHeading" style="WIDTH: 264px" colSpan="3">
			<P>
				<asp:validationsummary id="ValidationSummary1" runat="server" DESIGNTIMEDRAGDROP="294"></asp:validationsummary>
				<asp:label id="lblError" runat="server" CssClass="warning"></asp:label></P>
		</TD>
		<TD align="right"></TD>
	</TR>
</TABLE>
