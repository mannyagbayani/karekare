'--------------------------------------------------------------------------------------
'   Class:          PreRegister
'   Description:    Captures all preregistration details for a particular booking
'--------------------------------------------------------------------------------------

Partial Class PreRegister
    Inherits System.Web.UI.UserControl
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Property BookingId() As String
        Get
            Return CStr(viewstate("BookingID"))
        End Get
        Set(ByVal Value As String)
            viewstate("BookingID") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblError.Text = ""
        If Not Page.IsPostBack Then
            'LoadDropDownLists()
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'validate data and send via webservice
        If Validate() = True Then
            Try
                SaveDetails()
            Catch ex As Exception
                Dim message As String = ex.Message
            End Try

        End If

    End Sub

    Public Sub InitPreRegister(ByVal bookingId As String)
        Me.BookingId = bookingId
        txtBookingConfirmNo.Text = bookingId
    End Sub

    Private Sub SaveDetails()
        Dim RQ As New OTA_VehModifyRQ
        Dim RS As New OTA_VehModifyRS
        ' POS
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}

        ' Unique Id
        Dim OuniqId As New UniqueID_Type
        OuniqId.Type = "1"
        OuniqId.ID = Me.BookingId

        ' Locations
        Dim oRenCore As New VehicleRentalCoreType
        oRenCore.PickUpLocation = New LocationType
        oRenCore.PickUpLocation.LocationCode = "NA"
        oRenCore.ReturnLocation = New LocationType
        oRenCore.ReturnLocation.LocationCode = "NA"
        oRenCore.PickUpDateTime = DateTime.Now
        oRenCore.ReturnDateTime = DateTime.Now

        ' Attributes
        Dim vehModRq As New OTA_VehModifyRQVehModifyRQCore
        vehModRq.UniqueID = OuniqId
        vehModRq.VehRentalCore = oRenCore

        vehModRq.ModifyType = TransactionActionType.Book


        'customer name
        Dim vehModCustomer As New CustomerPrimaryAdditionalType
        Dim primaryType As New VehicleCustomerType
        Dim personNameType As New PersonNameType

        personNameType.NameTitle = New String() {ddlTitle.SelectedItem.Text}
        personNameType.GivenName = New String() {txtFirstName.Text}
        personNameType.MiddleName = New String() {"NA"}
        personNameType.Surname = txtSurname.Text
        primaryType.BirthDate = New Date(CInt(txtDOBYear.Text), CInt(ddlDOBMonth.SelectedItem.Value), CInt(txtDOBDay.Text))

        primaryType.PersonName = personNameType

        Dim telephoneType As New TelephoneInfoType
        telephoneType.PhoneNumber = txtPhoneNumber.Text
        primaryType.Telephone = New TelephoneInfoType() {telephoneType}

        Dim emailType As New EmailType
        emailType.Value = txtEmail.Text
        primaryType.Email = New EmailType() {emailType}

        'customer address
        Dim addressType As New AddressInfoType
        Dim srtNumberType As New StreetNmbrType
        srtNumberType.PO_Box = "NA"
        addressType.StreetNmbr = srtNumberType
        addressType.BldgRoom = "NA"
        addressType.AddressLine = New String() {txtAddress.Text}
        addressType.CityName = txtTown.Text
        addressType.PostalCode = "NA"
        addressType.County = txtCountry.Text

        primaryType.Address = New AddressInfoType() {addressType}

        'extra info4
        Dim cusPreRegInfoType As New CusPreRegInfo

        cusPreRegInfoType.DateOfBirth = New Date(CInt(txtDOBYear.Text), CInt(ddlDOBMonth.SelectedItem.Value), CInt(txtDOBDay.Text))
        cusPreRegInfoType.ExpiryDate = New Date(CInt(txtExpiryYear.Text), CInt(ddlLicenceMonth.SelectedItem.Value), CInt(txtExpiryDay.Text))
        cusPreRegInfoType.IssuingAuthority = txtAuthority.Text
        cusPreRegInfoType.LicenseNumber = txtLicenenseNo.Text

        primaryType.TPA_Extensions = New CusPreRegInfo() {cusPreRegInfoType}

        vehModCustomer.Primary = primaryType
        vehModRq.Customer = vehModCustomer
        RQ.VehModifyRQCore = vehModRq

        'Dim ser1 As XmlSerializer
        'Dim w1 As StringWriter

        'ser1 = New XmlSerializer(GetType(OTA_VehModifyRQ))
        'w1 = New StringWriter
        'ser1.Serialize(w1, RQ)
        'w1.ToString()

        RS = Proxy.VehModify(RQ)


        If Not IsNothing(RS.Errors) AndAlso RS.Errors.Length > 0 Then
            'an error occured, display it
            Dim errorString As String
            Dim i As Integer
            For i = 0 To RS.Errors.Length - 1
                errorString &= RS.Errors(i).Tag & "<br>"
            Next
            lblError.Text = errorString

        End If


    End Sub

    Private Function Validate() As Boolean
        Dim success As Boolean = True
        Dim sb As New System.Text.StringBuilder

        If txtBookingConfirmNo.Text.Trim = "" Then
            sb.Append("Please enter a Booking Confirmation Number" & "<br>")
            success = False
        End If

        If txtFirstName.Text.Trim = "" Then
            sb.Append("Please enter a Firstname" & "<br>")
            success = False
        End If

        If txtSurname.Text.Trim = "" Then
            sb.Append("Please enter a surname" & "<br>")
            success = False
        End If

        If txtDOBDay.Text.Trim = "" Then
            sb.Append("Please enter a DOB day" & "<br>")
            success = False
        ElseIf Not IsNumeric(txtDOBDay.Text.Trim) Then
            sb.Append("DOB day must be numeric" & "<br>")
            success = False
        End If

        If txtDOBYear.Text.Trim = "" Or txtDOBYear.Text.Length < 4 Then
            sb.Append("DOB year must be 4 digits" & "<br>")
            success = False
        ElseIf Not IsNumeric(txtDOBYear.Text.Trim) Then
            sb.Append("DOB year must be numeric" & "<br>")
            success = False
        End If

        If txtLicenenseNo.Text.Trim = "" Then
            sb.Append("Please enter a License number" & "<br>")
            success = False
        End If


        If txtExpiryDay.Text.Trim = "" Then
            sb.Append("Please enter a License expiry day" & "<br>")
            success = False
        ElseIf Not IsNumeric(txtExpiryDay.Text.Trim) Then
            sb.Append("License expiry day must be numeric" & "<br>")
            success = False
        End If

        If txtExpiryYear.Text.Trim = "" Or txtExpiryYear.Text.Length < 4 Then
            sb.Append("License expiry year must be 4 digits" & "<br>")
            success = False
        ElseIf Not IsNumeric(txtExpiryYear.Text.Trim) Then
            sb.Append("License expiry year must be numeric" & "<br>")
            success = False
        End If

        If txtAuthority.Text.Trim = "" Then
            sb.Append("Please enter an issuing authority" & "<br>")
            success = False
        End If

        If txtCountryOfIssue.Text.Trim = "" Then
            sb.Append("Please enter the Country of Issue" & "<br>")
            success = False
        End If

        If txtAddress.Text.Trim = "" Then
            sb.Append("Please enter an Address" & "<br>")
            success = False
        End If

        If txtTown.Text.Trim = "" Then
            sb.Append("Please enter a Town" & "<br>")
            success = False
        End If

        If txtCountry.Text.Trim = "" Then
            sb.Append("Please enter a Country" & "<br>")
            success = False
        End If

        If txtPassportNumber.Text.Trim = "" Then
            sb.Append("Please enter a Passport Number" & "<br>")
            success = False
        End If

        If txtPassportCountry.Text.Trim = "" Then
            sb.Append("Please enter the Country of Issus" & "<br>")
            success = False
        End If


        Try
            Dim birthdate As Date = New Date(CInt(txtDOBYear.Text), CInt(ddlDOBMonth.SelectedItem.Value), CInt(txtDOBDay.Text))
        Catch ex As Exception
            sb.Append("Invalid BirthDate" & "<br>")
            success = False
        End Try

        Try
            Dim birthdate As Date = New Date(CInt(txtExpiryYear.Text), CInt(ddlLicenceMonth.SelectedItem.Value), CInt(txtExpiryDay.Text))
        Catch ex As Exception
            sb.Append("Invalid License Expiry" & "<br>")
            success = False
        End Try


        lblError.Text = sb.ToString
        Return success
    End Function

    'Private Sub LoadDropDownLists()
    '	Dim oneYear As Integer
    '	Dim year As Integer
    '	Dim arrYear As New ArrayList

    '	For year = 1910 To DateTime.Now.Year
    '		arrYear.Add(year)
    '		year = year + 1
    '	Next

    '	Dim day As Integer
    '	Dim arrDay As New ArrayList
    '	For day = 1 To 31
    '		arrDay.Add(day)
    '		day = day + 1
    '	Next

    '	ddlDOBDay.DataSource = arrDay
    '	ddlDOBDay.DataBind()

    '	ddlDOBYear.DataSource = arrYear
    '	ddlDOBYear.DataBind()

    '	ddlLicenseDay.DataSource = arrDay
    '	ddlLicenseDay.DataBind()

    '	ddlLicenseYear.DataSource = arrYear
    '	ddlLicenseYear.DataBind()


    'End Sub


End Class
