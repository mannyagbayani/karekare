<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FeesAndCharges.ascx.vb"
    Inherits="THLAuroraWebInterface.FeesAndCharges" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table id="outerTable" runat="server" width="745" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table width="745" border="0" cellspacing="0" cellpadding="2">
                <tr class="StdConfirmationHeading">
                    <td width="350">
                        Description
                    </td>
                    <td align="right" width="86">
                        Quantity
                    </td>
                    <td align="right" width="85">
                        Unit Name
                    </td>
                    <td align="right" width="111">
                        UnitCharge
                    </td>
                    <td align="right" width="93">
                        Amount
                    </td>
                </tr>
                <asp:Repeater ID="rpCharges" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="normal">
                                <%#container.dataitem("Description")%>
                            </td>
                            <td align="right" class="normal">
                                <%#container.dataitem("Quantity")%>
                            </td>
                            <td align="right" class="normal">
                                <%#container.dataitem("UnitName")%>
                            </td>
                            <td align="right" class="normal">
                                <%#container.dataitem("UnitCharge")%>
                            </td>
                            <td class="normal" align="right">
                                <%#container.dataitem("Amount")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <tr id="TotalChargeRow" runat="server">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td class="StdConfirmationHeading" align="right" colspan="2">
                        Total Charged
                    </td>
                    <td class="normal" align="right">
                        <asp:Label ID="lblTotalRentalCharge" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="AgentRow" runat="server">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td class="StdConfirmationHeading" align="right" colspan="2">
                        Less Agent Discount
                    </td>
                    <td class="normal" align="right">
                        <asp:Label ID="lblAgentDiscount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="PaymentReceivedRow" runat="server">
                    <td style="height: 25px">
                    </td>
                    <td style="height: 25px">
                    </td>
                    <td class="StdConfirmationHeading" align="right" colspan="2" style="height: 24px">
                        Less Payment Received
                    </td>
                    <td class="normal" align="right" style="height: 24px">
                        <asp:Label ID="lblPaymentReceived" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="TotalPayableRow" runat="server">
                    <td>
                    </td>
                    <td class="StdConfirmationHeading" align="right" colspan="3" nowrap>
                        <asp:Label ID="lblTotalPayableHeadingText" runat="server"></asp:Label>
                    </td>
                    <td class="normal" align="right">
                        <asp:Label ID="lblTotalPayable" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="GSTRow" runat="server">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td colspan="2" class="StdConfirmationHeading" align="right">
                        <%--Changed By Nimesh on 23rd Sep 2014--%>
                        <%--GST Content--%>
                        <%= GSTContentText %>
                    </td>
                    <td class="normal" align="right">
                        <asp:Label ID="lblGst" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<span><%= OtherTaxesText%></span>
