<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FeesAndChargesNewBooking.ascx.vb"
    Inherits="THLAuroraWebInterface.FeesAndChargesNewBooking" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<p>
    <table id="Table1" cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr class="StdConfirmationHeading">
            <td>
                Description
            </td>
            <td align="right">
                Unit Charge
            </td>
            <td align="right">
                Quantity
            </td>
            <td align="right">
                Unit Name
            </td>
            <td align="right">
                <asp:Label ID="lblRateType" runat="server" />Amount
            </td>
        </tr>
        <asp:Repeater ID="rpCharges" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="normal">
                    <td>
                        <%#container.dataitem("ChargeDescription")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("UnitCharge")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("Quantity")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("UnitName")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("ChargeAmount")%>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>

        <tr>
            <td>
            </td>
            <td class="stdconfirmationheading" align="right" colspan="3">
                Total Payable to Agent (Inclusive of GST)
            </td>
            <td class="normal" align="right">
                <asp:Label ID="lblTotalPayableToAgentIncGST" runat="server"></asp:Label>
            </td>
        </tr>

        <asp:Repeater ID="rpFees" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="normal">
                    <td>
                        <%#container.dataitem("FeeDescription")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("FeeUnitCharge")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("FeeQuantity")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("FeeUnitName")%>
                    </td>
                    <td align="right">
                        <%#container.dataitem("FeeAmount")%>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>

         <tr>
            <td>
            </td>
            <td class="stdconfirmationheading" align="right" colspan="3">
                Total Payable at Pick Up (Inclusive of GST)
            </td>
            <td class="normal" align="right">
                <asp:Label ID="lblTotalPayableAtPickUpIncGST" runat="server"></asp:Label>
            </td>
        </tr>

        <%--Old and unwanted rows--%>
        <%--<tr>
            <td>
            </td>
            <td class="stdconfirmationheading" align="right" colspan="3">
                Total Rental Charge
            </td>
            <td class="normal" align="right">
                <asp:Label ID="lblRentalCharge" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td class="stdconfirmationheading" align="right" colspan="3">
                Total Payable (Inclusive of GST)
            </td>
            <td class="normal" align="right">
                <asp:Label ID="lblTotalCharges" runat="server"></asp:Label>
            </td>
        </tr>--%>
        <%--Old and unwanted rows--%>
    </table>
</p>
