'--------------------------------------------------------------------------------------
'   Class:          FeesAndChargesNewBooking
'   Description:    Displays Fees or Charges for an new booking (not in system yet) or 
'                   an existing booking
'                   Can be used with both the OTA_VehAvailRateRS (for new bookings)
'                   and OTA_VehModifyRS (to display fees and charges for a modified booking)
'                   This control is generally used to show the fees and chages for a selected package (Availability.ascx)
'                   (as a result of the options the user has selected in LocationAndTime) 
'--------------------------------------------------------------------------------------

Partial Class FeesAndChargesNewBooking
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim mVehAvailRateRS As OTA_VehAvailRateRS
    Dim mVehModifyRS As OTA_VehModifyRS

    Dim mVendorIndex As Integer
    Dim mVehicleIndex As Integer
    Dim dsFeesAndCharges As DataSet
    Dim dtFees As DataTable = New DataTable("Fees")
    Dim dtRentalCharges As DataTable = New DataTable("Charges")

#Region "Customer Confirmation Changes"
    Public ReadOnly Property GrossModeSelected() As Boolean
        Get
            If Session("GrossModeSelected") IsNot Nothing Then
                Return CBool(Session("GrossModeSelected"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property NettModeSelected() As Boolean
        Get
            If Session("NettModeSelected") IsNot Nothing Then
                Return CBool(Session("NettModeSelected"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRateIsNettEvenThoughGrossOptionWasSelected() As Boolean
        Get
            If Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected") IsNot Nothing Then
                Return CBool(Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected"))
            Else
                Return False
            End If
        End Get
    End Property
#End Region

#Region "Currency Changes"
    Public ReadOnly Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property MRU_CurrencyCode() As String
        Get
            Return CStr(Session("MRU_CurrencyCode"))
        End Get
    End Property

    Public ReadOnly Property MRU_CurrencyMultiplier() As Double
        Get
            If Session("MRU_CurrencyMultiplier") IsNot Nothing Then
                Return CDbl(Session("MRU_CurrencyMultiplier"))
            Else
                Return 0.0
            End If
        End Get
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
    End Sub

    Public Sub InitFeesAndChargesNewBooking(ByVal vehAvailRateRS As OTA_VehAvailRateRS, ByVal vendorIndex As Integer, ByVal vehicleIndex As Integer)
        ' This control will display both fees and charges for a vehicle package
        ' Used to load fees and charges for an new booking
        ' Use this to display fees and charges derived form the vehavailRate object (prices and bookings page, make booking)

        mVehAvailRateRS = vehAvailRateRS
        ' VendorIndex and  vehicleIndex specifies the selected vehicle package 
        ' which was seleceted by the user.
        mVendorIndex = vendorIndex
        mVehicleIndex = vehicleIndex

        DisplayFeesAndChargesNewBooking()
    End Sub

    Public Sub InitFeesAndChargesModifyBooking(ByVal vehModifyRS As OTA_VehModifyRS)
        ' Used to load fees and charges for an existing booking
        ' Use this to display fees and charges derived form the OTA_VehModifyRS object (quotes and bookings page, modify booking)

        mVehModifyRS = vehModifyRS

        DisplayFeesAndChargesModifyBooking()
    End Sub

    Private Sub DisplayFeesAndChargesNewBooking()

        ' read the gross/nett mode info

        ' Load fee and charges repeater

        CreateFeeAndChargesDatasets()

        Dim feeIndex As Integer
        Dim currency As String


        ' new logic for agent charge and pay at pickup charge
        ' go thru all fees and vehicle charges
        ' if purpose is = 1, then agent charge.
        ' if purpose is = 2, then pay at pickup charge

        If UserHasCurrencyOptions Then

            Dim vehicleChargeIndex As Integer

            For vehicleChargeIndex = 0 To mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges.Length - 1

                If mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Purpose = "1" Then
                    ' Agent Charge
                    ' currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).CurrencyCode.ToString

                    Dim dr As DataRow = dtRentalCharges.NewRow
                    dr("ChargeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Description.ToString
                    dr("ChargeAmount") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Amount * MRU_CurrencyMultiplier).ToString("f2") & " " & MRU_CurrencyCode

                    dr("UnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitName.ToString
                    dr("UnitCharge") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitCharge * MRU_CurrencyMultiplier).ToString("f2") & " " & MRU_CurrencyCode
                    dr("Quantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).Quantity.ToString
                    dtRentalCharges.Rows.Add(dr)
                Else
                    ' Pay at pickup charge
                    Dim dr As DataRow = dtFees.NewRow
                    'currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(vehicleChargeIndex).CurrencyCode.ToString
                    currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).CurrencyCode.ToString


                    dr("FeeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Description.ToString
                    dr("FeeAmount") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Amount).ToString("f2") & " " & currency

                    dr("FeeUnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitName.ToString
                    dr("FeeUnitCharge") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitCharge).ToString("f2") & " " & currency
                    dr("FeeQuantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).Quantity.ToString
                    dtFees.Rows.Add(dr)
                End If

            Next

            If Not IsNothing(mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees) Then
                For feeIndex = 0 To mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees.Length - 1


                    If mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Purpose = "1" Then
                        ' Agent Charge
                        Dim dr As DataRow = dtRentalCharges.NewRow
                        dr("ChargeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Description.ToString
                        dr("ChargeAmount") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Amount * MRU_CurrencyMultiplier).ToString("f2") & " " & MRU_CurrencyCode

                        dr("UnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitName.ToString
                        dr("UnitCharge") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitCharge * MRU_CurrencyMultiplier).ToString("f2") & " " & MRU_CurrencyCode
                        dr("Quantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).Quantity.ToString
                        dtRentalCharges.Rows.Add(dr)
                    Else
                        ' Pay at pickup charge
                        Dim dr As DataRow = dtFees.NewRow
                        'currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).CurrencyCode
                        currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(0).CurrencyCode.ToString
                        'mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).CurrencyCode.ToString

                        dr("FeeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Description.ToString
                        dr("FeeAmount") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Amount).ToString("f2") & " " & currency

                        dr("FeeUnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitName.ToString
                        dr("FeeUnitCharge") = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitCharge).ToString("f2") & " " & currency
                        dr("FeeQuantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).Quantity.ToString
                        dtFees.Rows.Add(dr)
                    End If


                Next
                'dsFeesAndCharges.Tables.Add(dtFees)
            End If


            dsFeesAndCharges.Tables.Add(dtRentalCharges)
            dsFeesAndCharges.Tables.Add(dtFees)
            lblTotalPayableToAgentIncGST.Text = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.THLAgentCharge * MRU_CurrencyMultiplier).ToString("f2") & " " & MRU_CurrencyCode
            lblTotalPayableAtPickUpIncGST.Text = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.THLCustomerCharge).ToString("f2") & " " & currency

            ' lblRentalCharge.Text = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.RateTotalAmount * MRU_CurrencyMultiplier).ToString("f2") & " " & MRU_CurrencyCode

            ' lblTotalCharges.Text = (mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.EstimatedTotalAmount * MRU_CurrencyMultiplier).ToString("f2") & " " & MRU_CurrencyCode

        Else
            Dim vehicleChargeIndex As Integer

            For vehicleChargeIndex = 0 To mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges.Length - 1

                If mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Purpose = "1" Then
                    ' Agent Charge
                    currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).CurrencyCode.ToString

                    Dim dr As DataRow = dtRentalCharges.NewRow
                    dr("ChargeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Description.ToString
                    dr("ChargeAmount") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Amount.ToString & " " & currency

                    dr("UnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitName.ToString
                    dr("UnitCharge") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                    dr("Quantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).Quantity.ToString

                    dtRentalCharges.Rows.Add(dr)
                Else
                    ' pay at pick up charge
                    Dim dr As DataRow = dtFees.NewRow
                    'currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(vehicleChargeIndex).CurrencyCode.ToString
                    '-RKS: 12-Jan-2011
                    '-Fees node was failing
                    currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).CurrencyCode.ToString

                    dr("FeeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Description.ToString
                    dr("FeeAmount") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Amount.ToString & " " & currency

                    dr("FeeUnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitName.ToString
                    dr("FeeUnitCharge") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                    dr("FeeQuantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(vehicleChargeIndex).Calculation(0).Quantity.ToString
                    dtFees.Rows.Add(dr)

                End If

            Next

            If Not IsNothing(mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees) Then
                For feeIndex = 0 To mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees.Length - 1

                    If mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Purpose = "1" Then
                        ' Agent Charge
                        currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).CurrencyCode
                        ' mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(feeIndex).CurrencyCode.ToString

                        Dim dr As DataRow = dtRentalCharges.NewRow
                        dr("ChargeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Description.ToString
                        dr("ChargeAmount") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Amount() & " " & currency

                        dr("UnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitName.ToString
                        dr("UnitCharge") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                        dr("Quantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).Quantity.ToString

                        dtRentalCharges.Rows.Add(dr)
                    Else
                        ' pay at pick up charge
                        Dim dr As DataRow = dtFees.NewRow
                        currency = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).CurrencyCode.ToString

                        dr("FeeDescription") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Description.ToString
                        dr("FeeAmount") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Amount() & " " & currency

                        dr("FeeUnitName") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitName.ToString
                        dr("FeeUnitCharge") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                        dr("FeeQuantity") = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Fees(feeIndex).Calculation(0).Quantity.ToString
                        dtFees.Rows.Add(dr)
                    End If

                Next
                'dsFeesAndCharges.Tables.Add(dtFees)
            End If

            dsFeesAndCharges.Tables.Add(dtRentalCharges)
            dsFeesAndCharges.Tables.Add(dtFees)
            lblTotalPayableToAgentIncGST.Text = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.THLAgentCharge.ToString("f2") & " " & currency
            lblTotalPayableAtPickUpIncGST.Text = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.THLCustomerCharge.ToString("f2") & " " & currency

            ' lblRentalCharge.Text = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.RateTotalAmount.ToString & " " & currency

            ' lblTotalCharges.Text = mVehAvailRateRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.TotalCharge.EstimatedTotalAmount.ToString & " " & currency
        End If

        rpCharges.DataSource = dsFeesAndCharges.Tables("Charges")
        rpCharges.DataBind()

        ' Customer Confirmation Change
        If NettModeSelected OrElse SelectedRateIsNettEvenThoughGrossOptionWasSelected Then
            lblRateType.Text = "Nett&nbsp;"
        Else
            lblRateType.Text = "Gross&nbsp;"
        End If



        If Not IsNothing(dsFeesAndCharges.Tables("Fees")) Then
            rpFees.DataSource = dsFeesAndCharges.Tables("Fees")
            rpFees.DataBind()
            rpFees.Visible = True

        Else
            rpFees.Visible = False
        End If
    End Sub


    Private Sub DisplayFeesAndChargesModifyBooking()
        ' Load fee and charges repeater

        CreateFeeAndChargesDatasets()

        Dim feeIndex As Integer
        Dim currency As String

        For feeIndex = 0 To mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees.Length - 1

            If mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(feeIndex).Purpose = "1" Then
                ' Agent Charge
                Dim dr As DataRow = dtRentalCharges.NewRow

                currency = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(feeIndex).CurrencyCode

                dr("ChargeDescription") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(feeIndex).Description.ToString
                dr("ChargeAmount") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(feeIndex).Amount.ToString & " " & currency
                dr("UnitName") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(feeIndex).Calculation(0).UnitName.ToString
                dr("UnitCharge") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(feeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                dr("Quantity") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(feeIndex).Calculation(0).Quantity.ToString

                dtRentalCharges.Rows.Add(dr)
            Else
                ' Pay at pickup charge
                Dim dr As DataRow = dtFees.NewRow

                currency = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(feeIndex).CurrencyCode.ToString

                dr("FeeDescription") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(feeIndex).Description.ToString
                dr("FeeAmount") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(feeIndex).Amount() & " " & currency

                'there will only ever be 1 calculation node returned
                dr("FeeUnitName") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(feeIndex).Calculation(0).UnitName.ToString
                dr("FeeUnitCharge") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(feeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                dr("FeeQuantity") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(feeIndex).Calculation(0).Quantity.ToString
                dtFees.Rows.Add(dr)
            End If


        Next

        'dsFeesAndCharges.Tables.Add(dtFees)

        Dim vehicleChargeIndex As Integer

        For vehicleChargeIndex = 0 To mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges.Length - 1

            If mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(vehicleChargeIndex).Purpose = "1" Then
                ' Agent Charge
                Dim dr As DataRow = dtRentalCharges.NewRow

                dr("ChargeDescription") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(vehicleChargeIndex).Description.ToString
                dr("ChargeAmount") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(vehicleChargeIndex).Amount.ToString & " " & currency
                dr("UnitName") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(vehicleChargeIndex).Calculation(0).UnitName.ToString
                dr("UnitCharge") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(vehicleChargeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                dr("Quantity") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(vehicleChargeIndex).Calculation(0).Quantity.ToString

                dtRentalCharges.Rows.Add(dr)
            Else
                ' Pay at pickup charge
                Dim dr As DataRow = dtFees.NewRow

                currency = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(vehicleChargeIndex).CurrencyCode.ToString

                dr("FeeDescription") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(vehicleChargeIndex).Description.ToString
                dr("FeeAmount") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(vehicleChargeIndex).Amount() & " " & currency

                'there will only ever be 1 calculation node returned
                dr("FeeUnitName") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(vehicleChargeIndex).Calculation(0).UnitName.ToString
                dr("FeeUnitCharge") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(vehicleChargeIndex).Calculation(0).UnitCharge.ToString & " " & currency
                dr("FeeQuantity") = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.Fees(vehicleChargeIndex).Calculation(0).Quantity.ToString
                dtFees.Rows.Add(dr)
            End If


        Next
        dsFeesAndCharges.Tables.Add(dtRentalCharges)
        dsFeesAndCharges.Tables.Add(dtFees)
        lblTotalPayableToAgentIncGST.Text = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.TotalCharge.THLAgentCharge.ToString("f2") & " " & currency
        lblTotalPayableAtPickUpIncGST.Text = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.TotalCharge.THLCustomerCharge.ToString("f2") & " " & currency

        ' lblRentalCharge.Text = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.TotalCharge.RateTotalAmount.ToString & " " & currency

        ' lblTotalCharges.Text = mVehModifyRS.VehModifyRSCore.VehReservation.VehSegmentCore.TotalCharge.EstimatedTotalAmount.ToString & " " & currency

        rpCharges.DataSource = dsFeesAndCharges.Tables("Charges")
        rpCharges.DataBind()

        rpFees.DataSource = dsFeesAndCharges.Tables("Fees")
        rpFees.DataBind()
    End Sub
    Private Sub CreateFeeAndChargesDatasets()

        dsFeesAndCharges = New DataSet

        Dim col As DataColumn = New DataColumn
        col.ColumnName = "FeeDescription"
        col.DataType = Type.GetType("System.String")
        dtFees.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "FeeAmount"
        col.DataType = Type.GetType("System.String")
        dtFees.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "FeeUnitName"
        col.DataType = Type.GetType("System.String")
        dtFees.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "FeeUnitCharge"
        col.DataType = Type.GetType("System.String")
        dtFees.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "FeeQuantity"
        col.DataType = Type.GetType("System.String")
        dtFees.Columns.Add(col)
        '--------------------------rental charge rows--------------------------------

        col = New DataColumn
        col.ColumnName = "UnitName"
        col.DataType = Type.GetType("System.String")
        dtRentalCharges.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "UnitCharge"
        col.DataType = Type.GetType("System.String")
        dtRentalCharges.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Quantity"
        col.DataType = Type.GetType("System.String")
        dtRentalCharges.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "ChargeDescription"
        col.DataType = Type.GetType("System.String")
        dtRentalCharges.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "ChargeAmount"
        col.DataType = Type.GetType("System.String")
        dtRentalCharges.Columns.Add(col)
    End Sub


End Class
