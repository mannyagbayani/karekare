Imports System.Diagnostics
#Region "Notes"
'26June2006 Karel Jordaan
'Changed the base from System.Web.UI.UserControl
'to THL_WebUserControlBase 
'update the global propertie for access from other ascx that does not call the database
#End Region
'--------------------------------------------------------------------------------------
'   Class:          FeesAndCharges
'   Description:    Displays Fees or Charges for an existing booking
'--------------------------------------------------------------------------------------

Partial Class FeesAndCharges
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCurrency3 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrency2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrency1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrency As System.Web.UI.WebControls.Label
    Protected WithEvents lblcurrency4 As System.Web.UI.WebControls.Label


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim RS As OTA_VehRetResRS
    Dim mCurrency As String
    Dim mAgentDiscount As String
    Dim mTotalRentalCharge As String
    Dim mTotalPayable As String
    Dim mTotalFees As String
    Dim mPaymentReceived As String
    Dim mGST As String
    Dim mDs As DataSet



    ' Added By Nimesh on 29th Sep 2014 for US Tax
    Private mCountrOfTravel As String

    Public Property CountryOfTravel() As String
        Get
            Return mCountrOfTravel
        End Get
        Set(ByVal value As String)
            mCountrOfTravel = value
        End Set
    End Property


    Public ReadOnly Property GSTContentText() As String
        Get
            Dim retString As String = String.Empty
            If Not String.IsNullOrEmpty(CountryOfTravel) Then
                If CountryOfTravel.Equals("US") Then
                    retString = "(*)"
                Else
                    retString = "GST Content"
                End If
            End If

            Return retString
        End Get

    End Property

    Public ReadOnly Property OtherTaxesText() As String
        Get
            Dim retString As String = String.Empty
            If Not String.IsNullOrEmpty(CountryOfTravel) Then
                If CountryOfTravel.Equals("US") Then
                    retString = "* Pre-paid items include US sales tax. Add local sales tax for items paid onsite."
                End If
            End If

            Return retString
        End Get

    End Property


    ' End Added By Nimesh on 29th Sep 2014 for US Tax
    'Customer Confirmation Changes
    Public ReadOnly Property SelectedRateIsNettEvenThoughGrossOptionWasSelected() As Boolean
        Get
            If Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected") IsNot Nothing Then
                Return CBool(Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected"))
            Else
                Return False
            End If
        End Get
    End Property
    'Customer Confirmation Changes

#Region "currency changes"
    Public ReadOnly Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
    End Property
#End Region

    Public Enum ControlUse
        'This contol is used for..
        RentalCharges = 0           ' this means Agent Version of Rental Charges
        Fees = 1                    ' this means Agent Version of Fees
        CustomerRentalCharges = 2   ' this means Customer Version of Rental Charges
        CustomerFees = 3            ' this means Customer Version of Fees
    End Enum
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
    End Sub


    Public Sub InitChargesAndFees(ByVal VehRetResRes As OTA_VehRetResRS, ByVal Multiplier As Double, ByVal CurrencyCode As String, ByVal UseControlAs As ControlUse)
        ' This contol is used to display fee or charge information for a booking that
        ' is already in the system (existing booking)

        RS = VehRetResRes
        StatusBarText = RS.Warnings(0).Tag
        UIScripts.StatusMsg(Me.Page, RS.Warnings(0).Tag)
        SetPrices()

        Select Case UseControlAs
            Case ControlUse.RentalCharges
                ' This control is used for rental charges (Agent Charges)
                ' Show agent discount
                AgentRow.Visible = True
                LoadRentalChargesData(Multiplier, CurrencyCode)
            Case ControlUse.Fees
                ' This control is used for fee charges (Customer Charges)
                ' Hide agent discount 
                AgentRow.Visible = False
                'LoadFeeData(Multiplier, CurrencyCode)
                LoadFeeData(1.0, mCurrency)
            Case ControlUse.CustomerRentalCharges
                ' This control is used for rental charges (Agent Charges)
                ' Show agent discount
                'AgentRow.Visible = True
                AgentRow.Visible = False
                LoadRentalChargesData(Multiplier, CurrencyCode, UseControlAs)
            Case ControlUse.CustomerFees
                ' This control is used for fee charges (Customer Charges)
                ' Hide agent discount 
                AgentRow.Visible = False
                'LoadFeeData(Multiplier, CurrencyCode)
                LoadFeeData(1.0, mCurrency)
        End Select

        'If UseControlAs = ControlUse.RentalCharges Then
        '    ' This control is used for rental charges (Agent Charges)
        '    ' Show agent discount
        '    AgentRow.Visible = True
        '    LoadRentalChargesData()
        'Else
        '    ' This control is used for fee charges (Customer Charges)
        '    ' Hide agent discount 
        '    AgentRow.Visible = False
        '    LoadFeeData()
        'End If

        SetProperties(Multiplier, CurrencyCode, UseControlAs)

        If rpCharges.Items.Count = 0 Then
            'hide repeater as there are no charges
            outerTable.Visible = False
        Else
            outerTable.Visible = True
        End If

    End Sub


    Private Sub SetProperties(ByVal Multiplier As Double, ByVal CurrencyCode As String, ByVal UseControlAs As ControlUse)
        ' Disaplay prices as set in SetPrices()
        If Not UserHasCurrencyOptions OrElse (UseControlAs <> ControlUse.RentalCharges And UseControlAs <> ControlUse.CustomerRentalCharges) Then
            CurrencyCode = mCurrency
            Multiplier = 1.0
        End If

        ' <<TO DO>> does this get changed??????
        'lblPaymentReceived.Text = mPaymentReceived & " " & mCurrency
        'lblGst.Text = mGST & " " & mCurrency
        lblPaymentReceived.Text = (CDbl(mPaymentReceived) * Multiplier).ToString("f2") & " " & CurrencyCode
        lblGst.Text = (CDbl(mGST) * Multiplier).ToString("f2") & " " & CurrencyCode

        Dim dTotalRentalCharge As Double = 0
        Dim dTotalPayableCharge As Double = 0
        Dim dPaymentReceived As Double = 0

        Select Case UseControlAs
            Case ControlUse.RentalCharges
                ' This will be displayed to Agents
                If mTotalRentalCharge = "" Then ' No total charge , hide details
                    TotalChargeRow.Visible = False
                Else
                    TotalChargeRow.Visible = True
                    lblTotalRentalCharge.Text = (CDbl(mTotalRentalCharge) * Multiplier).ToString("f2") & " " & CurrencyCode
                End If

                If mTotalPayable = "" Then ' No total payable, hide details
                    TotalPayableRow.Visible = False
                Else
                    TotalPayableRow.Visible = True
                    lblTotalPayable.Text = (CDbl(mTotalPayable) * Multiplier).ToString("f2") & " " & CurrencyCode
                End If

                If mAgentDiscount = "" Then ' No agent discount 
                    lblAgentDiscount.Text = ".00"
                Else
                    lblAgentDiscount.Text = (CDbl(mAgentDiscount) * Multiplier).ToString("f2") & " " & CurrencyCode
                End If

                AgentRow.Visible = True 'Show agent discount
                PaymentReceivedRow.Visible = False ' Show payment received

                'Following Line of Text was changed by Nimesh on 23rd Sep 2014
                'lblTotalPayableHeadingText.Text = "Nett Payable by Agent (Inclusive of GST)"
                '/*Start of Change*/
                lblTotalPayableHeadingText.Text = IIf(mCountrOfTravel = "US", "Nett Payable by Agent (*)", "Nett Payable by Agent (Inclusive of GST)").ToString()
                '/*End of Change*/
                'End Changed By Nimesh
            Case ControlUse.Fees
                ' This will be displayed to customers
                ' Don't show agent discount
                AgentRow.Visible = False
                ' But show payment recieved 
                PaymentReceivedRow.Visible = True
                lblTotalRentalCharge.Text = mTotalFees & " " & mCurrency
                'lblTotalPayable.Text = mTotalFees & " " & mCurrency
                'Following Line of Text was changed by Nimesh on 23rd Sep 2014
                'lblTotalPayableHeadingText.Text = "Customer to pay on arrival (Inclusive of GST)"
                '/*Start of Change*/
                lblTotalPayableHeadingText.Text = IIf(mCountrOfTravel = "US", "Customer to pay on arrival (*)", "Customer to pay on arrival (Inclusive of GST)").ToString()
                '/*End of Change*/
                'End Changed by Nimesh
                dTotalRentalCharge = CDbl(mTotalFees)
                dTotalPayableCharge = dTotalRentalCharge - CDbl(mPaymentReceived)
                lblTotalPayable.Text = (dTotalPayableCharge * Multiplier).ToString("f2") & " " & CurrencyCode

            Case ControlUse.CustomerRentalCharges
                ' This will be displayed to Customers

                If mTotalRentalCharge = "" Then ' No total charge , hide details
                    TotalChargeRow.Visible = False
                Else
                    TotalChargeRow.Visible = True
                    lblTotalRentalCharge.Text = (CDbl(mTotalRentalCharge) * Multiplier).ToString("f2") & " " & CurrencyCode
                    dTotalRentalCharge = CDbl(mTotalRentalCharge)
                End If

                If mTotalPayable = "" Then ' No total payable, hide details
                    TotalPayableRow.Visible = False
                Else
                    TotalPayableRow.Visible = True
                    If mPaymentReceived <> "" Then
                        dPaymentReceived = CDbl(mPaymentReceived)
                    End If
                    dTotalPayableCharge = dTotalRentalCharge - dPaymentReceived
                    lblTotalPayable.Text = (dTotalPayableCharge * Multiplier).ToString("f2") & " " & CurrencyCode
                End If

                PaymentReceivedRow.Visible = False ' Show payment received
                'Following Line of Text was changed by Nimesh on 23rd Sep 2014
                'lblTotalPayableHeadingText.Text = "Total Payable (Inclusive of GST)"
                '/*Start of Change*/
                lblTotalPayableHeadingText.Text = IIf(mCountrOfTravel = "US", "Total Payable (*)", "Total Payable (Inclusive of GST)").ToString()
                '/*End of Change*/
                'End Changed by Nimesh
                AgentRow.Visible = False
                lblGst.Text = "Included"

                ' Customer Confirmation Changes
                ' weird mode selected ?
                If SelectedRateIsNettEvenThoughGrossOptionWasSelected Then
                    ' hide all total and stuff for rental charges
                    TotalChargeRow.Visible = False
                    TotalPayableRow.Visible = False
                    GSTRow.Visible = False
                End If
                ' weird mode selected ?
                ' Customer Confirmation Changes

            Case ControlUse.CustomerFees
                ' This will be displayed to customers
                ' Don't show agent discount
                AgentRow.Visible = False
                ' But show payment recieved 
                PaymentReceivedRow.Visible = True

                dTotalRentalCharge = CDbl(mTotalFees)
                dTotalPayableCharge = dTotalRentalCharge - CDbl(mPaymentReceived)

                lblTotalRentalCharge.Text = (CDbl(mTotalFees) * Multiplier).ToString("f2") & " " & CurrencyCode
                lblTotalPayable.Text = (dTotalPayableCharge * Multiplier).ToString("f2") & " " & CurrencyCode 'mTotalFees & " " & mCurrency

                'lblTotalPayableHeadingText.Text = "Total Payable (Inclusive of GST)"
                'Following Line of Text was changed by Nimesh on 23rd Sep 2014
                'lblTotalPayableHeadingText.Text = "Total Payable (Inclusive of GST)"
                lblTotalPayableHeadingText.Text = IIf(mCountrOfTravel = "US", "Total Payable (*)", "Total Payable (Inclusive of GST)").ToString()
                'End Changed by Nimesh

                lblGst.Text = "Included"

        End Select
    End Sub

    Private Sub BindRepeater(ByVal ds As DataSet)
        rpCharges.DataSource = ds
        rpCharges.DataBind()
    End Sub


    Private Sub LoadRentalChargesData(ByVal Multiplier As Double, ByVal CurrencyCode As String, Optional ByVal UseControlAs As ControlUse = Nothing)
        'Fill dataset with Rental Charges data and bind to repeater
        Dim ds As DataSet
        ds = New DataSet
        Dim dt As DataTable = New DataTable("RentalCharges")
        Dim col As DataColumn = New DataColumn

        col.ColumnName = "Description"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Quantity"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "UnitName"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "UnitCharge"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Amount"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)
        Dim rentalRateIndex As Integer


        If IsNothing(RS.Errors) Then
            If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges) Then
                For rentalRateIndex = 0 To RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges.Length - 1
                    'Create rows to add to datatable
                    NewRentalChargeRow(dt, rentalRateIndex, Multiplier, CurrencyCode, UseControlAs)
                Next

                ds.Tables.Add(dt)
                BindRepeater(ds)
            End If

        Else
            ' An error occured, display it?
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
            Next
        End If

    End Sub

    Public Sub NewRentalChargeRow(ByRef dt As DataTable, ByVal i As Integer, ByVal Multiplier As Double, ByVal CurrencyCode As String, Optional ByVal UseControlAs As ControlUse = Nothing)

        Dim dr As DataRow = dt.NewRow
        Dim chargeDescription As String = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Description.ToLower
        ''rev:mia Oct. 9 2013 - RoadBear and Britz
        Dim countryOfTravel As String = RS.VehRetResRSCore.VehResSummaries(0).PickUpLocation.Value

        'Added by Nimesh on 26th Sep 2014
        Me.CountryOfTravel = countryOfTravel
        'End Added by Nimesh on 26th Sep 2014
        If chargeDescription = "agentdiscount" Or chargeDescription = "gst" Or chargeDescription = "totalgst" Then
            ' Don't want to include these charges to display
        Else
            Dim currency As String

            If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Calculation) Then
                ' There is calculation data
                If UserHasCurrencyOptions Then
                    currency = CurrencyCode
                Else
                    currency = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).CurrencyCode.ToString()
                    Multiplier = 1.0
                End If

                Dim unitcharge As Decimal = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Calculation(0).UnitCharge
                ''rev:mia Oct. 9 2013 - RoadBear and Britz. multiplier should be zero
                If countryOfTravel = "US" Then
                    Multiplier = 1.0
                End If
                If countryOfTravel = "AU" Then
                    Multiplier = Multiplier
                End If
                If countryOfTravel = "NZ" Then
                    Multiplier = Multiplier
                End If
                General.LogDebugNotes(countryOfTravel & " multiplier is " & Multiplier.ToString() & " and unit charge is " & unitcharge & " ...result is " & currency & (unitcharge * Multiplier))
                ''System.Diagnostics.Debug.Assert(Not String.IsNullOrEmpty(currency), "1.Currency is empty")
                'System.Diagnostics.Debug.Assert(currency <> "USD", "1.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "GBP", "1.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "AUD", "1. Currency is " & currency)

                
                dr("Description") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Description.ToString

                ' Customer Confirmation Changes
                dr("Amount") = IIf(SelectedRateIsNettEvenThoughGrossOptionWasSelected AndAlso UseControlAs = ControlUse.CustomerRentalCharges, "by Agent", (RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Amount * Multiplier).ToString("f2") & " " & currency)
                ' Customer Confirmation Changes

                dr("Quantity") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Calculation(0).Quantity.ToString
                dr("UnitName") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Calculation(0).UnitName.ToString

                ' Customer Confirmation Changes
                dr("UnitCharge") = IIf(SelectedRateIsNettEvenThoughGrossOptionWasSelected AndAlso UseControlAs = ControlUse.CustomerRentalCharges, "Payment collected", (RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Calculation(0).UnitCharge * Multiplier).ToString("f2") & " " & currency)
                ' Customer Confirmation Changes


            Else
                If UserHasCurrencyOptions Then
                    currency = CurrencyCode
                Else
                    currency = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).CurrencyCode.ToString()
                    Multiplier = 1.0
                End If

                ''rev:mia Oct. 9 2013 - RoadBear and Britz. multiplier should be zero
                If countryOfTravel = "US" Then
                    Multiplier = 1.0
                End If
                If countryOfTravel = "AU" Then
                    Multiplier = Multiplier
                End If
                If countryOfTravel = "NZ" Then
                    Multiplier = Multiplier
                End If

                'System.Diagnostics.Debug.Assert(currency <> "USD", "2.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "GBP", "2.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "AUD", "2. Currency is " & currency)


                dr("Description") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Description.ToString
                dr("Amount") = (RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(i).Amount * Multiplier).ToString("f2") & " " & currency
                dr("Quantity") = " "
                dr("UnitName") = " "
                dr("UnitCharge") = " "

            End If
            dt.Rows.Add(dr)

        End If
    End Sub

    Private Sub LoadFeeData(ByVal Multiplier As Double, ByVal CurrencyCode As String)
        ' Create dataset, load dataset with fee data and bind to repeater
        Dim ds As DataSet
        ds = New DataSet
        Dim dt As DataTable = New DataTable("RentalCharges")
        Dim col As DataColumn = New DataColumn

        col.ColumnName = "Description"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Quantity"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "UnitName"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "UnitCharge"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Amount"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        Dim feeIndex As Integer


        If IsNothing(RS.Errors) Then
            If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees) Then
                For feeIndex = 0 To RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees.Length - 1
                    NewFeeRow(dt, feeIndex, Multiplier, CurrencyCode)
                Next
                ds.Tables.Add(dt)
                BindRepeater(ds)
            End If
        Else
            ' An error occured, display it?
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
            Next
        End If

    End Sub

    Public Sub NewFeeRow(ByRef dt As DataTable, ByVal i As Integer, ByVal Multiplier As Double, ByVal CurrencyCode As String)

        Dim dr As DataRow = dt.NewRow
        Dim chargeDescription As String = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Description.ToLower
        ''rev:mia Oct. 9 2013 - RoadBear and Britz

        Dim countryOfTravel As String = ""
        Try
            countryOfTravel = RS.VehRetResRSCore.VehResSummaries(0).PickUpLocation.Value
            Me.CountryOfTravel = countryOfTravel
        Catch ex As Exception
        End Try

        If chargeDescription = "agentdiscount" Or chargeDescription = "gst" Then
            ' Don't want to add gst, totalgst or agentdiscount to fees grid as an individual item
            If RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Description.ToLower = "gst" Then
                mGST = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Amount.ToString
            End If

        Else
            Dim currency As String

            If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Calculation) Then
                ' There is calculation data
                If UserHasCurrencyOptions Then
                    currency = CurrencyCode
                Else
                    currency = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).CurrencyCode.ToString
                    Multiplier = 1.0
                End If

                If countryOfTravel = "US" Then
                    Multiplier = 1.0
                End If
                If countryOfTravel = "AU" Then
                    Multiplier = Multiplier
                End If
                If countryOfTravel = "NZ" Then
                    Multiplier = Multiplier
                End If

                'System.Diagnostics.Debug.Assert(currency <> "USD", "3.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "GBP", "3.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "AUD", "3. Currency is " & currency)

                dr("Description") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Description.ToString
                dr("Amount") = (RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Amount * Multiplier).ToString("f2") & " " & currency
                dr("Quantity") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Calculation(0).Quantity.ToString
                dr("UnitName") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Calculation(0).UnitName.ToString
                dr("UnitCharge") = (RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Calculation(0).UnitCharge * Multiplier).ToString("f2") & " " & currency
            Else
                If UserHasCurrencyOptions Then
                    currency = CurrencyCode
                Else
                    currency = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).CurrencyCode.ToString
                    Multiplier = 1.0
                End If

                If countryOfTravel = "US" Then
                    Multiplier = 1.0
                End If
                If countryOfTravel = "AU" Then
                    Multiplier = Multiplier
                End If
                If countryOfTravel = "NZ" Then
                    Multiplier = Multiplier
                End If

                'System.Diagnostics.Debug.Assert(currency <> "USD", "4.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "GBP", "4.Currency is " & currency)
                'System.Diagnostics.Debug.Assert(currency <> "AUD", "4. Currency is " & currency)

                dr("Description") = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Description.ToString
                dr("Amount") = (RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees(i).Amount * Multiplier).ToString("f2")
                dr("Quantity") = " "
                dr("UnitName") = " "
                dr("UnitCharge") = " "
            End If

            dt.Rows.Add(dr)
        End If

    End Sub

    Private Sub SetPrices()

        ' Get the following prices for the current booking
        ' Payment Receieved
        ' Agent Discount
        ' Gst
        ' Total Rental Charge
        ' Total Fees
        ' Total Payable


        mCurrency = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(0).CurrencyCode.ToString

        ' PaymentRules(0)= Payment Recieved
        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentInfo.PaymentRules) Then
            mPaymentReceived = RS.VehRetResRSCore.VehReservation.VehSegmentInfo.PaymentRules(0).Amount.ToString
        Else 'no payment recieved
            mPaymentReceived = ".00"
        End If

        Dim rentalRateIndex As Integer

        For rentalRateIndex = 0 To RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges.Length - 1
            ' Loop through the vehicle charges and extract agent discount and gst prices
            ' Gst and agent discount are included in the vehicle charges
            If RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(rentalRateIndex).Description.ToLower = "agentdiscount" Then
                ' Get the agent discount 
                mAgentDiscount = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(rentalRateIndex).Amount.ToString
            End If

            If RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(rentalRateIndex).Description.ToLower = "gst" Then
                ' Get gst
                mGST = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges(rentalRateIndex).Amount.ToString
            End If
        Next

        ' Now calculate fee and charge prices
        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.TotalCharge) Then
            ' Get total rental charge (RateTotalAmount)
            mTotalRentalCharge = RS.VehRetResRSCore.VehReservation.VehSegmentCore.TotalCharge.RateTotalAmount.ToString
            ' Calculate Total Fees (customer charges) (EstimatedTotalAmount-Rental Total Amount)
            mTotalFees = CStr(RS.VehRetResRSCore.VehReservation.VehSegmentCore.TotalCharge.EstimatedTotalAmount - RS.VehRetResRSCore.VehReservation.VehSegmentCore.TotalCharge.RateTotalAmount)
            ' Calculate Total RenatalPayable (TotalRentalCharge- AgentDiscount)
            mTotalPayable = CStr(RS.VehRetResRSCore.VehReservation.VehSegmentCore.TotalCharge.RateTotalAmount - CType(mAgentDiscount, Decimal))

        Else
            mTotalRentalCharge = ""
            mTotalPayable = ""
            mTotalFees = ""
        End If
    End Sub

    'Public Shared Function LogDebugNotes(message As String) As Boolean
    '    Dim timeStamp As String = DateTime.Now.ToString("ddMMyyyy")
    '    Dim sw As System.IO.StreamWriter = System.IO.File.AppendText("c:\temp\logs\b2b\" & "DebugNote_" & timeStamp & ".txt")
    '    Try
    '        sw.WriteLine(DateTime.Now.ToString("HHmmss") + " : " + message)
    '        Return True
    '    Catch ex As Exception
    '        Return False
    '    Finally
    '        sw.Close()
    '    End Try

    'End Function

End Class
