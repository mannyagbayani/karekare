
#Region "Notes"
'--------------------------------------------------------------------------------------
'   Class:          BookingContainer
'   Description:    This is the parent control responsible for managing the individual
'                   user controls which make up the vehicle booking process. This 
'                   control handles the initiation and displaying of the following steps 
'                   Making a booking, Modifying a booking, Cancelling a  booking,              
'                   Displaying a booking summary, Showing the booking confirmation.
'                   It also captures events fired off from these steps.
'--------------------------------------------------------------------------------------

'Karel Jordaan 6July2006 Issue #009
'Added the products page to go in between the makebooking and the booking summary

#End Region

Partial Class BookingContainer
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    Private Property Slot As String

    Private Property SlotId As String

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Events"

    Protected WithEvents ModifyBooking1 As ModifyBooking
    Protected WithEvents StandardConfirmation1 As StandardConfirmation
    Protected WithEvents BookingList1 As BookingList
    Protected WithEvents CancelBooking1 As CancelBooking
    Protected WithEvents MakeBooking1 As MakeBooking
    Protected WithEvents BookingSummary1 As BookingSummary
    'Karel Jordaan 6July2006 Issue #009 added
    'Protected WithEvents MakeBookingProducts1 As MakeBookingProducts

    Public Event BackToAvailability()

#End Region

#Region "Properties"
    ' Possible booking modes/steps
    Public Enum BookingMode
        MakeBooking = 0
        Modify = 1
        ModifyQuoteToBooking = 2
        BookingList = 3
        Cancel = 4
        NewBookingSummary = 5
        ExistingBookingSummary = 6
        StdConfirmation = 7
        'Karel Jordaan 6July2006 Issue #009 added
        AddProducts = 8
    End Enum

    ' Used to store what booking step the user is currently at
    Private Property CurrentBookingMode() As BookingMode
        Get
            Return CType(ViewState("CurrentBookingMode"), BookingMode)
        End Get
        Set(ByVal Value As BookingMode)
            ViewState("CurrentBookingMode") = Value
        End Set
    End Property

    ' The type of booking i.e.Quote, book, hold etc. This is an OTA type
    Private Property CurrentBookingType() As TransactionActionType
        Get
            Return CType(ViewState("CurrentBookingType"), TransactionActionType)
        End Get
        Set(ByVal Value As TransactionActionType)
            ViewState("CurrentBookingType") = Value
        End Set
    End Property

    Private Property BookingId() As String
        Get
            Return CStr(ViewState("BookingId"))
        End Get
        Set(ByVal Value As String)
            ViewState("BookingId") = Value
        End Set
    End Property

    'Customer Confirmation Changes
    Public ReadOnly Property GrossModeSelected() As Boolean
        Get
            If Session("GrossModeSelected") IsNot Nothing Then
                Return CBool(Session("GrossModeSelected"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property NettModeSelected() As Boolean
        Get
            If Session("NettModeSelected") IsNot Nothing Then
                Return CBool(Session("NettModeSelected"))
            Else
                Return False
            End If
        End Get
    End Property
    'Customer Confirmation Changes

#End Region

    Public ReadOnly Property GrossNettDefault() As String
        Get
            If Session("GrossNettDefault") IsNot Nothing Then
                Return CStr(Session("GrossNettDefault"))
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property ShowCustomerConfirmationByDefault() As Boolean
        Get
            If GrossNettDefault = "G" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         InitBookingMode
    '   Description:    Initiate the booking step. Load and display appropriote control
    '                   according to the booking mode
    '--------------------------------------------------------------------------------------
    ' Modified by Nimesh on 17th Feb 2015 for accepting Slot and Slot Id
    Public Sub InitBookingMode(ByVal mode As BookingContainer.BookingMode, ByVal ID As String, ByVal bookingtype As TransactionActionType, Optional ByVal CustomerConfirmationMode As Boolean = False)
        ' Determine whether this is a cancellation, new booking etc
        Me.CurrentBookingMode = mode
        Me.BookingId = ID
        Me.CurrentBookingType = bookingtype
        ' Load correct user control based on booking mode
        LoadBookingControl(CustomerConfirmationMode)
        ' and display correct user control based on booking mode
        DisplayControlByMode()
    End Sub

    Public Sub InitBookingModeV2(ByVal mode As BookingContainer.BookingMode, ByVal ID As String, ByVal bookingtype As TransactionActionType, Optional ByVal CustomerConfirmationMode As Boolean = False, Optional ByVal Slot As String = "", Optional ByVal SlotId As String = "")
        ' Determine whether this is a cancellation, new booking etc
        Me.CurrentBookingMode = mode
        Me.BookingId = ID
        Me.CurrentBookingType = bookingtype
        Me.Slot = Slot
        Me.SlotId = SlotId
        ' Load correct user control based on booking mode
        LoadBookingControl(CustomerConfirmationMode)
        ' and display correct user control based on booking mode
        DisplayControlByMode()
    End Sub

    'Karel Jordaan 6July2006 Issue #009 created
    Public Sub InitAddProductsMode(ByVal mode As BookingContainer.BookingMode, ByVal ID As String, ByVal bookingtype As TransactionActionType)
        ' Determine whether this is a cancellation, new booking etc
        Me.CurrentBookingMode = mode
        Me.BookingId = ID
        Me.CurrentBookingType = bookingtype
        ' Load correct user control based on booking mode
        LoadNewAddProducts()
        ' and display correct user control based on booking mode
        DisplayControlByMode()
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         LoadBookingControl
    '   Description:    Load the correct booking control based on the current booking mode/
    '                   step
    '--------------------------------------------------------------------------------------
    Private Sub LoadBookingControl(Optional ByVal CustomerConfirmationMode As Boolean = False)
        Select Case Me.CurrentBookingMode
            Case BookingMode.Modify
                LoadModify()

            Case BookingMode.ModifyQuoteToBooking
                LoadModifyQuotetoBooking(CustomerConfirmationMode)

            Case BookingMode.MakeBooking
                LoadMakeBooking(CustomerConfirmationMode)

            Case BookingMode.BookingList
                LoadBookingList()

            Case BookingMode.Cancel
                LoadCancel()

            Case BookingMode.StdConfirmation
                LoadConfirmation(CustomerConfirmationMode)

            Case BookingMode.ExistingBookingSummary
                LoadExistingBookingSummary(CustomerConfirmationMode)

            Case BookingMode.NewBookingSummary
                LoadNewBookingSummary(CustomerConfirmationMode)

                'Karel Jordaan 6July2006 Issue #009 added
            Case BookingMode.AddProducts
                LoadNewAddProducts()

        End Select
    End Sub

    '--------------------------------------------------------------------------------------
    '   Methods:         LoadConfirmation, LoadModify, LoadModifyQuotetoBooking, LoadMakeBooking
    '                   ,LoadBookingList, LoadExistingBookingSummary, LoadNewBookingSummary
    '   Description:    Initalise the booking control 
    '--------------------------------------------------------------------------------------

    Private Sub LoadConfirmation(Optional ByVal CustomerConfirmationMode As Boolean = False)
        StandardConfirmation1.InitStandardConfirmation(bookingId:=Me.BookingId, CustomerConfirmationMode:=CustomerConfirmationMode)
    End Sub
    Private Sub LoadModify()
        ModifyBooking1.InitModifyBooking(Me.BookingId, Me.CurrentBookingType)
    End Sub

    Private Sub LoadModifyQuotetoBooking(ByVal CustomerConfirmationMode As Boolean)
        ModifyBooking1.InitModifyBookingFromQuote(Me.BookingId, Me.CurrentBookingType, CustomerConfirmationMode, Slot, SlotId)
    End Sub

    Private Sub LoadMakeBooking(ByVal CustomerConfirmationMode As Boolean)
        MakeBooking1.InitMakeBooking(Me.BookingId, Me.CurrentBookingType, CustomerConfirmationMode)
    End Sub

    Private Sub LoadBookingList()
        BookingList1.InitBookingList()
    End Sub

    Private Sub LoadCancel()
        CancelBooking1.InitCancelBooking(Me.BookingId)
    End Sub

    Private Sub LoadExistingBookingSummary(ByVal CustomerConfirmationMode As Boolean)
        BookingSummary1.InitSummary(BookingSummary.TypeOfSummary.ExsitingBooking, Me.BookingId, CustomerConfirmationMode)
    End Sub

    Private Sub LoadNewBookingSummary(ByVal CustomerConfirmationMode As Boolean)
        BookingSummary1.InitSummary(BookingSummary.TypeOfSummary.NewBooking, Me.BookingId, CustomerConfirmationMode)
    End Sub

    'Karel Jordaan 6July2006 Issue #009 added
    Private Sub LoadNewAddProducts()
        ' MakeBookingProducts1.InitAddProducts(Me.BookingId, Me.CurrentBookingType)
    End Sub

    'Karel Jordaan 6July2006 Issue #009 changed
    Private Sub DisplayByMode(ByVal vBookingList1 As Boolean, ByVal vMakeBooking1 As Boolean, ByVal vModifyBooking1 As Boolean, _
     ByVal vStandardConfirmation1 As Boolean, ByVal vCancelBooking1 As Boolean, ByVal vBookingSummary1 As Boolean, ByVal vAddProduct1 As Boolean)

        BookingList1.Visible = vBookingList1
        MakeBooking1.Visible = vMakeBooking1
        ModifyBooking1.Visible = vModifyBooking1
        StandardConfirmation1.Visible = vStandardConfirmation1
        CancelBooking1.Visible = vCancelBooking1
        BookingSummary1.Visible = vBookingSummary1

        'Karel Jordaan 6July2006 Issue #009 added
        ' MakeBookingProducts1.Visible = vAddProduct1


    End Sub

    Private Sub DisplayControlByMode()
        ' Determine what controls to display

        Select Case Me.CurrentBookingMode
            Case BookingMode.BookingList
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(True, False, False, False, False, False)
                DisplayByMode(True, False, False, False, False, False, False)

            Case BookingMode.StdConfirmation
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(False, False, False, True, False, False)
                DisplayByMode(False, False, False, True, False, False, False)

            Case BookingMode.Modify
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(False, False, True, False, False, False)
                DisplayByMode(False, False, True, False, False, False, False)

            Case BookingMode.ModifyQuoteToBooking
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(False, False, True, False, False, False)
                DisplayByMode(False, False, True, False, False, False, False)

            Case BookingMode.MakeBooking
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(False, True, False, False, False, False)
                DisplayByMode(False, True, False, False, False, False, False)

            Case BookingMode.Cancel
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(False, False, False, False, True, False)
                DisplayByMode(False, False, False, False, True, False, False)

            Case BookingMode.ExistingBookingSummary
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(False, False, False, False, False, True)
                DisplayByMode(False, False, False, False, False, True, False)

            Case BookingMode.NewBookingSummary
                'Karel Jordaan 6July2006 Issue #009 changed
                'DisplayByMode(False, False, False, False, False, True)
                DisplayByMode(False, False, False, False, False, True, False)

                'Karel Jordaan 6July2006 Issue #009 added
            Case BookingMode.AddProducts
                DisplayByMode(False, False, False, False, False, False, True)
        End Select
    End Sub

#Region "BookingList Events"
    '''-----------------------------------------------------------------------------
    ''' <summary>
    ''' These methods react to events fired from the BookingListControl
    ''' </summary>
    '''-----------------------------------------------------------------------------
    Private Sub BookingList1_BookBooking(ByVal id As String, ByVal modifyType As TransactionActionType) Handles BookingList1.BookBooking
        ' Show the modify booking control
        ' NB: Progressing to a confirmed booking from a quote is a modify booking
        If modifyType = TransactionActionType.Quote Then
            ' Want to bypass date alteration when doing modify
            InitBookingMode(BookingMode.ModifyQuoteToBooking, id, TransactionActionType.Book, ShowCustomerConfirmationByDefault)
        Else
            InitBookingMode(BookingMode.Modify, id, TransactionActionType.Book, ShowCustomerConfirmationByDefault)
        End If
    End Sub

    Private Sub BookingList1_BookBookingV2(ByVal id As String, ByVal modifyType As TransactionActionType, Optional ByVal slot As String = "", Optional ByVal slotId As String = "") Handles BookingList1.BookBookingV2
        ' Show the modify booking control
        ' NB: Progressing to a confirmed booking from a quote is a modify booking
        If modifyType = TransactionActionType.Quote Then
            ' Want to bypass date alteration when doing modify
            InitBookingModeV2(BookingMode.ModifyQuoteToBooking, id, TransactionActionType.Book, ShowCustomerConfirmationByDefault, slot, slotId)
        Else
            InitBookingModeV2(BookingMode.Modify, id, TransactionActionType.Book, ShowCustomerConfirmationByDefault, slot, slotId)
        End If
    End Sub

    Private Sub BookingList1_CancelBooking(ByVal id As String) Handles BookingList1.CancelBooking
        InitBookingMode(BookingMode.Cancel, id, TransactionActionType.Cancel, ShowCustomerConfirmationByDefault)
    End Sub

    Private Sub BookingList1_ConfirmBooking(ByVal id As String, ByVal modifyType As TransactionActionType) Handles BookingList1.ConfirmBooking
        InitBookingMode(BookingMode.ExistingBookingSummary, id, modifyType, ShowCustomerConfirmationByDefault)
    End Sub

    Private Sub BookingList1_ModifyBooking(ByVal id As String, ByVal modifyType As TransactionActionType) Handles BookingList1.ModifyBooking
        InitBookingMode(BookingMode.Modify, id, modifyType, ShowCustomerConfirmationByDefault)
    End Sub
    Private Sub BookingList1_DeleteBooking(ByVal id As String) Handles BookingList1.DeleteBooking
        InitBookingMode(BookingMode.Cancel, id, TransactionActionType.Cancel, ShowCustomerConfirmationByDefault)
    End Sub

    Private Sub BookingList1_ViewConfirmation(ByVal Id As String, ByVal IsCustomer As Boolean) Handles BookingList1.ViewConfirmation
        InitBookingMode(BookingMode.StdConfirmation, Id, TransactionActionType.Book, IsCustomer)
    End Sub

#End Region

#Region "Make Booking Events"
    '''-----------------------------------------------------------------------------
    ''' <summary>
    '''  These methods react to events fired from the Make Booking Control
    ''' </summary>
    '''-----------------------------------------------------------------------------
    Private Sub MakeBooking1_Back() Handles MakeBooking1.Back
        InitBookingMode(BookingMode.BookingList, "", TransactionActionType.Ignore)
    End Sub

    Private Sub MakeBooking1_BackToAvailability() Handles MakeBooking1.BackToAvailability
        ' Raise an event to the page level 
        RaiseEvent BackToAvailability()
    End Sub

    Private Sub MakeBooking1_Continue(ByVal newId As String) Handles MakeBooking1.[Continue]
        Me.BookingId = newId
        InitBookingMode(BookingMode.StdConfirmation, Me.BookingId, TransactionActionType.Book, GrossModeSelected)
    End Sub
#End Region

#Region "StandardConfirmation Events"
    '''-----------------------------------------------------------------------------
    ''' <summary>
    '''  These methods react to events fired from the StandardConfirmation Control
    ''' </summary>
    '''-----------------------------------------------------------------------------
    Private Sub StandardConfirmation1_Continue() Handles StandardConfirmation1.[Continue]
        ' Show booking list
        InitBookingMode(BookingMode.BookingList, "", TransactionActionType.Book)
    End Sub

#End Region

#Region "ModifyBooking Events"
    '''-----------------------------------------------------------------------------
    ''' <summary>
    '''  These methods react to events fired from the ModifyBooking Control
    ''' </summary>
    '''-----------------------------------------------------------------------------
    Private Sub ModifyBooking1_BookingModified(ByVal CustomerConfirmationMode As Boolean) Handles ModifyBooking1.BookingModified
        InitBookingMode(BookingMode.StdConfirmation, Me.BookingId, Me.CurrentBookingType, CustomerConfirmationMode)
    End Sub

    Private Sub ModifyBooking1_CancelClicked() Handles ModifyBooking1.CancelClicked
        InitBookingMode(BookingMode.BookingList, "", TransactionActionType.Ignore)
    End Sub

#End Region

#Region "CancelBooking Events"
    '''-----------------------------------------------------------------------------
    ''' <summary>
    '''  These methods react to events fired from the CancelBooking Control
    ''' </summary>
    '''-----------------------------------------------------------------------------
    Private Sub CancelBooking1_Cancel() Handles CancelBooking1.Cancel
        ' Return to booking list
        InitBookingMode(BookingMode.BookingList, "", TransactionActionType.Cancel)
    End Sub

    Private Sub CancelBooking1_Confirm() Handles CancelBooking1.Confirm
        ' Cancellation commited, show standard confirmation
        InitBookingMode(BookingMode.StdConfirmation, Me.BookingId, TransactionActionType.Cancel)
    End Sub

#End Region

#Region "BookingSummary Events"
    '''-----------------------------------------------------------------------------
    ''' <summary>
    '''  These methods react to events fired from the CancelBooking Control
    ''' </summary>
    '''-----------------------------------------------------------------------------
    Private Sub BookingSummary1_Back() Handles BookingSummary1.Back
        InitBookingMode(BookingMode.BookingList, "", TransactionActionType.Ignore)
    End Sub
    Private Sub BookingSummary1_Continue(ByVal Id As String, ByVal CustomerConfirmationMode As Boolean) Handles BookingSummary1.[Continue]
        ModifyBooking1.InitModifyBooking(Id, Me.CurrentBookingType)
        ModifyBooking1.ModifyBooking(Me.CurrentBookingType)
        InitBookingMode(BookingMode.StdConfirmation, Me.BookingId, Me.CurrentBookingType, CustomerConfirmationMode)
    End Sub
#End Region

    'Karel Jordaan 6July2006 Issue #009 changed
#Region "AddProducts Events"
    '''-----------------------------------------------------------------------------
    ''' <summary>
    '''  These methods react to events fired from the MakeBookingProducts Control
    ''' </summary>
    '''-----------------------------------------------------------------------------
    'Private Sub AddProducts1_Back() Handles AddProducts1.Back
    '    InitBookingMode(BookingMode.BookingList, "", TransactionActionType.Ignore)
    'End Sub

    'Private Sub MakeBookingProducts1_Continue(ByVal Id As String) Handles MakeBookingProducts1.Continue
    '    'ModifyBooking1.InitModifyBooking(Id, Me.CurrentBookingType)
    '    'ModifyBooking1.ModifyBooking(Me.CurrentBookingType)
    '    InitAddProductsMode(BookingMode.StdConfirmation, Me.BookingId, Me.CurrentBookingType)
    'End Sub
#End Region


#Region "rev:mia 25March2015 B2B Auto generated Aurora Booking Confirmation"
    Public ReadOnly Property GetInstanceOfStandardConfirmation As StandardConfirmation
        Get
            Return StandardConfirmation1
        End Get
    End Property
#End Region
End Class
