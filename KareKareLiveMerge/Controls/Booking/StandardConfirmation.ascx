<%@ Register TagPrefix="uc1" TagName="FeesAndCharges" Src="../FeesAndCharges/FeesAndCharges.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CompanyAddress" Src="../CompanyAddress/CompanyAddress.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="StandardConfirmation.ascx.vb" Inherits="THLAuroraWebInterface.StandardConfirmation" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script language="JavaScript" type="text/javascript">
<!--
    function Print() {
        window.print()
    }
    //-->
</script>
<asp:label cssclass="warning" id="lblLoadError" runat="server" Font-Bold="true" Font-Size="Large"></asp:label>
<TABLE class="normal" id="tblAll" runat="server" cellSpacing="0" cellPadding="2" width="746"
	border="0">
	<TBODY>
		<TR>
			<TD class="warning">
				<TABLE id="Table3" cellSpacing="0" cellPadding="2" width="746" border="0">
					<TR>
						<TD align="right">
							<uc1:CompanyAddress id="CompanyAddress1" runat="server"></uc1:CompanyAddress></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD>
				<TABLE id="tblAgentInfo" cellSpacing="0" cellPadding="2" width="746" border="0">
					<TR>
						<TD class="StdConfirmationHeading" style="WIDTH: 192px; HEIGHT: 23px">Vehicle 
							Status</TD>
						<TD class="normal" style="WIDTH: 314px"><asp:label id="lblStatus" runat="server"></asp:label></TD>
						<TD class="StdConfirmationHeading" style="HEIGHT: 23px">Date</TD>
						<TD class="normal" align="right"><asp:label id="lblDate" runat="server"></asp:label></TD>
					</TR>
					<TR>
						<TD class="StdConfirmationHeading" style="WIDTH: 192px">Our Ref</TD>
						<TD class="normal" style="WIDTH: 314px"><asp:label id="lblRef" runat="server"></asp:label></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD class="normal" colSpan="4"></TD>
					</TR>
					<TR>
						<TD class="StdConfirmationHeading" style="WIDTH: 192px; HEIGHT: 7px">Agent Name</TD>
						<TD class="normal" style="WIDTH: 314px; HEIGHT: 7px"><asp:label id="lblAgentName" runat="server"></asp:label></TD>
						<TD class="StdConfirmationHeading" style="HEIGHT: 7px">Agent Ref</TD>
						<TD class="normal" style="HEIGHT: 7px" align="right"><asp:label id="lblAgentRef" runat="server"></asp:label></TD>
					</TR>
					<TR>
						<TD class="StdConfirmationHeading" style="WIDTH: 192px">Customer</TD>
						<TD class="normal" style="WIDTH: 314px"><asp:label id="lblCustomerName" runat="server"></asp:label></TD>
						<TD></TD>
						<TD></TD>
					</TR>
				</TABLE>
				<HR width="100%" color="#000000" SIZE="2">
			</TD>
		</TR>
		<TR>
			<TD>&nbsp;
				<asp:placeholder id="PlaceHolder1" runat="server"></asp:placeholder>
				<div id="divHeader" runat="server"></div>
				<HR width="100%" color="#000000" SIZE="2">
			</TD>
		</TR>
		<TR>
			<TD>
				<TABLE id="tblBookingDetails" cellSpacing="0" cellPadding="2" width="745" border="0">
					<TR>
						<TD class="StdConfirmationHeading" width="45%">Booking Details -
							<asp:label cssclass="normal" id="lblVehicle" runat="server"></asp:label></TD>
						<TD class="normal" width="42%">&nbsp;</TD>
						<TD width="12%"></TD>
					</TR>
					<TR>
						<TD class="StdConfirmationHeading" vAlign="top">Check Out</TD>
						<TD class="StdConfirmationHeading" align="left" vAlign="top">Address</TD>
						<TD class="StdConfirmationHeading" vAlign="top">Phone</TD>
					</TR>
					<TR>
						<TD class="normal" vAlign="top"><asp:label id="lblCheckout" runat="server"></asp:label></TD>
						<TD class="normal" vAlign="top"><asp:label id="lblCOAddress" runat="server"></asp:label></TD>
						<TD class="normal" vAlign="top"><asp:label id="lblCOPhone" runat="server"></asp:label></TD>
					</TR>
					<TR>
						<TD class="StdConfirmationHeading" vAlign="top">Check In</TD>
						<TD class="StdConfirmationHeading" width="35%" vAlign="top">Address</TD>
						<TD class="StdConfirmationHeading" width="25%" vAlign="top">Phone</TD>
					</TR>
					<TR>
						<TD class="normal" vAlign="top"><asp:label id="lblCheckin" runat="server"></asp:label></TD>
						<TD class="normal" vAlign="top"><asp:label id="lblCIAddress" runat="server"></asp:label></TD>
						<TD class="normal" vAlign="top"><asp:label id="lblCIPhone" runat="server"></asp:label></TD>
					</TR>
					<TR>
						<TD class="StdConfirmationHeading" vAlign="top">Operating Hours&nbsp;
							<asp:label cssclass="normal" id="lblOpHours" runat="server"></asp:label></TD>
						<TD class="normal"></TD>
						<TD></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR id="AgentPaymentRow" runat="server" > 
			<TD>
				<HR width="100%" color="#000000" SIZE="2" >
				<asp:label id="lblCustomertoPay1" runat="server" CssClass="stdconfirmationheading"></asp:label>
				<asp:label id="lblFlexRate" runat="server" CssClass="stdconfirmationheading">Flex Number</asp:label>
				<asp:label id="lblFlexRateValue" runat="server" CssClass="normal"></asp:label>
				<uc1:feesandcharges id="FeesAndCharges2" runat="server"></uc1:feesandcharges></TD>
		</TR>
		<TR id="customerPaymentRow" runat="server">
			<TD>
				<HR width="100%" color="#000000" SIZE="2">
                <!--rev:mia April 1 2014 Payable to supplier messages-->
				<asp:label id="lblCustomertoPay2" runat="server" CssClass="stdconfirmationheading"></asp:label>
				<P><uc1:feesandcharges id="FeesAndCharges1" runat="server"></uc1:feesandcharges></P>
			</TD>
		</TR>
		<TR>
			<TD>
				<HR width="100%" color="#000000" SIZE="2">
				&nbsp;</TD>
		</TR>
		<TR>
			<TD><asp:placeholder id="PlaceHolder2" runat="server"></asp:placeholder>
				<div id="divFooter" runat="server"></div>
			</TD>
		</TR>
		
	</TBODY>
</TABLE>

<%--rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation--%>
<table id="ConfirnmationButtons" width="746">
        <tr align="right">
			<td align="right">
                <input runat="server" id="btnprint" type="button" value="Print" onclick="print()" style="width: 79px; height: 24px"	size="20"/>
			    <asp:button id="btnprintfriendly" runat="server" text="Printer Friendly Version" width="154px" visible="false"></asp:button>
			    <asp:button id="btnprintfriendlycustomerversion" runat="server" text="Printer Friendly Version" width="154px" visible="false"></asp:button>
			    <asp:button id="btnviewagentconfirmation" runat="server" visible="false" text="View Agent Confirmation" />
			    <asp:button id="btnviewcustomerconfirmation" runat="server" visible="false" text="View Customer Confirmation" />
			    <asp:button id="btncontinue" runat="server" text="Finish" width="79px"></asp:button>
            </td>
		</tr>

</table>
