<%@ Register TagPrefix="uc1" TagName="FeesAndCharges" Src="../FeesAndCharges/FeesAndCharges.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FeesAndChargesNewBooking" Src="../FeesAndCharges/FeesAndChargesNewBooking.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="BookingSummary.ascx.vb" Inherits="THLAuroraWebInterface.BookingSummary" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE class="normal" id="tblOuterSummary" runat="server" cellSpacing="0" cellPadding="0"
	width="746" border="0" style="WIDTH: 746px; HEIGHT: 306px">
	<TR class="RowHeading">
		<TD style="HEIGHT: 17px" class="RowHeading">Rental Summary</TD>
	</TR>
	<TR>
		<TD>
			<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD style="WIDTH: 127px"></TD>
					<TD></TD>
					<TD style="WIDTH: 165px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="StdConfirmationHeading" style="WIDTH: 127px">Vendor</TD>
					<TD class="normal"><asp:label id="lblVendor" runat="server"></asp:label></TD>
					<TD style="WIDTH: 165px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="StdConfirmationHeading" style="WIDTH: 127px">
						Vehicle
					</TD>
					<TD class="normal"><asp:label id="lblVehMake" runat="server"></asp:label></TD>
					<TD class="StdConfirmationHeading" style="WIDTH: 165px">Vehicle Category&nbsp; 
						&amp; &nbsp;Size</TD>
					<TD class="normal"><asp:label id="lblVehSize" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="StdConfirmationHeading" style="WIDTH: 127px">PickUp&nbsp;&nbsp;Date/Time</TD>
					<TD class="normal"><asp:label id="lblPickupTime" runat="server"></asp:label></TD>
					<TD class="StdConfirmationHeading" style="WIDTH: 165px">Return Date/Time</TD>
					<TD class="normal"><asp:label id="lblReturnTime" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="StdConfirmationHeading" style="WIDTH: 127px">PickUp Location</TD>
					<TD class="normal"><asp:label id="lblPickupLocation" runat="server"></asp:label></TD>
					<TD class="StdConfirmationHeading" style="WIDTH: 165px">Return Location</TD>
					<TD class="normal"><asp:label id="lblReturnLocation" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 127px"></TD>
					<TD></TD>
					<TD style="WIDTH: 165px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 127px"></TD>
					<TD></TD>
					<TD style="WIDTH: 165px"></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR class="RowHeading">
		<TD class="RowHeading" style="HEIGHT: 12px">Charge Details</TD>
	</TR>
	<TR>
		<TD style="HEIGHT: 149px">
			<P>
				<uc1:FeesAndCharges id="FeesAndCharges2" runat="server"></uc1:FeesAndCharges></P>
			<P>
				<uc1:FeesAndCharges id="FeesAndCharges1" runat="server"></uc1:FeesAndCharges></P>
			<P>
				<uc1:FeesAndChargesNewBooking id="FeesAndChargesNewBooking1" runat="server"></uc1:FeesAndChargesNewBooking>
			</P>
		</TD>
	</TR>
	<TR>
		<TD align="right"><asp:button id="btnContinueNew" runat="server" Width="75px" Text="Continue"></asp:button><asp:button id="btnBack" runat="server" Width="75px" Text="Back" CausesValidation="False"></asp:button></TD>
	</TR>
</TABLE>
<asp:label id="lblLoadError" runat="server" CssClass="warning"></asp:label>
