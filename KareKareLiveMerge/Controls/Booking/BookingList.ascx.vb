'NOTE


'3June2006 Karel Jordaan line 250
'Added the DepartureDate column to the page


'8June2006 Karel Jordaan line 350, 390, 421, 497
'Added the ExpireDate column to the page

'26June2006 Karel Jordaan
'Changed the base from System.Web.UI.UserControl
'to THL_WebUserControlBase 
'update the global propertie for access from other ascx that does not call the database


Imports System.Xml.Serialization
Imports System.IO
Imports System.Xml
Imports System.Text
Imports AuroraOTAProxy
Imports THLAuroraWebInterface.WebServices
Imports System.Xml.Linq
Imports System.Linq
'Imports THLRentals.OTA.Utils

'--------------------------------------------------------------------------------------
'   Class:          BookingList
'   Description:    This control is responsible for displaying the current logged in users 
'                   bookings. Bookings can have a status of Confirm, Provisional or Quote.
'                   Events are fired of which are picked up by the bookingContainer and 
'                   include Modiying, cancelling, deleting or viewing the confirmation
'                   of a  quote, confirmed or provisional booking. A booking note can also
'                   be added to each of the above
'--------------------------------------------------------------------------------------

Partial Class BookingList

    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

    Protected Proxy As New AuroraOTAProxy.Aurora_OTA

#Region " Web Form Designer Generated Code "

    Public Property slotsPanel As String

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSearch As System.Web.UI.WebControls.Button
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public Event BookClicked(ByVal Id As String)
    Public Event BookBooking(ByVal Id As String, ByVal modifyType As TransactionActionType)
    Public Event BookBookingV2(ByVal Id As String, ByVal modifyType As TransactionActionType, ByVal slot As String, ByVal slotId As String)
    Public Event ModifyBooking(ByVal Id As String, ByVal modifyType As TransactionActionType)
    Public Event CancelBooking(ByVal Id As String)
    Public Event DeleteBooking(ByVal Id As String)
    Public Event ConfirmBooking(ByVal Id As String, ByVal modifyType As TransactionActionType)
    Public Event ViewConfirmation(ByVal Id As String, ByVal IsCustomer As Boolean)

    Dim RS As New OTA_VehRetResRS ' The Retrieve Vehicle Reservations Response object
    Dim dsView As DataView

    ' The number of agents this user is responsible for
    Private Property AgentCodeCount() As Integer
        Get
            Return CInt(ViewState("AgentCodeCount"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("AgentCodeCount") = Value
        End Set
    End Property

    ' The dataset used to store all vehicle reservation data
    Private Property BookingDataset() As DataSet
        Get
            Return CType(Session("BookingDataset"), DataSet)
        End Get
        Set(ByVal Value As DataSet)
            Session("BookingDataset") = Value
        End Set
    End Property

    ' Stores the Last Sort order. Asc or Desc
    Private Property LastSortOrder() As String
        Get
            Return CStr(ViewState("LastSortOrder"))
        End Get
        Set(ByVal Value As String)
            ViewState("LastSortOrder") = Value
        End Set
    End Property

    'Private nDisableAll As String
    'Public Property DisableAll() As String
    '    Get
    '        Return CStr(IIf(nDisableAll.Equals(String.Empty), "True", nDisableAll))
    '    End Get
    '    Set(ByVal value As String)
    '        nDisableAll = value
    '    End Set
    'End Property
    Public Property DisableAll() As Boolean
        Get
            Return CBool(ViewState("DisableAll"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("DisableAll") = Value
        End Set
    End Property

    Private nPickUpDayMonthStr As String
    Public Property PickUpDayMonthStr() As String
        Get
            Return nPickUpDayMonthStr
        End Get
        Set(ByVal value As String)
            nPickUpDayMonthStr = value
        End Set
    End Property

    Private nPickUpLocationStr As String
    Public Property PickUpLocationStr() As String
        Get
            Return nPickUpLocationStr
        End Get
        Set(ByVal value As String)
            nPickUpLocationStr = value
        End Set
    End Property

    'Private nSlotsPanel As String
    'Public Property SlotsPanel() As String
    '    Get
    '        Return nSlotsPanel
    '    End Get
    '    Set(ByVal value As String)
    '        nSlotsPanel = value
    '    End Set
    'End Property

    ' The last sort Criteria. (Set when user clicks on sortable datagrid column)
    Private Property LastSortColumn() As String
        Get
            Return CStr(ViewState("LastSortColumn"))
        End Get
        Set(ByVal Value As String)
            ViewState("LastSortColumn") = Value
        End Set
    End Property

    ' Was used for dataset sorting, useful for future?
    Private Property LastFilter() As String
        Get
            Return CStr(ViewState("LastFilter"))
        End Get
        Set(ByVal Value As String)
            ViewState("LastFilter") = Value
        End Set
    End Property

    Public ReadOnly Property UserCanSeeCustomerConfirmations() As Boolean
        Get
            If Session("UserCanSeeCustomerConfirmations") IsNot Nothing Then
                Return CBool(Session("UserCanSeeCustomerConfirmations"))
            Else
                Return False
            End If
        End Get
    End Property


#Region "Currency Changes"
    Public ReadOnly Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property CurrencyOptionsData() As ArrayList
        Get
            If Session("CurrencyOptionsData") IsNot Nothing Then
                Return CType(Session("CurrencyOptionsData"), ArrayList)
            Else
                Return New ArrayList
            End If
        End Get
    End Property

    Public ReadOnly Property ExchangeRatesLookupData() As ExchangeRatesTypeAgent()
        Get
            If Session("ExchangeRatesLookupData") IsNot Nothing Then
                Return CType(Session("ExchangeRatesLookupData"), ExchangeRatesTypeAgent())
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property MRU_CurrencyMultiplier() As Double
        Get
            If Session("MRU_CurrencyMultiplier") IsNot Nothing Then
                Return CDbl(Session("MRU_CurrencyMultiplier"))
            Else
                Return 0.0
            End If
        End Get
        Set(ByVal value As Double)
            Session("MRU_CurrencyMultiplier") = value
        End Set
    End Property

    Public Property MRU_CurrencyCode() As String
        Get
            Return CStr(Session("MRU_CurrencyCode"))
        End Get
        Set(ByVal value As String)
            Session("MRU_CurrencyCode") = value
        End Set
    End Property
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblLoadError.Text = ""
        'If Not Page.IsPostBack Then
        '    slotsDetail.Visible = False
        'End If
        'dgAgentData.Columns(4).ValueType=TypeOf(DateTime)
        'btnContinue.Attributes.Add("onclick", "validateSlot()")
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         InitBookingList
    '   Description:    Used to load booking list for first time. Gets all vehicle reservations
    '                   based on the logged in user, Loads agent dropdown list in search control
    '                   and Loads the datagrid resonsible for displaying all bookings.
    '--------------------------------------------------------------------------------------
    Public Sub InitBookingList()

        Try
            ' Get all vehicle bookings for the current user
            'slotsDetail.Visible = False
            GetVehicleReservationData()

            ' Check to see there have been no errors sent back in the response
            If IsNothing(RS.Errors) Then
                ' Load agent dropdown list in search table
                LoadAgentList()
                ' Load dataset and bind vehicle datagrid
                LoadBookingDataGrid()
            Else
                ' An error occured, display it.
                Dim errorIndex As Integer
                ' Loop through all errors and display
                For errorIndex = 0 To RS.Errors.Length - 1
                    lblLoadError.Text &= RS.Errors(errorIndex).Tag & "<br>"
                    If RS.Errors(errorIndex).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                        Session("POS") = Nothing
                        Response.Redirect("AccessDenied.aspx")
                    End If
                Next
            End If

        Catch ex As Exception
            lblLoadError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         GetVehicleReservationData
    '   Description:    This method has 2 purposes.
    '                   1-Retrieve all vehicle bookings based on the logged in user
    '                   2-Retrieve all vehicle bookings based on the logged in user and 
    '                   the specified search parameters 
    '--------------------------------------------------------------------------------------
    'Private Sub GetVehicleReservationData(Optional ByVal surname As String = "", _
    '                                     Optional ByVal bookingId As String = "", _
    '                                     Optional ByVal agent As String = "", _
    '                                     Optional ByVal agentRef As String = "")


    Private Sub GetVehicleReservationData(Optional ByVal surname As String = "", _
                                         Optional ByVal bookingId As String = "", _
                                         Optional ByVal agent As String = "", _
                                         Optional ByVal agentRef As String = "", _
                                         Optional ByVal sDepartureDate As String = "", _
                                         Optional ByVal sStatus As String = "" _
                                         )

        Dim RQ As New OTA_VehRetResRQ ' The request object which will store all data to be 
        ' passed to the backend Aurora system via web service

        RQ.VehRetResRQCore = New VehicleRetrieveResRQCoreType
        RQ.EchoToken = CStr(Session("SessionId"))
        ' Pass user data stored in POS
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}

        '---------The following is used for the search functionality--------------------

        'Dim VehRetCoreType As New VehicleRetrieveResRQCoreType

        ' Only enter search parameters in the RQ if the have been passed to this method

        Dim uniqueid As New UniqueID_Type
        If bookingId <> "" Then
            uniqueid.ID = bookingId
        Else
            uniqueid.ID = "NA"
        End If

        uniqueid.Type = "NA" ' Not used but required as part of the request
        RQ.VehRetResRQCore.UniqueID = uniqueid

        Dim persontype As New PersonNameType
        If surname <> "" Then
            persontype.Surname = surname
        Else
            persontype.Surname = "NA"
        End If
        RQ.VehRetResRQCore.PersonName = persontype

        Dim tpa As New VehicleResAdditionalInfoType
        If agent <> "" Then
            tpa.Agent = agent
        Else
            tpa.Agent = "NA"
        End If

        If agentRef <> "" Then
            tpa.AgentRef = agentRef
        Else
            tpa.AgentRef = "NA"
        End If
        tpa.Package = "NA" ' Not used but required as part of the request


        ''//rev:mia April 16 2012
        If (Not String.IsNullOrEmpty(sStatus)) Then
            tpa.Status = sStatus
        Else
            tpa.Status = "NA" ' Not used but required as part of the request
        End If

        ''Response.Write(tpa.Status)

        If sDepartureDate <> "" Then
            tpa.DepartureDate = sDepartureDate
        Else
            tpa.DepartureDate = "NA"
        End If
        RQ.VehRetResRQCore.TPA_Extensions = tpa


        '---------------------------------------------------------------------------

        ' Pass the request object to the webservice method and store the resultant data
        ' in the response object
        General.LogDebugNotes("Start BookingList: " & General.SerializeVehRetResRQObject(RQ))
        RS = Proxy.VehRetRes(RQ)
        General.LogDebugNotes("End BookingList: " & General.SerializeVehRetResRSObject(RS) & vbCrLf & vbCrLf)


        'update the global propertie for access from other ascx that does not call the database
        StatusBarText = RS.Warnings(0).Tag
        UIScripts.StatusMsg(Me.Page, RS.Warnings(0).Tag)

        'Dim ser1 As XmlSerializer
        'Dim w1 As StringWriter

        'ser1 = New XmlSerializer(GetType(OTA_VehRetResRS))
        'w1 = New StringWriter
        'ser1.Serialize(w1, RS)
        'w1.ToString()


    End Sub

    Private Sub LoadAgentList()

        ddlAgents.Items.Clear()

        Dim index As Integer = 0
        If Not IsNothing(RS.VehRetResRSInfo) Then
            ' Record the number of agents as we don't want to show
            ' agent name column in datagrid if user has access to only one agent code
            Me.AgentCodeCount = RS.VehRetResRSInfo.TPA_Extensions.Length

            ' Loop through array of agents(stored in TPA_Extensions) from response object 
            For index = 0 To RS.VehRetResRSInfo.TPA_Extensions.Length - 1
                ' Create a new list item to add to drop down list
                Dim newListItem As New ListItem
                ' Agents code and name comes back as a list seperated by a  |
                ' Store agent codes and name in an array then split to get 
                ' individual names and corresponding codes
                Dim delimStr As String = "|"

                Dim textAndValue() As String = RS.VehRetResRSInfo.TPA_Extensions(index).ToString.Split(CType(delimStr, Char))

                newListItem.Text = textAndValue(1).ToUpper    ' agent name
                newListItem.Value = textAndValue(0).ToString  ' agent code
                ' Add to agent dropdown list 
                ddlAgents.Items.Add(newListItem)
            Next

            ' Insert "All Agents" as  the first item in the dropdown list
            ddlAgents.Items.Insert(0, "All agents")
        End If
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         LoadBookingDataGrid
    '   Description:    Create and load a dataset with booking data and bind to datagrid
    '--------------------------------------------------------------------------------------

    Private Sub LoadBookingDataGrid()
        ' Create a dataset to hold all booking data 
        Dim ds As DataSet
        ds = New DataSet
        ' Create a Datatable to store in the dataset
        Dim dt As DataTable = New DataTable("Booking")
        ' Create a new column
        Dim stringDataType As Type = Type.GetType("System.String")

        Dim col As DataColumn = New DataColumn("BookingRefNo", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Vehicle", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Product", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Package", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Checkout", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Checkin", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Lastname", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Firstname", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Status", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("Agent", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("AgentRefNo", stringDataType)
        dt.Columns.Add(col)

        'datacolumn for TTL
        col = New DataColumn("ExpireDate", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("CustomerVersionOfConfirmationPresent", GetType(Boolean))
        dt.Columns.Add(col)

        ' currency changes
        col = New DataColumn("AgentCode", stringDataType)
        dt.Columns.Add(col)

        col = New DataColumn("CurrencyOfTravel", stringDataType)
        dt.Columns.Add(col)
        ' currency changes

        ''//rev:mia April 16 2012
        col = New DataColumn("PickupDate", GetType(System.DateTime))
        dt.Columns.Add(col)

        ''//rev:mia oct 09, 2013 - RoadBear and Britz
        col = New DataColumn("Country", stringDataType)
        dt.Columns.Add(col)

        Dim summaryIndex As Integer

        ' Loop through all vehcile reservations and create a new 
        ' datarow to insert into the datatable

        If Not IsNothing(RS.VehRetResRSCore.VehResSummaries) Then
            ' Reservations exist
            For summaryIndex = 0 To RS.VehRetResRSCore.VehResSummaries.Length - 1
                ' Need to pass the current reservation index to collect the correct data
                NewRow(dt, summaryIndex)
            Next
        End If

        ds.Tables.Add(dt)

        ' Reservations sorted by booking reference number in descending order
        Me.BookingDataset = ds
        Me.LastSortColumn = "bookingrefno"
        Me.LastSortOrder = "desc"

        BindBookingDataGrid(False)
    End Sub

    Public Sub NewRow(ByRef dt As DataTable, ByVal i As Integer)

        Dim dr As DataRow = dt.NewRow
        dr("BookingRefNo") = RS.VehRetResRSCore.VehResSummaries(i).ConfID.ID
        dr("vehicle") = RS.VehRetResRSCore.VehResSummaries(i).Vehicle.VehMakeModel.Name.ToString
        dr("Product") = RS.VehRetResRSCore.VehResSummaries(i).Vehicle.VehType.VehicleCategory
        dr("Package") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.Package.ToString  'RS.VehAvailRSCore.VehVendorAvails(0).VehAvails(0).VehAvailCore.RentalRate(0).RateQualifier & ")"
        'dr("ExpireDate") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.ExpireDate.ToString("dd\/MM\/yy\ HH\:mm")
        dr("ExpireDate") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.ExpireDate.ToString()

        If IsNothing(RS.VehRetResRSCore.VehResSummaries(i).PickUpDateTime) Then
            ' Pickup date optional
            dr("Checkout") = RS.VehRetResRSCore.VehResSummaries(i).PickUpLocation.CodeContext.ToString
        Else
            dr("Checkout") = RS.VehRetResRSCore.VehResSummaries(i).PickUpDateTime.ToString("dd\/MM\/yy\ HH\:mm") & "<br> " & RS.VehRetResRSCore.VehResSummaries(i).PickUpLocation.CodeContext.ToString
        End If

        dr("PickupDate") = CDate(RS.VehRetResRSCore.VehResSummaries(i).PickUpDateTime)

        If IsNothing(RS.VehRetResRSCore.VehResSummaries(i).ReturnDateTime) Then
            ' Return date optional
            dr("CheckIn") = RS.VehRetResRSCore.VehResSummaries(i).ReturnLocation.CodeContext.ToString
        Else
            dr("CheckIn") = RS.VehRetResRSCore.VehResSummaries(i).ReturnDateTime.ToString("dd\/MM\/yy\ HH\:mm") & "<br> " & RS.VehRetResRSCore.VehResSummaries(i).ReturnLocation.CodeContext.ToString
        End If

        If IsNothing(RS.VehRetResRSCore.VehResSummaries(i).PersonName.GivenName) Then
            ' Given name optional
            dr("Firstname") = "N/A"
        Else
            dr("Firstname") = RS.VehRetResRSCore.VehResSummaries(i).PersonName.NameTitle

        End If

        dr("Lastname") = RS.VehRetResRSCore.VehResSummaries(i).PersonName.Surname

        dr("Status") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.Status.ToString
        dr("Agent") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.Agent.ToString
        dr("AgentRefNo") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.AgentRef.ToString
        'dr("ExpireDate") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.ExpireDate.ToString("dd\/MM\/yy\ HH\:mm")
        dr("ExpireDate") = RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.ExpireDate.ToString()

        If RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.THL_CusConf IsNot Nothing _
           AndAlso _
           RS.VehRetResRSCore.VehResSummaries(i).TPA_Extensions.THL_CusConf.ToUpper().Contains("YES") _
        Then
            dr("CustomerVersionOfConfirmationPresent") = True
        Else
            dr("CustomerVersionOfConfirmationPresent") = False
        End If

        ' currency changes
        dr("AgentCode") = ""
        For Each sAgentDetail As String In RS.VehRetResRSInfo.TPA_Extensions
            If sAgentDetail.ToUpper().Split("|"c)(1).Trim() = dr("Agent").ToString().Trim().ToUpper() Then
                dr("AgentCode") = sAgentDetail.ToUpper().Split("|"c)(0).Trim()
                Exit For
            End If
        Next

        If RS.VehRetResRSCore.VehResSummaries(i).PickUpLocation.Value.Trim().ToUpper().Equals("AU") Then
            dr("CurrencyOfTravel") = "AUD"
        Else
            dr("CurrencyOfTravel") = "NZD"
        End If
        ' currency changes

        ''//rev:mia oct 09, 2013 - RoadBear and Britz
        dr("Country") = RS.VehRetResRSCore.VehResSummaries(i).PickUpLocation.Value.ToString

        dt.Rows.Add(dr)

    End Sub

    Private Sub BindBookingDataGrid(ByVal sort As Boolean)
        ' Create a view to use as the datagrid datasource
        dsView = Me.BookingDataset.Tables(0).DefaultView

        ' Only sort when specifed, don't need for paging etc
        If sort = True Then
            ' Sort= Column name & Asc/Desc
            dsView.Sort = Me.LastSortColumn & " " & Me.LastSortOrder
            dsView.RowFilter = Me.LastFilter
            ' Always show the first page
            dgAgentData.CurrentPageIndex = 0
        End If

        dgAgentData.DataSource = dsView
        ' BookingRefNo used to identify the booking record
        dgAgentData.DataKeyField = "BookingRefNo"
        'Clear selected item style
        dgAgentData.SelectedIndex = -1
        dgAgentData.DataBind()

        HideAllDatagridButtons()
        ' Determine which datagrid columns to show the user
        ShowDatagridColumns()
    End Sub

    Private Sub HideAllDatagridButtons()
        btnBook.Visible = False
        btnBookSlots.Visible = False
        'btnBookSlots.Visible = False
        btnModify.Visible = False
        btnDelete.Visible = False
        btnCancel.Visible = False
        btnConfirm.Visible = False
        btnRegister.Visible = False
        btnAddNote.Visible = False
        btnViewConfirmation.Visible = False
        btnCusConfirmation.Visible = False
        ' currency changes
        cmbCurrency.Visible = False
        ' currency changes
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         ShowDatagridColumns
    '   Description:    Different datagrid columns are shown depending on whether the user
    '                   is an agent or customer.
    '--------------------------------------------------------------------------------------
    Private Sub ShowDatagridColumns()

        If CStr(Session("TypeOfUser")) = "agent" Then
            dgAgentData.Columns(0).Visible = True 'select
            dgAgentData.Columns(1).Visible = True 'booking ref 
            dgAgentData.Columns(2).Visible = True 'surname
            dgAgentData.Columns(3).Visible = True 'package
            dgAgentData.Columns(4).Visible = True 'checkin
            dgAgentData.Columns(5).Visible = True 'checkout

            ' Don't want to show agent name column in datagrid 
            ' if user has access to only one agent code
            If Me.AgentCodeCount > 1 Then
                dgAgentData.Columns(6).Visible = True
            Else
                dgAgentData.Columns(6).Visible = False
            End If

            dgAgentData.Columns(7).Visible = True ' agent ref
            dgAgentData.Columns(8).Visible = True ' status
        Else
            dgAgentData.Columns(0).Visible = True   'select
            dgAgentData.Columns(1).Visible = True     'booking ref 
            dgAgentData.Columns(2).Visible = False   'surname
            dgAgentData.Columns(3).Visible = True    'package
            dgAgentData.Columns(4).Visible = True    'checkin
            dgAgentData.Columns(5).Visible = True    'checkout
            dgAgentData.Columns(6).Visible = False   'agent        
            dgAgentData.Columns(7).Visible = True    ' agent ref
            dgAgentData.Columns(8).Visible = True    ' status
            dgAgentData.Columns(9).Visible = True    ' TTL
        End If
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         dgAgentData_SortCommand
    '   Description:    Fires when a user clicks on a sortable datagrid column
    '                   Store sort criteria and rebind datagrid
    '--------------------------------------------------------------------------------------
    Public Sub dgAgentData_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Dim newSortColumn As String = e.SortExpression.ToString
        Dim newSortOrder As String = "Asc"

        If newSortColumn.Equals(LastSortColumn) And Me.LastSortOrder.Equals("Asc") Then
            'If same coumn was selected for sorting reverse sort order
            newSortOrder = "Desc"   'else=Asc
        End If

        ' Set new sortorder and column to sort by
        Me.LastSortOrder = newSortOrder
        Me.LastSortColumn = newSortColumn
        ' Rebind datagrid to show updated display
        BindBookingDataGrid(True)
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         dgAgentData_SelectedIndexChanged
    '   Description:    Fires when a user clicks on the 'select' button in the datagrid
    '--------------------------------------------------------------------------------------
    Public Sub dgAgentData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgAgentData.SelectedIndexChanged
        ' Get status of selected reservation, stored in a label in each row of the datagrid
        Dim lblText As String = CType(dgAgentData.SelectedItem.FindControl("lblStatus"), System.Web.UI.WebControls.Label).Text

        slotsDetail.Visible = False
        ' Agent and Customer Confirmation stuff

        If UserCanSeeCustomerConfirmations Then
            ' allowed to view customer confirmations
            btnCusConfirmation.Visible = True
            ' can view customer version of confirmation
            If CBool(dgAgentData.SelectedItem.Cells(11).Text) Then
                'btnCusConfirmation.Enabled = True
                'btnCusConfirmation.Text = "Customer Confirmation"
            Else
                'btnCusConfirmation.Enabled = false
                'btnCusConfirmation.Text = "Customer Document"
            End If
            '"View Customer Document"
        Else
            ' not allowed to view customer confirmations
            btnCusConfirmation.Visible = False
        End If

        Select Case lblText.ToLower
            ' Display certain modify, delete, cancel buttons etc
            Case "quotation"   'Quote


                'SlotsPanel = GetSlots()
                'If String.IsNullOrEmpty(SlotsPanel) Then
                '    btnBook.Visible = True
                '    btnBookSlots.Visible = False
                'Else
                '    btnBook.Visible = False
                '    btnBookSlots.Visible = True
                'End If
                'DisableAll = False
                'slotsPanel = GetSlots()
                'If String.IsNullOrEmpty(slotsPanel) Then
                '    DisableAll = True

                'End If

                btnBook.Visible = True

                btnModify.Visible = True
                btnDelete.Visible = True
                btnCancel.Visible = False
                btnConfirm.Visible = False
                btnRegister.Visible = False
                btnAddNote.Visible = True
                btnViewConfirmation.Visible = True

                btnViewConfirmation.Text = "Agent Quotation"
                btnCusConfirmation.Text = "Customer Quotation"

                ' Set the page focus on the Book button
                UIScripts.SetFocus(Me.Page, btnBook.ClientID)

            Case "provisional"    ' Provisional
                btnBook.Visible = False
                btnBookSlots.Visible = False
                btnModify.Visible = True
                btnDelete.Visible = False
                btnCancel.Visible = True
                btnConfirm.Visible = True
                btnAddNote.Visible = True
                btnViewConfirmation.Visible = True


                If CStr(Session("TypeOfUser")) = "customer" Then
                    ' Allow the customer to pre-register a booked or provisional booking
                    btnRegister.Visible = True
                Else
                    btnRegister.Visible = False
                End If

                btnViewConfirmation.Text = "Agent Provisional"
                btnCusConfirmation.Text = "Customer Provisional"

                UIScripts.SetFocus(Me.Page, btnModify.ClientID)

            Case "confirm"    ' confirmed
                btnBook.Visible = False
                btnBookSlots.Visible = False
                btnModify.Visible = True
                btnDelete.Visible = False
                btnCancel.Visible = True
                btnConfirm.Visible = False
                btnAddNote.Visible = True
                btnViewConfirmation.Visible = True


                If CStr(Session("TypeOfUser")) = "customer" Then
                    btnRegister.Visible = True
                Else
                    btnRegister.Visible = False
                End If

                btnViewConfirmation.Text = "Agent Confirmation"
                btnCusConfirmation.Text = "Customer Confirmation"

                UIScripts.SetFocus(Me.Page, btnModify.ClientID)
                'Added by nimesh on 9th April 2015
            Case "pending cancellation"
                btnBook.Visible = False
                btnBookSlots.Visible = False
                btnModify.Visible = False
                btnDelete.Visible = False
                btnCancel.Visible = False
                btnConfirm.Visible = False
                btnAddNote.Visible = False
                btnViewConfirmation.Visible = True
                btnCusConfirmation.Visible = False
                btnViewConfirmation.Text = "Agent Confirmation"
                'End Added by Nimesh on 9th April 2015
        End Select

        ' currency changes 

        Dim sCurrencyOfTravel As String = ""
        Dim sCurrencyOfTravelDesc As String = ""
        Dim countryoftravelValue As String = ""
        If UserHasCurrencyOptions Then


            Dim sAgentCode As String = ""
            Dim sDefaultCurrency As String = ""

            cmbCurrency.Items.Clear()

            sCurrencyOfTravel = dgAgentData.SelectedItem.Cells(13).Text.Trim.ToUpper()

            If sCurrencyOfTravel = "AUD" Then
                sCurrencyOfTravelDesc = "AUD-Australian Dollar"
            Else
                sCurrencyOfTravelDesc = "NZD-New Zealand Dollar"
            End If

            cmbCurrency.Items.Add(New ListItem(sCurrencyOfTravelDesc, "1"))

            sAgentCode = dgAgentData.SelectedItem.Cells(12).Text.Trim().ToUpper()
            ''rev:mia Oct. 9 2013 - RoadBear and Britz
            Dim countryoftravel As HiddenField = CType(dgAgentData.SelectedItem.Cells(1).FindControl("countryOfTravel"), HiddenField)

            If (Not countryoftravel Is Nothing) Then
                countryoftravelValue = countryoftravel.Value & "D"
            End If

            For Each oAgent As ExchangeRatesTypeAgent In ExchangeRatesLookupData
                If oAgent.Code.ToUpper().Trim().Equals(sAgentCode) Then
                    ' this is the agent
                    For Each oCurrency As ExchangeRatesTypeAgentCurr In oAgent.Curr
                        If oCurrency.FromCode.ToUpper().Trim().Equals(sCurrencyOfTravel) Then
                            ' this is the currency code
                            ' look in the current list of items and ensure its not already in there!
                            Dim bAlreadyExists As Boolean = False
                            For Each oListItem As ListItem In cmbCurrency.Items
                                If oListItem.Text = oCurrency.ToCode & "-" & CStr(oCurrency.ToDesc) Then
                                    bAlreadyExists = True
                                    Exit For
                                End If
                            Next

                            ' see if user has this currency for this agent
                            Dim bUserHasThisCurrencyInContextOfThisAgent As Boolean = False
                            For Each oCurrOption As String() In CurrencyOptionsData
                                If oCurrOption(0).ToUpper().Trim().Equals(sAgentCode) Then
                                    ' matching agent code
                                    If oCurrOption(1).ToUpper().Contains(oCurrency.ToCode) Then
                                        bUserHasThisCurrencyInContextOfThisAgent = True
                                        Exit For
                                    End If
                                End If
                            Next

                            If Not bAlreadyExists AndAlso Not CDbl(oCurrency.Rate) = 0.0 AndAlso bUserHasThisCurrencyInContextOfThisAgent Then
                                cmbCurrency.Items.Add(New ListItem(oCurrency.ToCode & "-" & CStr(oCurrency.ToDesc), oCurrency.Rate))
                            End If
                        End If
                    Next
                End If
            Next

            cmbCurrency.ClearSelection()

            ' figure out the default currency
            ' select the default item in the drop down
            ' get rid of currencies not allocated to this user

            For Each saCurrencySetting As String() In CurrencyOptionsData
                Dim sAgentCodeCurrent, sCurrencyList, sDefaultCurrencyCode As String
                sAgentCodeCurrent = saCurrencySetting(0)

                If sAgentCodeCurrent.ToUpper().Trim().Equals(sAgentCode.ToUpper().Trim()) Then
                    ' matching agent code
                    sCurrencyList = saCurrencySetting(1)
                    sDefaultCurrencyCode = saCurrencySetting(2)

                    For j As Integer = cmbCurrency.Items.Count - 1 To 1 Step -1
                        If Not sCurrencyList.Contains(cmbCurrency.Items(j).Text.Split("-"c)(0).ToUpper().Trim()) Then
                            cmbCurrency.Items.RemoveAt(j)
                            ' take away any currencies not assigned to user, but leave the currency of travel item (item 0) alone
                            Continue For
                        End If
                        If cmbCurrency.Items(j).Text.Split("-"c)(0).ToUpper().Trim() = sDefaultCurrencyCode.Trim().ToUpper() Then
                            cmbCurrency.SelectedIndex = j
                        End If
                    Next

                End If
            Next

            cmbCurrency.Visible = True
        Else
            cmbCurrency.Items.Clear()
            sCurrencyOfTravel = dgAgentData.SelectedItem.Cells(13).Text.Trim.ToUpper()

            If sCurrencyOfTravel = "AUD" Then
                sCurrencyOfTravelDesc = "AUD-Australian Dollar"
            Else
                sCurrencyOfTravelDesc = "NZD-New Zealand Dollar"
            End If

            cmbCurrency.Items.Add(New ListItem(sCurrencyOfTravelDesc, "1"))
            cmbCurrency.Items(0).Selected = True
            cmbCurrency.Visible = False
        End If

        ''rev:mia Oct. 9 2013 - RoadBear and Britz
        If (countryoftravelValue = "USD") Then
            Try
                cmbCurrency.SelectedIndex = cmbCurrency.Items.IndexOf(cmbCurrency.Items.FindByText("USD-US Dollar"))
            Catch ex As Exception
            End Try

        End If
        ' currency changes

    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         dgAgentData_ItemDataBound
    '   Description:    Fires when each row of the dataset is bound to the datagrid
    '                   Sets the datagrid row color depending on whether the item is a quote
    '                   confirmed or provisional booking. Sets tooltip as well
    '--------------------------------------------------------------------------------------
    Public Sub dgAgentData_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Dim status As String = e.Item.Cells(9).Text
        Select Case status.ToLower
            Case "confirm"
                e.Item.ForeColor = System.Drawing.Color.Black
                e.Item.Cells(0).ToolTip = "Click to Modify or Cancel"
            Case "quotation"
                e.Item.ForeColor = System.Drawing.Color.Green
                e.Item.Cells(0).ToolTip = "Click to Modify,Confirm or Cancel"
            Case "provisional"
                e.Item.ForeColor = System.Drawing.Color.Red
                e.Item.Cells(0).ToolTip = "Click to Modify,Confirm or Cancel"
        End Select
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         dgAgentData_PageIndexChanged
    '   Description:    Fired when paging buttons are clicked, Go to specified page.
    '--------------------------------------------------------------------------------------
    Private Sub dgAgentData_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAgentData.PageIndexChanged
        ' Set CurrentPageIndex to the page the user clicked.
        dgAgentData.CurrentPageIndex = e.NewPageIndex
        ' Rebind the data to refresh the DataGrid control.
        Me.LastSortColumn = "bookingrefno"
        Me.LastSortOrder = "desc"
        BindBookingDataGrid(False)

        UIScripts.StatusMsg(Me.Page, StatusBarText)

    End Sub

    Private Sub btnClearFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearFilter.Click
        ' Clear keyword, reset radio button and retrieve all reservations
        InitBookingList()
        txtSearchKey.Text = ""
        ddlFilter.SelectedIndex = 0
    End Sub

    Private Sub btnBook_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBook.Click
        If lblLoadError.Text = "" Then
            ' currency changes
            SetCurrencyData()

            ' currency changes
            ' No errors on page allow user to cary on with selection
            ' Pass the booking id and status (quote, confirm etc) 

            '1. Display slot Data if it is present
            '2. If Slot Data is not present then continue with normal routine
            '3. if slot data is present then display continue button and disable book button
            '4. on clicking of continue, perform booking with time slot

            DisableAll = False
            slotsPanel = GetSlots()
            If String.IsNullOrEmpty(slotsPanel) Then
                DisableAll = True
                RaiseEvent BookBooking(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), GetStatus)
            Else
                slotsDetail.Visible = True
                'btnBook.Visible = False
                HideAllDatagridButtons()

            End If
           
            'RaiseEvent BookBookingV2(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), GetStatus, testSlot, testSlotId)
            'RaiseEvent BookBooking(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), GetStatus)
        End If
    End Sub

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click
        If lblLoadError.Text = "" Then
            ' currency changes
            SetCurrencyData()
            ' currency changes
            Dim testSlot As String = txtSlot.Value
            Dim testSlotId As String = txtSlotId.Value
            RaiseEvent ModifyBooking(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), GetStatus)
        End If
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lblLoadError.Text = "" Then
            ' currency changes
            SetCurrencyData()
            ' currency changes
            RaiseEvent DeleteBooking(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)))
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If lblLoadError.Text = "" Then
            ' currency changes
            SetCurrencyData()
            ' currency changes
            RaiseEvent CancelBooking(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)))
        End If
    End Sub

    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        ' Progress from provisional to confirmed 
        If lblLoadError.Text = "" Then
            ' currency changes
            SetCurrencyData()
            ' currency changes
            RaiseEvent ConfirmBooking(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), TransactionActionType.Book)
        End If
    End Sub

    Private Sub btnRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        ' Redirect to pre-register page
        ' currency changes
        SetCurrencyData()
        ' currency changes
        Dim bookingId As String = CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex))
        Response.Redirect("Pre-Registration.aspx?No=" & Server.UrlEncode(bookingId))
    End Sub

    Private Sub btnAddNote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNote.Click
        ' currency changes
        SetCurrencyData()
        ' currency changes
        ' Display popup window to enter a booking note to selected booking
        UIScripts.SetFocus(Me.Page, btnAddNote.ClientID)
        Dim id As String = CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex))
        Dim status As TransactionActionType = GetStatus()
        Dim url As String = "../content/bookingnote.aspx?id=" & id & "&type=" & status
        UIScripts.NewWindow(Me.Page, url, 350, 250, 250, 150, False, False, False, False, False, False)
        UIScripts.NewWindow(Me.Page, url, 350, 250, 250, 150, False, False, False, False, False, False)
    End Sub

    Private Sub btnViewConfirmation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewConfirmation.Click
        ' currency changes
        SetCurrencyData()
        ' currency changes
        ' Fire an event to let BookingContainer know to show standard confirmation
        RaiseEvent ViewConfirmation(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), False)
    End Sub

    Private Sub btnCusConfirmation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCusConfirmation.Click
        ' currency changes
        SetCurrencyData()
        ' currency changes
        ' Fire an event to let BookingContainer know to show standard confirmation
        RaiseEvent ViewConfirmation(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), True)
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         GetStatus
    '   Description:    Gets the booking status (provisonal, quote, confirm) for the 
    '                   currently selected vehicle reservation and returns the corresponding
    '                   OTA transaction type.
    '--------------------------------------------------------------------------------------
    Private Function GetStatus() As TransactionActionType
        ' Get the status out of the status label in datagrid
        Dim status As String = CType(dgAgentData.SelectedItem.FindControl("lblStatus"), System.Web.UI.WebControls.Label).Text
        Dim modifyType As TransactionActionType

        Select Case status.ToLower
            Case "confirm"
                modifyType = TransactionActionType.Book
            Case "quotation"
                modifyType = TransactionActionType.Quote
            Case "provisional"
                modifyType = TransactionActionType.Hold
        End Select
        Return modifyType
    End Function

    '--------------------------------------------------------------------------------------
    '   Method:         btnSearchAll_Click
    '   Description:    Retrieves vehicle reservations based on the selected search criteria
    '                   and binds to datagrid
    '--------------------------------------------------------------------------------------
    Private Sub btnSearchAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAll.Click
        Dim surname As String = ""
        Dim agentrefNo As String = ""
        Dim agent As String = ""
        Dim bookingNo As String = ""
        Dim sDepartureDate As String = ""

        ''//rev:mia April 16 2012
        'Dim sQuote As String = ""
        'Dim sProvisional As String = ""
        'Dim sConfirmed As String = ""

        ' Get the search criteria based on the filter type selection
        Dim sStatus As String = ""
        Select Case ddlFilter.SelectedItem.Value
            Case "Cust Surname"
                surname = txtSearchKey.Text
            Case "Booking No"
                bookingNo = txtSearchKey.Text
            Case "Agent's Ref No"
                agentrefNo = txtSearchKey.Text
            Case "DepartureDate"
                sDepartureDate = txtSearchKey.Text

                ''//rev:mia April 16 2012
            Case "Quote"
                sStatus = "QN"
                bookingNo = txtSearchKey.Text
            Case "Provisional"
                sStatus = "NN"
                bookingNo = txtSearchKey.Text
            Case "Confirmed"
                sStatus = "KK"
                bookingNo = txtSearchKey.Text
        End Select


        Try
            'GetVehicleReservationData(surname, bookingNo, ddlAgents.SelectedItem.Value, agentrefNo)
            GetVehicleReservationData(surname, bookingNo, ddlAgents.SelectedItem.Value, agentrefNo, sDepartureDate, sStatus)
            If Not IsNothing(RS.Errors) Then
                If RS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                    Session("POS") = Nothing
                    'Response.Redirect("..\..\Content\AccessDenied.aspx")
                    lblLoadError.Text = RS.Errors(0).Tag.ToString
                    Exit Sub
                Else
                    lblLoadError.Text = "For " & ddlFilter.SelectedItem.Value & " - " & txtSearchKey.Text & " - " & RS.Errors(0).Tag.ToString.ToUpper()
                    Exit Sub
                End If
            End If
            ' Always show the first page  
            dgAgentData.CurrentPageIndex = 0
            LoadBookingDataGrid()
        Catch ex As Exception
            lblLoadError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try
    End Sub

    '--------------------------------------------------------------------------------------
    '   Method:         Page_PreRender
    '   Description:    Occurs when the page is about to be rendered. Determine whether to
    '                   display the datagrid depending if it contains data and also whether
    '                   to display the search functionality depending if the user is an agent
    '                   or customer.
    '--------------------------------------------------------------------------------------
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        If dgAgentData.Items.Count = 0 Then
            ' No vehicle bookings to display
            ' don't show datagrid and search funtionality
            tblSearch.Visible = False
            tblDatagrid.Visible = False
            lblExistingbookings.Visible = True
        Else
            ' Bookings to display
            If CStr(Session("TypeOfUser")) = "agent" Then
                tblSearch.Visible = True
            Else
                ' Customer is not allowed to see search function
                tblSearch.Visible = False
            End If
            tblDatagrid.Visible = True
            lblExistingbookings.Visible = False
        End If
    End Sub

#Region "Currency Changes"

    Sub SetCurrencyData()

        MRU_CurrencyCode = cmbCurrency.SelectedItem.Text.Split("-"c)(0).Trim()
        ''rev:mia Oct. 9 2013 - RoadBear and Britz
        MRU_CurrencyMultiplier = CDbl(cmbCurrency.SelectedValue)

        Try
            If (MRU_CurrencyCode = "USD") Then
                MRU_CurrencyMultiplier = CDbl(1)
            Else
                ''asked again
                Dim countryoftravel As HiddenField = CType(dgAgentData.SelectedItem.Cells(1).FindControl("countryOfTravel"), HiddenField)
                Dim countryoftravelValue As String = ""
                If (Not countryoftravel Is Nothing) Then
                    countryoftravelValue = countryoftravel.Value & "D"
                    If (countryoftravelValue = "USD") Then
                        Try
                            ''force it to select USD only
                            cmbCurrency.SelectedIndex = cmbCurrency.Items.IndexOf(cmbCurrency.Items.FindByText("USD-US Dollar"))
                            MRU_CurrencyMultiplier = CDbl(1)
                            MRU_CurrencyCode = "USD"
                        Catch ex As Exception
                        End Try

                    Else
                        MRU_CurrencyMultiplier = CDbl(cmbCurrency.SelectedValue)
                    End If
                Else
                    MRU_CurrencyMultiplier = CDbl(cmbCurrency.SelectedValue)
                End If


            End If
        Catch ex As Exception
            MRU_CurrencyMultiplier = CDbl(cmbCurrency.SelectedValue)

        End Try



    End Sub

#End Region

    Private Function GetSlots() As String
        'StringBuilder sb = new StringBuilder();
        ' //bool isDevEnv = bool.Parse(ConfigurationManager.AppSettings["isDevEnv"] == null ? "false" : ConfigurationManager.AppSettings["isDevEnv"]);
        ' //string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
        ' ////Use Rest compatible instead of SOAP
        ' //string slotData = string.Empty;
        ' //if (string.IsNullOrEmpty(bookingId))

        ' //    slotData = GetStringForURL(auroraBaseURL + "/GetAvailableSlot?sRntId=&sAvpId=" + avpID + "&sBpdId=");//TODO: consider loading an XML structure once service sealed
        ' //else
        ' //    slotData = GetStringForURL(auroraBaseURL + "/GetAvailableSlot?sRntId=" + bookingId + "&sAvpId=&sBpdId=");//TODO: consider loading an XML structure once service sealed

        ' //string quotePackageData = GetStringForURL(auroraBaseURL + "/GetInsuranceAndExtraHireItemsForRentalQuote?BookingReference=" + rentalNumber + "&ReferenceId=" + rentalNumber);
        ' //--
        ' string otaBase = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraOTABase"];
        ' Aurora_OTA otaProxy = new Aurora_OTA(otaBase);

        ' var res = otaProxy.GetAvailableSlot("", avpID, "", sNewPickupDate,sNewPickupLocation);
        ' string slotData = res.OuterXml;
        ' string retString = "Success";
        Dim sb As New StringBuilder()
        Dim otaBase As String = System.Web.Configuration.WebConfigurationManager.AppSettings("AuroraOTABase")
        Dim otaProxy As New Aurora_OTAV3(otaBase)

        Dim bookingId As String = CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex))




        ' Dim data As String = DataGridView1.SelectedRows(0).Cells(5).Value.ToString 
        'Dim retData As XmlNode = otaProxy.GetAvailableSlot(bookingId.Replace("/", "-"), String.Empty, String.Empty, String.Empty, String.Empty)
        Dim retData As XmlNode = otaProxy.GetAvailableSlot(bookingId.Replace("/", "-"), String.Empty, String.Empty, False, False, "KK", String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)

        Dim slotData As String
        If retData Is Nothing Then
            Return String.Empty
        Else
            slotData = retData.OuterXml
        End If

        If (slotData IsNot Nothing AndAlso slotData <> String.Empty) AndAlso slotData.IndexOf("AvailableSlots") >= 0 Then

            Dim slotXml As New XmlDocument()
            slotXml.LoadXml(slotData)
            'slotXml.GetXElement();
            Dim result As System.Collections.Generic.List(Of XElement) = (From c In slotXml.GetXElement().Descendants("Slot") Select c).ToList()
            Dim rowCount As Integer = result.Count()
            'var widthPercentage = Math.Round(Decimal.Parse(100.ToString()) / rowCount, 0);
            Dim widthPercentage As Integer = 100 \ rowCount
            Dim fontSize As String = "18px"
            If rowCount > 6 Then
                fontSize = "16px"
            End If

            Dim selectedCount As Integer = 0
            'Dim members = From m In doc.Element("Tabel").Elements("Member")
            'Where m.Element("Naam").Value = "Ghostbullet93"
            'Select Case kills = m.Element("Kills").Value

            Dim returnedIsSelected As IList = result.Elements("IsSelected").ToList()
            For ctr As Integer = 0 To returnedIsSelected.Count - 1
                'If returnedIsSelected(ctr)
                'If returnedIsSelected(ctr).ToString().Equals("0") Then
                '    selectedCount += 1
                'End If
                If DirectCast(returnedIsSelected(ctr), System.Xml.Linq.XElement).Value.Equals("1") Then
                    selectedCount += 1
                End If



            Next
            'For Each tempSelected As String In returnedIsSelected
            '    If tempSelected.Equals("1") Then selectedCount += 1
            'Next
            Dim bookingTime As String = String.Empty
            Dim bookingTotime As Date

            If selectedCount = 0 Then
                bookingTime = dgAgentData.Items(dgAgentData.SelectedIndex).Cells(4).Text.Split(CChar(" "))(1).Replace("<br>", "")
                bookingTotime = DateTime.Parse(bookingTime, System.Globalization.CultureInfo.CurrentCulture)

                bookingTime = bookingTotime.ToString("HH:mm:ss tt")
            End If

            For Each slot As XElement In result
                Dim slotDesc As String = slot.Element("SlotDesc").Value
                Dim slotId As String = slot.Element("SlotId").Value
                Dim allocatedNumbers As String = If(slot.Element("AllocatedNumbers") Is Nothing, "0", slot.Element("AllocatedNumbers").Value)
                Dim bookedNumbers As String = If(slot.Element("Bookednumbers") Is Nothing, "0", slot.Element("Bookednumbers").Value)
                Dim availableNumbers As String = If(slot.Element("AvailableNumbers") Is Nothing, "0", slot.Element("AvailableNumbers").Value)

                Dim isSlotAvailable As String = If(slot.Element("IsSlotAvailable") Is Nothing, "0", slot.Element("IsSlotAvailable").Value)

                Dim isAllDaySlot As String = If(slot.Element("IsAllDaySlot") Is Nothing, "0", slot.Element("IsAllDaySlot").Value)
                Dim isSelected As String = slot.Element("IsSelected").Value

                Dim slotTimes As String() = slotDesc.Split("-"c)
                Dim timeSplit1 As String() = slotTimes(0).Split(" "c)
                Dim timeSplit2 As String() = slotTimes(1).Trim().Split(" "c)

                Dim availUnavail As String = If(isSlotAvailable.Equals("1"), "available", "unavailable")
                Dim selected As Boolean = If(isSelected.Equals("1"), True, False)

                If Not selected And selectedCount = 0 Then
                    Dim t1 As DateTime = Convert.ToDateTime(bookingTime)
                    Dim t2 As DateTime = Convert.ToDateTime(slotTimes(0))
                    Dim t3 As DateTime = Convert.ToDateTime(slotTimes(1)).AddMinutes(-1)
                    Dim ts1 As TimeSpan = t1.TimeOfDay
                    Dim ts2 As TimeSpan = t2.TimeOfDay
                    Dim ts3 As TimeSpan = t3.TimeOfDay

                    'If ts1.isbetween(t2, t3) Then

                    'End If
                    If TimeBetween(t1, ts2, ts3) Then
                        selected = True
                    End If

                End If
                Dim availUnavailText As String = availUnavail.ToUpper()
                Dim ava As String = " (" & (Integer.Parse(allocatedNumbers) - Integer.Parse(bookedNumbers)).ToString() & ")"
                If Integer.Parse(availableNumbers) <= 0 Then
                    isSlotAvailable = "0"
                    isSelected = "0"
                    availUnavail = "unavailable"
                    selected = False
                    availUnavailText = "UNAVAILABLE"
                End If
                If selected Then
                    availUnavail += " selected"
                    availUnavailText = "SELECTED"
                    selectedCount += 1
                End If
                'else 
                'If Not availUnavail.Equals("unavailable") AndAlso isDevEnv Then
                '    availUnavailText += ava
                'End If

                sb.Append("<div class='timeslots " & availUnavail & "' id='slot" & slotId & "' style='width:" & widthPercentage.ToString() & "%'>")
                sb.Append("<div class='slots'>")
                sb.Append("<div class='time left' style='font-size:" & fontSize & "'>" & timeSplit1(0) & "<span class='ampm'>" & timeSplit1(1) & "</span></div>")
                sb.Append("<div class='arrow'><span class='arrowhover'>&nbsp;</span></div>")
                sb.Append("<div class='time right' style='font-size:" & fontSize & "'>" & timeSplit2(0) & "<span class='ampm'>" & timeSplit2(1) & "</span></div>")
                sb.Append("<div class='availability'>" & availUnavailText & "</div>")
                sb.Append("</div>")
                sb.Append("</div>")

            Next
            
        Else
            'sb.Append("<div class='timeslots unavailable' id='slot0' style='width:100%'>")
            'sb.Append("<div class='slots' style='width:100%'>")
            ''//sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='"+passedParams+"'>alternate availability</a></div>");
            ''//+"&rType=GetAlternateOptions&Brand=" + brand
            ''    ////sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='preloader.aspx?vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brand + "'>alternate availability</a></div>");
            ''sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='altavailability.aspx?sid=" + sid + "&vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brand + "'>Alternate Availability</a></div>")

            'sb.Append("<div class='availability'>unavailable</div>")
            'sb.Append("</div>")
            'sb.Append("</div>")
            Return Nothing
        End If
        Return sb.ToString()




        'Return sb.ToString()

    End Function

    Private Function TimeBetween(datetime As DateTime, start As TimeSpan, [end] As TimeSpan) As Boolean
        ' convert datetime to a TimeSpan
        Dim now As TimeSpan = datetime.TimeOfDay
        ' see if start comes before end
        If start < [end] Then
            Return start <= now AndAlso now <= [end]
        End If
        ' start is after end, so do the inverse comparison
        Return Not ([end] < now AndAlso now < start)
    End Function

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Dim testSlot As String = txtSlot.Value
        Dim testSlotId As String = txtSlotId.Value
        RaiseEvent BookBookingV2(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), GetStatus, testSlot, testSlotId)
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("QuotesAndBookings.aspx")
    End Sub
End Class

#Region "CodeNotUsed"

'Private Sub GetAllReservations()
'    Dim RQ As New OTA_VehRetResRQ
'    Dim RS As New OTA_VehRetResRS

'    RQ.EchoToken = System.Guid.NewGuid().ToString()
'    RQ.SequenceNmbr = CStr(1)
'    RQ.Target = OTA_VehAvailRateRQTarget.Test
'    RQ.TimeStamp = Date.Now
'    RQ.Version = CType(1.0, Decimal)
'    RQ.VehRetResRQCore.UniqueID.ID = "200"
'    RQ.VehRetResRQCore.UniqueID.Type = "Booking"

'    RS = Proxy.VehRetRes(RQ)
'End Sub
'Private Sub LoadDataTemp()
'    Dim ae As New System.Text.ASCIIEncoding
'    Dim byteXML() As Byte = ae.GetBytes(GetXML)
'    Dim memorystream As New MemoryStream(byteXML)
'    Dim ds As New DataSet
'    ds.ReadXml(memorystream)
'    DataGrid1.DataSource = ds
'    DataGrid1.DataBind()
'End Sub

'Private Function Convert(ByVal sqlString As String) As String
'    Return sqlString.Replace("'", "''")
'End Function

'Private Function GetXML() As String
'    Return " <NewDataSet> <Animal_Table> <Animal_Id>8</Animal_Id> <Animal_Name>Byrdie</Animal_Name> <Type_Of_Animal>Bird</Type_Of_Animal> </Animal_Table>  <Animal_Table> <Animal_Id>7</Animal_Id>  <Animal_Name>Tweetie</Animal_Name>  <Type_Of_Animal>Bird</Type_Of_Animal> </Animal_Table> </NewDataSet>"
'End Function

'Private Sub HideDatagridRows(ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
'    If Me.TypeOfUser = UserType.Agent Then
'        e.Item.Cells(0).Visible = True   'Bookingrefno
'        e.Item.Cells(1).Visible = False   'vehicle
'        e.Item.Cells(2).Visible = False   'product
'        e.Item.Cells(3).Visible = True   'package
'        e.Item.Cells(4).Visible = True   'checkout
'        e.Item.Cells(5).Visible = True   'checkin
'        e.Item.Cells(6).Visible = True   'lastname
'        e.Item.Cells(7).Visible = False   'firstname
'        e.Item.Cells(8).Visible = True   'status
'        e.Item.Cells(9).Visible = True   'agent
'        e.Item.Cells(10).Visible = True   ' agentrefno
'    Else
'        e.Item.Cells(0).Visible = True   'Bookingrefno
'        e.Item.Cells(1).Visible = False     'vehicle
'        e.Item.Cells(2).Visible = False   'product
'        e.Item.Cells(3).Visible = False   'package
'        e.Item.Cells(4).Visible = True   'checkout
'        e.Item.Cells(5).Visible = True   'checkin
'        e.Item.Cells(6).Visible = False   'lastname
'        e.Item.Cells(7).Visible = False   'firstname
'        e.Item.Cells(8).Visible = False   'status
'        e.Item.Cells(9).Visible = False   'agent
'        e.Item.Cells(10).Visible = False   ' agentrefno
'    End If
'End Sub




'Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
'    'This method dynamically creates a sql string based on the users input
'    'and applies it to the datatview from the dataset stored in viewstate.


'    Dim searchKey As String = Convert(txtSearchKey.Text)  'double any single qoutes to double qoutes (because it is going to be part of a sql string)
'    Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder

'    Select Case ddlFilter.SelectedItem.Value.ToLower  'determine if the filter is  applied to surname, booking no etc
'        Case "cust surname"
'            sb.Append("(lastname LIKE '")
'            sb.Append(searchKey)
'            sb.Append("%')")

'        Case "booking no"
'            sb.Append("(bookingrefno LIKE '")
'            sb.Append(searchKey)
'            sb.Append("%')")

'        Case "agent's ref no"
'            sb.Append("(AgentRefNo LIKE '")
'            sb.Append(searchKey)
'            sb.Append("%')")
'    End Select

'    If Not ddlAgents.SelectedItem.Value = "All agents" Then  ' determine if there is a filter on agents
'        If sb.Length > 0 Then
'            sb.Append("AND")    ' there is already a filter on one of the radio selections 
'        End If
'        sb.Append("(Agent LIKE '")
'        sb.Append(ddlAgents.SelectedItem.Value)
'        sb.Append("%')")
'    End If

'    Me.LastFilter = sb.ToString
'    Me.LastSortColumn = "bookingrefno"
'    Me.LastSortOrder = "desc"
'    BindBookingDataGrid(True)

'End Sub

'Private Sub LoadDatasetDummyData()
'    Dim ds As DataSet


'    If Not IsPostBack Then
'        ds = New DataSet
'        Dim dt As DataTable = New DataTable("Booking")
'        Dim col As DataColumn = New DataColumn

'        col.ColumnName = "BookingRefNo"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Vehicle"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Product"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Package"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Checkout"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Checkin"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)


'        col = New DataColumn
'        col.ColumnName = "Lastname"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Firstname"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Status"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        col = New DataColumn
'        col.ColumnName = "Agent"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)


'        col = New DataColumn
'        col.ColumnName = "AgentRefNo"
'        col.DataType = Type.GetType("System.String")
'        dt.Columns.Add(col)

'        Dim dr As DataRow = dt.NewRow
'        dr("BookingRefNo") = "200"
'        dr("vehicle") = "Mazda"
'        dr("Product") = "Car"
'        dr("Package") = "Skiwi"
'        dr("Checkout") = "Monday"
'        dr("CheckIn") = "Friday"
'        dr("Lastname") = "Lonergan"
'        dr("Firstname") = "Mark"
'        dr("Status") = "P"
'        dr("Agent") = "FER Malta"
'        dr("AgentRefNo") = "9"
'        dt.Rows.Add(dr)

'        Dim dr2 As DataRow = dt.NewRow
'        dr2("BookingRefNo") = "300"
'        dr2("vehicle") = "Mazda"
'        dr2("Product") = "Bus"
'        dr2("Package") = "All"
'        dr2("Checkout") = "Tuesday"
'        dr2("CheckIn") = "Saturday"
'        dr2("Lastname") = "Murphy"
'        dr2("Firstname") = "Larry"
'        dr2("Status") = "Q"
'        dr2("Agent") = "FER Zurich"
'        dr2("AgentRefNo") = "10"
'        dt.Rows.Add(dr2)


'        Dim dr3 As DataRow = dt.NewRow
'        dr3("BookingRefNo") = "300"
'        dr3("vehicle") = "Mazda"
'        dr3("Product") = "Airplane"
'        dr3("Package") = "All"
'        dr3("Checkout") = "wednesday"
'        dr3("CheckIn") = "Saturday"
'        dr3("Lastname") = "Lewis"
'        dr3("Firstname") = "Mary"
'        dr3("Status") = "C"
'        dr3("Agent") = "FER Zurich"
'        dr3("AgentRefNo") = "11"
'        dt.Rows.Add(dr3)

'        ds.Tables.Add(dt)

'        Me.BookingDataset = ds
'        Me.LastSortColumn = "bookingrefno"
'        Me.LastSortOrder = "desc"
'    End If
'    BindBookingDataGrid(True)


'End Sub
#End Region

