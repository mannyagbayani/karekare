#Region "Notes"
'26June2006 Karel Jordaan
'Changed the base from System.Web.UI.UserControl
'to THL_WebUserControlBase 
'update the global propertie for access from other ascx that does not call the database


'--------------------------------------------------------------------------------------
'   Class:          BookingSummary
'   Description:    This control displays the booking summary for either an existing booking
'                   or a completely new booking. all vehicle information as well as customer
'                   and agent fees are displayed (when appropriote)
'--------------------------------------------------------------------------------------
#End Region



Partial Class BookingSummary
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Protected WithEvents FeesAndCharges1 As FeesAndCharges
    Protected WithEvents FeesAndChargesNewBooking1 As FeesAndChargesNewBooking
    Protected WithEvents lblGst As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrency3 As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotalPayable As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrency2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblAgentDiscount As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrency1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotalRentalCharge As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrency As System.Web.UI.WebControls.Label
    Protected WithEvents AgentRow As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rpFeesAndCharges As System.Web.UI.WebControls.Repeater
    Protected WithEvents FeesAndCharges2 As FeesAndCharges


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim mVehicleIndex As Integer
    Dim mVendorIndex As Integer
    Dim dAvailableVehicleRS As OTA_VehAvailRateRS
    Dim RS As New OTA_VehRetResRS
    Public Enum TypeOfSummary
        ExsitingBooking = 0
        NewBooking = 1
    End Enum
    Public Event [Continue](ByVal Id As String, ByVal CustomerConfirmationMode As Boolean)
    Public Event Back()
    Public Event BackToAvailability()

    Public Property Currency() As String
        Get
            Return CStr(ViewState("Currency"))
        End Get
        Set(ByVal Value As String)
            ViewState("Currency") = Value
        End Set
    End Property

    Public Property BookingId() As String
        Get
            Return CStr(ViewState("BookingID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BookingID") = Value
        End Set
    End Property

    Private Property SummaryType() As TypeOfSummary
        Get
            Return CType(ViewState("SummaryType"), TypeOfSummary)
        End Get
        Set(ByVal Value As TypeOfSummary)
            ViewState("SummaryType") = Value
        End Set
    End Property

    Private Property CustConfMode() As Boolean
        Get
            Return CBool(ViewState("CustConfMode"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("CustConfMode") = value
        End Set
    End Property

#Region "Currency Changes"
    Public Property MRU_CurrencyMultiplier() As Double
        Get
            If Session("MRU_CurrencyMultiplier") IsNot Nothing Then
                Return CDbl(Session("MRU_CurrencyMultiplier"))
            Else
                Return 0.0
            End If
        End Get
        Set(ByVal value As Double)
            Session("MRU_CurrencyMultiplier") = value
        End Set
    End Property

    Public Property MRU_CurrencyCode() As String
        Get
            Return CStr(Session("MRU_CurrencyCode"))
        End Get
        Set(ByVal value As String)
            Session("MRU_CurrencyCode") = value
        End Set
    End Property

    Public ReadOnly Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property CurrencyOptionsData() As ArrayList
        Get
            If Session("CurrencyOptionsData") IsNot Nothing Then
                Return CType(Session("CurrencyOptionsData"), ArrayList)
            Else
                Return New ArrayList
            End If
        End Get
    End Property

    Public Property ExchangeRatesLookupData() As ExchangeRatesTypeAgent()
        Get
            If Session("ExchangeRatesLookupData") IsNot Nothing Then
                Return CType(Session("ExchangeRatesLookupData"), ExchangeRatesTypeAgent())
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As ExchangeRatesTypeAgent())
            Session("ExchangeRatesLookupData") = value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblLoadError.Text = ""
    End Sub

    Public Sub InitSummary(ByVal summaryType As TypeOfSummary, ByVal Id As String, ByVal CustomerConfirmationMode As Boolean)
        ' Initialse the summary control
        Me.SummaryType = summaryType
        Me.BookingId = Id

        CustConfMode = CustomerConfirmationMode

        Try
            DisplaySummaryButtons()
            DisplayBookingSummary()
        Catch ex As Exception
            lblLoadError.Text = "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try

    End Sub

    Private Sub DisplayBookingSummary()

        ' Display booking summary depending on whether this is a new or existing booking

        Select Case Me.SummaryType
            Case TypeOfSummary.ExsitingBooking
                RetrieveExistingBooking()
                DisplayBookingDetailsExistingBooking()


                ' currency changes
                Dim sDefaultCurrencyCode As String = ""
                Dim dMultiplier As Double = 1.0

                If UserHasCurrencyOptions Then

                    If MRU_CurrencyCode = "" OrElse MRU_CurrencyMultiplier = 0.0 Then
                        ' this isnt from the price and avail screen
                        ' so figure out the default currency for the current booking's agent
                        Dim sAgentName, sAgentCode, sCurrentCountryOfTravelCurrency As String


                        ' 1. Get Agent Code
                        sAgentName = RS.VehRetResRSCore.VehReservation.VehSegmentCore.TPA_Extensions.Agent.Trim().ToUpper()
                        For Each sAgnCodeName As String In RS.VehRetResRSInfo.TPA_Extensions
                            If sAgnCodeName.Trim().ToUpper().Split("|"c)(1) = sAgentName Then
                                sAgentCode = sAgnCodeName.Trim().ToUpper().Split("|"c)(1)
                                Exit For
                            End If
                        Next

                        ' 2. 
                        Dim slCurrencyData As New SortedList

                        For Each oAgent As ExchangeRatesTypeAgent In ExchangeRatesLookupData
                            If oAgent.Code.ToUpper().Trim().Equals(sAgentCode) Then
                                For Each oCurr As ExchangeRatesTypeAgentCurr In oAgent.Curr
                                    If oCurr.FromCode = sCurrentCountryOfTravelCurrency Then
                                        ' this is the correct exchange rate set
                                        slCurrencyData.Add(oCurr.ToCode, oCurr.Rate)
                                    End If
                                Next
                            End If
                        Next

                        ' pick up the default currency data
                        For Each saCurrencySetting As String() In CurrencyOptionsData
                            Dim sAgnCode, sCurrencyList As String
                            sAgnCode = saCurrencySetting(0)
                            If sAgnCode.ToUpper().Trim().Equals(sAgentCode) Then
                                ' matching agent code
                                sCurrencyList = saCurrencySetting(1)
                                sDefaultCurrencyCode = saCurrencySetting(2)
                                dMultiplier = CDbl(slCurrencyData(sDefaultCurrencyCode))
                                Exit For
                            End If
                        Next
                    Else
                        ' this is from the availability page, use the MRU details
                        sDefaultCurrencyCode = MRU_CurrencyCode
                        dMultiplier = MRU_CurrencyMultiplier

                    End If

                End If
                ' currency changes

                FeesAndCharges2.InitChargesAndFees(RS, dMultiplier, sDefaultCurrencyCode, FeesAndCharges.ControlUse.RentalCharges)
                FeesAndCharges1.InitChargesAndFees(RS, dMultiplier, sDefaultCurrencyCode, FeesAndCharges.ControlUse.Fees)


                FeesAndChargesNewBooking1.Visible = False
                FeesAndCharges1.Visible = True
                FeesAndCharges2.Visible = True

            Case TypeOfSummary.NewBooking
                RetrieveNewBooking()
                DisplayBookingDetailsNewBooking()
                FeesAndChargesNewBooking1.InitFeesAndChargesNewBooking(dAvailableVehicleRS, mVendorIndex, mVehicleIndex)
                FeesAndChargesNewBooking1.Visible = True
                FeesAndCharges1.Visible = False
                FeesAndCharges2.Visible = False
        End Select

    End Sub

    Private Sub RetrieveExistingBooking()
        'Dim success As Boolean = False
        Dim RQ As New OTA_VehRetResRQ

        RQ.EchoToken = CStr(Session("SessionId"))
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        Dim OrqCoreType As New VehicleRetrieveResRQCoreType
        RQ.VehRetResRQCore = OrqCoreType

        Dim OuniqType As New UniqueID_Type
        OuniqType.Type = CStr(22)
        OuniqType.ID = Me.BookingId
        RQ.VehRetResRQCore.UniqueID = OuniqType
        Dim persontype As New PersonNameType
        persontype.Surname = "NA"
        RQ.VehRetResRQCore.PersonName = persontype

        Dim tpa As New VehicleResAdditionalInfoType
        tpa.Agent = "NA"
        tpa.AgentRef = "NA"
        tpa.Package = "NA" 'not used but required as part of the request
        tpa.Status = "NA" 'not used but required as part of the request
        RQ.VehRetResRQCore.TPA_Extensions = tpa

        General.LogDebugNotes("Start BookingSummary: " & General.SerializeVehRetResRQObject(RQ))
        RS = Proxy.VehRetRes(RQ)
        General.LogDebugNotes("End BookingSummary: " & General.SerializeVehRetResRSObject(RS) & vbCrLf & vbCrLf)


        StatusBarText = RS.Warnings(0).Tag
        UIScripts.StatusMsg(Me.Page, RS.Warnings(0).Tag)

        'Dim ser1 As XmlSerializer
        'Dim w1 As StringWriter
        'ser1 = New XmlSerializer(GetType(OTA_VehRetResRS))
        'w1 = New StringWriter
        'ser1.Serialize(w1, RS)
        'w1.ToString()

        If IsNothing(RS.Errors) Then
        Else
            If RS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblLoadError.Text &= RS.Errors(0).Tag.ToString
        End If
    End Sub


    Private Sub RetrieveNewBooking()
        'this session object was created in the availalbilityy control
        dAvailableVehicleRS = General.DeserializeVehAvailRateRSObject(CStr(Session("AvailableVehicleData")), dAvailableVehicleRS)
        Dim vehicleIndex As Integer
        Dim vendorIndex As Integer


        For vendorIndex = 0 To dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails.Length - 1
            'list all vehicles under a specific vendor
            If Not IsNothing(dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails) Then
                For vehicleIndex = 0 To dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails.Length - 1
                    If dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TPA_Extensions.AvailableVehicleId.ToString() = Me.BookingId Then
                        'use this to identify the correct booking data
                        mVehicleIndex = vehicleIndex
                        mVendorIndex = vendorIndex
                    End If
                Next
            End If
        Next
    End Sub



    Private Sub DisplaySummaryButtons()
        Select Case Me.SummaryType
            Case TypeOfSummary.ExsitingBooking
                'Want both display of data and functionality
                btnContinueNew.Visible = True
                btnBack.Visible = True

            Case TypeOfSummary.NewBooking
                'Only want to show data not functionality
                btnContinueNew.Visible = False
                btnBack.Visible = False
        End Select
    End Sub

    Private Sub DisplayBookingDetailsNewBooking()

        Dim pickupDateTime As DateTime
        Dim returnDatetime As DateTime

        Dim pickUpTime As String
        Dim returnTime As String

        Dim pickupDate As String
        Dim returnDate As String
        Dim pickUpCountryCode As String
        Dim returnCountryCode As String

        'retrieve location and times for the current booking
        pickUpCountryCode = dAvailableVehicleRS.VehAvailRSCore.VehRentalCore.PickUpLocation.LocationCode.ToString
        returnCountryCode = dAvailableVehicleRS.VehAvailRSCore.VehRentalCore.ReturnLocation.LocationCode.ToString
        pickupDateTime = dAvailableVehicleRS.VehAvailRSCore.VehRentalCore.PickUpDateTime
        returnDatetime = dAvailableVehicleRS.VehAvailRSCore.VehRentalCore.ReturnDateTime

        lblPickupTime.Text = pickupDateTime.ToString("dd\/MM\/yy\ HH\:mm")
        lblReturnTime.Text = returnDatetime.ToString("dd\/MM\/yy\ HH\:mm")

        lblVehMake.Text = dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Vehicle.VehMakeModel.Name

        If Not IsNothing(dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Vehicle.VehMakeModel.Code) Then
            lblVehMake.Text &= " " & dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Vehicle.VehMakeModel.Code
        End If

        lblPickupLocation.Text = dAvailableVehicleRS.VehAvailRSCore.VehRentalCore.PickUpLocation.CodeContext.ToString
        lblReturnLocation.Text = dAvailableVehicleRS.VehAvailRSCore.VehRentalCore.ReturnLocation.CodeContext.ToString

        lblVehSize.Text = dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Vehicle.VehType.VehicleCategory.ToString
        lblVehSize.Text &= dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailCore.Vehicle.VehClass.Size.ToString

        If Not IsNothing(dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).Vendor) Then
            lblVendor.Text = dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).Vendor.Value.ToString
        End If

    End Sub

    Private Sub DisplayBookingDetailsExistingBooking()

        Dim pickupDateTime As DateTime
        Dim returnDatetime As DateTime

        Dim pickUpTime As String
        Dim returnTime As String

        Dim pickupDate As String
        Dim returnDate As String
        Dim pickUpCountryCode As String
        Dim returnCountryCode As String

        ' Retrieve location and times for the current booking
        pickUpCountryCode = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpLocation.CodeContext.ToString
        returnCountryCode = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnLocation.CodeContext.ToString
        pickupDateTime = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpDateTime
        returnDatetime = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnDateTime

        lblPickupTime.Text = pickupDateTime.ToLongDateString
        lblReturnTime.Text = returnDatetime.ToLongDateString

        lblVehMake.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vehicle.VehMakeModel.Name

        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vehicle.VehMakeModel.Code) Then
            lblVehMake.Text &= " " & RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vehicle.VehMakeModel.Code
        End If

        lblPickupLocation.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpLocation.LocationCode.ToString
        lblReturnLocation.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnLocation.LocationCode.ToString
        lblVehSize.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vehicle.VehClass.Size.ToString
        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vendor) Then
            lblVendor.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vendor.CompanyShortName.ToString
        End If

    End Sub
    Private Sub btnContinueNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinueNew.Click
        If lblLoadError.Text = "" Then
            RaiseEvent [Continue](Me.BookingId, CustConfMode)
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        If lblLoadError.Text = "" Then
            RaiseEvent Back()
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        If lblLoadError.Text <> "" Then
            'hide summary control
            tblOuterSummary.Visible = False
        Else
            tblOuterSummary.Visible = True

        End If

    End Sub
End Class
