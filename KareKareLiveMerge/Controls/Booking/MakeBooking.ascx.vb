Imports THLAuroraWebInterface.WebServices


#Region "Notes"
'Karel Jordaan 6July2006
'redirect this page to the addtional product page 
'instead of the summary page


'--------------------------------------------------------------------------------------
'   Class:          MakeBooking
'   Description:    Allows user to confirm a booking or quote etc. Takes payment if required
'--------------------------------------------------------------------------------------
#End Region


Partial Class MakeBooking
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator5 As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents BookingSummary1 As BookingSummary
    Protected WithEvents Deposit1 As Deposit
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Public Event [Continue](ByVal newId As String)
    Public Event Back()
    Public Event BackToAvailability()

    Private Property BookingId() As String
        Get
            Return CStr(ViewState("BookingID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BookingID") = Value
        End Set
    End Property

    Private Property QuoteId() As String
        Get
            Return CStr(ViewState("QuoteId"))
        End Get
        Set(ByVal Value As String)
            ViewState("QuoteId") = Value
        End Set
    End Property

    Private Property LastQuoteId() As String
        Get
            Return CStr(ViewState("LastQuoteId"))
        End Get
        Set(ByVal Value As String)
            ViewState("LastQuoteId") = Value
        End Set
    End Property

    Private Property CurrentBookingType() As TransactionActionType
        Get
            Return CType(ViewState("CurrentBookingType"), TransactionActionType)
        End Get
        Set(ByVal Value As TransactionActionType)
            ViewState("CurrentBookingType") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblLoadError1.Text = ""
        lblLoadError2.Text = ""
    End Sub

    Private Sub LoadCountryList()
        Dim tempCountryCodes As String = "AF|AL|DZ|AS|AD|AO|AI|AQ|AG|AR|AM|AW|AU|AT|AZ|BS|BH|BD|BB|BY|BE|BZ|BJ|BM|BO|BA|BW|BV|BR|IO|BN|BG|BF|BI|BT|KH|CM|CA|CV|KY|CF|TD|CL|CN|CX|CC|CO|KM|CG|CK|CR|CI|HR|CU|CY|CZ|DK|DJ|DM|DO|TP|EC|EG|SV|GQ|ER|EE|ET|FK|FO|FJ|FI|FR|FX|GF|PF|TF|GA|GM|GE|DE|GH|GI|GR|GL|GD|GP|GU|GT|GN|GW|GY|HT|HM|HN|HK|HU|IS|IN|ID|IR|IQ|IE|IL|IT|JM|JP|JO|KZ|KE|KI|KW|KG|LA|LV|LB|LS|LR|LY|LI|LT|LU|MO|MK|MG|MW|MY|MV|ML|MT|MH|MQ|MR|MU|YT|MX|FM|MD|MC|MN|MS|MA|MZ|MM|NA|NR|NP|NL|AN|NC|NZ|NI|NE|NG|NU|NF|KP|MP|NO|OM|PK|PW|PA|PG|PY|PE|PH|PN|PL|PT|PR|QA|RE|RO|RU|RW|SH|KN|LC|PM|VC|WS|SM|ST|SA|SN|SC|SG|SK|SI|SB|SO|ZA|GS|KR|ES|SL|SD|SR|SJ|SE|CH|SY|TI|TW|TJ|TZ|TH|TG|TK|TO|TT|TN|TR|TM|TC|TV|UG|UA|AE|UK|US|UY|UM|UZ|VU|VA|VE|VN|VG|VI|WF|EH|YE|YU|ZR|ZM|ZW"
        Dim tempCountryDescription As String = "Afghanistan|Albania|Algeria|American Samoa|Andora|Angola|Anguilla|Antarctica|Antigua & Barbados|Argentina|Armenia|Aruba|Australia|Austria|Azerbaijan|Bahamas|Bahrain|Bangladesh|Barbados|Belarus|Belgium|Belize|Benin|Bermuda|Bolivia|Bosnia and Herzegovina|Botswana|Bouvet Island|Brazil|British Indian Ocean Territory|Brunei|Bulgaria|Burkina Faso|Burundi|Butan|Cambodia|Cameroon|Canada|Cape Verde|Cayman Islands|Cental African Republic|Chad|Chile|China|Christmas Island|Cocos (Keeling) Island|Colombia|Comoros|Congo|Cook Island|Costa Rica|Cote D'ivoire|Croatia|Cuba|Cyprus|Czech Republic|Denmark|DjiboutiI|Dominica|Dominican Republic|East Timor|Ecuador|Egypt|El Salvador|Equatorial Guinea|Eritrea|Estonia|Ethiopia|Falkland Islands|Faroe Islands|Fiji|Finland|France|France, Metropolitan|French Guiana|French Polynesia|French Souther Territories|Gabon|Gambia|Georgia|Germany|Ghana|Gibraltar|Greece|Greenland|Grenada|Guadeloupe|Guam|Guatemala|Guinea|Guinea-Bissau|Guyana|Haiti|Heard and McDonald Islands|Honduras|Hong Kong|Hungary|Iceland|India|Indonesia|Iran|Iraq|Ireland|Israel|Italy|Jamaica|Japan|Jordan|Kazakhstan|Kenya|Kiribati|Kuwait|Kyrgyzstan|Lao Peoples Democratic Republic|Latvia|Lebanon|Lesotho|Liberia|Libya|Liechtenstein|Lithuania|Luxembourg|Macau|Macedonia|Madagascar|Malawi|Malaysia|Maldives|Mali|Malta|Marshall Islands|Martinique|Mauritania|Mauritius|Mayotte|Mexico|Micronesia, Federated States of|Moldova|Monaco|Mongolia|Montserrat|Morocco|Mozambique|Myanamar|Namibia|Nauru|Nepal|Netherlands|Netherlands Antilles|New Caledonia|New Zealand|Nicaragua|Niger|Nigeria|Niue|Norfolk Island|North Korea|Northern Mariana Islands|Norway|Oman|Pakistan|Palau|Panama|Papua New Guinea|Paraguay|Peru|Philippines|Pitcairn Islands|Poland|Portugal|Puerto Rico|Qatar|Reunion|Romania|Russian Federation|Rwanda|Saint Helena|Saint Kitts and Nevis|Saint Lucia|Saint Pierre and Miquelon|Saint Vincent and The Grenadines|Samoa|San Marino|Sao Tome and Principe|Saudi Arabia|Senegal|Seychelles|Singapore|Slovakia|Slovenia|Solomon Islands|Somalia|South Africa|South Georgia|South Korea|Spain|Sri Lanka|Sudan|Suriname|Svalbardand Jan Mayen Islands|Sweden|Switzerland|Syria|Tahiti|Taiwan|Tajikistan|Tanzania|Thailand|Togo|Tokelau|Tonga|Trinidad and Tobago|Tunisia|Turkey|Turkmenistan|Turks and Caicos Islands|Tuvalu|Uganda|Ukraine|United Arab Emirates|United Kingdom|United States Of America|Uruguay|US Minor Outlying Islands|Uzbekistan|Vanuatu|Vatican City|Venezuala|Vietnam|Virgin Islands (GB)|Virgin Islands (US)|Wallis and Futuna Islands|Western Sahara|Yemen|Yugoslavia|Zaire|Zambia|Zimbabwe"
        Dim delimStr As String = "|"
        Dim delimiter As Char() = delimStr.ToCharArray()
        Dim arrCountryCodes() As String = tempCountryCodes.Split(delimiter)
        Dim arrCountryDescription() As String = tempCountryDescription.Split(delimiter)

        Dim countryIndex As Integer
        ddlCountry.Items.Clear()

        ' Populate country dropdown list using full country names as text and codes as value
        For countryIndex = 0 To arrCountryCodes.Length - 1
            Dim newListItem As New ListItem(arrCountryDescription(countryIndex).ToString, arrCountryCodes(countryIndex).ToString)
            ddlCountry.Items.Add(newListItem)
        Next
        ' Insert a blank entry at the top of the dropw down list
        ddlCountry.Items.Insert(0, " ")
    End Sub

    Private Sub btnContinueAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinueAdd.Click
        ' Add this reservation to an existing one
        ' Check booking no exists
        txtTabStateValue.Text = "Add"
        If lblLoadError1.Text = "" And lblLoadError2.Text = "" Then
            If txtBookingNo.Text.Trim <> "" And ValidateForm("Add") = True Then
                Try
                    If MakeReservation(txtBookingNo.Text.Trim, txtAgentRefNo.Text) = True Then
                        Me.LastQuoteId = Me.QuoteId
                        If lblLoadError1.Text = "" And lblLoadError2.Text = "" Then
                            ' Reservation successful
                            RaiseEvent [Continue](Me.BookingId)
                        End If
                    End If
                Catch ex As Exception
                    lblLoadError1.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
                    lblLoadError2.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
                End Try
            End If
        End If
    End Sub

    Private Sub btnContinueNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinueNew.Click
        ' Make a new reservation
        '  Make sure an error hasn't already occurred
        txtTabStateValue.Text = "New"
        If lblLoadError1.Text = "" And lblLoadError2.Text = "" Then
            If Me.QuoteId = Me.LastQuoteId Then
                ' The user has pressed the refresh button, don't make this reservation
                lblLoadError1.Text &= "This booking has already been processed"
                lblLoadError2.Text &= "This booking has already been processed"
            Else
                ' Make sure all required fields are filled out
                If ValidateForm("") = True Then
                    Try
                        If MakeReservation() = True Then
                            Me.LastQuoteId = Me.QuoteId
                            If lblLoadError1.Text = "" And lblLoadError2.Text = "" Then
                                ' Reservation successful
                                RaiseEvent [Continue](Me.BookingId)
                            End If
                        End If

                        'Karel Jordaan Issue #009 added
                        'redirect to add additional productspage
                        'redirect to self and load the 


                    Catch ex As Exception
                        lblLoadError1.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
                        lblLoadError2.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
                    End Try

                End If
            End If
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        ' Want to go back to price and availabilty
        RaiseEvent BackToAvailability()
    End Sub

    Public Sub InitMakeBooking(ByVal Id As String, ByVal bookingType As TransactionActionType, ByVal CustomerConfirmationMode As Boolean)
        LoadCountryList()
        Me.QuoteId = Id
        Me.CurrentBookingType = bookingType
        'Load and display booking summary
        BookingSummary1.InitSummary(BookingSummary.TypeOfSummary.NewBooking, Id, CustomerConfirmationMode)
        ' Only grab payment details if the user is a customer
        If CStr(Session("TypeOfUser")) = "customer" Then
            SetPaymentDetails()
        End If
        ' Set up page according to user
        DisplayByUser()
    End Sub

    Private Sub SetPaymentDetails()
        Dim AvailableVehicleRS As OTA_VehAvailRateRS
        AvailableVehicleRS = General.DeserializeVehAvailRateRSObject(CStr(Session("AvailableVehicleData")), AvailableVehicleRS)
        Dim vehicleIndex As Integer
        Dim vendorIndex As Integer
        Dim mVehicleIndex As Integer
        Dim mVendorIndex As Integer

        For vendorIndex = 0 To AvailableVehicleRS.VehAvailRSCore.VehVendorAvails.Length - 1
            'List all vehicles under a specific vendor
            If Not IsNothing(AvailableVehicleRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails) Then
                For vehicleIndex = 0 To AvailableVehicleRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails.Length - 1
                    If AvailableVehicleRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TPA_Extensions.AvailableVehicleId.ToString() = Me.BookingId Then
                        ' Use this to identify the correct booking data
                        ' Get the index of the correct booking id
                        mVehicleIndex = vehicleIndex
                        mVendorIndex = vendorIndex
                    End If
                Next
            End If
        Next
        ' Deposit is always the first item under payment rules for a particular vehicle
        Deposit1.InitDeposit()
        Deposit1.Deposit = AvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailInfo.TPA_Extensions.PaymentRules(0).Amount.ToString("c")
        Deposit1.DepositCurrency = AvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailInfo.TPA_Extensions.PaymentRules(0).CurrencyCode.ToString

        ' Fullamount is always the second item under payment rules for a particular vehicle
        Deposit1.FullAmount = AvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailInfo.TPA_Extensions.PaymentRules(1).Amount.ToString("C")
        Deposit1.FullAmountCurrency = AvailableVehicleRS.VehAvailRSCore.VehVendorAvails(mVendorIndex).VehAvails(mVehicleIndex).VehAvailInfo.TPA_Extensions.PaymentRules(1).CurrencyCode.ToString
    End Sub

    Private Sub DisplayByUser()
        Deposit1.Visible = False
        'Select Case Session("TypeOfUser")
        '    Case "agent"
        '        'hide payment details
        '        Deposit1.Visible = False
        '    Case "customer"
        '        'show payment details
        '        Deposit1.Visible = True
        'End Select
    End Sub
    Private Function MakeReservation(Optional ByVal existingBookingId As String = "", Optional ByVal agentRef As String = "") As Boolean
        Dim success As Boolean = True
        ' All of the following objects are required according to the OTA standards,
        ' however only the BookingId, firstname and surname are used by Aurora to process the booking
        ' so dummy data can be entered to fill OTA criteria
        ' VehicleRentalCore and Customer objects are required

        ' There is the option of pasing the existing booking id and agent ref no in if adding a reservation
        ' to an existing reservatiion

        Dim RQ As New OTA_VehResRQ
        Dim RS As New OTA_VehResRS
        ' Set request header data
        RQ.Version = CDec(1.0)                                        'M
        RQ.TimeStamp = Date.Now                                 'M
        RQ.Target = OTA_VehAvailRateRQTarget.Test               'M
        'RQ.EchoToken = CStr(Session("SessionId"))
        RQ.EchoToken = Me.QuoteId

        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        RQ.VehResRQCore = New VehicleReservationRQCoreType

        'this is a new booking
        Dim tpa As New VehResRQCoreExtensionType
        tpa.ReserveType = Me.CurrentBookingType
        '--- Pass passenger numbers from B2B to Aurora
        '--- 8.7.11
        '--- START
        'If existingBookingId = "" Then
        tpa.THL_NoOfAdults = ddlAdultsNo.SelectedValue
        tpa.THL_NoOfChildren = ddlChildrenNo.SelectedValue
        tpa.THL_NoOfInfants = ddlInfantsNo.SelectedValue
        'Else
        '   tpa.THL_NoOfAdults = "0"
        '  tpa.THL_NoOfChildren = "0"
        ' tpa.THL_NoOfInfants = "0"
        'End If
        '--- END

        RQ.VehResRQCore.TPA_Extensions = tpa

        'Date and location is required
        Dim vrc As New VehicleRentalCoreType
        Dim pickUpDate As New Date(2003, 12, 12, 7, 0, 0)
        Dim returnDate As New Date(2003, 12, 25, 13, 0, 0)

        vrc.PickUpDateTime = pickUpDate         'M
        vrc.ReturnDateTime = returnDate         'M
        vrc.PickUpLocation = New LocationType("AKL", "AKL")  'M
        vrc.ReturnLocation = New LocationType("AKL", "AKL")  'M
        RQ.VehResRQCore.VehRentalCore = vrc

        'customer data required
        Dim cusPriAddTy As New CustomerPrimaryAdditionalType
        Dim VCusType As New VehicleCustomerType
        Dim SN As New StreetNmbrType
        SN.PO_Box = "NA"

        Dim Obj_AIT As New AddressInfoType

        With Obj_AIT
            .StreetNmbr = SN
            .BldgRoom = "NA"
            Dim addressLine1 As String
            Dim addressLine2 As String

            If txtAddressLine1.Text.Trim = "" Then
                addressLine1 = "NA"
            Else
                addressLine1 = txtAddressLine1.Text
            End If

            If txtAddressLine2.Text.Trim = "" Then
                addressLine2 = "NA"
            Else
                addressLine2 = txtAddressLine2.Text
            End If

            .AddressLine = New String() {addressLine1, addressLine2}
            .PostalCode = "NA"

            Dim city As String

            If txtCity.Text.Trim = "" Then
                city = "NA"
            Else
                city = txtCity.Text
            End If
            .CityName = city

            Dim country As String
            If ddlCountry.SelectedItem.Value = "" Then
                country = "NA"
            Else
                country = ddlCountry.SelectedItem.Value
            End If
            .County = country
        End With

        Dim PNT As New PersonNameType
        ' Firstname can only be empty if this process was called by the add to existing booking routine
        If txtFirstName.Text.Trim = "" Then
            PNT.GivenName = New String() {"NA"}
            PNT.NameTitle = New String() {"NA"}
        Else
            PNT.GivenName = New String() {txtFirstName.Text}
            PNT.NameTitle = New String() {ddlTitle.SelectedItem.Text}
        End If

        PNT.MiddleName = New String() {"NA"}

        If txtSurname.Text.Trim = "" Then
            PNT.Surname = "NA"
        Else
            PNT.Surname = txtSurname.Text
        End If

        Dim TIT As New TelephoneInfoType
        Dim EType As New EmailType
        Dim CCT As New CitizenCountryNameType
        With VCusType
            .AddAddress(Obj_AIT)
            TIT.PhoneNumber = "NA"
            .Telephone = New TelephoneInfoType() {TIT}
            If txtEmailAddress.Text.Trim = "" Then
                EType.Value = "NA"
            Else
                EType.Value = txtEmailAddress.Text
            End If
            .Email = New EmailType() {EType}
            CCT.Code = "NZ"
            .CitizenCountryName = New CitizenCountryNameType() {CCT}
            .PersonName = PNT
        End With

        cusPriAddTy.Primary = VCusType
        cusPriAddTy.Primary.BirthDate = Date.Now
        RQ.VehResRQCore.Customer = cusPriAddTy

        Dim CPA As New CustomerPrimaryAdditionalType

        RQ.VehResRQInfo = New VehicleReservationRQAdditionalInfoType


        Dim ReferenceType As New VehicleReservationRQAdditionalInfoTypeReference
        ' To Add the Note the Booking
        If existingBookingId <> "" Then
            If txtRentalNote.Text <> "" Then
                Dim ObjNotes(1) As VehicleSpecialReqPrefType
                ObjNotes(0) = New VehicleSpecialReqPrefType
                ObjNotes(0).Value = txtRentalNote.Text
                RQ.VehResRQInfo.SpecialReqPref = ObjNotes
            End If
        Else
            If txtRentalNoteNew.Text <> "" Then
                Dim ObjNotes(1) As VehicleSpecialReqPrefType
                ObjNotes(0) = New VehicleSpecialReqPrefType
                ObjNotes(0).Value = txtRentalNoteNew.Text
                RQ.VehResRQInfo.SpecialReqPref = ObjNotes
            End If
        End If

        'ReferenceType.ID = Me.QuoteId       '-- Commented
        'ReferenceType.Type = "1"
        'ReferenceType.ID_Context = "NA"

        If existingBookingId <> "" Then
            ' Add to existing booking
            ReferenceType.ID = existingBookingId    'Me.QuoteId 
            'ReferenceType.Instance = ""
            'Dim ObjExistingBookingRef As New ExistingBookingRefType
            'ObjExistingBookingRef.ExistingBookingRef = existingBookingId
            'RQ.VehResRQInfo.TPA_Extensions = ObjExistingBookingRef
            If agentRef = "" Then
                'Customers don't have to enter an agent reference
                ReferenceType.ID_Context = "NA"
            Else
                ReferenceType.ID_Context = agentRef
            End If

            ReferenceType.Type = "1"
            'ReferenceType.ID = Me.QuoteId                              'O

            RQ.VehResRQInfo.Reference = ReferenceType
        ElseIf txtAgentRefNoNew.Text <> "" Then
            ' Agent Ref has been entered, pass it back
            ReferenceType.ID = "NA"
            ReferenceType.ID_Context = txtAgentRefNoNew.Text
            ReferenceType.Type = "1"
            RQ.VehResRQInfo.Reference = ReferenceType
        End If

        '************************************ Extra Hire Item *******************************
        'Dim sProductList, sQtyList As String
        'Dim noOfProducts As Integer = 0
        'sProductList = txtProductList.text
        'sQtyList = txtQtyList.Text

        'If sProductList.Trim <> "" Then
        '    Dim VehEqPref() As VehicleEquipmentPrefType
        '    If sProductList.IndexOf(",") > 0 And sProductList.Trim <> "" Then
        '        noOfProducts = sProductList.Split(CChar(",")).Length
        '    End If
        '    Dim I As Integer
        '    If noOfProducts > 0 Then
        '        For I = 0 To noOfProducts - 1
        '            ReDim Preserve VehEqPref(I)
        '            VehEqPref(I) = New VehicleEquipmentPrefType
        '            VehEqPref(I).EquipType = sProductList.Split(CChar(","))(I)
        '            VehEqPref(I).Quantity = sQtyList.Split(CChar(","))(I)
        '        Next
        '    Else
        '        ReDim Preserve VehEqPref(0)
        '        VehEqPref(0) = New VehicleEquipmentPrefType
        '        VehEqPref(0).EquipType = sProductList
        '        VehEqPref(0).Quantity = sQtyList
        '    End If

        '    RQ.VehResRQCore.SpecialEquipPrefs = VehEqPref
        'End If


        ''******************* RKS 06-July-2006 Changes for Extra product *********************
        'Dim oFeeVehChargePurTyp() As THL_VehicleChargePurposeType
        'Dim oChargeVehChaTypCalc() As THL_VehicleChargeTypeCalculation
        'Dim OVehSegCoreType As New THL_VehicleSegmentCoreType
        'Dim OvehResType As New THL_VehicleReservationType
        'Dim NoOfDays As Long = DateDiff(DateInterval.Day, CDate(pickUpDate), CDate(returnDate))
        'Dim currentIndex As Integer

        'ReDim Preserve oFeeVehChargePurTyp(currentIndex)
        'oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
        'oFeeVehChargePurTyp(currentIndex).Description = "ER1"
        'oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
        'oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
        'oFeeVehChargePurTyp(currentIndex).Amount = NoOfDays * 20
        'oFeeVehChargePurTyp(currentIndex).Purpose = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_Quantity = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_UnitName = "Day"

        'ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
        'oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
        'oChargeVehChaTypCalc(currentIndex + 1).Quantity = NoOfDays.ToString()
        'oChargeVehChaTypCalc(currentIndex + 1).UnitName = "Day"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 20
        'oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
        'currentIndex = currentIndex + 1

        'ReDim Preserve oFeeVehChargePurTyp(currentIndex)
        'oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
        'oFeeVehChargePurTyp(currentIndex).Description = "CHAIR"
        'oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
        'oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
        'oFeeVehChargePurTyp(currentIndex).Amount = CInt(2) * 12
        'oFeeVehChargePurTyp(currentIndex).Purpose = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_Quantity = "2"
        'oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

        'ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
        'oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
        'oChargeVehChaTypCalc(currentIndex + 1).Quantity = "2"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 12

        'oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
        'currentIndex = currentIndex + 1

        'OVehSegCoreType.Fees = oFeeVehChargePurTyp
        'OVehSegCoreType.RentalRate = New THL_VehicleSegmentCoreTypeRentalRate
        ''OVehSegCoreType.RentalRate = oCoreTypRentRate
        'OvehResType.VehSegmentCore = New THL_VehicleSegmentCoreType
        'OvehResType.VehSegmentCore = OVehSegCoreType
        'RQ.CreateBookingRQInfoCore = New THL_VehicleRetrieveResRSCoreType
        'RQ.CreateBookingRQInfoCore.VehReservation = New THL_VehicleReservationType
        'RQ.CreateBookingRQInfoCore.VehReservation = OvehResType
        ''**************************** END ***************************************************

        'RQ.VehResRQInfo.Reference = ReferenceType
        '-----------------------payment details------------------------------------------

        'If CStr(Session("TypeOfUser")) = "customer" Then
        '    Dim oPaymentCardType As New PaymentCardType
        '    oPaymentCardType.CardHolderName = Deposit1.NameHolder
        '    oPaymentCardType.CardNumber = Deposit1.CardNumber
        '    oPaymentCardType.ExpireDate = Deposit1.Expiry
        '    oPaymentCardType.CardType = "1"
        '    oPaymentCardType.SeriesCode = Deposit1.SeriesCode


        '    Dim oBankAcctType As New BankAcctType

        '    oBankAcctType.BankID = "bankid"
        '    oBankAcctType.BankAcctName = "BankAcctName"


        '    Dim oDirectBillType As New DirectBillType
        '    Dim oDirectBillTypeCompanyName As New DirectBillTypeCompanyName
        '    Dim oAddressInfotype As New AddressInfoType
        '    oDirectBillTypeCompanyName.ContactName = "Tourism Holding Limited"
        '    oDirectBillType.CompanyName = oDirectBillTypeCompanyName
        '    oAddressInfotype.AddressLine = New String() {"#6/12", "Eldon Road"}
        '    oAddressInfotype.CityName = "Auckalnd"
        '    oAddressInfotype.County = "New Zealand"
        '    oAddressInfotype.PostalCode = "1003"
        '    oDirectBillType.Address = oAddressInfotype
        '    oDirectBillType.DirectBill_ID = "123456007"

        '    Dim oPaymentFrType(5) As PaymentFormType

        '    oPaymentFrType(0) = New PaymentFormType
        '    oPaymentFrType(0).Item = GetType(PaymentCardType)
        '    oPaymentFrType(0).Item = oPaymentCardType

        '    oPaymentFrType(1) = New PaymentFormType
        '    oPaymentFrType(1).Item = GetType(BankAcctType)
        '    oPaymentFrType(1).Item = oBankAcctType

        '    oPaymentFrType(2) = New PaymentFormType
        '    oPaymentFrType(2).Item = GetType(DirectBillType)
        '    oPaymentFrType(2).Item = oDirectBillType

        '    RQ.CreateBookingRQInfo.RentalPaymentPref = oPaymentFrType

        'End If
        '-----------------------------------------------------------------------------------
        'Dim ser1 As XmlSerializer
        'Dim w1 As StringWriter

        'ser1 = New XmlSerializer(GetType(THL_CreateBookingRQ))
        'w1 = New StringWriter
        'ser1.Serialize(w1, RQ)
        'Dim sString As String = w1.ToString()

        RS = Proxy.VehRes(RQ)


        If IsNothing(RS.Errors) Then
            Me.BookingId = RS.VehResRSCore.VehReservation.VehSegmentCore.ConfID.ID.ToString

            Return success    'if no errors, otherwise display errors.
        Else
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
                lblLoadError1.Text &= RS.Errors(errorIndex).Tag & "<br>"
                lblLoadError2.Text &= RS.Errors(errorIndex).Tag & "<br>"

                If RS.Errors(errorIndex).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                    Session("POS") = Nothing
                    Response.Redirect("AccessDenied.aspx")
                End If
            Next

        End If
    End Function

    Private Function MakeReservationNew(Optional ByVal existingBookingId As String = "", Optional ByVal agentRef As String = "") As Boolean
        Dim success As Boolean = True
        ' All of the following objects are required according to the OTA standards,
        ' however only the BookingId, firstname and surname are used by Aurora to process the booking
        ' so dummy data can be entered to fill OTA criteria
        ' VehicleRentalCore and Customer objects are required

        ' There is the option of pasing the existing booking id and agent ref no in if adding a reservation
        ' to an existing reservatiion


        Dim otaBase As String = System.Web.Configuration.WebConfigurationManager.AppSettings("AuroraOTABase")
        Dim otaProxy As New Aurora_OTAV3(otaBase)


        Dim RQ As New OTA_VehResRQV3
        Dim RS As New OTA_VehResRSV3
        ' Set request header data
        RQ.Version = CDec(1.0)                                        'M
        RQ.TimeStamp = Date.Now                                 'M
        RQ.Target = OTA_VehAvailRateRQTarget.Test               'M
        'RQ.EchoToken = CStr(Session("SessionId"))
        RQ.EchoToken = Me.QuoteId

        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        RQ.VehResRQCore = New VehicleReservationRQCoreType

        'this is a new booking
        Dim tpa As New VehResRQCoreExtensionType
        tpa.ReserveType = Me.CurrentBookingType
        '--- Pass passenger numbers from B2B to Aurora
        '--- 8.7.11
        '--- START
        'If existingBookingId = "" Then
        tpa.THL_NoOfAdults = ddlAdultsNo.SelectedValue
        tpa.THL_NoOfChildren = ddlChildrenNo.SelectedValue
        tpa.THL_NoOfInfants = ddlInfantsNo.SelectedValue
        'Else
        '   tpa.THL_NoOfAdults = "0"
        '  tpa.THL_NoOfChildren = "0"
        ' tpa.THL_NoOfInfants = "0"
        'End If
        '--- END

        RQ.VehResRQCore.TPA_Extensions = tpa

        'Date and location is required
        Dim vrc As New VehicleRentalCoreType
        Dim pickUpDate As New Date(2003, 12, 12, 7, 0, 0)
        Dim returnDate As New Date(2003, 12, 25, 13, 0, 0)

        vrc.PickUpDateTime = pickUpDate         'M
        vrc.ReturnDateTime = returnDate         'M
        vrc.PickUpLocation = New LocationType("AKL", "AKL")  'M
        vrc.ReturnLocation = New LocationType("AKL", "AKL")  'M
        RQ.VehResRQCore.VehRentalCore = vrc

        'customer data required
        Dim cusPriAddTy As New CustomerPrimaryAdditionalType
        Dim VCusType As New VehicleCustomerType
        Dim SN As New StreetNmbrType
        SN.PO_Box = "NA"

        Dim Obj_AIT As New AddressInfoType

        With Obj_AIT
            .StreetNmbr = SN
            .BldgRoom = "NA"
            Dim addressLine1 As String
            Dim addressLine2 As String

            If txtAddressLine1.Text.Trim = "" Then
                addressLine1 = "NA"
            Else
                addressLine1 = txtAddressLine1.Text
            End If

            If txtAddressLine2.Text.Trim = "" Then
                addressLine2 = "NA"
            Else
                addressLine2 = txtAddressLine2.Text
            End If

            .AddressLine = New String() {addressLine1, addressLine2}
            .PostalCode = "NA"

            Dim city As String

            If txtCity.Text.Trim = "" Then
                city = "NA"
            Else
                city = txtCity.Text
            End If
            .CityName = city

            Dim country As String
            If ddlCountry.SelectedItem.Value = "" Then
                country = "NA"
            Else
                country = ddlCountry.SelectedItem.Value
            End If
            .County = country
        End With

        Dim PNT As New PersonNameType
        ' Firstname can only be empty if this process was called by the add to existing booking routine
        If txtFirstName.Text.Trim = "" Then
            PNT.GivenName = New String() {"NA"}
            PNT.NameTitle = New String() {"NA"}
        Else
            PNT.GivenName = New String() {txtFirstName.Text}
            PNT.NameTitle = New String() {ddlTitle.SelectedItem.Text}
        End If

        PNT.MiddleName = New String() {"NA"}

        If txtSurname.Text.Trim = "" Then
            PNT.Surname = "NA"
        Else
            PNT.Surname = txtSurname.Text
        End If

        Dim TIT As New TelephoneInfoType
        Dim EType As New EmailType
        Dim CCT As New CitizenCountryNameType
        With VCusType
            .AddAddress(Obj_AIT)
            TIT.PhoneNumber = "NA"
            .Telephone = New TelephoneInfoType() {TIT}
            If txtEmailAddress.Text.Trim = "" Then
                EType.Value = "NA"
            Else
                EType.Value = txtEmailAddress.Text
            End If
            .Email = New EmailType() {EType}
            CCT.Code = "NZ"
            .CitizenCountryName = New CitizenCountryNameType() {CCT}
            .PersonName = PNT
        End With

        cusPriAddTy.Primary = VCusType
        cusPriAddTy.Primary.BirthDate = Date.Now
        RQ.VehResRQCore.Customer = cusPriAddTy

        Dim CPA As New CustomerPrimaryAdditionalType

        RQ.VehResRQInfo = New VehicleReservationRQAdditionalInfoTypeV3
        RQ.VehResRQInfo.ArrivalDetails = New VehicleArrivalDetailsTypeV3

        'RQ.VehResRQInfo.ArrivalDetails.ArrivalDateTime = DateTime.Now.AddDays(75)
        'RQ.VehResRQInfo.ArrivalDetails.ArrivalLocation = New LocationType("AKL", "AKL")
        ''RQ.VehResRQInfo.ArrivalDetails.ArrivalLocation.LocationCode = "AKL"
        'RQ.VehResRQInfo.ArrivalDetails.Slot = ""
        'RQ.VehResRQInfo.ArrivalDetails.SlotId = "0"


        Dim ReferenceType As New VehicleReservationRQAdditionalInfoTypeReference
        ' To Add the Note the Booking
        If existingBookingId <> "" Then
            If txtRentalNote.Text <> "" Then
                Dim ObjNotes(1) As VehicleSpecialReqPrefType
                ObjNotes(0) = New VehicleSpecialReqPrefType
                ObjNotes(0).Value = txtRentalNote.Text
                RQ.VehResRQInfo.SpecialReqPref = ObjNotes
            End If
        Else
            If txtRentalNoteNew.Text <> "" Then
                Dim ObjNotes(1) As VehicleSpecialReqPrefType
                ObjNotes(0) = New VehicleSpecialReqPrefType
                ObjNotes(0).Value = txtRentalNoteNew.Text
                RQ.VehResRQInfo.SpecialReqPref = ObjNotes
            End If
        End If

        'ReferenceType.ID = Me.QuoteId       '-- Commented
        'ReferenceType.Type = "1"
        'ReferenceType.ID_Context = "NA"

        If existingBookingId <> "" Then
            ' Add to existing booking
            ReferenceType.ID = existingBookingId    'Me.QuoteId 
            'ReferenceType.Instance = ""
            'Dim ObjExistingBookingRef As New ExistingBookingRefType
            'ObjExistingBookingRef.ExistingBookingRef = existingBookingId
            'RQ.VehResRQInfo.TPA_Extensions = ObjExistingBookingRef
            If agentRef = "" Then
                'Customers don't have to enter an agent reference
                ReferenceType.ID_Context = "NA"
            Else
                ReferenceType.ID_Context = agentRef
            End If

            ReferenceType.Type = "1"
            'ReferenceType.ID = Me.QuoteId                              'O

            RQ.VehResRQInfo.Reference = ReferenceType
        ElseIf txtAgentRefNoNew.Text <> "" Then
            ' Agent Ref has been entered, pass it back
            ReferenceType.ID = "NA"
            ReferenceType.ID_Context = txtAgentRefNoNew.Text
            ReferenceType.Type = "1"
            RQ.VehResRQInfo.Reference = ReferenceType
        End If


        'RQ.VehResRQInfo = New VehicleReservationRQAdditionalInfoTypeV3
        RQ.VehResRQInfo.ArrivalDetails = New VehicleArrivalDetailsTypeV3

        RQ.VehResRQInfo.ArrivalDetails.ArrivalDateTime = DateTime.Now.AddDays(75)
        RQ.VehResRQInfo.ArrivalDetails.ArrivalDateTimeSpecified = True
        RQ.VehResRQInfo.ArrivalDetails.ArrivalLocation = New LocationType("AKL", "AKL")
        ''RQ.VehResRQInfo.ArrivalDetails.ArrivalLocation.LocationCode = "AKL"
        RQ.VehResRQInfo.ArrivalDetails.Slot = "12:00 pm - 02:00 pm"
        RQ.VehResRQInfo.ArrivalDetails.SlotId = "3"

        'RQ.VehResRQInfo.RentalPaymentPref = New PaymentFormType()




        '************************************ Extra Hire Item *******************************
        'Dim sProductList, sQtyList As String
        'Dim noOfProducts As Integer = 0
        'sProductList = txtProductList.text
        'sQtyList = txtQtyList.Text

        'If sProductList.Trim <> "" Then
        '    Dim VehEqPref() As VehicleEquipmentPrefType
        '    If sProductList.IndexOf(",") > 0 And sProductList.Trim <> "" Then
        '        noOfProducts = sProductList.Split(CChar(",")).Length
        '    End If
        '    Dim I As Integer
        '    If noOfProducts > 0 Then
        '        For I = 0 To noOfProducts - 1
        '            ReDim Preserve VehEqPref(I)
        '            VehEqPref(I) = New VehicleEquipmentPrefType
        '            VehEqPref(I).EquipType = sProductList.Split(CChar(","))(I)
        '            VehEqPref(I).Quantity = sQtyList.Split(CChar(","))(I)
        '        Next
        '    Else
        '        ReDim Preserve VehEqPref(0)
        '        VehEqPref(0) = New VehicleEquipmentPrefType
        '        VehEqPref(0).EquipType = sProductList
        '        VehEqPref(0).Quantity = sQtyList
        '    End If

        '    RQ.VehResRQCore.SpecialEquipPrefs = VehEqPref
        'End If


        ''******************* RKS 06-July-2006 Changes for Extra product *********************
        'Dim oFeeVehChargePurTyp() As THL_VehicleChargePurposeType
        'Dim oChargeVehChaTypCalc() As THL_VehicleChargeTypeCalculation
        'Dim OVehSegCoreType As New THL_VehicleSegmentCoreType
        'Dim OvehResType As New THL_VehicleReservationType
        'Dim NoOfDays As Long = DateDiff(DateInterval.Day, CDate(pickUpDate), CDate(returnDate))
        'Dim currentIndex As Integer

        'ReDim Preserve oFeeVehChargePurTyp(currentIndex)
        'oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
        'oFeeVehChargePurTyp(currentIndex).Description = "ER1"
        'oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
        'oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
        'oFeeVehChargePurTyp(currentIndex).Amount = NoOfDays * 20
        'oFeeVehChargePurTyp(currentIndex).Purpose = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_Quantity = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_UnitName = "Day"

        'ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
        'oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
        'oChargeVehChaTypCalc(currentIndex + 1).Quantity = NoOfDays.ToString()
        'oChargeVehChaTypCalc(currentIndex + 1).UnitName = "Day"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 20
        'oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
        'currentIndex = currentIndex + 1

        'ReDim Preserve oFeeVehChargePurTyp(currentIndex)
        'oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
        'oFeeVehChargePurTyp(currentIndex).Description = "CHAIR"
        'oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
        'oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
        'oFeeVehChargePurTyp(currentIndex).Amount = CInt(2) * 12
        'oFeeVehChargePurTyp(currentIndex).Purpose = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_Quantity = "2"
        'oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

        'ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
        'oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
        'oChargeVehChaTypCalc(currentIndex + 1).Quantity = "2"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 12

        'oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
        'currentIndex = currentIndex + 1

        'OVehSegCoreType.Fees = oFeeVehChargePurTyp
        'OVehSegCoreType.RentalRate = New THL_VehicleSegmentCoreTypeRentalRate
        ''OVehSegCoreType.RentalRate = oCoreTypRentRate
        'OvehResType.VehSegmentCore = New THL_VehicleSegmentCoreType
        'OvehResType.VehSegmentCore = OVehSegCoreType
        'RQ.CreateBookingRQInfoCore = New THL_VehicleRetrieveResRSCoreType
        'RQ.CreateBookingRQInfoCore.VehReservation = New THL_VehicleReservationType
        'RQ.CreateBookingRQInfoCore.VehReservation = OvehResType
        ''**************************** END ***************************************************

        'RQ.VehResRQInfo.Reference = ReferenceType
        '-----------------------payment details------------------------------------------

        'If CStr(Session("TypeOfUser")) = "customer" Then
        '    Dim oPaymentCardType As New PaymentCardType
        '    oPaymentCardType.CardHolderName = Deposit1.NameHolder
        '    oPaymentCardType.CardNumber = Deposit1.CardNumber
        '    oPaymentCardType.ExpireDate = Deposit1.Expiry
        '    oPaymentCardType.CardType = "1"
        '    oPaymentCardType.SeriesCode = Deposit1.SeriesCode


        '    Dim oBankAcctType As New BankAcctType

        '    oBankAcctType.BankID = "bankid"
        '    oBankAcctType.BankAcctName = "BankAcctName"


        '    Dim oDirectBillType As New DirectBillType
        '    Dim oDirectBillTypeCompanyName As New DirectBillTypeCompanyName
        '    Dim oAddressInfotype As New AddressInfoType
        '    oDirectBillTypeCompanyName.ContactName = "Tourism Holding Limited"
        '    oDirectBillType.CompanyName = oDirectBillTypeCompanyName
        '    oAddressInfotype.AddressLine = New String() {"#6/12", "Eldon Road"}
        '    oAddressInfotype.CityName = "Auckalnd"
        '    oAddressInfotype.County = "New Zealand"
        '    oAddressInfotype.PostalCode = "1003"
        '    oDirectBillType.Address = oAddressInfotype
        '    oDirectBillType.DirectBill_ID = "123456007"

        '    Dim oPaymentFrType(5) As PaymentFormType

        '    oPaymentFrType(0) = New PaymentFormType
        '    oPaymentFrType(0).Item = GetType(PaymentCardType)
        '    oPaymentFrType(0).Item = oPaymentCardType

        '    oPaymentFrType(1) = New PaymentFormType
        '    oPaymentFrType(1).Item = GetType(BankAcctType)
        '    oPaymentFrType(1).Item = oBankAcctType

        '    oPaymentFrType(2) = New PaymentFormType
        '    oPaymentFrType(2).Item = GetType(DirectBillType)
        '    oPaymentFrType(2).Item = oDirectBillType

        '    RQ.CreateBookingRQInfo.RentalPaymentPref = oPaymentFrType

        'End If
        '-----------------------------------------------------------------------------------
        'Dim ser1 As XmlSerializer
        'Dim w1 As StringWriter

        'ser1 = New XmlSerializer(GetType(THL_CreateBookingRQ))
        'w1 = New StringWriter
        'ser1.Serialize(w1, RQ)
        'Dim sString As String = w1.ToString()

        'RS = Proxy.VehRes(RQ)
        RS = otaProxy.VehRes(RQ)



        If IsNothing(RS.Errors) Then
            Me.BookingId = RS.VehResRSCore.VehReservation.VehSegmentCore.ConfID.ID.ToString

            Return success    'if no errors, otherwise display errors.
        Else
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
                lblLoadError1.Text &= RS.Errors(errorIndex).Tag & "<br>"
                lblLoadError2.Text &= RS.Errors(errorIndex).Tag & "<br>"

                If RS.Errors(errorIndex).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                    Session("POS") = Nothing
                    Response.Redirect("AccessDenied.aspx")
                End If
            Next

        End If
    End Function
    Private Function MakeReservationOld(Optional ByVal existingBookingId As String = "", Optional ByVal agentRef As String = "") As Boolean
        Dim success As Boolean = True
        ' All of the following objects are required according to the OTA standards,
        ' however only the BookingId, firstname and surname are used by Aurora to process the booking
        ' so dummy data can be entered to fill OTA criteria
        ' VehicleRentalCore and Customer objects are required

        ' There is the option of pasing the existing booking id and agent ref no in if adding a reservation
        ' to an existing reservatiion

        Dim RQ As New OTA_VehResRQ
        Dim RS As New OTA_VehResRS
        ' Set request header data
        RQ.Version = CDec(1.0)                                        'M
        RQ.TimeStamp = Date.Now                                 'M
        RQ.Target = OTA_VehAvailRateRQTarget.Test               'M
        'RQ.EchoToken = CStr(Session("SessionId"))
        RQ.EchoToken = Me.QuoteId

        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        RQ.VehResRQCore = New VehicleReservationRQCoreType

        'this is a new booking
        Dim tpa As New VehResRQCoreExtensionType
        tpa.ReserveType = Me.CurrentBookingType
        '--- Pass passenger numbers from B2B to Aurora
        '--- 8.7.11
        '--- START
        'If existingBookingId = "" Then
        tpa.THL_NoOfAdults = ddlAdultsNo.SelectedValue
        tpa.THL_NoOfChildren = ddlChildrenNo.SelectedValue
        tpa.THL_NoOfInfants = ddlInfantsNo.SelectedValue
        'Else
        '   tpa.THL_NoOfAdults = "0"
        '  tpa.THL_NoOfChildren = "0"
        ' tpa.THL_NoOfInfants = "0"
        'End If
        '--- END

        RQ.VehResRQCore.TPA_Extensions = tpa

        'Date and location is required
        Dim vrc As New VehicleRentalCoreType
        Dim pickUpDate As New Date(2003, 12, 12, 7, 0, 0)
        Dim returnDate As New Date(2003, 12, 25, 13, 0, 0)

        vrc.PickUpDateTime = pickUpDate         'M
        vrc.ReturnDateTime = returnDate         'M
        vrc.PickUpLocation = New LocationType("AKL", "AKL")  'M
        vrc.ReturnLocation = New LocationType("AKL", "AKL")  'M
        RQ.VehResRQCore.VehRentalCore = vrc

        'customer data required
        Dim cusPriAddTy As New CustomerPrimaryAdditionalType
        Dim VCusType As New VehicleCustomerType
        Dim SN As New StreetNmbrType
        SN.PO_Box = "NA"

        Dim Obj_AIT As New AddressInfoType

        With Obj_AIT
            .StreetNmbr = SN
            .BldgRoom = "NA"
            Dim addressLine1 As String
            Dim addressLine2 As String

            If txtAddressLine1.Text.Trim = "" Then
                addressLine1 = "NA"
            Else
                addressLine1 = txtAddressLine1.Text
            End If

            If txtAddressLine2.Text.Trim = "" Then
                addressLine2 = "NA"
            Else
                addressLine2 = txtAddressLine2.Text
            End If

            .AddressLine = New String() {addressLine1, addressLine2}
            .PostalCode = "NA"

            Dim city As String

            If txtCity.Text.Trim = "" Then
                city = "NA"
            Else
                city = txtCity.Text
            End If
            .CityName = city

            Dim country As String
            If ddlCountry.SelectedItem.Value = "" Then
                country = "NA"
            Else
                country = ddlCountry.SelectedItem.Value
            End If
            .County = country
        End With

        Dim PNT As New PersonNameType
        ' Firstname can only be empty if this process was called by the add to existing booking routine
        If txtFirstName.Text.Trim = "" Then
            PNT.GivenName = New String() {"NA"}
            PNT.NameTitle = New String() {"NA"}
        Else
            PNT.GivenName = New String() {txtFirstName.Text}
            PNT.NameTitle = New String() {ddlTitle.SelectedItem.Text}
        End If

        PNT.MiddleName = New String() {"NA"}

        If txtSurname.Text.Trim = "" Then
            PNT.Surname = "NA"
        Else
            PNT.Surname = txtSurname.Text
        End If

        Dim TIT As New TelephoneInfoType
        Dim EType As New EmailType
        Dim CCT As New CitizenCountryNameType
        With VCusType
            .AddAddress(Obj_AIT)
            TIT.PhoneNumber = "NA"
            .Telephone = New TelephoneInfoType() {TIT}
            If txtEmailAddress.Text.Trim = "" Then
                EType.Value = "NA"
            Else
                EType.Value = txtEmailAddress.Text
            End If
            .Email = New EmailType() {EType}
            CCT.Code = "NZ"
            .CitizenCountryName = New CitizenCountryNameType() {CCT}
            .PersonName = PNT
        End With

        cusPriAddTy.Primary = VCusType
        cusPriAddTy.Primary.BirthDate = Date.Now
        RQ.VehResRQCore.Customer = cusPriAddTy

        Dim CPA As New CustomerPrimaryAdditionalType

        RQ.VehResRQInfo = New VehicleReservationRQAdditionalInfoType

        RQ.VehResRQInfo.ArrivalDetails.ArrivalDateTime = DateTime.Now.AddDays(75)
        RQ.VehResRQInfo.ArrivalDetails.ArrivalLocation.LocationCode = "AKL"
        


        Dim ReferenceType As New VehicleReservationRQAdditionalInfoTypeReference
        ' To Add the Note the Booking
        If existingBookingId <> "" Then
            If txtRentalNote.Text <> "" Then
                Dim ObjNotes(1) As VehicleSpecialReqPrefType
                ObjNotes(0) = New VehicleSpecialReqPrefType
                ObjNotes(0).Value = txtRentalNote.Text
                RQ.VehResRQInfo.SpecialReqPref = ObjNotes
            End If
        Else
            If txtRentalNoteNew.Text <> "" Then
                Dim ObjNotes(1) As VehicleSpecialReqPrefType
                ObjNotes(0) = New VehicleSpecialReqPrefType
                ObjNotes(0).Value = txtRentalNoteNew.Text
                RQ.VehResRQInfo.SpecialReqPref = ObjNotes
            End If
        End If

        'ReferenceType.ID = Me.QuoteId       '-- Commented
        'ReferenceType.Type = "1"
        'ReferenceType.ID_Context = "NA"

        If existingBookingId <> "" Then
            ' Add to existing booking
            ReferenceType.ID = existingBookingId    'Me.QuoteId 
            'ReferenceType.Instance = ""
            'Dim ObjExistingBookingRef As New ExistingBookingRefType
            'ObjExistingBookingRef.ExistingBookingRef = existingBookingId
            'RQ.VehResRQInfo.TPA_Extensions = ObjExistingBookingRef
            If agentRef = "" Then
                'Customers don't have to enter an agent reference
                ReferenceType.ID_Context = "NA"
            Else
                ReferenceType.ID_Context = agentRef
            End If

            ReferenceType.Type = "1"
            'ReferenceType.ID = Me.QuoteId                              'O

            RQ.VehResRQInfo.Reference = ReferenceType
        ElseIf txtAgentRefNoNew.Text <> "" Then
            ' Agent Ref has been entered, pass it back
            ReferenceType.ID = "NA"
            ReferenceType.ID_Context = txtAgentRefNoNew.Text
            ReferenceType.Type = "1"
            RQ.VehResRQInfo.Reference = ReferenceType
        End If

        '************************************ Extra Hire Item *******************************
        'Dim sProductList, sQtyList As String
        'Dim noOfProducts As Integer = 0
        'sProductList = txtProductList.text
        'sQtyList = txtQtyList.Text

        'If sProductList.Trim <> "" Then
        '    Dim VehEqPref() As VehicleEquipmentPrefType
        '    If sProductList.IndexOf(",") > 0 And sProductList.Trim <> "" Then
        '        noOfProducts = sProductList.Split(CChar(",")).Length
        '    End If
        '    Dim I As Integer
        '    If noOfProducts > 0 Then
        '        For I = 0 To noOfProducts - 1
        '            ReDim Preserve VehEqPref(I)
        '            VehEqPref(I) = New VehicleEquipmentPrefType
        '            VehEqPref(I).EquipType = sProductList.Split(CChar(","))(I)
        '            VehEqPref(I).Quantity = sQtyList.Split(CChar(","))(I)
        '        Next
        '    Else
        '        ReDim Preserve VehEqPref(0)
        '        VehEqPref(0) = New VehicleEquipmentPrefType
        '        VehEqPref(0).EquipType = sProductList
        '        VehEqPref(0).Quantity = sQtyList
        '    End If

        '    RQ.VehResRQCore.SpecialEquipPrefs = VehEqPref
        'End If


        ''******************* RKS 06-July-2006 Changes for Extra product *********************
        'Dim oFeeVehChargePurTyp() As THL_VehicleChargePurposeType
        'Dim oChargeVehChaTypCalc() As THL_VehicleChargeTypeCalculation
        'Dim OVehSegCoreType As New THL_VehicleSegmentCoreType
        'Dim OvehResType As New THL_VehicleReservationType
        'Dim NoOfDays As Long = DateDiff(DateInterval.Day, CDate(pickUpDate), CDate(returnDate))
        'Dim currentIndex As Integer

        'ReDim Preserve oFeeVehChargePurTyp(currentIndex)
        'oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
        'oFeeVehChargePurTyp(currentIndex).Description = "ER1"
        'oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
        'oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
        'oFeeVehChargePurTyp(currentIndex).Amount = NoOfDays * 20
        'oFeeVehChargePurTyp(currentIndex).Purpose = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_Quantity = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_UnitName = "Day"

        'ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
        'oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
        'oChargeVehChaTypCalc(currentIndex + 1).Quantity = NoOfDays.ToString()
        'oChargeVehChaTypCalc(currentIndex + 1).UnitName = "Day"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 20
        'oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
        'currentIndex = currentIndex + 1

        'ReDim Preserve oFeeVehChargePurTyp(currentIndex)
        'oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
        'oFeeVehChargePurTyp(currentIndex).Description = "CHAIR"
        'oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
        'oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
        'oFeeVehChargePurTyp(currentIndex).Amount = CInt(2) * 12
        'oFeeVehChargePurTyp(currentIndex).Purpose = "1"
        'oFeeVehChargePurTyp(currentIndex).THL_Quantity = "2"
        'oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

        'ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
        'oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
        'oChargeVehChaTypCalc(currentIndex + 1).Quantity = "2"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
        'oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 12

        'oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
        'currentIndex = currentIndex + 1

        'OVehSegCoreType.Fees = oFeeVehChargePurTyp
        'OVehSegCoreType.RentalRate = New THL_VehicleSegmentCoreTypeRentalRate
        ''OVehSegCoreType.RentalRate = oCoreTypRentRate
        'OvehResType.VehSegmentCore = New THL_VehicleSegmentCoreType
        'OvehResType.VehSegmentCore = OVehSegCoreType
        'RQ.CreateBookingRQInfoCore = New THL_VehicleRetrieveResRSCoreType
        'RQ.CreateBookingRQInfoCore.VehReservation = New THL_VehicleReservationType
        'RQ.CreateBookingRQInfoCore.VehReservation = OvehResType
        ''**************************** END ***************************************************

        'RQ.VehResRQInfo.Reference = ReferenceType
        '-----------------------payment details------------------------------------------

        'If CStr(Session("TypeOfUser")) = "customer" Then
        '    Dim oPaymentCardType As New PaymentCardType
        '    oPaymentCardType.CardHolderName = Deposit1.NameHolder
        '    oPaymentCardType.CardNumber = Deposit1.CardNumber
        '    oPaymentCardType.ExpireDate = Deposit1.Expiry
        '    oPaymentCardType.CardType = "1"
        '    oPaymentCardType.SeriesCode = Deposit1.SeriesCode


        '    Dim oBankAcctType As New BankAcctType

        '    oBankAcctType.BankID = "bankid"
        '    oBankAcctType.BankAcctName = "BankAcctName"


        '    Dim oDirectBillType As New DirectBillType
        '    Dim oDirectBillTypeCompanyName As New DirectBillTypeCompanyName
        '    Dim oAddressInfotype As New AddressInfoType
        '    oDirectBillTypeCompanyName.ContactName = "Tourism Holding Limited"
        '    oDirectBillType.CompanyName = oDirectBillTypeCompanyName
        '    oAddressInfotype.AddressLine = New String() {"#6/12", "Eldon Road"}
        '    oAddressInfotype.CityName = "Auckalnd"
        '    oAddressInfotype.County = "New Zealand"
        '    oAddressInfotype.PostalCode = "1003"
        '    oDirectBillType.Address = oAddressInfotype
        '    oDirectBillType.DirectBill_ID = "123456007"

        '    Dim oPaymentFrType(5) As PaymentFormType

        '    oPaymentFrType(0) = New PaymentFormType
        '    oPaymentFrType(0).Item = GetType(PaymentCardType)
        '    oPaymentFrType(0).Item = oPaymentCardType

        '    oPaymentFrType(1) = New PaymentFormType
        '    oPaymentFrType(1).Item = GetType(BankAcctType)
        '    oPaymentFrType(1).Item = oBankAcctType

        '    oPaymentFrType(2) = New PaymentFormType
        '    oPaymentFrType(2).Item = GetType(DirectBillType)
        '    oPaymentFrType(2).Item = oDirectBillType

        '    RQ.CreateBookingRQInfo.RentalPaymentPref = oPaymentFrType

        'End If
        '-----------------------------------------------------------------------------------
        'Dim ser1 As XmlSerializer
        'Dim w1 As StringWriter

        'ser1 = New XmlSerializer(GetType(THL_CreateBookingRQ))
        'w1 = New StringWriter
        'ser1.Serialize(w1, RQ)
        'Dim sString As String = w1.ToString()

        RS = Proxy.VehRes(RQ)


        If IsNothing(RS.Errors) Then
            Me.BookingId = RS.VehResRSCore.VehReservation.VehSegmentCore.ConfID.ID.ToString

            Return success    'if no errors, otherwise display errors.
        Else
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
                lblLoadError1.Text &= RS.Errors(errorIndex).Tag & "<br>"
                lblLoadError2.Text &= RS.Errors(errorIndex).Tag & "<br>"

                If RS.Errors(errorIndex).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                    Session("POS") = Nothing
                    Response.Redirect("AccessDenied.aspx")
                End If
            Next

        End If
    End Function

    Private Function ValidateForm(ByVal sExistingBookingNo As String) As Boolean
        Dim success As Boolean = True
        If ECheck(txtEmailAddress.Text) = False Then
            Return False
        End If
        Dim sb As New System.Text.StringBuilder
        If sExistingBookingNo.Trim.Length = 0 Then
            If txtFirstName.Text.Trim = "" Then
                'sb.Append("Please enter a first name" & "<br")
                success = False
                'UIScripts.SetFocus(Me.Page, lblLoadError.ClientID)
            End If

            If txtSurname.Text.Trim = "" Then
                'sb.Append("Please enter a surname" & "<br")
                success = False
                'UIScripts.SetFocus(Me.Page, lblLoadError.ClientID)
            End If

            If ddlAdultsNo.SelectedValue.Trim.Length = 0 Then
                'sb.Append("Please enter a surname" & "<br")
                success = False
                'UIScripts.SetFocus(Me.Page, lblLoadError.ClientID)
            End If

            If txtAgentRefNoNew.Text.Trim.Length = 0 Then
                success = False
            End If
        Else
            If txtBookingNo.Text.Trim.Length = 0 Then
                success = False
            End If
            If txtAgentRefNo.Text.Trim.Length = 0 Then
                success = False
            End If
        End If

        If Deposit1.Visible = True AndAlso Deposit1.ValidateDepositForm = False Then
            'if a deposit is required check that all the relavant fields have been filled out
            'let the deposit control handle this
            success = False
        End If


        If success = False Then
            ' Focus on error message
            sb.Append("Fields marked with '*' is mandatory")
            lblLoadError1.Text = sb.ToString
            lblLoadError2.Text = sb.ToString
            If txtTabStateValue.Text = "Add" Then
                UIScripts.SetFocus(Me.Page, lblLoadError2.ClientID)
            Else
                UIScripts.SetFocus(Me.Page, lblLoadError1.ClientID)
            End If
        Else
            ' new changes to prevent strings between 1-2 chars sneaking in
            If txtFirstName.Text.Trim.Length > 0 AndAlso txtFirstName.Text.Trim.Length < 2 Then
                sb.Append("First name")
                success = False
            End If
            If txtSurname.Text.Trim.Length > 0 AndAlso txtSurname.Text.Trim.Length < 2 Then
                If sb.Length > 0 Then
                    sb.Append(", Surname")
                Else
                    sb.Append("Surname")
                End If
                success = False
            End If
            If txtAgentRefNoNew.Text.Trim.Length > 0 AndAlso txtAgentRefNoNew.Text.Trim.Length < 2 Then
                If sb.Length > 0 Then
                    sb.Append(", Agent reference")
                Else
                    sb.Append("Agent reference")
                End If
                success = False
            End If
            If txtAddressLine1.Text.Trim.Length > 0 AndAlso txtAddressLine1.Text.Trim.Length < 2 Then
                If sb.Length > 0 Then
                    sb.Append(", Address line 1")
                Else
                    sb.Append("Address line 1")
                End If
                success = False
            End If
            If txtAddressLine2.Text.Trim.Length > 0 AndAlso txtAddressLine2.Text.Trim.Length < 2 Then
                If sb.Length > 0 Then
                    sb.Append(", Address line 2")
                Else
                    sb.Append("Address line 2")
                End If
                success = False
            End If
            If txtCity.Text.Trim.Length > 0 AndAlso txtCity.Text.Trim.Length < 2 Then
                If sb.Length > 0 Then
                    sb.Append(", City")
                Else
                    sb.Append("City")
                End If
                success = False
            End If

            If Not success Then
                sb.Append(" must be at least 2 characters long")

                lblLoadError1.Text = sb.ToString
                lblLoadError2.Text = sb.ToString
                'If txtTabStateValue.Text = "Add" Then
                '    UIScripts.SetFocus(Me.Page, lblLoadError2.ClientID)
                'Else
                '    UIScripts.SetFocus(Me.Page, lblLoadError1.ClientID)
                'End If
            End If
            ' new changes to prevent strings between 1-2 chars sneaking in
        End If

        Return success
    End Function

    Private Function ECheck(ByVal str As String) As Boolean

        Dim at As String = "@"
        Dim dot As String = "."

        Dim lat As Integer = str.IndexOf(at)
        Dim lstr As Integer = str.Length
        Dim ldot As Integer = str.IndexOf(dot)

        If (str = "") Then
            lblLoadError1.Text = ""
            Return True
        End If

        If str.IndexOf(at) = -1 Then
            lblLoadError1.Text = "Invalid E-mail ID"

            Return False
        End If

        If str.IndexOf(at) = -1 Or str.IndexOf(at) = 0 Or str.IndexOf(at) = lstr Then
            lblLoadError1.Text = "Invalid E-mail ID"

            Return False
        End If

        If str.IndexOf(dot) = -1 Or str.IndexOf(dot) = 0 Or str.IndexOf(dot) = lstr Then
            lblLoadError1.Text = "Invalid E-mail ID"

            Return False
        End If

        If str.IndexOf(at, (lat + 1)) <> -1 Then
            lblLoadError1.Text = "Invalid E-mail ID"
            Return False
        End If

        'If str.Substring(lat - 1, lat) = dot Or str.Substring(lat + 1, lat + 2) = dot Then
        'lblLoadError1.Text = "Invalid E-mail ID"
        'Return False
        'End If

        If str.IndexOf(dot, (lat + 2)) = -1 Then
            lblLoadError1.Text = "Invalid E-mail ID"

            Return False
        End If

        If str.IndexOf(" ") <> -1 Then
            lblLoadError1.Text = "Invalid E-mail ID"

            Return False
        End If
        Return True
        lblLoadError1.Text = ""
    End Function


End Class
