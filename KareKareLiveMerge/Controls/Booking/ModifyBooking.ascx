<%@ Register TagPrefix="uc1" TagName="LocationAndTime" Src="../PriceAndAvailability/LocationAndTime.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Deposit" Src="../Deposit/Deposit.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="ModifyBooking.ascx.vb" Inherits="THLAuroraWebInterface.ModifyBooking" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="uc1" TagName="FeesAndChargesNewBooking" Src="../FeesAndCharges/FeesAndChargesNewBooking.ascx" %>
<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="746" border="0">
	<TR>
		<TD>
			<uc1:LocationAndTime id="LocationAndTime1" runat="server"></uc1:LocationAndTime></TD>
	</TR>
	<TR>
		<TD>
			<TABLE id="tblFees" runat="server" cellSpacing="0" cellPadding="2" width="746" border="0">
				<TR>
					<TD class="StdConfirmationHeading">Booking Summary</TD>
				</TR>
				<TR>
					<TD>
						<uc1:FeesAndChargesNewBooking id="FeesAndChargesNewBooking1" runat="server"></uc1:FeesAndChargesNewBooking></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD>
			<TABLE id="tblDeposit" runat="server" cellSpacing="0" cellPadding="2" width="746" border="0">
				<TR>
					<TD>
						<uc1:Deposit id="Deposit1" runat="server"></uc1:Deposit></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD align="right">
			<asp:Button id="btnConfirm" runat="server" Text="Confirm" Visible="False" Width="100px"></asp:Button></TD>
	</TR>
	<TR>
		<TD>
			<asp:Label id="lblModifyError" runat="server" CssClass="warning"></asp:Label></TD>
	</TR>
</TABLE>
