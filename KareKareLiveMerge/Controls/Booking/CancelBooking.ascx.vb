Imports System.Net
Imports System.Text
Imports System.Xml

'--------------------------------------------------------------------------------------
'   Class:          CancelBooking
'   Description:    Allows user to cancel a booking and enter a reason for cancellation
'--------------------------------------------------------------------------------------

Partial Class CancelBooking
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

#Region " Web Form Designer Generated Code "

    Dim HTTPProxy As String = String.Empty

    Private Property CustomerConfirmationMode As Boolean

    'Private Property CurrentAgent As String

    



    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents emailAddressTxt As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Public Event Confirm()
    Public Event Cancel()
    Public Enum BookingType
        Quote = 0
        Provisional = 1
        Confirmed = 2
    End Enum

    Private Property BookingId() As String
        Get
            Return CStr(ViewState("BookingID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BookingID") = Value
        End Set
    End Property

    Public Property TypeOfBooking() As BookingType
        Get
            Return CType(ViewState("BookingType"), BookingType)
        End Get
        Set(ByVal Value As BookingType)
            ViewState("BookingType") = Value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
       
    End Sub

    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        ''Commit cancellation and show standard confirmation
       
        CancelBooking()
        If lblLoadError.Text = "" Then
            RaiseEvent Confirm()
        Else
            btnConfirm.Visible = False
            btnCancel.Visible = False
            btnBackQandB.Visible = True
        End If

        'Now Get Agent's Email Address
        'Dim da As New THL.Booking.DataAccess
        StandardConfirmation1.InitStandardConfirmation(BookingId, False, CustomerConfirmationMode)
        sendEmailConfirmation()





    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'back to booking list
        If lblLoadError.Text = "" Then
            RaiseEvent Cancel()
        End If
    End Sub

    Public Sub InitCancelBooking(ByVal BookingId As String)
        Me.BookingId = BookingId
        btnConfirm.Visible = True
        btnCancel.Visible = True
        btnBackQandB.Visible = False
        'StandardConfirmation1.InitStandardConfirmation(BookingId, False, False)
        'CurrentAgent = HttpContext.Current.Session("CurrencyOptionsdata")(0)(0)
        emailAddressTxt.Text = getAgentEmail(StandardConfirmation1.AgentCode)
    End Sub

    Private Sub CancelBooking()
        Dim RQ As New OTA_VehCancelRQ
        Dim RS As New OTA_VehCancelRS
        ' POS
        RQ.EchoToken = CStr(Session("SessionId"))
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}

        ' Unique Id
        Dim OuniqId As New UniqueID_Type
        OuniqId.Type = "1"
        OuniqId.ID = Me.BookingId
        RQ.VehCancelRQCore = New CancelInfoRQType
        RQ.VehCancelRQCore.CancelType = TransactionActionType.Cancel
        RQ.VehCancelRQCore.UniqueID = New UniqueID_Type() {OuniqId}

        Dim ObjExtension(0) As VehicleResCancelInfoType
        ObjExtension(0) = New VehicleResCancelInfoType
        If txtReason.Text = "" Then
            ObjExtension(0).Reason = "NA"
        Else
            ObjExtension(0).Reason = txtReason.Text
        End If

        RQ.VehCancelRQInfo = New VehicleCancelRQAdditionalInfoType
        'RQ.VehCancelRQInfo.TPA_Extensions(0).Reason = "TExt"
        'RQ.VehCancelRQInfo.TPA_Extensions(0) = New VehicleResCancelInfoType
        RQ.VehCancelRQInfo.TPA_Extensions = ObjExtension

        RS = Proxy.VehCancel(RQ)

        If IsNothing(RS.Errors) Then

        Else
            If RS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblLoadError.Text = RS.Errors(0).Tag.ToString
        End If

    End Sub

    Private Sub btnBackQandB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackQandB.Click
        Response.Redirect("QuotesAndBookings.aspx")
    End Sub

    Private Sub sendEmailConfirmation()



        Dim retValue As String = String.Empty
        Dim stringURL As String = String.Empty
        'GetStringForURL(stringURL)
        Dim SendEmailTo As String = String.Empty
        If emailAddressTxt.Text.Trim.Equals(String.Empty) Then
            SendEmailTo = getAgentEmail(StandardConfirmation1.AgentCode)
            'SendEmailTo = "nimesh.trivedi@thlonline.com"
            CustomerConfirmationMode = False
        Else
            SendEmailTo = emailAddressTxt.Text.Trim
            CustomerConfirmationMode = True
        End If
        SendConfirmation(SendEmailTo, "Email of Cancellation Booking ref : " + BookingId, txtReason.Text)

    End Sub

    'Private Sub GetStringForURL(stringURL As String)
    '    Throw New NotImplementedException
    'End Sub
    Public Function GetStringForURL(url As String) As String
        Dim result As String = Nothing
        Try
            Dim request As HttpWebRequest = getHTTPRequest(url)
            'request.Proxy.Credentials = CredentialCache.DefaultCredentials
            Dim myHttpWebResponse1 As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
            Dim enc As Encoding = System.Text.Encoding.GetEncoding(1252)
            Dim loResponseStream As New StreamReader(myHttpWebResponse1.GetResponseStream(), enc)
            result = loResponseStream.ReadToEnd()
            loResponseStream.Close()
            myHttpWebResponse1.Close()
        Catch ex As Exception
            'THLDebug.LogError(ErrorTypes.Database, "DataAccess.GetStringForURL", ex.Message, (Convert.ToString("{url:'") & url) + "'}")
            result = Nothing
        End Try
        Return result
    End Function

    'Private Function getHTTPRequest(url As String) As HttpWebRequest
    '    Throw New NotImplementedException
    'End Function
    Private Function getHTTPRequest(url As String) As HttpWebRequest
        HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings("Proxy")
        Dim byPassList As String = System.Web.Configuration.WebConfigurationManager.AppSettings("ByPassProxy")
        Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create(url), HttpWebRequest)
        If HTTPProxy IsNot Nothing AndAlso HTTPProxy <> String.Empty Then
            Dim localProxy As New WebProxy(HTTPProxy, True, New String() {byPassList})
            request.Proxy = localProxy
        End If
        'THLDebug.LogEvent(EventTypes.HTTPRequest, (Convert.ToString("{ url:'") & url) + "'}")
        Return request
    End Function


    Public Function getAgentEmail(agentName As String) As String
        Dim retValue As String = String.Empty
        'Dim agentCode As String = CStr(HttpContext.Current.Session("CurrencyOptionsdata")(0)(0))

        'Dim auroraBaseURL As String = System.Web.Configuration.WebConfigurationManager.AppSettings("AuroraServiceBaseURL")
        Dim currentAgent As String = String.Empty
        'currentAgent = HttpContext.Current.Session("CurrencyOptionsdata")(0)(0)
        Dim tempArray(3) As String
        'Dim tempString As String = DirectCast(HttpContext.Current.Session("CurrencyOptionsdata")(0), String())(0)
        'tempArray = DirectCast(HttpContext.Current.Session("CurrencyOptionsdata")(0)(0), String)
        Dim AgentCode As String = GetDefaultAgentCode() ' DirectCast(DirectCast(Session("CurrencyOptionsdata"), System.Collections.ArrayList)(0), String())(0)
        Dim auroraBaseURL As String = System.Web.Configuration.WebConfigurationManager.AppSettings("AuroraServiceBaseURL")
        Dim passingParameters As String = Convert.ToString("B2BAgentEmailAddress?agentCode=") & AgentCode
        Dim requestURL As String = Convert.ToString(auroraBaseURL & Convert.ToString("/")) & passingParameters
        Try
            Dim stringXml As String = GetStringForURL(requestURL)
            Dim responseXML As New XmlDocument()

            responseXML.LoadXml(stringXml)
            'Dim agentEmail As String = String.Empty
            retValue = responseXML.InnerText
            If (retValue.IndexOf("ERROR") <> -1) Then
                'If retValue.Contains("Invalid/Empty Email Address. Please contact Reservations") Then
                lblErrorMessage.Text = "The agent email address is not specified or invalid. Please contact reservations"
                'Else
                'lblErrorMessage.Text = retValue
                'End If
                'lblErrorMessage.Text = retValue
                retValue = String.Empty
            End If
        Catch generatedExceptionName As Exception

            Throw
        End Try
        Return retValue

    End Function

    Private Function SendConfirmation(ByVal emailAdd As String, ByVal subject As String, ByVal content As String) As Boolean
        Dim retValue As Boolean = False


        Dim PWService As New PhoenixWebServiceVB
        HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings("Proxy")
        Dim byPassList As String = System.Web.Configuration.WebConfigurationManager.AppSettings("ByPassProxy")

        If HTTPProxy IsNot Nothing AndAlso HTTPProxy <> String.Empty Then
            Dim localProxy As New WebProxy(HTTPProxy, True, New String() {byPassList})
            PWService.Proxy = localProxy
        End If
        Dim wsResponse As String = ""
        Try
            Dim GetContent As String = StandardConfirmation1.ConfirmationRawContent '  GetConfirmationContent()
            wsResponse = PWService.B2BSendConfirmation(emailAdd, subject, GetContent, (StandardConfirmation1.AgentCode))
            If (wsResponse.IndexOf("ERROR") <> -1) Then
                If wsResponse.Contains("Invalid/Empty Email Address. Please contact Reservations") Then
                    lblErrorMessage.Text = "The agent email address is not specified or invalid. Please contact reservations"
                Else
                    lblErrorMessage.Text = wsResponse
                End If

                retValue = False
            Else
                If Not emailAddressTxt.Text.Trim().Equals(String.Empty) Then
                    lblErrorMessage.Text = "The Cancellation confirmation email has been sent to recipients specified"

                End If

                retValue = True
            End If
        Catch ex As Exception
            'Dim errorText As String = ex.Message
            lblErrorMessage.Text = ex.Message
        End Try
        Return retValue
    End Function
    Dim RS As New OTA_VehRetResRS
    Private Function GetVehicleReservation() As Boolean
        Dim success As Boolean = False
        Dim RQ As New OTA_VehRetResRQ
        RQ.EchoToken = CStr(Session("SessionId"))
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        Dim OrqCoreType As New VehicleRetrieveResRQCoreType
        RQ.VehRetResRQCore = OrqCoreType

        Dim OuniqType As New UniqueID_Type
        OuniqType.Type = CStr(22)
        OuniqType.ID = BookingId
        OuniqType.Type = "NA"
        RQ.VehRetResRQCore.UniqueID = OuniqType


        Dim VehRetCoreType As New VehicleResRSAdditionalInfoType

        Dim persontype As New PersonNameType
        persontype.Surname = "NA"
        RQ.VehRetResRQCore.PersonName = persontype

        Dim tpa As New VehicleResAdditionalInfoType
        tpa.Agent = "NA"
        tpa.AgentRef = "NA"
        tpa.Package = "NA"    'not used but required as part of the request
        tpa.Status = "NA"    'not used but required as part of the request
        RQ.VehRetResRQCore.TPA_Extensions = tpa
        '------------------------------------------------------------------------------

        General.LogDebugNotes("Start StandardConfirmation: " & General.SerializeVehRetResRQObject(RQ))
        RS = Proxy.VehRetRes(RQ)
        General.LogDebugNotes("End StandardConfirmation: " & General.SerializeVehRetResRSObject(RS) & vbCrLf & vbCrLf)

        If IsNothing(RS.Errors) Then

            success = True
        Else
            If RS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblLoadError.Text = RS.Errors(0).Tag.ToString
            success = False
        End If

        Return success
    End Function

    Private Function GetDefaultAgentCode() As String
        Dim RQ As New THL_RetUsersRQ
        Dim RS As New THL_RetUsersRS
        Dim Proxy As New AuroraOTAProxy.Aurora_OTA

        Dim retValue As String = String.Empty
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)} ' Only pos required in request
        Try
            RS = Proxy.RetUsers(RQ) ' All user data retrieved based on user passed in pos

            Dim FullName As String = RS.Users(0).FirstName & " " & RS.Users(0).LastName
            retValue = RS.Users(0).DefAgentCode
            

            ' Currency

        Catch ex As Exception
            ExceptionManager.Publish(ex)
        End Try
        Return retValue

    End Function

   
End Class
