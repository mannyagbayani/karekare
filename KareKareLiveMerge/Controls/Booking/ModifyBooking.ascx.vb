Imports THLAuroraWebInterface.WebServices

'--------------------------------------------------------------------------------------
'   Class:          ModifyBooking
'   Description:    Allows user to modifying a booking or quote etc. Takes payment if required
'                   Modify has to go through an initialise setp if a booking folowed by a
'                   commit step. Quotes are committed straight away.
'--------------------------------------------------------------------------------------

Partial Class ModifyBooking
    Inherits System.Web.UI.UserControl
    Protected WithEvents FeesAndChargesNewBooking1 As FeesAndChargesNewBooking
    Protected WithEvents LocationAndTime1 As LocationAndTime
    Protected WithEvents Deposit1 As Deposit
    Dim RS As New OTA_VehModifyRS

#Region " Web Form Designer Generated Code "

    Private Property Slot As String

    Private Property SlotId As String

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Proxy As New AuroraOTAProxy.Aurora_OTA

    Public Event BookingModified(ByVal CustomerConfirmationMode As Boolean)
    Public Event CancelClicked()
    Private Enum BookingState
        Initialise = 0
        CommitNoDeposit = 1
        CommitDeposit = 2
    End Enum


    Private Property BookingId() As String
        Get
            Return CStr(ViewState("BookingID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BookingID") = Value
        End Set
    End Property



    Public Property CurrentBookingType() As TransactionActionType
        Get
            Return CType(ViewState("CurrentBookingType"), TransactionActionType)
        End Get
        Set(ByVal Value As TransactionActionType)
            ViewState("CurrentBookingType") = Value
        End Set
    End Property

    Public ReadOnly Property GrossNettDefault() As String
        Get
            If Session("GrossNettDefault") IsNot Nothing Then
                Return CStr(Session("GrossNettDefault"))
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property ShowCustomerConfirmationByDefault() As Boolean
        Get
            If GrossNettDefault = "G" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblModifyError.Text = ""
    End Sub


    Public Sub ModifyBooking(ByVal modifyType As TransactionActionType)
        Me.CurrentBookingType = modifyType
        If CStr(Session("TypeOfUser")) = "agent" Then
            PerformModify(BookingState.CommitNoDeposit, LocationAndTime1.PickUpLocation, LocationAndTime1.ReturnLocation, LocationAndTime1.PickUpDateTime, LocationAndTime1.ReturnDateTime, Slot, SlotId)
            LocationAndTime1.EnableControls(False)
        Else
            If modifyType = TransactionActionType.Quote Then
                ' No need to Initialise as this is a quote and can be changed straight away
                PerformModify(BookingState.CommitNoDeposit, LocationAndTime1.PickUpLocation, LocationAndTime1.ReturnLocation, LocationAndTime1.PickUpDateTime, LocationAndTime1.ReturnDateTime)
                LocationAndTime1.EnableControls(False)
            Else
                ' Need to Initialse 
                PerformModify(BookingState.Initialise, LocationAndTime1.PickUpLocation, LocationAndTime1.ReturnLocation, LocationAndTime1.PickUpDateTime, LocationAndTime1.ReturnDateTime)
                LocationAndTime1.EnableControls(False)
            End If

        End If

        If lblModifyError.Text = "" Then
            btnConfirm.Visible = True
            If Not modifyType = TransactionActionType.Quote Then
                ' Don't want to display deposit details if a quote
                If RS.VehModifyRSCore.ModifyStatus = TransactionStatusType.Pending Then
                    ' This is the second phase of a modify, it is pending waiting on a deposit.
                    If Not IsNothing(RS.VehModifyRSCore.VehReservation.VehSegmentInfo.PaymentRules) Then
                        'Check to see if this modification requires a deposit
                        LoadDepositDetails()
                        ' And display updated fees
                        FeesAndChargesNewBooking1.InitFeesAndChargesModifyBooking(RS)
                        tblFees.Visible = True
                    End If
                End If
            End If
        End If
    End Sub
    Private Function PerformModify(ByVal mBookingState As BookingState, ByVal pickUpLocation As String, ByVal returnLocation As String, ByVal pickUpDate As DateTime, ByVal returnDate As DateTime, Optional ByVal slot As String = "", Optional ByVal slotId As String = "") As Boolean
        Dim RQ As New OTA_VehModifyRQV3
        Dim ProxyV3 As Aurora_OTAV3
        Dim otaBase As String = System.Web.Configuration.WebConfigurationManager.AppSettings("AuroraOTABase")

        ProxyV3 = New Aurora_OTAV3(otaBase)
        Dim RS3 As New OTA_VehModifyRS
        ' POS
        'Dim tmpSourceType As SourceType() = New SourceType() {CType(Session("POS"), SourceType)}
        'Dim tmpSourceTypeV3 As New SourceTypeV3()

        ''tmpSourceType.CopyTo(tmpSourceTypeV3, 0)
        'tmpSourceTypeV3.AgentDutyCode = tmpSourceType(0).AgentDutyCode
        'tmpSourceTypeV3.AgentSine = tmpSourceType(0).AgentSine
        'tmpSourceTypeV3.AirlineVendorID = tmpSourceType(0).AirlineVendorID
        'tmpSourceTypeV3.AirportCode = tmpSourceType(0).AirportCode
        'tmpSourceTypeV3.BookingChannel = tmpSourceType(0).BookingChannel
        'tmpSourceTypeV3.ERSP_UserID = tmpSourceType(0).ERSP_UserID
        'tmpSourceTypeV3.FirstDepartPoint = tmpSourceType(0).FirstDepartPoint
        'tmpSourceTypeV3.ISOCountry = tmpSourceType(0).ISOCountry
        'tmpSourceTypeV3.ISOCurrency = tmpSourceType(0).ISOCurrency
        'tmpSourceTypeV3.Position = tmpSourceType(0).Position
        'tmpSourceTypeV3.PseudoCityCode = tmpSourceType(0).PseudoCityCode
        'tmpSourceTypeV3.RequestorID = tmpSourceType(0).RequestorID

        'RQ.POS = New SourceTypeV3() {tmpSourceTypeV3}
        RQ.Slot = slot
        RQ.SlotID = slotId
        ' RQ.POS = New SourceTypeV3() {CType(Session("POS"), SourceTypeV3)}
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}

        RQ.EchoToken = CStr(Session("SessionId"))

        ' Unique Id
        Dim OuniqId As New UniqueID_Type
        OuniqId.Type = "1"
        OuniqId.ID = Me.BookingId

        ' Locations
        Dim oRenCore As New VehicleRentalCoreType
        oRenCore.PickUpLocation = New LocationType
        oRenCore.PickUpLocation.LocationCode = pickUpLocation
        oRenCore.ReturnLocation = New LocationType
        oRenCore.ReturnLocation.LocationCode = returnLocation
        oRenCore.PickUpDateTime = pickUpDate
        oRenCore.ReturnDateTime = returnDate
        

        ' Attributes
        Dim vehModRq As New OTA_VehModifyRQVehModifyRQCore
        vehModRq.UniqueID = OuniqId
        vehModRq.VehRentalCore = oRenCore

        'vehModRq.Slot = slot
        'vehModRq.SlotId = slotId
        'this is a two step process, we need to send the modified details and see if 
        'the modifications require a deposit

        Select Case mBookingState
            Case BookingState.CommitDeposit
                'need to send deposit details 
                vehModRq.ModifyType = CType(Me.CurrentBookingType, TransactionActionType)
            Case BookingState.CommitNoDeposit
                vehModRq.ModifyType = CType(Me.CurrentBookingType, TransactionActionType)
            Case BookingState.Initialise
                vehModRq.ModifyType = CType(TransactionActionType.Initiate, TransactionActionType)
        End Select
        'Dim rs As New OTA_VehResRQV3()
        'rs.VehResRQCore = oRenCore

        RQ.VehModifyRQCore = vehModRq
        'Proxy.Url = "http://localhost/THLRentals.OTA.Utils/Aurora_OTA.asmx"
        'ProxyV3.VehRes(RQ)
        RS = ProxyV3.VehModify(RQ)

        If Not IsNothing(RS.Errors) AndAlso RS.Errors.Length > 0 Then
            'an error occured, display it
            Dim errorString As String
            Dim i As Integer
            For i = 0 To RS.Errors.Length - 1
                errorString &= RS.Errors(i).Tag & "<br>"
            Next

            If errorString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If

            lblModifyError.Text = errorString + ".."

        Else
            'give ability to confirm this modification
            btnConfirm.Visible = True
        End If

    End Function

    Private Sub LoadDepositDetails()
        Deposit1.InitDeposit()

        Deposit1.Deposit = RS.VehModifyRSCore.VehReservation.VehSegmentInfo.PaymentRules(0).Amount.ToString
        Deposit1.DepositCurrency = RS.VehModifyRSCore.VehReservation.VehSegmentInfo.PaymentRules(0).CurrencyCode.ToString()
        Deposit1.FullAmount = ""

        If RS.VehModifyRSCore.VehReservation.VehSegmentInfo.PaymentRules(0).Amount.ToString = "0" Then
            ' Hide deposit as none is required
            tblDeposit.Visible = False
        Else
            tblDeposit.Visible = True
        End If
    End Sub


    Public Sub InitModifyBooking(ByVal BookingId As String, ByVal modifyType As TransactionActionType)
        Me.BookingId = BookingId
        Me.CurrentBookingType = modifyType
        LocationAndTime1.InitLocationAndTime(BookingId, Me.CurrentBookingType)
        tblDeposit.Visible = False
        tblFees.Visible = False
        btnConfirm.Visible = False
    End Sub

    Public Sub InitModifyBookingFromQuote(ByVal BookingId As String, ByVal modifyType As TransactionActionType, _
                                          ByVal CustomerConfirmationMode As Boolean, Optional slot As String = "", Optional slotId As String = "")
        ' Progress a quote to a booking

        Me.BookingId = BookingId
        Me.CurrentBookingType = modifyType
        Me.Slot = slot
        Me.SlotId = slotId
        LocationAndTime1.InitLocationAndTime(BookingId)
        ModifyBooking(modifyType)
        tblDeposit.Visible = False
        tblFees.Visible = False
        btnConfirm.Visible = False

        If lblModifyError.Text = "" Then
            RaiseEvent BookingModified(CustomerConfirmationMode)
        End If

    End Sub

    ' '' added by nimesh on 17th feb 2015 for accepting slot and slot id
    'Public Sub InitModifyBookingFromQuote(ByVal BookingId As String, ByVal modifyType As TransactionActionType, _
    '                                     ByVal CustomerConfirmationMode As Boolean, ByVal Optional )
    '    ' Progress a quote to a booking

    '    Me.BookingId = BookingId
    '    Me.CurrentBookingType = modifyType
    '    LocationAndTime1.InitLocationAndTime(BookingId)
    '    ModifyBooking(modifyType)
    '    tblDeposit.Visible = False
    '    tblFees.Visible = False
    '    btnConfirm.Visible = False

    '    If lblModifyError.Text = "" Then
    '        RaiseEvent BookingModified(CustomerConfirmationMode)
    '    End If

    'End Sub
    '' end added by nimesh on 17th feb
    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        ' Need to check all deposit details are entered
        ' and make finalise modification
        PerformModify(BookingState.CommitDeposit, LocationAndTime1.PickUpLocation, LocationAndTime1.ReturnLocation, LocationAndTime1.PickUpDateTime, LocationAndTime1.ReturnDateTime)

        If lblModifyError.Text = "" Then
            RaiseEvent BookingModified(ShowCustomerConfirmationByDefault)
        End If
    End Sub

    Private Sub LocationAndTime1_ModifyBookingclicked1() Handles LocationAndTime1.ModifyBookingclicked
        ModifyBooking(Me.CurrentBookingType)
        If CStr(Session("TypeOfUser")) = "agent" Then
            ' Booking has been modified by an agent proceed to confirmation 
            ' as long as  there are no errors
            If lblModifyError.Text = "" Then
                RaiseEvent BookingModified(ShowCustomerConfirmationByDefault)
            End If
        Else
            ' This is a customer booking, customers require a deposit if they are not modifting a quote
            If Me.CurrentBookingType = TransactionActionType.Quote Then
                'if a quote can go directly to sumary page
                If lblModifyError.Text = "" Then
                    RaiseEvent BookingModified(ShowCustomerConfirmationByDefault)
                End If
            End If
        End If

    End Sub

    Private Sub LocationAndTime1_ModifyBookingclickedV2(Optional ByVal slot As String = "", Optional ByVal slotId As String = "0") Handles LocationAndTime1.ModifyBookingclickedV2
        Me.Slot = slot
        Me.SlotId = slotId
        ModifyBooking(Me.CurrentBookingType)
        If CStr(Session("TypeOfUser")) = "agent" Then
            ' Booking has been modified by an agent proceed to confirmation 
            ' as long as  there are no errors
            If lblModifyError.Text = "" Then
                RaiseEvent BookingModified(ShowCustomerConfirmationByDefault)
            End If
        Else
            ' This is a customer booking, customers require a deposit if they are not modifting a quote
            If Me.CurrentBookingType = TransactionActionType.Quote Then
                'if a quote can go directly to sumary page
                If lblModifyError.Text = "" Then
                    RaiseEvent BookingModified(ShowCustomerConfirmationByDefault)
                End If
            End If
        End If

    End Sub

    Private Sub LocationAndTime1_CancelClicked1() Handles LocationAndTime1.CancelClicked
        RaiseEvent CancelClicked()
    End Sub
End Class
