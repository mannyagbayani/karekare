<%@ Register TagPrefix="uc1" TagName="BookingSummary" Src="BookingSummary.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="MakeBooking.ascx.vb" Inherits="THLAuroraWebInterface.MakeBooking" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="uc1" TagName="Deposit" Src="../Deposit/Deposit.ascx" %>
<TABLE class="normal" id="Table1" cellSpacing="0" cellPadding="2" width="746" border="0">
	<TR>
		<TD><uc1:bookingsummary id="BookingSummary1" runat="server"></uc1:bookingsummary></TD>
	</TR>
</TABLE>
<TABLE style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px"
	cellSpacing="0" cellPadding="0" border="0">
	<TR class1="RowHeading">
		<td><asp:label id="lblLoadError1" Runat="server" CssClass="warning"></asp:label>
			<TABLE style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0x; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-BOTTOM: gray thin; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none"
				cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<TD class="SelectedRowHeading" id="NewBooking" style="CURSOR: hand" onclick="DispDiv(0,false)"
						align="center"><img alt="New Booking" id="ImgNewBooking" name="ImgNewBooking" src="..\Images\NewBooking.GIF"></img></TD>
					<TD class="LightRowHeading" id="AddToExBoo" style="CURSOR: hand" onclick="DispDiv(1,false)"
						align="center"><img alt="Add To Existing Booking" id="ImgAddToExBooking" name="ImgAddToExBooking" src="..\Images\AddToExistingBooking_in.GIF"></img></TD>
				</tr>
			</TABLE>
		</td>
	</TR>
	</TD></TR>
	<tr>
		<td>
			<div id="AddToBookingDiv" style="BORDER-RIGHT: gray 1px outset; TABLE-LAYOUT: fixed; BORDER-TOP: gray 1px outset; VISIBILITY: hidden; BORDER-LEFT: gray 1px outset; BORDER-BOTTOM: gray 1px outset; POSITION: absolute; BORDER-COLLAPSE: separate">
				<TABLE class="normal" id="Table11" cellSpacing="0" cellPadding="2" width="746" border="0">
					<TR>
						<TD>
							<TABLE class="normal" id="Table2" cellSpacing="0" cellPadding="2" width="100%" border="0">
								<TR>
									<TD class="stdconfirmationheading" width="80">Booking&nbsp;No</TD>
									<TD><asp:textbox id="txtBookingNo" runat="server" MaxLength="64"></asp:textbox><font color="#ff0033">&nbsp;*</font></TD>
									<TD class="stdconfirmationheading" align="right">Agent&nbsp;Reference&nbsp;No</TD>
									<TD style="WIDTH: 23px" align="right"><asp:textbox id="txtAgentRefNo" runat="server" MaxLength="64"></asp:textbox><font color="#ff0033">&nbsp;*</font></TD>
									<TD align="right"><asp:button id="btnContinueAdd" runat="server" CausesValidation="False" Text="Continue"></asp:button></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading" width="80">Notes</TD>
									<TD colSpan="4"><asp:textbox id="txtRentalNote" onkeyup="CheckStringLength(this)" runat="server" MaxLength="254"
											Width="600px" TextMode="MultiLine"></asp:textbox></TD>
								</TR>
								<tr>
									<td colSpan="2"><asp:label id="lblLoadError2" Runat="server" CssClass="warning"></asp:label></td>
								</tr>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</div>
			<div id="NewBookingDiv" style="BORDER-RIGHT: gray 1px outset; TABLE-LAYOUT: fixed; BORDER-TOP: gray 1px outset; BORDER-LEFT: gray 1px outset; BORDER-BOTTOM: gray 1px outset; POSITION: absolute; BORDER-COLLAPSE: separate">
				<table>
					<TR>
						<TD>
							<TABLE class="normal" id="Table3" style="WIDTH: 472px; HEIGHT: 216px" cellSpacing="0" cellPadding="2"
								border="0">
								<TR>
									<TD class="stdconfirmationheading" width="80">Title</TD>
									<TD style="HEIGHT: 26px"><asp:dropdownlist tabindex="1" id="ddlTitle" runat="server" Width="48px">
											<asp:ListItem Value="Mr">Mr</asp:ListItem>
											<asp:ListItem Value="Mrs">Mrs</asp:ListItem>
											<asp:ListItem Value="Ms">Ms</asp:ListItem>
										</asp:dropdownlist></TD>
									<TD class="warning" style="WIDTH: 14px; HEIGHT: 26px"></TD>
									<TD class="stdconfirmationheading"></TD>
									<TD style="HEIGHT: 26px"></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading" width="80">Customer&nbsp;First&nbsp;Name</TD>
									<TD style="HEIGHT: 26px"><asp:textbox id="txtFirstName" tabIndex="2" runat="server" MaxLength="64"></asp:textbox><font color="#ff0033">&nbsp;*</font></TD>
									<TD class="warning" style="WIDTH: 14px; HEIGHT: 26px"></TD>
									<TD class="stdconfirmationheading">No Adults</TD>
									<TD style="HEIGHT: 26px"><asp:dropdownlist id="ddlAdultsNo" tabIndex="11" runat="server" CssClass="normalddl">
											<asp:ListItem Value="" Selected="True">0</asp:ListItem>
											<asp:ListItem Value="1">1</asp:ListItem>
											<asp:ListItem Value="2">2</asp:ListItem>
											<asp:ListItem Value="3">3</asp:ListItem>
											<asp:ListItem Value="4">4</asp:ListItem>
											<asp:ListItem Value="5">5</asp:ListItem>
											<asp:ListItem Value="6">6</asp:ListItem>
											<asp:ListItem Value="7">7</asp:ListItem>
											<asp:ListItem Value="8">8</asp:ListItem>
											<asp:ListItem Value="9">9</asp:ListItem>
											<asp:ListItem Value="10">10</asp:ListItem>
										</asp:dropdownlist><font color="#ff0033">&nbsp;*</font></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading">Customer&nbsp;Surname</TD>
									<TD><asp:textbox id="txtSurname" tabIndex="3" runat="server" MaxLength="64"></asp:textbox><font color="#ff0033">&nbsp;*</font></TD>
									<TD class="warning" style="WIDTH: 14px"></TD>
									<TD class="stdconfirmationheading">NoChildren</TD>
									<TD><asp:dropdownlist id="ddlChildrenNo" tabIndex="12" runat="server" CssClass="normalddl">
											<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
											<asp:ListItem Value="1">1</asp:ListItem>
											<asp:ListItem Value="2">2</asp:ListItem>
											<asp:ListItem Value="3">3</asp:ListItem>
											<asp:ListItem Value="4">4</asp:ListItem>
											<asp:ListItem Value="5">5</asp:ListItem>
											<asp:ListItem Value="6">6</asp:ListItem>
											<asp:ListItem Value="7">7</asp:ListItem>
											<asp:ListItem Value="8">8</asp:ListItem>
											<asp:ListItem Value="9">9</asp:ListItem>
											<asp:ListItem Value="10">10</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
								<TR>
                                    <TD class="stdconfirmationheading" width="80">Customer&nbsp;Email</TD>
									<TD><asp:textbox id="txtEmailAddress" OnBlur="echeck(this)" tabIndex="4" runat="server" MaxLength="128"></asp:textbox></TD>
									<TD style="WIDTH: 14px"></TD>
									<TD class="stdconfirmationheading">No Infants</TD>
									<TD><asp:dropdownlist id="ddlInfantsNo" tabIndex="13" runat="server" CssClass="normalddl">
											<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
											<asp:ListItem Value="1">1</asp:ListItem>
											<asp:ListItem Value="2">2</asp:ListItem>
											<asp:ListItem Value="3">3</asp:ListItem>
											<asp:ListItem Value="4">4</asp:ListItem>
											<asp:ListItem Value="5">5</asp:ListItem>
											<asp:ListItem Value="6">6</asp:ListItem>
											<asp:ListItem Value="7">7</asp:ListItem>
											<asp:ListItem Value="8">8</asp:ListItem>
											<asp:ListItem Value="9">9</asp:ListItem>
											<asp:ListItem Value="10">10</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
                                <TR>
									<TD class="stdconfirmationheading" width="80">Agent&nbsp;Reference&nbsp;No</TD>
									<TD><asp:textbox id="txtAgentRefNoNew" tabIndex="5" runat="server" MaxLength="64"></asp:textbox><font color="#ff0033">&nbsp;*</font></TD>
									<TD style="WIDTH: 14px"></TD>
                                    <TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading" width="80">Address&nbsp;Line&nbsp;1</TD>
									<TD><asp:textbox id="txtAddressLine1" tabIndex="6" runat="server" MaxLength="64"></asp:textbox></TD>
									<TD style="WIDTH: 14px"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading" width="80">Address&nbsp;Line&nbsp;2</TD>
									<TD><asp:textbox id="txtAddressLine2" tabIndex="7" runat="server" MaxLength="64"></asp:textbox></TD>
									<TD style="WIDTH: 14px"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading" width="80">City</TD>
									<TD><asp:textbox id="txtCity" tabIndex="8" runat="server" MaxLength="64"></asp:textbox></TD>
									<TD style="WIDTH: 14px"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading" width="80">Country</TD>
									<TD><asp:dropdownlist id="ddlCountry" tabIndex="9" runat="server" Width="100%"></asp:dropdownlist></TD>
									<TD style="WIDTH: 14px"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="stdconfirmationheading" width="80">Notes</TD>
									<TD colSpan="4"><asp:textbox id="txtRentalNoteNew" onblur="CheckStringLength(this)" onkeyup="CheckStringLength(this)"
											tabIndex="10" runat="server" MaxLength="255" Width="600px" TextMode="MultiLine"></asp:textbox></TD>
								</TR>
                                <!--<TR>
									<TD class="stdconfirmationheading" width="80">Product List</TD>
									<TD colSpan="4"><asp:textbox id="txtProductList" tabIndex="12" runat="server" MaxLength="255" Width="600px"></asp:textbox></TD>
								</TR>
                                <TR>
									<TD class="stdconfirmationheading" width="80">Qty List</TD>
									<TD colSpan="4"><asp:textbox id="txtQtyList" tabIndex="13" runat="server" MaxLength="255" Width="600px"></asp:textbox></TD>
								</TR>-->
							</TABLE>
							<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></TD>
					</TR>
					<TR>
						<TD><uc1:deposit id="Deposit1" runat="server"></uc1:deposit></TD>
					</TR>
					<TR>
						<TD align="right"><asp:button id="btnContinueNew" tabIndex="14" runat="server" Text="Continue" Width="75px"></asp:button><asp:button id="btnBack" tabIndex="15" runat="server" CausesValidation="False" Text="Back" Width="75px"></asp:button></TD>
					</TR>
				</table>
			</div>
		</td>
	</tr>
</TABLE>
<asp:textbox Height="0px" Width="0px" id="txtTabStateValue" Runat="server" Visible="true">test</asp:textbox>
<div style="POSITION: absolute"></div>
<script language="javascript">
	function DispDiv(bDivFlag,bFromFrontEnd){
		if(bDivFlag==0){
			//ImgNewBooking.src="..\Images\NewBooking.GIF" 
			//ImgAddToExBooking.src="..\Images\AddToExistingBooking_in.GIF"
			AddToBookingDiv.style.visibility="hidden";
			NewBookingDiv.style.visibility="";
			//NewBooking.className="SelectedRowHeading";
			//AddToExBoo.className="LightRowHeading";
			NewBooking.childNodes[0].src = '../Images/NewBooking.GIF';
			AddToExBoo.childNodes[0].src = '../Images/AddToExistingBooking_in.GIF';
			
			if(bFromFrontEnd==false){
				BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="";
				BookingContainer1_MakeBooking1_lblLoadError2.innerHTML="";
			}
			
		}
		if(bDivFlag==1){
			AddToBookingDiv.style.visibility="";
			NewBookingDiv.style.visibility="hidden";
			//NewBooking.className="LightRowHeading";
			//AddToExBoo.className="SelectedRowHeading";
			NewBooking.childNodes[0].src = '../Images/NewBooking_in.GIF';
			AddToExBoo.childNodes[0].src = '../Images/AddToExistingBooking.GIF';
			//ImgNewBooking.src="..\Images\NewBooking_in.GIF" 
			//ImgAddToExBooking.src="..\Images\AddToExistingBooking.GIF"
			
			if(bFromFrontEnd==false){
				BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="";
				BookingContainer1_MakeBooking1_lblLoadError2.innerHTML="";
			}
		}
	}
	function CheckStringLength(Obj){
		var StrText = Obj.value
		if(StrText.length>255){
			Obj.value = StrText.substring(0,255)
		}
	
	}
if(document.forms[0].BookingContainer1_MakeBooking1_txtTabStateValue.value=='Add')
    DispDiv(1, true)

/****************************************/
function echeck(Control) {

		var at="@"
		var dot = "."
        var str = Control.value
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot = str.indexOf(dot)
		if (str == "") {
		    BookingContainer1_MakeBooking1_lblLoadError1.innerHTML = ""
		    return true;
		}
		if (str.indexOf(at)==-1){
		   BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="Invalid E-mail ID"
		   Control.focus()
		   return false;
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="Invalid E-mail ID"
		   Control.focus()
		   return false;
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="Invalid E-mail ID"
		    Control.focus()
		    return false;
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="Invalid E-mail ID"
		    Control.focus()
		    return false;
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="Invalid E-mail ID"
		    Control.focus()
		    return false;
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="Invalid E-mail ID"
		    Control.focus()
		    return false;
		 }
		
		 if (str.indexOf(" ")!=-1){
		    BookingContainer1_MakeBooking1_lblLoadError1.innerHTML="Invalid E-mail ID"
		    Control.focus()
		    return false;
		 }

 		BookingContainer1_MakeBooking1_lblLoadError1.innerHTML=""					
	}
</script>

</script>
