<%@ Register TagPrefix="uc1" TagName="StandardConfirmation" Src="StandardConfirmation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ModifyBooking" Src="ModifyBooking.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BookingList" Src="BookingList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CancelBooking" Src="CancelBooking.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BookingSummary" Src="BookingSummary.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="BookingContainer.ascx.vb" Inherits="THLAuroraWebInterface.BookingContainer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="uc1" TagName="MakeBooking" Src="MakeBooking.ascx" %>
<uc1:ModifyBooking id="ModifyBooking1" runat="server" Visible="False"></uc1:ModifyBooking>
<uc1:StandardConfirmation id="StandardConfirmation1" runat="server" Visible="False"></uc1:StandardConfirmation>
<uc1:BookingList id="BookingList1" runat="server" Visible="False"></uc1:BookingList>
<uc1:CancelBooking id="CancelBooking1" runat="server" Visible="False"></uc1:CancelBooking>
<uc1:MakeBooking id="MakeBooking1" runat="server" Visible="False"></uc1:MakeBooking>
<uc1:BookingSummary id="BookingSummary1" runat="server" Visible="False"></uc1:BookingSummary>
