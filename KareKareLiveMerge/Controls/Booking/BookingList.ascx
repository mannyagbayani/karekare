<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BookingList.ascx.vb"
    Inherits="THLAuroraWebInterface.BookingList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
    <style type="text/css">
    .WrappedCell
    {
        WORD-BREAK:BREAK-ALL    	
    }
    </style>
    
<table id="tblSearch" cellspacing="0" cellpadding="2" width="750" border="0" runat="server">
    <tr class="Normal">
        <td class="CellHeading2 " style="width: 40px" valign="middle">
            Agent
        </td>
        <td class="CellHeading2 " style="width: 147px" valign="top">
            <asp:DropDownList ID="ddlAgents" runat="server" CssClass="Normalddl" Width="153px">
                <asp:ListItem Value="All agents">All agents</asp:ListItem>
                <asp:ListItem Value="FER Zurich">FER Zurich</asp:ListItem>
                <asp:ListItem Value="FER Malta">FER Malta</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="CellHeading2 " valign="middle">
            Search for
        </td>
        <td class="CellHeading2 " valign="top">
            <asp:DropDownList ID="ddlFilter" runat="server" CssClass="Normalddl" Width="153px">
                <asp:ListItem Value="Cust Surname">Lastname</asp:ListItem>
                <asp:ListItem Value="Booking No">Booking Ref No</asp:ListItem>
                <asp:ListItem Value="Agent's Ref No">Agent's Ref No</asp:ListItem>
                <asp:ListItem Value="DepartureDate">Pickup Date</asp:ListItem>
                <asp:ListItem Value="Quote">Quote</asp:ListItem>
                <asp:ListItem Value="Provisional">Provisional</asp:ListItem>
                <asp:ListItem Value="Confirmed">Confirmed</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="CellHeading2 " valign="middle">
            =
        </td>
        <td class="CellHeading2 " valign="top">
            <asp:TextBox ID="txtSearchKey" runat="server" Width="153px"></asp:TextBox>
        </td>
        <td class="CellHeading2 " valign="top">
            <asp:Button ID="btnSearchAll" runat="server" Width="58px" Text="Search" ToolTip="Search all Records">
            </asp:Button><asp:Button ID="btnClearFilter" runat="server" Width="87px" Text="Clear Search">
            </asp:Button>
        </td>
    </tr>
</table>
<table id="tblDatagrid" cellspacing="0" cellpadding="0" width="750" border="0" runat="server">
    <tr>
        <td class="normal">
            <asp:DataGrid ID="dgAgentData" runat="server" AllowPaging="True" OnItemDataBound="dgAgentData_ItemDataBound"
                OnSelectedIndexChanged="dgAgentData_SelectedIndexChanged" AutoGenerateColumns="False"
                AllowSorting="True" DataKeyField="BookingRefNo" OnSortCommand="dgAgentData_SortCommand"
                BorderColor="#999999" BorderWidth="1px" CellPadding="2">
                <SelectedItemStyle BackColor="#FFFFCC"></SelectedItemStyle>
                <ItemStyle CssClass="DatagridItem"></ItemStyle>
                <HeaderStyle CssClass="DatagridHeading"></HeaderStyle>
                <Columns>
                    <asp:ButtonColumn Text="Select" CommandName="Select">
                        <ItemStyle CssClass="DatagridItem"></ItemStyle>
                    </asp:ButtonColumn>
                    <asp:TemplateColumn SortExpression="BookingrefNo" HeaderText="Booking Ref No">
                        <ItemTemplate>
                            <asp:Label runat="server" Text="" ID="Label1" NAME="Label1"> <!--CommandName="select" CausesValidation="false"-->
								<%#container.dataitem("BookingRefNo")%>
                            </asp:Label>
                             <%--//rev:mia Oct 8 2013 RoadBear and Britz--%>
                            <asp:HiddenField ID="countryOfTravel" runat="server" Value='<%# Bind("Country")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="Lastname" SortExpression="Lastname" HeaderText="Lastname">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Package" HeaderText="Package"></asp:BoundColumn>
                    
                    <%--//rev:mia April 16 2012--%>
                    <asp:BoundColumn DataField="Checkout" SortExpression="PickupDate" HeaderText="Pickup Date (dd/mm/yy) /Location " DataFormatString="{0:dd-mm-yyyy}">
                          <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>


                    <asp:BoundColumn DataField="Checkin" HeaderText="Return Date (dd/mm/yy) /Location">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Agent" SortExpression="agent" HeaderText="Agent"></asp:BoundColumn>
                    <asp:BoundColumn DataField="AgentRefNo" SortExpression="agentrefno" HeaderText="Agent's Ref No">
                    <ItemStyle CssClass="WrappedCell" />
                    </asp:BoundColumn>
                    <asp:TemplateColumn SortExpression="status" HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblStatus" Text='	<%#container.dataitem("status")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="status" HeaderText="Status"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ExpireDate" HeaderText="Expires"></asp:BoundColumn>
                    <asp:BoundColumn DataField="CustomerVersionOfConfirmationPresent" Visible="false">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="AgentCode" Visible="false" />
                    <asp:BoundColumn DataField="CurrencyOfTravel" Visible="false" />
                </Columns>
                <PagerStyle HorizontalAlign="Center" CssClass="DatagridHeading" Mode="NumericPages">
                </PagerStyle>
            </asp:DataGrid>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="btnBook" runat="server" Width="97px" Text="Book" ToolTip="Progress to Booking"
                Visible="False" Style="float: left" />
            <asp:Button ID="btnBookSlots" runat="server" Width="97px" Text="Book With Slots" ToolTip="Progress to Booking"
                Visible="False" Style="float: left" />
            <asp:Button ID="btnModify" runat="server" Width="97px"
                    Text="Modify" ToolTip="Modify Booking" Visible="False" Style="float: left" /><asp:Button
                        ID="btnDelete" runat="server" Width="97px" Text="Delete" ToolTip="Delete Booking"
                        Visible="False" Style="float: left" /><asp:Button ID="btnCancel" runat="server" Width="97px"
                            Text="Cancel" ToolTip="Cancel Booking" Visible="False" Style="float: left" /><asp:Button
                                ID="btnRegister" runat="server" Width="97px" Text="Pre-register" Visible="False"
                                Style="float: left" /><asp:Button ID="btnConfirm" runat="server" Width="97px" Text="Confirm"
                                    ToolTip="Confirm Booking" Style="float: left" Visible="False" /><asp:Button ID="btnViewConfirmation"
                                        runat="server" Width="140px" Text="Agent Confirmation" ToolTip="View Agent Confirmation"
                                        Visible="False" Style="float: left" /><asp:Button ID="btnCusConfirmation" runat="server"
                                            Width="160px" Text="Customer Confirmation" ToolTip="View Customer Confirmation"
                                            Visible="False" Style="float: left" /><asp:Button ID="btnAddNote" runat="server"
                                                Width="97px" Text="Add Note" ToolTip="Add Note" Visible="False" Style="float: left" /><asp:DropDownList
                                                    ID="cmbCurrency" runat="server" Width="150px" Visible="False" Style="float: right;
                                                    vertical-align: middle;" Height="25px" />
        </td>
    </tr>

    <!-- Display Slots Details if available, added by nimesh on 16th Feb 2015 -->
    <tr>
        <td>
            <div id="slotsDetail" runat ="server" visible="false" style="font-family:Helvetica, Arial, sans-serif;width:750px;">
                                <div class="title">
                            <h4 style="color:#222;font-size:14px;font-weight:bold;">Select your pickup time:&nbsp;</h4>
                        </div>
                        <p style="padding-left:32px;font-family:Helvetica, Arial, sans-serif; font-size:12px;">Please tell us when you'll be arriving so we can ensure you have the best experience when picking up your campervan.</p>
                                <div id="pickuptimes">
                                    <%--<div id="currentday">
                                        <span class="date"><%= PickUpDayMonthStr %></span>
                                        <span class="location"><%= PickUpLocationStr %></span>
                                    </div>--%>
                                    <div id="times" style="font-size:12px">
                                        <%=slotsPanel%>
                                    </div>
                                    
                                </div>
                <br />
                <asp:Button ID="btnContinue" runat ="server" Text ="Continue" OnClientClick="return validateSlot();" />
                <asp:Button ID="btnBack" runat="server" Text ="Back" />
                            </div>
        </td>
    </tr>

    <%--<tr>
        <td><%=slotsPanel%></td>
    </tr>--%>
    <!-- End Display Slots Details if available, added by nimesh on 16th Feb 2015 -->
</table>
<p>
    <asp:Label ID="lblLoadError" runat="server" CssClass="warning"></asp:Label></p>
<p>
    <asp:Label ID="lblExistingbookings" runat="server">There are no existing Quotes or Bookings to show.</asp:Label></p>
    <asp:HiddenField ID="txtSlot" Value ="" runat="server"  />
    <asp:HiddenField ID="txtSlotId" Value ="0" runat="server" />

<script type="text/javascript">
    // added by Nimesh on 16th Feb 2015 for selection change for timeslots
    var disableAll;
    <% If DisableAll Then%>
    disableAll = true;
    <% Else%>
    disableAll = <%= DisableAll.ToString().ToLower() %>
    <% End If%>

    
     $(document).ready(function () {
         
         if (!disableAll) {
             var timeLeft = jQuery('.timeslots.available.selected').find('.time.left').text();
             var timeRight = jQuery('.timeslots.available.selected').find('.time.right').text();
             //var slotId = jQuery('.timeslots.available.selected').attr('id').replace('slot', '');
             //$('#BookingContainer1_BookingList1_txtSlot').val(timeLeft + " - " + timeRight);
             //$('#BookingContainer1_BookingList1_txtSlotId').val(slotId);
                jQuery('.timeslots.available').click(function () {
                    //alert('clicked');
                    //jQuery('.timeslots.available').removeClass('selected').find('.availability').text('available');
                    //jQuery(this).toggleClass('selected').find('.availability').text('selected');

                    jQuery('.timeslots.available').removeClass('selected');
                    jQuery('.timeslots.available').find('.availability').each(function () {
                        jQuery(this).text(jQuery(this).text().replace('SELECTED', 'AVAILABLE'));
                    }
                        )
                    //jQuery('.timeslots.available').find('.availability').text(function () {
                    //   return $(this).text().replace("selected", "available"); 
                    //})
                    jQuery(this).toggleClass('selected');
                    jQuery(this).find('.availability').text(jQuery(this).find('.availability').text().replace('AVAILABLE', 'SELECTED'));
                    //jQuery(this).find('.availability').text(function () {
                    //   return $(this).text().replace("available", "selected"); 
                    //})
                    //var timeLeft = jQuery('.timeslots.available.selected').find('.time.left').each(function () {
                    //    return this.innerText;
                    //});
                    //var timeRight = jQuery('.timeslots.available.selected').find('.time.right').each(function () {
                    //    return this.innerText + "'";
                    //});
                    var timeLeft = jQuery('.timeslots.available.selected').find('.time.left').text();
                    var timeRight = jQuery('.timeslots.available.selected').find('.time.right').text();
                    var slotId = jQuery('.timeslots.available.selected').attr('id').replace('slot', '');
                    $('#BookingContainer1_BookingList1_txtSlot').val(timeLeft + " - " + timeRight);
                    $('#BookingContainer1_BookingList1_txtSlotId').val(slotId);
                })
            }
            else {
                //jQuery("#paymentForm :input").attr('disabled', true);
                //jQuery('.SubmitBtn').hide();
                jQuery('#slotsDetail').hide();
                //jQuery('.SubmitBtn').attr('disabled', 'disabled');
                //jQuery('.SubmitBtn').removeClass("SubmitBtn");
            }

         // end added by Nimesh on 13th Feb 2015 for selection change for timeslots
     });

    function validateSlot(e) {
        /* Added by Nimesh on 23rd Feb 2015 to validate if slot is selected*/
        var $k = $("#selectionError");
        $k.remove();
        var $j = $("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");
        
        if (jQuery('.timeslots.available').length > 0) {
            if (!(jQuery('.timeslots.available.selected').length === 1)) {
                //var error = $j("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");
                $j.insertBefore('#pickuptimes');
                var position = jQuery('#pickuptimes').position();
                scroll(0, position.TopPosition);
                return false;
            }
            else {
                //$j.remove();
                selSlot = true;
                return true;
            }
        }
        /* End Added by Nimesh on 23rd Feb 2015 to validate if slot is selected*/
    }
</script>