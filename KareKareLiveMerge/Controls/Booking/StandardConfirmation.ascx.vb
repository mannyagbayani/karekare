'--------------------------------------------------------------------------------------
'   Class:          StandardConfirmation
'   Description:    Displays all reservation details including a breakdown of costs.
'--------------------------------------------------------------------------------------

Option Strict Off
Imports System.IO
Imports System.Net
Imports System.Text
Imports TelecomNZ.Common.Library


Partial Class StandardConfirmation
    Inherits System.Web.UI.UserControl

    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Protected WithEvents FeesAndCharges1 As FeesAndCharges

    Protected WithEvents lblFooter As System.Web.UI.WebControls.Label
    Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
    Protected WithEvents FeesAndCharges2 As FeesAndCharges
    Protected WithEvents CompanyAddress1 As CompanyAddress

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rpAgentCharge As System.Web.UI.WebControls.Repeater
    Protected WithEvents rpCustomerCharges As System.Web.UI.WebControls.Repeater

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public Event [Continue]()
    Dim RS As New OTA_VehRetResRS

    Private Property BookingId() As String
        Get
            Return CStr(ViewState("BookingID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BookingID") = Value
        End Set
    End Property
    Public Property AgentCode() As String
        Get
            Return CStr(ViewState("AgentCode"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgentCode") = Value
        End Set
    End Property

    Public Property Currency() As String
        Get
            Return CStr(ViewState("Currency"))
        End Get
        Set(ByVal Value As String)
            ViewState("Currency") = Value
        End Set
    End Property

    Public Property RentalChargeGST() As String
        Get
            Return CStr(ViewState("RentalChargeGST"))
        End Get
        Set(ByVal Value As String)
            ViewState("RentalChargeGST") = Value
        End Set
    End Property

    Public Property FeeGST() As String
        Get
            Return CStr(ViewState("FeeGST"))
        End Get
        Set(ByVal Value As String)
            ViewState("FeeGST") = Value
        End Set
    End Property

    Public Property AgentDiscount() As String
        Get
            Return CStr(ViewState("AgentDiscount"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgentDiscount") = Value
        End Set
    End Property

    Public Property TotalRentalCharge() As String
        Get
            Return CStr(ViewState("TotalRentalCharge"))
        End Get
        Set(ByVal Value As String)
            ViewState("TotalRentalCharge") = Value
        End Set
    End Property

    Public Property TotalRentalPayable() As String
        Get
            Return CStr(ViewState("TotalRentalPayable"))
        End Get
        Set(ByVal Value As String)
            ViewState("TotalRentalPayable") = Value
        End Set
    End Property

    Public Property TotalFeesPayable() As String
        Get
            Return CStr(ViewState("TotalFeesPayable"))
        End Get
        Set(ByVal Value As String)
            ViewState("TotalFeesPayable") = Value
        End Set
    End Property

#Region "Customer Confirmation Changes"
    Public WriteOnly Property SelectedRateIsNettEvenThoughGrossOptionWasSelected() As Boolean
        Set(ByVal value As Boolean)
            Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected") = value
        End Set
    End Property

    Public ReadOnly Property UserCanSeeCustomerConfirmations() As Boolean
        Get
            If Session("UserCanSeeCustomerConfirmations") IsNot Nothing Then
                Return CBool(Session("UserCanSeeCustomerConfirmations"))
            Else
                Return False
            End If
        End Get
    End Property
#End Region

#Region "Currency Changes"
    Public Property MRU_CurrencyMultiplier() As Double
        Get
            If Session("MRU_CurrencyMultiplier") IsNot Nothing Then
                Return CDbl(Session("MRU_CurrencyMultiplier"))
            Else
                Return 0.0
            End If
        End Get
        Set(ByVal value As Double)
            Session("MRU_CurrencyMultiplier") = value
        End Set
    End Property

    Public Property MRU_CurrencyCode() As String
        Get
            Return CStr(Session("MRU_CurrencyCode"))
        End Get
        Set(ByVal value As String)
            Session("MRU_CurrencyCode") = value
        End Set
    End Property

    Public ReadOnly Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property CurrencyOptionsData() As ArrayList
        Get
            If Session("CurrencyOptionsData") IsNot Nothing Then
                Return CType(Session("CurrencyOptionsData"), ArrayList)
            Else
                Return New ArrayList
            End If
        End Get
    End Property

    Public Property ExchangeRatesLookupData() As ExchangeRatesTypeAgent()
        Get
            If Session("ExchangeRatesLookupData") IsNot Nothing Then
                Return CType(Session("ExchangeRatesLookupData"), ExchangeRatesTypeAgent())
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As ExchangeRatesTypeAgent())
            Session("ExchangeRatesLookupData") = value
        End Set
    End Property

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblLoadError.Text = ""
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        If lblLoadError.Text = "" Then
            Session("MRU_CurrencyCode") = Nothing
            Session("MRU_CurrencyMultiplier") = "0.0"

            ' RaiseEvent Continue()
            Response.Redirect("QuotesAndBookings.aspx")
        End If
    End Sub

    Public Sub InitStandardConfirmation(ByVal bookingId As String, Optional ByVal printVersion As Boolean = False, _
                                        Optional ByVal CustomerConfirmationMode As Boolean = False)


        ' currency changes
        Dim sDefaultCurrencyCode As String = ""
        Dim dMultiplier As Double = 1.0
        ' currency changes

        ''rev:mia oct 21 2013 -- code to check if cur and rate  is present in URL
        Dim curInUrl As String = ""
        Dim rateInUrl As String = ""
        If (Not String.IsNullOrEmpty(Request.QueryString("cur"))) Then
            curInUrl = Request.QueryString("cur")
        End If
        If (Not String.IsNullOrEmpty(Request.QueryString("rate"))) Then
            rateInUrl = Request.QueryString("rate")
        End If
        Dim IsCurAndRatePresent As Boolean = Not String.IsNullOrEmpty(rateInUrl) And Not String.IsNullOrEmpty(curInUrl)

        Me.BookingId = bookingId
        Try
            ''rev:mia April 1 2014 Payable to supplier messages
            Dim isCar As Boolean = False

            If GetVehicleReservation() = True Then
                ''rev:mia April 1 2014 Payable to supplier messages
                isCar = IIf(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vehicle.VehType.VehicleCategory.ToString = "C2", True, False)

                For Each sAgnCodeName As String In RS.VehRetResRSInfo.TPA_Extensions
                    If sAgnCodeName.Trim().ToUpper().Split("|"c)(1) = RS.VehRetResRSCore.VehReservation.VehSegmentCore.TPA_Extensions.Agent.Trim().ToUpper() Then
                        AgentCode = sAgnCodeName.Trim().ToUpper().Split("|"c)(0)
                        Exit For
                    End If
                Next
                ' currency changes
                If UserHasCurrencyOptions Then

                    If MRU_CurrencyCode = "" OrElse MRU_CurrencyMultiplier = 0.0 Then
                        ' this isnt from the price and avail screen
                        ' so figure out the default currency for the current booking's agent
                        Dim sAgentName, sAgentCode, sCurrentCountryOfTravelCurrency As String
                        sCurrentCountryOfTravelCurrency = RS.VehRetResRSCore.VehReservation.VehSegmentCore.TotalCharge.CurrencyCode

                        ' 1. Get Agent Code
                        sAgentName = RS.VehRetResRSCore.VehReservation.VehSegmentCore.TPA_Extensions.Agent.Trim().ToUpper()
                        For Each sAgnCodeName As String In RS.VehRetResRSInfo.TPA_Extensions
                            If sAgnCodeName.Trim().ToUpper().Split("|"c)(1) = sAgentName Then
                                sAgentCode = sAgnCodeName.Trim().ToUpper().Split("|"c)(0)
                                AgentCode = sAgentCode
                                Exit For
                            End If
                        Next

                        ' 2. 
                        Dim slCurrencyData As New SortedList



                        For Each oAgent As ExchangeRatesTypeAgent In ExchangeRatesLookupData
                            If oAgent.Code.ToUpper().Trim().Equals(sAgentCode) Then
                                For Each oCurr As ExchangeRatesTypeAgentCurr In oAgent.Curr
                                    If oCurr.FromCode = sCurrentCountryOfTravelCurrency Then
                                        ' this is the correct exchange rate set
                                        slCurrencyData.Add(oCurr.ToCode, oCurr.Rate)
                                    End If
                                Next
                            End If
                        Next

                        ' pick up the default currency data
                        For Each saCurrencySetting As String() In CurrencyOptionsData
                            Dim sAgnCode, sCurrencyList As String
                            sAgnCode = saCurrencySetting(0)

                            If sAgnCode.ToUpper().Trim().Equals(sAgentCode) Then
                                ' matching agent code
                                sCurrencyList = saCurrencySetting(1)
                                sDefaultCurrencyCode = saCurrencySetting(2)
                                dMultiplier = slCurrencyData(sDefaultCurrencyCode)
                                Exit For
                            End If
                        Next

                        ''rev:mia sept 20 2013 -- code to check if USD is present in currency exchage
                        If (sCurrentCountryOfTravelCurrency <> sDefaultCurrencyCode And sCurrentCountryOfTravelCurrency = "USD") Then
                            sDefaultCurrencyCode = "USD"
                            dMultiplier = 1.0
                        Else
                            If IsCurAndRatePresent Then
                                If (sDefaultCurrencyCode.TrimEnd <> curInUrl.TrimEnd) Then
                                    sDefaultCurrencyCode = curInUrl
                                    dMultiplier = CDec(rateInUrl)
                                End If

                            End If
                        End If
                    Else
                        ' this is from the availability page, use the MRU details
                        sDefaultCurrencyCode = MRU_CurrencyCode
                        dMultiplier = MRU_CurrencyMultiplier

                    End If

                End If
                ' currency changes




                ' Load stdconfirmation page with data 
                LoadControls(CustomerConfirmationMode)

                If CustomerConfirmationMode Then

                    If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges) Then
                        ' There are rental/ Agent charges 
                        FeesAndCharges2.InitChargesAndFees(RS, dMultiplier, sDefaultCurrencyCode, FeesAndCharges.ControlUse.CustomerRentalCharges)

                        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.RateQualifier.RateQualifier) Then
                            lblFlexRate.Visible = True
                            lblFlexRateValue.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.RateQualifier.RateQualifier.ToString() & "<BR>"
                        Else
                            lblFlexRate.Visible = False
                        End If

                        ' Show hr and fee control
                        AgentPaymentRow.Visible = True
                    Else
                        ' This is most likely a cancellation which hasn't incurred any charges
                        AgentPaymentRow.Visible = False
                    End If

                    If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees) Then
                        ' There are fee/customer charges

                        FeesAndCharges1.InitChargesAndFees(RS, dMultiplier, sDefaultCurrencyCode, FeesAndCharges.ControlUse.CustomerFees)

                        ' Show hr and rental charge control
                        customerPaymentRow.Visible = True
                    Else
                        customerPaymentRow.Visible = False
                    End If


                    'lblCustomertoPay.Text = "Customer To Pay"
                    lblCustomertoPay1.Text = "Customer to pay agent<br/><br/>"

                    ''rev:mia April 1 2014 Payable to supplier messages
                    ''lblCustomertoPay2.Text = "Customer to pay at pick up"
                    lblCustomertoPay2.Text = IIf(isCar = False, Config.GetMessage("CustomerToPaySupplier"), "Customer to pay at pick up")

                    btnViewAgentConfirmation.Visible = True
                    btnViewCustomerConfirmation.Visible = False
                Else
                    lblCustomertoPay1.Text = ""
                    btnViewAgentConfirmation.Visible = False
                    ''rev:mia April 1 2014 Payable to supplier messages
                    lblCustomertoPay2.Text = IIf(isCar = False, Config.GetMessage("PayableToSupplerDays"), "Customer to pay at pick up")

                    ' btnViewCustomerConfirmation.Visible = True
                    ' controlled from LoadControls() now

                    If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.VehicleCharges) Then
                        ' There are rental/ Agent charges charges

                        FeesAndCharges2.InitChargesAndFees(RS, dMultiplier, sDefaultCurrencyCode, FeesAndCharges.ControlUse.RentalCharges)

                        'Karel Jordaan  - Issue #007 - Change to package and flexrate
                        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.RateQualifier.RateQualifier) Then
                            lblFlexRate.Visible = True
                            lblFlexRateValue.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.RentalRate.RateQualifier.RateQualifier.ToString() & "<BR>"
                        Else
                            lblFlexRate.Visible = False
                        End If

                        ' Show hr and fee control
                        AgentPaymentRow.Visible = True
                    Else
                        ' This is most likely a cancellation which hasn't incurred any charges
                        AgentPaymentRow.Visible = False
                    End If

                    If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Fees) Then
                        ' There are fee/customer charges

                        FeesAndCharges1.InitChargesAndFees(RS, dMultiplier, sDefaultCurrencyCode, FeesAndCharges.ControlUse.Fees)


                        ' Show hr and rental charge control
                        customerPaymentRow.Visible = True
                    Else
                        customerPaymentRow.Visible = False
                    End If
                End If

            End If

            Select Case RS.VehRetResRSCore.VehReservation.VehSegmentCore.TPA_Extensions.Status.ToLower()
                Case "quotation"
                    btnViewAgentConfirmation.Text = "View Agent Quotation"
                    btnViewCustomerConfirmation.Text = "View Customer Quotation"
                Case "provisional"
                    btnViewAgentConfirmation.Text = "View Agent Provisional"
                    btnViewCustomerConfirmation.Text = "View Customer Provisional"
                Case "confirm"
                    btnViewAgentConfirmation.Text = "View Agent Confirmation"
                    btnViewCustomerConfirmation.Text = "View Customer Confirmation"
            End Select

            If printVersion = True Then
                btnContinue.Visible = False
                btnPrint.Visible = True

                btnViewAgentConfirmation.Visible = False
                btnViewCustomerConfirmation.Visible = False

                btnPrintFriendlyCustomerVersion.Visible = False
                btnPrintFriendly.Visible = False
            Else
                btnContinue.Visible = True

                btnPrintFriendlyCustomerVersion.Visible = CustomerConfirmationMode
                btnPrintFriendly.Visible = Not CustomerConfirmationMode

                btnPrint.Visible = False
            End If


            ConfirmationBookingId = bookingId
        Catch ex As Exception
            lblLoadError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try

        If (String.IsNullOrEmpty(ConfirmationRawContent)) Then
            ConfirmationRawContent = GetConfirmationContent()
            IsCustomerMode = CustomerConfirmationMode
        End If
        

    End Sub

    Private Function GetVehicleReservation() As Boolean
        Dim success As Boolean = False
        Dim RQ As New OTA_VehRetResRQ
        RQ.EchoToken = CStr(Session("SessionId"))
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        Dim OrqCoreType As New VehicleRetrieveResRQCoreType
        RQ.VehRetResRQCore = OrqCoreType

        Dim OuniqType As New UniqueID_Type
        OuniqType.Type = CStr(22)
        OuniqType.ID = BookingId
        OuniqType.Type = "NA"
        RQ.VehRetResRQCore.UniqueID = OuniqType


        Dim VehRetCoreType As New VehicleResRSAdditionalInfoType

        Dim persontype As New PersonNameType
        persontype.Surname = "NA"
        RQ.VehRetResRQCore.PersonName = persontype

        Dim tpa As New VehicleResAdditionalInfoType
        tpa.Agent = "NA"
        tpa.AgentRef = "NA"
        tpa.Package = "NA"    'not used but required as part of the request
        tpa.Status = "NA"    'not used but required as part of the request
        RQ.VehRetResRQCore.TPA_Extensions = tpa
        '------------------------------------------------------------------------------

        General.LogDebugNotes("Start StandardConfirmation: " & General.SerializeVehRetResRQObject(RQ))
        RS = Proxy.VehRetRes(RQ)
        General.LogDebugNotes("End StandardConfirmation: " & General.SerializeVehRetResRSObject(RS) & vbCrLf & vbCrLf)

        'Dim ser1 As XmlSerializer
        'Dim w1 As StringWriter
        'ser1 = New XmlSerializer(GetType(OTA_VehRetResRS))
        'w1 = New StringWriter
        'ser1.Serialize(w1, RS)
        'w1.ToString()


        If IsNothing(RS.Errors) Then

            success = True
        Else
            If RS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblLoadError.Text = RS.Errors(0).Tag.ToString
            success = False
        End If

        Return success
    End Function

    Private Sub LoadControls(Optional ByVal CustomerConfirmationMode As Boolean = False)

        Dim company As String = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vendor.CompanyShortName.ToLower
        Dim countrycode As String = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vendor.Code.ToLower
        Dim iCompany As Integer
        Dim iCountry As Integer
        Dim sStatusText As String

        If company = "maui" Then
            iCompany = 0
        ElseIf company = "britz" Then
            iCompany = 1

            ''rev:mia May 22 2012 -- added Mighty
        ElseIf (company = "backpacker" Or company.IndexOf("backpacker") <> -1) Then
            iCompany = 2
        ElseIf company.ToLower = "mighty" Or company.ToLower.Contains("mighty") = True Then
            ''rev:mia May 22 2012 -- added Mighty
            iCompany = 3

        ElseIf company.ToLower = "kea" Or company.ToLower.Contains("kea") = True Then
            iCompany = 4
        ElseIf company.ToLower = "alpha" Or company.ToLower.Contains("alpha") = True Then
            iCompany = 5
        ElseIf company.ToLower = "united" Or company.ToLower.Contains("united") = True Then
            iCompany = 6
        ElseIf company.ToLower = "econo" Or company.ToLower.Contains("econo") = True Then
            iCompany = 7
            ''rev:mia sept 20 2013 -- added britz-usa
        ElseIf company.ToLower = "britz usa" Or company.ToLower.Contains("brits usa") = True Then
            iCompany = 8
        End If

        ''rev:mia sept 20 2013 -- added britz-usa
        If countrycode = "au" Then
            iCountry = 0
        ElseIf countrycode = "nz" Then
            iCountry = 1
        ElseIf countrycode = "us" Then
            iCountry = 2
        End If


        sStatusText = RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status
        CompanyAddress1.InitCompanyAddress(CType(iCompany, CompanyAddress.Company), CType(iCountry, CompanyAddress.Country), sStatusText, CustomerConfirmationMode)
        lblRef.Text = Me.BookingId.ToString
        lblStatus.Text = RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status


        lblAgentName.Text = RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Agent
        lblAgentRef.Text = RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.AgentRef

        lblDate.Text = Today.Date.ToString("dd\/MM\/yy")

        ' Need to build an html table to display all booking notes 
        Dim vendorIndex As Integer = 0
        Dim iSubSectionIndex As Integer

        Dim headerHtml As New System.Text.StringBuilder
        Dim footerHtml As New System.Text.StringBuilder
        ' Start table for header
        headerHtml.Append("<table width=740 cellspacing=0 cellpadding=2>")
        footerHtml.Append("<table width=740 cellspacing=0 cellpadding=2>")
        For iSubSectionIndex = 0 To RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection.Length - 1

            Select Case RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).Paragraph(0).Name
                Case "RNTSTATUSTEXT"
                    If Not RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).SubTitle.ToString() = "N/A" Then
                        headerHtml.Append("<tr>")
                        headerHtml.Append("<td valign=top width=200 class=StdConfirmationHeading>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).SubTitle.ToString() & "</td>")
                        headerHtml.Append("<td  valign=topwidth=570 class=normal>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).Paragraph(0).Items(0).value & "</td>")
                        headerHtml.Append("</tr>")
                    Else
                        headerHtml.Append("<tr>")
                        headerHtml.Append("<td valign=top colspan=2 width=770 class=normal>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).Paragraph(0).Items(0).value & "</td>")
                        headerHtml.Append("</tr>")
                    End If

                Case "HEADER"

                    If Not RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).SubTitle.ToString() = "N/A" Then
                        headerHtml.Append("<tr>")
                        headerHtml.Append("<td valign=top width=200 class=StdConfirmationHeading>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).SubTitle.ToString() & "</td>")
                        headerHtml.Append("<td valign=top width=570 class=normal>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).Paragraph(0).Items(0).value & "</td>")
                        headerHtml.Append("</tr>")
                    Else
                        headerHtml.Append("<tr>")
                        headerHtml.Append("<td  valign=top colspan=2 width=770 class=normal>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).Paragraph(0).Items(0).value & "</td>")
                        headerHtml.Append("</tr>")
                    End If


                Case "FOOTER"

                    If Not RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).SubTitle.ToString() = "N/A" Then
                        footerHtml.Append("<tr>")
                        footerHtml.Append("<td valign=top width=200 class=StdConfirmationHeading>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).SubTitle.ToString() & "</td>")
                        footerHtml.Append("<td  valign=top width=570 class=normal>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).Paragraph(0).Items(0).value & "</td>")
                        footerHtml.Append("</tr>")
                    Else
                        footerHtml.Append("<tr>")
                        footerHtml.Append("<td  valign=top colspan=2 width=770 class=normal>" & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.VendorMessages(vendorIndex).SubSection(iSubSectionIndex).Paragraph(0).Items(0).value & "</td>")
                        footerHtml.Append("</tr>")
                    End If
                    'Nimesh Moved footerHtml.Append("<tr><td></td><td  valign=top colspan=2 width=770 align=center class=normal>To check in prior to arrival go to: <a href=https://selfcheckin.thlonline.com>https://selfcheckin.thlonline.com</a></td></tr>") 
                    'to just before close table tags, because quote returns two footer and SCI link and message should be displayed just once at the end and not after each footer message
            End Select
        Next

        ' Close table tags
        ' following line was moved on 2nd April 15 from within the loop to outside
        ' Changes by Nimesh on 13th April 15 to check for car and display self check in link if it is not a car
        Dim isCar As Boolean = False
        isCar = IIf(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vehicle.VehType.VehicleCategory.ToString = "C2", True, False)

        If isCar = False And IfStatusCanDisplay(RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status.ToLower()) Then
            footerHtml.Append("<tr><td></td><td  valign=top colspan=2 width=770 align=left class=normal>To check in prior to arrival go to: <a target=_blank href=https://selfcheckin.thlonline.com>https://selfcheckin.thlonline.com</a></td></tr>")
        End If
        ' end Changes by Nimesh on 13th April
        headerHtml.Append("</table>")
        footerHtml.Append("</table>")

        If headerHtml.Length < 15 Then
            ' Do nothing as there is no header info , just the table tags
        Else
            ' Display on screen
            divHeader.InnerHtml = headerHtml.ToString
        End If

        If footerHtml.Length < 15 Then
            ' Do nothing as there is no header info , just the table tags
        Else
            divFooter.InnerHtml = footerHtml.ToString
        End If

        Dim nameTitle As String = RS.VehRetResRSCore.VehReservation.Customer.Primary.PersonName.NameTitle(0).ToString
        Dim firstName As String = RS.VehRetResRSCore.VehReservation.Customer.Primary.PersonName.GivenName(0).ToString
        Dim surname As String = RS.VehRetResRSCore.VehReservation.Customer.Primary.PersonName.Surname

        ' Build name up according to what info we have
        Dim name As New System.Text.StringBuilder
        name.Append(nameTitle)

        If name.ToString = "" Then
            name.Append(firstName)
        Else
            name.Append(" " & firstName)
        End If

        If name.ToString = "" Then
            name.Append(surname)
        Else
            name.Append(" " & surname)
        End If


        lblCustomerName.Text = name.ToString

        lblCheckin.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnDateTime.ToString("dddd\, dd\ MMMM\ yyyy\, h\:mm\ tt")
        lblCheckout.Text = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpDateTime.ToString("dddd\, dd\ MMMM\ yyyy\, h\:mm\ tt")

        lblVehicle.Text = RS.VehRetResRSCore.VehResSummaries(0).Vehicle.VehMakeModel.Name.ToString

        Dim pickupAddress As New System.Text.StringBuilder
        ' Presuming 0= pickup address
        ' Address line 1
        pickupAddress.Append(RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(0).Address(0).AddressLine(0).ToString)
        ' Address line 2
        pickupAddress.Append(" " & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(0).Address(0).AddressLine(1).ToString)
        ' State
        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(0).Address(0).StateProv) Then
            pickupAddress.Append(" " & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(0).Address(0).StateProv.StateCode.ToString)
        End If

        lblCOAddress.Text = pickupAddress.ToString
        lblCOPhone.Text = RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(0).Telephone(0).PhoneNumber.ToString

        Dim returnAddress As New System.Text.StringBuilder
        ' Presuming 1= return address
        ' Address line 1
        returnAddress.Append(RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(1).Address(0).AddressLine(0).ToString)
        ' Address line 2
        returnAddress.Append(" " & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(1).Address(0).AddressLine(1).ToString)
        ' State
        If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(1).Address(0).StateProv) Then
            returnAddress.Append(" " & RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(1).Address(0).StateProv.StateCode.ToString)
        End If

        lblCIAddress.Text = returnAddress.ToString
        lblCIPhone.Text = RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(1).Telephone(0).PhoneNumber.ToString
        Dim openTime As String = RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(0).AdditionalInfo.OperationSchedules(0).Start.ToShortTimeString
        Dim closeTime As String = RS.VehRetResRSCore.VehReservation.VehSegmentInfo.LocationDetails(0).AdditionalInfo.OperationSchedules(0).End.ToShortTimeString

        lblOpHours.Text = openTime & " - " & closeTime

        If UserCanSeeCustomerConfirmations Then
            ' allowed to view customer confirmations
            btnViewCustomerConfirmation.Visible = True
            ' can view customer version of confirmation

            If RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.THL_CusConf IsNot Nothing _
               AndAlso _
               RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.THL_CusConf.ToUpper().Contains("YES") _
            Then
                'btnViewCustomerConfirmation.Enabled = True
                SelectedRateIsNettEvenThoughGrossOptionWasSelected = False
                'btnViewCustomerConfirmation.Text = "View Customer Confirmation"
            Else
                'btnViewCustomerConfirmation.Enabled = False
                SelectedRateIsNettEvenThoughGrossOptionWasSelected = True
                'btnViewCustomerConfirmation.Text = "View Customer Document"
            End If
        Else
            ' not allowed to view customer confirmations
            btnViewCustomerConfirmation.Visible = False
        End If


    End Sub

    Private Function IfStatusCanDisplay(ByVal statusCode As String) As Boolean
        Dim retValue As Boolean = False
        If statusCode = "confirm" Or statusCode = "checked out" Or statusCode = "checked in" Then
            retValue = True
        End If
        Return retValue
    End Function
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        If (Not String.IsNullOrEmpty(SendingEmailErrorMessage)) Then
            ''  If (SendingEmailErrorMessage.IndexOf("ERROR") = 0 And RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status <> "Quotation" And (Not IsCustomerMode) And SendingEmailErrorMessage.IndexOf("Please contact") > 0) Then
            If (RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status <> "Quotation" And (Not IsCustomerMode) And SendingEmailErrorMessage.IndexOf("Please contact") > 0) Then
                SendingEmailErrorMessage = SendingEmailErrorMessage.Replace("ERROR:", "")

                lblLoadError.Text = SendingEmailErrorMessage
            End If

        Else
            If lblLoadError.Text <> "" Then
                'hide everything except for error tag
                tblAll.Visible = False
            Else
                tblAll.Visible = True
            End If
        End If
        If (Not btnviewagentconfirmation.Visible) Then
            OnBubbleClick(e)
        End If
    End Sub

    Private Sub btnPrintFriendly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintFriendly.Click
        Dim urlBookingId As String = Server.UrlEncode(Me.BookingId)
        Dim url As String = "PrinterFriendlyPage.aspx?print=" & urlBookingId  'page=" & Me.Request.Url.ToString
        UIScripts.NewWindow(Me.Page, url, "610", "700", 0, 0, False, False, False, False, True, True)
    End Sub

    Private Sub btnPrintFriendlyCustomerVersion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintFriendlyCustomerVersion.Click
        Dim urlBookingId As String = Server.UrlEncode(Me.BookingId)
        Dim url As String = "PrinterFriendlyPage.aspx?print=" & urlBookingId & "&CustomerConfirmationMode=true" 'page=" & Me.Request.Url.ToString
        UIScripts.NewWindow(Me.Page, url, "610", "700", 0, 0, False, False, False, False, True, True)
    End Sub

    Protected Sub btnViewAgentConfirmation_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewAgentConfirmation.Click
        InitStandardConfirmation(bookingId:=Me.BookingId, CustomerConfirmationMode:=False)
        ''rev:mia: 11March2015-B2B Auto generated Aurora Booking
        ConfirmationRawContent = GetConfirmationContent()
        IsCustomerMode = False
        OnBubbleClick(e)
    End Sub

    Protected Sub btnViewCustomerConfirmation_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewCustomerConfirmation.Click
        InitStandardConfirmation(bookingId:=Me.BookingId, CustomerConfirmationMode:=True)
        ''rev:mia: 11March2015-B2B Auto generated Aurora Booking
        ConfirmationRawContent = GetConfirmationContent()
        IsCustomerMode = True
        OnBubbleClick(e)
    End Sub


#Region "rev:mia: 11March2015-B2B Auto generated Aurora Booking "

    Private Property ConfirmationBookingId As String
        Get
            Return ViewState("StandardConfirmation_BookingId")
        End Get
        Set(value As String)
            ViewState("StandardConfirmation_BookingId") = value
        End Set
    End Property

    Public Property IsCustomerMode As Boolean
        Get
            Return ViewState("IsCustomerMode")
        End Get
        Set(value As Boolean)
            ViewState("IsCustomerMode") = value
        End Set
    End Property

    Public Property ConfirmationRawContent As String
        Get
            Return ViewState("ConfirmationContent")
        End Get
        Set(value As String)
            ViewState("ConfirmationContent") = value
        End Set
    End Property

    Private Function GetConfirmationContent() As String
        Dim writer As New StringWriter
        Dim htmlwrite As HtmlTextWriter = New HtmlTextWriter(writer)
        tblAll.RenderControl(htmlwrite)
        Return writer.ToString()
    End Function


    Public Function GetSubject() As String
        If (String.IsNullOrEmpty(RS.EchoToken)) Then
            InitStandardConfirmation(ConfirmationBookingId, False, IsCustomerMode)
        End If
        Dim company As String = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vendor.CompanyShortName.ToUpper()
        Dim surname As String = RS.VehRetResRSCore.VehReservation.Customer.Primary.PersonName.Surname
        Dim booking As String = Me.BookingId.ToString
        Dim bookingstatus As String = RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status

        Return String.Concat(company, " - ", surname, "  ", booking, "  ", bookingstatus, " Request")
    End Function

    Public Event BubbleClick As EventHandler

    Protected Sub OnBubbleClick(e As EventArgs)
        RaiseEvent BubbleClick(Me, e)
    End Sub

    Private _SendingEmailErrorMessage As String
    Public Property SendingEmailErrorMessage As String
        Get
            Return _SendingEmailErrorMessage
        End Get
        Set(value As String)
            _SendingEmailErrorMessage = value
        End Set
    End Property

    Public ReadOnly Property GetBookingStatus As String
        Get
            If (String.IsNullOrEmpty(RS.EchoToken)) Then
                InitStandardConfirmation(ConfirmationBookingId, False, IsCustomerMode)
            End If
            Return RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status
        End Get
    End Property

#End Region



End Class
