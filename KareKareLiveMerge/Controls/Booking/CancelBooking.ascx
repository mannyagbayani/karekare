﻿<%@ Control Language="vb" AutoEventWireup="false" Codebehind="CancelBooking.ascx.vb" Inherits="THLAuroraWebInterface.CancelBooking" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<%@ Register TagPrefix="uc1" TagName="StandardConfirmation" Src="../Booking/StandardConfirmation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../General/Header.ascx" %>

<TABLE id="Table1" style="WIDTH: 646px; HEIGHT: 187px" cellSpacing="0" cellPadding="2"
	width="646" border="0">
    
    <tr>
        <td vAlign="top" class="normal">Cancellation confirmation email to be send to</td>
        <td>

            <asp:TextBox class="email ClickTaleSensitive" ID="emailAddressTxt" runat="server" Width="427px"></asp:TextBox>

        </td>
    </tr>
   <%-- <tr>
        <td vAlign="top" class="normal">Confirm address</td>
        <td>

            <asp:TextBox class="email ClickTaleSensitive" ID="confirmEmailAddressTxt" runat="server"></asp:TextBox>

        </td>
    </tr>--%>
	<TR>
		<TD vAlign="top" class="normal">Cancellation Reason</TD>
		<TD vAlign="top">
			<asp:TextBox id="txtReason" runat="server" Width="427px" Height="143px" TextMode="MultiLine"
				MaxLength="255"></asp:TextBox></TD>
	</TR>
    
	<TR>
		<TD vAlign="top"></TD>
		<TD vAlign="top">
			<asp:Button id="btnConfirm" runat="server" Text="Confirm" OnClientClick ="return ValidateEmailAddress(this);"></asp:Button>
			<asp:Button id="btnCancel" runat="server" Text="Cancel"></asp:Button>
			<asp:Button id="btnBackQandB" runat="server" Text="Back to Quotes and Bookings" Visible="False"
				Width="192px"></asp:Button></TD>
	</TR>
    <tr>
        <TD vAlign="top"></TD>
        <td vAlign="top">
        <%--<span class="warning">Note: Please enter your customer's email address carefully as this will be the address we send your confirmation to</span>--%>
            <br />

            <asp:Label ID="lblErrorMessage" runat="server" Font-Bold="True" cssclass="warning"></asp:Label>

        </td>
    </tr>
	<TR>
		<TD vAlign="top"></TD>
		<TD vAlign="top">
			<asp:Label cssclass="warning" id="lblLoadError" runat="server"></asp:Label>
		</TD>
	</TR>
</TABLE>


<div>
        <uc1:Header ID="Header1" runat="server" Visible ="false"></uc1:Header>
        <uc1:StandardConfirmation ID="StandardConfirmation1" runat="server" Visible="false" ></uc1:StandardConfirmation>
    </div>
