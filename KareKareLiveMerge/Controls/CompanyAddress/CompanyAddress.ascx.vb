'--------------------------------------------------------------------------------------
'   Class:          CompanyAddress
'   Description:    Displays company address depending on company and country specified
'--------------------------------------------------------------------------------------

''ADDED STUFFS
''rev:mia Aug 8 2012 -- ADDED KEA URL AND ITS ONLY FOR AUSTRALIA.
''rev:mia Oct 26 2012 -- ADDED United/Alpha/Econo

Partial Class CompanyAddress
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public Enum Company
        maui = 0
        britz = 1
        backpacker = 2
        mighty = 3  ''rev:mia May 22 2012 -- added Mighty
        kea = 4     ''rev:mia Aug 8 2012 -- added Kea
        alpha = 5
        united = 6
        econo = 7
        britzUS = 8 ''rev:mia sept 20 2013 -- added britz-us and roadbear
        roadbear = 9
    End Enum

    Public Enum Country
        au = 0
        nz = 1
        us = 2 ''rev:mia sept 20 2013 -- added us
    End Enum

    Private Const auCompany As String = "Tourism Holdings Australia Pty Ltd"
    Private Const nzCompany As String = "Tourism Holdings Limited"

    Private Const auAddressLine1 As String = "ABN 94 001 789 957"
    Private Const nzAddressLine1 As String = "Private Bag 92133"

    Private Const auAddressLine2 As String = "P O Box 4194"
    Private Const nzAddressLine2 As String = "36 Richard Pearse Drive, Mangere"

    Private Const auAddressLine3 As String = "Footscray West DC, Melbourne"
    Private Const nzAddressLine3 As String = "Auckland"

    Private Const auAddressLine4 As String = "VIC 3012"
    Private Const nzAddressLine4 As String = ""

    Private Const auPhone As String = "61 3 8379 8800 or 1800331454"
    Private Const nzPhone As String = "64-9-255 0620 or 0800651080"

    Private Const britzURL As String = "www.britz.com"
    Private Const mauiURL As String = "www.maui-rentals.com"
    Private Const backpackerURL As String = "www.backpackercampervan.com"
    Private Const mightyURLAU As String = "www.mightycampers.com.au"
    Private Const mightyURLNZ As String = "www.mightycampers.co.nz"

    ''rev:mia Aug 8 2012 -- added Kea
    Private Const keaURLAU As String = "www.aurentals.keacampers.com"
    Private Const keaURLNZ As String = "www.nzrentals.keacampers.com"


    Private Const auGSTNo As String = ""
    Private Const nzGSTNo As String = "GST Reg No 68 875 633"

    Private Const alphaURLNZ As String = "www.alphacampervans.co.nz"
    Private Const econoURLNZ As String = "www.econocampers.co.nz"
    Private Const unitedURLNZ As String = "www.unitedcampervans.co.nz"

    ''rev:mia sept 20 2013 -- added britz
    Private Const usGSTNo As String = ""
    Private Const usBritzURL As String = "http://www.britz-usa.com/"
    Private Const usAddressLine1 As String = "28404 Roadside Drive"
    Private Const usAddressLine2 As String = "Agoura Hills"
    Private Const usAddressLine3 As String = "CA 91301"
    Private Const usAddressLine4 As String = "United States"
    Private Const usCompany As String = "Tourism Holdings Limited"
    Private Const usPhone As String = "+1 855 427 4898 or +1 818 865 2925"

    ''rev:mia sept 20 2013 -- added RoadBear
    Private Const rbGSTNo As String = ""
    Private Const rbBritzURL As String = "http://www.roadbearrv.com"
    Private Const rbAddressLine1 As String = "28404 Roadside Drive "
    Private Const rbAddressLine2 As String = "Agoura Hills"
    Private Const rbAddressLine3 As String = "CA 91301"
    Private Const rbAddressLine4 As String = "United States"
    Private Const rbCompany As String = "Tourism Holdings Limited"
    Private Const rbPhone As String = "+1-818-865-2925 or +1-866-491-9853"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
    End Sub

    Public Sub InitCompanyAddress(ByVal company As Company, ByVal country As Country, ByVal sStatusText As String, Optional ByVal CustomerConfirmationMode As Boolean = False)
        ' Display company address according to company and country
        If CustomerConfirmationMode Then
            Select Case sStatusText.ToLower
                Case "quotation"
                    lblStatusText.Text = "CUSTOMER RENTAL QUOTATION"
                Case "confirm"
                    lblStatusText.Text = "CUSTOMER RENTAL CONFIRMATION"
                Case "provisional"
                    lblStatusText.Text = "CUSTOMER PROVISIONAL BOOKING"
            End Select
        Else
            Select Case sStatusText.ToLower
                Case "quotation"
                    lblStatusText.Text = "AGENT RENTAL QUOTATION"
                Case "confirm"
                    lblStatusText.Text = "AGENT RENTAL CONFIRMATION"
                Case "provisional"
                    lblStatusText.Text = "AGENT PROVISIONAL BOOKING"
            End Select
        End If
        'Select Case sStatusText.ToLower
        '    Case "quotation"
        '        lblStatusText.Text = "RENTAL QUOTATION"
        '    Case "confirm"
        '        lblStatusText.Text = "RENTAL CONFIRMATION"
        '    Case "provisional"
        '        lblStatusText.Text = "PROVISIONAL BOOKING"
        'End Select

        If company = company.britz And country = country.au Then
            LoadBritzAUDetails()
        ElseIf company = company.britz And country = country.nz Then
            LoadBritzNZDetails()
        ElseIf company = company.maui And country = country.au Then
            LoadMauiAUDetails()
        ElseIf company = company.maui And country = country.nz Then
            LoadMauiNZDetails()
        ElseIf company = company.backpacker And country = country.au Then
            LoadBackpackerAUDetails()
        ElseIf company = company.backpacker And country = country.nz Then
            LoadBackpackerNZDetails()

            ''rev:mia May 22 2012 -- added Mighty
        ElseIf company = company.mighty Then
            LoadMightDetails(CBool(IIf(country = country.nz, True, False)))

            ''rev:mia OCT 30 2012 -- CHANGE kea for all countries
            ''ElseIf company = company.kea And country = country.au Then
        ElseIf company = company.kea Then
            LoadKEADetails(CBool(IIf(country = country.nz, True, False)))
            ''rev:mia sept 20 2013 -- added britz-usa
        ElseIf company = CompanyAddress.Company.britzUS Then
            LoadBritzUSDetails()
        ElseIf company = CompanyAddress.Company.roadbear Then
            LoadRoabBearDetails()
        Else
            LoadGenericDetails(True, company)
        End If

    End Sub

    Private Sub LoadBritzAUDetails()
        lblCompany.Text = auCompany
        lblAddressLine1.Text = auAddressLine1
        lblAddressLine2.Text = auAddressLine2
        lblAddressLine3.Text = auAddressLine3
        lblAddressLine4.Text = auAddressLine4
        lblPhone.Text = auPhone
        lblURL.Text = britzURL & " " & auGSTNo
    End Sub

    Private Sub LoadBritzNZDetails()
        lblCompany.Text = nzCompany
        lblAddressLine1.Text = nzAddressLine1
        lblAddressLine2.Text = nzAddressLine2
        lblAddressLine3.Text = nzAddressLine3
        lblAddressLine4.Text = nzAddressLine4
        lblPhone.Text = nzPhone
        lblURL.Text = britzURL & " " & nzGSTNo
    End Sub

    Private Sub LoadMauiAUDetails()
        lblCompany.Text = auCompany
        lblAddressLine1.Text = auAddressLine1
        lblAddressLine2.Text = auAddressLine2
        lblAddressLine3.Text = auAddressLine3
        lblAddressLine4.Text = auAddressLine4
        lblPhone.Text = auPhone
        lblURL.Text = mauiURL & " " & auGSTNo
    End Sub

    Private Sub LoadMauiNZDetails()
        lblCompany.Text = nzCompany
        lblAddressLine1.Text = nzAddressLine1
        lblAddressLine2.Text = nzAddressLine2
        lblAddressLine3.Text = nzAddressLine3
        lblAddressLine4.Text = nzAddressLine4
        lblPhone.Text = nzPhone
        lblURL.Text = mauiURL & " " & nzGSTNo
    End Sub

    Private Sub LoadBackpackerAUDetails()
        lblCompany.Text = auCompany
        lblAddressLine1.Text = auAddressLine1
        lblAddressLine2.Text = auAddressLine2
        lblAddressLine3.Text = auAddressLine3
        lblAddressLine4.Text = auAddressLine4
        lblPhone.Text = auPhone
        lblURL.Text = backpackerURL & " " & auGSTNo
    End Sub

    Private Sub LoadBackpackerNZDetails()
        lblCompany.Text = nzCompany
        lblAddressLine1.Text = nzAddressLine1
        lblAddressLine2.Text = nzAddressLine2
        lblAddressLine3.Text = nzAddressLine3
        lblAddressLine4.Text = nzAddressLine4
        lblPhone.Text = nzPhone
        lblURL.Text = backpackerURL & " " & nzGSTNo
    End Sub

    ''rev:mia May 22 2012 -- added Mighty
    Private Sub LoadMightDetails(ByVal isNZ As Boolean)
        lblCompany.Text = CStr(IIf(isNZ = True, nzCompany, auCompany))
        lblAddressLine1.Text = CStr(IIf(isNZ = True, nzAddressLine1, auAddressLine1))
        lblAddressLine2.Text = CStr(IIf(isNZ = True, nzAddressLine2, auAddressLine2))
        lblAddressLine3.Text = CStr(IIf(isNZ = True, nzAddressLine3, auAddressLine3))
        lblAddressLine4.Text = CStr(IIf(isNZ = True, nzAddressLine4, auAddressLine4))
        lblPhone.Text = CStr(IIf(isNZ = True, nzPhone, auPhone))
        lblURL.Text = String.Concat(CStr(IIf(isNZ = True, mightyURLNZ, mightyURLAU)), " ", CStr(IIf(isNZ = True, nzGSTNo, auGSTNo)))
    End Sub

    ''rev:mia OCT 30 2012 -- CHANGE kea for all countries
    Private Sub LoadKEADetails(ByVal isNZ As Boolean)
        'lblCompany.Text = auCompany
        'lblAddressLine1.Text = auAddressLine1
        'lblAddressLine2.Text = auAddressLine2
        'lblAddressLine3.Text = auAddressLine3
        'lblAddressLine4.Text = auAddressLine4
        'lblPhone.Text = auPhone
        'lblURL.Text = String.Concat(keaURLAU, " ", auGSTNo)


        lblCompany.Text = CStr(IIf(isNZ = True, nzCompany, auCompany))
        lblAddressLine1.Text = CStr(IIf(isNZ = True, nzAddressLine1, auAddressLine1))
        lblAddressLine2.Text = CStr(IIf(isNZ = True, nzAddressLine2, auAddressLine2))
        lblAddressLine3.Text = CStr(IIf(isNZ = True, nzAddressLine3, auAddressLine3))
        lblAddressLine4.Text = CStr(IIf(isNZ = True, nzAddressLine4, auAddressLine4))
        lblPhone.Text = CStr(IIf(isNZ = True, nzPhone, auPhone))
        lblURL.Text = String.Concat(CStr(IIf(isNZ = True, keaURLNZ, keaURLAU)), " ", CStr(IIf(isNZ = True, nzGSTNo, auGSTNo)))
    End Sub

    ''rev:mia sept 20 2013 -- added britz-us
    Private Sub LoadBritzUSDetails()
        lblCompany.Text = usCompany
        lblAddressLine1.Text = usAddressLine1
        lblAddressLine2.Text = usAddressLine2
        lblAddressLine3.Text = usAddressLine3
        lblAddressLine4.Text = auAddressLine4
        lblPhone.Text = usPhone
        lblURL.Text = usBritzURL & " " & usGSTNo
    End Sub

    Private Sub LoadRoabBearDetails()
        lblCompany.Text = rbCompany
        lblAddressLine1.Text = rbAddressLine1
        lblAddressLine2.Text = rbAddressLine2
        lblAddressLine3.Text = rbAddressLine3
        lblAddressLine4.Text = rbAddressLine4
        lblPhone.Text = rbPhone
        lblURL.Text = rbBritzURL & " " & rbGSTNo
    End Sub



    ''rev:mia Oct 26 2012 -- ADDED United/Alpha/Econo
    Private Sub LoadGenericDetails(ByVal isNZ As Boolean, company As Company)
        lblCompany.Text = CStr(IIf(isNZ = True, nzCompany, auCompany))
        lblAddressLine1.Text = CStr(IIf(isNZ = True, nzAddressLine1, auAddressLine1))
        lblAddressLine2.Text = CStr(IIf(isNZ = True, nzAddressLine2, auAddressLine2))
        lblAddressLine3.Text = CStr(IIf(isNZ = True, nzAddressLine3, auAddressLine3))
        lblAddressLine4.Text = CStr(IIf(isNZ = True, nzAddressLine4, auAddressLine4))
        lblPhone.Text = CStr(IIf(isNZ = True, nzPhone, auPhone))

        Dim url As String = ""

        If company = company.united Then
            url = unitedURLNZ
        ElseIf company = company.alpha Then
            url = alphaURLNZ
        ElseIf company = company.econo Then
            url = econoURLNZ
        End If
        lblURL.Text = String.Concat(url, " ", nzGSTNo)
    End Sub

End Class
