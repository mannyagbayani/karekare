<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Header.ascx.vb" Inherits="THLAuroraWebInterface.Header"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table id="Table1" width="750" height="30" border="0" cellpadding="0" cellspacing="0"
    bgcolor="#da471f">
    <tr>
        <td colspan="6" width="98%" height="92" align="left" valign="top" class="headBannerDummy" style="background-image:url('../../Images/bANDm_header.jpg')">
            <asp:HyperLink ID="hypPrice" CssClass="Nav oldNavLnk" runat="server" style="display:none;" NavigateUrl="../../Content/PriceAndAvailability.aspx">Old Price & Availability</asp:HyperLink>
            <asp:Image ID="imgHeader" runat="server" Width="752px" style="display:none;"></asp:Image>
        </td>
    </tr>
    <tr class="Nav" height="20">
        <td width="150" class="NavBckgrd" id="td1" runat="server">
            <asp:HyperLink ID="hypQuotes" CssClass="Nav" runat="server" NavigateUrl="../../Content/QuotesAndBookings.aspx">Quotes & Bookings</asp:HyperLink>
        </td>
		<!--
        <td width="150" class="NavBckgrd" id="td7" runat="server">
            <asp:HyperLink ID="hypAllQuotes" CssClass="Nav" runat="server" NavigateUrl="../../Content/BookingReports.aspx">Booking Reports</asp:HyperLink>
        </td>
		-->
        <td width="150" class="NavBckgrd" id="td2" runat="server">
            
            <span>&nbsp;</span>
            <a class="Nav" href="../../content/Availability.aspx">Price & Availability</a>
        </td>
        <td width="150" class="NavBckgrd" id="td3" runat="server">
            <asp:HyperLink ID="hypyDetails" CssClass="Nav" runat="server" NavigateUrl="../../Content/EditMyDetails.aspx">Edit My Details</asp:HyperLink>
        </td>
        <td width="150" class="NavBckgrd" id="td4" runat="server">
            <asp:HyperLink ID="hypMaintainAgent" CssClass="Nav" runat="server" NavigateUrl="../../Content/MaintainAgentUsers.aspx">Maintain other Agent Users</asp:HyperLink>
        </td>
        <td width="150" class="NavBckgrd" id="td6" runat="server">
            <asp:HyperLink ID="hypB2BOptions" CssClass="Nav" runat="server" NavigateUrl="../../Content/B2BOptions.aspx">B2B Options</asp:HyperLink>
        </td>
        <td width="150" class="NavBckgrd" id="Td5" runat="server" align="right">
            <asp:HyperLink ID="hypLogOff" CssClass="Nav" runat="server" NavigateUrl="../../Content/AgentLogin.aspx">Log Off</asp:HyperLink>&nbsp;
        </td>
    </tr>
    <tr height="20">
        <td class="HeaderWhite" bgcolor="#666666" colspan="6" align="right">
            Welcome
            <asp:Label ID="lblname" runat="server" Font-Bold="True"></asp:Label>&nbsp;
        </td>
    </tr>
</table>
