'--------------------------------------------------------------------------------------
'   Class:          Header
'   Description:    The appropriate header image is loaded according to the company specified
'                   in session. Also displays the navigation for the site (dependent on type of user)
'                   and the name of  the current user.
'--------------------------------------------------------------------------------------

Partial Class Header
    Inherits System.Web.UI.UserControl
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA


#Region "Currency Properties"
    Public Property CurrencyOptionsData() As ArrayList
        Get
            If Session("CurrencyOptionsData") IsNot Nothing Then
                Return CType(Session("CurrencyOptionsData"), ArrayList)
            Else
                Return New ArrayList
            End If
        End Get
        Set(ByVal value As ArrayList)
            Session("CurrencyOptionsData") = value
        End Set
    End Property

    Public Property ExchangeRatesLookupData() As ExchangeRatesTypeAgent()
        Get
            If Session("ExchangeRatesLookupData") IsNot Nothing Then
                Return CType(Session("ExchangeRatesLookupData"), ExchangeRatesTypeAgent())
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As ExchangeRatesTypeAgent())
            Session("ExchangeRatesLookupData") = value
        End Set
    End Property

    Public Property MRU_CurrencyMultiplier() As Double
        Get
            If Session("MRU_CurrencyMultiplier") IsNot Nothing Then
                Return CDbl(Session("MRU_CurrencyMultiplier"))
            Else
                Return 0.0
            End If
        End Get
        Set(ByVal value As Double)
            Session("MRU_CurrencyMultiplier") = value
        End Set
    End Property

    Public Property MRU_CurrencyCode() As String
        Get
            Return CStr(Session("MRU_CurrencyCode"))
        End Get
        Set(ByVal value As String)
            Session("MRU_CurrencyCode") = value
        End Set
    End Property

    Public Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("UserHasCurrencyOptions") = value
        End Set
    End Property

#End Region



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim RS As New THL_RetUsersRS
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If IsNothing(Session("POS")) Then
            ' User is either not logged in or their session has expired 
            ' Redirect 
            Response.Redirect("AccessDenied.aspx")
        Else
            Dim RQ As New THL_RetUsersRQ
            If Not Page.IsPostBack Then
                ' Retrieve the logged in users name for display

                ' Fullname added to cache to improve performancezz
                ' No need to use webservice everytime.
                ' Need to invalidate cache when user updates theier own details

                If Not IsNothing(Session("FullName")) Then
                    lblname.Text = CStr(Session("FullName"))
                Else
                    RQ.POS = New SourceType() {CType(Session("POS"), SourceType)} ' Only pos required in request
                    Try
                        RS = Proxy.RetUsers(RQ) ' All user data retrieved based on user passed in pos

                        Dim FullName As String = RS.Users(0).FirstName & " " & RS.Users(0).LastName
                        Session("FullName") = FullName
                        Session("IsSuperUser") = RS.Users(0).IsSuperUser.ToString
                        lblname.Text = FullName

                        ' Currency
                        If Not String.IsNullOrEmpty(RS.Users(0).AgentDutyCode) _
                           AndAlso _
                           RS.Users(0).AgentDutyCode.Split("~"c).Length >= 5 _
                           AndAlso _
                           RS.Users(0).AgentDutyCode.Split("~"c)(4).Trim().ToUpper().Equals("YES") _
                           AndAlso _
                           RS.Users(0).CurrOptions IsNot Nothing _
                           AndAlso _
                           RS.Users(0).CurrOptions.Length > 0 _
                           AndAlso _
                           RS.ExchangeRates IsNot Nothing _
                        Then
                            ' currency option is enabled and there are some currency options
                            Dim alTemp As New ArrayList
                            For Each oCurrOption As CurrOptionsTypeAgent In RS.Users(0).CurrOptions
                                alTemp.Add(New String() {oCurrOption.code, oCurrOption.currList, oCurrOption.default})
                            Next
                            CurrencyOptionsData = alTemp

                            ExchangeRatesLookupData = RS.ExchangeRates

                            UserHasCurrencyOptions = True
                        Else
                            ' Currency Option Disabled
                            UserHasCurrencyOptions = False
                        End If

                        ' Currency

                    Catch ex As Exception
                        ExceptionManager.Publish(ex)
                    End Try
                End If


                DisplayLinks()

                If CStr(Session("TypeOfUser")) = "customer" Then
                    ' This is b2c
                    If CStr(Session("Company")) = "britz" Then
                        ' Load britz image
                        imgHeader.ImageUrl = "~/images/britz_header_nz.jpg"
                    Else
                        ' Load maui image
                        imgHeader.ImageUrl = "~/images/maui_header_nz.jpg"
                    End If
                Else
                    ' This is b2b, load universal image
                    imgHeader.ImageUrl = "~/images/bANDm_header.jpg"
                End If

            End If
        End If


    End Sub

    Private Sub DisplayLinks()
        ' Dsiplay hyperlinks based on type of user
        If CBool(Session("IsSuperUser")) = True AndAlso CStr(Session("TypeOfUser")) = "agent" Then
            'display maintain users
            td1.Width = "120px"
           '' td7.Width = "120px"
            td2.Width = "120px"
            td3.Width = "120px"
            td4.Width = "150px"
            td4.Visible = True
            Td5.Width = "100px"
            If UserHasCurrencyOptions Then
                td6.Visible = True
                td6.Width = "110px"
            Else
                td6.Visible = False
            End If
        Else
            td1.Width = "187px"
          ''  td7.Width = "187px"
            td2.Width = "187px"
            td3.Width = "187px"
            td4.Visible = False
            Td5.Width = "187px"
            td6.Visible = False
        End If

        If CStr(Session("TypeOfUser")) = "agent" Then
            ' Set log off to navigate back to agent login
            hypLogOff.NavigateUrl = "../../Content/agentLogin.aspx"
        Else
            ' Set log off to navigat back to customerlogin
            hypLogOff.NavigateUrl = "../../Content/customerLogin.aspx"

        End If

    End Sub

End Class
