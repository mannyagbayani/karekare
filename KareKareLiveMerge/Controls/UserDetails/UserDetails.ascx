<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UserDetails.ascx.vb"
    Inherits="THLAuroraWebInterface.UserDetails" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<script type="text/javascript">
    function ValidateGrossNettStuff()
    {
//        var chkGross = document.getElementById("UserDetails1_cblGrossNettSettings_0");
//        var chkNett = document.getElementById("UserDetails1_cblGrossNettSettings_1");
        var chkGross = document.getElementById("<% =cblGrossNettSettings.ClientID %>").getElementsByTagName("input")[0];
        var chkNett = document.getElementById("<% =cblGrossNettSettings.ClientID %>").getElementsByTagName("input")[1];
        
        var divGrossNettDefaultRadioButtonsHolder = document.getElementById("<% =divGrossNettDefaultRadioButtonsHolder.ClientID %>");
        var lblGrossNettDefault = document.getElementById("<% =lblGrossNettDefault.ClientID %>");
    
        if(!chkGross.checked || !chkNett.checked)
        {
            divGrossNettDefaultRadioButtonsHolder.style.display = "none";
            lblGrossNettDefault.style.display = "none";
        }
        else
        {
            divGrossNettDefaultRadioButtonsHolder.style.display = "block";
            lblGrossNettDefault.style.display = "block";
        }
    }

        

</script>

<table id="Table3" cellspacing="0" cellpadding="0" width="750" border="0">
    <tr valign="top">
        <td style="width: 389px" valign="top">
            <table id="Table1" style="width: 332px; height: 163px" cellspacing="0" cellpadding="1"
                width="332" border="0">
                <tr>
                    <td class="CellHeading" valign="top">
                        <asp:Label ID="lblFirstName" runat="server">First name</asp:Label>
                    </td>
                    <td style="width: 186px" valign="top">
                        <asp:TextBox ID="txtFirstName" runat="server" Width="180px" MaxLength="64"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter a First Name"
                            ControlToValidate="txtFirstName"><</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="CellHeading" valign="top">
                        <asp:Label ID="lblSurname" runat="server">Surname</asp:Label>
                    </td>
                    <td style="width: 186px" valign="top">
                        <asp:TextBox ID="txtSurname" runat="server" Width="180px" MaxLength="64"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter a Surname"
                            ControlToValidate="txtSurname"><</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="CellHeading" valign="top">
                        <asp:Label ID="lblPassword" runat="server">Password</asp:Label>
                    </td>
                    <td style="width: 186px" valign="top">
                        <asp:TextBox ID="txtPassword" runat="server" Width="180px" MaxLength="64" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="valPassword" runat="server" ErrorMessage="Please enter a Password"
                            ControlToValidate="txtPassword"><</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="CellHeading" valign="top">
                        <asp:Label ID="lblConfirmPassword" runat="server">Confirm Password</asp:Label>
                    </td>
                    <td style="width: 186px" valign="top">
                        <asp:TextBox ID="txtConfirmPassword" runat="server" Width="180px" MaxLength="64"
                            TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="valRequiredPassword" runat="server" ErrorMessage="Please Confirm Password"
                            ControlToValidate="txtConfirmPassword"><</asp:RequiredFieldValidator><asp:CompareValidator
                                ID="valComparePasswords" runat="server" ErrorMessage="Passwords do not match"
                                ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword"><</asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="CellHeading" valign="top">
                        <asp:Label ID="lblEmail" runat="server">Email Address</asp:Label>
                    </td>
                    <td style="width: 186px" valign="top">
                        <asp:TextBox ID="txtEmail" runat="server" Width="180px" MaxLength="64"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter an Email Address"
                            ControlToValidate="txtEmail"><</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please enter a valid email address"
                                ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"><</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <%--Super User Editable Gross Nett Changes--%>
                <tr id="trGrossNettSettings" runat="server">
                    <td class="CellHeading" valign="top">
                        <asp:Label ID="lblGrossNettSettings" runat="server">Gross/Nett Settings</asp:Label>
                    </td>
                    <td style="width: 186px" valign="top">
                        <asp:CheckBoxList ID="cblGrossNettSettings" runat="server" RepeatDirection="Horizontal"
                            RepeatColumns="2" CssClass="GrossNettLabel">
                            <asp:ListItem Text="Gross Amount" Value="Gross" />
                            <asp:ListItem Text="Nett Amount" Value="Nett" />
                        </asp:CheckBoxList>
                        <asp:Label ID="lblGrossNettEnabledSetting" runat="server" CssClass="GrossNettLabel" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr id="trGrossNettDefaults" runat="server">
                    <td class="CellHeading" valign="top">
                        <asp:Label ID="lblGrossNettDefault" runat="server">Gross/Nett Default</asp:Label>
                    </td>
                    <td style="width: 186px" valign="top">
                        <div runat="server" id="divGrossNettDefaultRadioButtonsHolder">
                            <asp:RadioButtonList ID="rdlGrossNettDefault" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                CssClass="GrossNettLabel">
                                <asp:ListItem Text="Gross Amount" Value="Gross"></asp:ListItem>
                                <asp:ListItem Text="Nett Amount" Value="Nett"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <asp:Label CssClass="GrossNettLabel" ID="lblGrossNettDefaultSetting" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
                <%--Super User Editable Gross Nett Changes--%>
                <%--<TR>
					<TD class="CellHeading" vAlign="top"><asp:label id="Label1" runat="server">Total Display</asp:label></TD>
					<TD style="WIDTH: 186px" vAlign="top"><asp:dropdownlist id="ddTotalDisplay" runat="server" Width="176px">
							<asp:ListItem Value="Both" Selected="True">Both</asp:ListItem>
							<asp:ListItem Value="Gross">Gross</asp:ListItem>
							<asp:ListItem Value="Nett">Nett</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD><asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter an Email Address"><</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter a valid email address"
							ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"><</asp:regularexpressionvalidator></TD>
				</TR>--%>
                <tr>
                    <td class="CellHeading">
                    </td>
                    <td style="width: 186px" valign="top" colspan="1">
                        <asp:Button ID="btnCancel" runat="server" Width="88px" Text="Cancel" CausesValidation="False">
                        </asp:Button><asp:Button ID="btnUpdate" runat="server" Width="88px" Text="Save">
                        </asp:Button>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top" width="100%">
            <table id="tblAgents" cellspacing="0" cellpadding="2" width="100%" border="0" runat="server">
                <tr>
                    <td class="PriceAndAvailabilityRow" style="width: 25px" valign="top" nowrap align="left">
                        Default
                    </td>
                    <td class="PriceAndAvailabilityRow" valign="top" >
                        Agent List
                    </td>
                </tr>
                <tr>
                    <td style="width: 25px">
                        <asp:RadioButtonList ID="radDefaultAgent" runat="server" Width="25px" TextAlign="Left" 
                            ForeColor="White" style="white-space:nowrap;">
                        </asp:RadioButtonList>
                    </td>
                    <td >
                        <asp:CheckBoxList ID="chkAgents" runat="server" CssClass="NormalDDL">
                        </asp:CheckBoxList>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 321px" colspan="2">
            <asp:Label ID="lblWarning" runat="server" Width="288px" CssClass="warning"></asp:Label><asp:ValidationSummary
                ID="ValidationSummary1" runat="server" Width="175px"></asp:ValidationSummary>
        </td>
    </tr>
</table>
