Imports System.Xml

'--------------------------------------------------------------------------------------
'   Class:          SignIn
'   Description:    This control is responsible for displaying and updating users information. 
'                   As well as  deleting and inserting users
'                   This control relies on the Session("UserData") (THL_RetUsersRS) , been set externally 
'                   i.e EditMyDetails.aspx, MaintainAgentUsers.aspx  
'--------------------------------------------------------------------------------------

Partial Class UserDetails
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public Event CancelClicked()
    Public Event SaveClicked(ByVal userId As String)
    Public Event DeletePerformed()

    Private Property UserData() As String
        Get
            Return CStr(Session("UserData"))
        End Get
        Set(ByVal value As String)
            Session("UserData") = value
        End Set
    End Property

    Private Property UserId() As String
        Get
            Return CStr(ViewState("UserId"))
        End Get
        Set(ByVal Value As String)
            ViewState("UserId") = Value
        End Set
    End Property

    Private Property DefaultAgentCode() As String
        Get
            Return CStr(ViewState("DefaultAgentCode"))
        End Get
        Set(ByVal Value As String)
            ViewState("DefaultAgentCode") = Value
        End Set
    End Property
    Private Property AgentList() As String()
        Get
            Return CType(ViewState("AgentList"), String())
        End Get
        Set(ByVal Value As String())
            ViewState("AgentList") = Value
        End Set
    End Property

    Private Property IsSuperUser() As Boolean
        Get
            Return CBool(ViewState("IsSuperUser"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsSuperUser") = Value
        End Set
    End Property

    Private Property UserRSIndex() As Integer
        Get
            Return CInt(ViewState("UserRSIndex"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("UserRSIndex") = Value
        End Set
    End Property

    Private Property Password() As String
        Get
            Return CStr(ViewState("Password"))
        End Get
        Set(ByVal Value As String)
            ViewState("Password") = Value
        End Set
    End Property

    Public Property EditingState() As UserDetails.State
        Get
            Return CType(ViewState("EditingState"), UserDetails.State)
        End Get
        Set(ByVal Value As UserDetails.State)
            ViewState("EditingState") = Value
        End Set
    End Property

#Region "Customer Confirmation Changes"
    ' Customer Confirmation Changes

    Const GROSS_RADIO_BUTTON_INDEX As Int16 = 0
    Const NETT_RADIO_BUTTON_INDEX As Int16 = 1

    Public ReadOnly Property GrossNettDefault() As String
        Get
            If Session("GrossNettDefault") IsNot Nothing Then
                Return CStr(Session("GrossNettDefault"))
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property LoggedInUserIsSuperUser() As Boolean
        Get
            If Session("IsSuperUser") IsNot Nothing Then
                Return CBool(Session("IsSuperUser"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property UserCanSeeCustomerConfirmations() As Boolean
        Get
            If Session("UserCanSeeCustomerConfirmations") IsNot Nothing Then
                Return CBool(Session("UserCanSeeCustomerConfirmations"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property UserCanSeeGrossNett() As Boolean
        Get
            If Session("UserCanSeeGrossNett") IsNot Nothing Then
                Return CBool(Session("UserCanSeeGrossNett"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property GrossModeEnabled() As Boolean
        Get
            If Session("GrossModeEnabled") IsNot Nothing Then
                Return CBool(Session("GrossModeEnabled"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property NettModeEnabled() As Boolean
        Get
            If Session("NettModeEnabled") IsNot Nothing Then
                Return CBool(Session("NettModeEnabled"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property GrossModeSelected() As Boolean
        Get
            If Session("GrossModeSelected") IsNot Nothing Then
                Return CBool(Session("GrossModeSelected"))
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property NettModeSelected() As Boolean
        Get
            If Session("NettModeSelected") IsNot Nothing Then
                Return CBool(Session("NettModeSelected"))
            Else
                Return False
            End If
        End Get
    End Property

    Public Property CurrentlyUnderEditUserCanSeeCustomerConfirmation() As Boolean
        Get
            If Session("CurrentlyUnderEditUserCanSeeCustomerConfirmation") IsNot Nothing Then
                Return CBool(Session("CurrentlyUnderEditUserCanSeeCustomerConfirmation"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("CurrentlyUnderEditUserCanSeeCustomerConfirmation") = value
        End Set
    End Property

    Public Property CurrentlyUnderEditUserCanSeeGrossNett() As Boolean
        Get
            If Session("CurrentlyUnderEditUserCanSeeGrossNett") IsNot Nothing Then
                Return CBool(Session("CurrentlyUnderEditUserCanSeeGrossNett"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("CurrentlyUnderEditUserCanSeeGrossNett") = value
        End Set
    End Property

    ' Customer Confirmation Changes
#End Region

#Region "Currency Changes"
    Public Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("UserHasCurrencyOptions") = value
        End Set
    End Property

    Public Property CurrencyOptionsData() As ArrayList
        Get
            If Session("CurrencyOptionsData") IsNot Nothing Then
                Return CType(Session("CurrencyOptionsData"), ArrayList)
            Else
                Return New ArrayList
            End If
        End Get
        Set(ByVal value As ArrayList)
            Session("CurrencyOptionsData") = value
        End Set
    End Property
#End Region


    Public Enum State
        NewUser = 0
        ModifyUser = 1
        DeleteUser = 2
    End Enum

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblWarning.Text = ""
    End Sub


    Public Sub InitNew()
        'Initialise form for adding a new user
        valComparePasswords.Enabled = True
        valRequiredPassword.Enabled = True
        valPassword.Enabled = True
        ClearFormFields()

        If CStr(Session("TypeOfUser")) = "customer" Then
            ' Don't show agent lists if a customer
            tblAgents.Visible = False
        Else
            tblAgents.Visible = True
            LoadAgentLists()
        End If

        ' customer confirmation change
        If LoggedInUserIsSuperUser Then
            lblGrossNettEnabledSetting.Visible = False
            lblGrossNettDefaultSetting.Visible = False

            trGrossNettSettings.Visible = True
            trGrossNettDefaults.Visible = True

            For Each chkOption As ListItem In cblGrossNettSettings.Items
                chkOption.Attributes.Add("onclick", "ValidateGrossNettStuff();")
                chkOption.Selected = False
            Next

            ' set defaults in radio list
            rdlGrossNettDefault.Items(GROSS_RADIO_BUTTON_INDEX).Selected = False
            rdlGrossNettDefault.Items(NETT_RADIO_BUTTON_INDEX).Selected = True

            cblGrossNettSettings.Visible = True
            lblGrossNettSettings.Visible = True
            lblGrossNettDefault.Visible = True
            rdlGrossNettDefault.Visible = True

                ' hide radio button row
            divGrossNettDefaultRadioButtonsHolder.Style("display") = "none"
            lblGrossNettDefault.Style("display") = "none"
        
        End If

        ' We are creating a new user
        Me.EditingState = State.NewUser
    End Sub

    Public Sub InitModify(ByVal UserId As String)
        'Initialise form for modifying an existing user
        ClearFormFields()
        ' Turn off validators for passwords as password text boxes are not populated 
        valComparePasswords.Enabled = False
        valRequiredPassword.Enabled = False
        valPassword.Enabled = False

        ' We are modifing a user
        Me.EditingState = State.ModifyUser
        ' Store the users id for use later
        Me.UserId = UserId

        ' Retrieve USer information and set appropriote fields
        LoadUserData(UserId)

        If CStr(Session("TypeOfUser")) = "customer" Then
            ' Don't show agent lists if a customer
            tblAgents.Visible = False
        Else
            tblAgents.Visible = True
            ' Load agent radio and checkbox lists 
            LoadAgentLists()
            ' And select correct agents for selectd userid
            SetAgentLists()
        End If


    End Sub

    Public Function InitDelete(ByVal userId As String) As Boolean
        ClearFormFields()
        ' We are deleting a user
        Me.EditingState = State.DeleteUser

        LoadUserData(userId)
        ' Do the delete
        PerformUpdateInsertDelete()

        If lblWarning.Text = "" Then
            Return True 'no errors
        Else
            btnUpdate.Enabled = False
            Return False ' errors
        End If

    End Function

    Private Sub LoadUserData(ByVal UserId As String)
        ' Retrieve user information based on userid 
        Dim RS As THL_RetUsersRS = CType(General.DeserializeRetUsersRSObject(Me.UserData, RS), THL_RetUsersRS)
        Dim i As Integer
        Dim index As Integer

        ' Get the user index where the userid's match
        For i = 0 To RS.Users.Length - 1
            If RS.Users(i).UserId = UserId Then
                index = i
                Exit For
            End If
        Next

        Me.UserRSIndex = index ' Used to identify user in array

        txtFirstName.Text = RS.Users(index).FirstName
        txtSurname.Text = RS.Users(index).LastName
        txtEmail.Text = RS.Users(index).EmailAddress
        txtPassword.Text = RS.Users(index).PassWord

        'Karel Jordaan 9Oct2006
        '----------------------------------------------
        'ddTotalDisplay.SelectedValue = RS.Users(index).TotalDisplay.ToString
        '-----------------------------------------------

        If CStr(Session("TypeOfUser")) = "customer" Then
            ' Need to store default agent code and agent list for updating
            ' as these will not be displayed on the page
            Me.DefaultAgentCode = RS.Users(index).DefAgentCode
            Me.AgentList = RS.Users(index).Agents
        End If

        Me.Password = txtPassword.Text
        Me.UserId = UserId
        Me.IsSuperUser = RS.Users(index).IsSuperUser

        ' Super User Editable Gross Nett Changes
        'cblGrossNettSettings.Enabled = Me.IsSuperUser

        Dim saCustomerConfAndGrossNettSettings As String()
        saCustomerConfAndGrossNettSettings = RS.Users(index).AgentDutyCode.Split("~"c)

        ' 0 - Customer Confirmation Allowed? Yes/No
        ' 1 - Gross+Nett Allowed? Yes/No
        ' 2 - Gross/Nett/Both?  G,N
        ' 3 - default option out of Gross/Nett?  G
        ' 4 - currency option? Yes/No
        ' 5 - currency data

        CurrentlyUnderEditUserCanSeeCustomerConfirmation = (saCustomerConfAndGrossNettSettings(0).ToUpper().Trim() = "YES")
        CurrentlyUnderEditUserCanSeeGrossNett = (saCustomerConfAndGrossNettSettings(1).ToUpper().Trim() = "YES")
        Dim bGrossModeEnabled As Boolean = (saCustomerConfAndGrossNettSettings(2).ToUpper().Trim().Contains("G"))
        Dim bNettModeEnabled As Boolean = (saCustomerConfAndGrossNettSettings(2).ToUpper().Trim().Contains("N"))
        Dim bGrossOptionIsDefault As Boolean = (Left(saCustomerConfAndGrossNettSettings(3).ToUpper().Trim(), 1) = "G")
        Dim bNettOptionIsDefault As Boolean = (Left(saCustomerConfAndGrossNettSettings(3).ToUpper().Trim(), 1) = "N")

        ' Currency
        Try
            Dim bUserHasCurrencyOptions As Boolean = (saCustomerConfAndGrossNettSettings(4).ToUpper().Trim() = "YES")
            If bUserHasCurrencyOptions Then
                Dim alCurrencyData As New ArrayList
                For Each sItem As String In saCustomerConfAndGrossNettSettings(5).Split(","c)
                    alCurrencyData.Add(sItem.Split("|"c))
                Next
                CurrencyOptionsData = alCurrencyData
            End If
        Catch ex As Exception

        End Try
        ' Currency

        rdlGrossNettDefault.Items(GROSS_RADIO_BUTTON_INDEX).Selected = bGrossOptionIsDefault
        rdlGrossNettDefault.Items(NETT_RADIO_BUTTON_INDEX).Selected = bNettOptionIsDefault

        If Not LoggedInUserIsSuperUser Then

            trGrossNettSettings.Visible = CurrentlyUnderEditUserCanSeeGrossNett
            trGrossNettDefaults.Visible = CurrentlyUnderEditUserCanSeeGrossNett

            For Each chkOption As ListItem In cblGrossNettSettings.Items
                chkOption.Enabled = False
            Next
            cblGrossNettSettings.Visible = False
            lblGrossNettEnabledSetting.Visible = CurrentlyUnderEditUserCanSeeGrossNett

            If bGrossModeEnabled Then
                If bNettModeEnabled Then
                    ' both
                    lblGrossNettEnabledSetting.Text = "Gross, Nett"
                Else
                    ' only gross
                    lblGrossNettEnabledSetting.Text = "Gross"
                End If
            Else
                If bNettModeEnabled Then
                    ' only nett
                    lblGrossNettEnabledSetting.Text = "Nett"
                Else
                    ' nothing! - should be Nett by default
                    lblGrossNettEnabledSetting.Text = "Nett"
                End If
            End If

            For iCount As Integer = 0 To rdlGrossNettDefault.Items.Count - 1
                Dim radOption As ListItem
                radOption = rdlGrossNettDefault.Items(iCount)
                radOption.Enabled = False
            Next
            rdlGrossNettDefault.Visible = False
            lblGrossNettDefaultSetting.Visible = CurrentlyUnderEditUserCanSeeGrossNett
            lblGrossNettDefaultSetting.Text = IIf(bGrossOptionIsDefault, "Gross", "Nett").ToString()

            cblGrossNettSettings.Visible = CurrentlyUnderEditUserCanSeeGrossNett ''
            lblGrossNettSettings.Visible = CurrentlyUnderEditUserCanSeeGrossNett
            lblGrossNettDefault.Visible = CurrentlyUnderEditUserCanSeeGrossNett
            rdlGrossNettDefault.Visible = CurrentlyUnderEditUserCanSeeGrossNett ''

            lblGrossNettEnabledSetting.Visible = False ''
            lblGrossNettDefaultSetting.Visible = False ''

        Else
            ' this is for a super user login who can edit this stuff
            lblGrossNettEnabledSetting.Visible = False
            lblGrossNettDefaultSetting.Visible = False

            trGrossNettSettings.Visible = UserCanSeeGrossNett  'CurrentlyUnderEditUserCanSeeGrossNett 'True
            trGrossNettDefaults.Visible = UserCanSeeGrossNett 'CurrentlyUnderEditUserCanSeeGrossNett 'True

            For Each chkOption As ListItem In cblGrossNettSettings.Items
                chkOption.Attributes.Add("onclick", "ValidateGrossNettStuff();")
            Next

            cblGrossNettSettings.Visible = True
            lblGrossNettSettings.Visible = True
            lblGrossNettDefault.Visible = True
            rdlGrossNettDefault.Visible = True

            If Not bGrossModeEnabled OrElse Not bNettModeEnabled Then
                ' hide radio button row
                divGrossNettDefaultRadioButtonsHolder.Style("display") = "none"
                lblGrossNettDefault.Style("display") = "none"
            Else
                divGrossNettDefaultRadioButtonsHolder.Style("display") = "block"
                lblGrossNettDefault.Style("display") = "block"
            End If
        End If

        cblGrossNettSettings.Items(GROSS_RADIO_BUTTON_INDEX).Selected = bGrossModeEnabled
        cblGrossNettSettings.Items(NETT_RADIO_BUTTON_INDEX).Selected = bNettModeEnabled

        ' Super User Editable Gross Nett Changes

    End Sub

    Private Sub ClearFormFields()
        Me.UserId = Nothing
        Me.EditingState = Nothing
        txtFirstName.Text = ""
        txtSurname.Text = ""
        txtEmail.Text = ""
        txtPassword.Text = ""
        txtConfirmPassword.Text = ""
        btnUpdate.Enabled = True
        btnCancel.Enabled = True
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        RaiseEvent CancelClicked()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        PerformUpdateInsertDelete()
    End Sub

    Private Sub PerformUpdateInsertDelete()
        ' Validate form fields
        If Validate() = True Then
            Dim RQ As New THL_UpdateUserDetailsRQ
            Dim RS As New THL_UpdateUserDetailsRS

            RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
            RQ.UserInfo = New UserInfoType

            If Me.IsSuperUser = True AndAlso Me.EditingState = State.DeleteUser Then

                ' Don't do delete as this is a superuser
                lblWarning.Text &= "Cannot delete a superuser"
            Else

                Select Case Me.EditingState
                    Case State.NewUser
                        RQ.Mode = "I" ' Creating a new user
                    Case State.ModifyUser
                        RQ.Mode = "U" ' Modifying a user
                    Case State.DeleteUser
                        RQ.Mode = "D" ' Deleting a user
                End Select

                RQ.UserInfo.UserId = Me.UserId
                RQ.UserInfo.FirstName = txtFirstName.Text
                RQ.UserInfo.LastName = txtSurname.Text

                'Karel Jordaan 9Oct2006
                '----------------------------------------------
                'RQ.UserInfo.TotalDisplay = Me.ddTotalDisplay.SelectedValue.ToString()

                '----------------------------------------------

                If Me.EditingState = State.ModifyUser AndAlso txtPassword.Text = "" Then
                    ' User hasn't changed password, use existing
                    RQ.UserInfo.PassWord = Me.Password
                Else
                    RQ.UserInfo.PassWord = txtPassword.Text
                End If

                RQ.UserInfo.EmailAddress = txtEmail.Text ' Email

                If CStr(Session("TypeOfUser")) = "customer" Then
                    ' Retrieve default agent and agent list 
                    RQ.UserInfo.DefAgentCode = Me.DefaultAgentCode
                    RQ.UserInfo.Agents = Me.AgentList
                Else

                    ' Agent list are not displayed if the user is a customer
                    RQ.UserInfo.DefAgentCode = radDefaultAgent.SelectedItem.Value ' Default agent

                    Dim agentArray(chkAgents.Items.Count - 1) As String
                    Dim index As Integer
                    ' Loop through all agents in checkbox list, if selected add to agent array
                    For index = 0 To chkAgents.Items.Count - 1
                        If chkAgents.Items.Item(index).Selected = True Then
                            agentArray(index) = chkAgents.Items.Item(index).Value
                        End If
                    Next

                    RQ.UserInfo.Agents = agentArray
                End If

                ' Customer Confirmation Changes
                ' collect data to update confirmation and nett gross stuff
                Dim sNettGrossAndCustomerConfirmationDetails As String = ""

                ' 0 - Customer Confirmation Allowed? Yes/No
                ' 1 - Gross+Nett Allowed? Yes/No
                ' 2 - Gross/Nett/Both?  G,N
                ' 3 - default option out of Gross/Nett?  G-HTC Touch Pro 2

                Dim sNettGrossDefault As String = ""
                Dim sNettGrossSettings As String = ""

                If cblGrossNettSettings.Items(GROSS_RADIO_BUTTON_INDEX).Selected Then
                    If cblGrossNettSettings.Items(NETT_RADIO_BUTTON_INDEX).Selected Then
                        ' both - read default from the radio buttons
                        sNettGrossDefault = IIf(rdlGrossNettDefault.Items(GROSS_RADIO_BUTTON_INDEX).Selected, "G~", "N~").ToString()
                        sNettGrossSettings = "G,N~"
                    Else
                        ' only gross
                        sNettGrossDefault = "G~"
                        sNettGrossSettings = "G~"
                    End If
                Else
                    If cblGrossNettSettings.Items(NETT_RADIO_BUTTON_INDEX).Selected Then
                        ' only nett
                        sNettGrossDefault = "N~"
                        sNettGrossSettings = "N~"
                    Else
                        ' none
                        sNettGrossDefault = "~"
                        sNettGrossSettings = "~"
                    End If
                End If

                Dim sGrossNettSetting, sCustomerConfirmationSetting As String
                If LoggedInUserIsSuperUser Then
                    'UserCanSeeGrossNett
                    sGrossNettSetting = IIf(UserCanSeeGrossNett, "Yes~", "No~").ToString()
                    'sCustomerConfirmationSetting = IIf(UserCanSeeCustomerConfirmations, "Yes~", "No~").ToString()
                    ' for now, cust confirmation is same as gross nett
                    sCustomerConfirmationSetting = sGrossNettSetting
                Else
                    sGrossNettSetting = IIf(UserCanSeeGrossNett, "Yes~", "No~").ToString()
                    sCustomerConfirmationSetting = sGrossNettSetting
                End If

                'sNettGrossAndCustomerConfirmationDetails = IIf(CurrentlyUnderEditUserCanSeeCustomerConfirmation, "Yes~", "No~").ToString() & _
                '                                           IIf(CurrentlyUnderEditUserCanSeeGrossNett, "Yes~", "No~").ToString() & _
                '                                           sNettGrossSettings & _
                '                                           sNettGrossDefault

                sNettGrossAndCustomerConfirmationDetails = sCustomerConfirmationSetting & _
                                                           sGrossNettSetting & _
                                                           sNettGrossSettings & _
                                                           sNettGrossDefault

                RQ.UserInfo.AgentDutyCode = sNettGrossAndCustomerConfirmationDetails
                RQ.POS(0).AgentDutyCode = sNettGrossAndCustomerConfirmationDetails

                ' collect data to update confirmation and nett gross stuff
                ' Customer Confirmation Changes

                RS = Proxy.UpdateUserDetails(RQ)

                Dim sb As New System.Text.StringBuilder
                Dim i As Integer = 0

                If Not IsNothing(RS.Errors) AndAlso RS.Errors.Length > 0 Then
                    'there were errors
                    For i = 0 To RS.Errors.Length - 1
                        sb.Append(RS.Errors(0).Tag & "<br>")
                    Next

                    lblWarning.Text = sb.ToString

                Else ' All ok, fire the correct action based on editing state

                    ' update session xml
                    Try
                        Dim xmlTemp As New System.Xml.XmlDocument
                        xmlTemp.LoadXml(Me.UserData)
                        Dim oNsMgr As XmlNamespaceManager = New XmlNamespaceManager(xmlTemp.NameTable)
                        oNsMgr.AddNamespace("ns", "http://www.opentravel.org/OTA/2003/05")
                        xmlTemp.DocumentElement.SelectSingleNode("//ns:Users", oNsMgr).ChildNodes(UserRSIndex).SelectSingleNode("//ns:AgentDutyCode", oNsMgr).InnerText = sNettGrossAndCustomerConfirmationDetails
                        'xmlTemp.DocumentElement.LastChild.ChildNodes(UserRSIndex).LastChild.InnerText = sNettGrossAndCustomerConfirmationDetails
                        Me.UserData = xmlTemp.OuterXml
                    Catch ex As Exception
                        ' do nothing
                    End Try
                    ' update session xml

                    Me.UserId = RS.UserInfo.UserId.ToString
                    Select Case Me.EditingState
                        Case State.NewUser
                            RaiseEvent SaveClicked(Me.UserId)
                        Case State.ModifyUser
                            RaiseEvent SaveClicked(Me.UserId)
                        Case State.DeleteUser
                            RaiseEvent DeletePerformed()
                    End Select

                End If
                End If
        End If

    End Sub


    Private Sub LoadAgentLists()
        'Load agent checkbox list and default agent radiobutton lists

        Dim i As Integer = 0
        Dim x As Integer = 0
        chkAgents.Items.Clear()
        radDefaultAgent.Items.Clear()

        'Retrieve RS object from session
        Dim RS As THL_RetUsersRS = CType(General.DeserializeRetUsersRSObject(Me.UserData, RS), THL_RetUsersRS)

        For i = 0 To RS.Agents.Length - 1

            ' Create checkbox list item
            Dim newCheckBoxItem As New ListItem
            ' Agent code and name are in sth same node, need to split 

            Dim delimStr As String = "|"
            Dim delimiter As Char() = delimStr.ToCharArray()
            Dim textAndValue() As String = RS.Agents(i).Split(delimiter)
            newCheckBoxItem.Text = textAndValue(1).ToUpper ' agent name
            newCheckBoxItem.Value = textAndValue(0).ToString 'agent code

            chkAgents.Items.Add(newCheckBoxItem)
        Next

        ' Create radiobutton  list item
        For i = 0 To RS.Agents.Length - 1
            Dim newRadButtonItem As New ListItem
            Dim delimStr As String = "|"
            Dim delimiter As Char() = delimStr.ToCharArray()
            Dim textAndValue() As String = RS.Agents(i).Split(delimiter)
            newRadButtonItem.Text = "."    ' Is invisible, white forecolor
            newRadButtonItem.Value = textAndValue(0).ToString
            radDefaultAgent.Items.Add(newRadButtonItem)
        Next
    End Sub
    Private Sub SetAgentLists()
        ' Choose correct settings based on user data
        Dim RS As THL_RetUsersRS = CType(General.DeserializeRetUsersRSObject(Me.UserData, RS), THL_RetUsersRS)

        ' Available agents list
        chkAgents.ClearSelection()
        Dim i As Integer = 0

        ' If the agent checkbox list contains the agent that this user belongs to select it. 
        If Not IsNothing(RS.Users(Me.UserRSIndex).Agents) Then
            For i = 0 To RS.Users(Me.UserRSIndex).Agents.Length - 1
                Dim delimStr As String = "|"
                Dim delimiter As Char() = delimStr.ToCharArray()
                Dim textAndValue() As String = RS.Users(Me.UserRSIndex).Agents(i).Split(delimiter)
                chkAgents.Items.FindByText(textAndValue(1).ToUpper).Selected = True
            Next
        End If

        ' Select the default agent in the radio button list
        radDefaultAgent.ClearSelection()
        Dim listItem As New ListItem(".", RS.Users(Me.UserRSIndex).DefAgentCode)

        If radDefaultAgent.Items.Contains(listItem) Then
            radDefaultAgent.Items.FindByValue(RS.Users(Me.UserRSIndex).DefAgentCode).Selected = True
        End If



    End Sub

    Private Function Validate() As Boolean
        ' Validate form
        Dim success As Boolean = True
        If Not Me.EditingState = State.DeleteUser Then

            Dim sb As New System.Text.StringBuilder

            If txtFirstName.Text.Trim = "" Then
                sb.Append("Please enter a Firstname" & "<br>")
                success = False
            End If

            If txtSurname.Text.Trim = "" Then
                sb.Append("Please enter a Surname" & "<br>")
                success = False
            End If

            If txtEmail.Text.Trim = "" Then
                sb.Append("Please enter an email address" & "<br>")
                success = False
            End If

            If Me.EditingState = State.ModifyUser AndAlso Not txtPassword.Text.Trim = "" Or Me.EditingState = State.NewUser Then
                ' Need to do validation on password as user has  changed it or this is a new user
                If txtPassword.Text.Trim = "" Then
                    sb.Append("Please enter a password" & "<br>")
                    success = False
                ElseIf Not Me.Password.Equals(txtPassword.Text.Trim) Then
                    ' Password has changed perform validation
                    If txtPassword.Text.Trim.Equals(txtConfirmPassword.Text.Trim) = False Then
                        sb.Append("Passwords do not match" & "<br>")
                        success = False
                    End If
                End If
            End If

            If CStr(Session("TypeOfUser")) = "agent" Then
                ' Agent list is only visible to agents
                If IsNothing(chkAgents.SelectedItem) Then
                    sb.Append("Please select at least one Agent" & "<br>")
                    success = False
                End If

                If IsNothing(radDefaultAgent.SelectedItem) Then
                    sb.Append("Please specify a Default Agent" & "<br>")
                    success = False
                End If

                If Not IsNothing(chkAgents.SelectedItem) AndAlso Not IsNothing(radDefaultAgent.SelectedItem) Then
                    ' Check default agent corresponds to a selected agent in agent list

                    If chkAgents.Items.FindByValue(radDefaultAgent.SelectedItem.Value).Selected = False Then
                        sb.Append("Default Agent must match selected Agent" & "<br>")
                        success = False
                    End If
                End If
            End If
            lblWarning.Text = sb.ToString
            Return success
        End If
        Return success
    End Function


End Class
