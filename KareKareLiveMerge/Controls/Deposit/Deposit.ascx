<%@ Control Language="vb" AutoEventWireup="false" Codebehind="Deposit.ascx.vb" Inherits="THLAuroraWebInterface.Deposit" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE class="normal" id="Table3" cellSpacing="0" cellPadding="2" width="746" border="0">
	<TR class="RowHeading">
		<TD class="rowheading" style="WIDTH: 174px">Deposit Requirements</TD>
	</TR>
	<TR>
		<TD>
			<TABLE id="Table5" cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="stdconfirmationheading" width="174">Deposit</TD>
					<TD class="normal" align="left"><asp:label id="lblDeposit" runat="server"></asp:label></TD>
					<TD class="normal" align="left"><asp:label id="lblDepositCurrency" runat="server"></asp:label></TD>
				</TR>
				<TR id="FullAmountRow" runat="server">
					<TD class="stdconfirmationheading" width="174">Full Amount</TD>
					<TD class="normal" align="left"><asp:label id="lblFullAmount" runat="server"></asp:label></TD>
					<TD class="normal"><asp:label id="lblFullamountCurrency" runat="server"></asp:label></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR class="RowHeading">
		<TD class="rowheading" style="WIDTH: 174px; HEIGHT: 23px" vAlign="top">Credit Card 
			Details</TD>
	</TR>
	<TR>
		<TD>
			<TABLE class="normal" id="Table4" cellSpacing="1" cellPadding="1" width="616" border="0">
				<TR>
					<TD class="stdconfirmationheading" style="HEIGHT: 26px" width="174">Card Number</TD>
					<TD style="HEIGHT: 26px"><asp:textbox id="txtCardNo" runat="server" Width="243px" MaxLength="16"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="stdconfirmationheading" vAlign="top" width="174">Card Expiry</TD>
					<TD vAlign="top"><asp:dropdownlist id="ddlExpiryMonth" runat="server">
							<asp:ListItem Value="01">01</asp:ListItem>
							<asp:ListItem Value="02">02</asp:ListItem>
							<asp:ListItem Value="03">03</asp:ListItem>
							<asp:ListItem Value="04">04</asp:ListItem>
							<asp:ListItem Value="05">05</asp:ListItem>
							<asp:ListItem Value="06">06</asp:ListItem>
							<asp:ListItem Value="07">07</asp:ListItem>
							<asp:ListItem Value="08">08</asp:ListItem>
							<asp:ListItem Value="09">09</asp:ListItem>
							<asp:ListItem Value="10">10</asp:ListItem>
							<asp:ListItem Value="11">11</asp:ListItem>
							<asp:ListItem Value="12">12</asp:ListItem>
						</asp:dropdownlist><asp:dropdownlist id="ddlExpiryYear" runat="server"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="stdconfirmationheading" vAlign="top" width="174">Name Holder</TD>
					<TD vAlign="top"><asp:textbox id="txtNameHolder" runat="server" Width="243px" MaxLength="64"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="stdconfirmationheading" vAlign="top" width="174">Security No (last 3 
						digits on back of card)</TD>
					<TD vAlign="top"><asp:textbox id="txtSeriesCode" runat="server" Width="46px" MaxLength="3"></asp:textbox></TD>
				</TR>
			</TABLE>
			<asp:Label id="lblError" runat="server" CssClass="warning"></asp:Label>
		</TD>
	</TR>
	<TR>
		<TD><asp:checkbox id="chkTermsAndConditions" runat="server" CssClass="normal" Text="I have read and understand the "></asp:checkbox><A href="http://www.nzherald.co.nz">Terms 
				and Conditions</A></TD>
	</TR>
</TABLE>
