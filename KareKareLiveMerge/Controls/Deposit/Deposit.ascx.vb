'--------------------------------------------------------------------------------------
'   Class:          Deposit
'   Description:    Displays deposit details and captures credit card details
'--------------------------------------------------------------------------------------

Partial Class Deposit
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public WriteOnly Property Deposit() As String
        Set(ByVal Value As String)
            lblDeposit.Text = Value
        End Set
    End Property

    Public WriteOnly Property FullAmount() As String
        Set(ByVal Value As String)
            If Value = "" Then
                ' Hide full amount row
                FullAmountRow.Visible = False
            Else
                FullAmountRow.Visible = True
                lblFullAmount.Text = Value
            End If

        End Set
    End Property

    Public WriteOnly Property DepositCurrency() As String
        Set(ByVal Value As String)
            lblDepositCurrency.Text = Value
        End Set
    End Property

    Public WriteOnly Property FullAmountCurrency() As String
        Set(ByVal Value As String)
            lblFullamountCurrency.Text = Value
        End Set
    End Property

    Public ReadOnly Property CardNumber() As String
        Get
            Return txtCardNo.Text
        End Get
    End Property

    Public ReadOnly Property Expiry() As String
        Get
            Return ddlExpiryMonth.SelectedItem.Value & ddlExpiryYear.SelectedItem.Value.Substring(2, 2)
        End Get
    End Property

    Public ReadOnly Property SeriesCode() As String
        Get
            Return txtSeriesCode.Text
        End Get
    End Property

    Public ReadOnly Property NameHolder() As String
        Get
            Return txtNameHolder.Text
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblError.Text = ""

    End Sub

    Private Sub LoadYearList()
        Dim arrYear As New ArrayList
        Dim yearsToAdd As Integer
        ' Load year list for next seven years
        For yearsToAdd = 0 To 7
            arrYear.Add(DateTime.Now.AddYears(yearsToAdd).Year)
        Next
        ddlExpiryYear.DataSource = arrYear
        ddlExpiryYear.DataBind()

    End Sub

    Public Sub InitDeposit()
        ' Clear all form fields
        lblDeposit.Text = ""
        lblDepositCurrency.Text = ""
        lblFullAmount.Text = ""
        lblFullamountCurrency.Text = ""
        txtCardNo.Text = ""
        txtNameHolder.Text = ""
        txtSeriesCode.Text = ""

        ' Load credit card expiry year dropdownlist
        LoadYearList()
    End Sub

    Public Function ValidateDepositForm() As Boolean
        Dim success As Boolean = True
        Dim sb As New System.Text.StringBuilder

        If txtCardNo.Text.Trim = "" Then
            sb.Append("Please enter a Card Number" & "<br>")
            success = False
        End If

        If txtNameHolder.Text.Trim = "" Then
            sb.Append("Please enter a Name" & "<br>")
            success = False
        End If

        If txtSeriesCode.Text.Trim = "" Then
            sb.Append("Please enter the security number (Last 3 digits on back of card) " & "<br>")
            success = False
        End If

        If chkTermsAndConditions.Checked = False Then
            sb.Append("Please read the Terms and Conditions" & "<br>")
            success = False
        End If

        If success = False Then
            ' Focus on error message
            UIScripts.SetFocus(Me.Page, lblError.ClientID)
            lblError.Text = sb.ToString
        End If
        Return success
    End Function

End Class
