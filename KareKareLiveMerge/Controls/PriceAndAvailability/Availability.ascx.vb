'--------------------------------------------------------------------------------------
'   Class:          Availability
'   Description:    Displays all available packages based on user selected options
'                   from LocationAndTime.ascx. A user can then Provisonally book, save quote or book
'                   selected package
'--------------------------------------------------------------------------------------

Partial Class Availability
    Inherits System.Web.UI.UserControl
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtPromoCode As System.Web.UI.WebControls.TextBox


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'Customer Confirmation Changes
    Const GROSS_RADIO_BUTTON_INDEX As Int16 = 0
    Const NETT_RADIO_BUTTON_INDEX As Int16 = 1
    'Customer Confirmation Changes

    Public Event BookClicked(ByVal ID As String)
    Public Event SaveQuoteClicked(ByVal ID As String)
    Public Event BookProvisionalClicked(ByVal ID As String)

    Dim VehAvailRateRS As OTA_VehAvailRateRS
    Dim dsView As DataView

    Private Property AvailableVehicleData() As String
        Get
            Return CStr(Session("AvailableVehicleData"))
        End Get
        Set(ByVal Value As String)
            Session("AvailableVehicleData") = Value
        End Set
    End Property

    Private Property BookingDataset() As DataSet
        Get
            Return CType(ViewState("BookingDataset"), DataSet)
        End Get
        Set(ByVal Value As DataSet)
            ViewState("BookingDataset") = Value
        End Set
    End Property

    'Customer Confirmation Changes

    Public ReadOnly Property GrossNettDefault() As String
        Get
            If Session("GrossNettDefault") IsNot Nothing Then
                Return CStr(Session("GrossNettDefault"))
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property GrossModeEnabled() As Boolean
        Get
            If Session("GrossModeEnabled") IsNot Nothing Then
                Return CBool(Session("GrossModeEnabled"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("GrossModeEnabled") = value
        End Set
    End Property

    Public Property NettModeEnabled() As Boolean
        Get
            If Session("NettModeEnabled") IsNot Nothing Then
                Return CBool(Session("NettModeEnabled"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("NettModeEnabled") = value
        End Set
    End Property

    Public Property GrossModeSelected() As Boolean
        Get
            If Session("GrossModeSelected") IsNot Nothing Then
                Return CBool(Session("GrossModeSelected"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("GrossModeSelected") = value
        End Set
    End Property

    Public Property NettModeSelected() As Boolean
        Get
            If Session("NettModeSelected") IsNot Nothing Then
                Return CBool(Session("NettModeSelected"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("NettModeSelected") = value
        End Set
    End Property

    Public Property AvailabilityRequest() As OTA_VehAvailRateRQ
        Get
            If Session("Avail_AvailabilityRequest") IsNot Nothing Then
                Return CType(Session("Avail_AvailabilityRequest"), OTA_VehAvailRateRQ)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As OTA_VehAvailRateRQ)
            Session("Avail_AvailabilityRequest") = value
        End Set
    End Property

    Public Property SelectedRateIsNettEvenThoughGrossOptionWasSelected() As Boolean
        Get
            If Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected") IsNot Nothing Then
                Return CBool(Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("SelectedRateIsNettEvenThoughGrossOptionWasSelected") = value
        End Set
    End Property
    'Customer Confirmation Changes

    ' Currency changes
#Region "Currency Changes"
    Public ReadOnly Property UserHasCurrencyOptions() As Boolean
        Get
            If Session("UserHasCurrencyOptions") IsNot Nothing Then
                Return CBool(Session("UserHasCurrencyOptions"))
            Else
                Return False
            End If
        End Get
    End Property

    'Public ReadOnly Property UserDefaultCurrency() As String
    '    Get
    '        'If Session("UserDefaultCurrency") IsNot Nothing Then
    '        '    Return CStr(Session("UserDefaultCurrency"))
    '        'Else
    '        '    Return ""
    '        'End If
    '        REM <<REMOVE>>
    '        Return "EUR"
    '    End Get
    'End Property

    Public ReadOnly Property CurrencyOptionsData() As ArrayList
        Get
            If Session("CurrencyOptionsData") IsNot Nothing Then
                Return CType(Session("CurrencyOptionsData"), ArrayList)
            Else
                Return New ArrayList
            End If
        End Get
    End Property

    Public Property ExchangeRatesLookupData() As ExchangeRatesTypeAgent()
        Get
            If Session("ExchangeRatesLookupData") IsNot Nothing Then
                Return CType(Session("ExchangeRatesLookupData"), ExchangeRatesTypeAgent())
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As ExchangeRatesTypeAgent())
            Session("ExchangeRatesLookupData") = value
        End Set
    End Property

    Public Property CurrentCountryOfTravelCurrency() As String
        Get
            Return CStr(ViewState("CurrentCountryOfTravelCurrency")).Trim()
        End Get
        Set(ByVal value As String)
            ViewState("CurrentCountryOfTravelCurrency") = value
        End Set
    End Property

    Public Property MRU_CurrencyCode() As String
        Get
            Return CStr(Session("MRU_CurrencyCode"))
        End Get
        Set(ByVal value As String)
            Session("MRU_CurrencyCode") = value
        End Set
    End Property

    'Public Property MRU_CurrencyDesc() As String
    '    Get
    '        Return CStr(Session("MRU_CurrencyDesc"))
    '    End Get
    '    Set(ByVal value As String)
    '        Session("MRU_CurrencyDesc") = value
    '    End Set
    'End Property

    Public Property MRU_CurrencyMultiplier() As Double
        Get
            If Session("MRU_CurrencyMultiplier") IsNot Nothing Then
                Return CDbl(Session("MRU_CurrencyMultiplier"))
            Else
                Return 0.0
            End If
        End Get
        Set(ByVal value As Double)
            Session("MRU_CurrencyMultiplier") = value
        End Set
    End Property

#End Region

    Public Property VehicleAvailData() As VehicleAvailRSCoreType
        Get
            If Session("VehicleAvailData_Currency") IsNot Nothing Then
                Return CType(Session("VehicleAvailData_Currency"), VehicleAvailRSCoreType)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As VehicleAvailRSCoreType)
            Session("VehicleAvailData_Currency") = value
        End Set
    End Property

    ' Currency changes

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblErrorVehAvailRate.Text = ""

        ' to prevent the dropdowns getting messed up
        For Each liListItem As ListItem In rdgGrossNett.Items
            liListItem.Attributes.Add("onclick", "SaveDropDownSettings()")
        Next

        For Each liListItem As ListItem In cmbCurrency.Items
            liListItem.Attributes.Add("onclick", "SaveDropDownSettings()")
        Next
    End Sub

    Private Sub DeserializeAvailableVehicleRS()
        ' Create the object out of the response string stored in session
        VehAvailRateRS = General.DeserializeVehAvailRateRSObject(Me.AvailableVehicleData, VehAvailRateRS)
    End Sub

    ' currency change
    Private Sub LoadAvailableVehicleDataGrid(ByVal UseDefaultCurrencyIfAvailable As Boolean, ByVal DefaultCurrencyCode As String)
        ' currency change

        ' Create dataset to hold available vehicle data
        Dim ds As DataSet
        ds = New DataSet
        Dim dt As DataTable = New DataTable("Vehicle")
        Dim col As DataColumn = New DataColumn

        col = New DataColumn
        col.ColumnName = "VendorCode"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "VehicleId"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Vehicle"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Product"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)


        col = New DataColumn
        col.ColumnName = "Availability"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "Package"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)


        col = New DataColumn
        col.ColumnName = "VehicleRate"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        ' New Columns

        col = New DataColumn
        col.ColumnName = "THLAgentCharge"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "THLCustomerCharge"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        ' New Columns

        col = New DataColumn
        col.ColumnName = "RentalCharge"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "OtherCharge"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        col = New DataColumn
        col.ColumnName = "TotalCost"
        col.DataType = Type.GetType("System.String")
        dt.Columns.Add(col)

        'Customer Confirmation Changes
        col = New DataColumn
        col.ColumnName = "GrossRateAvailable"
        col.DataType = Type.GetType("System.Boolean")
        dt.Columns.Add(col)

        Dim IsANettRateOnlyItemPresent As Boolean
        'Customer Confirmation Changes

        Dim vehicleIndex As Integer
        Dim vendorIndex As Integer

        ' currency changes
        If VehAvailRateRS IsNot Nothing AndAlso VehAvailRateRS.VehAvailRSCore IsNot Nothing Then
            ' this must be coming from a webservice call
            VehicleAvailData = VehAvailRateRS.VehAvailRSCore
        End If

        If VehicleAvailData IsNot Nothing Then
            For vendorIndex = 0 To VehicleAvailData.VehVendorAvails.Length - 1
                ' List all vehicles under a specific vendor
                If Not IsNothing(VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails) Then
                    ' There are available vehicles
                    For vehicleIndex = 0 To VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails.Length - 1
                        ' Create a new data row in dataset for each vehicle for a particular vendor
                        'Customer Confirmation Changes
                        newRow(dt, vendorIndex, vehicleIndex, UseDefaultCurrencyIfAvailable, DefaultCurrencyCode, IsANettRateOnlyItemPresent)
                        'Customer Confirmation Changes
                    Next
                End If
            Next

            ds.Tables.Add(dt)
            Me.BookingDataset = ds
            BindBookingDataGrid()

            'Customer Confirmation Changes
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''lblNettPriceIndicator.Visible = IsANettRateOnlyItemPresent
            'Customer Confirmation Changes

        End If

        ' currency changes


        'If Not IsNothing(VehAvailRateRS.VehAvailRSCore) Then
        '    For vendorIndex = 0 To VehAvailRateRS.VehAvailRSCore.VehVendorAvails.Length - 1
        '        ' List all vehicles under a specific vendor
        '        If Not IsNothing(VehAvailRateRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails) Then
        '            ' There are available vehicles
        '            For vehicleIndex = 0 To VehAvailRateRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails.Length - 1
        '                ' Create a new data row in dataset for each vehicle for a particular vendor

        '                'Customer Confirmation Changes
        '                newRow(dt, vendorIndex, vehicleIndex, IsANettRateOnlyItemPresent)
        '                'Customer Confirmation Changes
        '            Next
        '        End If
        '    Next

        '    ds.Tables.Add(dt)
        '    Me.BookingDataset = ds
        '    BindBookingDataGrid()

        '    'Customer Confirmation Changes
        '    lblNettPriceIndicator.Visible = IsANettRateOnlyItemPresent
        '    'Customer Confirmation Changes

        'End If
    End Sub

    Public Sub newRow(ByRef dt As DataTable, ByVal vendorIndex As Integer, ByVal vehicleIndex As Integer, _
                      ByVal UseDefaultCurrencyIfAvailable As Boolean, ByVal DefaultCurrencyCode As String, _
                      ByRef IsANettRateOnlyItemPresent As Boolean)
        '
        Dim currency As String = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(0).CurrencyCode.ToString
        Dim GrossModeAvailable As Boolean = (VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).RateQualifier.RateCategory = "1")
        Dim dr As DataRow = dt.NewRow
        Dim strRate As String
        dr("VendorCode") = VehicleAvailData.VehVendorAvails(vendorIndex).Vendor.Code.ToString
        dr("VehicleId") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TPA_Extensions.AvailableVehicleId.ToString
        dr("vehicle") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.Vehicle.VehMakeModel.Name.ToString
        dr("Product") = "ToDo"  'dAvailableVehicleRS.VehAvailRSCore.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).RateQualifier.Pro

        'Customer Confirmation Changes
        dr("GrossRateAvailable") = GrossModeAvailable
        If Not IsANettRateOnlyItemPresent Then
            IsANettRateOnlyItemPresent = Not GrossModeAvailable
        End If
        'Customer Confirmation Changes

        'Karel Jordaan  - Issue #007 - Change to package and flexrate
        'Change start
        Try

            strRate = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).RateQualifier.RateQualifier.ToString()

        Catch ex As Exception

        End Try

        If strRate Is Nothing Then
            dr("Package") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).RateQualifier.PromoDesc.ToString
        Else
            dr("Package") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).RateQualifier.PromoDesc.ToString & " (" & _
            VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).RateQualifier.RateQualifier.ToString() & ")"

        End If
        'Change End
        'dr("Package") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).RateQualifier.PromoDesc.ToString

        dr("Availability") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.Status.ToString
        Dim unitName As String = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(0).Calculation(0).UnitName.ToString
        Dim unitCharge As String = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(0).Calculation(0).UnitCharge.ToString
        'dr("VehicleRate") = unitCharge & " " & currency & " / " & unitName     '-- RKS: Commented 05/02/2004
        dr("VehicleRate") = unitCharge & " / " & unitName                       '-- RKS : Added(5 / 2 / 2004)

        ' Need to total all rentalcharges, presume only one rental rate will be returned
        Dim chargeIndex As Integer
        Dim totalcharge As Decimal
        ' Loop through all charges and total
        For chargeIndex = 0 To VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges.Length - 1
            totalcharge = totalcharge + VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.RentalRate(0).VehicleCharges(0).Amount
        Next

        'dr("RentalCharge") = totalcharge.ToString & " " & currency             '-- RKS: Commented 05/02/2004
        dr("RentalCharge") = totalcharge.ToString                               '-- RKS: Added 05/02/2004

        ' New Columns
        dr("THLAgentCharge") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TotalCharge.THLAgentCharge.ToString("f2")
        dr("THLCustomerCharge") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TotalCharge.THLCustomerCharge.ToString("f2")
        ' New Columns

        ' Need to total all fees
        Dim feeIndex As Integer
        Dim totalFee As Decimal
        ' Loop through all fees and total
        If Not IsNothing(VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.Fees) Then
            For feeIndex = 0 To VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.Fees.Length - 1
                totalFee = totalFee + VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.Fees(feeIndex).Amount
            Next
            'dr("OtherCharge") = totalFee.ToString & " " & currency               '-- RKS: Commented 05/02/2004 
            dr("OtherCharge") = totalFee.ToString                                 '-- RKS: Added 05/02/2004  
        Else
            ' No fees to display
            dr("OtherCharge") = ".00"
        End If

        'Customer Confirmation Changes
        '
        'dr("TotalCost") = IIf(GrossModeAvailable, "", "*&nbsp;").ToString() & VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TotalCharge.EstimatedTotalAmount.ToString & " " & currency
        dr("TotalCost") = VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TotalCharge.EstimatedTotalAmount.ToString & " " & currency
        '' '' ''dr("TotalCost") = IIf((GrossModeSelected AndAlso Not GrossModeAvailable), "*&nbsp;", "").ToString() & VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TotalCharge.EstimatedTotalAmount.ToString & " " & currency

        '' '' ''lblNettPriceIndicator.Visible = (GrossModeSelected AndAlso Not GrossModeAvailable)

        'Customer Confirmation Changes
        ' <<TODOCC>>
        ' currency change
        Dim dConversionMultiplier As Double = 1.0

        ' also save to MRU
        If UserHasCurrencyOptions Then
            MRU_CurrencyCode = cmbCurrency.SelectedItem.Text.Split("-"c)(0)
            MRU_CurrencyMultiplier = CDbl(cmbCurrency.SelectedItem.Value)
        End If

        If UserHasCurrencyOptions _
           AndAlso _
           ( _
               ( _
                    cmbCurrency.SelectedIndex <> 0 _
               ) _
               OrElse _
               ( _
                    UseDefaultCurrencyIfAvailable _
               ) _
           ) _
        Then
            Dim sCurrencyString As String = currency ' default to country of travel
            ' figure out the default currency and conversion rate for this agent

            If UseDefaultCurrencyIfAvailable Then
                ' first load with default

                For Each oItem As ListItem In cmbCurrency.Items
                    If oItem.Text.ToUpper().Split("-"c)(0).Trim() = DefaultCurrencyCode Then
                        dConversionMultiplier = CDbl(oItem.Value)
                        sCurrencyString = DefaultCurrencyCode
                        ' this ensures that default is not used if no rates are defined
                        cmbCurrency.ClearSelection()
                        oItem.Selected = True
                        Exit For
                    End If
                Next

            ElseIf cmbCurrency.SelectedIndex = 0 Then
                ' its the currency of travel
                sCurrencyString = currency
                dConversionMultiplier = 1.0
            Else
                ' its a selected index change event
                dConversionMultiplier = CDbl(cmbCurrency.SelectedValue)
                sCurrencyString = cmbCurrency.SelectedItem.Text.Split("-"c)(0)
            End If

            ' save to session
            MRU_CurrencyCode = sCurrencyString
            MRU_CurrencyMultiplier = dConversionMultiplier

            ' start multiplying
            Double.TryParse(cmbCurrency.SelectedValue, dConversionMultiplier)

            Dim dTemp As Double = 0.0

            If Double.TryParse(unitCharge, dTemp) Then
                dr("VehicleRate") = (dTemp * dConversionMultiplier).ToString("f2") & " " & sCurrencyString & " / " & unitName
            End If
            dTemp = 0.0


            'dr("TotalCost") = IIf(GrossModeAvailable, "", "*&nbsp;").ToString() & _
            dr("TotalCost") = (VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TotalCharge.EstimatedTotalAmount * dConversionMultiplier).ToString("f2") & _
                              " " & sCurrencyString

            If Not dr("OtherCharge").ToString = ".00" Then
                If Double.TryParse(dr("OtherCharge").ToString(), dTemp) Then
                    dr("OtherCharge") = (dTemp * dConversionMultiplier).ToString("f2")
                End If
                dTemp = 0.0
            End If

            If Double.TryParse(dr("RentalCharge").ToString(), dTemp) Then
                dr("RentalCharge") = (dTemp * dConversionMultiplier).ToString("f2")
            End If
            dTemp = 0.0

            ' New Columns

            If Double.TryParse(dr("THLAgentCharge").ToString(), dTemp) Then
                dr("THLAgentCharge") = (dTemp * dConversionMultiplier).ToString("f2") & " " & sCurrencyString
            End If
            dTemp = 0.0

            If Double.TryParse(dr("THLCustomerCharge").ToString(), dTemp) Then
                dr("THLCustomerCharge") = (dTemp).ToString("f2") & " " & VehicleAvailData.VehVendorAvails(vendorIndex).VehAvails(vehicleIndex).VehAvailCore.TotalCharge.CurrencyCode
            End If
            dTemp = 0.0

            ' New Columns


        End If

        ' currency change

        dr("THLAgentCharge") = IIf(GrossModeAvailable, "", "*&nbsp;").ToString() & dr("THLAgentCharge").ToString()

        dt.Rows.Add(dr)
    End Sub


    Private Sub BindBookingDataGrid()
        showDatagridButtons(False)
        dsView = Me.BookingDataset.Tables(0).DefaultView  ' Create a view to use as the datagrid datasource

        dgrVehicle.DataSource = dsView
        dgrVehicle.DataKeyField = "VehicleId"  ' Used to identify the booking record
        dgrVehicle.SelectedIndex = -1  ' Clears selected item style
        dgrVehicle.DataBind()

        dgrVehicle.Columns(8).Visible = Not UserHasCurrencyOptions
    End Sub


    Protected Sub dgrVehicle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgrVehicle.SelectedIndexChanged
        ' Show appropriote buttons
        showDatagridButtons(True)
        trButtons.Style("display") = "block"
        'trButtons2.Style("display") = "block"
        ' And focus on the book button
        UIScripts.SetFocus(Me.Page, btnBook.ClientID)
    End Sub

    Private Sub showDatagridButtons(ByVal show As Boolean)
        btnBook.Visible = show
        btnSaveQuote.Visible = show
        btnBookProvisional.Visible = show
    End Sub

    Public Sub InitAvailabilityData(ByVal category As String, _
                                    ByVal brand As String, _
                                    ByVal pickUpLocation As String, _
                                    ByVal returnLocation As String, _
                                    ByVal pickUpDateTime As DateTime, _
                                    ByVal returnDateTime As DateTime, _
                                    ByVal promoCode As String, _
                                    ByVal agentCode As String, _
                                    ByVal SizeCode As String, _
                                    ByVal VehicleCode As String, _
                                    Optional ByVal CachedAvailabilityRequest As OTA_VehAvailRateRQ = Nothing, _
                                    Optional ByVal UseDefaultCurrency As Boolean = True)

        Me.BookingDataset = Nothing
        lblErrorVehAvailRate.Style("color") = "" ' reset colour
        lblErrorVehAvailRate.Text = "" ' reset error

        Try
            If IsNothing(Session("POS")) Then
                ' User is not authenticated or session has expireed
                ' Redirect to redirect page
                Response.Redirect("AccessDenied.aspx")
            Else
                '  Get all available vehicles based on the content captured in locationandtime.ascx
                LoadVehicleAvailabilityData(category, brand, pickUpLocation, returnLocation, pickUpDateTime, returnDateTime, promoCode, agentCode, SizeCode, VehicleCode, CachedAvailabilityRequest)
            End If
        Catch ex As Exception
            lblErrorVehAvailRate.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try

        If lblErrorVehAvailRate.Text = "" Then    ' Only progress if there is no errors

            ' Warnings
            If Not IsNothing(VehAvailRateRS.Warnings) AndAlso VehAvailRateRS.Warnings.Length > 0 Then
                For Each oWarning As WarningType In VehAvailRateRS.Warnings
                    lblErrorVehAvailRate.Text &= "<br/>" & oWarning.Status & " - " & oWarning.ShortText
                Next
                lblErrorVehAvailRate.Style("color") = "orange"
            End If
            ' Warnings

            ' Currency changes
            Dim sDefaultCurrencyCode As String = ""

            If Trim(agentCode) <> "" Then
                ' only nuke if its a radio change
                cmbCurrency.Items.Clear()

                If UserHasCurrencyOptions AndAlso Not String.IsNullOrEmpty(CurrentCountryOfTravelCurrency) Then
                    Dim sCurrencyOfTravelDesc As String
                    If CurrentCountryOfTravelCurrency = "AUD" Then
                        sCurrencyOfTravelDesc = "AUD-Australian Dollar"
                    Else
                        sCurrencyOfTravelDesc = "NZD-New Zealand Dollar"
                    End If

                    cmbCurrency.Items.Add(New ListItem(sCurrencyOfTravelDesc, "1"))

                    For Each oAgent As ExchangeRatesTypeAgent In ExchangeRatesLookupData
                        If oAgent.Code.ToUpper().Trim().Equals(agentCode.ToUpper().Trim()) Then
                            For Each oCurr As ExchangeRatesTypeAgentCurr In oAgent.Curr
                                If oCurr.FromCode = CurrentCountryOfTravelCurrency Then
                                    ' this is the correct exchange rate set
                                    ' look in the current list of items and ensure its not already in there!
                                    Dim bAlreadyExists As Boolean = False
                                    For Each oListItem As ListItem In cmbCurrency.Items
                                        If oListItem.Text = oCurr.ToCode & "-" & CStr(oCurr.ToDesc) Then
                                            bAlreadyExists = True
                                            Exit For
                                        End If
                                    Next

                                    ' see if user has this currency for this agent
                                    Dim bUserHasThisCurrencyInContextOfThisAgent As Boolean = False
                                    For Each oCurrOption As String() In CurrencyOptionsData
                                        If oCurrOption(0).ToUpper().Trim().Equals(agentCode.ToUpper().Trim()) Then
                                            ' matching agent code
                                            If oCurrOption(1).ToUpper().Contains(oCurr.ToCode) Then
                                                bUserHasThisCurrencyInContextOfThisAgent = True
                                                Exit For
                                            End If
                                        End If
                                    Next

                                    If Not bAlreadyExists AndAlso Not CDbl(oCurr.Rate) = 0.0 AndAlso bUserHasThisCurrencyInContextOfThisAgent Then
                                        cmbCurrency.Items.Add(New ListItem(oCurr.ToCode & "-" & CStr(oCurr.ToDesc), oCurr.Rate))
                                    End If
                                End If
                            Next
                        End If
                    Next
                    cmbCurrency.ClearSelection()
                    tdCurrency.Style("display") = "block"

                End If

                ' pick up the default currency data
                For Each saCurrencySetting As String() In CurrencyOptionsData
                    Dim sAgentCode, sCurrencyList As String
                    sAgentCode = saCurrencySetting(0)

                    If sAgentCode.ToUpper().Trim().Equals(agentCode.ToUpper().Trim()) Then
                        ' matching agent code
                        sCurrencyList = saCurrencySetting(1)
                        sDefaultCurrencyCode = saCurrencySetting(2)

                        For j As Integer = cmbCurrency.Items.Count - 1 To 1 Step -1
                            If Not sCurrencyList.Contains(cmbCurrency.Items(j).Text.Split("-"c)(0).ToUpper().Trim()) Then
                                cmbCurrency.Items.RemoveAt(j)
                                ' take away any currencies not assigned to user, but leave the currency of travel item (item 0) alone
                                Continue For
                            End If
                            If cmbCurrency.Items(j).Text.Split("-"c)(0).ToUpper().Trim() = sDefaultCurrencyCode.Trim().ToUpper() Then
                                cmbCurrency.SelectedIndex = j
                            End If
                        Next

                    End If
                Next
            Else
                ' skip the whole cycle of refreshing options

            End If





            LoadAvailableVehicleDataGrid(UseDefaultCurrency, sDefaultCurrencyCode)
            ' Currency changes

            If IsNothing(Me.BookingDataset) Then
                '  There are no available vehicles
                lblErrorVehAvailRate.Text &= "There are no vehicles available." & "<br>"
                dgrVehicle.Visible = False
                showDatagridButtons(False)
                'Customer Confirmation Changes
                SetGrossNettOptions(False)
                'Customer Confirmation Changes
            Else
                'Customer Confirmation Changes
                SetGrossNettOptions(True)
                'Customer Confirmation Changes

                If Me.BookingDataset.Tables(0).Rows.Count = 0 Then
                    ' Booking dataset exists but there are no vehicles
                    lblErrorVehAvailRate.Text &= "There are no vehicles available." & "<br>"
                    dgrVehicle.Visible = False ' Hide datagrid
                    showDatagridButtons(False)
                Else
                    '  There are vehicles, show datagrid
                    dgrVehicle.Visible = True
                    For Each oItem As ListItem In rdgGrossNett.Items
                        If oItem.Attributes("onchange") = "" Then
                            oItem.Attributes.Add("onchange", "document.getElementById('AvailabilityRow').style.display='none';BeginPageLoad();")
                        End If
                    Next
                End If
            End If
        Else
            'hide datagrid as there was an error
            dgrVehicle.Visible = False
            'Customer Confirmation Changes
            SetGrossNettOptions(False)
            'Customer Confirmation Changes
        End If

    End Sub


    Private Sub LoadVehicleAvailabilityData(ByVal category As String, _
                                            ByVal brand As String, _
                                            ByVal pickUpLocation As String, _
                                            ByVal returnLocation As String, _
                                            ByVal pickUpDateTime As DateTime, _
                                            ByVal returnDateTime As DateTime, _
                                            ByVal promoCode As String, _
                                            ByVal agentCode As String, _
                                            ByVal SizeCode As String, _
                                            ByVal VehicleCode As String, _
                                            Optional ByVal CachedAvailabilityRequest As OTA_VehAvailRateRQ = Nothing)

        Dim RQ As New OTA_VehAvailRateRQ

        If CachedAvailabilityRequest Is Nothing Then
            ' Get all available vehicles based on category, brand etc

            'Dim RQ As New OTA_VehAvailRateRQ
            Dim mRequestorId As New UniqueID_Type


            ' Set request header data
            RQ.Version = CDec(1.0)
            RQ.TimeStamp = Date.Now
            RQ.Target = OTA_VehAvailRateRQTarget.Test
            RQ.EchoToken = CStr(Session("SessionId"))
            RQ.MaxResponses = CStr(3)
            RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}

            mRequestorId.ID = agentCode
            mRequestorId.Type = "22"
            RQ.POS(0).RequestorID = mRequestorId

            '  Set rental locations/dates 

            RQ.VehAvailRQCore = New VehicleAvailRQCoreType
            Dim vrc As New VehicleRentalCoreType

            vrc.PickUpDateTime = pickUpDateTime
            vrc.ReturnDateTime = returnDateTime

            vrc.PickUpLocation = New LocationType(pickUpLocation, pickUpLocation)
            vrc.ReturnLocation = New LocationType(returnLocation, returnLocation)

            RQ.VehAvailRQCore.VehRentalCore = vrc

            ' Set vehicle request details
            RQ.VehAvailRQCore.VehPrefs = New VehiclePrefType() {New VehiclePrefType}
            RQ.VehAvailRQCore.Status = InventoryStatusType.Available
            RQ.VehAvailRQCore.VendorPrefs = New CompanyNamePrefType() {New CompanyNamePrefType}
            RQ.VehAvailRQCore.VendorPrefs(0).CompanyShortName = brand
            If (VehicleCode.Trim.Length > 0) Then RQ.VehAvailRQCore.VendorPrefs(0).Code = VehicleCode

            ' Get correct vehicle type from dropdown list
            Dim vehicleCategoryEnum As VehicleCategoryEnum
            Select Case category.ToLower
                Case "v2"
                    vehicleCategoryEnum = vehicleCategoryEnum.Campervan2WD
                Case "v4"
                    vehicleCategoryEnum = vehicleCategoryEnum.Campervan4WD
                Case "m2"
                    vehicleCategoryEnum = vehicleCategoryEnum.Motorhome2WD
                Case "m4'"
                    vehicleCategoryEnum = vehicleCategoryEnum.Motorhome4WD
                Case "c2"
                    vehicleCategoryEnum = vehicleCategoryEnum.Car2WD
                Case "c4"
                    vehicleCategoryEnum = vehicleCategoryEnum.Car4WD
            End Select

            RQ.VehAvailRQCore.VehPrefs(0).VehType = New VehicleTypeType(vehicleCategoryEnum)

            RQ.VehAvailRQCore.VehPrefs(0).TypePref = PreferLevelType.Only
            RQ.VehAvailRQCore.VehPrefs(0).ClassPref = PreferLevelType.Only
            RQ.VehAvailRQCore.VehPrefs(0).VehClass = New VehicleClassType
            RQ.VehAvailRQCore.VehPrefs(0).VehClass.Size = SizeCode



            RQ.VehAvailRQCore.RateQualifier = New RateQualifierType
            If Not promoCode = "" Then
                RQ.VehAvailRQCore.RateQualifier.PromotionCode = promoCode
            Else
                promoCode = "NA"
            End If

            Dim timebefore As String = Now.ToLongTimeString
            Dim timeafter As String = Now.ToLongTimeString

            Label1.Text = "Before " & timebefore & " -  After " & timeafter

            ' change for promocode
            RQ.VehAvailRQCore.RateQualifier.RateQualifier = "Yes"
            ' change for promocode

            ' save to session
            AvailabilityRequest = RQ
        Else
            ' this is caused by a radio button toggle event , use session data
            RQ = AvailabilityRequest
        End If

        ' this will change according to gross/nett
        SetGrossOrNettMode(False)
        If GrossModeSelected Then
            RQ.VehAvailRQCore.Status = InventoryStatusType.Gross
        Else
            RQ.VehAvailRQCore.Status = InventoryStatusType.Available
        End If
        ' read the gross/nett mode and add to input thingie

        General.SerializeVehAvailRateRQObject(RQ)
        VehAvailRateRS = Proxy.VehAvailRate(RQ)

        If IsNothing(VehAvailRateRS.Errors) Then
            ' No errors store response
            ' This is used by the make booking control
            Me.AvailableVehicleData = General.SerializeVehAvailRateRSObject(VehAvailRateRS)
            Try
                CurrentCountryOfTravelCurrency = VehAvailRateRS.VehAvailRSCore.VehVendorAvails(0).VehAvails(0).VehAvailCore.RentalRate(0).VehicleCharges(0).CurrencyCode
            Catch ex As Exception
            End Try
        Else
            If VehAvailRateRS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblErrorVehAvailRate.Text &= VehAvailRateRS.Errors(0).Tag
            trButtons.Style("display") = "none"
            'trButtons2.Style("display") = "none"
        End If

    End Sub

    Private Sub btnBook_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBook.Click
        'Customer Confirmation Changes
        SetGrossOrNettMode(True)
        'Customer Confirmation Changes
        If lblErrorVehAvailRate.Text = "" Then
            ' Fire an event and send the selected quote id with it
            RaiseEvent BookClicked(CStr(dgrVehicle.DataKeys(dgrVehicle.SelectedIndex)))
        End If
    End Sub

    Private Sub btnSaveQuote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveQuote.Click
        'Customer Confirmation Changes
        SetGrossOrNettMode(True)
        'Customer Confirmation Changes
        If lblErrorVehAvailRate.Text = "" Then
            ' Fire an event and send the selected quote id with it
            RaiseEvent SaveQuoteClicked(CStr(dgrVehicle.DataKeys(dgrVehicle.SelectedIndex)))
        End If
    End Sub

    Private Sub btnBookProvisional_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBookProvisional.Click
        'Customer Confirmation Changes
        SetGrossOrNettMode(True)
        'Customer Confirmation Changes
        If lblErrorVehAvailRate.Text = "" Then
            ' Fire an event and send the selected quote id with it
            RaiseEvent BookProvisionalClicked(CStr(dgrVehicle.DataKeys(dgrVehicle.SelectedIndex)))
        End If
    End Sub

    Private Sub dgrVehicle_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgrVehicle.PageIndexChanged
        ' Set CurrentPageIndex to the page the user clicked.
        dgrVehicle.CurrentPageIndex = e.NewPageIndex
        ' Rebind the data to refresh the DataGrid control. 		
        BindBookingDataGrid()
    End Sub

    'Customer Confirmation Changes
    Sub SetGrossNettOptions(ByVal ShowOrHide As Boolean)

        Dim radGross As ListItem = rdgGrossNett.Items(GROSS_RADIO_BUTTON_INDEX)
        Dim radNett As ListItem = rdgGrossNett.Items(NETT_RADIO_BUTTON_INDEX)

        If ShowOrHide Then
            If GrossModeEnabled Then
                'radGross.Enabled = True
                If Not NettModeEnabled Then
                    radGross.Selected = True
                End If
            Else
                'radGross.Enabled = False
            End If

            If NettModeEnabled Then
                'radNett.Enabled = True
                If Not GrossModeEnabled Then
                    radNett.Selected = True
                End If
            Else
                'radNett.Enabled = False
            End If

            If Not GrossModeSelected AndAlso Not NettModeSelected Then
                If GrossNettDefault = "G" Then
                    GrossModeSelected = True
                Else
                    ' Default action is always Nett rate
                    NettModeSelected = True
                End If
            End If

            radGross.Enabled = (GrossModeEnabled AndAlso NettModeEnabled)
            radNett.Enabled = (GrossModeEnabled AndAlso NettModeEnabled)

            radGross.Selected = GrossModeSelected
            radNett.Selected = NettModeSelected

        End If

        'rdgGrossNett.Visible = ShowOrHide
        'tdGrossNett.Visible = (GrossModeEnabled OrElse NettModeEnabled)
        'rdgGrossNett.Visible = (GrossModeEnabled OrElse NettModeEnabled)
        If ShowOrHide AndAlso (GrossModeEnabled OrElse NettModeEnabled) Then
            tdGrossNett.Style("display") = "block"
        Else
            tdGrossNett.Style("display") = "none"
        End If

    End Sub

    Sub SetGrossOrNettMode(Optional ByVal SelectGrossOrNettModeSessionObject As Boolean = False)
        Dim radGross As ListItem = rdgGrossNett.Items(GROSS_RADIO_BUTTON_INDEX)
        Dim radNett As ListItem = rdgGrossNett.Items(NETT_RADIO_BUTTON_INDEX)

        If SelectGrossOrNettModeSessionObject Then
            GrossModeSelected = radGross.Selected
            NettModeSelected = radNett.Selected
        End If

        If Not GrossModeSelected AndAlso Not NettModeSelected Then
            ' a very first load, nothing is set
            GrossModeSelected = (GrossNettDefault = "G")
            NettModeSelected = Not GrossModeSelected
        End If

        ' for the weird condition where you asked for gross (customer mode) but only nett (agent mode) was available
        If dgrVehicle.SelectedIndex > -1 Then
            ' weird mode selected? = is gross rate available for chosen row? x were you looking for gross rates?
            SelectedRateIsNettEvenThoughGrossOptionWasSelected = Not CBool(dgrVehicle.Items(dgrVehicle.SelectedIndex).Cells(10).Text) AndAlso radGross.Selected
        End If

    End Sub
    'Customer Confirmation Changes

    Protected Sub rdgGrossNett_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdgGrossNett.SelectedIndexChanged
        ' This is trigger a call to the webservice again
        GrossModeSelected = rdgGrossNett.Items(GROSS_RADIO_BUTTON_INDEX).Selected
        NettModeSelected = rdgGrossNett.Items(NETT_RADIO_BUTTON_INDEX).Selected

        'Customer Confirmation Changes
        InitAvailabilityData("", "", "", "", Now, Now, "", "", "", "", AvailabilityRequest, False)
        'Customer Confirmation Changes
    End Sub

    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbCurrency.SelectedIndexChanged
        LoadAvailableVehicleDataGrid(False, "")
    End Sub
End Class
