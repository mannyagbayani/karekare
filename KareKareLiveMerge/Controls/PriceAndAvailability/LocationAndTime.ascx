<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LocationAndTime.ascx.vb"
    Inherits="THLAuroraWebInterface.LocationAndTime" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<script language="javascript">

    //Firefox getXML funtion
    function getXML(doc) {

        var serializer = new XMLSerializer();
        var prettyString = serializer.serializeToString(doc);

        return prettyString;
    }


    var iLoopCounter = 1;
    var iMaxLoop = 5;
    var iIntervalId;
    var iPickupChanged = false;
    function getTime(myDate) {

        var a_p = "";
        var d = new Date(myDate);
        if (d.toString() == "NaN") return ""
        var curr_hour = d.getHours();
        a_p = (curr_hour < 12) ? "a.m." : "p.m."
        if (curr_hour == 0)
            curr_hour = 12;
        if (curr_hour > 12)
            curr_hour = curr_hour - 12;

        var curr_min = d.getMinutes();
        curr_min = (curr_min.toString().length == 2) ? curr_min : "0" + curr_min
        return (curr_hour + ":" + curr_min + " " + a_p);
    }

    function LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime) {

        //Populate either pick up or return time list based on the chosen location

        var xmltext = replaceAll(replaceAll(replaceAll(document.forms[0].LocationAndTime1_LocAndTimeXML.value, '&gt;', '>'), '&lt;', '<'), '&quot;', '"')
        try //Internet explorer
        {
            var XMLDom = new ActiveXObject("Microsoft.XMLDom")
            XMLDom.loadXML(xmltext)
        }
        catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
        {
            try {
                var XMLDom = document.implementation.createDocument("", "", null);
                parser = new DOMParser();
                XMLDom = parser.parseFromString(xmltext, "text/xml");

            }
            catch (e) {
                alert(e.message);
            }
        }

        var index = 0



        var timeDifference


        var countryIndex = 0
        var vehicleIndex = 0
        var locationIndex = 0

        //Loop through all countries to get the specified one
        var tPickupTime
        var tReturnTime
        var NewDateRanges
        for (countryIndex = 0; countryIndex <= XMLDom.documentElement.childNodes[1].childNodes[1].childNodes.length - 1; countryIndex++) {

            // Country Code determines the vehicle types that can be displayed
            // Vehicle types determines the locations that are shown
            //var CategoryNode = new ActiveXObject("Microsoft.XMLDom")
            try //Internet explorer
            {
                var CategoryNode = new ActiveXObject("Microsoft.XMLDom")
                CategoryNode.loadXML(XMLDom.documentElement.selectNodes("*//PickupAndDropOffLocations/Country")[countryIndex].xml)
            }
            catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
            {
                try {
                    var CategoryNode = document.implementation.createDocument("", "", null);
                    Nodetext = getXML(XMLDom.documentElement.childNodes[1].childNodes[1].childNodes[countryIndex])
                    parser = new DOMParser();
                    CategoryNode = parser.parseFromString(Nodetext, "text/xml");
                }
                catch (e) {
                    alert(e.message);
                }
            }


            if (CategoryNode.documentElement.attributes[1].value.toLowerCase() == countryCode) {

                //Loop through vehicles and get the one that matches the specified vehicle code for the country code
                for (vehicleIndex = 0; vehicleIndex <= CategoryNode.documentElement.childNodes.length - 1; vehicleIndex++) {
                    // If the vehicle code matches, get the locations for that vehicle
                    if (CategoryNode.documentElement.childNodes[vehicleIndex].attributes[0].value.toLowerCase() == vehicleCode) {
                        //var LocNode = new ActiveXObject("Microsoft.XMLDom")
                        try //Internet explorer
                        {
                            var LocNode = new ActiveXObject("Microsoft.XMLDom")
                            LocNode.loadXML(CategoryNode.documentElement.childNodes[vehicleIndex].xml)
                        }
                        catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
                        {
                            try {
                                var LocNode = document.implementation.createDocument("", "", null);
                                Nodetext = getXML(CategoryNode.documentElement.childNodes[vehicleIndex])
                                parser = new DOMParser();
                                LocNode = parser.parseFromString(Nodetext, "text/xml");

                            }
                            catch (e) {
                                alert(e.message);
                            }
                        }

                        // Loop through all locations to find the specifeied one
                        for (locationIndex = 0; locationIndex <= LocNode.documentElement.childNodes.length - 1; locationIndex++) {
                            // And load the correct time for the location
                            if (LocNode.documentElement.childNodes[locationIndex].attributes[0].value.toLowerCase() == locationCode) {
                                var sPickUpTime = "01/01/2001 " + (LocNode.documentElement.childNodes[locationIndex].attributes[2].value).replace('.', '').replace('.', '')
                                var sReturnTime = "01/01/2001 " + (LocNode.documentElement.childNodes[locationIndex].attributes[3].value).replace('.', '').replace('.', '')

                                tPickupTime = new Date(sPickUpTime)
                                tReturnTime = new Date(sReturnTime)
                                var startTime = tPickupTime.getTime()
                                var EndTime = tReturnTime.getTime()
                            }

                        }

                    }

                }

            }

        }
        var bPickupTimeFlag = false
        var bReturnTimeFlag = false
        var sPickupTime = document.forms[0].LocationAndTime1_ddlPickUpTime.value
        var sReturnTime = document.forms[0].LocationAndTime1_ddlReturnTime.value

        if (bPickUpTime == 1) {
            //We are loading the pickuptime dropdown list
            document.forms[0].LocationAndTime1_ddlPickUpTime.innerHTML = ""
        }
        else {
            //We are loading the return dropdown list
            document.forms[0].LocationAndTime1_ddlReturnTime.innerHTML = ""
        }
        if (tPickupTime > tReturnTime) {
            tReturnTime = new Date(tReturnTime.getTime() + (24 * 60 * 60 * 1000))
        }

        for (tPickupTime; tPickupTime <= tReturnTime; tPickupTime = new Date(tPickupTime.getTime() + (15 * 60 * 1000))) {

            var obj = new Option(tPickupTime, tPickupTime);

            obj.text = getTime(tPickupTime);
            obj.value = getTime(tPickupTime);

            if (bPickUpTime == 1) {
                if ((sPickupTime == obj.value) && (bPickupTimeFlag == false)) {
                    document.forms[0].LocationAndTime1_ddlPickUpTime.options.add(obj)
                    document.forms[0].LocationAndTime1_ddlPickUpTime.value = sPickupTime
                    document.forms[0].hd_PickupTime.value = sPickupTime
                    bPickupTimeFlag = true
                }
                else
                    document.forms[0].LocationAndTime1_ddlPickUpTime.options.add(obj)
            }
            else {
                if (sReturnTime == obj.value && bReturnTimeFlag == false) {
                    document.forms[0].LocationAndTime1_ddlReturnTime.options.add(obj)
                    document.forms[0].LocationAndTime1_ddlReturnTime.value = sReturnTime
                    document.forms[0].hd_DropOffTime.value = sReturnTime
                    bReturnTimeFlag = true
                }
                else
                    document.forms[0].LocationAndTime1_ddlReturnTime.options.add(obj)
            }
        }

        if ('<%=request("hd_PickupTime")%>' != '' && bReturnTimeFlag == false && bPickUpTime == 1) {
            if (document.forms[0].LocationAndTime1_ddlPickUpTime.innerHTML.search('<%=request("hd_PickupTime")%>') != -1)
                document.forms[0].LocationAndTime1_ddlPickUpTime.value = '<%=request("hd_PickupTime")%>'
        }
        if ('<%=request("hd_DropOffTime")%>' != '' && bReturnTimeFlag == false && bPickUpTime == 0) {
            if (document.forms[0].LocationAndTime1_ddlReturnTime.innerHTML.search('<%=request("hd_DropOffTime")%>') != -1)
                document.forms[0].LocationAndTime1_ddlReturnTime.value = '<%=request("hd_DropOffTime")%>'
        }



        if (bPickUpTime == 1 && bPickupTimeFlag == false)
            document.forms[0].hd_PickupTime.value = document.forms[0].LocationAndTime1_ddlPickUpTime.value
        if (bPickUpTime == 0 && bReturnTimeFlag == false)
            document.forms[0].hd_DropOffTime.value = document.forms[0].LocationAndTime1_ddlReturnTime.value

    }

    function replaceAll(myValue, from, to) {
        var rtnVal = ''
        var re = new RegExp(from, 'gi');
        var rtnVal = myValue.replace(re, to)
        return rtnVal
    }

    function ddlBrand_SelectedIndexChanged(sValue) {
        document.forms[0].hd_Brand.value = sValue
        var sCtyCode = document.forms[0].LocationAndTime1_ddlCountry.value
        ddlCountry_SelectedIndexChanged(sCtyCode)
        // Change for Promo Code Drop Down
        ProcessPromoCodeList();
        // Change for Promo Code Drop Down
    }

    function ddlCountry_SelectedIndexChanged(svalue) {

        if (document.forms[0].LocationAndTime1_ddlBrand == null)
            var sBrand = '<%=Session("tmp_brand")%>'
        else
            var sBrand = document.forms[0].LocationAndTime1_ddlBrand.value
        var XMLDom
        var xmltext = replaceAll(replaceAll(replaceAll(document.forms[0].LocationAndTime1_LocAndTimeXML.value, '&gt;', '>'), '&lt;', '<'), '&quot;', '"')
        try //Internet explorer
        {
            XMLDom = new ActiveXObject("Microsoft.XMLDom")
            XMLDom.async = "false";
            XMLDom.loadXML(xmltext)

        }
        catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
        {

            try {
                XMLDom = document.implementation.createDocument("", "", null);
                //alert(XMLDom)
                parser = new DOMParser();
                XMLDom = parser.parseFromString(xmltext, "text/xml");


            }
            catch (e) {
                alert(e.message);
            }
        }

        document.forms[0].LocationAndTime1_ddlCategory.innerHTML = ""


        for (var I = 0; I <= XMLDom.documentElement.childNodes[1].childNodes[1].childNodes.length - 1; I++) {
            var Node
            try //Internet explorer
            {
                Node = new ActiveXObject("Microsoft.XMLDom")
                Node.loadXML(XMLDom.documentElement.selectNodes("*//PickupAndDropOffLocations/Country")[I].xml)
            }
            catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
            {
                try {

                    Nodetext = getXML(XMLDom.documentElement.childNodes[1].childNodes[1].childNodes[I])
                    Node = parser.parseFromString(Nodetext, "text/xml");

                }
                catch (e) {
                    alert(e.message);
                }
            }

            if (Node.documentElement.attributes[1].value == svalue) {

                for (var j = 0; j <= Node.documentElement.childNodes.length - 1; j++) {

                    if (Node.documentElement.childNodes[j].nodeName != "VehicleTypes")
                        continue; //skip this node as its not a category!

                    var a = Node.documentElement.childNodes[j].attributes[0].value
                    var b = Node.documentElement.childNodes[j].attributes[1].value

                    if ((svalue.toLowerCase() == 'nz') && (sBrand.toLowerCase() == 'maui')) {
                        if (a.toLowerCase() != 'v4') {

                            var obj = new Option(b, a);
                            document.getElementById("LocationAndTime1_ddlCategory").options.add(obj);
                        }
                    }
                    else {

                        var obj = new Option(b, a);
                        document.getElementById("LocationAndTime1_ddlCategory").options.add(obj);

                    }
                }
            }
        }

        ddlCategory_SelectedIndexChanged(document.forms[0].LocationAndTime1_ddlCategory.value)

        var countryCode = svalue.toLowerCase()
        var vehicleCode = document.forms[0].LocationAndTime1_ddlCategory.value.toLowerCase()
        var locationCode = document.forms[0].LocationAndTime1_ddlPickUpLocation.value.toLowerCase()
        var bPickUpTime = 1

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkoLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)
        var locationCode = document.forms[0].LocationAndTime1_ddlReturnLocation.value.toLowerCase()
        var bPickUpTime = 0
        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkiLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        // Change for Promo Code Drop Down
        ProcessPromoCodeList();
        // Change for Promo Code Drop Down

    }

    function ddlCategory_SelectedIndexChanged(Value) {
        document.forms[0].hd_Category.value = Value


        //Make the ddlSize enabled or not
        //Based on the ddlCatagory value 'C2' 'V2' 'C4' 'V4'
        if (document.forms[0].hd_Category.value == 'C2' | document.forms[0].hd_Category.value == 'C4') {
            document.forms[0].LocationAndTime1_ddlSize.options[0].selected = true;
            document.forms[0].LocationAndTime1_ddlSize.disabled = true;
        }
        else
            document.forms[0].LocationAndTime1_ddlSize.disabled = false;

        var bPickUpSelected = false
        var bDropOffSelected = false

        var selCty = document.forms[0].LocationAndTime1_ddlCountry.value
        var sPickUpLocCode = document.forms[0].LocationAndTime1_ddlPickUpLocation.value
        var sDropOffLocCode = document.forms[0].LocationAndTime1_ddlReturnLocation.value


        var selCategory = Value.toLowerCase()

        if (document.forms[0].LocationAndTime1_ddlBrand == null)
            var sBrand = '<%=Session("tmp_brand")%>'
        else
            var sBrand = document.forms[0].LocationAndTime1_ddlBrand.value


        var xmltext = replaceAll(replaceAll(replaceAll(document.forms[0].LocationAndTime1_LocAndTimeXML.value, '&gt;', '>'), '&lt;', '<'), '&quot;', '"')

        try //Internet explorer
        {
            var XMLDom = new ActiveXObject("Microsoft.XMLDom")
            XMLDom.loadXML(xmltext)
        }
        catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
        {
            try {
                var XMLDom = document.implementation.createDocument("", "", null);
                parser = new DOMParser();
                XMLDom = parser.parseFromString(xmltext, "text/xml");

            }
            catch (e) {
                alert(e.message);
            }
        }



        document.forms[0].LocationAndTime1_ddlPickUpLocation.innerHTML = ""
        document.forms[0].LocationAndTime1_ddlReturnLocation.innerHTML = ""

        for (var I = 0; I <= XMLDom.documentElement.childNodes[1].childNodes[1].childNodes.length - 1; I++) {
            //var CategoryNode = new ActiveXObject("Microsoft.XMLDom")
            try //Internet explorer
            {
                var CategoryNode = new ActiveXObject("Microsoft.XMLDom");
                CategoryNode.loadXML(XMLDom.documentElement.selectNodes("*//PickupAndDropOffLocations/Country")[I].xml);
            }
            catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
            {
                try {
                    var CategoryNode = document.implementation.createDocument("", "", null);
                    Nodetext = getXML(XMLDom.documentElement.childNodes[1].childNodes[1].childNodes[I])
                    parser = new DOMParser();
                    CategoryNode = parser.parseFromString(Nodetext, "text/xml");

                }
                catch (e) {
                    alert(e.message);
                }
            }


            if (CategoryNode.documentElement.attributes[1].value == selCty) {
                for (var j = 0; j <= CategoryNode.documentElement.childNodes.length - 1; j++) {
                    var a = CategoryNode.documentElement.childNodes[j].attributes[0].value.toLowerCase()

                    if (a == selCategory) {
                        //var LocNode = new ActiveXObject("Microsoft.XMLDom")
                        var LocNode
                        try //Internet explorer
                        {
                            LocNode = new ActiveXObject("Microsoft.XMLDom")
                            LocNode.loadXML(CategoryNode.documentElement.childNodes[j].xml)
                        }
                        catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
                        {
                            try {
                                LocNode = document.implementation.createDocument("", "", null);
                                Nodetext = getXML(CategoryNode.documentElement.childNodes[j])
                                parser = new DOMParser();
                                LocNode = parser.parseFromString(Nodetext, "text/xml");


                            }
                            catch (e) {
                                alert(e.message);
                            }
                        }


                        for (var iLoc = 0; iLoc <= LocNode.documentElement.childNodes.length - 1; iLoc++) {
                            var sLocCode = LocNode.documentElement.childNodes[iLoc].attributes[0].value
                            var sLocDesc = LocNode.documentElement.childNodes[iLoc].attributes[1].value

                            var obj = new Option(sLocDesc, sLocCode);

                            var Obj1 = new Option(sLocDesc, sLocCode);

                            document.forms[0].LocationAndTime1_ddlPickUpLocation.options.add(obj)
                            document.forms[0].LocationAndTime1_ddlReturnLocation.options.add(Obj1)
                            if (sLocCode == sPickUpLocCode && bPickUpSelected == false) {
                                bPickUpSelected = true
                                document.forms[0].LocationAndTime1_ddlPickUpLocation.value = sLocCode
                            }
                            if (sLocCode == sDropOffLocCode && bDropOffSelected == false) {
                                bDropOffSelected = true
                                document.forms[0].LocationAndTime1_ddlReturnLocation.value = sLocCode
                            }
                        }
                    }

                }
            }
        }

        if (('<%=request("hd_CkoLocCode")%>' != '') && (bPickUpSelected == true))
            document.forms[0].hd_CkoLocCode.value = '<%=request("hd_CkoLocCode")%>'
        else
            document.forms[0].hd_CkoLocCode.value = document.forms[0].LocationAndTime1_ddlPickUpLocation.value

        if (('<%=request("hd_CkiLocCode")%>' != '') && (bDropOffSelected == true))
            document.forms[0].hd_CkiLocCode.value = '<%=request("hd_CkiLocCode")%>'
        else
            document.forms[0].hd_CkiLocCode.value = document.forms[0].LocationAndTime1_ddlReturnLocation.value


        var countryCode = document.forms[0].LocationAndTime1_ddlCountry.value.toLowerCase()
        var vehicleCode = selCategory.toLowerCase()
        var locationCode = document.forms[0].LocationAndTime1_ddlPickUpLocation.value.toLowerCase()
        var bPickUpTime = 1

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkoLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)
        var locationCode = document.forms[0].LocationAndTime1_ddlReturnLocation.value.toLowerCase()
        var bPickUpTime = 0
        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkiLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        // Change for Promo Code Drop Down
        ProcessPromoCodeList();
        // Change for Promo Code Drop Down

    }


    function ddlPickUpLocation_SelectedIndexChanged(sValue) {
        PerformSlotStuff();
        document.forms[0].hd_CkoLocCode.value = sValue
        var countryCode = document.forms[0].LocationAndTime1_ddlCountry.value.toLowerCase()
        var vehicleCode = document.forms[0].LocationAndTime1_ddlCategory.value.toLowerCase()
        var locationCode = sValue.toLowerCase()
        var bPickUpTime = 1
        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)
        var locationCode = document.forms[0].LocationAndTime1_ddlReturnLocation.value.toLowerCase()
        var bPickUpTime = 0
        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)


    }

    function ddlReturnLocation_SelectedIndexChanged(sValue) {
        PerformContinueStuff();
        document.forms[0].hd_CkiLocCode.value = sValue
        var countryCode = document.forms[0].LocationAndTime1_ddlCountry.value.toLowerCase()
        var vehicleCode = document.forms[0].LocationAndTime1_ddlCategory.value.toLowerCase()
        var locationCode = document.forms[0].LocationAndTime1_ddlPickUpLocation.value.toLowerCase()
        var bPickUpTime = 1
        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)
        var locationCode = sValue.toLowerCase()
        var bPickUpTime = 0
        LoadTimeList(countryCode, vehicleCode, locationCode, bPickUpTime)

    }

    // Change for Promo Code Drop Down
    function ddlAgentCode_SelectedIndexChanged(sValue) {
        Message.innerHTML = " Loading -- Please Wait";
        // Change for Promo Code Drop Down
        ProcessPromoCodeList();
        // Change for Promo Code Drop Down
    }

    function cmbPromoCode_SelectedIndexChanged(sValue) {
        updatePromoCodeSelection(sValue);
        // also validate the package selection w.r.t the date range
        validatePromoCodeSelection();
    }
    // Change for Promo Code Drop Down

    function BeginPageLoad() {
        Message.innerHTML = " Loading -- Please Wait";


        var locationCode = document.forms[0].LocationAndTime1_ddlPickUpLocation.value.toLowerCase()

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkoLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        locationCode = document.forms[0].LocationAndTime1_ddlReturnLocation.value.toLowerCase()

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkiLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        iIntervalId = window.setInterval("iLoopCounter=UpdateProgress(iLoopCounter, iMaxLoop)", 500);

    }

    function EndPageLoad() {
        window.clearInterval(iIntervalId);
        Message.innerHTML = "";
        Progress.innerHTML = "";
        ResetControl()
    }

    function UpdateProgress(iCurrentLoopCounter, iMaximumLoops) {

        iCurrentLoopCounter += 1;

        if (iCurrentLoopCounter <= iMaximumLoops) {
            Progress.innerHTML += ".";
            return iCurrentLoopCounter;
        }
        else {
            Progress.innerHTML = "";
            return 1;
        }
    }

    function ddlPickupDate_SelectedIndexChanged(sValue) {
        PerformSlotStuff();
        try {
            var selDay = document.forms[0].LocationAndTime1_ddlPickupDay.value
            document.forms[0].LocationAndTime1_ddlPickupDay.innerHTML = ""
        }
        catch (e) {
            var selDay = document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickupDay.value
            document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickupDay.innerHTML = ""
        }

        var MonthYear = sValue.split(',')
        var Month = MonthYear[0]
        var Year = MonthYear[1]
        var bSelected = false
        var PickUpDate = new Date(Month + '/01/' + Year)
        for (var i = 0; i <= 31; i++) {
            if (parseInt(PickUpDate.getMonth()) + 1 != parseInt(Month)) {

                if (bSelected == false) {
                    try {
                        document.forms[0].hd_PickupDay.value = document.forms[0].LocationAndTime1_ddlPickupDay.value
                    }
                    catch (e) {
                        document.forms[0].hd_PickupDay.value = document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickupDay.value

                    }

                }
                return
            }
            else {
                //alert(i)
                //var obj = Option.create(PickUpDate.getDate(),PickUpDate.getDate())
                //var obj = document.createElement("option");
                var obj = new Option(PickUpDate.getDate(), PickUpDate.getDate());
                //obj.text = PickUpDate.getDate();
                //obj.value = PickUpDate.getDate();
                try {
                    document.forms[0].LocationAndTime1_ddlPickupDay.options.add(obj)
                }
                catch (e) {
                    document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickupDay.options.add(obj)
                }
                if (i == selDay && bSelected == false) {
                    bSelected = true
                    try {
                        document.forms[0].LocationAndTime1_ddlPickupDay.value = i
                    }
                    catch (e) {
                        document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickupDay.value = i

                    }
                }
            }
            PickUpDate = new Date(PickUpDate.getTime() + (24 * 60 * 60 * 1000))
        }

        //alert(selDay)
        try {
            document.forms[0].LocationAndTime1_ddlPickupDay.value = selDay
        }
        catch (e) {
            document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickupDay.value = selDay
        }

    }


    function ddlReturnDate_SelectedIndexChanged(sValue) {
        PerformContinueStuff();
        try {
            var selDay = document.forms[0].LocationAndTime1_ddlReturnDay.value
            document.forms[0].LocationAndTime1_ddlReturnDay.innerHTML = ""
        }
        catch (e) {
            var selDay = document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlReturnDay.value
            document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlReturnDay.innerHTML = ""
        }

        var MonthYear = sValue.split(',')
        var Month = MonthYear[0]
        var Year = MonthYear[1]
        var bSelected = false
        var ReturnDate = new Date(Month + '/01/' + Year)
        for (var i = 0; i <= 31; i++) {
            if (parseInt(ReturnDate.getMonth()) + 1 != parseInt(Month)) {

                if (bSelected == false) {
                    try {
                        document.forms[0].hd_PickupDay.value = document.forms[0].LocationAndTime1_ddlReturnDay.value
                    }
                    catch (e) {
                        document.forms[0].hd_PickupDay.value = document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlReturnDay.value

                    }

                }
                return
            }
            else {
                //alert(i)
                //var obj = Option.create(PickUpDate.getDate(),PickUpDate.getDate())
                //var obj = document.createElement("option");
                var obj = new Option(ReturnDate.getDate(), ReturnDate.getDate());
                //obj.text = PickUpDate.getDate();
                //obj.value = PickUpDate.getDate();
                try {
                    document.forms[0].LocationAndTime1_ddlReturnDay.options.add(obj)
                }
                catch (e) {
                    document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlReturnDay.options.add(obj)
                }
                if (i == selDay && bSelected == false) {
                    bSelected = true
                    try {
                        document.forms[0].LocationAndTime1_ddlReturnDay.value = i
                    }
                    catch (e) {
                        document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlReturnDay.value = i

                    }
                }
            }
            ReturnDate = new Date(ReturnDate.getTime() + (24 * 60 * 60 * 1000))
        }
        //alert(selDay)
        try {
            document.forms[0].LocationAndTime1_ddlReturnDay.value = selDay
        }
        catch (e) {
            document.forms[0].BookingContainer1_ModifyBooking1_LocationAndTime1_ddlReturnDay.value = selDay
        }

    }
    function ddlPickUpTime_SelectedIndexChanged(sValue) {
        document.forms[0].hd_PickupTime.value = sValue

        var locationCode = document.forms[0].LocationAndTime1_ddlPickUpLocation.value.toLowerCase()

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkoLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        locationCode = document.forms[0].LocationAndTime1_ddlReturnLocation.value.toLowerCase()

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkiLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

    }

    function ddlReturnTime_SelectedIndexChanged(sValue) {
        PerformContinueStuff();
        document.forms[0].hd_DropOffTime.value = sValue

        var locationCode = document.forms[0].LocationAndTime1_ddlPickUpLocation.value.toLowerCase()

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkoLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank

        locationCode = document.forms[0].LocationAndTime1_ddlReturnLocation.value.toLowerCase()

        // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkiLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank
    }
    function ddlPickupDay_SelectedIndexChanged(sValue) {
        PerformSlotStuff();
        document.forms[0].hd_PickupDay.value = sValue
    }
    function ddlReturnDay_SelectedIndexChanged(sValue) {
        PerformContinueStuff();
        document.forms[0].hd_ReturnDay.value = sValue
    }

    // Change for Promo Code Drop Down

    function ProcessPromoCodeList(sChosenValue, bValidateDateRangeOnly, dtPickUpDateToValidate, dtReturnDateToValidate) {
        // 1. Load the master list of Country-Brand-Agent-Package 

        var XMLDom;
        var xmltext = replaceAll(replaceAll(replaceAll(document.getElementById('<%=hdnMasterPackageXML.ClientID %>').value, '&gt;', '>'), '&lt;', '<'), '&quot;', '"');

        try //Internet explorer
        {
            XMLDom = new ActiveXObject("Microsoft.XMLDom")
            XMLDom.async = "false";
            XMLDom.loadXML(xmltext)
        }
        catch (e) ////Firefox, Mozilla, Opera, etc.<BR>
        {

            try {
                XMLDom = document.implementation.createDocument("", "", null);
                //alert(XMLDom)
                parser = new DOMParser();
                XMLDom = parser.parseFromString(xmltext, "text/xml");
            }
            catch (e) {
                alert(e.message);
            }
        }

        // 2. Get the filtered nodes from the master package xml
        var sCountryCode, sBrandName, sAgentCode, sVehicleCategoryCode;
        sCountryCode = document.getElementById('<%=ddlCountry.ClientID %>').value;
        sBrandName = document.getElementById('<%=ddlBrand.ClientID %>').value;
        sAgentCode = document.getElementById('<%=ddlAgentCode.ClientID %>').value;
        sVehicleCategoryCode = document.getElementById('<%=ddlCategory.ClientID %>').value;

        if (sVehicleCategoryCode == "V2" || sVehicleCategoryCode == "V4") {
            sVehicleCategoryCode = "AV";
        }
        else {
            sVehicleCategoryCode = "AC";
        }

        var results = evaluateXPath(XMLDom, "//Data/Country[@Code='" + sCountryCode + "']/Agent[@Code='" + sAgentCode + "']/Brand[@Name='" + sBrandName + "']/VehicleClass[@Code='" + sVehicleCategoryCode + "']/Package");

        if (bValidateDateRangeOnly != true) {
            // 3. Populate the promo code dropdown
            document.getElementById('<%= cmbPromoCode.ClientID%>').options.length = 0;

            // add a blank option
            addOptionsToDropDown(document.getElementById('<%= cmbPromoCode.ClientID%>'), "", "All");

            // go through the nodes selected
            if (typeof (results.length) != "undefined") { // IE
                if (results.length > 0) {
                    for (var i = 0; i < results.length; i++) {
                        addOptionsToDropDown(document.getElementById('<%= cmbPromoCode.ClientID%>'), evaluateXPath(results[i], "@Code")[0].nodeValue, evaluateXPath(results[i], "@Code")[0].nodeValue + " - " + evaluateXPath(results[i], "@Name")[0].nodeValue);
                    }
                }
            }
            else { // FF and others
                var oNode;
                while (oNode = results.iterateNext()) {
                    addOptionsToDropDown(document.getElementById('<%= cmbPromoCode.ClientID%>'), evaluateXPath(oNode, "@Code").iterateNext().textContent, evaluateXPath(oNode, "@Code").iterateNext().textContent + " - " + evaluateXPath(oNode, "@Name").iterateNext().textContent);
                }
            }

            if (sChosenValue == null) {
                updatePromoCodeSelection(""); // clear selection
            }
            else {
                updatePromoCodeSelection(sChosenValue); // store it in the hidden field

                var cmbPromoCode = document.getElementById('<%= cmbPromoCode.ClientID%>');
                for (var i = 0; i < cmbPromoCode.options.length; i++) {
                    if (cmbPromoCode.options[i].value == sChosenValue) {
                        cmbPromoCode.options[i].selected = true;
                        break;
                    }
                }
            }
        }
        else {
            // this is the bit where a promo code is validated against the chosen date range
            updatePromoCodeSelection(sChosenValue);

            // clear the error message
            Message.innerHTML = "";

            var dtCurrentPromoCodeStartDate, dtCurrentPromoCodeEndDate;
            var arrStartDate, arrEndDate;

            // go through the nodes selected
            if (typeof (results.length) != "undefined") { // IE
                if (results.length > 0) {
                    for (var i = 0; i < results.length; i++) {

                        if (sChosenValue == evaluateXPath(results[i], "@Code")[0].nodeValue) {
                            dtCurrentPromoCodeStartDate = evaluateXPath(results[i], "@TravelFrom")[0].nodeValue;
                            dtCurrentPromoCodeEndDate = evaluateXPath(results[i], "@TravelTo")[0].nodeValue;

                            arrStartDate = dtCurrentPromoCodeStartDate.split('/');
                            arrEndDate = dtCurrentPromoCodeEndDate.split('/');

                            dtCurrentPromoCodeStartDate = new Date(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
                            dtCurrentPromoCodeEndDate = new Date(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);

                            if (dtCurrentPromoCodeStartDate > dtPickUpDateToValidate || dtCurrentPromoCodeEndDate < dtReturnDateToValidate) {
                                // set the error message
                                Message.innerHTML = "The promo code - " + sChosenValue + " - is only valid for travel from " + getStringRepresentationOfDate(dtCurrentPromoCodeStartDate) + ' to ' + getStringRepresentationOfDate(dtCurrentPromoCodeEndDate);
                                return -1;
                            }
                        }
                    }
                }
            }
            else { // FF and others
                var oNode;
                while (oNode = results.iterateNext()) {

                    if (sChosenValue == evaluateXPath(oNode, "@Code").iterateNext().textContent) {
                        dtCurrentPromoCodeStartDate = evaluateXPath(oNode, "@TravelFrom").iterateNext().textContent;
                        dtCurrentPromoCodeEndDate = evaluateXPath(oNode, "@TravelTo").iterateNext().textContent;

                        arrStartDate = dtCurrentPromoCodeStartDate.split('/');
                        arrEndDate = dtCurrentPromoCodeEndDate.split('/');

                        dtCurrentPromoCodeStartDate = new Date(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
                        dtCurrentPromoCodeEndDate = new Date(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);

                        if (dtCurrentPromoCodeStartDate > dtPickUpDateToValidate || dtCurrentPromoCodeEndDate < dtReturnDateToValidate) {
                            // set the error message
                            Message.innerHTML = "The promo code - " + sChosenValue + " - is only valid for travel from " + getStringRepresentationOfDate(dtCurrentPromoCodeStartDate) + ' to ' + getStringRepresentationOfDate(dtCurrentPromoCodeEndDate);
                            return -1;
                        }
                    }
                }
            }
            // this is the bit where a promo code is validated against the chosen date range

        }

    }

    // Evaluate an XPath expression aExpression against a given DOM node
    // or Document object (aNode), returning the results as an array
    function evaluateXPath(xmlDoc, xpath) {
        try {//Internet explorer
            return xmlDoc.selectNodes(xpath);
        }
        catch (ex) {////Firefox, Mozilla, Opera, etc.
            //return xmlDoc.evaluate(xpath, xmlDoc, null, XPathResult.ANY_TYPE, null);
            var oEvaluator = new XPathEvaluator();
            return oEvaluator.evaluate(xpath, xmlDoc, null, XPathResult.ANY_TYPE, null);
        }
    }

    function addOptionsToDropDown(dropDown, optionValue, optionText) {
        // Create an Option object        
        var opt = new Option();
        // Assign text and value to Option object
        opt.text = optionText;
        opt.value = optionValue;

        // Add an Option object to Drop Down/List Box
        dropDown.options.add(opt);
    }

    function updatePromoCodeSelection(sValue) {
        document.getElementById('hd_PromoCode').value = sValue;
    }

    function validatePromoCodeSelection() {
        // check if the date ranges are valid
        //shoeldebug
        var dtPickUpDate, dtDropOffDate;

        var cmbPickupDate = document.getElementById('<%=ddlPickupDate.ClientID %>');
        var cmbReturnDate = document.getElementById('<%=ddlReturnDate.ClientID %>');

        var cmbPickupDay = document.getElementById('<%=ddlPickupDay.ClientID %>');
        var cmbReturnDay = document.getElementById('<%=ddlReturnDay.ClientID %>');

        // get the pickup date and drop off date
        var PickUpMonthYear = cmbPickupDate.value.split(',');
        var PickUpMonth = PickUpMonthYear[0];
        var PickUpYear = PickUpMonthYear[1];

        var ReturnMonthYear = cmbReturnDate.value.split(',');
        var ReturnMonth = ReturnMonthYear[0];
        var ReturnYear = ReturnMonthYear[1];

        var PickUpDate = new Date(PickUpMonth + '/' + cmbPickupDay.value + '/' + PickUpYear);
        var ReturnDate = new Date(ReturnMonth + '/' + cmbReturnDay.value + '/' + ReturnYear);

        return ProcessPromoCodeList(document.getElementById('hd_PromoCode').value, true, PickUpDate, ReturnDate);
    }

    // date format
    function getStringRepresentationOfDate(dateToProcess) {
        var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        var curr_date = dateToProcess.getDate();
        var curr_month = dateToProcess.getMonth();
        var curr_year = dateToProcess.getFullYear();

        return (curr_date + " " + m_names[curr_month] + " " + curr_year);
    }

    // Change for Promo Code Drop Down
    function PerformSlotStuff() {
        if ($("#BookingContainer1_ModifyBooking1_LocationAndTime1_IsQuote").val() != "true") {
            //alert('Performing slot stuff');
            iPickupChanged = true;
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_slotsDetail').html('');
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_txtSlot').val('');
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_txtSlotId').val('0');
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_btnModifyBooking').show();
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_btnContinue').hide();
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_lblNoNeedMessaeg').hide();
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickUpTime').hide();
            $('#BookingContainer1_ModifyBooking1_LocationAndTime1_lblPickUpTime').hide();
        }


    }
    function PerformContinueStuff() {
        if ($("#BookingContainer1_ModifyBooking1_LocationAndTime1_IsQuote").val() != "true") {
            if (iPickupChanged === false) {
                $('#BookingContainer1_ModifyBooking1_LocationAndTime1_btnModifyBooking').hide();
                $('#BookingContainer1_ModifyBooking1_LocationAndTime1_btnContinue').show();
                $('#BookingContainer1_ModifyBooking1_LocationAndTime1_lblNoNeedMessaeg').hide();
                $('#BookingContainer1_ModifyBooking1_LocationAndTime1_ddlPickUpTime').show();
                $('#BookingContainer1_ModifyBooking1_LocationAndTime1_lblPickUpTime').show();
            }
        }
    }
</script>
<table id="tblPriceAndAvailability" cellspacing="0" cellpadding="2" width="746" border="0"
    runat="server">
    <tr>
        <td class="CellHeading">Country
        </td>
        <td>
            <asp:DropDownList ID="ddlCountry" runat="server" Width="200px">
            </asp:DropDownList>
        </td>
        <td class="CellHeading" style="height: 2px">Brand
        </td>
        <td style="height: 2px">
            <asp:DropDownList ID="ddlBrand" runat="server" Width="100px">
                <asp:ListItem Value="Maui">Maui</asp:ListItem>
                <asp:ListItem Value="Britz">Britz</asp:ListItem>
                <asp:ListItem Value="Backpacker">Backpacker</asp:ListItem>
                <%--rev:mia May 22 2012 -- added Mighty--%>
                <asp:ListItem Value="Mighty">Mighty</asp:ListItem>
                <asp:ListItem Value="Kea">Kea</asp:ListItem>
                <%--rev:mia- oct.16, 2012 - addition of Alpha and Econo--%>
                <asp:ListItem Value="United">United</asp:ListItem>
                <asp:ListItem Value="Econo">Econo</asp:ListItem>
                <asp:ListItem Value="Alpha">Alpha</asp:ListItem>
                <%--rev:mia- sept.19, 2013 - addition of RoadBear and Britz-US--%>
                <asp:ListItem Value="RoadBear">RoadBear</asp:ListItem>
                <asp:ListItem Value="Britz USA">Britz USA</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr >
        <td class="CellHeading" style="height: 2px" align="left">Vehicle Category
        </td>
        <td style="height: 2px">
            <asp:DropDownList ID="ddlCategory" runat="server" Width="200px">
                <asp:ListItem Value="V2">Motorhome 2wd</asp:ListItem>
                <asp:ListItem Value="V4">Motorhome 4wd</asp:ListItem>
                <asp:ListItem Value="C2">Car 2wd</asp:ListItem>
                <asp:ListItem Value="C4">Car 4wd</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="CellHeading" style="height: 2px">Size
        </td>
        <td style="height: 2px">
            <asp:DropDownList ID="ddlSize" runat="server" Width="100px">
                <asp:ListItem Value="NA">All</asp:ListItem>
                <asp:ListItem Value="2B">2 Berth</asp:ListItem>
                <asp:ListItem Value="4B">4 Berth</asp:ListItem>
                <asp:ListItem Value="6B">6 Berth</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td style="height: 2px"></td>
    </tr>
    <tr >
        <td class="CellHeading">
            <asp:Label ID="lblPromoCode" runat="server">Promotional Code</asp:Label>
        </td>
        <td>
            <%--rev:mia May 22 2012 -- added Mighty--%>
            <asp:HiddenField ID="hdnMasterPackageXML" runat="server" />
            <asp:DropDownList ID="cmbPromoCode" runat="server" Width="275px">
            </asp:DropDownList>
        </td>
        <td class="CellHeading">
            <asp:Label ID="lblAgentCode" runat="server">Agent Code</asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlAgentCode" AutoPostBack="true" runat="server" Width="100%">
            </asp:DropDownList>
        </td>
        <td align="right"></td>
    </tr>
    <tr>
        <td class="CellHeading" style="height: 1px">Pick up Location
        </td>
        <td style="height: 1px">
            <asp:DropDownList ID="ddlPickUpLocation" runat="server" Width="200px">
            </asp:DropDownList>
        </td>
        <td class="CellHeading" style="height: 1px">Return Location
        </td>
        <td style="height: 1px">
            <asp:DropDownList ID="ddlReturnLocation" runat="server" Width="200px">
            </asp:DropDownList>
        </td>
        <td style="height: 1px"></td>
    </tr>
    <tr >
        <td class="CellHeading" valign="middle" style="height: 23px">Pick up Date
        </td>
        <td style="height: 23px">
            <asp:DropDownList ID="ddlPickupDay" runat="server" Width="45px">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlPickupDate" runat="server" Width="155px">
            </asp:DropDownList>
        </td>
        <td class="CellHeading" valign="middle" style="height: 23px">Return Date
        </td>
        <td style="height: 23px">
            <asp:DropDownList ID="ddlReturnDay" runat="server" Width="45px">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlReturnDate" runat="server" Width="155px">
            </asp:DropDownList>
        </td>
        <td style="height: 23px"></td>
    </tr>
    <tr>
        <td class="CellHeading"><asp:Label class="CellHeading" ID="lblPickUpTime" runat="server" Text="Pick up Time"></asp:Label></td>
        <td><asp:DropDownList ID="ddlPickUpTime" runat="server" Width="93px">
                </asp:DropDownList></td>
        <td class="CellHeading">Return&nbsp;Time
        </td>
        <td>
            <asp:DropDownList ID="ddlReturnTime" runat="server" Width="93px">
            </asp:DropDownList>
        </td>
        <td></td>
    </tr>
    
    <!-- Display Slots Details if available, added by nimesh on 16th Feb 2015 -->
    <tr>

        <td colspan="4" >
            
                <%--rev:mia- oct.16, 2012 - addition of Alpha and Econo--%>
                <%--<asp:Label class="CellHeading" ID="lblPickUpTime" runat="server" Text="Pick up Time &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp"></asp:Label>
                <asp:DropDownList ID="ddlPickUpTime" runat="server" Width="93px">
                </asp:DropDownList>--%>

                <asp:Label ID="lblNoNeedMessaeg" runat="server" Text="No need to change bookingdetails, no changes applied"></asp:Label>
                &nbsp;<div id="slotsDetail" runat="server">
                    <div id="configureContainer" style="width:auto;border:none;margin:0">
                    <div class="title">
                        <h4 style="font-weight:bold;font-size:1em;color:#222222">Select your pickup time:&nbsp;</h4>
                    </div>
                    
                    <div id="pickuptimes" style="margin:0px">

                        <div id="times" style="width:auto;">
                            <%--rev:mia- sept.19, 2013 - addition of RoadBear and Britz-US--%><%--rev:mia May 22 2012 -- added Mighty--%>
                            <%=slotsPanel%>
                        </div>

                    </div>
                        </div>
                    <br />

                    <asp:HiddenField ID="txtSlot" Value="" runat="server" />
                    <asp:HiddenField ID="txtSlotId" Value="0" runat="server" />
                </div>
                <asp:HiddenField ID="IsQuote" runat="server" Value="false" />
            
        </td>
    </tr>
    <tr style="visibility: hidden" >
        <td class="CellHeading">
            <asp:Label ID="Label1" runat="server">Vehicle Code</asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtVehicleCode" runat="server" Width="200px" MaxLength="32"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="4">
            <asp:Label ID="lblErrorOccurred" runat="server" CssClass="warning"></asp:Label>
            <asp:TextBox Width="0" Height="0" Style="visibility: hidden" AutoPostBack="True"
                ID="LocAndTimeXML" runat="server"></asp:TextBox>
        </td>

    </tr>
    <tr>
        <td valign="top" align="right" colspan="4">
            <asp:Button ID="btnAvailability" runat="server" Width="100px" Text="Get Availability"></asp:Button>
            <asp:Button ID="btnModifyBooking" runat="server" Width="197px" Text="Modify Pickup Time or Booking"></asp:Button>
            <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClientClick="return validateSlot();" />
            <asp:Button ID="btnCancel" runat="server" Width="100px" Text="Cancel"
                Visible="False"></asp:Button>
        </td>
    </tr>
</table>
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_CkoLocCode"
    name="hd_CkoLocCode" value="<%=request("hd_CkoLocCode")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_CkiLocCode"
    name="hd_CkiLocCode" value="<%=request("hd_CkiLocCode")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_Category"
    name="hd_Category" value="<%=request("hd_Category")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_Brand"
    name="hd_Brand" value="<%=request("hd_Brand")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_PickupTime"
    name="hd_PickupTime" value="<%=request("hd_PickupTime")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_DropOffTime"
    name="hd_DropOffTime" value="<%=request("hd_DropOffTime")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_PickupDay"
    name="hd_PickupDay" value="<%=request("hd_PickupDay")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_ReturnDay"
    name="hd_ReturnDay" value="<%=request("hd_ReturnDay")%>">
<input type="text" width="0" height="0" style="visibility: hidden" id="hd_PromoCode"
    name="hd_PromoCode" value="<%=request("hd_PromoCode")%>">

<script>

    if ('<%=request("hd_Brand")%>' != '') {
        document.forms[0].LocationAndTime1_ddlBrand.value = '<%=request("hd_Brand")%>'
        ddlBrand_SelectedIndexChanged('<%=request("hd_Brand")%>')
    }
    if ('<%=request("hd_Category")%>' != '') {
        document.forms[0].LocationAndTime1_ddlCategory.value = '<%=request("hd_Category")%>'
        ddlCategory_SelectedIndexChanged('<%=request("hd_Category")%>')
    }
    if ('<%=request("hd_CkoLocCode")%>' != '')
        document.forms[0].LocationAndTime1_ddlPickUpLocation.value = '<%=request("hd_CkoLocCode")%>'

    if ('<%=request("hd_CkiLocCode")%>' != '')
        document.forms[0].LocationAndTime1_ddlReturnLocation.value = '<%=request("hd_CkiLocCode")%>'

    if ('<%=request("hd_PickupTime")%>' != '')
        document.forms[0].LocationAndTime1_ddlPickUpTime.value = '<%=request("hd_PickupTime")%>'

    if ('<%=request("hd_DropOffTime")%>' != '')
        document.forms[0].LocationAndTime1_ddlReturnTime.value = '<%=request("hd_DropOffTime")%>'

    if ('<%=request("hd_PickupDay")%>' != '')
        document.forms[0].LocationAndTime1_ddlPickupDay.value = '<%=request("hd_PickupDay")%>'

    if ('<%=request("hd_ReturnDay")%>' != '' && document.forms[0].LocationAndTime1_ddlReturnDay != null)
        document.forms[0].LocationAndTime1_ddlReturnDay.value = '<%=request("hd_ReturnDay")%>'

    // this line forces the dropdown value to be selected correclty after the postback
    ProcessPromoCodeList('<%=request("hd_PromoCode")%>');
    // this line forces the dropdown value to be selected correclty after the postback
</script>

<script type="text/javascript">
    // added by Nimesh on 16th Feb 2015 for selection change for timeslots
    var disableAll;
    <% If DisableAll Then%>
    disableAll = true;
    <% Else%>
    disableAll = <%= DisableAll.ToString().ToLower() %>
    <% End If%>


     $(document).ready(function () {
         $("#BookingContainer1_ModifyBooking1_LocationAndTime1_IsQuote").val(<%=IsQuoteValue%>);
         if (!disableAll) {
             var timeLeft = jQuery('.timeslots.available.selected').find('.time.left').text();
             var timeRight = jQuery('.timeslots.available.selected').find('.time.right').text();
             var slotId
             if (jQuery('.timeslots.available.selected').attr('id') != undefined) {
                 slotId = jQuery('.timeslots.available.selected').attr('id').replace('slot', '');
                 $('#BookingContainer1_ModifyBooking1_LocationAndTime1_txtSlot').val(timeLeft + " - " + timeRight);
                 $('#BookingContainer1_ModifyBooking1_LocationAndTime1_txtSlotId').val(slotId);
             }
             jQuery('.timeslots.available').click(function () {
                 //alert('clicked');
                 //jQuery('.timeslots.available').removeClass('selected').find('.availability').text('available');
                 //jQuery(this).toggleClass('selected').find('.availability').text('selected');

                 jQuery('.timeslots.available').removeClass('selected');
                 jQuery('.timeslots.available').find('.availability').each(function () {
                     jQuery(this).text(jQuery(this).text().replace('SELECTED', 'AVAILABLE'));
                 }
                     )
                 //jQuery('.timeslots.available').find('.availability').text(function () {
                 //   return $(this).text().replace("selected", "available"); 
                 //})
                 jQuery(this).toggleClass('selected');
                 jQuery(this).find('.availability').text(jQuery(this).find('.availability').text().replace('AVAILABLE', 'SELECTED'));
                 //jQuery(this).find('.availability').text(function () {
                 //   return $(this).text().replace("available", "selected"); 
                 //})
                 //var timeLeft = jQuery('.timeslots.available.selected').find('.time.left').each(function () {
                 //    return this.innerText;
                 //});
                 //var timeRight = jQuery('.timeslots.available.selected').find('.time.right').each(function () {
                 //    return this.innerText + "'";
                 //});
                 var timeLeft = jQuery('.timeslots.available.selected').find('.time.left').text();
                 var timeRight = jQuery('.timeslots.available.selected').find('.time.right').text();
                 var slotId = jQuery('.timeslots.available.selected').attr('id').replace('slot', '');
                 $('#BookingContainer1_ModifyBooking1_LocationAndTime1_txtSlot').val(timeLeft + " - " + timeRight);
                 $('#BookingContainer1_ModifyBooking1_LocationAndTime1_txtSlotId').val(slotId);
             })
         }
         else {
             //jQuery("#paymentForm :input").attr('disabled', true);
             //jQuery('.SubmitBtn').hide();
             jQuery('#slotsDetail').hide();
             //jQuery('.SubmitBtn').attr('disabled', 'disabled');
             //jQuery('.SubmitBtn').removeClass("SubmitBtn");
         }

         // end added by Nimesh on 13th Feb 2015 for selection change for timeslots
         if ($('#BookingContainer1_ModifyBooking1_LocationAndTime1_lblNoNeedMessaeg').length === 1)
             iPickupChanged = false;
     });
    function validateSlot(e) {
        /* Added by Nimesh on 23rd Feb 2015 to validate if slot is selected*/
        var $k = $("#selectionError");
        $k.remove();
        var $j = $("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");

        if (jQuery('.timeslots.available').length > 0) {
            if (!(jQuery('.timeslots.available.selected').length === 1)) {
                //var error = $j("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");
                $j.insertBefore('#pickuptimes');
                var position = jQuery('#pickuptimes').position();
                scroll(0, position.TopPosition);
                return false;
            }
            else {
                //$j.remove();
                selSlot = true;
                return true;
            }
        }
        /* End Added by Nimesh on 23rd Feb 2015 to validate if slot is selected*/
    }
</script>
