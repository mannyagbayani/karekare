<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Availability.ascx.vb"
    Inherits="THLAuroraWebInterface.Availability" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table id="tblVehicleSummary" width="746" runat="server" cellspacing="0" cellpadding="2">
    <%--Customer Confirmation Changes--%>
    <tr>
        <td align="right" id="tdGrossNett" runat="server" style="display: none; float: left;width:40%">
            <asp:RadioButtonList ID="rdgGrossNett" runat="server" CssClass="GrossNettLabel" AutoPostBack="true"
                RepeatDirection="Horizontal">
                <asp:ListItem Text="Gross Amount" Value="Gross" />
                <asp:ListItem Text="Nett Amount" Value="Nett" />
            </asp:RadioButtonList>
        </td>
        <td align="right" id="tdCurrency" runat="server" style="display: none; font-family: Arial, Helvetica, sans-serif;
            font-size: 11px; font-style: normal; float: right;width:40%">
            Currency Selection:
            <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="true" />
        </td>
    </tr>
    <%--Customer Confirmation Changes--%>
    <tr>
        <td colspan="2">
            <asp:DataGrid ID="dgrVehicle" runat="server" BorderColor="#999999" BorderWidth="1px"
                AutoGenerateColumns="False" DataKeyField="Vehicle" AllowSorting="True" OnSelectedIndexChanged="dgrVehicle_SelectedIndexChanged"
                Width="746px" CellPadding="2">
                <SelectedItemStyle BackColor="#ffffcc"></SelectedItemStyle>
                <ItemStyle CssClass="DatagridItem"></ItemStyle>
                <HeaderStyle CssClass="DatagridHeading"></HeaderStyle>
                <Columns>
                    <asp:ButtonColumn Text="Select" CommandName="Select">
                        <ItemStyle CssClass="DatagridItem"></ItemStyle>
                    </asp:ButtonColumn>
                    <asp:BoundColumn DataField="Vehicle" HeaderText="Vehicle"></asp:BoundColumn>
                    <asp:BoundColumn Visible="False" DataField="Product" HeaderText="Product"></asp:BoundColumn>
                    <asp:BoundColumn Visible="False" DataField="Availability" HeaderText="Availability">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Package" HeaderText="Package"></asp:BoundColumn>
                    <asp:BoundColumn DataField="VehicleRate" HeaderText="Vehicle Rate">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <%--New Columns--%>
                    <asp:BoundColumn DataField="THLAgentCharge" HeaderText="Payable to Agent">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="THLCustomerCharge" HeaderText="Payable at Pick Up">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <%--New Columns--%>
                    <asp:BoundColumn DataField="TotalCost" HeaderText="Total Estimated Cost">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <%--Unwanted Columns--%>
                    <asp:BoundColumn Visible="false" DataField="RentalCharge" HeaderText="Rental Charges">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="false" DataField="OtherCharge" HeaderText="Other Charges &amp; Fees">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <%--Unwanted Columns--%>
                    <asp:BoundColumn Visible="False" DataField="VendorCode" HeaderText="VendorCode">
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="false" DataField="GrossRateAvailable" />
                </Columns>
                <PagerStyle HorizontalAlign="Center" CssClass="DatagridHeading" Mode="NumericPages">
                </PagerStyle>
            </asp:DataGrid><asp:Label ID="lblErrorVehAvailRate" runat="server" CssClass="warning"></asp:Label>
        </td>
    </tr>
    <tr id="trButtons" runat="server">
        <td align="left" nowrap="nowrap" style="white-space: nowrap" colspan="2">
            <asp:Button ID="btnBook" runat="server" Width="120px" Text="Book" Visible="False"
                ToolTip="Make Booking"></asp:Button>
            <asp:Button ID="btnBookProvisional" runat="server" Width="120px" Visible="False"
                Text="Book Provisional" ToolTip="Make Provisional Booking"></asp:Button>
            <asp:Button ID="btnSaveQuote" runat="server" Width="120px" Text="Save Quote" Visible="False"
                ToolTip="Save Quote"></asp:Button>
        </td>
    </tr>
    <tr>
        <td style="float:right" align="right" colspan="2">
            <asp:Label ID="lblNettPriceIndicator" style="float:right" CssClass="GrossNettLabel" runat="server" syle="display:none">* Price displayed above for this product is NETT</asp:Label>
            <script type="text/javascript">
            try
            {
                var tblAvail = document.getElementById("<%=dgrVehicle.ClientID %>");
                var lblNettPriceIndicator = document.getElementById("<%=lblNettPriceIndicator.ClientID %>");
                if(tblAvail)
                {
                    tblAvail.innerHTML.indexOf("*") != -1 ? lblNettPriceIndicator.style.display = "block" : lblNettPriceIndicator.style.display = "none";
                }
                else
                {
                    lblNettPriceIndicator.style.display = "none";
                }
            }
            catch(err){}
            </script>
        </td>
    </tr>
</table>
<asp:Label ID="Label1" runat="server" CssClass="warning" Visible="False"></asp:Label>
<script type="text/javascript">
    function SaveDropDownSettings()
    {
        var locationCode = 	document.forms[0].LocationAndTime1_ddlPickUpLocation.value.toLowerCase()
    			
	    // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkoLocCode.value = locationCode.toUpperCase()
	    // update hidden fields also -- fix for location drop down going blank
		
	    locationCode = 	document.forms[0].LocationAndTime1_ddlReturnLocation.value.toLowerCase()
		
	    // update hidden fields also -- fix for location drop down going blank
        document.forms[0].hd_CkiLocCode.value = locationCode.toUpperCase()
        // update hidden fields also -- fix for location drop down going blank
    }
</script>