#Region "Notes"
'26June2006 Karel Jordaan
'Changed the base from System.Web.UI.UserControl
'to THL_WebUserControlBase 
'update the global propertie for access from other ascx that does not call the database
#End Region


Imports System.Xml
Imports System.Text
Imports THLAuroraWebInterface.WebServices
Imports System.Xml.Linq
Imports System.Linq
'--------------------------------------------------------------------------------------
'   Class:          LocationAndTime
'   Description:    Displays all available options based on a set of buisness rules. User 
'                   can make selection to get all available packages. Also used to modify 
'                   a current booking, quote etc.
'--------------------------------------------------------------------------------------

Partial Class LocationAndTime
    'Inherits System.Web.UI.UserControl
    Inherits THL_WebUserControlBase

    Dim sCkoLocCode As String = ""
    Dim sCkiLocCode As String = ""
    Dim sCategory As String = ""
    Dim sBrand As String = ""
    Dim sPickupTime As String = ""
    Dim sReturnTime As String = ""
    Dim sPickupDay As String = ""
    Dim sReturnDay As String = ""
    Dim sPromoCode As String = ""



#Region " Web Form Designer Generated Code "

    'Public Property DisableAll As Boolean

    Public Property slotsPanel As String

    Private Shared tPassedbookingId As String
    Public Shared Property PassedBookingId() As String
        Get
            Return tPassedbookingId
        End Get
        Set(ByVal value As String)
            tPassedbookingId = value.Replace("/", "-")
        End Set
    End Property

    Private Property BookingType As TransactionActionType

    Public Property IsQuoteValue As String


    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Protected WithEvents lblErrorOccurredOccurred As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label

    Protected WithEvents Dropdownlist2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents imgTracking As System.Web.UI.WebControls.Image

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Dim PriceAndAvailDataRS As THL_PriceAndAvailDataRS
    Public Event GetAvailabilityClicked(ByVal category As String, ByVal brand As String, ByVal pickUpLocation As String, ByVal returnLocation As String, ByVal pickUpDateTime As DateTime, ByVal returnDateTime As DateTime, ByVal promoCode As String, ByVal agentCode As String, ByVal SizeCode As String, ByVal VehicleCode As String)
    Public Event ModifyBookingclicked()
    Public Event ModifyBookingclickedV2(ByVal slot As String, ByVal slotId As String)
    Public Event CancelClicked()

    Private nAssetBaseBath As String
    Public Property AssetBasePath() As String
        Get
            Dim isSecure As Boolean = False
            Boolean.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings("SecureMode"), isSecure)

            Dim prefix As String = IIf(isSecure, "https://", "http://").ToString()
            Return prefix + System.Web.Configuration.WebConfigurationManager.AppSettings("AssetsURL")
        End Get
        Set(ByVal value As String)
            nAssetBaseBath = value
        End Set
    End Property
    Private Property PriceAndAvailabilityData() As String
        Get
            Return CStr(Session("PriceAndAvailabilityData"))
        End Get
        Set(ByVal Value As String)
            Session("PriceAndAvailabilityData") = Value
        End Set
    End Property

    Public Property DisableAll() As Boolean
        Get
            Return CBool(ViewState("DisableAll"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("DisableAll") = Value
        End Set
    End Property

    Private nPickUpDayMonthStr As String
    Public Property PickUpDayMonthStr() As String
        Get
            Return nPickUpDayMonthStr
        End Get
        Set(ByVal value As String)
            nPickUpDayMonthStr = value
        End Set
    End Property

    Private nPickUpLocationStr As String
    Public Property PickUpLocationStr() As String
        Get
            Return nPickUpLocationStr
        End Get
        Set(ByVal value As String)
            nPickUpLocationStr = value
        End Set
    End Property
    Public Enum ThisControlIsUsedTo
        ModifyABooking = 0
        GetVehicleAvailability = 1
    End Enum

    Private Enum CategoryListType
        All = 0
        NotFourWheelDriveCamperandCar = 1
        NotFourWheelDriveCar = 2
    End Enum

    Public ReadOnly Property PickUpLocation() As String
        Get
            Return ddlPickUpLocation.SelectedItem.Value
        End Get
    End Property

    Public ReadOnly Property ReturnLocation() As String
        Get
            Return ddlReturnLocation.SelectedItem.Value
        End Get
    End Property

    ' Change for Promo Code Drop Down
    Public Property MasterPackageXML() As XmlDocument
        Get
            Dim xmlMasterPkgXML As New XmlDocument
            Try
                xmlMasterPkgXML.LoadXml(Server.HtmlDecode(hdnMasterPackageXML.Value))
            Catch
                ' do nothing 
            End Try
            Return xmlMasterPkgXML
        End Get
        Set(ByVal value As XmlDocument)
            hdnMasterPackageXML.Value = Server.HtmlEncode(value.OuterXml)
        End Set
    End Property

    Public Property CachedPriceAndAvailDataRS As THL_PriceAndAvailDataRS
        Get
            If Session("PriceAndAvailDataRSXML") Is Nothing Then
                Return Nothing
            Else
                Return CType(Session("PriceAndAvailDataRSXML"), THL_PriceAndAvailDataRS)
            End If
        End Get
        Set(ByVal value As THL_PriceAndAvailDataRS)
            Session("PriceAndAvailDataRSXML") = value
        End Set
    End Property
    ' Change for Promo Code Drop Down

    'Private nAssetBaseBath As String
    'Public Property AssetBasePath() As String
    '    Get
    '        Return nAssetBaseBath
    '    End Get
    '    Set(ByVal value As String)
    '        nAssetBaseBath = value
    '    End Set
    'End Property

    

    Public ReadOnly Property PickUpDateTime() As DateTime
        Get
            ' Get Time
            Dim mPickUpTime As Date
            Dim iPickupDay As Integer
            If Not IsNothing(sPickupDay) Then
                If sPickupDay.Trim.Length > 0 Then
                    iPickupDay = CInt(sPickupDay)
                Else
                    iPickupDay = CInt(ddlPickupDay.SelectedItem.Value)
                End If
            Else
                iPickupDay = CInt(ddlPickupDay.SelectedItem.Value)
            End If

            If Not IsNothing(sPickupTime) Then
                If sPickupTime.Trim.Length = 0 Then
                    mPickUpTime = CType(ddlPickUpTime.SelectedItem.Text, DateTime)
                Else
                    mPickUpTime = CType(sPickupTime, DateTime)
                End If
            Else
                mPickUpTime = CType(ddlPickUpTime.SelectedItem.Text, DateTime)
            End If

            ' Get Month and Year. 
            Dim selectedMonthYear As String = ddlPickupDate.SelectedItem.Value
            ' Need to separate month and year into a string array
            Dim monthYear() As String = selectedMonthYear.Split(CType(",", Char))
            ' Create a DateTime object with the values (year, month, day, hour, minute, second)
            Dim mPickupDateTime As New DateTime(CInt(monthYear(1).ToString), CInt(monthYear(0).ToString), iPickupDay, mPickUpTime.Hour, mPickUpTime.Minute, 0)
            Return mPickupDateTime
        End Get
    End Property

    Public ReadOnly Property ReturnDateTime() As DateTime
        Get
            ' See PickUpDateTime() comments
            Dim mReturnTime As Date
            Dim iReturnDay As Integer
            If Not IsNothing(sReturnDay) Then
                If sReturnDay.Trim.Length > 0 Then
                    iReturnDay = CInt(sReturnDay)
                Else
                    iReturnDay = CInt(ddlReturnDay.SelectedItem.Value)
                End If
            Else
                iReturnDay = CInt(ddlReturnDay.SelectedItem.Value)
            End If

            If Not IsNothing(sReturnTime) Then
                If CStr(sReturnTime).Trim.Length = 0 Then
                    mReturnTime = CType(ddlReturnTime.SelectedItem.Text, DateTime)
                Else
                    mReturnTime = CType(sReturnTime, DateTime)
                End If
            Else
                mReturnTime = CType(ddlReturnTime.SelectedItem.Text, DateTime)
            End If

            Dim selectedMonthYear As String = ddlReturnDate.SelectedItem.Value
            Dim monthYear() As String = selectedMonthYear.Split(CType(",", Char))

            Dim mReturnDateTime As New DateTime(CInt(monthYear(1).ToString), CInt(monthYear(0).ToString), iReturnDay, mReturnTime.Hour, mReturnTime.Minute, 0)

            Return mReturnDateTime
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Show the progress bar whenever btnAvailability or  Modify booking is clicked
        Me.btnAvailability.Attributes.Add("OnClick", "javascript:BeginPageLoad();")
        Me.btnModifyBooking.Attributes.Add("OnClick2", "javascript:BeginPageLoad();")
        ' -- new Code for Client side processing ---
        Me.ddlCountry.Attributes.Add("OnChange", "javascript:ddlCountry_SelectedIndexChanged(this.value);")
        Me.ddlCategory.Attributes.Add("OnChange", "javascript:ddlCategory_SelectedIndexChanged(this.value);")
        Me.ddlBrand.Attributes.Add("OnChange", "javascript:ddlBrand_SelectedIndexChanged(this.value);")
        Me.ddlPickUpLocation.Attributes.Add("OnChange", "javascript:ddlPickUpLocation_SelectedIndexChanged(this.value);")
        Me.ddlReturnLocation.Attributes.Add("OnChange", "javascript:ddlReturnLocation_SelectedIndexChanged(this.value);")
        Me.ddlPickupDate.Attributes.Add("OnChange", "javascript:ddlPickupDate_SelectedIndexChanged(this.value);")
        Me.ddlReturnDate.Attributes.Add("OnChange", "javascript:ddlReturnDate_SelectedIndexChanged(this.value);")
        Me.ddlPickUpTime.Attributes.Add("OnChange", "javascript:ddlPickUpTime_SelectedIndexChanged(this.value);")
        Me.ddlReturnTime.Attributes.Add("OnChange", "javascript:ddlReturnTime_SelectedIndexChanged(this.value);")
        Me.ddlPickupDay.Attributes.Add("OnChange", "javascript:ddlPickupDay_SelectedIndexChanged(this.value);")
        Me.ddlReturnDay.Attributes.Add("OnChange", "javascript:ddlReturnDay_SelectedIndexChanged(this.value);")
        ' Change for Promo Code Drop Down
        Me.ddlAgentCode.Attributes.Add("OnChange", "javascript:ddlAgentCode_SelectedIndexChanged(this.value);")
        Me.cmbPromoCode.Attributes.Add("OnChange", "javascript:return cmbPromoCode_SelectedIndexChanged(this.value);")
        ' Change for Promo Code Drop Down
        ' -- new Code for Client side processing ---
        ' Clear the error label
        lblErrorOccurred.Text = ""

        sBrand = Request("hd_Brand")
        sCategory = Request("hd_Category")
        sCkoLocCode = Request("hd_CkoLocCode")
        sCkiLocCode = Request("hd_CkiLocCode")
        sPickupTime = Request("hd_PickupTime")
        sReturnTime = Request("hd_DropOffTime")
        sPickupDay = Request("hd_PickupDay")
        sReturnDay = Request("hd_ReturnDay")
        sPromoCode = IIf(Request("hd_PromoCode") Is Nothing, "", Request("hd_PromoCode")).ToString()

        LoadPromoCodeDropDown(sPromoCode)

       
        'Dim sPickLocCode As String = Request("LocationAndTime1_ddlPickUpLocation")%>"
        'Dim sDropLocCode As String = Request("LocationAndTime1_ddlReturnLocation")
        'Button1.Attributes.Add("onclick",
        '"javascript:alert('ALERT ALERT!!!')");
        'btnContinue.Attributes.Add("onclick", "validateSlot()")

    End Sub

    Public Sub InitLocationAndTime(Optional ByVal Bookingid As String = "", Optional ByVal BookingType As TransactionActionType = Nothing)
        Me.EnableControls(True)
        lblErrorOccurred.Text = ""
        ddlCategory.ClearSelection()
        Me.BookingType = BookingType
        IsQuoteValue = "false"
        If BookingType = TransactionActionType.Quote Then 'Or BookingType = TransactionActionType.Hold
            IsQuoteValue = "true"

        End If

        'IsQuote.Style.Add("display", "none")
        ddlCategory.Items.FindByValue("V2").Selected = True
        Try
            'Get all price and availability data to load control lists etc
            GetPriceAndAvailabilityData()
        Catch ex As Exception
            lblErrorOccurred.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try

        If lblErrorOccurred.Text = "" Then 'no errors getting data
            'populate control lists etc
            If IsNothing(PriceAndAvailDataRS.Errors) Then
                LoadAgentList()
                LoadCountryList()
                LoadYearandMonthList()
                LoadPickupDayList()
                LoadReturnDayList()
                LoadCategoryList(ddlCountry.SelectedItem.Value)
                LoadLocationLists(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value)
                LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
                LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)

                ' Change for Promo Code Drop Down
                LoadPromoCodeDropDown(sPromoCode)
                ' Change for Promo Code Drop Down
            Else
                lblErrorOccurred.Text = "There was an error loading this page"
            End If

            If Bookingid <> "" Then
                'this control is used to modify a booking
                SelectListOptions(Bookingid)
                PassedBookingId = Bookingid

                DisplayForUse(ThisControlIsUsedTo.ModifyABooking)
            Else
                'this control is used to get available vehicles
                Dim oPos() As SourceType
                oPos = New SourceType() {CType(Session("POS"), SourceType)}
                Dim DefAgent As String = oPos(0).RequestorID.ID
                ddlAgentCode.SelectedValue = DefAgent

                DisplayForUse(ThisControlIsUsedTo.GetVehicleAvailability)
                SetReturnDate(Today.Date.AddDays(7))
                SetPickUpDate(Today.Date)
            End If
        End If

    End Sub

    Private Sub DisplayForUse(ByVal controlUse As ThisControlIsUsedTo)
        If controlUse = ThisControlIsUsedTo.ModifyABooking Then
            'disable controls that can't be modified 
            ddlCountry.Enabled = False
            ddlCategory.Enabled = False
            ddlBrand.Enabled = False
            ddlAgentCode.Enabled = False
            ' txtPromocode.Enabled = False
            cmbPromoCode.Enabled = False

            btnAvailability.Style.Add("display", "none")
            If Me.BookingType = TransactionActionType.Quote Then ' Or Me.BookingType = TransactionActionType.Hold
                btnModifyBooking.Style.Add("display", "none")
                btnContinue.Style.Add("display", "visible")
                'ddlPickUpTime.Visible = True
                'lblPickUpTime.Visible = True
                ddlPickUpTime.Style.Add("display", "visible")
                lblPickUpTime.Style.Add("display", "visible")
            Else
                btnModifyBooking.Style.Add("display", "visible")
                btnContinue.Style.Add("display", "none")
                'ddlPickUpTime.Visible = False
                'lblPickUpTime.Visible = False
                ddlPickUpTime.Style.Add("display", "none")
                lblPickUpTime.Style.Add("display", "none")
            End If
            ddlSize.Enabled = False
            btnCancel.Visible = True
            lblNoNeedMessaeg.Style.Add("display", "none")
            slotsDetail.Visible = False
        Else
            ddlCountry.Enabled = True
            ddlCategory.Enabled = True
            ddlBrand.Enabled = True
            ddlAgentCode.Enabled = True
            ' txtPromocode.Enabled = True
            cmbPromoCode.Enabled = True
            btnAvailability.Style.Add("display", "visible")
            btnModifyBooking.Style.Add("display", "none")
            btnContinue.Style.Add("display", "none")
            btnCancel.Visible = False
            ddlSize.Enabled = True
        End If

    End Sub
    Private Sub BindTimeLists()
        Dim time() As String = {"07:00 a.m.", "08:00 a.m.", "09:00 a.m.", "10:00 a.m.", "11:00 a.m.", "12:00 p.m.", "13:00 p.m.", "14:00 p.m.", "15:00 p.m.", "4:00 p.m.", "5:00 p.m.", "6:00 p.m.", "7:00 p.m."}
        ddlPickUpTime.DataSource = time
        ddlReturnTime.DataSource = time
        ddlPickUpTime.DataBind()
        ddlReturnTime.DataBind()
    End Sub

    Private Function DateFormat(ByVal selectedDate As Date) As String
        Dim day As Integer = selectedDate.Day
        Dim Month As Integer = selectedDate.Month
        Dim year As Integer = selectedDate.Year
        Return day & "/" & Month & "/" & year
    End Function


    Private Sub GetPriceAndAvailabilityData()
        ' Get data from webservice to populate screen, should only need to do once 
        Dim XMLDom As New XmlDocument
        If IsNothing(Session("POS")) Then
            ' User not logged in or session expired, redirect to Access denied page
            Response.Redirect("AccessDenied.aspx")
        Else

            Dim RQ As New THL_PriceAndAvailDataRQ
            RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}

            'If CachedPriceAndAvailDataRS Is Nothing Then
            PriceAndAvailDataRS = Proxy.PriceAndAvailData(RQ)
            ' Store response in session for use later
            'CachedPriceAndAvailDataRS = PriceAndAvailDataRS
            'Else
            ' retrieve from session
            ' PriceAndAvailDataRS = CachedPriceAndAvailDataRS
            'End If

            XMLDom.LoadXml(General.SerializePriceAndAvailabilityRSObject(PriceAndAvailDataRS))

            BuildMasterPackageXML()

            Dim I, J, K As Integer
            Dim PickUpDate, DropOffDate As Date

            Try

                For I = 0 To XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes.Count - 1
                    For J = 0 To XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes(I).ChildNodes.Count - 1

                        If XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes(I).ChildNodes(J).Name <> "VehicleTypes" Then Continue For

                        For K = 0 To XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes(I).ChildNodes(J).ChildNodes.Count - 1
                            XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes(I).ChildNodes(J).ChildNodes(K).Attributes(2).Value = CDate(XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes(I).ChildNodes(J).ChildNodes(K).Attributes(2).Value).ToShortTimeString
                            XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes(I).ChildNodes(J).ChildNodes(K).Attributes(3).Value = CDate(XMLDom.DocumentElement.ChildNodes(1).ChildNodes(1).ChildNodes(I).ChildNodes(J).ChildNodes(K).Attributes(3).Value).ToShortTimeString
                        Next
                    Next
                Next

            Catch ex As Exception
                Console.WriteLine(ex.Message & vbNewLine & ex.StackTrace)
            End Try
            LocAndTimeXML.Text = Server.HtmlEncode(XMLDom.InnerXml)
            'LocAndTimeXML.Text = Server.HtmlEncode(General.SerializePriceAndAvailabilityRSObject(PriceAndAvailDataRS))
            'End If
            Me.PriceAndAvailabilityData = General.SerializePriceAndAvailabilityRSObject(PriceAndAvailDataRS)
        End If
    End Sub

    Private Sub LoadAgentList()
        'Loop through array of agents from PriceAndAvailDataRS response object
        Dim index As Integer = 0
        For index = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.Agents.Length - 1
            ' Agent code and Name is returned as AgentCode|AgentName, need to split into a string array
            Dim newListItem As New ListItem
            Dim delimStr As String = "|"
            Dim delimiter As Char() = delimStr.ToCharArray()
            Dim textAndValue() As String = PriceAndAvailDataRS.PriceAndAvailDataCore.Agents(index).ToString.Split(delimiter)

            If textAndValue.Length = 1 Then
                ' Take the first item as both the value and text 
                newListItem.Text = textAndValue(0).ToString ' Agent Name
                newListItem.Value = textAndValue(0).ToString    ' Agent Code
                ddlAgentCode.Items.Add(newListItem) ' Populate dropdownlist
            Else
                newListItem.Text = textAndValue(1).ToString ' Agent Name
                newListItem.Value = textAndValue(0).ToString    ' Agent Code
                ddlAgentCode.Items.Add(newListItem) ' Populate dropdownlist
            End If
        Next
    End Sub

    Private Sub LoadCategoryList(ByVal countryCode As String)
        ddlCategory.Items.Clear()

        Dim countryIndex As Integer = 0
        Dim vehicleIndex As Integer = 0
        Dim locationIndex As Integer = 0

        For countryIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations.Length - 1
            ' Country Code determines the vehicle types that can be displayed

            If PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).Code.ToUpper = countryCode.ToUpper Then
                ' Loop through and dislpaye vehicles for a particular country
                For vehicleIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes.Length - 1
                    Dim text As String
                    Dim value As String

                    text = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Type.ToString()
                    value = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Code

                    If (ddlCountry.SelectedItem.Value.ToLower = "au" Or ddlCountry.SelectedItem.Value.ToLower = "tas") AndAlso ddlBrand.SelectedItem.Text.ToLower = "maui" Then
                        ' Dont display 4wd car or 4wd motorhome if Austrailia and Maui selected
                        'If text.ToLower <> "motorhome 4wd" AndAlso text.ToLower <> "car 4wd" Then
                        newCategoryListItem(text, value)
                        'End If
                    ElseIf ddlCountry.SelectedItem.Value.ToLower = "nz" AndAlso ddlBrand.SelectedItem.Text.ToLower = "maui" Then
                    ' Don't display 4wd motorhome if NZ and Maui selected
                    If text.ToLower <> "motorhome 4wd" Then
                        newCategoryListItem(text, value)
                    End If
                    Else
                    newCategoryListItem(text, value)
                    End If
                Next
            End If
        Next

    End Sub

    Private Sub newCategoryListItem(ByVal Text As String, ByVal Value As String)
        Dim listitem As New ListItem(Text, Value)
        ddlCategory.Items.Add(listitem)
    End Sub

    Private Sub LoadYearandMonthList()
        ddlPickupDate.Items.Clear()
        ddlReturnDate.Items.Clear()

        Dim months() As String = New String() {"January", "February", "March", "April", "May", _
                                                "June", "July", "August", "September", "October", _
                                                "November", "December"}
        Dim yearsToAdd As Integer
        Dim month As String
        Dim year As Integer
        Dim iMonth As Integer
        Dim iTillYear As Integer = CInt(System.Configuration.ConfigurationSettings.AppSettings("Year"))
        Dim sTillMonth As String = CStr(System.Configuration.ConfigurationSettings.AppSettings("Month"))

        ' Load month and year dropdown list 2 years ahead
        For yearsToAdd = 0 To iTillYear
            'Start with January
            iMonth = 1
            year = DateTime.Now.AddYears(yearsToAdd).Year 'Current year
            ' Loop through each month in the month array list
            For Each month In months
                ' Create a new list item for both Pickup and Return Date dropdownlists
                ' In the format (January, 2003), (1, 2003) as text and value respectively
                Dim newPickupListItem As New ListItem(month & ", " & year, iMonth & "," & year)
                Dim newReturnListItem As New ListItem(month & ", " & year, iMonth & "," & year)
                If Not (DateTime.Now.Year = year And iMonth < (DateTime.Now.Month)) Then
                    ddlPickupDate.Items.Add(newPickupListItem)
                    ddlReturnDate.Items.Add(newReturnListItem)
                End If
                'Now do for the next month in the current year i.e February
                iMonth = iMonth + 1
                If yearsToAdd = iTillYear AndAlso month = sTillMonth Then
                    ' Only want to display dates up until April of following year (from today)
                    Exit For
                End If
            Next
        Next
    End Sub


    Private Function GetDayList(ByVal month As Integer, ByVal year As Integer) As ArrayList
        ' Get the number of days in the month for the specifed date
        Dim arrDays As New ArrayList
        Dim day As Integer
        For day = 1 To Today.DaysInMonth(year, month)
            arrDays.Add(day)
        Next
        Return arrDays
    End Function

    Private Sub LoadCountryList()
        ddlCountry.Items.Clear()
        Dim index As Integer = 0
        For index = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations.Length - 1

            ' We need to loop through all the PickupAndDropOffLocations to get all the country codes
            ' But we don't want to add mulitple same country codes to the dropdown list.

            Dim currentListItem As New ListItem
            currentListItem.Text = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(index).Name.ToString
            currentListItem.Value = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(index).Code.ToString


            ' Don't want to add mulitple country codes 
            If Not ddlCountry.Items.Contains(currentListItem) Then
                ddlCountry.Items.Add(currentListItem)
            End If
        Next

        If Not IsNothing(ddlCountry.Items(0)) Then
            ' Select the first item in the dropdown list
            ddlCountry.Items(0).Selected = True
        End If
    End Sub

    Private Sub LoadLocationLists(ByVal countryCode As String, ByVal vehicleCode As String)
        ' This method will populate the location dropdown lists based on country code
        ' and vehicle code

        ddlPickUpLocation.Items.Clear()
        ddlReturnLocation.Items.Clear()
        Dim countryIndex As Integer = 0
        Dim vehicleIndex As Integer = 0
        Dim locationIndex As Integer = 0

        ' Loop through all countries to get the specified one.
        For countryIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations.Length - 1
            ' Country Code determines the vehicle types that can be displayed
            ' Vehicle types determines the locations that are shown

            If PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).Code.ToString.ToUpper = countryCode.ToUpper Then
                ' Loop through vehicles and get the one that matches the specified vehicle code for the country code
                For vehicleIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes.Length - 1
                    ' If the vehicl code matches, get the locations for that vehicle
                    If PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Code.ToLower = vehicleCode.ToLower Then
                        ' Loop through and add all locations to pickup and dropoff dropwdowns according to country>category>location
                        For locationIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location.Length - 1
                            Dim pickUpLocationItem As New ListItem
                            pickUpLocationItem.Text = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location(locationIndex).Name
                            ' Location name
                            pickUpLocationItem.Value = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location(locationIndex).Code                      ' Location code

                            Dim returnLocationItem As New ListItem
                            returnLocationItem.Text = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location(locationIndex).Name
                            ' Location name
                            returnLocationItem.Value = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location(locationIndex).Code
                            ' Location code

                            ddlPickUpLocation.Items.Add(pickUpLocationItem)
                            ddlReturnLocation.Items.Add(returnLocationItem)
                        Next
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub LoadTimeList(ByVal countryCode As String, ByVal vehicleCode As String, ByVal locationCode As String, ByVal bPickUpTime As Boolean)
        'Populate either pick up or return time list based on the chosen location
        If bPickUpTime = True Then
            ' We are loading the pickuptime dropdown list
            ddlPickUpTime.Items.Clear()
        Else
            ' We are loading the return dropdown list
            ddlReturnTime.Items.Clear()
        End If

        Dim index As Integer = 0
        Dim tPickupTime As DateTime
        Dim tReturnTime As DateTime
        Dim timeDifference As TimeSpan


        Dim countryIndex As Integer = 0
        Dim vehicleIndex As Integer = 0
        Dim locationIndex As Integer = 0

        ' Loop through all countries to get the specified one
        For countryIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations.Length - 1
            ' Country Code determines the vehicle types that can be displayed
            ' Vehicle types determines the locations that are shown

            If PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).Code.ToString.ToUpper = countryCode.ToUpper Then
                ' Loop through vehicles and get the one that matches the specified vehicle code for the country code
                For vehicleIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes.Length - 1
                    ' If the vehicle code matches, get the locations for that vehicle
                    If PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Code.ToLower = vehicleCode.ToLower Then
                        ' Loop through all locations to find the specifeied one
                        For locationIndex = 0 To PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location.Length - 1
                            ' And load the correct time for the location
                            If PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location(locationIndex).Code.ToLower = locationCode.ToLower Then
                                tPickupTime = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location(locationIndex).PickUpTime
                                tReturnTime = PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations(countryIndex).VehicleTypes(vehicleIndex).Location(locationIndex).ReturnTime
                            End If
                        Next
                    End If
                Next
            End If
        Next


        ' Get time difference between dropoff and pickup
        timeDifference = tReturnTime.Subtract(tPickupTime)

        If timeDifference.TotalHours < 0 Then
            ' Dropoff time is after midnight
            Dim tHours As New TimeSpan(24, 0, 0)
            ' So add 24 hrs
            timeDifference = timeDifference.Add(tHours)
        End If

        Dim arrTime As New ArrayList
        Dim hours As Integer

        ' Populate datasource for dropdown list
        For hours = 0 To CInt(timeDifference.TotalHours) * 60 Step 15
            arrTime.Add(tPickupTime.AddMinutes(hours).ToShortTimeString)
        Next

        'If bPickUpTime = True Then
        '    ' We are loading the pickuptime dropdown list
        '    ddlPickUpTime.DataSource = arrTime
        '    ddlPickUpTime.DataBind()

        'Else
        '    ' We are loading the return dropdown list
        '    ddlReturnTime.DataSource = arrTime
        '    ddlReturnTime.DataBind()

        'End If

        If bPickUpTime = True Then
            ' We are loading the pickuptime dropdown list
            ddlPickUpTime.DataSource = arrTime
            ddlPickUpTime.DataBind()
            Dim bUseDefault As Boolean = False
            For Each oItem As ListItem In ddlPickUpTime.Items
                If oItem.Value = ConfigurationSettings.AppSettings("DefaultPickUpTime") Then
                    bUseDefault = True
                    Exit For
                End If
            Next

            If bUseDefault Then
                ddlPickUpTime.SelectedValue = ConfigurationSettings.AppSettings("DefaultPickUpTime")
            End If
        Else
            ' We are loading the return dropdown list
            ddlReturnTime.DataSource = arrTime
            ddlReturnTime.DataBind()

            Dim bUseDefault As Boolean = False
            For Each oItem As ListItem In ddlReturnTime.Items
                If oItem.Value = ConfigurationSettings.AppSettings("DefaultPickUpTime") Then
                    bUseDefault = True
                    Exit For
                End If
            Next

            If bUseDefault Then
                ddlReturnTime.SelectedValue = ConfigurationSettings.AppSettings("DefaultReturnTime")
            End If
        End If
    End Sub

    Private Sub DeserializePriceAndAvailibilityRS()
        ' Create the response object out of the response string stored in session
        PriceAndAvailDataRS = General.DeserializePriceAndAvailabilityRSObject(Me.PriceAndAvailabilityData, PriceAndAvailDataRS)
    End Sub

    Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        ' Need to change locations based on country and times based on locations
        ' Make the RS object available
        DeserializePriceAndAvailibilityRS()
        LoadCategoryList(ddlCountry.SelectedItem.Value)
        LoadLocationLists(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value)

        ' And since the times change for different locations load the time lists based on the location
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)

    End Sub

    Private Sub ddlPickUpLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        DeserializePriceAndAvailibilityRS()
        ' Change pickup time list based on location
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
    End Sub

    Private Sub ddlReturnLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        DeserializePriceAndAvailibilityRS()
        ' Change return time list based on location
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)
    End Sub

    Private Sub btnAvailability_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAvailability.Click
        GetAvailability()
    End Sub

    Public Sub GetAvailability()
        ' Get the availability of the current selection
        ' Validate form to check date ranges are correct etc

        If FormValidated() = True Then

            Dim pickUpTime As Date = CType(ddlPickUpTime.SelectedItem.Text, DateTime)
            Dim returnTime As Date = CType(ddlReturnTime.SelectedItem.Text, DateTime)

            If lblErrorOccurred.Text = "" Then
                'Fire an event to let the Availability.ascx control retrun all available vehicles
                ' Send Category, Brand, pickup location, retrun location, pickuptime, return time, promotion code, agent code
                If sBrand.Trim.Length = 0 Then sBrand = ddlBrand.SelectedItem.Value
                If sCategory.Trim.Length = 0 Then sCategory = ddlCategory.SelectedItem.Value
                If sCkoLocCode.Trim.Length = 0 Then sCkoLocCode = ddlPickUpLocation.SelectedItem.Value
                If sCkiLocCode.Trim.Length = 0 Then sCkiLocCode = ddlReturnLocation.SelectedItem.Value
                If sPickupTime.Trim.Length = 0 Then sPickupTime = ddlPickUpTime.SelectedItem.Value
                If sReturnTime.Trim.Length = 0 Then sReturnTime = ddlReturnTime.SelectedItem.Value
                'If sSize.Trim.Length = 0 Then sSize = ddlSize.SelectedItem.Value
                'RaiseEvent GetAvailabilityClicked(sCategory, sBrand, sCkoLocCode, sCkiLocCode, Me.PickUpDateTime, Me.ReturnDateTime, txtPromocode.Text, ddlAgentCode.SelectedItem.Value, ddlSize.SelectedItem.Value)
                RaiseEvent GetAvailabilityClicked(sCategory, sBrand, sCkoLocCode, sCkiLocCode, Me.PickUpDateTime, Me.ReturnDateTime, sPromoCode, ddlAgentCode.SelectedItem.Value, ddlSize.SelectedItem.Value, Me.txtVehicleCode.Text)
            End If
        End If
    End Sub

    Private Sub btnModifyBooking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModifyBooking.Click

        If FormValidated() = True Then
            If lblErrorOccurred.Text = "" Then
                DisableAll = False
                slotsPanel = GetSlots()
                If String.IsNullOrEmpty(slotsPanel) Then
                    DisableAll = True
                    'ddlPickUpTime.Visible = True
                    'RaiseEvent ModifyBookingclicked()
                Else
                    slotsDetail.Visible = True
                    'btnContinue.Style.Add("display", "visible")
                    ''btnModifyBooking.Visible = False
                    'btnModifyBooking.Style.Add("display", "none")
                End If
                'btnContinue.Style.Add("display", "visible")
                'btnModifyBooking.Visible = False
                btnModifyBooking.Style.Add("display", "none")
                'RaiseEvent ModifyBookingclicked()
            End If
        End If
    End Sub

    Private Sub SelectListOptions(ByVal BookingId As String)
        Try
            ' Get the booking information for this booking and load controls with correct info
            GetAndSetBookingData(BookingId)
        Catch ex As Exception
            lblErrorOccurred.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try
    End Sub

    Private Function GetAndSetBookingData(ByVal bookingid As String) As Boolean

        Dim success As Boolean = False
        Dim RQ As New OTA_VehRetResRQ
        Dim RS As New OTA_VehRetResRS

        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        RQ.EchoToken = CStr(Session("SessionId"))
        Dim OrqCoreType As New VehicleRetrieveResRQCoreType
        RQ.VehRetResRQCore = OrqCoreType

        Dim OuniqType As New UniqueID_Type
        OuniqType.Type = CStr(22)
        OuniqType.ID = bookingid
        RQ.VehRetResRQCore.UniqueID = OuniqType
        Dim VehRetCoreType As New VehicleRetrieveResRQCoreType

        Dim persontype As New PersonNameType
        persontype.Surname = "NA"
        RQ.VehRetResRQCore.PersonName = persontype

        Dim tpa As New VehicleResAdditionalInfoType
        tpa.Agent = "NA"
        tpa.AgentRef = "NA"
        tpa.Package = "NA" 'not used but required as part of the request
        tpa.Status = "NA" 'not used but required as part of the request

        RQ.VehRetResRQCore.TPA_Extensions = tpa

        ''General.LogDebugNotes("Start LocationAndTime: " & General.SerializeVehRetResRQObject(RQ))
        RS = Proxy.VehRetRes(RQ)
        ''General.LogDebugNotes("End LocationAndTime: " & General.SerializeVehRetResRSObject(RS) & vbCrLf & vbCrLf)


        StatusBarText = RS.Warnings(0).Tag
        UIScripts.StatusMsg(Me.Page, RS.Warnings(0).Tag)

        If IsNothing(RS.Errors) Then

            success = True

            Dim pickupLocation As String
            Dim returnLocation As String

            Dim pickupDateTime As DateTime
            Dim returnDatetime As DateTime

            Dim pickUpTime As String
            Dim returnTime As String

            Dim pickupDate As String
            Dim returnDate As String
            Dim pickUpCountryCode As String
            Dim returnCountryCode As String

            ' Retrieve location and times for the current booking
            pickupLocation = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpLocation.LocationCode
            returnLocation = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnLocation.LocationCode
            pickUpCountryCode = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpLocation.Value
            returnCountryCode = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnLocation.Value

            pickupDateTime = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpDateTime
            returnDatetime = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnDateTime

            pickUpTime = pickupDateTime.ToShortTimeString
            returnTime = returnDatetime.ToShortTimeString

            Dim category As String
            Dim brand As String

            If Not IsNothing(RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vendor) Then
                ' Get brand for current booking
                brand = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vendor.CompanyShortName.ToString

                Dim brandListItem As New ListItem(brand)
                ' And select in brand dropdown list
                If ddlBrand.Items.Contains(brandListItem) Then

                    ddlBrand.ClearSelection()
                    ddlBrand.Items.FindByText(brand).Selected = True

                End If
            End If

            'Get category for current booking
            Try
                ''rev:mia - Oct.16, 2012 - not sure if all vehicle needs to have a category coz booking for Alpha returned empty category
                category = RS.VehRetResRSCore.VehReservation.VehSegmentCore.Vehicle.VehType.VehicleCategory.ToString
            Catch ex As Exception
            End Try

            ' And select in category dropdownn list
            Dim CategoryListItem As New ListItem(category)
            'If ddlCategory.Items.Contains(CategoryListItem) Then
            'ddlCategory.ClearSelection()
            'ddlCategory.Items.FindByText(category).Selected = True
            'End If

            'If ddlCategory.Items.Contains(CategoryListItem) Then
            '    ddlCategory.ClearSelection()
            '    ddlCategory.Items.FindByValue(category).Selected = True
            'End If
            ' Select country code in country dropdownlist
            ddlCountry.ClearSelection()
            ddlCountry.Items.FindByValue(pickUpCountryCode).Selected = True

            ' And  load correct location lists based on country code
            LoadCategoryList(ddlCountry.SelectedItem.Value)

            If Not ddlCategory.Items.FindByValue(category) Is Nothing Then
                ddlCategory.ClearSelection()
                ddlCategory.Items.FindByValue(category).Selected = True
            End If

            LoadLocationLists(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value)

            ' And consequently the time lists based on location
            LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
            LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)

            ' Select pickup location in pick up location list
            ddlPickUpLocation.ClearSelection()
            ddlPickUpLocation.Items.FindByValue(pickupLocation).Selected = True

            ' Select return location in return location list
            ddlReturnLocation.ClearSelection()
            ddlReturnLocation.Items.FindByValue(returnLocation).Selected = True

            ' Select pickup and return dates and make visible to the user
            SetPickUpDate(pickupDateTime.Date)
            SetReturnDate(returnDatetime.Date)


            ' Select pickup time in pick up time list
            ddlPickUpTime.ClearSelection()
            Dim pickupTimeListItem As New ListItem(pickUpTime)
            If ddlReturnTime.Items.Contains(pickupTimeListItem) Then
                ddlPickUpTime.Items.FindByText(pickUpTime).Selected = True
            End If

            ' Select return time in return location list
            ddlReturnTime.ClearSelection()
            Dim returnTimeListItem As New ListItem(returnTime)

            If ddlReturnTime.Items.Contains(returnTimeListItem) Then
                ddlReturnTime.Items.FindByText(returnTime).Selected = True
            End If

            Dim vehStatus As String = RS.VehRetResRSCore.VehResSummaries(0).TPA_Extensions.Status
        Else
            ' An error occurred display it
            If RS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblErrorOccurred.Text &= RS.Errors(0).Tag.ToString
            success = False
        End If

        Return success

    End Function

    Private Function FormValidated() As Boolean
        Dim success As Boolean = True

        ' Validate selected dates
        If Me.PickUpDateTime.Date < Today.Date Then
            lblErrorOccurred.Text = "Please select a pick up date in the future" & "<br>"
            success = False

        ElseIf Me.ReturnDateTime.Date < Me.PickUpDateTime.Date Then
            lblErrorOccurred.Text &= "Return date must be later than pick up date"
            success = False
        End If

        If Me.PickUpDateTime.Date = Me.ReturnDateTime.Date AndAlso CType(ddlPickUpTime.SelectedItem.Text, DateTime) >= CType(ddlReturnTime.SelectedItem.Text, DateTime) Then
            lblErrorOccurred.Text &= "Return time must be later than the PickUp time"
            success = False
        End If
        Return success
    End Function

    Public Sub SetupScreen(ByVal countryCode As String, ByVal category As String, ByVal brand As String)

        ' Select country in country dropdown list
        Dim countryName As String
        Select Case countryCode.ToLower
            Case "nz"
                countryName = "New Zealand"
            Case "tas"
                countryName = "Tasmania-Australia"
            Case "au"
                countryName = "Australia"
        End Select

        Dim CountryListItem As New ListItem(countryName, countryCode.ToUpper)
        If ddlCountry.Items.Contains(CountryListItem) Then
            ddlCountry.ClearSelection()
            ddlCountry.Items.FindByValue(countryCode.ToUpper).Selected = True
            ' Need to load correct location lists based on country code
            LoadCategoryList(ddlCountry.SelectedItem.Value)
            LoadLocationLists(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value)
            ' And consequently the time lists based on Location
            LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
            LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)
        End If

        If brand.ToLower = "maui" Then
            brand = "Maui"
        End If
        If brand.ToLower = "britz" Then
            brand = "Britz"
        End If
        ' Select brand in dropdown list
        Dim brandListItem As New ListItem(brand)
        If ddlBrand.Items.Contains(brandListItem) Then
            ddlBrand.ClearSelection()
            ddlBrand.Items.FindByText(brand).Selected = True
        End If

        Dim fullNameCategory As String
        Select Case category.ToLower
            Case "m2"
                fullNameCategory = "Motorhome 2wd"
            Case "m4'"
                fullNameCategory = "Motorhome 4wd"
            Case "c2"
                fullNameCategory = "Car 2wd"
            Case "c4"
                fullNameCategory = "Car 4wd"
        End Select

        ' Select category in category dropdownlist
        Dim CategoryListItem As New ListItem(fullNameCategory, category.ToUpper)
        If ddlCategory.Items.Contains(CategoryListItem) Then
            ddlCategory.ClearSelection()
            ddlCategory.Items.FindByValue(category.ToUpper).Selected = True
        End If

        ' Select default pickup location in pick up location list
        If countryCode.ToLower = "nz" Then
            Dim PickUpLocationListItem As New ListItem("AUCKLAND", "AKL")

            If ddlPickUpLocation.Items.Contains(PickUpLocationListItem) Then
                ddlPickUpLocation.ClearSelection()
                ddlPickUpLocation.Items.FindByValue("AKL").Selected = True
            End If

            Dim ReturnLocationListItem As New ListItem("CHRISTCHURCH", "CCH")

            If ddlPickUpLocation.Items.Contains(ReturnLocationListItem) Then
                ' Select return location in return location list
                ddlReturnLocation.ClearSelection()
                ddlReturnLocation.Items.FindByValue("CCH").Selected = True
            End If

        Else
            ' Select default  au codes
            Dim PickUpLocationListItem As New ListItem("ADELAIDE", "ADL")

            If ddlPickUpLocation.Items.Contains(PickUpLocationListItem) Then
                ddlPickUpLocation.ClearSelection()
                ddlPickUpLocation.Items.FindByValue("ADL").Selected = True
            End If

            Dim ReturnLocationListItem As New ListItem("ADELAIDE", "ADL")

            If ddlPickUpLocation.Items.Contains(ReturnLocationListItem) Then
                'select return location in return location list
                ddlReturnLocation.ClearSelection()
                ddlReturnLocation.Items.FindByValue("ADL").Selected = True
            End If
        End If
        ' Load timelists based on selcetd location

        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)

    End Sub

    Private Sub SetPickUpDate(ByVal ddlDate As Date)
        ' Set dropdown pickup date based on specified date

        ddlPickupDate.ClearSelection()
        ddlPickupDate.Items.FindByValue(CStr(ddlDate.Month) & "," & CStr(ddlDate.Year)).Selected = True

        ' And load pickup day  list again to get the correct number of days for the selected monthe and year
        LoadPickupDayList()
        ddlPickupDay.ClearSelection()
        ddlPickupDay.Items.FindByValue(CStr(ddlDate.Day)).Selected = True
    End Sub

    Private Sub SetReturnDate(ByVal ddlDate As Date)
        ' Set dropdown return date based on specified date

        ddlReturnDate.ClearSelection()
        ddlReturnDate.Items.FindByValue(CStr(ddlDate.Month) & "," & CStr(ddlDate.Year)).Selected = True

        ' And load return day  list again to get the correct number of days for the selected monthe and year
        LoadReturnDayList()
        ddlReturnDay.ClearSelection()
        ddlReturnDay.Items.FindByValue(CStr(ddlDate.Day)).Selected = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ' Let whoever wants to know that the cancel button has been clicked
        btnContinue.Style.Add("display", "visible")
        IsQuoteValue = ""
        IsQuote.Value = ""

        RaiseEvent CancelClicked()
    End Sub

    Private Sub ddlBrand_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        'Load category list based on brand
        DeserializePriceAndAvailibilityRS()
        LoadCategoryList(ddlCountry.SelectedItem.Value)
        LoadLocationLists(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value)
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)

    End Sub

    Public Sub EnableControls(ByVal status As Boolean)
        ddlPickupDay.Enabled = status
        ddlPickupDate.Enabled = status

        ddlReturnDay.Enabled = status
        ddlReturnDate.Enabled = status

        ddlPickUpLocation.Enabled = status
        ddlReturnLocation.Enabled = status

        ddlPickUpTime.Enabled = status
        ddlReturnTime.Enabled = status
        ddlAgentCode.Enabled = status
        ' txtPromocode.Enabled = status
        cmbPromoCode.Enabled = status
        btnModifyBooking.Enabled = status
    End Sub

    Private Sub ddlPickupMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' Get currently selected day so that it can be reselected once new day list is bound
        Dim currentDayItem As ListItem = ddlPickupDay.SelectedItem
        ' Load day list based on selected month
        LoadPickupDayList()
        ' And select
        If ddlPickupDay.Items.Contains(currentDayItem) Then
            ddlPickupDay.ClearSelection()
            ddlPickupDay.Items.FindByText(currentDayItem.Text).Selected = True
        End If
    End Sub


    Private Sub LoadPickupDayList()
        ' This method loads the pickupDay dropdown list
        ddlPickupDay.Items.Clear()
        Dim selectedMonthYear As String = ddlPickupDate.SelectedItem.Value
        Dim monthYear() As String = selectedMonthYear.Split(CType(",", Char))
        ' monthYear(0).ToString = "1" etc =january
        ' monthYear(1).ToString ="03" etc = year
        ddlPickupDay.DataSource = GetDayList(CInt(monthYear(0).ToString), CInt(monthYear(1).ToString))
        ddlPickupDay.DataBind()
    End Sub

    Private Sub LoadReturnDayList()
        ' This method loads the returnDay dropdown list

        ddlReturnDay.Items.Clear()
        Dim selectedMonthYear As String = ddlReturnDate.SelectedItem.Value
        Dim monthYear() As String = selectedMonthYear.Split(CType(",", Char))
        'monthYear(0).ToString = "1" etc =month
        'monthYear(1).ToString ="03" etc = year
        ddlReturnDay.DataSource = GetDayList(CInt(monthYear(0).ToString), CInt(monthYear(1).ToString))
        ddlReturnDay.DataBind()
    End Sub


    Private Sub ddlPickupDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPickupDate.SelectedIndexChanged
        'Load correct number of days for selected month
        ' Need to do this to account for leap years (february) and different number of days in each month
        Dim day As String = ddlPickupDay.SelectedItem.Value
        LoadPickupDayList()

        ' Reselect previosuly selectd day if exist in current month
        ddlPickupDay.ClearSelection()
        If IsNothing(ddlPickupDay.Items.FindByValue(day)) Then
        Else
            ddlPickupDay.Items.FindByValue(day).Selected = True
        End If


    End Sub

    Private Sub ddlReturnDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlReturnDate.SelectedIndexChanged
        'Load correct number of days for selected month
        Dim day As String = ddlReturnDay.SelectedItem.Value
        LoadReturnDayList()
        ddlReturnDay.ClearSelection()

        ' Reselect previosuly selectd day if exist in current month
        If IsNothing(ddlReturnDay.Items.FindByValue(day)) Then

        Else
            ddlReturnDay.Items.FindByValue(day).Selected = True
        End If

    End Sub

    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        ' Need to load location lists based on selected country and selecetd vehicle
        DeserializePriceAndAvailibilityRS()
        LoadLocationLists(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value)
        ' Then load time lists based on locations
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlPickUpLocation.SelectedItem.Value, True)
        LoadTimeList(ddlCountry.SelectedItem.Value, ddlCategory.SelectedItem.Value, ddlReturnLocation.SelectedItem.Value, False)
    End Sub

    ' Change for Promo Code Drop Down
    Sub LoadPromoCodeDropDown(ByVal SelectedValue As String)
        Dim sCountryCode, sAgentCode, sBrandName, sVehicleCategoryCode As String
        sCountryCode = ddlCountry.SelectedValue
        sAgentCode = ddlAgentCode.SelectedValue
        sBrandName = ddlBrand.SelectedValue
        sVehicleCategoryCode = ddlCategory.SelectedValue

        If (sVehicleCategoryCode = "V2" OrElse sVehicleCategoryCode = "V4") Then
            sVehicleCategoryCode = "AV"
        Else
            sVehicleCategoryCode = "AC"
        End If

        Dim xnlPackages As XmlNodeList
        xnlPackages = MasterPackageXML.SelectNodes("//Data/Country[@Code='" + sCountryCode + "']/Agent[@Code='" + sAgentCode + "']/Brand[@Name='" + sBrandName + "']/VehicleClass[@Code='" + sVehicleCategoryCode + "']/Package")

        cmbPromoCode.Items.Clear()
        cmbPromoCode.Items.Add(New ListItem("", ""))

        cmbPromoCode.Items(0).Selected = SelectedValue.Trim().Equals("")


        If xnlPackages IsNot Nothing AndAlso xnlPackages.Count > 0 Then
            For Each xmlPackage As XmlNode In xnlPackages
                Dim liNewListItem As New ListItem(xmlPackage.Attributes("Name").Value.Trim(), xmlPackage.Attributes("Code").Value.Trim())
                liNewListItem.Selected = SelectedValue.Trim().Equals(xmlPackage.Attributes("Code").Value.Trim())
                cmbPromoCode.Items.Add(liNewListItem)
            Next
        End If

    End Sub

    Private Sub ddlAgentCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAgentCode.SelectedIndexChanged
        CType(Session("POS"), SourceType).RequestorID.ID = ddlAgentCode.SelectedValue
        GetPriceAndAvailabilityData()
    End Sub

    Sub BuildMasterPackageXML()

        Dim sbMasterPackageXMLString As New System.Text.StringBuilder

        With sbMasterPackageXMLString
            .Append("<Data>")
            For Each oCountry As CountryType In PriceAndAvailDataRS.PriceAndAvailDataCore.PickupAndDropOffLocations
                .Append("<Country Code='" & oCountry.Code & "'>")

                For Each oBrand As BrandsType In oCountry.Brands

                    .Append("<Agent Code='" & oBrand.AgnCode & "'>")
                    .Append("<Brand Code='" & oBrand.Code & "' Name='" & GetBrandName(oBrand.Code) & "'>")

                    For Each oPackage As PackageType In oBrand.Package

                        .Append("<VehicleClass Code='" & oPackage.VehType & "'>")

                        ' good line below
                        '.Append("<Package Code='" & oPackage.Code & "' Name='" & Server.HtmlEncode(oPackage.Name) & "' IsInc='" & oPackage.IsInc & "' />")
                        ' experimental line below
                        .Append("<Package Code='" & oPackage.Code _
                                      & "' Name='" & Server.HtmlEncode(oPackage.Name) _
                                      & "' IsInc='" & oPackage.IsInc _
                                      & "' TravelFrom='" & oPackage.TravelFrom _
                                      & "' TravelTo='" & oPackage.TravelTo _
                                      & "' IsSpecial='" _
                                      & oPackage.IsSpecial _
                                & "' />")

                        .Append("</VehicleClass>")

                    Next

                    .Append("</Brand>")
                    .Append("</Agent>")

                Next

                .Append("</Country>")
            Next
            .Append("</Data>")
        End With

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml(sbMasterPackageXMLString.ToString())
        MasterPackageXML = xmlTemp

        

    End Sub

    Function GetBrandName(ByVal BrandCode As String) As String
        Select Case BrandCode
            Case "B"
                Return "Britz"
            Case "P"
                Return "Backpacker"
            Case "M"
                Return "Maui"
            Case "X"
                Return "Explore More"
            Case Else
                Return ""
        End Select
    End Function

    ' Change for Promo Code Drop Down

    Private Function GetSlots() As String
        'StringBuilder sb = new StringBuilder();
        ' //bool isDevEnv = bool.Parse(ConfigurationManager.AppSettings["isDevEnv"] == null ? "false" : ConfigurationManager.AppSettings["isDevEnv"]);
        ' //string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
        ' ////Use Rest compatible instead of SOAP
        ' //string slotData = string.Empty;
        ' //if (string.IsNullOrEmpty(bookingId))

        ' //    slotData = GetStringForURL(auroraBaseURL + "/GetAvailableSlot?sRntId=&sAvpId=" + avpID + "&sBpdId=");//TODO: consider loading an XML structure once service sealed
        ' //else
        ' //    slotData = GetStringForURL(auroraBaseURL + "/GetAvailableSlot?sRntId=" + bookingId + "&sAvpId=&sBpdId=");//TODO: consider loading an XML structure once service sealed

        ' //string quotePackageData = GetStringForURL(auroraBaseURL + "/GetInsuranceAndExtraHireItemsForRentalQuote?BookingReference=" + rentalNumber + "&ReferenceId=" + rentalNumber);
        ' //--
        ' string otaBase = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraOTABase"];
        ' Aurora_OTA otaProxy = new Aurora_OTA(otaBase);

        ' var res = otaProxy.GetAvailableSlot("", avpID, "", sNewPickupDate,sNewPickupLocation);
        ' string slotData = res.OuterXml;
        ' string retString = "Success";
        Dim sb As New StringBuilder()
        Dim otaBase As String = System.Web.Configuration.WebConfigurationManager.AppSettings("AuroraOTABase")
        Dim otaProxy As New Aurora_OTAV3(otaBase)

        Dim bookingId As String = PassedBookingId
        'Dim retData As XmlNode = otaProxy.GetAvailableSlot(bookingId.Replace("/", "-"), String.Empty, String.Empty, String.Empty, String.Empty)
        Dim retData As XmlNode = otaProxy.GetAvailableSlot(bookingId.Replace("/", "-"), String.Empty, String.Empty, False, False, String.Empty, PickUpDateTime.ToString(), PickUpLocation, String.Empty, String.Empty, String.Empty)

        Dim slotData As String
        If retData Is Nothing Then
            'lblPickUpTime.Visible = True
            'ddlPickUpTime.Visible = True
            ddlPickUpTime.Style.Add("display", "visible")
            lblPickUpTime.Style.Add("display", "visible")
            lblNoNeedMessaeg.Style.Add("display", "none")
            slotsDetail.Visible = False
            btnContinue.Style.Add("display", "visible")
            Return String.Empty
        Else
            ddlPickUpTime.Style.Add("display", "none")
            lblPickUpTime.Style.Add("display", "none")
            lblNoNeedMessaeg.Style.Add("display", "none")
            slotsDetail.Visible = True
            btnContinue.Style.Add("display", "visible")
            slotData = retData.OuterXml
        End If

        If (slotData IsNot Nothing AndAlso slotData <> String.Empty) AndAlso slotData.IndexOf("AvailableSlots") >= 0 Then

            Dim slotXml As New XmlDocument()
            slotXml.LoadXml(slotData)
            'slotXml.GetXElement();
            Dim result As System.Collections.Generic.List(Of XElement) = (From c In slotXml.GetXElement().Descendants("Slot") Select c).ToList()
            Dim rowCount As Integer = result.Count()
            'var widthPercentage = Math.Round(Decimal.Parse(100.ToString()) / rowCount, 0);
            Dim widthPercentage As Integer = 100 \ rowCount
            Dim fontSize As String = "16px"
            If rowCount > 6 Then
                fontSize = "14px"
            End If
            For Each slot As XElement In result
                Dim slotDesc As String = slot.Element("SlotDesc").Value
                Dim slotId As String = slot.Element("SlotId").Value
                Dim allocatedNumbers As String = If(slot.Element("AllocatedNumbers") Is Nothing, "0", slot.Element("AllocatedNumbers").Value)
                Dim bookedNumbers As String = If(slot.Element("Bookednumbers") Is Nothing, "0", slot.Element("Bookednumbers").Value)
                Dim availableNumbers As String = If(slot.Element("AvailableNumbers") Is Nothing, "0", slot.Element("AvailableNumbers").Value)

                Dim isSlotAvailable As String = If(slot.Element("IsSlotAvailable") Is Nothing, "0", slot.Element("IsSlotAvailable").Value)

                Dim isAllDaySlot As String = If(slot.Element("IsAllDaySlot") Is Nothing, "0", slot.Element("IsAllDaySlot").Value)
                Dim isSelected As String = slot.Element("IsSelected").Value

                Dim slotTimes As String() = slotDesc.Split("-"c)
                Dim timeSplit1 As String() = slotTimes(0).Split(" "c)
                Dim timeSplit2 As String() = slotTimes(1).Trim().Split(" "c)

                Dim availUnavail As String = If(isSlotAvailable.Equals("1"), "available", "unavailable")
                Dim selected As Boolean = If(isSelected.Equals("1"), True, False)
                Dim availUnavailText As String = availUnavail.ToUpper()
                Dim ava As String = " (" & (Integer.Parse(allocatedNumbers) - Integer.Parse(bookedNumbers)).ToString() & ")"
                If Integer.Parse(availableNumbers) <= 0 Then
                    isSlotAvailable = "0"
                    isSelected = "0"
                    availUnavail = "unavailable"
                    selected = False
                    availUnavailText = "UNAVAILABLE"
                End If
                If selected Then
                    availUnavail += " selected"
                    availUnavailText = "SELECTED"
                End If
                'else 
                'If Not availUnavail.Equals("unavailable") AndAlso isDevEnv Then
                'availUnavailText += ava
                'End If

                sb.Append("<div class='timeslots " & availUnavail & "' id='slot" & slotId & "' style='width:" & widthPercentage.ToString() & "%;font-family:Helvetica, Arial, sans-serif'>")
                sb.Append("<div class='slots'>")
                sb.Append("<div class='time left' style='font-size:" & fontSize & "'>" & timeSplit1(0) & "<span class='ampm'>" & timeSplit1(1) & "</span></div>")
                sb.Append("<div class='arrow'><span class='arrowhover'>&nbsp;</span></div>")
                sb.Append("<div class='time right' style='font-size:" & fontSize & "'>" & timeSplit2(0) & "<span class='ampm'>" & timeSplit2(1) & "</span></div>")
                sb.Append("<div class='availability'>" & availUnavailText & "</div>")
                sb.Append("</div>")
                sb.Append("</div>")

            Next
        Else
            'sb.Append("<div class='timeslots unavailable' id='slot0' style='width:100%'>")
            'sb.Append("<div class='slots' style='width:100%'>")
            ''//sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='"+passedParams+"'>alternate availability</a></div>");
            ''//+"&rType=GetAlternateOptions&Brand=" + brand
            ''    ////sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='preloader.aspx?vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brand + "'>alternate availability</a></div>");
            ''sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='altavailability.aspx?sid=" + sid + "&vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brand + "'>Alternate Availability</a></div>")

            'sb.Append("<div class='availability'>unavailable</div>")
            'sb.Append("</div>")
            'sb.Append("</div>")
            'If retData.InnerText.Equals("SlotIsRequired") Then
            '    ddlPickUpTime.Style.Add("display", "none")
            '    lblPickUpTime.Style.Add("display", "none")
            '    lblNoNeedMessaeg.Style.Add("display", "visible")
            '    slotsDetail.Visible = False
            '    btnContinue.Style.Add("display", "none")
            '    'Else
            '    '    ddlPickUpTime.Visible = True
            'End If
            If retData.InnerText.Equals("SlotIsRequired") Then
                ddlPickUpTime.Style.Add("display", "visible")
                lblPickUpTime.Style.Add("display", "visible")
                lblNoNeedMessaeg.Style.Add("display", "none")
                slotsDetail.Visible = False
                btnContinue.Style.Add("display", "visible")
                'Else
                '    ddlPickUpTime.Visible = True
            End If
            Return Nothing
        End If
       
        Return sb.ToString()




        'Return sb.ToString()

    End Function

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Dim testSlot As String = txtSlot.Value
        Dim testSlotId As String = txtSlotId.Value
        'RaiseEvent BookBookingV2(CStr(dgAgentData.DataKeys(dgAgentData.SelectedIndex)), GetStatus, testSlot, testSlotId)
        RaiseEvent ModifyBookingclickedV2(testSlot, testSlotId)
        btnContinue.Style.Add("display", "visible")
        IsQuoteValue = ""
        IsQuote.Value = ""
    End Sub


End Class
