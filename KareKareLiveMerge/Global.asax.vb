Imports System.Web
Imports System.Web.SessionState

Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any Initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
        Session.Timeout = 5
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
		' Fires when an error occurs
		Dim ex As Exception
        ''Dim sErrorMessage As String
		ex = Server.GetLastError
		Session("LastErrorNumber") = ExceptionManager.Publish(ex)
		Session("LastErrorMessage") = ex.Message

        'Try
        '	'SendSimpleEmail()
        '	SendMail(ex)
        'Catch ex2 As Exception
        '	ExceptionManager.Publish(ex2)
        'End Try

        'Karel Jordaan 30June2006
        'We want complete error msg in debug
#If Not debug Then
		redirect(ex)
#End If
	End Sub
	Public Sub redirect(ByVal ex As Exception)
		Dim redirectPath As String = "~\Error.aspx?No=" & CStr(Session("LastErrorNumber")) & "&Message=" & CStr(Session("LastErrorMessage"))
		Response.Redirect(redirectPath)
	End Sub

	Private Sub SendMail(ByVal ex As Exception)
		Dim mailMessage As New Mail.MailMessage
		mailMessage.Subject = "Error in Aurora Application"
		mailMessage.From = "mark.lonergan@advancedsolutions.co.nz"
		mailMessage.Body = ex.Message.ToString
		mailMessage.Body &= " Error number: " & CStr(Session("LastErrorNumber"))
		mailMessage.BodyFormat = Mail.MailFormat.Text
		mailMessage.To = "mark.lonergan@advancedsolutions.co.nz"
		Dim SmtpMail As Mail.SmtpMail
		SmtpMail.SmtpServer = Config.GetConfigValue("SMTPServer")
		SmtpMail.Send(mailMessage)
	End Sub


	Private Sub SendSimpleEmail()
		'	Dim message As String = "Error Number: " & CStr(Session("LastErrorNumber"))
		System.Web.Mail.SmtpMail.SmtpServer = System.Configuration.ConfigurationSettings.AppSettings("SMTPServer")
		System.Web.Mail.SmtpMail.Send("mark.lonergan@advancedsolutions.co.nz", "mark.lonergan@advancedsolutions.co.nz", "Error Occurred", "message")
	End Sub

	Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
        Response.Redirect("AgentLogin.aspx")
	End Sub

	Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
		' Fires when the application ends
	End Sub

End Class
