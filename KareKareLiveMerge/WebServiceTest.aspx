<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WebServiceTest.aspx.vb" Inherits="THLAuroraWebInterface.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:Button id="btnPing" runat="server" Text="Ping"></asp:Button></P>
			<P>
				<asp:Label id="lblPing" runat="server"></asp:Label></P>
			<P>
				<asp:Button id="btnVehicle" runat="server" Text="Get Available Vehicle"></asp:Button></P>
			<P>
				<asp:Label id="lblVehicle" runat="server"></asp:Label></P>
		</form>
	</body>
</HTML>
