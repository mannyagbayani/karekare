﻿

function initConfigurePage() {

    $("a.PopUp").click(function () {
        NewWindow($(this).attr('href'), 'vCode', '820', '500', 'yes', 'center');
        return false;
    });


    $('#bookingSubmitted').val('0');
    $(".ShowPriceList").click(function (e) {//bind expand
        var parentElm = $(this).parent()[0];
        while (parentElm.tagName != "DIV")
            parentElm = $(parentElm).parent()[0];
        $(parentElm).toggleClass("Collapse");
        if ($(parentElm).hasClass("Collapse"))
            $(this).text("Details");
        else
            $(this).text("Close Details");
        e.stopPropagation();
        e.preventDefault();
    });

    /*if ($.browser.safari) {
        $("#hireItemsPanel select").blur(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });
        $("#hireItemsPanel input").blur(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });
    }
    else {*/
        $("#hireItemsPanel select").change(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });

        $("#hireItemsPanel input").click(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });
    // }
    $("#ferryPanel select").change(function () {//bind ferry select change
        updateFerrySelection(this);
    });
    $("#ferryPanel input[type='checkbox']").attr('checked', ''); //clear checked cboxs
    $("#ferryPanel input").click(function () {//bind ferry CheckBox
        var elm = $(this);
        if (elm.attr('checked'))//expand ferry panel
            $("#ferryPanel table." + elm.attr('id')).removeClass('Hide');
        else {//hide and reset
            $("#ferryPanel table." + elm.attr('id')).addClass('Hide');
            $("#ferryPanel table." + elm.attr('id') + " select").val('0'); //reset ddowns
        }
        updateFerrySelection(elm);
    });
    $("#insOptionsTable input").click(function () {//bind insurance CheckBox/Radio
        var elm = $(this);
        updateIsuranceOption(elm);
    });
    $("#insuranceAgeSelector input").click(function () {//bind insurance CheckBox/Radio
        var elm = $(this);
        updateDriversAge(elm);
    });
    updateTotal();
    setER0ByAge(false);
    if (ferryItems.length == 0) {
        $('#ferryPanel').addClass('Hide');
    }

    if ($('#promptTextArea span').text().length < 5) $('#promptTextArea').css('display', 'none'); //hide msg on empty.

    $("a.PopUp").click(function () {
        NewWindow($(this).attr('href'), 'vCode', '720', '500', 'yes', 'center'); //mauiv2 wider popup
        return false;
    });

    //------restore form (on crumbs)
    $("#hireItemsPanel input, #hireItemsPanel select").each(
        function (i) {
            if ($(this).attr('id') != 'ehi_total') {
                //console.log($(this).attr('id'));
                updateExtraHiresSelection(this);
            }
        });

    swapAgeSelector();

    $("#currencySelector").change(function () {//bind currency swap
        var elm = $(this);
        currencyCode = elm.val();
        factor = $('#currencySelector :selected').attr('rel');
        var currentTotal = $('#totalAmount').text().replace(/[^0-9.]+/g, '');
        if (currencyCode != currencyStr) {
            $("#alternateCurrency").css('display', 'block');
            populateForeignTotal($("#alternateCurrency"), currentTotal, currencyCode);
        } else {
            $("#alternateCurrency").css('display', 'none');
        }
    });
    $("#currencySelector").val(currencyStr); //reset to local currency

    calculatePrice(currencyStr, selectedExRate.code, selectedExRate.rate);//Exchange rates
}


function swapAgeSelector() {
    var hasAges = false;
    for (i = 0; i < insuranceProducts.length; i++) {
        if (insuranceProducts[i].minAge > 0 || insuranceProducts[i].maxAge > 0)
            hasAges = true;
    }
    if (hasAges) $('#insuranceAgeSelector').addClass('Show');
}

function updateDriversAge(elm) {
    if (elm.val() == '18+') {
        $('#insOptionsTable').addClass('Under21');
        $('#insOptionsTable').removeClass('Over21');
        //setER0ByAge(true);
    }
    else {
        $('#insOptionsTable').removeClass('Under21');
        $('#insOptionsTable').addClass('Over21');
        //setER0ByAge(false);
    }
}


function setER0ByAge(under21) {//set the relevant ER0
    if (typeof insuranceProducts == 'undefined') return;
    $('#insOptionsTable td.total em').remove(); //remove pricing label elm    
    for (var i = 0; i < insuranceProducts.length; i++) {
        insuranceProducts[i].numItems = 0;
        if (insuranceProducts[i].code.split('_')[0] == 'ER0') {//found ER0           
            insuranceProducts[i].numItems = ((under21 && insuranceProducts[i].minAge == 18) || (!under21 && insuranceProducts[i].minAge == 21)) ? 1 : 0;
            jQuery.each($('#insOptionsTable tr input'), function () {
                if ($(this).attr('id') == "ipi_" + insuranceProducts[i].code) {
                    if ((under21 && insuranceProducts[i].minAge == 18) || (!under21 && insuranceProducts[i].minAge == 21) || (insuranceProducts[i].minAge == 0)) {
                        $(this).attr('checked', 'checked'); //biz logic in front-end                        
                    }
                }
            });
        }
    }
    updateTotal();
}


function updateFerrySelection(elm) {
    var selectedElm = $(elm);
    var elmType = elm.tagName;
    var selected = $(selectedElm);
    var amount = 0;
    var checkboxes = $('#ferryPanel input'); //get all vehicles
    for (var i = 0; i < checkboxes.length; i++) {
        var check = $(checkboxes[i]);
        if (check.attr('checked')) {
            amount += parseFloat(check.val());
        }
    }
    //ferryItems = {pct_wlg:{selected:false,vehicle:209.00,Adult:75.00,Child:40.00,Infant:0.00},wlg_pct:{selected:false,Adult:75.00,Child:40.00,Infant:0.00,},total:0};
    var ddowns = $('#ferryPanel tr.pax td select'); //get all pax

    //reset the hidden containers
    $("#ferryPanel #paxCounter").val('');

    for (var i = 0; i < ddowns.length; i++) {
        var dd = $(ddowns[i]);
        var idParams = dd.attr('id').split('_');
        var valStr = "ferryItems." + idParams[0] + "_" + idParams[1] + "." + idParams[2];
        var currentItemPrice = parseFloat(eval(valStr));
        var ddownVal = parseFloat(dd.val());
        if (ddownVal > 0)
            amount += ddownVal * currentItemPrice;
        var parentTR = dd;
        while (parentTR.tagName != "TR")
            parentTR = $(parentTR).parent()[0];
        $('.total', $(parentTR)).text((ddownVal > 0) ? CurrencyFormatted(ddownVal * currentItemPrice, currencyStr, 2) : '');

        //get and set the relevant hidden container
        var currentPrdId = dd.attr('rel');
        var hiddenInput = $("#ferryPanel #paxCounter");
        var currentVal = hiddenInput.val();
        hiddenInput.val(currentVal + idParams[0] + "_" + idParams[1] + "_" + idParams[2] + "_" + currentPrdId + ":" + ddownVal + ",");
        //end update hidden        
    }
    ferryItems.total = amount;

    if (typeof vCharges == "undefined") updateTotal(); //Added for Modify Quote
    else updateQuoteTotal();

}

function updateTotal() {
    var hiddenHasCustomerChargeValue = $("#HiddenHasCustomerCharge").attr("value")
    var hiddenHasCustomerChargeName = $("#HiddenHasCustomerCharge").attr("name")

    

    var hiredays = availabilityItem.hirePeriod;
    var totalPayableAtPickup = vehicleB2BTotals.pap;
    var totalPayableToAgent = vehicleB2BTotals.pta;
    $('.Total .pap').attr('rel', totalPayableAtPickup);
    $('.Total .pta').attr('rel', totalPayableToAgent);
    for (var i = 0; i < insuranceProducts.length; i++) {//Add Insurance
        if (insuranceProducts[i].numItems == 1) 
            totalPayableAtPickup += insuranceProducts[i].charge  //TODO: is insuranceProducts.MaxCharge less..
        
    }
    
    for (var i = 0; i < extraHireItems.length; i++) {//add extra hires
        if (extraHireItems[i].numItems > 0) 
        {
            //-----------------------------------------------------------------------------------------------------
            //rev:mia MAY 3 2013  - PART OF APCOVER ENHANCEMENT. 
            //                    - THIS WILL RETRIEVE ADD THE VALUE OF THE NEW APCOVER(ETC) TO THE totalPayableToAgent
            //-----------------------------------------------------------------------------------------------------
            if (hiddenHasCustomerChargeValue == "pta" && extraHireItems[i].code == hiddenHasCustomerChargeName) {
                totalPayableToAgent += extraHireItems[i].charge  
            }        
            else
                totalPayableAtPickup += calculateExtraHireTotal(extraHireItems[i], hiredays);
        }
    }
    $('#allInclusivePanel .total .exRate').each(function () {
        if ($(this).is('.pta') && parseFloat($(this).attr('rel')) > 0) {
            totalPayableToAgent = parseFloat(vehicleB2BTotals.pta) + parseFloat($(this).attr('rel'));
        }
        else if ($(this).is('.pap') && parseFloat($(this).attr('rel')) > 0) {
            totalPayableAtPickup += parseFloat($(this).attr('rel'));
        }
    });
    //if (ferryItems.total != null && ferryItems.total > 0) n/a
    //    totalAmount += ferryItems.total; //add ferry totals..

    $('#totalAmount').text(CurrencyFormatted(totalPayableToAgent, currencyStr, 10)); //agent charges
    $('#totalAmount').attr('rel', totalPayableToAgent); //agent charges

    $('#payAtPickUp').text(CurrencyFormatted(totalPayableAtPickup, currencyStr, 10)); //pick up totals
    $('#payAtPickUp').attr('rel', totalPayableAtPickup);

    calculatePrice(currencyStr, selectedExRate.code, selectedExRate.rate); //Exchange rates
}

function getAvailTotal(avp) {
    for (var i = 0; i < vehicleB2BTotals.length; i++)
        if (vehicleB2BTotals[i].AVPID == avp) return vehicleB2BTotals[i];
        return null; 
}


function calculateExtraHireTotal(itemElm, hiredays) {
    return (itemElm.charge);
}

function updateExtraHiresSelection(elm) {
    var selectedElm = $(elm);
    var elmType = elm.tagName;
    var selected = $(selectedElm);
    var itemId = selected.attr('id').split('_')[1];
    var selectedElmData = getHireItemById(itemId);

    var selectedUOM = selectedElmData.uom;
    var selectedMaxCharge = selectedElmData.maxCharge;
    if (selectedElmData == null)
        return; //no data match
    var amount = 0;
    if (elmType == "INPUT") {
        amount = (selected.attr('checked') ? 1 : 0);
    } else {//<SELECT>
        amount = selected.val();
    }
    selectedElmData.numItems = amount;
    var numDays = (selectedUOM == 'Day') ? availabilityItem.hirePeriod : 1;
    var perUnitTotalCap = (numDays * selectedElmData.dailyRate > selectedMaxCharge && selectedMaxCharge > 0) ? selectedMaxCharge : numDays * selectedElmData.dailyRate;
    var totalCalCharge = perUnitTotalCap * amount;
    totalCalCharge = (selectedElmData.grossAmt * amount);
    selectedElmData.charge = totalCalCharge;
    setRowTotal(selectedElm, selectedElmData.charge, 'extraHire');
    updateTotal();
}

function getHireItemById(itemId) {
    for (i = 0; i < extraHireItems.length; i++)
        if (extraHireItems[i].id == itemId)
            return extraHireItems[i];
    return null;
}


function updateIsuranceOption(elm) {
    if ($(elm).attr('type') == 'checkbox') {//this is a <no worries> exp
        var selectedinsuranceElm = null; //init
        for (var i = 0; i < insuranceProducts.length; i++) {
            if ("ipi_" + insuranceProducts[i].id == elm.attr('value')) {
                if (elm.attr('checked'))
                    insuranceProducts[i].numItems = 1;
                else
                    insuranceProducts[i].numItems = 0;
                selectedinsuranceElm = insuranceProducts[i];
            }
            else {
                //insuranceProducts[i].numItems = 0;
            }
        }
        setRowTotal(elm, selectedinsuranceElm.charge, 'insurance');
        updateTotal();
        return;
    }
    //else
    //    $("#insOptionsTable INPUT[type='checkbox']").attr('checked', '');
    $('#allInclusiveDetails').addClass('Hide');

    var selectedinsuranceElm = null; //init
    for (var i = 0; i < insuranceProducts.length; i++) {
        if ("ipi_" + insuranceProducts[i].id == elm.attr('value')) {
            insuranceProducts[i].numItems = 1;
            selectedinsuranceElm = insuranceProducts[i];
        }
        else {
            insuranceProducts[i].numItems = 0;
        }
    }
    setRowTotal(elm, selectedinsuranceElm.charge, 'insurance');
    setProductDependencies(elm);
    updateTotal();
}

function setRowTotal(elm, totalValue, rowType) {//Set Total within UI Row
    if (rowType == "extraHire") {
        var urlCurrency;
        var rateConversion = 1;
        var differentCurr = false;

        if (getUrlVars()["cur"] != null) {
            urlCurrency = getUrlVars()["cur"];
            if (currencyStr != urlCurrency) {
                differentCurr = true;
            }
        }
        

        if (getUrlVars()["rate"] != null) {
            rateConversion = getUrlVars()["rate"];
        }

        totalValue = (totalValue > 0 ? totalValue : "");
        parentElm = $(elm.parent()[0]);
        var trElm = $(parentElm).parent()[0];

        //-----------------------------------------------------------------------------------------------------
        //rev:mia MAY 3 2013  - PART OF APCOVER ENHANCEMENT. 
        //                    - THIS WILL RETRIEVE THE "TD.TOTAL" AND INJECT DUMMY CSS IN THE HEAD SECTION
        //-----------------------------------------------------------------------------------------------------
        var papmode = $(elm).attr("class")
        if (papmode == 'ptaMode') {
            $("<style[id='injectedCss']").remove() 
            $("<style type='text/css' id='injectedCss'> #hireItemsPanel table td.price {width:290px;} </style>").appendTo("head");
            var totalElm = $(parentElm).parent().find("td:eq(4)")
            $(totalElm).css("text-align", "left")

            if (differentCurr == true) {
                totalValue = parseFloat(totalValue) * parseFloat(rateConversion)
                $('.total', trElm).html((totalValue > 0) ? "<em class='pta' rel='" + totalValue + "'>" + CurrencyFormatted(totalValue, urlCurrency, 10) + "</em>" : "");
            }
            else
                    $('.total', trElm).html((totalValue > 0) ? "<em class='pta' rel='" + totalValue + "'>" + CurrencyFormatted(totalValue, currencyStr, 10) + "</em>" : "");
        }
        else
        $('.total', trElm).html((totalValue > 0) ? "<em class='pap' rel='" + totalValue + "'>" + CurrencyFormatted(totalValue, currencyStr, 10) + "</em>" : "");
        //-----------------------------------------------------------------------------------------------------

    } else if (rowType == "insurance") {
        totalValue = (totalValue > 0 ? totalValue : "");
        parentElm = $(elm.parent()[0]);
        var trElm = $(parentElm).parent()[0];
        $('.total', trElm).html((totalValue > 0) ? "<em class='pap' rel='" + totalValue + "'>" + CurrencyFormatted(totalValue, currencyStr, 10) + "</em>" : "");
        jQuery.each($('#insOptionsTable tr'), function () {
            if (!$('.cb input', $(this)).attr('checked') || (typeof $('.cb input', $(this)).attr('checked') == 'undefined')) {
                $('.total', $(this)).html('');
            }
        });
    }
}

function disableInsCbs() {
    $("#insOptionsTable INPUT[type='checkbox']").attr('disabled', 'disabled');
    $("#insOptionsTable INPUT[type='checkbox']").attr('checked', false);
}

function setProductDependencies(selectedElm) {
    //iterate through all items and check if selected item key exists
    // if exists and has NOTDISPLAY value untick and disable
    disableInsCbs();
    var selectedElmId = selectedElm.attr('id').split('_')[1];
    for (var i = 0; i < insuranceProducts.length; i++) {
        //found the selected meta and it has not display items in it
        if (insuranceProducts[i].requiredActions != null && insuranceProducts[i].requiredActions.length > 0) {//has required actions
            for (var j = 0; j < insuranceProducts[i].requiredActions.length; j++) {//for each action
                var currentParams = insuranceProducts[i].requiredActions[j].split(':');
                if (currentParams[0] == selectedElmId && currentParams[1] == 'DISPLAYWHEN') {//has display action
                    $("#ipi_" + insuranceProducts[i].code).attr('disabled', false);
                    $("#ipi_" + insuranceProducts[i].code).attr('checked', 'checked');
                    $("#ipi_" + insuranceProducts[i].code).trigger('click');
                }
            }
        }
    }
}

var baseUrl;

function renderInclusive(elm, avpId) {
    $('#allInclusivePanel').html('<big>Please wait while we are updating your insurance options</big>');
    var cbElm = $(elm);
    var selected = (cbElm.attr('checked') == "checked")
    baseUrl = document.location.toString();
    baseUrl = baseUrl.indexOf('inc') > 0 ? baseUrl : baseUrl + "&inc=0"; //for backwards compatibility
    baseUrl = baseUrl.replace("inc=" + (selected ? "0" : "1"), "inc=" + (selected ? "1" : "0"));
    //-----------------------------------------------------------------------------------------------------
    //rev:mia APRIL 4 2013  - PART OF APCOVER ENHANCEMENT. 
    //                        IT WILL CHECK IF 'PRDID' QUERY STRING IS PRESENT, OTHERWISE ADD IT 
    //-----------------------------------------------------------------------------------------------------
    TagInsuranceInUrl(elm)
    setTimeout("window.location = baseUrl", 2000);
}

function CurrencyFormatted(amount, currencyCode, pricePadding) {
    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s; //s is now Neutral Decimal
    var regEx = /(\d+)(\d{3})/;
    while (regEx.test(s)) {
        s = s.replace(regEx, '$1' + ',' + '$2');
    }
    var padding = "";
    for (var i = 0; i < (pricePadding - s.length); i++)
        padding = padding + " ";

    //-----------------------------------------------------------------------------------------------------
    //rev:mia MAY 30 2013  - PART OF APCOVER ENHANCEMENT. 
    //-----------------------------------------------------------------------------------------------------
    if (currencyStr != currencyCode) {
        s = currencyCode + " " + getCurSymForCode(currencyCode) + padding + s;
    }
    else
        s = currencyCode + " $" + padding + s;
    
    

    return s;
}

function setER0ByAge(under21) {//set the relevant ER0
    if (typeof insuranceProducts == 'undefined') return;
    $('#insOptionsTable td.total em').remove(); //remove pricing label elm    
    for (var i = 0; i < insuranceProducts.length; i++) {
        insuranceProducts[i].numItems = 0;
        if (insuranceProducts[i].code.split('_')[0] == 'ER0') {//found ER0           
            insuranceProducts[i].numItems = ((under21 && insuranceProducts[i].minAge == 18) || (!under21 && insuranceProducts[i].minAge == 21)) ? 1 : 0;
            jQuery.each($('#insOptionsTable tr input'), function () {
                if ($(this).attr('id') == "ipi_" + insuranceProducts[i].code) {
                    if ((under21 && insuranceProducts[i].minAge == 18) || (!under21 && insuranceProducts[i].minAge == 21) || (insuranceProducts[i].minAge == 0)) {
                        $(this).attr('checked', 'checked'); //biz logic in front-end                        
                    }
                }
            });
        }
    }
    updateTotal();
}

function validateSlot(e) {
        /* Added by Nimesh on 23rd Feb 2015 to validate if slot is selected*/
        var $k = $("#selectionError");
        $k.remove();
        var $j = $("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");
        
        if (jQuery('.timeslots.available').length > 0) {
            if (!(jQuery('.timeslots.available.selected').length === 1)) {
                //var error = $j("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");
                $j.insertBefore('#pickuptimes');
                var position = jQuery('#pickuptimes').position();
                scroll(0, position.TopPosition);
                return false;
            }
            else {
                //$j.remove();
                selSlot = true;
                return true;
            }
        }
        /* End Added by Nimesh on 23rd Feb 2015 to validate if slot is selected*/
    }
function submitBooking(submitElm) {
    //debugger;
    var agentRef = "";  var cNote = "";
    if ($(submitElm).val() == "SAVE") {
        agentRef = ($("#addtob").val() == "T" ? $("#agentRef").val() : $("#custAgentRef").val()); 
        cNote = ($("#addtob").val() == "T" ? $("#customerNoteTxt1").val() : $("#customerNoteTxt").val());   
    }    
    if ($(submitElm).val() == "SAVE" && !$("#saveBooking").valid() && $("#addtob").val() == "F") {//new b no cust
        return false;
    } else if ($("#addtob").val() == "T" && ($("#bookingNo").val() == "" || $("#agentRef").val() == "")) {
        alert("'agent ref', 'booking required' and 'Confirmation to be send to' are required.");
        return false;
    }

    if ($("#addtob").val() == "T") {
        var label = $("label.error").attr("for", "emailConfirmationSentToBox");
        if ($("#emailConfirmationSentTo").css("display") != 'none') {
            if (label != null) {
                var email = $("#emailConfirmationSentToBox.valid").val();
                try {
                    if (email === undefined) {
                        alert("'agent ref', 'booking required' and 'Confirmation to be send to' are required.");
                        return false;
                    }
                }
                catch (ex) {
                }

            }
        }
    }
    
    

    /* Added by Nimesh on 13 thFeb 2015 to validate if slot is selected*/
    validateSlot();
    var selSlot = false;
    if (jQuery('.timeslots.available').length > 0) {
        if (!(jQuery('.timeslots.available.selected').length === 1)) {
            var error = $j("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");
            error.insertBefore('#pickuptimes');
            var position = jQuery('#pickuptimes').position();
            scroll(0, position.TopPosition);
            return false;
        }
        else {
            //$j('#selectionError').remove();
            selSlot = true;
        }
    }
    /* End Added by Nimesh on 13thFeb 2015 to validate if slot is selected*/


    var cParams = ""; var slotParams = "";
    if ($(submitElm).val() == "SAVE") {
        cParams = $("#addtob").val() + ":" + $('#customerTitleDD').val() + ":" + $('#customerFirstName').val() + ":" + $('#customerLastName').val() + ":" + $('#emailAddressTxt').val() + ":" + $('#telephoneTxt1').val() + $('#telephoneTxt2').val() + $('#telephoneTxt3').val() + ":" + cNote + ":" + $("#bookingNo").val() + ":" + agentRef;
        // Nimesh Added this for time slot on 13th Feb 2015

        if (selSlot) {
            slotParams += ",selectedSlot:'";
            jQuery('.timeslots.available.selected').find('.time.left').each(function () {
                slotParams += this.innerText;
            });
            slotParams += " - ";
            jQuery('.timeslots.available.selected').find('.time.right').each(function () {
                slotParams += this.innerText + "'";
            });
            var slotId = jQuery('.timeslots.available.selected').attr('id').replace('slot', '');
            slotParams += ",slotId:'" + slotId + "'";
            //formParams += ",selectedSlot:'11:00 am : 04:30 pm'";

        }
        else
            slotParams += ",selectedSlot:'', slotId:''";
        // End Nimesh Added this for time slot on 13th Feb 2015
    }
    else {
        slotParams += ",selectedSlot:'',slotId:''";
        for (var i = 0; i < extraHireItems.length; i++) {
            if (extraHireItems[i].numItems > 0) {
                cParams += "eh:" + extraHireItems[i].id + ":" + extraHireItems[i].numItems + ":" + extraHireItems[i].name + ":" + extraHireItems[i].maxCharge + ":" + extraHireItems[i].dailyRate + ":" + extraHireItems[i].promptQuant + ":" + extraHireItems[i].uom + ":";
                cParams += extraHireItems[i].code + ",";
            }
        }
    }

    //rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
    var emailaddressConfirmation = $('#emailConfirmationSentToBox').val()
    if (typeof emailaddressConfirmation != "undefined") {
        cParams = cParams + ":" + emailaddressConfirmation;
    }
    var B2BAgentEmailResultHidden = $('#B2BAgentEmailResultHidden').val()
    if (typeof B2BAgentEmailResultHidden != "undefined") {
        cParams = cParams + ":" + $('#B2BAgentEmailResultHidden').val();
    }



    var options = {
        url: '/Content/Configure.aspx/Submit',
        // Below code was changed by Nimesh on 13th Feb 2015 to add slot details
        //data: "{'avp':'" + ((typeof availabilityItem != 'undefined') ? availabilityItem.avp : "") + "','confParams':'" + cParams + "','bookingType':'" + $(submitElm).val() + "'}",

        data: "{'avp':'" + ((typeof availabilityItem != 'undefined') ? availabilityItem.avp : "") + "','confParams':'" + cParams + "','bookingType':'" + $(submitElm).val() + "'" + slotParams + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'post',
        beforeSend: function () {
            $('#configureUpdatePanel .Submit').addClass("submitting");
        },
        success: function (json) {
            //debugger
            var confParams = eval(json.d);
            if (confParams[0].status == "failed") {
                alert(confParams[0].msg);
                $('#configureUpdatePanel .Submit').removeClass("submitting");
                if (confParams[0].msg.indexOf("session expired") > 0)
                    document.location = "Availability.aspx";
                else
                    return;
            }
            else {

                if ((typeof confParams[0].avp != 'undefined') && confParams[0].status == "success" && confParams[0].quoteId == "") {//conf success
                    //debugger;
                    //-----------------------------------------------------------------------------------------------------
                    //rev:mia APRIL 4 2013  - PART OF APCOVER ENHANCEMENT. 
                    //                        IT WILL CHECK IF 'PRDID' QUERY STRING IS PRESENT, OTHERWISE ADD IT 
                    //-----------------------------------------------------------------------------------------------------

                    var configurl = document.location.search;
                    //if (configurl.charAt(0) === '?')
                    //    configurl = configurl.slice(1);

                    //alert(configurl)
                    $.cookie('urlForConfigurePage', configurl);
                    var saveUrl = "Save.aspx?avp=" + confParams[0].avp
                    saveUrl = saveUrl + "&cur=" + selectedExRate.code
                    saveUrl = saveUrl + "&rate=" + selectedExRate.rate
                    //saveUrl = saveUrl + "&prev=" + configurl

                    /*
                    if (getUrlVars()["cur"] == null) {
                    saveUrl = saveUrl + "&cur=" + selectedExRate.code
                    }
                    if (getUrlVars()["rate"] == null) {
                    saveUrl = saveUrl + "&rate=" + selectedExRate.rate
                    }
                    */
                    saveUrl = saveUrl + PersistInsuranceUrlWhenNavigating();
                  
                    //document.location = "Save.aspx?avp=" + confParams[0].avp + "&cur=" + selectedExRate.code + "&rate=" + selectedExRate.rate + PersistInsuranceUrlWhenNavigating();
                    document.location = saveUrl
                } else {//save success
                    //debugger;
                    document.location = "/content/confirm.aspx?bid=" + confParams[0].quoteId + "&cur=" + selectedExRate.code + "&rate=" + selectedExRate.rate
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, error) {
            alert(XMLHttpRequest, error);
            $('#configureUpdatePanel .Submit').removeClass("submitting");
        },
        timeout: function () {
            alert("timeout");
            $('#configureUpdatePanel .Submit').removeClass("submitting");
        }
    };
    var xhr = $.ajax(options);   
    return false;
}

function swapSubmitForm(elm) {
    $("#addtob").val($(elm).attr('rel'));
    if ($("#addtob").val() == "F") {
        $('#customerDetails').addClass("newBooking");
    }
    else
        $('#customerDetails').removeClass("newBooking");
    $("#customerDetails .Radios .tab").removeClass('selected');
    $(elm).parent().addClass('selected');
}

function initSaveBooking() {

    $("a.PopUp").click(function () {
        NewWindow($(this).attr('href'), 'vCode', '820', '500', 'yes', 'center');
        return false;
    });

    $("#customerDetails .Radios .tab label").click(function () { swapSubmitForm(this); });

    $("#saveBooking").validate({
        rules: {
            emailAddressTxt: {
                required: false,
                email: true
            },
            confirmEmailAddressTxt: {
                required: false,
                equalTo: "#emailAddressTxt"
            },
            customerLastName: { required: true }
        }

        ,
        messages: {
            customerFirstName: "Please enter customer's firstname",
            customerLastName: "Please enter customer's lastname",
            emailAddressTxt: "Please enter customer's contact email",
            confirmEmailAddressTxt: "Please retype customer's email"
        }
    });
}

//-----------------------------------------------------------------------------------------------------
//rev:mia APRIL 4 2013  - PART OF APCOVER ENHANCEMENT. 
//                        IT WILL CHECK IF 'PRDID' AND 'PAP' QUERY STRING IS PRESENT, OTHERWISE ADD IT 
//-----------------------------------------------------------------------------------------------------
function TagInsuranceInUrl(elm) {
    var hiddenHasCustomerChargeValue = '';


    var sapId = '';
    var indexes = baseUrl.indexOf('&prdId');

    if ($(elm).attr("id").indexOf('allInclusivePkg') != -1) {
        sapId = $(elm).attr("name")
        hiddenHasCustomerChargeValue = $(elm).attr("class");

        if ($(elm).is(':checked')) {
            if (indexes > 0) {
                baseUrl = baseUrl.substring(0, indexes);
                baseUrl = baseUrl + "&prdId=" + sapId;
                baseUrl = baseUrl + "&pap=" + hiddenHasCustomerChargeValue;

            }
            else {
                baseUrl = baseUrl + "&prdId=" + sapId;
                baseUrl = baseUrl + "&pap=" + hiddenHasCustomerChargeValue;
            } //if (indexes > 0) {
        }
        else {
            baseUrl = baseUrl.substring(0, indexes);
            //baseUrl = baseUrl.replace("inc=0", "inc=1");
        }
    }
    else {
        if (indexes != -1) baseUrl = baseUrl.substring(0, indexes);

    }
}

function PersistInsuranceUrlWhenNavigating() {
    //debugger;
    var myUrl = document.location.toString();
    var indexes = myUrl.indexOf('&prdId');


    if (indexes > 0) {
        //rev:mia March 5 2014 - fixes for trailfinder. the query string Request.Params['cur'] returned two values like 'GBP,GBP'    
        //save.aspx already have avpid, cur and rate parameter
        var tempurl = myUrl.substring(indexes);
        var prdid = getUrlVars()["prdId"]
        var pap = getUrlVars()["pap"]
        tempurl = ""
        tempurl = tempurl + (prdid == null ? "&prdid=" : "&prdid=" + prdid)
        tempurl = tempurl + (pap == null ? "&pap=" : "&pap=" + pap)
        return tempurl
    }
    else
        myUrl = "";


    return myUrl;
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

//-------------------------------------------------------------------------------------------