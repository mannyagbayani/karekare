﻿var bookingReportsData = (function () {
    var data = [
        createInAndOutReport(),
        bookingPaymentInformationReport()
    ];

    function createInAndOutReport() {
        var name = 'Ins and Outs By Branch - Summary - Inc Ext Count';
        var url = '/THL Rentals/UAT/Since 15Aug2014/Customer Services/Ins and Outs By Branch - Summary - Inc Ext Count';
        var params = [
            new ReportDateParam('Start Date', 'StartDate', true, true),
            new ReportDateParam('End Date', 'EndDate', true, true),
            new ReportListParam('Country', 'Country', true, true, [new ReportParamOption('Australia', 'AU'), new ReportParamOption('New Zealand', 'NZ')]),
            new ReportStringParam('ExPkgStr', 'ExPkgStr', true, false, true),
            new ReportStringParam('Loc', 'Loc', false, false, true),
//            new ReportParam('Test', 'Test', ReportParamType.bool, true, true, true, [new ReportParamOption("Value 1", 'val1'), new ReportParamOption('Value2', 'val2')]),
//            new ReportParam('Test2', 'Test2', ReportParamType.string, true, true, true, [], "testing")
        ];
        return new Report(name, url, params);
    }

    function bookingPaymentInformationReport() {
        var name = 'Booking Payment Information - LIVE';
        var url = '/THL Rentals/Accounts/Booking Payment Information - LIVE';
        var params = [
            new ReportDateParam('Pickup From Date', 'PickupFromDate', true, true),
            new ReportDateParam('Pickup To Date', 'PickupTodate', true, true),
            new ReportListParam('Country', 'Country', true, true, [new ReportParamOption('Australia', 'AU'), new ReportParamOption('New Zealand', 'NZ')]),
        ];
        return new Report(name, url, params);
    }

    function getJson() {
        return data;
    }

    return {
        getJson: getJson,
    };
})();