﻿var reportHelper = (function () {
    var reports, selectedReport;
    var reportParamsTag, reportSelectTag;

    function createReportOption(report, reportTag) {
        createListOption(report.name, report.url, reportTag);
    }

    function createList(parameter) {
        reportParamsTag.append('<label id="' + parameter.name + 'label" for="' + parameter.name + '">' + parameter.displayName + '</label>');
        reportParamsTag.append('<select id="' + parameter.name + '"></select>');

        var listTag = $('#' + parameter.name);
        listTag.addClass('list');
        $.each(parameter.options, function (index, option) {
            createListOption(option.name, option.value, listTag);
        });

        listTag.change(function () {
            var option = findOptionByValue(parameter, listTag.val());
            parameter.selectedValue = option;
        });
        parameter.selectedValue = parameter.options[0];
    }

    function createListOption(name, value, tag) {
        tag.append('<option value="' + value + '">' + name + '</option>');
    }

    function createBool(parameter) {
        reportParamsTag.append('<label id="' + parameter.name + 'label" for="' + parameter.name + '">' + parameter.displayName + '</label>');

        $.each(parameter.options, function (index, option) {
            reportParamsTag.append('<input type="radio" name="' + parameter.name + '" value="' + option.value + '">' + option.name + '</input>');
        });

        $('input[name="' + parameter.name + '"]').addClass('bool');
        $('input[name="' + parameter.name + '"]').change(function () {
            var option = findOptionByValue(parameter, $(this).val());
            parameter.selectedValue = option;
        });
    }

    function createString(parameter) {
        reportParamsTag.append('<label id="' + parameter.name + 'label" for="' + parameter.name + '">' + parameter.displayName + '</label>');
        reportParamsTag.append('<input type="text" name="' + parameter.name + '" value="' + parameter.selectedValue + '"></input>');

        $('input[name="' + parameter.name + '"]').addClass('string');
        $('input[name="' + parameter.name + '"]').change(function () {
            parameter.selectedValue = $(this).val();
        });
    }

    function createDatePicker(parameter) {
        reportParamsTag.append('<label id="' + parameter.name + 'label" for="' + parameter.name + '">' + parameter.displayName + '</label>');
        reportParamsTag.append('<input type="text" id="' + parameter.name + '"/>');

        var datePickerTag = $('#' + parameter.name);
        datePickerTag.addClass('date');
        datePickerTag.datepicker({
            disabled: false,
            dateFormat: 'dd-mm-yy',
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
            onSelect: function (text, instance) {
                parameter.selectedValue = text;
            }
        });
        datePickerTag.change(function () {
            parameter.selectedValue = datePickerTag.val();
        });
    }

    function createReportParams() {
        reportSelectTag.empty();
        reportParamsTag.empty();

        reportSelectTag.change(function () {
            var report = findReportByUrl(reportSelectTag.val());
            selectedReport = report;
            reportParamsTag.empty();
            refreshParams();
        });

        $.each(reports, function (index, report) {
            createReportOption(report, reportSelectTag);
        });

        selectedReport = reports[0];
        refreshParams();
    }

    function refreshParams() {
        $.each(selectedReport.params, function (index, param) {
            createReportParam(param);
        });
    }

    function createReportParam(param) {
        if (!param.visible) {
            return;
        }

        switch (param.type) {
            case ReportParamType.date:
                createDatePicker(param);
                break;
            case ReportParamType.bool:
                createBool(param);
                break;
            case ReportParamType.string:
                createString(param);
                break;
            case ReportParamType.list:
                createList(param);
                break;
            default:
        }
    }

    function findReportByUrl(url) {
        var reportToReturn = '';
        $.each(reports, function (index, report) {
            if (report.url == url) {
                reportToReturn = report;
                return;
            }
        });
        return reportToReturn;
    }

    function findOptionByValue(param, value) {
        var optionToReturn = '';
        if (!param) {
            return optionToReturn;
        }

        $.each(param.options, function (index, option) {
            if (option.value == value) {
                optionToReturn = option;
                return;
            }
        });
        return optionToReturn;
    }

    function initialise(reportSelectTagToInit, reportParamsTagToInit) {
        reportSelectTag = reportSelectTagToInit;
        reportParamsTag = reportParamsTagToInit;
    }

    function updateReports(reportsToUpdate) {
        reports = reportsToUpdate;
    }

    function getSelectedReport() {
        return selectedReport;
    }

    function validateSelectedReport() {
        var isValid = true;
        $.each(selectedReport.params, function (index, param) {
            var paramIsValid = param.isValid();
            isValid = isValid && paramIsValid;

            var paramTag = $('#' + param.name + 'label');
            if (paramIsValid) {
                paramTag.removeClass('invalid');
            } else {
                paramTag.addClass('invalid');
            }
        });

        return isValid;
    }

    return {
        initialise: initialise,
        updateReports: updateReports,
        createReportParams: createReportParams,
        getSelectedReport: getSelectedReport,
        validateSelectedReport: validateSelectedReport
    }
})();
