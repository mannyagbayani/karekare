﻿var bookingReports = (function () {
    var baseReportUrl;

    function bindActions() {
        $('#getReport').click(function () {
            var isValid = reportHelper.validateSelectedReport();
            if (!isValid) {
                return;
            }

            var reportCommands = '&rs:Command=Render&rc:Parameters=false&rc:LinkTarget=reportIframe';
            var selectedReport = reportHelper.getSelectedReport();
            var reportPath = selectedReport.getFullPath(baseReportUrl, reportCommands);
            $('#reportIframe').attr('src', reportPath);
        });
    }

    function initialise(baseReportUrlToInit) {
        baseReportUrl = baseReportUrlToInit;
        bindActions();
    }

    function updateReports(reportsToUpdate) {

        reportHelper.updateReports(reportsToUpdate);
        reportHelper.createReportParams();
    }

    return {
        initialise: initialise,
        updateReports: updateReports,
    };
})();