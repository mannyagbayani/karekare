﻿function Report(name, url, params) {
    this.name = name;
    this.url = url;
    this.params = params || [];

    this.getFullPath = function (baseUrl, reportCommands) {
        var reportParams = getParamsString(this.params);
        var reportUrl = baseUrl + '?' + this.url + reportParams + reportCommands;
        return reportUrl;
    };

    function getParamsString(parameters) {
        var result = '';
        $.each(parameters, function (index, parameter) {
            result += parameter.getString();
        });
        return result;
    }
}

function ReportDateParam(displayName, name, required, visible) {
    var params = [displayName, name, ReportParamType.date, required, visible, false];
    ReportParam.apply(this, params);
}

function ReportStringParam(displayName, name, required, visible, allowEmpty, value) {
    var params = [displayName, name, ReportParamType.string, required, visible, allowEmpty, value];
    ReportParam.apply(this, params);
}

function ReportListParam(displayName, name, required, visible, options) {
    var params = [displayName, name, ReportParamType.list, required, visible, false, options];
    ReportParam.apply(this, params);
}

function ReportParam(displayName, name, type, required, visible, allowEmpty, options, value) {
    this.displayName = displayName;
    this.name = name;
    this.type = type;
    this.required = required;
    this.allowEmpty = allowEmpty;
    this.visible = visible;
    this.options = options;
    this.selectedValue = value;

    this.getString = function () {
        var result = '&' + this.name + '=';

        if (!this.required && (!this.selectedValue || !this.selectedValue.value)) {
            return '';
        }
        return result + getValueString(this.type, this.selectedValue);
    }

    this.isValid = function () {
        if (!this.required) {
            return true;
        }

        switch (this.type) {
            case ReportParamType.string:
                if (this.selectedValue) {
                    return true;
                } else {
                    return this.allowEmpty;
                }
            case ReportParamType.date:
                if (this.selectedValue) {
                    return true;
                } else {
                    return false;
                }
            case ReportParamType.bool:
            case ReportParamType.list:
                if (this.selectedValue && this.selectedValue.value) {
                    return true;
                } else {
                    return false;
                }
            default:
                return true;
        }
    }

    function getValueString(selectedType, selectedValue) {
        switch (selectedType) {
            case ReportParamType.string:
            case ReportParamType.date:
                return selectedValue || '';
            case ReportParamType.bool:
            case ReportParamType.list:
                if (selectedValue) {
                    return selectedValue.value || '';
                }
            default:
                return '';
        }
    }
}

function ReportParamOption(name, value) {
    this.name = name;
    this.value = value;
}

var ReportParamType = (function () {
    return {
        date: 0,
        bool: 1,
        string: 2,
        list: 3
    }
})();