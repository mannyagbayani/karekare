﻿function getUrlVarsInvariable() {
    var vars = [], hash;
    var hashes = b2bParamUrl.slice(b2bParamUrl.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
  
    return vars;
}

function ReloadValues() {

    var na = crossSellData.na;
    var nc = crossSellData.nc;
    var pb = crossSellData.pb;
    var db = crossSellData.db;
    var pd = crossSellData.pd;
    var pm = crossSellData.pm;
    var py = crossSellData.py;
    var dd = crossSellData.dd;
    var dm = crossSellData.dm;
    var dy = crossSellData.dy;
    var pt = crossSellData.pt;
    var dt = crossSellData.dt;
    var cr = crossSellData.cr;


        $("#na").val(na);
        $("#nc").val(nc);
        $("#na").next().closest('input').val($("#na").val())
        $("#nc").next().closest('input').val($("#nc").val())
        
        $('#pb').val(pb);
        $('#db').val(db);
        $('#pb,#db').trigger("change");
        //SelectLocation(pb, db);

        
        $('#fromDateDiv').datepicker("setDate", new Date(py, pm-1, pd));
        $('#toDateDiv').datepicker("setDate", new Date(dy, dm-1, dd));

        $('#pd').val(pd);
        $('#pm').val(pm);
        $('#py').val(py);
        $('#dd').val(dd);
        $('#dm').val(dm);
        $('#dy').val(dy);

        $('#pt').val(pt);
        $('#dt').val(dt);

        $("#pt").next().closest('input').val($("#pt option[value='" + pt + "']").text())
        $("#dt").next().closest('input').val($("#pt option[value='" + dt + "']").text())

        $("#countriesDD").val(cr);
        var countryofRes = $("#countriesDD option:selected").text();
        $("#countriesDD").next().closest('input').val(countryofRes)
}


function getCrossSellUrl() {
   // debugger;
    var cty = $("input[name=cc]:checked").attr('rel');
    var cc = getRadioBtnSelValue('cc');
   
    var na;
    var nc;
    var pb;
    var db;

    

    var pd;
    var pm;
    var py;
    var dd;
    var dm;
    var dy;
    var pt;
    var dt;
    var cr;

    if (crossSellData.iscrosssell == true && crossSellData.url != "") {
        if (crossSellData.url != "") {
            ReloadValues();
            cc = crossSellCountry;
            cty = crossSellCountry;

            $('input[name="cc"]:checked').val(cty)
            $("#brandSelector").val("all");
            $("#brandSelector").next().closest('input').val($("#brandSelector option[value='all']").text())
            
            SelectCountries(cc)
            updateCtrlHeader();
        } 
    }

    try {
        if (B2BJSONParams != null) {
            na = B2BJSONParams.na;
            nc = B2BJSONParams.nc;
            pb = B2BJSONParams.pb;
            db = B2BJSONParams.db;
            db = (db == "0") ? $('#pb').val() : db; //handle same as pickup

            var pickupDay = B2BJSONParams.pd.split("-")[0]
            var pickupMonth = B2BJSONParams.pd.split("-")[1]
            var pickupYear = B2BJSONParams.pd.split("-")[2]
            pd = pickupDay;
            pm = pickupMonth;
            py = pickupYear;

            var dropDay = B2BJSONParams.dd.split("-")[0]
            var dropMonth = B2BJSONParams.dd.split("-")[1]
            var dropYear = B2BJSONParams.dd.split("-")[2]
            dd = dropDay;
            dm = dropMonth;
            dy = dropYear;

            pt = B2BJSONParams.pt;
            dt = B2BJSONParams.dt;
        }
        else {
            na = $("#na option:selected").val()
            nc = $("#nc option:selected").val();
            pb = $('#pb').val();
            db = $('#db').val();
            db = (db == "0") ? $('#pb').val() : db; //handle same as pickup
            pd = $('#pd').val();
            pm = $('#pm').val();
            py = $('#py').val();
            dd = $('#dd').val();
            dm = $('#dm').val();
            dy = $('#dy').val();
            pt = $('#pt').val();
            dt = $('#dt').val();
        }
    }
    catch (errorForB2BJSONParams) {
        na = $("#na option:selected").val()
        nc = $("#nc option:selected").val();
        pb = $('#pb').val();
        db = $('#db').val();
        db = (db == "0") ? $('#pb').val() : db; //handle same as pickup
        pd = $('#pd').val();
        pm = $('#pm').val();
        py = $('#py').val();
        dd = $('#dd').val();
        dm = $('#dm').val();
        dy = $('#dy').val();
        pt = $('#pt').val();
        dt = $('#dt').val();
    }


    //var params = csToken.base + "/b2b/" + getRadioBtnSelValue('cc') + "/select/?utm_campaign=CrossLink&ac=" + csToken.agentId + "&sc=rv&pc=&na=" + na + "&nc=" + nc + "&cr=ca&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vh="
    var countryofRes = $("#countriesDD option:selected").val();
    var agentCode = $("#agentsDD option:selected").val();
	var params = csToken.base + "/b2b/" + cc + "/select/?utm_campaign=CrossLink&ac=" + (csToken.agentId != agentCode ? agentCode : csToken.agentId) + "&sc=rv&pc=&na=" + na + "&nc=" + nc + "&cr=" + countryofRes + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt 

	//rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT 
	//THL history: last day of niven  
	//1. set the default option (gross or net)

    //hso = hascurrencyoption => if true, display dropdown for currency
	params = params + "&hso=" + agentData.HasCurrencyOption;

	//hgo = hasgrossnetoption => if true, display gross and net
	params = params + "&hgo=" + agentData.HasGrossNetOptions;

	//gno = hasgrossnetoption => if not empty, then active else disabled
	params = params + "&gno=" + agentData.GrossNetOptions;
	var json = JSON.stringify(minifyCurrency);
	json = $.param({ "json": json })
	params = params + "&" + json;


	params = params + "&cty=" + (cty.indexOf("D") != -1 ? cty : cty + "D");

	params = params + "&vh=";

	params = params + "&brd=" + $("#brandSelector").val();
	
	//console.log("getCrossSellUrl: " + params);
   return params;
}

function initCrossSell() {
    //extend #brandSelector     
}


function submitCrossSell() {

//    if (msieversion() == 10) {
//        alert("All Brand is not supported in IE10");
//        return;
//    }
    
    $('.catalogContainer').addClass('loading');

    var crosssell = getCrossSellUrl() + getAgentParams()
    $('#catalogContainer').html('')

    //$('#catalogContainer').html('<iframe id="csellFrame" frameborder="0" src="' + getCrossSellUrl() + getAgentParams() + '" width="973px" height="600px"/>');
    csellFrame.postMessage(crosssell, "*")
    

    $('#availabilityFilter').css('display','none');
    $('#ctrlBody').addClass('collapse');
}


function getAgentParams() {
    //debugger;
    var prm = "&tkn=" + csToken.token + "&brands=" + csToken.brands + "&grDefOn=" + agentData.GrossDefaultOn + "&grossNetOpn=" + agentData.GrossNetOptions;
    var selectedCurrency = agentData.agents[0].DefaultCurrency;//TODO: biz logic on selected agent here.
    var country = $("input:[name='cc']:checked").val();
    var baseCode = country + "D";    
    for (var i = 0; i < agentData.agents[0].CurrencyRates.length; i++) {
        if (agentData.agents[0].CurrencyRates[i].FromCode == baseCode && agentData.agents[0].CurrencyRates[i].ToCode == selectedCurrency)
            prm += "&cursel=" + selectedCurrency + "&rt=" + agentData.agents[0].CurrencyRates[i].Rate;
    }           
    return prm;
}


function SelectCountries(cty) {
    $("#spanCountries").buttonset();
    $("#spanCountries :radio[value='" + cty + "']").attr("checked", "checked")
    $("#spanCountries").buttonset("refresh");
}

function SelectLocation(pickup,dropoff) {
    $(".locPanel #pb").next().closest('input').val($("#pb option[value='" + pickup + "']").text());
    $(".locPanel #db").next().closest('input').val($("#bb option[value='" + dropoff + "']").text());
}

//https://support.microsoft.com/en-gb/kb/167820
function msieversion() {
    var ua = window.navigator.userAgent
    var msie = ua.indexOf("MSIE ")

    if (msie > 0)      // If Internet Explorer, return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))
    else                 // If another browser, return 0
        return 0

}
