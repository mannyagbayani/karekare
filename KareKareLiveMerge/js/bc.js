﻿//rev:mia april 16 2012
var displayCountry = false
var crossSellData;
var crossSellCountry = "";
var ddDatePicker, pdDatePicker;
var cache = {};
var urlquery;
   

var enableClientCache = false;
function getCurSymForCode(code) {
	var curSym = {
		AUD : "$",
		CAD : "$",
		CNY : "",
		EUR : "€",
		GBP : "£",
		HKD : "$",
		JPY : "¥",
		SGD : "$",
		ZAR : "",
		KRW : "",
		CHF : "",
		USD : "$",
		NZD : "$",
		INR : "Rs",
		FJD : "$",
		SBD : "$", 
		THB : "฿" 
	}; 
	var symbol = eval('curSym.' + code);
	if(symbol == null || typeof symbol == 'undefined')
		symbol = ""; 
	return symbol;
}

function getCurFullNameForCode(code) {
	var curSym = {
		NZD : "New Zealand Dollar",
		USD : "United States Dollar",
		GBP : "Pound Sterling",
		AUD : "Australian Dollar",
		EUR : "Euro",
		JPY : "Japanese Yen",
		CAD : "Canadian Dollar",
		CHF : "Swiss Franc",
		XPF : "C.F.P. Franc",
		DKK : "Danish Krone",
		FJD : "Fijian Dollar",
		HKD : "Hong Kong Dollar",
		INR : "Indian Rupee",
		NOK : "Norwegian Krone",
		PKR : "Pakistan Rupee",
		PGK : "Papua New Guinean Kina",
		PHP : "Philippine Peso",
		SGD : "Singapore Dollar",
		SBD : "Solomon Islands Dollar",
		ZAR : "South African Rand",
		LKR : "Sri Lankan Rupee",
		SEK : "Swedish Krona",
		THB : "Thai Baht",
		TOP : "Tongan Pa'anga",
		VUV : "Vanuatu Vatu",
		WST : "Western Samoan Tala"
	}; 
	var symbol = eval('curSym.' + code);
	if(symbol == null || typeof symbol == 'undefined')
		symbol = ""; 
	return symbol;
}

function init() {

    crossSellData = new CrossSellData();
    if(crossSellData != null) crossSellCountry = crossSellData.url.substring(5, 7)

	//check if an ajax call has been made before user left the page
	//init pick up and drop off date
	initDatePicker();
	//bind control from JSON objects
	bindJSONObjToCtrls();
	//register all the control handlers
	registerEventHandler();
	//push states for ajax callbacks 		
	initCacheStack();
	
	//init enhanced mode
	if(enhancedUI) { 
		$(".combobox").combobox('destroy');
		$(".combobox").combobox();
		$(".Radios").buttonset();
		$("body").addClass("enhancedUI");
    }
    
    $(".combobox").change(function () { $("#errorPanel span").html(''); });//clear err

    if ($.browser.webkit) { $(".Radios input.smart-radio-button").css('display', 'none'); } //chrome radio fix

    urlquery = getUrlVars();
    updateCtrlHeader()
}

function loadFromServer() {

    var url = $.param.fragment();

    if (url != '') {
        var requestParam = $.deparam.fragment();
        setBookingControlsFromParam(requestParam);
        if (requestParam.requestType == 1)
            submitAvailabilityRequest(requestParam);
        if (requestParam.requestType == 2)
            submitAltAvailabilityRequest(requestParam);
    }
    else 
            updateUI('init');
}
function loadClientCache(results){
	if($(results).find('xml').length>0){
		var requestParam = $.parseJSON($(results).find('xml').attr('B2BJSONParams'));				
		setBookingControlsFromParam(requestParam);						
		handleResults(results);			
		setRadioBtnValue('grsnett',requestParam.isGross=='true'?'gross':'nett', false);			
	}
	else{
		updateUI('init');
	}
}

function initCacheStack(){
  // a init state when no request has been made
  
  $(window).bind( 'hashchange', function(e) {
  	
    // Get the hash (fragment) as a string, with any leading # removed. Note that
    // in jQuery 1.4, you should use e.fragment instead of $.param.fragment().
   	var url = $.param.fragment();
   	
    if ( cache[ url ] && enableClientCache ) {
      // Since the element is already in the cache, it doesn't need to be
      // created, so instead of creating it again, let's just show it!
		loadClientCache(cache[url]);
    }
    else{
    	loadFromServer();
    }
  });
  
  // Since the event is only triggered when the hash changes, we need to trigger
  // the event now, to handle the hash the page may have loaded with.
  
  $(window).trigger( 'hashchange' );	
}

function initDatePicker() {
	var targetFrom = $('.fromDate').attr('id');
	pdDatePicker = $('#' + targetFrom).datepicker({
		disabled : false,
		dateFormat : 'dd-mm-yy',
		//beforeShowDay: function(){} ,
		numberOfMonths : 1,
		changeMonth : true,
		changeYear : true,
		minDate : new Date(),
		onSelect: function (dateText, inst) { 
            setDateFromDatePicker($('#from-date'), dateText, inst);
		    
        },
		onChangeMonthYear : function(year, month, inst) {
			changeMonthYear($(this), year, month, inst, $('#from-date'));		    
        }
	});
	ddDatePicker = $("#toDateDiv").datepicker({
		dateFormat : 'dd-mm-yy',
		numberOfMonths : 1,
		minDate : new Date(),
		changeMonth : true,
		changeYear : true,
		onSelect : function(dateText, inst) { setDateFromDatePicker($('#to-date'), dateText, inst);
		},
		onChangeMonthYear : function(year, month, inst) {
			changeMonthYear($(this), year, month, inst, $('#to-date'));
		}
	});
}

function changeMonthYear(calendar, year, month, inst, hiddenObj) {
	var oldDate = calendar.datepicker("getDate");
	if(oldDate != null) {
		var newDate = new Date(year, month - 1, oldDate.getDate());
		if(newDate.getDate() != oldDate.getDate()) {
			var lastDay = 32 - new Date(year, month - 1, 32).getDate();
			newDate = new Date(year, month - 1, lastDay);
		}
		calendar.datepicker("setDate", newDate);
		setDateFromDatePicker(hiddenObj, inst.input.val(), inst);
	}
}

function setDateFromDatePicker(hiddenObj, dateText, inst) {
	$(hiddenObj).val(dateText);
	setDate(inst);
	updateCtrlHeader();
	$('#errorPanel span').html('');
}

function setBookingControlsFromParam(param){
	setRadioBtnValue('cc',param.cc, true);
	setRadioBtnValue('brand',param.brand, true);
	setRadioBtnValue('vt',param.vt, true);
	setRadioBtnValue('grsnett',(param.isGross=='true'?'gross':'nett'),false);
	pdDatePicker.datepicker("setDate",param.pd);
	ddDatePicker.datepicker("setDate",param.dd);
	try{
		$('#vehicleDD').val(param.model);
		$('#agentsDD').val(param.ac);
		$('#packagesDD').val(param.pc);	
		$('#pb').val(param.pb);
		$('#db').val(param.db);
		$('#pt').val(param.pt);
		$('#dt').val(param.dt);
		$('#na').val(param.na);
		$('#nc').val(param.nc);
		$('#countriesDD').val(param.ctyResidence);
	}
	catch(e){
		setTimeout(function(){	
			$('#vehicleDD').val(param.model);
			$('#agentsDD').val(param.ac);
			$('#packagesDD').val(param.pc);					
			$('#pb').val(param.pb);
			$('#db').val(param.db);
			$('#pt').val(param.pt);
			$('#dt').val(param.dt);
			$('#na').val(param.na);
			$('#nc').val(param.nc);
		},100);
	}
	if(enhancedUI){
		$(".combobox").combobox('destroy');
		$(".combobox").combobox();	
	}
	updateCtrlHeader();
}

function bindJSONObjToCtrls() {

    //rev:mia 23june2014 b2bmac
    var b2ball = crossSellData.iscrosssell; 

    var selectedType = getRadioBtnSelValue("vt");
    var currentBrand = getRadioBtnSelValue("brand");

    //rev:mia 23june2014 b2bmac
    if (b2ball && crossSellData.url != "") {
        currentBrand = "all";
        $('input[name="cc"]:checked').val(crossSellCountry)
    }

    //bind countries, package code and vehiclde codes
    //debugger;
    $.each(ctrlParams.countries, function () {
        var cc = this.code;
        //changed here to set the default value based on client side
        if (this.code == $('input[name=cc]:checked').val()) {

            var locCodes = [];

            $.each(this.locations, function () {
                var unique = false;
                if ($.inArray(this.code, locCodes) == -1) {
                    locCodes.push(this.code);
                    unique = true;
                }
                //$.inArray(value, array)
                if ((this.Type == "Car" && selectedType == "ac") || (this.Type == "Campervan" && selectedType == "av") || this.Type == "Both") {

                    //rev:mia 23june2014 b2bmac
                    if ((currentBrand == this.BrandStr && unique) || b2ball ) {
                        $('#pb,#db').append($('<option>', {
                            value: this.code
                        }).text(this.name));
                    }
                }
            });
            $.each(this.brands, function () {//brands
                if (this.selected) {
                    var loadedVType = (getRadioBtnSelValue('vt'));

                    //rev:mia oct. 8 2013 - fixing problem when navigating back
                    if (this.vehicles !== null && this.vehicles !== 'undefined' && this.vehicles !== 'null') {
                        $.each(this.vehicles, function () {
                            if (loadedVType == "av" && this.type == 1 || loadedVType == "ac" && this.type == 0)
                                $('#vehicleDD').append($('<option>', {
                                    value: this.code
                                }).text(this.code.substring(this.code.indexOf('.') + 1, this.code.length) + " - " + this.name));
                        });
                    }


                   if (this.packages !== null && this.packages !== 'undefined' && this.packages !== 'null') {
                            $.each(this.packages, function () {
                                if (loadedVType.toString().toUpperCase() == this.vt)
                                    $('#packagesDD').append($('<option>', {
                                        value: this.code
                                    }).text(this.name));
                            });
                    }
                    
                }
            });
        }
    });

    //bind agent code drop down
	bindAgentCodes();

	//bind gross net options
	bindGrossNetOptions();

}

function bindAgentCodes() {
	var agentDD = $('#agentsDD');
	$.each(agentData.agents, function() {
		agentDD.append($('<option>', {
			value : this.ID
		}).text(this.Name));
	});
	//Default Agent Code
	try{
		agentDD.val(agentData.DefaultAgentCode);
	}
	catch(e){
		//workaround ie6 issue with programatically select value on dynamically binded options
		setTimeout(function(){agentDD.val(agentData.DefaultAgentCode)},100);
	}
}

function bindGrossNetOptions() {
	if(agentData.HasGrossNetOptions){	
		if(agentData.GrossDefaultOn)
			$('#gnGross').attr("checked", "checked");
		else
			$('#gnNett').attr("checked", "checked");
		//G,N
		if(agentData.GrossNetOptions.indexOf('G') < 0 || agentData.GrossNetOptions.indexOf('N') < 0) {
			$('#gnNett').attr('disabled', 'disabled')
			$('#gnGross').attr("disabled", "disabled");
		}
	}
	else{
		$('#availabilityFilter').addClass('nogrossnetoption');
	}
}

function getRadioBtnSelValue(rName) {

    if (rName == 'brand')
        return $('#brandSelector').val();
    
    return ($('input[name=' + rName + ']:checked').val());
}

function setRadioBtnValue(rName,val, triggerEvent){

    if (rName == 'brand') {
        $('#brandSelector').val(val);
        $('#brandSelector').trigger('change');
        return; //swapped to dropdown, rh7
    }
    
    var radioInput = $('input[name='+rName+'][value='+val+']');
	var radioGroup = $('input[name='+rName+']');
	if(triggerEvent){
		radioInput.trigger('click');
		radioGroup.trigger('change');
	}
	else{
		radioInput.trigger('click');
	}
}

function initPopUp(elm) {
	var elm = $(elm);
	var parentElm = elm.parent()[0];
	while(parentElm.tagName != "DIV")
	parentElm = $(parentElm).parent()[0];
	if(!elm.hasClass('Loaded')) {
		elm.addClass('Loaded');
		elm.tooltip({
			track : true,
			delay : 0,
			showURL : false,
			showBody : " - ",
			extraClass : "bubble",
			fixPNG : true,
			opacity : 0.95,
			left : -120,
			bodyHandler : function() {
				return $("<div class='bubbleBody'>" + $(".DiscountedDaysPopUp", parentElm).html() + "</div>");
			}
		});
		elm.trigger('mouseover');
	}
}

function initToolTip(elm, extraClass, bodyContent){
	elm.tooltip({
		track : true,
		delay : 0,
		showURL : false,
		showBody : " - ",
		extraClass : extraClass,
		fixPNG : true,
		opacity : 0.95,
		left : 0,
		bodyHandler : function() {
			return $("<div class='bubbleBody'>" + bodyContent + "</div>");
		}
	});
}
//load currency list
function bindCurrencyList() {
	if(agentData.HasCurrencyOption) {
		//clear previous insertion
		$('#currencySelector').empty();
		//insert the default country of travel currency
		$('#currencySelector').append($('<option>', {
			value : 1.0,
			rel : $("input[name=cc]:checked").attr('rel')
		}).text(getCurFullNameForCode($("input[name=cc]:checked").attr('rel')) + ' ('+$("input[name=cc]:checked").attr('rel')+')'));

		$('#currencySelector').val($('#currencySelector option:first').val());

		//bind the currency list based on the selected agent code
		jQuery.each(agentData.agents, function() {
			if(this.ID == $("#agentsDD option:selected").val()) {
				if(this.CurrencyRates != null) {
					var defaultCurrency = this.DefaultCurrency;
					jQuery.each(this.CurrencyRates, function() {
						if(this.FromCode == $("input[name=cc]:checked").attr('rel')) {
							$('#currencySelector').append($('<option>', {
								value : this.Rate,
								rel : this.ToCode,
								selected: (defaultCurrency == this.ToCode)
							}).text( getCurFullNameForCode(this.ToCode) + ' ('+ this.ToCode+')'));
						}
					});
					return false;
				}
			}
		});
		
		bindCurrencyDD();
				
		if($.deparam.fragment().selCurrency){
			try{
				$('#currencySelector option[rel='+$.deparam.fragment().selCurrency+']').attr('selected',1);
			}catch(e){
				setTimeout(function(){	$('#currencySelector option[rel='+$.deparam.fragment().selCurrency+']').attr('selected',1); },100);
			}
			$('#currencySelector').trigger('change');
		}
		
		if(enhancedUI)
			refreshCombobox([$('#currencySelector')]);
	}
	else{
        $('.curSelector').hide();
	}
}


function bindCurrencyDD() {
    //bind events and display drop downlist
    if ($('#currencySelector option').length > 1) {
        $('#currencySelector').change(function () {
            var selectedCur = $("#currencySelector option:selected").attr('rel');
            var selectorRate = $("#currencySelector option:selected").val();
            jQuery.each($('a.SelectBtn,.aAVail, .AltSelect'), function () {

                //var incLinkParam = ($(this).attr('href').indexOf('&inc=1') > 0 ? "&inc=1" : "&inc=0");
                //incLinkParam = $(this).attr('href').split('&')[0].indexOf("inc=") > 0 ? "" : incLinkParam;

                //debugger
                if ($(this).attr('href').indexOf('cur') > 0) {
                    $(this).attr('href', $(this).attr('href').split('&')[0] + "&cur=" + selectedCur + "&rate=" + selectorRate);
                }
                else//first timer
                    $(this).attr('href', $(this).attr('href') + "&cur=" + selectedCur + "&rate=" + selectorRate );
            });
            calculatePrice();
        });
        $('#currencySelector').trigger('change');
        $('.curSelector').show();
    } else
        $('.curSelector').hide();
}

function registerEventHandler() {
    //radio button event registration
    //debugger;
    

    var _cc;
    var _brand;
    $('#leftPanel .Radios input, #brandSelector').change(function () {
        //rev:mia 23june2014 b2bmac
        var b2ball = crossSellData.iscrosssell;
        var cc = $('input[name="cc"]:checked').val()

        if ((!b2ball && crossSellData.url == "") || (cc == "US" || cc == "NZ" || cc == "AU")) {


            updateAgentFields(getRadioBtnSelValue('cc'), getRadioBtnSelValue('brand'));
            _cc = getRadioBtnSelValue('cc');
            _brand = getRadioBtnSelValue('brand');
            if (_cc == "US")
                _brand = "Z";
            else {
                if (_brand == "Z") {
                    _brand = oldBrand;
                }
            }
            getAgentData($('#agentsDD option:selected').val(), _cc, _brand, 0);
        }
        else {
            _cc = crossSellCountry;
            if (_cc != "") {
                updateAgentFields(_cc, "all");
                getAgentData($('#agentsDD option:selected').val(), _cc, "all", 0);
            }
            hideSameDayMessage();
        }

        // getAgentData($('#agentsDD option:selected').val(), getRadioBtnSelValue('cc'), getRadioBtnSelValue('brand'), 0);

        //retain locations
        if (state == null) state = getCurrentSelection();
        if (state != null) {

            if (state.pb == undefined && state.db == undefined) {
                $('#pb').val(-1);
                $('#db').val(-1);
            }
            else {
                $('#pb').val(state.pb);
                $('#db').val(state.db != 0 ? state.db : state.pb);
            }
            if (enhancedUI)
                refreshCombobox([$('#pb'), $('#db')]);
        }

    });
	//pick up location, drop of location event registration
    $('#pb,#db').change(function () {
        updateCtrlHeader();
        //reset the drop off location to default when pickup is selected to default and it's on same as pick up option
        if ($(this).is('#pb') && $('#pb option:selected').val() == -1 && $('#db option[value="0"]').length == 1) {
            $('#db').val(-1);
        }
        if ($('#pb option:selected').val() != -1 && $('#db option:selected').val() == -1) {
            if ($('#db option[value="0"]').length == 0) {
                $('#db').append($('<option>', {
                    value: '0'
                }).text('Same as pick up'));
            }
            $('#db').val(0);
            $('#db').trigger('change');
        }
        $(this).removeClass('error');
        $(eval("'." + $(this).attr('id') + "'")).removeClass('expand');
        if (enhancedUI) {
            refreshCombobox([$('#pb'), $('#db')]);
        }

        if (state == null) state = { "pb": $('#pb').val(), "db": $('#db').val() };//save locations
        else { state.pb = $('#pb').val(); state.db = $('#db').val(); };
    });

    //-- ie9 tweak
    $('.spanCountries').change(function () {
        $('.spanCountries input').css('background-color', '#fff');
    });
    //--

	//gross net option on change
	if(agentData.HasGrossNetOptions && agentData.GrossNetOptions.length>1){
		$('#availabilityFilter input').change(function() {
			$('#catalogContainer').html('<div id="loaderContainer"><span class="loader"></span></div>');
			var requestParam = $.deparam.fragment();
			requestParam.isGross = (getRadioBtnSelValue("grsnett") == "gross");		
			requestParam.selCurrency = $('#currencySelector option:selected').attr('rel');
			pushState(requestParam);
		});
	}
	$('#ctrlHead').click(function() {
		if(!$(this).is('.disabled'))
		    $('#ctrlBody').toggleClass('collapse');
		$('#ctrlHead big em').text( $('#ctrlBody').hasClass('collapse') ?  "(click to expand)" : "(click to hide)" )
           
	});

	$('#vehicleDD').change(function() {
		$('#na').val(1);
		$('#nc').val(0);
		if(enhancedUI) {
			refreshCombobox([$('#na'), $('#nc')]);
		}
	});
	$('#getAvailSubmit').click(function () {
	    //debugger;
	    var requestParam = getAvailabilityRequestParam();

	    if ($('#brandSelector').val() != "all") {
	        b2bParamUrl = "";
	        crossSellData = new CrossSellData(); //start with a fresh copy
	    }

	    if ((crossSellData != null && crossSellData.url != "") || (crossSellData != null || requestParam.brand != "all")) {
	        if (crossSellData.url != "" && crossSellData.iscrosssell) {
	            if (CrossSellDataHaveChanged(requestParam) == true) {
	                b2bParamUrl = ""
	                crossSellData = new CrossSellData(); //start with a fresh copy
	            }
	        }
	        else if (CrossSellDataHaveChanged(requestParam) == true) {
	            b2bParamUrl = ""
	            crossSellData = new CrossSellData(); //start with a fresh copy
	        }
	    }

	    if (requestParam == null) return;
	    //added for cross sell support

	    if ($('#brandSelector').val() == "all") {
	        $("#catalogContainerClone").css('display', 'visible').addClass("loading")
	        updateCtrlHeader();
	        submitCrossSell();
	        return;
	    } else {
	        $("#catalogContainerClone").css('display', 'block')
	        csellFrame.postMessage("about:blank", "*")
	        $('#availabilityFilter').css('display', 'block')
	    };
	    //cross sell ends
	    requestParam.isGross = agentData.GrossDefaultOn;
	    pushState(requestParam);
	});

	$('#agentsDD').change(function () {
	    //debugger;
	    var brand = $('input[name=brand]:checked').attr('value');
	    var cc = $('input[name=cc]:checked').attr('value');
	    getAgentData($(this).val(), cc, brand,1);
	});
}

//***************************
//helper methods
//***************************
function calculatePrice(countryOfTravelCurrency, selectedCurrency, selectedExRate) {

    var altPrice;
    var altCurCountry;
    var altCurSymbol;
    var altRate;
    var dispPrice;
    var curCountry;
    var curSymbol;    
    if(selectedCurrency==null)
    	selectedCurrency = $('#currencySelector option:selected').attr('rel')==null?$('input[name=cc]:checked').attr('rel'): $('#currencySelector option:selected').attr('rel');    
	if(selectedExRate==null)
    	selectedExRate = $('#currencySelector option:selected').val() == null ? 1 : $('#currencySelector option:selected').val();
	if(countryOfTravelCurrency==null) 
    	countryOfTravelCurrency = $('input[name=cc]:checked').attr('rel');
    	    
    $('.exRate').each(function () {
        if ($(this).is('.pap')) {
            //$(this).parentsUntil('.chargesTable').find('.total').remove();			
            //$(this).parent('tr').find('.subtotal').addClass('hide');
            //altCurCountry = $('#currencySelector option:selected').attr('rel')==null?$('input[name=cc]:checked').attr('rel'): $('#currencySelector option:selected').attr('rel');
            altCurCountry = selectedCurrency;
            altPrice = parseFloat($(this).attr('rel')) * parseFloat(selectedExRate);
            curCountry = countryOfTravelCurrency;
            dispPrice = parseFloat($(this).attr('rel'));
        } else {
            altCurCountry = countryOfTravelCurrency;
            altPrice = parseFloat($(this).attr('rel'));
            curCountry = selectedCurrency;
            dispPrice = parseFloat($(this).attr('rel')) * parseFloat(selectedExRate);
        }
        curSymbol = getCurSymForCode(curCountry);        
        altCurSymbol = getCurSymForCode(altCurCountry);
          
        $(this).append($('<span class="altRate">' + altCurCountry + ' ' + altCurSymbol + formatCurrency(altPrice.toFixed(2)) + '</span>'));
        initToolTip($(this), "altCurrencyToolTip", $(this).find('.altRate').text());
        $(this).text(curCountry + ' ' + curSymbol + formatCurrency(dispPrice.toFixed(2)));
    });
}

//string manipulation to get the currency in commar separated format
function formatCurrency(amount) {
	amount = amount.toString();
	var delimiter = ",";
	var a = amount.split('.', 2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) {
		return '';
	}
	var minus = '';
	if(i < 0) {
		minus = '-';
	}
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3) {
		var nn = n.substr(n.length - 3);
		a.unshift(nn);
		n = n.substr(0, n.length - 3);
	}
	if(n.length > 0) {
		a.unshift(n);
	}
	n = a.join(delimiter);
	if(d.length < 1) {
		amount = n;
	} else {
		amount = n + '.' + d;
	}
	amount = minus + amount;
	return amount;
}

//This is B2B specific control manipulations
function updateAgentFields() {
    //rev:mia 23june2014 b2bmac
    var b2ball = crossSellData.iscrosssell;
    var cc;
    var brand = getRadioBtnSelValue('brand');
    var selectedType = getRadioBtnSelValue("vt");

    if (b2ball && crossSellData.url != "" ) {
        cc = crossSellCountry; 
        brand = "all";
    }
    else {
        cc = getRadioBtnSelValue('cc');
        brand = getRadioBtnSelValue('brand');
    }

    //rev:mia- aug.28, 2013 - addition of roadbear and US britz
    if (cc == "US") {
        if (brand != 'Z' && brand != 'R') brand = 'Z'
    }
    else {
        if (((cc == "NZ" || cc == "AU") && brand == "Z")) {
            brand = "M"
        }
    }

	updateOpeningHours(cc, selectedType);
    
    //-- kea nz fix
	if (cc != "AU") {
	    $(".brandQ").css('display', 'none');
	    $("#brandQ").css('display', 'none');
	}
	else {
	    $(".brandQ").css('display', 'block');
	    $("#brandQ").css('display', 'block');
	}
    //--

    $('.locSelector').empty();

    $.each(ctrlParams.countries, function () {
        var cc = this.code;
        //changed here to set the default value based on client side
        if (this.code == $('input[name=cc]:checked').val()) {

            $('#pb, #db').html("<option value='-1'>Please Select</option>"); //added

            //rev:mia 23june2014 b2bmac
            var csMode = ($('#brandSelector').val() == "all"); //added from crosssell
            if (csMode == false && b2ball) {
                csMode = b2ball;
            }
            var locCodes = [];
            $.each(this.locations, function () {
                var unique = false;
                if ($.inArray(this.code, locCodes) == -1) {
                    locCodes.push(this.code);
                    unique = true;
                }
                if ((this.Type == "Car" && selectedType == "ac") || (this.Type == "Campervan" && selectedType == "av") || this.Type == "Both" && unique)
                    if ((this.BrandStr == brand) || (csMode && unique)) {
                        $('#pb,#db').append($('<option>', {
                            value: this.code
                        }).text(this.name));
                    }
            });
        }
    });
	$('#packagesDD').html("<option value='0'>All Packages</option>");

	for(var i = 0; i < ctrlParams.countries.length; i++) {
	    if (ctrlParams.countries[i].code == cc) {//country match
	        //debugger;
			for(var j = 0; j < ctrlParams.countries[i].brands.length; j++)
			if(ctrlParams.countries[i].brands[j].code == brand) {
				var vType = getRadioBtnSelValue("vt");
				$.each(ctrlParams.countries[i].brands[j].packages, function() {
					if(vType.toString().toUpperCase() == this.vt)
						$('#packagesDD').append($('<option>', {
							value : this.code
						}).text(this.name));
				});
				$('#vehicleDD').html("<option value='0'>All Models</option>");                
                
                if(ctrlParams.countries[i].brands[j].vehicles != null)//added for no service on country, rh7
                $.each(ctrlParams.countries[i].brands[j].vehicles, function() {
					var vType = getRadioBtnSelValue("vt");
					if((vType == "ac" && this.type == 0) || (vType == "av" && this.type == 1))
						$('#vehicleDD').append($('<option>', {
							value : this.code
						}).text(this.code.substring(this.code.indexOf('.')+1,this.code.length) + " - " + this.name));
				});
			}
		}
	}
	//get packages for selected brand
	updateCtrlHeader();
	if(enhancedUI) {
		refreshCombobox([$('.locSelector'), $('#vehicleDD'), $('#packagesDD')]);
	}
	//get packages for selected brand
}

function refreshCombobox(elements) {
	$.each(elements, function() {
		$(this).combobox("destroy");
		$(this).combobox();
	});
}

function setDate(inst) {
	if(inst.id == "toDateDiv") {
		$('#dd').val(inst.selectedDay);
		$('#dm').val(inst.selectedMonth + 1);
		$('#dy').val(inst.selectedYear);
		toDateSelected = true;
	} else {//from date changed
		$('#pd').val(inst.selectedDay);
		$('#pm').val(inst.selectedMonth + 1);
		$('#py').val(inst.selectedYear);
		var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
		var nextDay = new Date(toDate.getTime() + 1 * 86400000);
		if($("#fromDateDiv").datepicker("getDate") > $("#toDateDiv").datepicker("getDate")) {
			$('#dd').val(nextDay.getDate());
			$('#dm').val(nextDay.getMonth() + 1);
			$('#dy').val(nextDay.getFullYear());
			$('#toDateDiv').datepicker("setDate", new Date(nextDay.getFullYear(), nextDay.getMonth(), nextDay.getDate()));
		}
		$('#toDateDiv').datepicker("option", "minDate", nextDay);
	}
}

function getBookingTitle() {
	var title = ""
	if($('#pb').val() != '-1')
		title = "Pick Up " + $('#pb').val();
	return title;
}

var fParams = "";

function updateCtrlHeader() {

    if (urlquery == undefined) {
        urlquery = getUrlVars();
    }
    var db = urlquery['db'];
	var headerTxt = "<big>" + $("#brandSelector option:selected").text() + " " + getRadioBtnSelValue('cc') + ", " + $('input[name=vt]:checked').attr('rel') + ", " + $('#vehicleDD :selected').text() + ", "+ $("#agentsDD option:selected").text()  +"</big>";
	if ($("#pb option:selected").val() != '-1' && $("#db option:selected").val() != '-1')
	    headerTxt += "<small>" + $("#pb option:selected").text() + " <em>to</em> " + $("#db option:selected").text() + " | " + formatDate($('#fromDateDiv')) + " <em>to</em> " + formatDate($("#toDateDiv")) + " | <span class='changeSearchQuery'>change your search</span></small>";
	else if (db != undefined) {
	    var pb = window.location.href.substr(window.location.href.indexOf("#pb=") + 4, window.location.href.substr(window.location.href.indexOf("#pb=") + 4).indexOf("&"));
	    headerTxt += "<small>" + $("#pb option[value='" + pb + "']").text() + " <em>to</em> " + $("#db option[value='" + db + "']").text() + " | " + formatDate($('#fromDateDiv')) + " <em>to</em> " + formatDate($("#toDateDiv")) + " | <span class='changeSearchQuery'>change your search</span></small>";
	}
	else
	    headerTxt = "";
	//TODO: define default
	$('#ctrlHead span.bookingData').html(headerTxt);
}




function formatDate(pickerObj) {
	var day1 = pickerObj.datepicker('getDate').getDate();
	var month1 = pickerObj.datepicker('getDate').getMonth();
	month1 = monthNames[month1];

	var year1 = pickerObj.datepicker('getDate').getFullYear();
	var fullDate = dayOfWeek[pickerObj.datepicker('getDate').getUTCDay()] + " " + day1 + " " + month1 + " " + year1;
	//return fullDate;
	var options = {
	    weekday: "short", year: "numeric", month: "short",
	    day: "numeric"
	};
	/*Added by Nimesh on 25th Mar 2015 and returning new retDate instead of fullDate*/
	//var testDate = pickerObj.datepicker('getDate').toLocaleDateString("en-NZ", options);
	var retDate = dateFormatNew(pickerObj.datepicker('getDate'));
	
    //return fullDate;
	return retDate;
}
function dateFormatNew(d) {
    var month = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    ],
      weekday = [
        'Sun',
        'Mon',
        'Tue',
        'Wed',
        'Thu',
        'Fri',
        'Sat'
      ];

    //return weekday[d.getDay()] + ", " +
    //  month[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
    return weekday[d.getDay()] + " " + d.getDate() + " " + month[d.getMonth()] + " " +   d.getFullYear();
}

function validateInputs(pickupLoc, dropoffLoc, pickupDate, dropoffDate, countryofRes) {
    var b2ball = crossSellData.iscrosssell;
    //debugger;
    //TODO:validate userinputs
	var valid = true;
	if(pickupLoc == -1) {
		$('#pdPanel input').css('background-color', '#FFE45C');//ie9 tweak
        return false;
	}
	if(dropoffLoc == -1) {
		$('#ddPanel input').css('background-color', '#FFE45C');//ie9 tweak
		return false;
    }

    var brandselector = $("#brandSelector").val();
    if (brandselector == "all") {
        if (countryofRes == 0) {
            $('#b2bPanel .spanCountries input').css('background-color', '#FFE45C'); //ie9 tweak
            valid = false;
        }
        else return true;
    }
    //rev:mia April 16 2012
    //if (displayCountry == true && !crossSellData.iscrosssell && crossSellData.url == "") {
    if (displayCountry == true) {
         if (countryofRes == 0) {
             $('#b2bPanel .spanCountries input').css('background-color', '#FFE45C');//ie9 tweak
             valid = false;
         }
     }
     else
         valid = true;

    return valid;
}

function NewWindow(mypage, myname, w, h, scroll, pos) {
	if(pos == "random") {
		LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
		TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
	}
	if(pos == "center") {
		LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
		TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
	} else if((pos != "center" && pos != "random") || pos == null) {
		LeftPosition = 0;
		TopPosition = 20
	}
	settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=1,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes';
	//alert(myname);
    win = window.open(mypage, myname, settings);
	win.focus();
}

//********************************************************
// ajax call - form submission
//********************************************************
function getAvailabilityRequestParam(){
	var country = $("input:[name='cc']:checked").val();
	var brand = $('#brandSelector').val();  //$("input:[name='brand']:checked").val();
	var vehicleType = $("input:[name='vt']:checked").val();
	var vehicleModel = $("#vehicleDD option:selected").val() == 0 ? '' : $("#vehicleDD option:selected").val();
	var agentCode = $("#agentsDD option:selected").val();
	var promoCode = $("#packagesDD option:selected").val() == 0 ? '' : $("#packagesDD option:selected").val();
	var pickupLoc = $("#pb option:selected").val();
	var dropOffLoc = $("#db option:selected").val() == 0 ? pickupLoc : $("#db option:selected").val();
	var pickupDate = $("#fromDateDiv").datepicker('getDate').getDate() + "-" + ($("#fromDateDiv").datepicker('getDate').getMonth() + 1) + "-" + $("#fromDateDiv").datepicker('getDate').getFullYear();
	var dropOffDate = $("#toDateDiv").datepicker('getDate').getDate() + "-" + ($("#toDateDiv").datepicker('getDate').getMonth() + 1) + "-" + $("#toDateDiv").datepicker('getDate').getFullYear();
	var pickupTime = $("#pt option:selected").val();
	var dropOffTime = $("#dt option:selected").val();
	var adults = $("#na option:selected").val();
	var children = $("#nc option:selected").val();

	//rev:mia april 16 2012
	var countryofRes = $("#countriesDD option:selected").val();

	if (validateInputs(pickupLoc, dropOffLoc, pickupDate, dropOffDate, countryofRes)) {
		B2BJSONParams = {
			pb : pickupLoc,
			db : dropOffLoc,
			pd : pickupDate,
			dd : dropOffDate,
			pt : pickupTime,
			dt : dropOffTime,
			na : adults,
			nc : children,
			cc : country,
			brand : brand,
			vt : vehicleType,
			model : vehicleModel,
			ac : agentCode,
			pc : promoCode,
			isGross : agentData.GrossDefaultOn,
			requestTimeInMil : Date.parse(new Date()),
			requestType: 1
			//rev:mia april 16 2012
            , ctyResidence: countryofRes

        };
        return B2BJSONParams;
	}
	return null;
}

var reqTime;//added for analytics

function submitAvailabilityRequest(B2BJSONParams) {

    if (!getPackageValidity(B2BJSONParams.cc, B2BJSONParams.pc, B2BJSONParams.pd, B2BJSONParams.dd)) 
        return;
    

    if(B2BJSONParams != null) {
		//"{'pb':'AKL', 'db':'AKL', 'pd' : '01-12-2011', 'dd':'15-12-2011', 'pt':'10:00', 'dt':'15:00', 'na':2, 'nc':3, 'cc':'NZ', 'brand':'b', 'vt':'av', 'model':'all', 'ac' :'ARARAT', 'pc':'FLEXB',isGross: true    }";
        var options = {
            url: '/Content/Availability.aspx/GetAvailabilityItems',
            data: "{ 'B2BJSONParams':'" + JSONStringify(B2BJSONParams) + "'}",
            contentType: 'application/json; charset=utf-8',
            dataType: 'xml',
            type: 'post',
            beforeSend: function () {
                updateCtrlHeader();
                updateUI("beforeSubmitAvail");
                reqTime = +new Date();
            },
            success: function (xml) {
                handleResults(xml);
                reqTime = (+new Date() - reqTime)/1000;
                _gaq.push(['_trackEvent', 'AvailabilityResponse', agentData.DefaultAgentCode, reqTime.toString()]);
            },
            error: function (XMLHttpRequest, textStatus, error) {
                handleError(XMLHttpRequest, error);                
            },
            timeout: function () {
                updateUI("timeout");
                _gaq.push(['_trackEvent', 'AvailabilityResponse', agentData.DefaultAgentCode, "TimeOut"]);
            }
        };
		var xhr = $.ajax(options);
		$('.cancelRequest').click(function(e){
			e.preventDefault();
			xhr.abort();
		});
	}
}

function submitAltAvailabilityRequest(B2BJSONParams){
	if(B2BJSONParams != null){
			var options = {
			url : '/Content/Availability.aspx/GetAltAvailabilityItems',
			data : "{ 'B2BJSONParams':'" + JSONStringify(B2BJSONParams) + "'}",
			contentType : 'application/json; charset=utf-8',
			dataType : 'xml',
			type : 'post',
			beforeSend : function() {
				updateUI("beforeSubmitAltAvail");	
			},
			success : function(xml) {
				//setBookingControlsFromParam($.parseJSON($(xml).find('xml').attr('B2BJSONParams')));
				handleResults(xml);
				updateUI("afterSubmitAltAvail");
			},
			error : function(XMLHttpRequest, textStatus, error) {
				handleError(XMLHttpRequest, error);
			}, 
			abort: function(){
				updateUI("abortSubmit");	
			},
			timeout : function() {
				updateUI("timeout");
			}
		};
		var xhr = $.ajax(options);
		$('.cancelRequest').click(function(e){
			e.preventDefault();
			xhr.abort();
		});
	}
}

//helper function to turn json object into string
//this function not available in jquery
//and JSON object is not supported in ie7
function JSONStringify(obj) {
	var t = typeof (obj);
	if(t != "object" || obj === null) {
		// simple data type
		if(t == "string")
			obj = '"' + obj + '"';
		return String(obj);
	} else {
		// recurse array or object
		var n, v, json = [], arr = (obj && obj.constructor == Array);
		for(n in obj) {
			v = obj[n];
			t = typeof (v);
			if(t == "string")
				v = '"' + v + '"';
			else if(t == "object" && v !== null)
				v = JSON.stringify(v);
			json.push(( arr ? "" : '"' + n + '":') + String(v));
		}
		return ( arr ? "[" : "{") + String(json) + ( arr ? "]" : "}");
	}
}

function handleResults(results) {
	//set the state to be the current result xml
	var resultXml = $(results).find('xml');
	if(resultXml.length > 0 && resultXml.attr('count') > 0) {
		//append results
		$('#catalogContainer').html(resultXml.text());
		//register events
		registerSearchResultsEventHandler(resultXml);
		//bind currency list
		bindCurrencyList();
		//calculate price based on exchange rate
		calculatePrice();		
		//ui update
		updateUI("afterSubmitWithSuccess");
		
        if(enableClientCache)
		    saveState(results);
		
		if(resultXml.attr('hasnogross') == 'True')
		    $('#availabilityFilter .Radios').after('<span class="hasnogross">Price highlighted below is NETT only</span>');

		//if (typeof agentData.GrossNetOptions != 'undefined' && agentData.GrossNetOptions == "G")//gross only agent scenario, rh 120312
        //    $('#availabilityFilter .hasnogross').css('visibility', 'hidden');
            	
		
	} else {
		$('#catalogContainer').html('<div id="vehicleCatalog">Your search returned no results, please try again.</div>');
		updateUI("afterSubmitWithNoResult");
	}

}

function pushState(key){
	$.bbq.pushState(key);	
}

function saveState(results){
	var key = $.param.fragment();
	cache[key] = results;
}

function handleError(XMLHttpRequest, errorMsg) {
	if(XMLHttpRequest.status == 0){
		updateUI("afterSubmitWithAbort");
	}
	else if(XMLHttpRequest.status == 401){
		window.location = "/content/AccessDenied.aspx";
	}
	else{
		$('#catalogContainer').html("<div id='vehicleCatalog'>Sorry, there seems to be a problem with our system, please try again or contact the administrator for assistance, or try again <a href='/content/AgentLogin.aspx'>Here</a></div>");
		updateUI("afterSubmitWithError");
	}
}

function registerSearchResultsEventHandler($resultXml) {
	$(".ShowPriceList").click(function() {//bind expand
	    $(this).closest('.OptionRow').toggleClass("Collapse");
	    if ($(this).closest('.OptionRow').hasClass("Collapse"))
			$(this).text("Price Details");
		else
			$(this).text("Hide Details");
	});

    $(".TimeSpan").each(function() {
   		initToolTip($(this).find('.hireDates'),"altAvailToolTip", $(this).find('.popUpTxt').html());   		
    });
	
	$('.NoAlt').each(function(){
		initToolTip($(this).find('.details'),"noAltAvailToolTip", $(this).find('.popUpTxt').html());   		
	})
	$('.aAvail').click(function(e){
		e.preventDefault();
		if($(this).attr('rel') != null){
			$(this).parents('.OptionRow').addClass('loading');
			var brandVCode = $(this).attr('rel').split(',');
			var origionalRequest = $.parseJSON($resultXml.attr('B2BJSONParams'));
			var brand = brandVCode[0];
			var vCode = brandVCode[1];				
			origionalRequest.brand = brand;
			origionalRequest.model = vCode;
			origionalRequest.requestType = 2;
			pushState(origionalRequest);
		}
	});
	
	$("a.PopUp").click(function() {
		NewWindow($(this).attr('href'), 'vCode', ($(this).attr('rel') == 'm' ? '720' : '600'), '500', 'yes', 'center');
		return false;
	});
	
    $('#selectedVehiclePanel').click(function(){
    	if($(this).attr('rel')!=null)
    		NewWindow($(this).find('h3').attr('rel'),'vCode', ($(this).attr('rel') == 'm' ? '720' : '600'), '500', 'yes', 'center')
    })
    $('.SelectBtn, .AltSelect').click(function(){
    	$(this).addClass('loading');
    });
}

function updateUI(event) {
	if(event =='init'){
		$('#ctrlBody').removeClass("collapse");
		$('#priceAvailabilityContainer').removeClass("loaded loading");
		$('#catalogContainer').html('');
		$('#ctrlHead').removeClass('disabled');		
	}
	else{
		if(event.indexOf("before") > -1) {
			$('#priceAvailabilityContainer').removeClass('loaded noresults error').addClass('loading');
			$('#ctrlHead').removeClass('disabled');				
			switch(event){
				case "beforeSubmitAvail":
					$('#priceAvailabilityContainer').addClass('loadingAltAvail');
					break;	
				default:
					break;
			}
		} 
		else if(event =='abortSubmit')
			$('.loading').removeClass('.loading');
		else if(event =='afterSubmitAltAvail')
			$('#ctrlHead').addClass('disabled');
		else {
			$('#priceAvailabilityContainer').removeClass('loading').addClass('loaded');
			$('#ctrlBody').addClass("collapse");
			$('#availabilityFilter .hasnogross').remove();
			switch(event){
				case "afterSubmitWithNoResult":
					$('#priceAvailabilityContainer').addClass('noresults');
					break;
				case "afterSubmitWithError":
					$('#priceAvailabilityContainer').addClass('error');
					break;
				case "afterSubmitWithAbort":
					$('#priceAvailabilityContainer').removeClass('loaded')
					$('#ctrlBody').removeClass("collapse");
					break;	
				case "timeout":
					$('#priceAvailabilityContainer').removeClass('loaded')	
			}
		}
	}
}

//custom combobox
(function($) {
	$.widget("ui.combobox", {
		_create : function() {
			var self = this, select = this.element.hide(), selected = select.children(":selected"), value = selected.val() ? selected.text() : "";
			var input = this.input = $("<input>").insertAfter(select).val(value).autocomplete({
				delay : 0,
				minLength : 0,
				source : function(request, response) {
					var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
					response(select.children("option").map(function() {
						var text = $(this).text();
						if(this.value && (!request.term || matcher.test(text)))
							return {
								label : text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
								value : text,
								option : this
							};
					}));
				},
				select : function(event, ui) {
					ui.item.option.selected = true;
					self._trigger("selected", event, {
						item : ui.item.option
					});
					select.trigger("change");
				},
				change : function(event, ui) {
					if(!ui.item) {
						var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"), valid = false;
						select.children("option").each(function() {
							if($(this).text().match(matcher)) {
								this.selected = valid = true;
								return false;
							}
						});
						if(!valid) {
							// remove invalid value, as it didn't match anything
							$(this).val(select.children("option:first").text());
							select.val("");
							input.data("autocomplete").term = "";
							return false;
						}
					}
				}
			}).addClass("smart-combobox-input").addClass(this.element.attr('class'));
			input.data("autocomplete")._renderItem = function(ul, item) {
				ul.addClass(self.element.attr("class"));
				return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul);
			};

			this.button = $("<button type='button'>&nbsp;</button>").attr("tabIndex", -1).attr("title", "Show All Items").insertAfter(input).button({
				icons : {
					primary : "ui-icon-triangle-1-s"
				},
				text : false
			}).removeClass("ui-corner-all").addClass("smart-combobox-button").click(function() {
				// close if already visible
				if(input.autocomplete("widget").is(":visible")) {
					input.autocomplete("close");
					return;
				}

				// work around a bug (likely same cause as #5265)
				$(this).blur();

				// pass empty string as value to search for, displaying all results
				input.autocomplete("search", "");
				input.focus();
			});
		},
		destroy : function() {
			this.input.remove();
			this.button.remove();
			this.element.show();
			$.Widget.prototype.destroy.call(this);
		}
	});
})(jQuery);

//cookie
/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
jQuery.cookie = function (key, value, options) {

    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

var dayOfWeek = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

function enableEUI() {
    var allowEUI = false;
    var ua = $.browser;
    var ver = 0;
    if (ua.mozilla) {
        if(ua.version.slice(0, 3) == "1.9")// FF3.X
            allowEUI = false;
        else if (parseInt(ua.version, 10)> 3)//any other higher then 3
        {
            allowEUI = true;
        }           
    }
    if ($.browser.msie) {//IE7-
        //alert(parseInt(ua.version, 10));
        if(parseInt(ua.version,10) > 6)
            allowEUI = true;
    }
    if ($.browser.safari)
        allowEUI = true;
    return allowEUI;
}


function NewWindow(mypage, myname, w, h, scroll, pos) {
    if (pos == "random") { LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100; TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100; }
    if (pos == "center") { LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100; TopPosition = (screen.height) ? (screen.height - h) / 2 : 100; }
    else if ((pos != "center" && pos != "random") || pos == null) { LeftPosition = 0; TopPosition = 20 }
    settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=1,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes';
    win = window.open(mypage, myname, settings);
    win.focus();
}

//rev:mia april 16 2012
//display=1: to add package in the the package dropdown
function getAgentData(aCode, cc, brand, display) {
    var b2ball;
    //debugger;
    displayCountry = false;
    var options = {
        url: '/Content/Availability.aspx/GetAgentData',
        data: "{ 'agentCode':'" + aCode + "','cc':'" + cc + "', 'brand':'" + brand + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'post',
        beforeSend: function () { /* */ },
        success: function (json) {
            var items = eval(json.d);

            if (display == 1) {
                $('#packagesDD').empty();
                $('#packagesDD').append($('<option>', { value: "" }).text("All Packages"));
            }

            // debugger;
            $.each(items, function () {

                if (display == 1) {
                    $('#packagesDD').append($('<option>', {
                        value: this.code
                    }).text(this.name));
                }

                //alert(this.isDomestic)
                //if (this.isDomestic == "Yes" && !displayCountry) {
                if (this.isDomestic == "Yes") {
                    displayCountry = true;
                }
            });


            //rev:mia april 16 2012
            if (getRadioBtnSelValue('brand') != "all") {
                if (window.location.href.indexOf("b2bAll") != -1 && window.location.href.indexOf("&brand") == -1) {
                    //do nothing
                }
                else {
                    crossSellData.url = "";
                    crossSellData.iscrosssell = false;
                }
            }
            b2ball = crossSellData.iscrosssell;

            if (displayCountry == true && !b2ball && crossSellData.url == "") {
                $('span.spanCountries').show()
                $('span.sectionCountriesDD').addClass("errormsg countriesDD")
                //$('#countriesDD').val(0)
                //$("#countriesDD").next().closest('input').val($("#countriesDD option[value='0']").text())

            } else if (displayCountry) {
                $('span.spanCountries').show()
                $('span.sectionCountriesDD').addClass("errormsg countriesDD")
                //$('#countriesDD').val(0)
                //$("#countriesDD").next().closest('input').val($("#countriesDD option[value='0']").text())
            }
            else
            {
                $('span.spanCountries').hide()
                $('span.sectionCountriesDD').removeClass("errormsg countriesDD")
            }

            
            DisableIfBrandIsAll();

        },

        error: function (XMLHttpRequest, textStatus, error) {
            //XMLHttpRequest, error
            //alert("please try again");
        },
        timeout: function () {
            //alert("timeout");//TODO: 
        }
    };
    var xhr = $.ajax(options);
    return false;
}

//Configure
function initConfigure() {
    $("a.PopUp").click(function () {
        NewWindow($(this).attr('href'), 'vCode', '820', '500', 'yes', 'center');
        return false;
    });
}


//Configure


function ValidRange(start, end, pickDay, dropDay) {

    //alert("Valid Date params: start:" + start + " end: " + end + " pick: " + pickDay + " Drop: " + dropDay);
    return ((pickDay.getTime() >= start.getTime()) && (pickDay.getTime() <= end.getTime()) && (dropDay.getTime() >= start.getTime()) && (dropDay.getTime() <= end.getTime()));
}


function getPackageValidity(cc, selectedPackage, pUpDate, dropDate) {

    //alert(cc + "," + selectedPackage + "," + pUpDate + "," + dropDate);    
    //15-12-2011
    var pickUpParams = pUpDate.split('-');
    var pickUpDate = new Date(pickUpParams[2], pickUpParams[1]-1, pickUpParams[0]);
        
    var dropParams = dropDate.split('-');
    var dropDate = new Date(dropParams[2], dropParams[1] - 1, dropParams[0]);
        
    var validDate = true;
    var testStr = "";
     for(var i=0; i< ctrlParams.countries.length; i++)
     {
         if (ctrlParams.countries[i].code == cc) {
             for (var j = 0; j < ctrlParams.countries[i].brands.length; j++ ) {
                var selectedBrand = ctrlParams.countries[i].brands[j];
                for (var k = 0; k < selectedBrand.packages.length; k++) {
                    testStr += "{" + selectedBrand.packages[k].code + ":" + selectedBrand.packages[k].country  + "}";
                    if (selectedPackage == selectedBrand.packages[k].code) {//found package
                        var PackObj = selectedBrand.packages[k];
                        var stDayParams = PackObj.from.split("/");
                        var stDay = new Date(stDayParams[2], stDayParams[1]-1, stDayParams[0]);
                        var endDayParams = PackObj.to.split("/");
                        var endDay = new Date(endDayParams[2], endDayParams[1] - 1, endDayParams[0]);
                        validDate = ValidRange(stDay, endDay, pickUpDate, dropDate);
                        if (validDate == false) {
                            $('#errorPanel span').html("The promo code " + selectedPackage + " - is only valid for travel from " + stDay.toDateString() + " to " + endDay.toDateString());
                            $('#errorPanel').addClass('show');
                        }
                        return validDate;
                    }   
                } 
            }
        }
    }
    $('#errorPanel span').html('');
    $('#errorPanel').removeClass('show');
    return validDate;
}

function updateOpeningHours(cCode, vType) {
    cCode = cCode.toLowerCase();
    vType = (vType == "rv" || vType == "av") ? "av" : "ac";
    $('select.timeSelector').each(function () {
        var dd = $(this);
        for (i = 0; i < openHours.length; i++) {
            if (openHours[i].vt == vType && openHours[i].cc == cCode) {
                var html = '';
                for (j = openHours[i].pt; j <= openHours[i].dt; j++) {
                    if(!(j==7 && vType == "av"))
                        html += "<option value='" + (j < 10 ? "0" : "") + j + ":00' " + ((j == 10 && dd.attr('id') == "pt") || (j == 15 && dd.attr('id') == "dt")  ? "selected='selected'" : "") + "   >" + (j < 10 ? "0" : "") + j + ":00 " + (j < 12 ? "AM" : "PM") + "</option>";
                    html += "<option value='" + (j < 10 ? "0" : "") + j + ":30'>" + (j < 10 ? "0" : "") + j + ":30 " + (j < 12 ? "AM" : "PM") + "</option>";
                }
                dd.html(html);
                updateDefaultTimes(openHours[i].startTime, openHours[i].endTime);
                if (enhancedUI) refreshCombobox([dd]);

            }
        }
    })
}


function getCurrentSelection() {
    if ($.bbq.getState() != null) state = $.bbq.getState();    
    return state;
}
function updateDefaultTimes(startTime, endTime) {
    $('#pt > option[value="' + startTime + '"]').attr("selected", "selected");
    $('#dt > option[value="' + endTime + '"]').attr("selected", "selected");
}

function showSameDayMessage() {
    $('#sameDayMessage').show();
}

function hideSameDayMessage() {
    $('#sameDayMessage').hide();
}
//rev:mia 20June2014 - B2BMAC
function DisableIfBrandIsAll() {
    var brand = $("#brandSelector").val();
    var b2ball = crossSellData.iscrosssell;

    if (brand != "all") {
        $("#vehicleDD, #packagesDD").next().closest('input').removeAttr('disabled');
        $("#vehicleDD, #packagesDD").next().next().closest('button').removeAttr('disabled');
        $("#spanVehicleType").buttonset();
        $("#spanVehicleType #avRadio, #spanVehicleType #acRadio").removeAttr("disabled")
        $("#spanVehicleType").buttonset("refresh");
     
    }
    else {
        if (brand == "all" || (b2ball && crossSellData.url != "")) {
            $("#vehicleDD, #packagesDD").next().closest('input').attr('disabled', 'disabled');
            $("#vehicleDD, #packagesDD").next().next().closest('button').attr('disabled', 'disabled');
            $("#spanVehicleType").buttonset();
            $("#spanVehicleType #avRadio, #spanVehicleType #acRadio").attr("disabled", "disabled")
            $("#spanVehicleType").buttonset("refresh");
        }
        else {
            $("#vehicleDD, #packagesDD").next().closest('input').removeAttr('disabled');
            $("#vehicleDD, #packagesDD").next().next().closest('button').removeAttr('disabled');
            $("#spanVehicleType").buttonset();
            $("#spanVehicleType #avRadio, #spanVehicleType #acRadio").removeAttr("disabled")
            $("#spanVehicleType").buttonset("refresh");
        } 
    }
}


function CrossSellData()
{
    var query = getUrlVarsInvariable();
    try {
        this.na = query["na"];
        this.nc = query["nc"];
        this.pb = query["pb"];
        this.db = query["db"];
        this.pd = query["pd"];
        this.pm = query["pm"];
        this.py = query["py"];
        this.dd = query["dd"];
        this.dm = query["dm"];
        this.dy = query["dy"];
        this.pt = query["pt"];
        this.dt = query["dt"];
        this.ac = query["ac"];
        this.iscrosssell = window.location.href.indexOf("b2bAll") != -1;

        this.url = (this.iscrosssell ? b2bParamUrl : "");
        this.cc = (this.iscrosssell ? b2bParamUrl.substring(5, 7) : null);
        this.cr = query["cr"];
        b2bParamUrl = "";
    }
    catch (error) {
        this.na = null;
        this.nc = null;
        this.pb = null;
        this.db = null;
        this.pd = null;
        this.pm = null;
        this.py = null;
        this.dd = null;
        this.dm = null;
        this.dy = null;
        this.pt = null;
        this.dt = null;
        this.url = null;
        this.cc = null;
        this.iscrosssell = false;
        this.ac = null;
        this.cr = null;
    }
}

function CrossSellDataHaveChanged(newrequest) {
    try {

        if (newrequest.na != crossSellData.na) return true;
        if (newrequest.nc != crossSellData.nc) return true;
        if (newrequest.pb != crossSellData.pb) return true;
        if (newrequest.db != crossSellData.db) return true;


        var pickupDay = newrequest.pd.split("-")[0]
        var pickupMonth = newrequest.pd.split("-")[1]
        var pickupYear = newrequest.pd.split("-")[2]
        if (pickupDay != crossSellData.pd) return true;
        if (pickupMonth != crossSellData.pm) return true;
        if (pickupYear != crossSellData.py) return true;


        var dropDay = newrequest.dd.split("-")[0]
        var dropMonth = newrequest.dd.split("-")[1]
        var dropYear = newrequest.dd.split("-")[2]
        if (dropDay != crossSellData.dd) return true;
        if (dropMonth != crossSellData.dm) return true;
        if (dropYear != crossSellData.dy) return true;


        if (newrequest.pt != crossSellData.pt) return true;
        if (newrequest.dt != crossSellData.dt) return true;


        if (newrequest.cc != crossSellData.cc) return true; //au , nz
        if (newrequest.ac != (CrossSellData.ac == undefined ? csToken.agentId : CrossSellData.ac)) return true; //ararat

        //if (newrequest.brand != CrossSellData.brand) return true;
        //if (newrequest.vt != CrossSellData.vt) return true;
        //if (newrequest.model != CrossSellData.model) return true;

        //if (newrequest.pc != CrossSellData.pc) return true;
        //if (newrequest.isGross != CrossSellData.isGross) return true;
        //if (newrequest.ctyResidence != CrossSellData.ctyResidence) return true;
        return false;
    } catch (ex) {
        return true;
    }
}


function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}