<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WebForm2.aspx.vb" Inherits="THLAuroraWebInterface.WebForm2"%>
<%@ Register TagPrefix="uc1" TagName="FeesAndCharges" Src="../Controls/FeesAndCharges/FeesAndCharges.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebForm2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
		<script language="javascript">
<!--  

 function openmypage()

 {

	  window.open ("testpage.aspx","openmypage","width=500,height=250,location=0,menubar=0,resizable=0,scrollbars=1,status=1,titlebar=1,toolbar=0,screenX=50,left=50,screenY=30,top=50");

 }
//-->
		</script>
		<form id="Form1" method="post" runat="server">
			<table width="560" border="2">
				<TR>
					<TD>
						<P>
							<asp:Label id="Label1" runat="server" Width="746">Label</asp:Label></P>
						<P>&nbsp;</P>
						<P>
							<TABLE id="Table2" cellSpacing="0" cellPadding="2" width="745" border="0">
								<TR>
									<TD class="StdConfirmationHeading" style="WIDTH: 287px">Booking Details -
										<asp:label id="lblVehicle" runat="server" cssclass="normal"></asp:label></TD>
									<TD class="normal" style="WIDTH: 327px">&nbsp;</TD>
									<TD style="WIDTH: 120px"></TD>
								</TR>
								<TR>
									<TD class="StdConfirmationHeading" style="WIDTH: 287px; HEIGHT: 11px">Check Out</TD>
									<TD class="StdConfirmationHeading" style="WIDTH: 327px; HEIGHT: 11px">Address</TD>
									<TD class="StdConfirmationHeading" style="WIDTH: 120px; HEIGHT: 11px">Phone</TD>
								</TR>
								<TR>
									<TD class="normal" style="WIDTH: 287px">
										<asp:label id="lblCheckout" runat="server"></asp:label></TD>
									<TD class="normal" style="WIDTH: 327px">
										<asp:label id="lblCOAddress" runat="server"></asp:label></TD>
									<TD class="normal" style="WIDTH: 120px">
										<asp:label id="lblCOPhone" runat="server"></asp:label></TD>
								</TR>
								<TR>
									<TD class="StdConfirmationHeading" style="WIDTH: 287px">Check In</TD>
									<TD class="StdConfirmationHeading" style="WIDTH: 327px">Address</TD>
									<TD class="StdConfirmationHeading" style="WIDTH: 120px">Phone</TD>
								</TR>
								<TR>
									<TD class="normal" style="WIDTH: 287px; HEIGHT: 24px">
										<asp:label id="lblCheckin" runat="server"></asp:label></TD>
									<TD class="normal" style="WIDTH: 327px; HEIGHT: 24px">
										<asp:label id="lblCIAddress" runat="server"></asp:label></TD>
									<TD class="normal" style="WIDTH: 120px; HEIGHT: 24px">
										<asp:label id="lblCIPhone" runat="server"></asp:label></TD>
								</TR>
								<TR>
									<TD class="StdConfirmationHeading" style="WIDTH: 287px">Operating Hours&nbsp;
										<asp:label id="lblOpHours" runat="server" cssclass="normal"></asp:label></TD>
									<TD class="normal" style="WIDTH: 327px"></TD>
									<TD style="WIDTH: 120px"></TD>
								</TR>
							</TABLE>
							<asp:label id="lblLoadError" runat="server" cssclass="warning"></asp:label>
							<TABLE class="normal" id="tblAll" cellSpacing="0" cellPadding="2" width="746" border="0"
								runat="server">
								<TR>
									<TD class="warning" style="HEIGHT: 22px">
										<TABLE id="Table3" cellSpacing="0" cellPadding="2" width="746" border="0">
											<TR>
												<TD align="right"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 22px">
										<TABLE id="tblAgentInfo" cellSpacing="0" cellPadding="2" width="746" border="0">
											<TR>
												<TD class="StdConfirmationHeading" style="WIDTH: 105px; HEIGHT: 23px">Vehicle 
													Status</TD>
												<TD class="normal" style="HEIGHT: 23px">
													<asp:label id="lblStatus" runat="server"></asp:label></TD>
												<TD class="StdConfirmationHeading" style="HEIGHT: 23px">Date</TD>
												<TD class="normal" style="HEIGHT: 23px">
													<asp:label id="lblDate" runat="server"></asp:label></TD>
											</TR>
											<TR>
												<TD class="StdConfirmationHeading" style="WIDTH: 105px">Our Ref</TD>
												<TD class="normal">
													<asp:label id="lblRef" runat="server"></asp:label></TD>
												<TD></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD class="normal" colSpan="4"></TD>
											</TR>
											<TR>
												<TD class="StdConfirmationHeading" style="WIDTH: 105px; HEIGHT: 7px">Agent Name</TD>
												<TD class="normal" style="HEIGHT: 7px">
													<asp:label id="lblAgentName" runat="server"></asp:label></TD>
												<TD class="StdConfirmationHeading" style="HEIGHT: 7px">Agent Ref</TD>
												<TD class="normal" style="HEIGHT: 7px">
													<asp:label id="lblAgentRef" runat="server"></asp:label></TD>
											</TR>
											<TR>
												<TD class="StdConfirmationHeading" style="WIDTH: 105px">Customer</TD>
												<TD class="normal">
													<asp:label id="lblCustomerName" runat="server"></asp:label></TD>
												<TD></TD>
												<TD></TD>
											</TR>
										</TABLE>
										<HR width="100%" color="#000000" SIZE="2">
									</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 22px">&nbsp;
										<asp:placeholder id="PlaceHolder1" runat="server"></asp:placeholder>
										<DIV id="divHeader" runat="server"></DIV>
										<HR width="100%" color="#000000" SIZE="2">
									</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 22px">
									</TD>
								</TR>
								<TR id="AgentPaymentRow" runat="server">
									<TD style="HEIGHT: 22px">
										<HR width="100%" color="#000000" SIZE="2">
										<uc1:FeesAndCharges id="FeesAndCharges1" runat="server"></uc1:FeesAndCharges></TD>
								</TR>
								<TR id="customerPaymentRow" runat="server">
									<TD style="HEIGHT: 44px">
										<HR width="100%" color="#000000" SIZE="2">
										<asp:label id="lblCustomertoPay" runat="server" CssClass="stdconfirmationheading">Customer to pay on arrival</asp:label>
										<P>&nbsp;</P>
									</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 44px">
										<HR width="100%" color="#000000" SIZE="2">
										&nbsp;</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 44px">
										<asp:placeholder id="PlaceHolder2" runat="server"></asp:placeholder>
										<DIV id="divFooter" runat="server"></DIV>
									</TD>
								</TR>
								<TR>
									<TD align="right"><INPUT type="button" value="Button" onclick="openmypage()">
										<asp:button id="btnContinue" runat="server" Text="Continue"></asp:button></TD>
								</TR>
							</TABLE>
						</P>
					</TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
