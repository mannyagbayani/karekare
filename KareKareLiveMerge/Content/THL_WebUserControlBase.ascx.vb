#Region "Notes"
'Karel Jordaan 27June2006
'Created as base for the usercontrols
'Currently to keep the webservicedowntime notification value

#End Region
Partial Class THL_WebUserControlBase
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Shared mStatusBarText As String


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'Serverside validation
        'Page.Validate()
        'If Not Page.IsValid() Then Throw New Exception("Page is not valid")

    End Sub

#Region "Properties"

    Public Property StatusBarText() As String

        Get
            Return mStatusBarText
        End Get
        Set(ByVal Value As String)
            mStatusBarText = Value
        End Set

    End Property

#End Region

End Class
