﻿Public Partial Class B2BOptions
    Inherits System.Web.UI.Page

    Protected Proxy As New AuroraOTAProxy.Aurora_OTA

    Public Property BackgroundURL() As String
        Get
            Return CStr(ViewState("BackgroundURL"))
        End Get
        Set(ByVal Value As String)
            ViewState("BackgroundURL") = Value
        End Set
    End Property

    Private Property UserData() As String
        Get
            Return CStr(Session("UserData"))
        End Get
        Set(ByVal value As String)
            Session("UserData") = value
        End Set
    End Property

    Public Property UserId() As String
        Get
            Return CStr(ViewState("UserId"))
        End Get
        Set(ByVal Value As String)
            ViewState("UserId") = Value
        End Set
    End Property


    Public Property RSInstance() As THL_RetUsersRS
        Get
            Return CType(Session("THL_RetUsersRS"), THL_RetUsersRS)
        End Get
        Set(ByVal value As THL_RetUsersRS)
            Session("THL_RetUsersRS") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to Initialize the page here
        If Not Page.IsPostBack Then
            '''''LoadUserDetailsControl()
            ' See what the background image is as determined by the user session
            Me.BackgroundURL = General.BackGroundImageURL(Session)
            LoadUserDetailsControl()
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        If IsNothing(Session("POS")) Then
            'redirect to redirect page
            Response.Redirect("AccessDenied.aspx")
        End If

    End Sub


    Private Sub LoadUserDetailsControl()
        Try
            ' Get all user information from web service
            If GetUserData() = True Then
                'Userdetails1.Visible = True
                '' Initialise the userdetails control
                'Userdetails1.InitModify(Me.UserId)
            End If
        Catch ex As Exception
            lblLoadError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try
    End Sub

    Private Function GetUserData() As Boolean
        Dim success As Boolean = True

        Dim RQ As New THL_RetUsersRQ
        Dim RS As New THL_RetUsersRS

        ' Only pos required in request
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        ' All user data retrieved based on user passed in pos
        RS = Proxy.RetUsers(RQ)
        If IsNothing(RS.Errors) Then
            Me.UserId = RS.Users(0).UserId.ToString
            ' Store response in session
            Me.UserData = General.SerializeRetUsersRSObject(RS)
            success = True
            RSInstance = RS
        Else
            'errors occurred 
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
                lblLoadError.Text &= RS.Errors(errorIndex).Tag & "<br>"
            Next
            success = False
        End If
        Return success
    End Function

End Class