﻿Imports System.Web.Configuration
Imports System.Collections.Generic
Imports THLAuroraWebInterface.THLAuroraWebInterface.Reporting

Public Class BookingReports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Public ReadOnly Property AssetBasePath() As String
        Get
            Return HttpPrefix + WebConfigurationManager.AppSettings.Item("AssetsURL")
        End Get
    End Property

    Public ReadOnly Property HttpPrefix() As String
        Get
            If Request.IsSecureConnection Then
                Return "HTTPS://"
            Else
                Return "HTTP://"
            End If
        End Get
    End Property

    Public ReadOnly Property EnhancedUI() As String
        Get
            Dim eui As String = Request.Params.Item("eui")
            If Not (String.IsNullOrEmpty(eui)) Then
                If (eui = "1") Then
                    Return "enhancedUI = true;"
                Else
                    Return "enhancedUI = false;"
                End If
            Else
                Return String.Empty
            End If
        End Get
    End Property
End Class