Imports System.Xml
Partial Class ErrorLog
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
    End Sub

    Private Sub btnClearLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLog.Click

        Dim path As String = Server.MapPath("../content/ErrorLog.xml")
        Dim xmlWriter As XmlTextWriter

        xmlWriter = New XmlTextWriter(path, Nothing)
        With xmlWriter
            .Indentation = 2
            .Formatting = Formatting.Indented
            'Create an XML declaration for this XMl document
            '  .WriteStartDocument(Fal)
            'Add Comment
            ' .WriteComment("This XML file is used to log errors in the THLAuorora Application")
            'The root element is <root>
            .WriteStartElement("root")

            ''Write a new <Error> element
            '.WriteStartElement("Error")
            '.WriteElementString("ErrorId", errorId)
            '.WriteElementString("Message", message)
            ''close <root> element
            .WriteEndElement()

            'close root </Error>
            ' .WriteEndDocument()
            'close underlying stream
            .Close()
        End With

    End Sub
End Class
