'--------------------------------------------------------------------------------------
'   Class:          EditMyDetails
'   Description:    Loads user information based on user info stored in pos and allows
'                   updates to user info
'--------------------------------------------------------------------------------------

Partial Class EditMyDetails
    Inherits System.Web.UI.Page
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Protected WithEvents Userdetails1 As UserDetails



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        '  General.MaintainSessionObjects(General.PageName.EditMyDetails, Session)
        If IsNothing(Session("POS")) Then
            'redirect to redirect page
            Response.Redirect("AccessDenied.aspx")
        End If

    End Sub

#End Region

    Public Property BackgroundURL() As String
        Get
            Return CStr(viewstate("BackgroundURL"))
        End Get
        Set(ByVal Value As String)
            viewstate("BackgroundURL") = Value
        End Set
    End Property

    Private Property UserData() As String
        Get
            Return CStr(Session("UserData"))
        End Get
        Set(ByVal Value As String)
            Session("UserData") = Value
        End Set
    End Property

    Public Property UserId() As String
        Get
            Return CStr(viewstate("UserId"))
        End Get
        Set(ByVal Value As String)
            viewstate("UserId") = Value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        If Not Page.IsPostBack Then
            LoadUserDetailsControl()
            ' See what the background image is as determined by the user session
            Me.BackgroundURL = General.BackGroundImageURL(Session)
        End If
    End Sub

    Private Sub LoadUserDetailsControl()
        Try
            ' Get all user information from web service
            If GetUserData() = True Then
                Userdetails1.Visible = True
                ' Initialise the userdetails control
                Userdetails1.InitModify(Me.UserId)
            End If
        Catch ex As Exception
            lblLoadError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try
    End Sub

    Private Function GetUserData() As Boolean
        Dim success As Boolean = True

        Dim RQ As New THL_RetUsersRQ
        Dim RS As New THL_RetUsersRS

        ' Only pos required in request
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        ' All user data retrieved based on user passed in pos
        RS = Proxy.RetUsers(RQ)
        If IsNothing(RS.Errors) Then
            Me.UserId = RS.Users(0).UserId.ToString
            ' Store response in session
            Me.UserData = General.SerializeRetUsersRSObject(RS)
            success = True
        Else
            'errors occurred 
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
                lblLoadError.Text &= RS.Errors(errorIndex).Tag & "<br>"
            Next
            success = False
        End If
        Return success
    End Function

    Private Sub Userdetails1_CancelClicked() Handles Userdetails1.CancelClicked
        LoadUserDetailsControl()
    End Sub

    Private Sub Userdetails1_SaveClicked(ByVal userId As String) Handles Userdetails1.SaveClicked
        ' Invalidate the fullname cache so that the latest details will be displayed in header
        Session("FullName") = Nothing
    End Sub
End Class
