﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3603
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class OTACreateBooking

    '''<summary>
    '''Form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblErrors control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrors As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''drpTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpTitle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtLastName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLastName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFirstName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFirstName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMiddleName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMiddleName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPhoneNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPhoneNumber As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmail As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCountry As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''drpBrand control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpBrand As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtBooNum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBooNum As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCkoDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCkoDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCkoLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCkoLoc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtckiDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtckiDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCkiLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCkiLoc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''drpVehicle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpVehicle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtPkgCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPkgCode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''StdInsurence control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents StdInsurence As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''ER1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ER1 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''ER2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ER2 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''PrePkg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PrePkg As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''drpBabyCapsuleQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpBabyCapsuleQty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''drpBabySeatQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpBabySeatQty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''drpBoostSeatQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpBoostSeatQty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''drpTableQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpTableQty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''drpChairQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpChairQty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''drpCamPackQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpCamPackQty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''drpTentQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpTentQty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtCardType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCardType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtPmtAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPmtAmt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCardNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCardNumber As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtExpiryMn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtExpiryMn As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtExpiryYr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtExpiryYr As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCardHolderName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCardHolderName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtNoteText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNoteText As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCreateBooking control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCreateBooking As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Button2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button2 As Global.System.Web.UI.WebControls.Button
End Class
