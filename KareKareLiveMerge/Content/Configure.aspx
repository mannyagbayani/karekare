﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Configure.aspx.cs" Inherits="Content_Configure" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Configure your Booking</title>
    
    <!--<link rel="stylesheet" type="text/css" href="<%= AssetBasePath %>/css/b2b/jquery.ui.autocomplete.css" />-->
    <link href="<%= AssetBasePath %>/css/b2b/bc.css" rel="stylesheet" type="text/css"  />
    <link href="<%= AssetBasePath %>/css/b2b/configure.css" rel="stylesheet" type="text/css"  /><!-- TODO: remove once merged -->
    <link rel="stylesheet" type="text/css" href="../Styles.css" />
    <link href="/css/b2b_local.css" rel="stylesheet" type="text/css"  />
    <script src="<%= HTTPPrefix %>ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
    <script src="<%= HTTPPrefix %>ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="/js/jquery.ba-bbq.min.js" type="text/javascript"></script>
    <script src="/js/bc.js" type="text/javascript"></script>
    <script src="/js/configure.js" type="text/javascript"></script><!-- REfactor into above -->
    <script type="text/javascript">
        var currencyStr = '<%= CurrencyStr %>';
        var vehicleCharges = <%= VehicleJSON %>;
        var extraHireItems = <%= ExtraHireItemsJSON %>;
        var insuranceProducts = <%= InsuranceProductJSON %>;
        var ferryItems = <%= FerryJSON %>;
        var availabilityItem = <%= AvailabilityJSON %>;            
        var allInclusive = <%= AllInclusiveJSON %>;
        var oneWayFeeWaiver = <%= OneWayFee %>;
        var selectedExRate = { "code" : "<%= SelectedCurrencyRate.ToCode %>", "rate" : "<%= SelectedCurrencyRate.Rate %>"};
                
        $(document).ready(function () {
            initConfigurePage();

            $('#configureBackbutton').click(function()
            {
                     if (getUrlVars()["b2bAll"] != null) {
                           window.location.href= "/content/availability.aspx?b2bAll=true" //+ navbackurl;
                     }
                     else                        
                        window.history.back();
                        
            });

        });    


    </script>
    <style type="text/css">
        
        .PriceDetailsList .chargesTable td.rt {
            width: 320px;        }
    
        #vehicleChargeDetails .NoDiscount .chargesTable td.hp {
            width: 100px;        
            /*width: 50px;        */
        }
    
        #vehicleChargeDetails .NoDiscount .chargesTable td.pdp {
            width: 200px;
            /*width: 370px;*/
        }
               
        #vehicleChargeDetails .NoDiscount .chargesTable td.pta {
            /*width: 100px;*/
            width: 200px;
            /*text-align: right;*/
        }
               
        #vehicleChargeDetails .NoDiscount .chargesTable td.pap {
            /*width: 100px;*/
            width: 200px;
            text-align: right; 
        }
        /*-----*/
        
        .hasDisc  .chargesTable td.rt {
            width: 294px;        
        }   
        
        .hasDisc .chargesTable td.pap, .hasDisc  .chargesTable th.PayAtPU {
            text-align: right !important;            
        }       
    
        .NoDiscount th.PayAtPU {
            text-align: right !important;            
        }   
        .NoDiscount table td.price {
            width: 195px !important;
        }
        
    </style>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35407162-1']);
        _gaq.push(['_setDomainName', 'thlrentalsb2b.com']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>           
</head>
<body class="<%= (BonusPackAvailable ? "hasIncOption" : "") %><%= (IsGrossRequest ? " IsGross" : "" )  %>">
    <form id="form1" runat="server" onsubmit="return false;" name="ConfigureForm">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div id="priceAvailabilityContainer">
        <uc1:Header ID="Header1" runat="server"></uc1:Header>    
        <div id="configureContainer">
            <div class="section breadCrumb config <%= IsAlternativeAvailability ? "aa" : "" %>"></div>            
            <h2>2. Configure your vehicle</h2>
            <div id="configurePanel" class="<%= (InclusiveMode ? "incMode" : "") + (SelectedInclusive ? "incSelected" : "") %>">
                <%= ConfigureDisclaimer %>
                <div id="topHeader">
                    <span class="currencyPanel"><small>Showing Alternative Rates in:</small>
                        <label><%= SelectedCurrencyRate.ToCode %></label>                            
                    </span>
                </div>                                                                                                            
                <div id="vehicleTopPanel" class="section">
                    <div class="VehicleHeader">
                        <a class='VehicleTitle PopUp' href="http://<%= VehicleContentPage %>"><%= VehicleName %></a>
                        <h4>Travel details:&nbsp;</h4>                        
                        <%= LocationsPanel %>
                    </div>
                    <div class="VehicleImages">
                        <span class="left">
                            <img src="<%= VehicleInclusionPath %>" alt="<%= VehicleName %> inclusions" title="<%= VehicleName %> inclusions" />
                        </span>
                        <span class="right">
                            <a class='VehicleTitle PopUp' href="http://<%= VehicleContentPage %>">
                                <img src="<%= VehicleImagePath %>" title="<%= VehicleName %>" alt="<%= VehicleName %>" />
                            </a>
                        </span>
                    </div>                
                </div>                
                <div id="vehicleChargeDetails" class="section <%= (HasDiscountedDays ? "hasDisc" : "") %>">
                   <div class="title">
                        <h4>Vehicle charges:&nbsp;</h4>
                        <a href="http://<%= ChargesURL %>" class="Quest PopUp">?</a>                    
                    </div>
                    <span id="ChargeHeader">
                        <span class="Title">                            
                            <a href="#details" class="ShowPriceList">Close Details</a> 
                        </span>
                        <span class="Total">
                            <small class="ptalabel">Payable to agent</small>
                            <small class="exRate pta" rel="<%= pta %>"><%= pta %></small> 
                            <!--rev:mia April 1 2014 Payable to supplier messages-->
                            <small class="paplabel"> <%= GetMessage("PayableAtPickupText")%></small>
                            <small class="exRate pap" rel="<%= pap %>"><%= pap %></small> 
                        </span>
                    </span>                 
                    <%= PriceDetailsTable %>
                </div>          
                <%= InusranceOptionsStr %>
                <div id="hireItemsPanel"  class="section<%= (hasDiscountDays ? "" : " NoDiscount") %>">
                    <%= ExtraHiresTable %>
                </div>
                <div id="ferryPanel" style="display:none;">                
                    <%= FerryPanel %>
                    <input type="hidden" id="ferryInfo" name="ferryInfo" />                                    
                </div>
                <input type='hidden' id='ehi_total' name='ehi_total' />
            </div>                
            <div id="configureUpdatePanel" class="section">
                <span class="total">
                    <span class="labels">
                        <label>Payable to agent:</label>
                        <!--rev:mia April 1 2014 Payable to supplier messages-->
                        <label><%= GetMessage("PayableToSupplerDays") %>:</label>
                    </span>
                    <span class="price" title="<%= PackageCode %>">
                        <small id="totalAmount" class="exRate pta">0.00</small> 
                        <small id="payAtPickUp" class="exRate pap">0.00</small> 
                    </span>
                </span>
                <span class="TaxMsg ">
                    <%--<small>Total includes all taxes (GST)</small>    --%>     
                    <small><%= TaxMessage %></small>                   
                </span>
                <span class="Submit">
                    <%--<a class="BackBtn" href="javascript:history.back();" id='A1'>Back</a>                 --%>
                    <a class="BackBtn" id='configureBackbutton'>Back</a>                 
                    <input id="confrmBooking" type="submit" onclick="submitBooking(this);" value="KK"/>
                    <input id="provisionalQuote"  onclick="submitBooking(this);" value="NN"/>
                    <input id="saveQuote" onclick="submitBooking(this);" value="QN" />
                    <input type="hidden" id="bookingSubmitted" name="bookingSubmitted" value="0" />
                </span>
            </div>
        </div>
        <%-------------------------------------------------------------------------------------------------------
        rev:mia MAY 3 2013  - PART OF APCOVER ENHANCEMENT. 
        -------------------------------------------------------------------------------------------------------%>
        <asp:HiddenField ID="IsCustomerChargeHidden" Value="" runat="server"  />
    </form>
</body>
</html>