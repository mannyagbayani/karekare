﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using THL.Booking;

public partial class Content_SetSession : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        B2BSessionCache sessionCache = SessionManager.GetSessionData().B2BCache;
        NameValueCollection rqst = Request.Params;
        string results = rqst["cb"] + @"({""msg"":""Session Expired""})";
        if (!B2BAvailabilityHelper.SessionExpired && sessionCache != null)
        {
            try
            {
                results = rqst["cb"] + @"({""msg"":""Session Expired""})";
                string pd = rqst["pd"] + "-" + rqst["pm"] + "-" + rqst["py"];
                string dd = rqst["dd"] + "-" + rqst["dm"] + "-" + rqst["dy"];
                string B2BJSONParams = @"{""pb"":""" + rqst["pb"] + @""",""db"":""" + rqst["db"] + @""",""pd"":""" + pd + @""",""dd"":""" + dd + @""",""pt"":""" + rqst["pt"] + @""",""dt"":""" + rqst["dt"] + @""",""na"":""" + rqst["na"] + @""",""nc"":""" + rqst["nc"] + @""",""cc"":""" + rqst["cc"] + @""",""brand"":""" + rqst["brand"] + @""",""vt"":""av"",""model"":""" + rqst["model"] + @""",""ac"":""" + rqst["ac"] + @""",""pc"":"""",""isGross"":""" + rqst["isGross"] + @""",""requestTimeInMil"":""1396826216000"",""requestType"":""1"",""ctyResidence"":""" + rqst["cr"] + @"""}";
                

                AgentLoginData agentData = HttpContext.Current.Session["AgentData"] as AgentLoginData;
                B2BAvailabilityRequest b2bReq = new B2BAvailabilityRequest(B2BJSONParams, agentData);
                SessionManager.GetSessionData().AvailabilityRequestData = b2bReq;//added to remember selections
                SessionManager.GetSessionData().CurrentAgent = agentData.Sine;
                //Pull items from session
                sessionCache = SessionManager.GetSessionData().B2BCache == null ? new B2BSessionCache() : SessionManager.GetSessionData().B2BCache;
                AvailabilityItem[] availabiltyItems = sessionCache.GetAvailabilityResponse(b2bReq);
                SessionManager.GetSessionData().B2BCache = sessionCache;
                availabiltyItems = AvailabilityHelper.SortAvailabilityItems(availabiltyItems, "bubble", "asc");

                string avp = string.Empty;
                if (availabiltyItems != null && availabiltyItems.Length > 0 && !string.IsNullOrEmpty(availabiltyItems[0].AVPID))
                {
                    avp = availabiltyItems[0].AVPID;
                    foreach (AvailabilityItem item in availabiltyItems)
                        if (item.VehicleCode.ToLower().Equals(rqst["vh"].ToLower()))
                        {
                            //rev:mia 03June2015 - break when found
                            avp = item.AVPID;
                            break;
                        }
                }
                SessionManager.GetSessionData().AvailabilityRequestData = b2bReq;//update the current sessions request
                string sortedItems = String.Empty;
                string encodedHTML = HttpContext.Current.Server.HtmlEncode(sortedItems);
                results = rqst["cb"] + @"({""ag"":""" + agentData.DefaultAgentCode + @""",""avp"":""" + avp + @""",""items"":" + availabiltyItems.Length + @",""rqst"":""" + /*B2BJSONParams +*/  @"""})";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.Application, "SetAgentSession", ex.Message + "," + ex.StackTrace, @"{""ac"":""" + rqst["ac"] + @"""}");
                results = rqst["cb"] + @"({""msg"":""" +  "unepected error" + @"""})";
            }
        } 
        Response.ContentType = "application/json";
        Response.Write(results);
        Response.End();
    }
}