<%@ Register TagPrefix="uc1" TagName="StandardConfirmation" Src="../Controls/Booking/StandardConfirmation.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrinterFriendlyPage.aspx.vb"
    Inherits="THLAuroraWebInterface.PrinterFriendlyPageaspx"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Print Version</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="../Styles.css">
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <uc1:StandardConfirmation ID="StandardConfirmation1" runat="server"></uc1:StandardConfirmation>

    <script language="javascript">
<!--

	
			window.print();

//-->
    </script>

    </form>
</body>
</html>
