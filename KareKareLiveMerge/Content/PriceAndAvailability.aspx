<%@ Register TagPrefix="uc1" TagName="BookingContainer" Src="../Controls/Booking/BookingContainer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PriceAndAvailability.aspx.vb" Inherits="THLAuroraWebInterface.PriceAndAvailability"%>
<%@ Register TagPrefix="uc1" TagName="FooterImage" Src="../Controls/Graphics/FooterImage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LocationAndTime" Src="../Controls/PriceAndAvailability/LocationAndTime.ascx" %>
<%@ Register TagPrefix="uc2" TagName="Availability" Src="../Controls/PriceAndAvailability/Availability.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PriceAndAvailability</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.js" type="text/javascript"></script>
	</HEAD>
	<body background='<%=BackgroundURL()%>'>
		<form id="Form1" method="post" runat="server">
			<table id="Table1" borderColor="#999999" cellSpacing="0" cellPadding="0" width="746" border="0">
				<tr>
					<td>
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="746" border="0">
							<TR>
								<TD style="HEIGHT: 15px" vAlign="top">
									<uc1:Header id="Header1" runat="server"></uc1:Header>
								</TD>
							</TR>
							<TR id="LocationAndTimeRow" runat="server">
								<TD>
									<uc1:LocationAndTime id="LocationAndTime1" runat="server"></uc1:LocationAndTime></TD>
							</TR>
							<tr id="waitingMessageRow" runat="server">
								<td>
									<span class="warning" id="Message"></span><span class="warning" id="Progress" style="WIDTH: 25px; TEXT-ALIGN: left">
									</span>
								</td>
							</tr>
							<TR id="AvailabilityRow" runat="server">
								<TD><U><FONT color="#800080">
											<uc2:Availability id="Availability1" runat="server" Visible="False"></uc2:Availability></FONT></U></TD>
							</TR>
							<TR id="BookingContainerRow" runat="server">
								<TD>
									<uc1:BookingContainer id="BookingContainer1" runat="server" Visible="False"></uc1:BookingContainer></TD>
							</TR>
							<TR>
								<TD>
									<asp:Label id="lblError2" runat="server"></asp:Label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</table>
			<br><br><br><br><br><br><br>
			<uc1:FooterImage id="FooterImage1" runat="server"></uc1:FooterImage>
		</form>
	</body>
</HTML>
