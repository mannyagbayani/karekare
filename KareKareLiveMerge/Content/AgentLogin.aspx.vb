'--------------------------------------------------------------------------------------
'   Class:          Login
'   Description:    Authenticates Agent Users. Redirects to a secure
'                   channel if not already on one. Sets agent session info.
'                  
'--------------------------------------------------------------------------------------

Imports System.Xml.Serialization
Imports System.IO
'Imports tst.localhost
'Imports THLRentals.OTA.Utils
''Imports TelecomNZ.Common.Library



Partial Class Login
	Inherits System.Web.UI.Page
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA


#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        General.MaintainSessionObjects(General.PageName.AgentLogin, Session)
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here

        If Not Page.IsPostBack Then
            General.ClearSession(Session)
        End If

        ' This is an agent user running under b2b 
        Session("TypeOfUser") = "agent"
        Session("Company") = "mauibritz"
        Session("b2basync") = Nothing

        'don't redirect to https if in developement mode
        If Config.GetEnvironment <> Config.Environment.ENV_DEV AndAlso Config.GetEnvironment <> Config.Environment.ENV_TEST Then
            Dim httpPart As String = Me.Request.Url.Scheme
            Dim rawURL As String = Me.Request.Url.AbsoluteUri.ToLower

            If httpPart.ToLower = "http" Then
                'redirect back to this page using a secure channel
                rawURL = rawURL.Replace("http", "https")
                Response.Redirect(rawURL)
            End If

        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
