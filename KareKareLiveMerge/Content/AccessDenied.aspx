<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AccessDenied.aspx.vb" Inherits="THLAuroraWebInterface.AccessDenied"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AccessDenied</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P>
				<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="746" border="0">
					<TR>
						<TD class="normal" align="center">Your session has either expired or you are not 
							authorised to view this page.</TD>
					</TR>
					<TR>
						<TD align="center">
							<asp:HyperLink id="hypReturnToLogin" runat="server">Return to Login page</asp:HyperLink>
						</TD>
					</TR>
				</TABLE>
			</P>
		</form>
	</body>
</HTML>
