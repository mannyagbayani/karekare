﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using THL.Booking;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using System.Net;
using THLAuroraWebInterface;

public partial class Content_Availability : System.Web.UI.Page
{

    public string DebugStr, CtrlJSONObj, aLoginDataJSONObj, StateJSON;

    /// <summary>
    /// Get the Agents Token in JSON format, TODO: refactor into B2BCrossSell Utils
    /// </summary>
    /// <param name="agentId"></param>
    /// <returns></returns>
    public static string AgentKeyJSON(AgentLoginData aLoginData, string baseUrl)
    {
        string agentId = string.Empty, brandsStr = string.Empty;
        try
        {
            agentId = aLoginData.DefaultAgentCode;
            brandsStr = string.Empty;//aggregate brands sold by agent..
            foreach (THLPackage pkg in aLoginData.packages)
                if (!brandsStr.Contains(pkg.brand))
                    brandsStr += pkg.brand;

            if (string.IsNullOrEmpty(agentId))
                return "{\"token\":null}";
            string requestUrl = baseUrl + "/webservices/setagenttoken.aspx?agentId=" + agentId;//from config
            string json = @"{""token"":""tkn""}";//auth stub;
            json = json.Replace("}", ",\"agentId\":\"" +  agentId +"\",\"base\":\"" + baseUrl + "\",\"brands\":\"" +  brandsStr +@"""}");
            //    return json;
            //}
            return json;
        }
        catch (Exception ex)
        {
            THLDebug.LogError(ErrorTypes.ExternalProvider, "AgentKeyJSON", ex.Message, "{agentId:\"" + agentId + "\"}");
            return "{\"token\":null}";
        }
    }

    public string EnhancedUI
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.Params["eui"]))
            {
                return (Request.Params["eui"].Equals("1") ? "enhancedUI = true;" : "enhancedUI = false;");
            }
            else
            {
                return "";
            }
        }
    }


    public static string AssetBasePath
    {
        get { return ApplicationManager.HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"]; }
    }

    //rev:mia 27Jan2015
    public static string CrossSellPath
    {
        get { return System.Web.Configuration.WebConfigurationManager.AppSettings["CrossSellURL"]; }
    }

    /// <summary>
    /// Secure or Not
    /// </summary>
    public string HTTPPrefix { get { return ApplicationManager.HTTPPrefix; } }


    public static Char BrandChar { get; set; }
    public static string VehicleName { get; set; }
    public static string VehicleCode { get; set; }
    public static string PackageName { get; set; }
    public static string VehicleType { get; set; }
    public static string VehicleImageBase { get; set; }
    public static string VehicleImage72 { get; set; }
    public static string VehicleImage144 { get; set; }
    public static string VehicleInclusionImage { get; set; }
    public static string VehiclePopupURL { get; set; }
    public static string AgentsBrandRadios { get; set; }
    public static string TokenJson { get; set; }

    public AgentLoginData aLoginData;

    //rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT 
    //THL history: last day of niven  
    //1. set the default option (gross or net)
    public static string minifyCurrency { get; set; }
    public static string b2bParamUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        AvailabilityRequest cachedAvailability = SessionManager.GetSessionData().AvailabilityRequestData;
        if (cachedAvailability != null)
            StateJSON = @"{""pb"":""" + cachedAvailability.PickUpBranch + @""",""db"":""" + cachedAvailability.DropOffBranch + @"""}";
        else
            StateJSON = "null";

        if (!IsPostBack)
        {
            csellFrame.Attributes.Add("src", CrossSellPath + "/IFramePrePRocess.html");
    
            if (!B2BAvailabilityHelper.SessionExpired)//has B2B valid agent
            {
                DataAccess dal = new DataAccess();
                aLoginData = dal.GetAgentData((SourceType)(Session["POS"]));
                

                string crossSellURL = System.Web.Configuration.WebConfigurationManager.AppSettings["CrossSellURL"];
                TokenJson = AgentKeyJSON(aLoginData, crossSellURL);
                
                Session["AgentData"] = aLoginData;//TODO: encapsulate into session manager..
                //rev:mia june 16, 2014 - injection of code to display Gross  and NET AMOUNT 
                //Session["b2btokenjson"] = TokenJson;

                //DebugStr = QuoteTHLLogin((SourceType)(Session["POS"]), aLoginData);                
                B2BCacheManager cm = new B2BCacheManager();//TODO: from app cache 
                THLCountry[] countries = cm.GetCountriesForAgent(aLoginData);
                CtrlJSONObj = countries.ToJSON();
                aLoginDataJSONObj = aLoginData.ToJSON();
                //rev:mia june 16, 2014 - injection of code to display Gross  and NET AMOUNT 
                Session["aLoginDataJSONObj"] = aLoginDataJSONObj;
                if (Session["b2basync"] != null)
                {
                    b2bParamUrl = Session["b2basync"].ToString();
                    Session["b2basync"] = null;
                }
                else
                {
                    b2bParamUrl = "";
                }
                //rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT 
                THL.Booking.AgentDescription[] activeAgent = null;
                string agentIDdefault = "";
                string agentNAMEdefault = "";
                foreach (THL.Booking.AgentDescription agent in aLoginData.agents)
                {
                    if (agent.CurrencyRates != null && agent.DefaultCurrency != "" && agent.ID == aLoginData.DefaultAgentCode)
                    {
                        activeAgent = new AgentDescription[] { agent };
                    }
                    else if (agent.ID == aLoginData.DefaultAgentCode)
                    {
                        agentIDdefault = agent.ID;
                        agentNAMEdefault = agent.Name;
                    }
                }
                if (activeAgent == null)
                {
                    THL.Booking.AgentDescription activeAgentdefault = new THL.Booking.AgentDescription();
                    List<THL.Booking.ExchangeRate> crs = new List<THL.Booking.ExchangeRate>();
                    THL.Booking.ExchangeRate currencyRates = new THL.Booking.ExchangeRate();// { FromCode = "xxx", Rate = 1, ToCode = "xxx" };
                    //currencyRates.FromCode = "xxx";
                    //currencyRates.ToCode = "xxx";
                    currencyRates.FromCode = "AUD";
                    currencyRates.ToCode = "AUD";
                    currencyRates.Rate = 1;
                    crs.Add(currencyRates);

                    activeAgentdefault.CurrencyRates = crs;
                    activeAgentdefault.ID = agentIDdefault;
                    activeAgentdefault.Name = agentNAMEdefault;
                    activeAgentdefault.DefaultCurrency = "AUD";
                    activeAgentdefault.CurrencyList = new string[1] { "AUD" };
                    activeAgent = new THL.Booking.AgentDescription[1];
                    activeAgent[0] = activeAgentdefault;
                }
                
                minifyCurrency = activeAgent[0].CurrencyRates.ToJSON().Replace("FromCode", "fc").Replace("Rate", "rt").Replace("ToCode", "tc");


                //swapped with DropDown down:
                //AgentsBrandRadios = RenderAgentsBrandRadios(countries);
                
                AgentsBrandRadios = RenderAgentsBrandDropDown(countries);
            }
        }
    }

    /// <summary>
    /// Process Ajax requst for Availability Results
    /// </summary>
    /// <param name="B2BJSONParams"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string GetAvailabilityItems(string B2BJSONParams)
    {
        if (!B2BAvailabilityHelper.SessionExpired)
        {
            //string B2BJSONParams = "{'pb':'AKL', 'db':'AKL', 'pd' : '01-12-2011', 'dd':'15-12-2011', 'pt':'10:00', 'dt':'15:00', 'na':2, 'nc':3, 'cc':'NZ', 'brand':'b', 'vt':'av', 'model':'all', 'ac' :'ARARAT', 'pc':'FLEXB'}";
            AgentLoginData agentData = HttpContext.Current.Session["AgentData"] as AgentLoginData;
            B2BAvailabilityRequest b2bReq = new B2BAvailabilityRequest(B2BJSONParams, agentData);

            SessionManager.GetSessionData().AvailabilityRequestData = b2bReq;//added to remember selections

            SessionManager.GetSessionData().CurrentAgent = agentData.Sine;
            
            //Pull items from session
            B2BSessionCache sessionCache = SessionManager.GetSessionData().B2BCache == null ? new B2BSessionCache() : SessionManager.GetSessionData().B2BCache;
            AvailabilityItem[] availabiltyItems = sessionCache.GetAvailabilityResponse(b2bReq);
            SessionManager.GetSessionData().B2BCache = sessionCache;
            availabiltyItems = AvailabilityHelper.SortAvailabilityItems(availabiltyItems, "bubble", "asc");
            SessionManager.GetSessionData().AvailabilityRequestData = b2bReq;//update the current sessions request
            string sortedItems = String.Empty;
            bool hasNogross = HasNoGross(availabiltyItems);
            sortedItems = RenderAvailabilityCatalog(availabiltyItems, b2bReq);
            string encodedHTML = HttpContext.Current.Server.HtmlEncode(sortedItems);
            string searchResults = String.Format("<xml count='{0}' hasnogross='{1}' B2BJSONParams='{2}'>{3}</xml>", availabiltyItems.Length, hasNogross, B2BJSONParams, encodedHTML);
            return searchResults;
        }
        else
            HttpContext.Current.Response.StatusCode = 404;
        return String.Empty;
    }


    /// <summary>
    /// Return Agent Packages for a given request state (possible add date support and refactor
    /// </summary>
    /// <param name="B2BJSONParams"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetAgentData(string agentCode, string cc, string brand)
    {
        StringBuilder sb = new StringBuilder();
        if (!B2BAvailabilityHelper.SessionExpired)
        {            
            //TODO: mode into data access layer once finalised
            string otaBase = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraOTABase"];
            THL.Booking.DAL.Aurora_OTA wsProxy = new THL.Booking.DAL.Aurora_OTA(otaBase);
            THL.Booking.DAL.SourceType pos = new THL.Booking.DAL.SourceType();            
            THL.Booking.DAL.UniqueID_Type uit = new THL.Booking.DAL.UniqueID_Type();
            uit.ID = agentCode;
            uit.Type = "22";
            pos.RequestorID = uit;
            THL.Booking.DAL.THL_PriceAndAvailDataRQ req = new THL.Booking.DAL.THL_PriceAndAvailDataRQ();
            req.POS = new THL.Booking.DAL.SourceType[]{ pos };

            THL.Booking.DAL.THL_PriceAndAvailDataRS rs = new THL.Booking.DAL.THL_PriceAndAvailDataRS();
            try
            {
                rs = wsProxy.PriceAndAvailData(req);
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "Availability.GetAgentData", ex.Message, "{'agent':'" + agentCode + "','cc':'" + cc + "','brand':'" + brand + "'}");
                return "[]";//TODO: define error protocol for misconfigured agents
            }
            List<THL.Booking.DAL.PackageType> agentPackageList = new List<THL.Booking.DAL.PackageType>();
            foreach (THL.Booking.DAL.CountryType cType in rs.PriceAndAvailDataCore.PickupAndDropOffLocations)
            { 
                if(cType.Code.Equals(cc))
                    foreach (THL.Booking.DAL.BrandsType bType in cType.Brands)
                    {
                        if (bType.Code.Equals(brand) || brand.Equals("all"))
                            foreach (THL.Booking.DAL.PackageType pType in bType.Package)
                            {
                                agentPackageList.Add(pType);
                            }
                    }
            }
            //json it, TODO: refactor into displayer
            foreach (THL.Booking.DAL.PackageType pkg in agentPackageList)
                sb.Append("{'name':'" + pkg.Code + " - " + pkg.Name + "','code':'" + pkg.Code + "', 'isDomestic':'" + pkg.IsDomestic + "'},");  //rev:mia April 16 2012
            string response = "[" + sb.ToString().TrimEnd(',') + "]";
            return response;
        }
        return "[]";
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string GetAltAvailabilityItems(string B2BJSONParams)
    {
        if (!B2BAvailabilityHelper.SessionExpired)
        {
            //string B2BJSONParams = "{'pb':'AKL', 'db':'AKL', 'pd' : '01-12-2011', 'dd':'15-12-2011', 'pt':'10:00', 'dt':'15:00', 'na':2, 'nc':3, 'cc':'NZ', 'brand':'b', 'vt':'av', 'model':'all', 'ac' :'ARARAT', 'pc':'FLEXB'}";
            AgentLoginData agentData = HttpContext.Current.Session["AgentData"] as AgentLoginData;
            B2BAltAvailabilityRequest b2bReq = new B2BAltAvailabilityRequest(B2BJSONParams, agentData);
            B2BSessionCache sessionCache = SessionManager.GetSessionData().B2BCache == null ? new B2BSessionCache() : SessionManager.GetSessionData().B2BCache;
            AlternativeAvailabilityItem[] aaItems = sessionCache.GetAltAvailabilityResponse(b2bReq);
            SessionManager.GetSessionData().B2BCache = sessionCache;//assign
            SessionManager.GetSessionData().AvailabilityRequestData = b2bReq;
            string encodedHTML = HttpContext.Current.Server.HtmlEncode(RenderAltAvailabilityCatalog(aaItems, b2bReq));

            bool hasNogross = HasNoGross(aaItems);
            //hasNogross = b2bReq.IsGross;

            string searchResults = String.Format("<xml count='{0}' hasnogross='{1}' B2BJSONParams='{2}'>{3}</xml>", aaItems.Length, hasNogross, B2BJSONParams, encodedHTML);
            return searchResults;
        }
        else
            HttpContext.Current.Response.StatusCode = 404;
        return String.Empty;
    }

    /// <summary>
    /// Implemented to swap brand radios option with drop down (support United and KeaNZ and increasing number of brands in B2B)
    /// </summary>
    /// <param name="countries"></param>
    /// <returns></returns>
    public static string RenderAgentsBrandDropDown(THLCountry[] countries)
    {
        //rev:mia- aug.28, 2013 - addition of roadbear and US britz
        bool isUS = false;
        string optionClass = "AUNZbrand";

        string defaultBrand = "M";//TODO: consider from config
        Dictionary<string, B2BBrand> brandDic = new Dictionary<string, B2BBrand>();
        foreach (THLCountry country in countries)
            foreach (B2BBrand brand in country.brands)
                if (!brandDic.ContainsKey(brand.code) && brand.packages != null && brand.packages.Length > 0)  brandDic.Add(brand.code, brand);
                //if (!brandDic.ContainsKey(brand.code) ) brandDic.Add(brand.code, brand);

        if (!brandDic.ContainsKey("M"))
            defaultBrand = "B";

        StringBuilder sb = new StringBuilder();

        int brandCount = 0;
        sb.Append(@" 
                        <script type=""text/javascript"">
                            $(document).ready(function(){
                                    //set defaults here                                  
                            });
                        </script>
                        <span>
                            <select id=""brandSelector"" class=""combobox""> 
                                <option value=""all"" class=""AUNZbrand"">All Brands</option>                      
        ");
        foreach (B2BBrand brand in brandDic.Values.ToArray().OrderBy(c => c.name))
        {
            isUS = (brand.code.Equals("Z") || brand.code.Equals("R"));
            optionClass = (isUS ? "USbrand": "AUNZbrand");

            sb.Append(@"
                                <option class=""" + optionClass + @"""  +   value=""" + brand.code + @""" " + (brand.code.Equals(defaultBrand) ? "selected='selected'" : "") + @" >" + brand.name + @"</option>
            ");   

            brandCount++;
        }
        sb.Append(@"
                            </select>
                        </span>");
        return sb.ToString();
    }

    public string USBrandContent
    {
        get;
        set;
    }

    public string AUNZBrandContent
    {
        get;
        set;
    }
    /// <summary>
    /// Render agents brand radios conditional on agents possible brands
    /// TODO: refactor into a designated View Diplayer
    /// </summary>
    /// <param name="agentData">agent data</param>
    /// <returns>HTML representation</returns>
    public static string RenderAgentsBrandRadios(THLCountry[] countries)
    {
        string defaultBrand = "M";//TODO: consider from config
        Dictionary<string,B2BBrand> brandDic = new Dictionary<string,B2BBrand>();
        foreach (THLCountry country in countries)
            foreach (B2BBrand brand in country.brands)
                if (!brandDic.ContainsKey(brand.code) && brand.packages != null && brand.packages.Length > 0) brandDic.Add(brand.code, brand);

        if (!brandDic.ContainsKey("M"))
            defaultBrand = "B";
        
        StringBuilder sb = new StringBuilder();
        
        int brandCount = 0;
        foreach(B2BBrand brand in brandDic.Values.ToArray())
        {

            if(brandCount % 2 == 0)            
            sb.Append(@" 
                        <script type=""text/javascript"">
                            $(document).ready(function(){
                                    $('#brandSelector').val('"+  defaultBrand +  @"');                                    
                            });
                        </script>    
                        <span class=""Radios"">
            ");
            sb.Append(@"
                            <input type=""radio"" name=""brand"" value=""" + brand.code + @""" id=""brand" + brand.code + @""" class=""smart-radio-button"" " + (brand.code.Equals(defaultBrand) ? "checked='checked'" : "") + @" />
                            <label for=""brand" + brand.code + @""" class=""brand" + brand.code + @""">" + brand.name + @"</label>
            ");

            if (brandCount % 2 ==1)
                sb.Append(@"
                        </span>
            ");
            brandCount++;
        }
        sb.Append("</span>");
        
        
        return sb.ToString();
    }
    
    public static void BindVehicleInfo(B2BAvailabilityRequest request, AvailabilityItem item)
    {
        ////1 brand per request and handles the AA xml where brand is returned as T(generic);
        item.Brand = THLBrand.GetBrandForString(request.Brand);
        BrandChar = THLBrand.GetBrandChar(item.Brand);
        VehicleName = item.VehicleName;
        VehicleCode = item.VehicleCode;
        PackageName = item.PackageCode;
        VehicleType = (request.VehicleType == THL.Booking.VehicleType.Car ? "Cars" : "Campervans");
        VehicleImageBase = AvailabilityHelper.GetVehicleImageBasePath(request.CountryCode, item.Brand, request.VehicleType,VehicleCode, AssetBasePath, false);
        VehicleInclusionImage = String.Format("{0}-InclusionIcons.gif", AvailabilityHelper.GetVehicleImageBasePath(request.CountryCode, item.Brand, request.VehicleType, VehicleCode, AssetBasePath, true));
        VehicleImage72 = String.Format("{0}-ClearCut-72.jpg", VehicleImageBase);
        VehicleImage144 = String.Format("{0}-ClearCut-144.jpg", VehicleImageBase);
        VehiclePopupURL = B2BAvailabilityDisplayer.GetVehicleContentLink(request, item);


        //if (item.Brand == THLBrands.Kea)
        //    VehiclePopupURL = KeaURLFix(item.VehicleCode);

    }

    /// <summary>
    /// Helper to by pass no vanity url limitation on KeaAU, to be removed once Kea transition into MOSS..
    /// </summary>
    /// <param name="vehicleCode"></param>
    /// <returns></returns>
    public static string KeaURLFix(string vehicleCode)
    {
        string vehicleUrl = string.Empty;
        switch(vehicleCode)
        {
            case "2K":
                vehicleUrl = "http://aurentals.keacampers.com/en/vehicles/2-berth-campervan.aspx";
                break;
            case "4K":
                vehicleUrl = "http://aurentals.keacampers.com/en/vehicles/4-berth-motorhome.aspx";
                break;
            case "6K":
                vehicleUrl = "http://aurentals.keacampers.com/en/vehicles/6-berth-motorhome.aspx";
                break;
        }
        return vehicleUrl;
    }

    /// <summary>
    /// Debug Method, TODO: invoke on debug mode
    /// </summary>
    /// <param name="st"></param>
    /// <returns></returns>
    public string QuoteTHLLogin(SourceType st, AgentLoginData aLoginData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<br/>Session ID:" + Session["SessionId"]);
        sb.Append("<br/>POS:" + st.ToString());
        sb.Append("<br />AgentSine :" + st.AgentSine);
        sb.Append("<br />AirlineVendorID:" + st.AirlineVendorID);
        sb.Append("<br />AgentDutyCode:" + st.AgentDutyCode);
        sb.Append("<br />AirportCode:" + st.AirportCode);
        sb.Append("<br />BookingChannel:" + st.BookingChannel);
        sb.Append("<br />ERSP_UserID:" + st.ERSP_UserID);
        sb.Append("<br />FirstDepartPoint:" + st.FirstDepartPoint);
        sb.Append("<br />ISOCountry:" + st.ISOCountry);
        sb.Append("<br />ISOCurrency:" + st.ISOCurrency);
        sb.Append("<br />Position:" + st.Position);
        sb.Append("<br />PseudoCityCode:" + st.PseudoCityCode);
        sb.Append("<br />RequestorID:" + st.RequestorID);
        sb.Append("<br />Agent's Currencies</br>");

        return sb.ToString();
    }

    public static bool HasNoGross(AvailabilityItem[] availabilityItems)
    {
        foreach (AvailabilityItem item in availabilityItems)
        {
            if (!item.CanGetGross && item.AVPID != null)
                return true;
        }
        return false;
    }

    /// <summary>
    /// TODO: to be moved into B2BAvailabilityHelper class 
    /// </summary>
    /// <param name="availabilityItems"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    public static string RenderAvailabilityCatalog(AvailabilityItem[] availabilityItems, B2BAvailabilityRequest request)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<div class='breadCrumb' />");
        sb.Append(@"<ul id=""vehicleCatalog"">");

        int itemCount = 0;
        
        foreach (AvailabilityItem item in availabilityItems)
        {
            itemCount++;
            BindVehicleInfo(request, item);

            string noAvailMsg = "Search online to find the following<br/>alternative travel options for this vehicle:<br /><b>Reverse route, +/- 10 days, Shorter hire.</b>";
            //string noAvailLink = "javascript:preloadPage(\"vCode=" + VehicleCode + "&rType=GetAlternateOptions&Brand=" + brandChar + "\")";
            string noAvailTitle = "Unavailable - Search Alternative Availability";

            bool shortLead = false;

            if (!string.IsNullOrEmpty(item.ErrorMessage) && item.ErrorMessage.Contains("DISP24HR"))//swap above if short leadtime detected
            {//less then 24hr lead
                item.ErrorMessage = "Travel is within 24 hours - Please search again";
                //b2b does not require contact us display custom error message the same as blocking message
                item.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                shortLead = true;//phone and no alt avail
            }

            else if (!string.IsNullOrEmpty(item.ErrorMessage) && item.ErrorMessage.Contains("DISPRateCalc"))
            {//weekly mantenance case
                item.ErrorMessage = "We are undergoing scheduled maintenance, will be back 12.15 NZST";
                item.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                shortLead = true;//phone and no alt avail            
            }

            sb.Append(@"<li class=""OptionRow Collapse"">");

            string addedLinkForInclusiveButton = "";
            if (item.AvailabilityType == AvailabilityItemType.Available || item.AvailabilityType == AvailabilityItemType.OnRequest)
            {
                decimal pta = 0m;
                decimal pap = 0m;
                string B2BPriceDetails = B2BAvailabilityDisplayer.RenderB2BPriceDetails(item, ref pta, ref pap);

                //-----------------------------------------------------------------------------------------------------
                //rev:mia MAY 8 2013  - PART OF APCOVER ENHANCEMENT. 
                //-----------------------------------------------------------------------------------------------------
                if (item.IncludesBonusPack == true)
                {
                    foreach (AvailabilityItemChargeRow ai in item.AItemChargeCollection)
                    {
                        if (ai.IsBonusPack == true)
                        {
                            AvailabilityRequest CurrentRequest = SessionManager.GetSessionData().AvailabilityRequestData;
                            bool IsGrossRequest = (CurrentRequest as B2BAvailabilityRequest).b2bParams.isGross;

                            try
                            {
                                DataAccess dal = new DataAccess();
                                string insINC = dal.GetForceInsuranceAvpId(item.AVPID, ai.ProductCode, IsGrossRequest);//is gross should be passed based on session
                                if (string.IsNullOrEmpty(insINC) == false)
                                {
                                    addedLinkForInclusiveButton = "&prdId=" + insINC;
                                    addedLinkForInclusiveButton += "&pap=" + (ai.IsCustomerCharge ? "papMode" : "ptaMode");
                                }
                            }
                            catch (Exception ex)
                            { }
                        }
                    }

                }
                else
                {
                    addedLinkForInclusiveButton = "";
                }//if (item.IncludesBonusPack == true)
                //-----------------------------------------------------------------------------------------------------


                sb.Append(@"
                        <div class=""VehicleItem"">                            
                            <a class=""VehicleThumb PopUp"" rel='" + BrandChar + @"' href=""" + VehiclePopupURL + @"""><img src='" + VehicleImage72 + @"' border='0' title='" + VehicleName + @"'/></a>
                            <div class=""VehicleFeatures"">
                                <a class=""PopUp"" rel='" + BrandChar + @"' href=""" + VehiclePopupURL + @""">" + THLBrand.GetNameForBrand(item.Brand) + " " + VehicleName + @"</a><img src='"
                                + VehicleInclusionImage + @"' border='0' title='" + VehicleCode + @"' />
                            </div>
                            <div class=""PriceDescription"">
                                <a class=""Avail"" href=""Configure.aspx?avp=" + item.AVPID + @"&inc=" + (item.IsInclusive ? "1" : "0")  + @""">AVAILABLE NOW</a>
                                    <span class='pkgInfo"+ (string.IsNullOrEmpty(item.PromoText) ? "" :" hotDeal"  )  +@"' " +
                                                            (!string.IsNullOrEmpty(item.PromoText) ? @" style=""background-image: url('" + AssetBasePath + "/CentralLibraryImages/" + THLBrand.GetNameForBrand(item.Brand) + "/Promotions/special.png" + @"');""  " : " ") +
                                    @">" + (!string.IsNullOrEmpty(item.PromoText) ? item.PromoText : "<big>" + item.PackageDescription + (!string.IsNullOrEmpty(item.FlexCode) ? " (" + item.FlexCode + ")" : string.Empty) + "</big>") +  @"</span>");

                //if (item.DisplayPromoTile)
                //    sb.Append("<span><img src='" + AssetBasePath + "/CentralLibraryImages/" + THLBrand.GetNameForBrand(item.Brand) + "/Promotions/" + item.PackageCode + ".png' title='Promotion' /></span>");
                //rev:mia April 1 2014 Payable to supplier messages
                string payablemessage = Config.GetPayablePickupMessage("PayableAtPickupText", request.b2bParams.vt.ToString());
                sb.Append(@"</div><div title=""" + item.PackageCode + @""" class=""TotalPrice"">
                                <span class='ptalabel'>Payable to agent</span>
                                <span rel='" + pta + @"' class=""pta price exRate" + (item.CanGetGross ? string.Empty : " nettprice") + @""">" + pta + @"</span>
                                <span class='paplabel'>" + payablemessage + @"</span>
                                <span rel='" + pap + @"' class=""pap price exRate"">" + pap + @"</span>
                                <a class=""ShowPriceList"">Price Details</a>
                            </div>
                            <a class=""SelectBtn"" title=""" + item.PackageDescription +  @""" href=""Configure.aspx?avp=" + item.AVPID + @"&inc=" + (item.IsInclusive ? "1" : "0") +  addedLinkForInclusiveButton +  @""">Select</a>
                        </div>
                        " + B2BPriceDetails + @"
                        <span class=""disc"">For any local payments by credit card, a credit card surcharge may apply.</span>
                    </li>
                "); 
            }
            else
            {
                bool isBlocking = (item.AvailabilityType == AvailabilityItemType.BlockingRuleTaC);

                string errMsg = item.BlockingRuleMessage + item.ErrorMessage;

                //rev:mia Sept 18 2013 - catch exception here
                bool hasAfteCallAvailability = true;
                try
                {
                    hasAfteCallAvailability = availabilityItems[0].AvailabilityType != AvailabilityItemType.Available && availabilityItems[1].AvailabilityType == AvailabilityItemType.Available && itemCount.Equals(1);
                }
                catch (Exception ex)
                {
                    hasAfteCallAvailability = false;
                }

                //if (availabilityItems[0].AvailabilityType != AvailabilityItemType.Available && availabilityItems[1].AvailabilityType == AvailabilityItemType.Available && itemCount.Equals(1))
                if (hasAfteCallAvailability)
                    sb.Append(@"
                        <div class=""step1a"">
                            <span>The vehicle you have chosen has limited availability for the dates selected.</span>
                        </div>");

                sb.Append(@"
                        <div class='VehicleItem " + (isBlocking ? "Blocking" : string.Empty) + @"'>
                            <img src='" + AssetBasePath + "/images/" + BrandChar + "/icons/rowLogo.gif' title='" + item.Brand + @"' class='RowLogo' />
                            <a href='" + VehiclePopupURL + @"' rel='" + BrandChar + @"' class='VehicleThumb PopUp'><img src='" + VehicleImage72 + @"' border='0' title='" + VehicleName + @"'/></a>
                            <div class='VehicleFeatures'>
                             <a class=""PopUp"" rel='" + BrandChar + @"' href=""" + VehiclePopupURL + @""">" + THLBrand.GetNameForBrand(item.Brand) + " " + VehicleName + @"</a><img src='"
                                + VehicleInclusionImage + @"' border='0' title='"+ VehicleCode + @"' />
                            </div>
                            <div class='Err'><span>" + errMsg + @"</span></div>
                            <div class='Limited'><div class='Top'><span><a rel='" + BrandChar + "," + VehicleCode + @"' href='#' class='aAvail" + (shortLead ? " PopUp" : string.Empty) + "'>" + noAvailTitle + "</a></span><span class='Contact'>" + noAvailMsg + "</span></div>"
                );

                if (!shortLead)
                    sb.Append("<div class='AALink LeftLnk'><a class='aAvail' rel='" + BrandChar + "," + VehicleCode + "' href='#'>Search Alternate Availability</a></div>");

                sb.Append("</div>");
                sb.Append("</div>");

                
                //if (availabilityItems[0].AvailabilityType != AvailabilityItemType.Available && availabilityItems[1].AvailabilityType == AvailabilityItemType.Available && itemCount.Equals(1))
                if(hasAfteCallAvailability)
                    sb.Append(@"
                        <div class=""step1a"">
                            <span>Other vehicles to select from:</span>
                        </div>");

            }
        }
        return sb.ToString();
    }

    public static string RenderAltAvailabilityCatalog(AlternativeAvailabilityItem[] aaItems, B2BAltAvailabilityRequest request)
    {
        BindVehicleInfo(request, aaItems[0]);

        StringBuilder sb = new StringBuilder();
        sb.Append(@"
        <div class='breadCrumb aa'></div>
        <div id='vechileCatalogAAContainer'>   
             <div id='alternativeCatalogContainer'>
                <div id='selectedVehiclePanel' rel='" + BrandChar + @"'>
                    <h3 rel='"+VehiclePopupURL+@"'>" + VehicleName + @"</h3>                     
                    <img id='selectedVImage' src='" + VehicleImage144 + @"' title='" + VehicleCode + @"' alt='" + VehicleName + @"'/>
                    <img id='selectedVIncs' src='" + VehicleInclusionImage + @"' title='Inclusions' alt='Vehicle Inclusions'/> 
                    <span class='reel'>
                        <img id='selectedVReel' src='" + VehicleImageBase + @"-OverviewThumbs.png' alt='Vehicle Reel' />       
                    </span>

                </div>");

        sb.Append(RenderAlternateRow(B2BAvailabilityDisplayer.GetOriginalItemForRequest(request), request));

        sb.Append("<div id='alternativeMessagePanel'><span>Next best availability for the " + VehicleName + "</span></div>");

        foreach (AlternativeAvailabilityItem item in aaItems)
        {
            sb.Append(RenderAlternateRow(item, request));
        }
        sb.Append("<div class='footerRow'><a class='BackBtn' href='javascript:history.go(-1)'>Back</a></div>");
        sb.Append("</div></div>");
        return sb.ToString();
    }

    public static string RenderAlternateRow(AlternativeAvailabilityItem alternateItem, B2BAltAvailabilityRequest origionalRequest)
    {
        AlternativeAvailabilityType aType = alternateItem.Type;
        bool isAvailable = (alternateItem.AvailabilityType == AvailabilityItemType.Available);

        string alternateTypeLabel = B2BAvailabilityDisplayer.GetAlternativeTypeLabel(aType);//refacor into static helper

        int spannerWidth = 400;
        int hireDays = alternateItem.HirePeriod;
        int leadTime = 5 + new Random().Next(5, 100);

        int suggestedPickUpPriorToTravel = (alternateItem.PickUpDate - origionalRequest.PickUpDate.AddDays(-10)).Days;

        decimal totalDays = 20m + origionalRequest.GetHirePeriod();
        decimal PxPerDay = spannerWidth / totalDays;
        DateTime pickUpDate = alternateItem.PickUpDate;
        DateTime dropOffDate = alternateItem.DropOffDate;
        string pickUpLoc = alternateItem.PickUpLoc, dropOffLoc = alternateItem.DropOffLoc, pickUpDateStr = pickUpDate.ToString("dd MMM yy") + "'", dropOffDateStr = dropOffDate.ToString("dd MMM yy") + "'";
        string spanClass = (aType == AlternativeAvailabilityType.OriginalSearch ? "noAvail" : "Avail");

        string brandChar = THLBrand.GetBrandChar(alternateItem.Brand).ToString();

        StringBuilder sb = new StringBuilder();
        sb.Append(@"
        <div class=""OptionRow Collapse"">
            <div class=""AltRow " + aType.ToString() + @""">
                <div class=""AltType"" title=""" + alternateItem.PackageCode + @""" rel=""" + aType.ToString() + @""">
                    <span>" + alternateTypeLabel + @"</span>
                </div>");
        if (isAvailable)
        {
            sb.Append(@"
                <div class=""AltTime"">
                    <div class=""TimeLabels period"">                
                        <span style=""width: " + (Math.Round(PxPerDay * (suggestedPickUpPriorToTravel)) + Math.Round((PxPerDay * hireDays) / 2)) + @"px;"" class=""preORDates"">&nbsp;</span>
                        <span rel=""pudate"" style=""width:40px;"" class=""hireORDates"">
                            <small>" + hireDays + @" days</small>
                        </span>
                    </div>
                    <div class=""TimeSpan"">
                        <span class=""preDates"" rel='" + PxPerDay + @"' style=""width:" + Math.Round(PxPerDay * (suggestedPickUpPriorToTravel)) + @"px;"">&nbsp;</span>
                        <span class=""hireDates " + spanClass + @""" style=""width:" + Math.Floor(PxPerDay * hireDays) + @"px;"">&nbsp;</span>
                        <div style=""display:none;"" class=""popUpTxt"">
                            <div class='availPopUp'>" + ((aType == AlternativeAvailabilityType.OriginalSearch) ? "Not Availabile" : "We checked to see if the vehicle was available if you " + B2BAvailabilityDisplayer.GetAvailabilityTextForType(aType)) + @"</div></div>
                        <span class=""postDates"">&nbsp;</span>
                    </div>
                    <div class=""TimeLabels dates"">
                        <span style=""width: " + (Math.Round(PxPerDay * (suggestedPickUpPriorToTravel)) - 20) + @"px;"" class=""preORDates"">&nbsp;</span> 
                        <span rel=""pudate"" class=""hireORDates"" style=""width: 72px;"">
                            <small>" + pickUpDateStr + " " + pickUpLoc + @"</small>
                        </span>
                        <span style=""width:" + ((((PxPerDay * hireDays)) + (Math.Round(PxPerDay * (suggestedPickUpPriorToTravel)))) < 350 ? ((PxPerDay * hireDays) - 72) : (Math.Floor(PxPerDay * hireDays) - 82))/*q&d pad extreme long */ + @"px"">&nbsp;</span>
                        <span rel=""dodate"" class=""postORDates"" style=""width: 72px;"">
                            <small>" + dropOffDateStr + " " + dropOffLoc + @"</small>
                        </span>
                    </div>
                </div>");
            if (!(aType == AlternativeAvailabilityType.OriginalSearch))
            {
                decimal pta = 0m;
                decimal pap = 0m;
                string B2BPriceDetails = B2BAvailabilityDisplayer.RenderB2BPriceDetails(alternateItem, ref pta, ref pap);
                sb.Append(@"<div class=""AltPrice"" title=""Available"">");
                /*if (alternateItem.DisplayPromoTile)
                    sb.Append(@" <img src=""" + AssetBasePath + @"/images/" + brandChar + @"/promotions/" + alternateItem.PackageCode + @".png"" class=""ads"" /> ");*/
                //rev:mia April 1 2014 Payable to supplier messages
                string payablemessage = Config.GetMessage("PayableAtPickupText");
                sb.Append(@"<span class=""ptalabel"">Payable to agent</span><span class=""exRate pta" + (alternateItem.CanGetGross ? string.Empty : " nettprice") + @""" rel=""" + pta + @""">" + pta + @"</span><span class=""paplabel"">"""+ payablemessage + @"""</span><span class=""exRate pap"" rel=""" + pap + @""">" + pap + @"</span><a class=""ShowPriceList"">Price Details</a>");
                sb.Append("</div>");
                sb.Append(@"<a href=""Configure.aspx?avp=" + alternateItem.AVPID + @"&inc=" + (alternateItem.IsInclusive ? "1" : "0") + @""" class=""AltSelect"" title=""" +  alternateItem.PackageDescription + @""">Select</a>");
                sb.Append("</div>");
                sb.Append(B2BPriceDetails);
                sb.Append("<span class=\"disc\">For any local payments by credit card, a credit card surcharge may apply.</span>");
                sb.Append("</div>");
            }
            else
                sb.Append(@"
                <div class=""noAvailability"">
                    <span>Unavailable for some of the</span>
                    <span>days requested</span> 
                </div></div>");
        }
        else//this option is unavailable
            sb.Append(@"
                <div class=""NoAlt"">
                    <span>Unavailable</span>
                    <span class=""details"">Details</span>
                        <div style=""display:none;"" class=""popUpTxt"">
                        <div class='availPopUp'>We checked to see if the vehicle was available if you " + B2BAvailabilityDisplayer.GetAvailabilityTextForType(aType) + @", but there were still days that were unavailable</div></div>
                    <small class=""noAvailX"" title=""Unavailable"">X</small>
                </div></div>");
        sb.Append("</div>");
        return sb.ToString();
    }

}