'--------------------------------------------------------------------------------------
'   Class:          AccessDenied
'   Description:    Users will be directed here when their session expires
'                  
'--------------------------------------------------------------------------------------

Partial Class AccessDenied
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here

        ' Determine what login page to direct the user back to

        If CStr(Session("TypeOfUser")) = "customer" Then
            hypReturnToLogin.NavigateUrl = "customerlogin.aspx"
        Else
            hypReturnToLogin.NavigateUrl = "agentlogin.aspx"
        End If

    End Sub

End Class
