Partial Class OTACreateBooking
    Inherits System.Web.UI.Page
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub



    Private Sub txtCreateBooking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCreateBooking.Click
        'Put user code to initialize the page here
        Try
            Dim RQ As New THL_CreateBookingRQ

            Dim RS As New OTA_VehResRS
            RQ.Version = CDec(1.0)                                        'M
            RQ.TimeStamp = Date.Now                                 'M
            RQ.Target = OTA_VehAvailRateRQTarget.Test               'M
            'RQ.EchoToken = CStr(Session("SessionId"))
            RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
            RQ.CreateBookingRQCore = New VehicleReservationRQCoreType

            'this is a new booking
            Dim tpa As New VehResRQCoreExtensionType

            RQ.CreateBookingRQCore.VendorPref = New CompanyNamePrefType
            RQ.CreateBookingRQCore.VendorPref.CompanyShortName = drpBrand.SelectedValue 'drpBrand.SelectedItem.Value
            RQ.CreateBookingRQCore.VendorPref.Code = drpVehicle.SelectedValue
            tpa.ReserveType = TransactionActionType.Book
            RQ.CreateBookingRQCore.TPA_Extensions = tpa

            'Date and location is required
            Dim vrc As New VehicleRentalCoreType
            'Dim pickUpDate As New Date(2006, 12, 25, 13, 0, 0)
            'Dim returnDate As New Date(2007, 1, 25, 13, 0, 0)

            vrc.PickUpDateTime = CDate(txtCkoDate.Text)         'M
            vrc.ReturnDateTime = CDate(txtckiDate.Text)          'M

            vrc.PickUpLocation = New LocationType(txtCkoLoc.Text, txtCkoLoc.Text)  'M
            vrc.ReturnLocation = New LocationType(txtCkiLoc.Text, txtCkiLoc.Text)  'M
            RQ.CreateBookingRQCore.VehRentalCore = vrc
            RQ.CreateBookingRQCore.RateQualifier = New RateQualifierCoreType
            RQ.CreateBookingRQCore.RateQualifier.PromotionCode = txtPkgCode.Text
            'customer data required
            Dim cusPriAddTy As New CustomerPrimaryAdditionalType
            Dim VCusType As New VehicleCustomerType
            Dim SN As New StreetNmbrType
            SN.PO_Box = "NA"

            Dim Obj_AIT As New AddressInfoType

            With Obj_AIT
                .StreetNmbr = SN
                .BldgRoom = "NA"
                Dim addressLine1 As String
                Dim addressLine2 As String

                addressLine1 = "NA"
                addressLine2 = "NA"

                .AddressLine = New String() {addressLine1, addressLine2}
                .PostalCode = "NA"
                .CityName = "NA"
                .County = "NA"
            End With

            Dim PNT As New PersonNameType
            ' Firstname can only be empty if this process was called by the add to existing booking routine
            PNT.GivenName = New String() {txtFirstName.Text}
            PNT.NameTitle = New String() {drpTitle.SelectedValue}
            PNT.Surname = txtLastName.Text

            PNT.MiddleName = New String() {txtMiddleName.Text}

            Dim TIT As New TelephoneInfoType
            Dim EType As New EmailType

            Dim CCT As New CitizenCountryNameType
            With VCusType
                .AddAddress(Obj_AIT)
                TIT.PhoneNumber = "NA"
                .Telephone = New TelephoneInfoType() {TIT}
                .Telephone(0).PhoneNumber = txtPhoneNumber.Text
                EType.Value = "Personel"
                .Email = New EmailType() {EType}
                .Email(0).Value = txtEmail.Text
                CCT.Code = txtCountry.Text
                .CitizenCountryName = New CitizenCountryNameType() {CCT}
                .PersonName = PNT
            End With

            cusPriAddTy.Primary = VCusType
            cusPriAddTy.Primary.BirthDate = Date.Now
            RQ.CreateBookingRQCore.Customer = cusPriAddTy
            RQ.CreateBookingRQInfo = New VehicleReservationRQAdditionalInfoType
            Dim ReferenceType As New VehicleReservationRQAdditionalInfoTypeReference
            ReferenceType.ID = txtBooNum.Text
            ReferenceType.ID_Context = "NA"
            ReferenceType.Type = "1"
            RQ.CreateBookingRQInfo.Reference = ReferenceType

            '-----------------------payment details------------------------------------------
            Dim oPaymentCardType As New PaymentCardType
            oPaymentCardType.CardHolderName = txtCardHolderName.Text
            oPaymentCardType.CardNumber = txtCardNumber.Text
            oPaymentCardType.ExpireDate = txtExpiryMn.Text & txtExpiryYr.Text
            oPaymentCardType.CardType = txtCardType.SelectedValue
            'oPaymentCardType.SeriesCode = "111"
            oPaymentCardType.THLAmt = txtPmtAmt.Text
            oPaymentCardType.THLDpsRef = "0000000401b0743b"


            Dim oPaymentFrType(1) As PaymentFormType

            oPaymentFrType(0) = New PaymentFormType
            oPaymentFrType(0).Item = GetType(PaymentCardType)
            oPaymentFrType(0).Item = oPaymentCardType

            Dim vehModRqInfo As New VehicleReservationRQAdditionalInfoType
            Dim vehSpecialPref As New VehicleSpecialReqPrefType
            vehSpecialPref.Value = txtNoteText.Text
            vehModRqInfo.SpecialReqPref = New VehicleSpecialReqPrefType() {vehSpecialPref}

            RQ.CreateBookingRQInfo.SpecialReqPref = New VehicleSpecialReqPrefType() {vehSpecialPref}

            'RQ.CreateBookingRQInfo.SpecialReqPref(0) = New VehicleSpecialReqPrefType
            'RQ.CreateBookingRQInfo.SpecialReqPref(0).Value = 
            RQ.CreateBookingRQInfo.RentalPaymentPref = oPaymentFrType


            '----------------------------------------------------------------------
            '------------ VEHICLE Product and Other Charges -----------------------
            '----------------------------------------------------------------------
            Dim oVehChargePurTyp() As THL_VehicleChargePurposeType
            Dim oFeeVehChargePurTyp() As THL_VehicleChargePurposeType
            Dim oCoreTypRentRate As New THL_VehicleSegmentCoreTypeRentalRate

            Dim oChargeVehChaTypCalc() As THL_VehicleChargeTypeCalculation
            Dim OvehResType As New THL_VehicleReservationType
            Dim OvehResRsCore As New VehicleRetrieveResRSCoreType
            Dim OVehSegCoreType As New THL_VehicleSegmentCoreType
            ReDim Preserve oVehChargePurTyp(1)
            oVehChargePurTyp(0) = New THL_VehicleChargePurposeType
            oVehChargePurTyp(0).Description = drpVehicle.SelectedValue
            oVehChargePurTyp(0).Amount = 1000
            oVehChargePurTyp(0).Purpose = "1"
            ReDim Preserve oChargeVehChaTypCalc(0)
            oChargeVehChaTypCalc(0) = New THL_VehicleChargeTypeCalculation
            oChargeVehChaTypCalc(0).Quantity = "10"
            oChargeVehChaTypCalc(0).UnitName = "Day"
            oChargeVehChaTypCalc(0).UnitCharge = 100
            oVehChargePurTyp(0).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(0)}
            'oCoreTypRentRate.VehicleCharges(0) = New VehicleChargePurposeType
            '--------Dummy Data --------------------
            'oCoreTypRentRate.RateDistance = New VehicleRateDistanceType
            'oCoreTypRentRate.RateDistance.Unlimited = True


            oCoreTypRentRate.RateQualifier = New RateQualifierType
            'oCoreTypRentRate.RateQualifier.RateQualifier = "1234"
            '------------- Dummy Data ------------------------
            OVehSegCoreType.RentalRate = oCoreTypRentRate
            oCoreTypRentRate.VehicleCharges = oVehChargePurTyp
            Dim NoOfDays As Long = DateDiff(DateInterval.Day, CDate(txtCkoDate.Text), CDate(txtckiDate.Text))
            Dim currentIndex As Integer = 0
            '--- Insurence Product
            If ER1.Checked Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "ER1"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = NoOfDays * 20
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = "1"
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "Day"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = NoOfDays.ToString()
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "Day"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 20
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            ElseIf ER2.Checked Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "ER2"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = NoOfDays * 38
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = "1"
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "Day"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = NoOfDays.ToString()
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "Day"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 38
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            ElseIf PrePkg.Checked Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "ER2"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = NoOfDays * 50
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = "1"
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "Day"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = NoOfDays.ToString()
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "Day"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 50
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If




            'oFeeVehChargePurTyp(0) = New VehicleChargePurposeType
            'oFeeVehChargePurTyp(0).Description = "Chair"
            'oFeeVehChargePurTyp(0).CurrencyCode = "NZD"
            'oFeeVehChargePurTyp(0).DecimalPlaces = "2"
            'oFeeVehChargePurTyp(0).Amount = 30
            'oFeeVehChargePurTyp(0).Purpose = "1"
            'oChargeVehChaTypCalc(1) = New VehicleChargeTypeCalculation
            'oChargeVehChaTypCalc(1).Quantity = "1"
            'oChargeVehChaTypCalc(1).UnitName = "RentalPeriod"
            'oChargeVehChaTypCalc(1).UnitCharge = 20
            'oFeeVehChargePurTyp(0).Calculation = New VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(1)}

            If drpBabyCapsuleQty.SelectedValue <> "0" Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "CHSEAT"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = CInt(drpBabyCapsuleQty.SelectedValue) * 25
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = drpBabyCapsuleQty.SelectedValue
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = drpBabyCapsuleQty.SelectedValue
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 25
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If

            If drpBabySeatQty.SelectedValue <> "0" Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "CHSEAT"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = CInt(drpBabySeatQty.SelectedValue) * 25
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = drpBabySeatQty.SelectedValue
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = drpBabySeatQty.SelectedValue
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 25
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If

            If drpBoostSeatQty.SelectedValue <> "0" Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "CHSEAT"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = CInt(drpBoostSeatQty.SelectedValue) * 25
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = drpBoostSeatQty.SelectedValue
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = drpBoostSeatQty.SelectedValue
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 25
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If

            If drpTableQty.SelectedValue <> "0" Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "TABLE"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = CInt(drpTableQty.SelectedValue) * 22
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = drpTableQty.SelectedValue
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = drpTableQty.SelectedValue
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 22
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If

            If drpChairQty.SelectedValue <> "0" Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "CHAIR"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = CInt(drpChairQty.SelectedValue) * 12
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = drpChairQty.SelectedValue
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = drpChairQty.SelectedValue
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 12
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If

            If drpCamPackQty.SelectedValue <> "0" Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "VAGABO"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = CInt(NoOfDays) * CInt(drpCamPackQty.SelectedValue) * 20
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = drpCamPackQty.SelectedValue
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "Day"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = drpCamPackQty.SelectedValue
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "Day"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 20
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If

            If drpTentQty.SelectedValue <> "0" Then
                ReDim Preserve oFeeVehChargePurTyp(currentIndex)
                oFeeVehChargePurTyp(currentIndex) = New THL_VehicleChargePurposeType
                oFeeVehChargePurTyp(currentIndex).Description = "TENT"
                oFeeVehChargePurTyp(currentIndex).CurrencyCode = "NZD"
                oFeeVehChargePurTyp(currentIndex).DecimalPlaces = "2"
                oFeeVehChargePurTyp(currentIndex).Amount = CInt(drpTentQty.SelectedValue) * 75
                oFeeVehChargePurTyp(currentIndex).Purpose = "1"
                oFeeVehChargePurTyp(currentIndex).THL_Quantity = drpTentQty.SelectedValue
                oFeeVehChargePurTyp(currentIndex).THL_UnitName = "RentalPeriod"

                ReDim Preserve oChargeVehChaTypCalc(currentIndex + 1)
                oChargeVehChaTypCalc(currentIndex + 1) = New THL_VehicleChargeTypeCalculation
                oChargeVehChaTypCalc(currentIndex + 1).Quantity = drpTentQty.SelectedValue
                oChargeVehChaTypCalc(currentIndex + 1).UnitName = "RentalPeriod"
                oChargeVehChaTypCalc(currentIndex + 1).UnitCharge = 75
                oFeeVehChargePurTyp(currentIndex).Calculation = New THL_VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(currentIndex + 1)}
                currentIndex = currentIndex + 1
            End If

            'ReDim Preserve oFeeVehChargePurTyp(1)
            'oFeeVehChargePurTyp(1) = New VehicleChargePurposeType
            'oFeeVehChargePurTyp(1).Description = "Table"
            'oFeeVehChargePurTyp(1).CurrencyCode = "NZD"
            'oFeeVehChargePurTyp(1).DecimalPlaces = "2"
            'oFeeVehChargePurTyp(1).Purpose = "1"
            'oFeeVehChargePurTyp(1).Amount = 40
            'ReDim Preserve oChargeVehChaTypCalc(2)
            'oChargeVehChaTypCalc(2) = New VehicleChargeTypeCalculation
            'oChargeVehChaTypCalc(2).Quantity = "2"
            'oChargeVehChaTypCalc(2).UnitName = "RentalPeriod"
            'oChargeVehChaTypCalc(2).UnitCharge = 20
            'Dim VehChargeCal(2) As VehicleChargeTypeCalculation
            '--- To Pass Multiple rate band
            'Dim VehChTypeCal() As VehicleChargeTypeCalculation
            'ReDim Preserve VehChTypeCal(0)
            'VehChTypeCal(0) = New VehicleChargeTypeCalculation
            'VehChTypeCal(0).Quantity = "2"
            'VehChTypeCal(0).UnitCharge = 20
            'VehChTypeCal(0).UnitName = "Day"

            'ReDim Preserve VehChTypeCal(1)
            'VehChTypeCal(1) = New VehicleChargeTypeCalculation
            'VehChTypeCal(1).Quantity = "3"
            'VehChTypeCal(1).UnitCharge = 60
            'VehChTypeCal(1).UnitName = "Day"

            'ReDim Preserve VehChTypeCal(2)
            'VehChTypeCal(2) = New VehicleChargeTypeCalculation
            'VehChTypeCal(2).Quantity = "1"
            'VehChTypeCal(2).UnitCharge = 0
            'VehChTypeCal(2).UnitName = "Day"

            ''oFeeVehChargePurTyp(1).Calculation = New VehicleChargeTypeCalculation() {oChargeVehChaTypCalc(2)}
            'oFeeVehChargePurTyp(1).Calculation = New VehicleChargeTypeCalculation() {}
            'oFeeVehChargePurTyp(1).Calculation = VehChTypeCal


            OVehSegCoreType.Fees = oFeeVehChargePurTyp

            OVehSegCoreType.RentalRate = New THL_VehicleSegmentCoreTypeRentalRate
            OVehSegCoreType.RentalRate = oCoreTypRentRate
            OvehResType.VehSegmentCore = New THL_VehicleSegmentCoreType
            OvehResType.VehSegmentCore = OVehSegCoreType

            RQ.CreateBookingRQInfoCore = New THL_VehicleRetrieveResRSCoreType
            RQ.CreateBookingRQInfoCore.VehReservation = New THL_VehicleReservationType
            RQ.CreateBookingRQInfoCore.VehReservation = OvehResType

            'RQ.CreateBookingRQInfoCore.VehReservation.Customer.Primary()
            'RQ.CreateBookingRQInfoCore = OvehResRsCore


            '----------------------------------------------------------------------

            Dim ser As XmlSerializer
            Dim w As StringWriter
            Dim RequestString As String
            w = New StringWriter
            ser = New XmlSerializer(GetType(THL_CreateBookingRQ))
            w = New StringWriter
            ser.Serialize(w, RQ)

            RequestString = w.ToString()

            RS = Proxy.CreateAuroraBooking(RQ)

            Dim sErrString As String = ""

            If IsNothing(RS.Errors) Then
                lblErrors.ForeColor = Drawing.Color.Green
                lblErrors.Text = "Booking Created"
            Else
                Dim errorIndex As Integer
                For errorIndex = 0 To RS.Errors.Length - 1
                    sErrString = sErrString & RS.Errors(errorIndex).Tag & "<br>"
                Next
                lblErrors.ForeColor = Drawing.Color.Red
                lblErrors.Text = sErrString
            End If

        Catch Ex As Exception
            lblErrors.Text = Ex.Message
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim RQ As New OTA_VehAvailRateRQ
        Dim mRequestorId As New UniqueID_Type
        Dim VehAvailRateRS As New THL_VehAvailabilityOnlyRS


        ' Set request header data
        RQ.Version = CDec(1.0)
        RQ.TimeStamp = Date.Now
        RQ.Target = OTA_VehAvailRateRQTarget.Test
        RQ.EchoToken = CStr(Session("SessionId"))
        RQ.MaxResponses = CStr(3)
        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}

        mRequestorId.ID = RQ.POS(0).RequestorID.ID
        mRequestorId.Type = "22"
        RQ.POS(0).RequestorID = mRequestorId

        '  Set rental locations/dates 

        RQ.VehAvailRQCore = New VehicleAvailRQCoreType
        Dim vrc As New VehicleRentalCoreType

        vrc.PickUpDateTime = CDate(txtCkoDate.Text)
        vrc.ReturnDateTime = CDate(txtckiDate.Text)

        vrc.PickUpLocation = New LocationType(txtCkoLoc.Text, txtCkoLoc.Text)
        vrc.ReturnLocation = New LocationType(txtCkiLoc.Text, txtCkiLoc.Text)

        RQ.VehAvailRQCore.VehRentalCore = vrc

        ' Set vehicle request details
        'RQ.VehAvailRQCore.VehPrefs = New VehiclePrefType() {New VehiclePrefType}

        RQ.VehAvailRQCore.Status = InventoryStatusType.Available
        RQ.VehAvailRQCore.VendorPrefs = New CompanyNamePrefType() {New CompanyNamePrefType}
        RQ.VehAvailRQCore.VendorPrefs(0).CompanyShortName = drpBrand.SelectedValue
        RQ.VehAvailRQCore.VendorPrefs(0).Code = drpVehicle.SelectedValue

        ' Get correct vehicle type from dropdown list
        Dim vehicleCategoryEnum As VehicleCategoryEnum
        Dim category As String = "v2"
        Select Case category.ToLower
            Case "v2"
                vehicleCategoryEnum = vehicleCategoryEnum.Campervan2WD
            Case "v4"
                vehicleCategoryEnum = vehicleCategoryEnum.Campervan4WD
            Case "m2"
                vehicleCategoryEnum = vehicleCategoryEnum.Motorhome2WD
            Case "m4'"
                vehicleCategoryEnum = vehicleCategoryEnum.Motorhome4WD
            Case "c2"
                vehicleCategoryEnum = vehicleCategoryEnum.Car2WD
            Case "c4"
                vehicleCategoryEnum = vehicleCategoryEnum.Car4WD
        End Select

        'RQ.VehAvailRQCore.VehPrefs(0).VehType = New VehicleTypeType(vehicleCategoryEnum)

        'RQ.VehAvailRQCore.VehPrefs(0).TypePref = PreferLevelType.Only
        'RQ.VehAvailRQCore.VehPrefs(0).ClassPref = PreferLevelType.Only
        'RQ.VehAvailRQCore.VehPrefs(0).VehClass = New VehicleClassType
        'RQ.VehAvailRQCore.VehPrefs(0).VehClass.Size = "2B"

        RQ.VehAvailRQCore.RateQualifier = New RateQualifierType
        If Not txtPkgCode.Text = "" Then
            RQ.VehAvailRQCore.RateQualifier.PromotionCode = txtPkgCode.Text
        Else
            Dim promoCode As String = "NA"
        End If

        Dim ser2 As XmlSerializer
        Dim w2 As StringWriter

        w2 = New StringWriter
        ser2 = New XmlSerializer(GetType(OTA_VehAvailRateRQ))
        w2 = New StringWriter
        ser2.Serialize(w2, RQ)

        Dim sResult1 As String = w2.ToString()
        Dim timebefore As String = Now.ToLongTimeString
        Try
            VehAvailRateRS = Proxy.VehAvailabilityOnly(RQ)
        Catch ex As Exception
            Dim serrstring As String
            serrstring = ex.Message
            Exit Sub
        End Try

        Dim timeafter As String = Now.ToLongTimeString

        Dim ser1 As XmlSerializer
        Dim w1 As StringWriter

        w1 = New StringWriter
        ser1 = New XmlSerializer(GetType(THL_VehAvailabilityOnlyRS))
        w1 = New StringWriter
        ser1.Serialize(w1, VehAvailRateRS)

        Dim sResult As String = w1.ToString()

        Response.Write(sResult)

        
        
    End Sub
End Class
