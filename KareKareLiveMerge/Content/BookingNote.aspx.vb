#Region "Notes"
'Karel Jordaan 5July2006 Added base page in inheretanse tree
#End Region

'--------------------------------------------------------------------------------------
'   Class:          BookingNote
'   Description:    Alows users to add a booking note to their existing reservation
'--------------------------------------------------------------------------------------

Partial Class BookingNote
    'Inherits System.Web.UI.Page
    Inherits THL_PageBase
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim RS As OTA_VehRetResRS

    Private Property BookingId() As String
        Get
            Return CStr(viewstate("BookingId"))
        End Get
        Set(ByVal Value As String)
            viewstate("BookingId") = Value
        End Set
    End Property

    Private Property TransactionType() As TransactionActionType
        Get
            Return CType(viewstate("Type"), TransactionActionType)
        End Get
        Set(ByVal Value As TransactionActionType)
            viewstate("Type") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'Serverside validation
        'If Not Page.IsValid() Then

        ' Set the click event of the save button to display the Loading... text
        Me.btnSave.Attributes.Add("OnClick", "javascript:BeginPageLoad();")
        lblError.Text = ""

        If Not IsPostBack Then
            ' Store Booking Id and Transaction Type (from querystring) to be used later
            If Not IsNothing(Request.QueryString("Id")) Then
                BookingId = Request.QueryString("Id")
            End If

            If Not IsNothing(Request.QueryString("Type")) Then
                Me.TransactionType = CType(Request.QueryString("Type"), TransactionActionType)
            End If
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            ' Get existing info for the current booking id
            ' inorder to repopulate modify request

            lblError.Text = ""
            GetVehicleReservation()

            If lblError.Text = "" Then
                SaveNote()
            End If

            If lblError.Text = "" Then
                UIScripts.CloseWindow(Me.Page)
            End If

        Catch ex As Exception
            lblError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        UIScripts.CloseWindow(Me.Page)
    End Sub

    Private Function GetVehicleReservation() As Boolean
        Dim success As Boolean = False
        Dim RQ As New OTA_VehRetResRQ


        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)}
        RQ.EchoToken = CStr(Session("SessionId"))
        Dim OrqCoreType As New VehicleRetrieveResRQCoreType
        RQ.VehRetResRQCore = OrqCoreType

        Dim OuniqType As New UniqueID_Type
        OuniqType.Type = CStr(22)
        OuniqType.ID = Me.BookingId
        OuniqType.Type = "NA"
        RQ.VehRetResRQCore.UniqueID = OuniqType


        Dim VehRetCoreType As New VehicleRetrieveResRQCoreType

        Dim persontype As New PersonNameType
        persontype.Surname = "NA"
        RQ.VehRetResRQCore.PersonName = persontype

        Dim tpa As New VehicleResAdditionalInfoType
        tpa.Agent = "NA"
        tpa.AgentRef = "NA"
        tpa.Package = "NA"    'not used but required as part of the request
        tpa.Status = "NA"    'not used but required as part of the request
        RQ.VehRetResRQCore.TPA_Extensions = tpa
        '------------------------------------------------------------------------------
        General.LogDebugNotes("Start BookingNote: " & General.SerializeVehRetResRQObject(RQ))
        RS = Proxy.VehRetRes(RQ)
        General.LogDebugNotes("End BookingNote: " & General.SerializeVehRetResRSObject(RS) & vbCrLf & vbCrLf & vbCrLf)


        If IsNothing(RS.Errors) Then

            success = True
        Else
            If RS.Errors(0).Tag.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblError.Text = RS.Errors(0).Tag.ToString
            success = False
        End If

        Return success
    End Function


    Private Sub SaveNote()
        Dim RQModify As New OTA_VehModifyRQ
        Dim RSModify As New OTA_VehModifyRS


        RQModify.POS = New SourceType() {CType(Session("POS"), SourceType)}
        RQModify.EchoToken = CStr(Session("SessionId"))
        ' Unique Id
        Dim OuniqId As New UniqueID_Type
        OuniqId.Type = "1"
        OuniqId.ID = Me.BookingId

        ' Locations
        Dim oRenCore As New VehicleRentalCoreType
        oRenCore.PickUpLocation = New LocationType
        oRenCore.PickUpLocation.LocationCode = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpLocation.LocationCode
        oRenCore.ReturnLocation = New LocationType
        oRenCore.ReturnLocation.LocationCode = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnLocation.LocationCode

        oRenCore.PickUpDateTime = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.PickUpDateTime

        oRenCore.ReturnDateTime = RS.VehRetResRSCore.VehReservation.VehSegmentCore.VehRentalCore.ReturnDateTime

        ' Attributes
        Dim vehModRq As New OTA_VehModifyRQVehModifyRQCore
        vehModRq.UniqueID = OuniqId
        vehModRq.VehRentalCore = oRenCore

        vehModRq.ModifyType = Me.TransactionType

        If Not txtNote.Text = "" Then
            Dim vehModRqInfo As New VehicleModifyRQAdditionalInfoType
            Dim vehSpecialPref As New VehicleSpecialReqPrefType
            vehSpecialPref.Value = txtNote.Text
            vehModRqInfo.SpecialReqPref = New VehicleSpecialReqPrefType() {vehSpecialPref}
            RQModify.VehModifyRQInfo = vehModRqInfo
        End If

        RQModify.VehModifyRQCore = vehModRq
        RSModify = Proxy.VehModify(RQModify)

        If Not IsNothing(RSModify.Errors) AndAlso RSModify.Errors.Length > 0 Then
            'an error occured, display it
            Dim errorString As String
            Dim i As Integer
            For i = 0 To RSModify.Errors.Length - 1
                errorString &= RSModify.Errors(i).Tag & "<br>"
            Next
            If errorString.ToString.Split(CChar("/"))(0).ToUpper = "SESSIONEXP" Then
                Session("POS") = Nothing
                Response.Redirect("AccessDenied.aspx")
            End If
            lblError.Text = errorString
        End If

    End Sub
End Class
