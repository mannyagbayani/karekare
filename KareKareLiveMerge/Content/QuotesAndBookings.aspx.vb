'--------------------------------------------------------------------------------------
'   Class:          QuotesAndBookings
'   Description:    Shows a list of all current quotes and bookings
'--------------------------------------------------------------------------------------

Imports System.Xml.Serialization
Imports System.IO
Imports System.Web.Hosting
Imports THL.B2B.VBClasses

'Imports THLRentals.OTA.Utils


Partial Class QuotesAndBookings
    Inherits System.Web.UI.Page
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA
    Protected WithEvents BookingContainer1 As BookingContainer


#Region "Currency Changes"
    Public Property MRU_CurrencyMultiplier() As Double
        Get
            If Session("MRU_CurrencyMultiplier") IsNot Nothing Then
                Return CDbl(Session("MRU_CurrencyMultiplier"))
            Else
                Return 0.0
            End If
        End Get
        Set(ByVal value As Double)
            Session("MRU_CurrencyMultiplier") = value
        End Set
    End Property

    Public Property MRU_CurrencyCode() As String
        Get
            Return CStr(Session("MRU_CurrencyCode"))
        End Get
        Set(ByVal value As String)
            Session("MRU_CurrencyCode") = value
        End Set
    End Property

#End Region


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        '   General.MaintainSessionObjects(General.PageName.QuotesAndBookings, Session)
        'If IsNothing(Request.QueryString("ignore")) Then
        If IsNothing(Session("POS")) Then
            ' redirect to redirect page
            Response.Redirect("AccessDenied.aspx")
        End If
        'End If

        '' currency change
        '' nuke the MRU objects
        'MRU_CurrencyCode = ""
        'MRU_CurrencyMultiplier = 0.0
        '' currency change
    End Sub

#End Region


    Private nAssetBaseBath As String
    Public Property AssetBasePath() As String
        Get
            Dim isSecure As Boolean = False
            Boolean.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings("SecureMode"), isSecure)

            Dim prefix As String = IIf(isSecure, "https://", "http://").ToString()
            Return prefix + System.Web.Configuration.WebConfigurationManager.AppSettings("AssetsURL")
        End Get
        Set(ByVal value As String)
            nAssetBaseBath = value
        End Set
    End Property
    Public Property BackgroundURL() As String
        Get
            Return CStr(ViewState("BackgroundURL"))
        End Get
        Set(ByVal Value As String)
            ViewState("BackgroundURL") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        ''Me.Page.SmartNavigation = False
        Me.Page.MaintainScrollPositionOnPostBack = False
        If Not IsPostBack Then
            Me.BackgroundURL = General.BackGroundImageURL(Session)
            ' Load booking list control (done from inside booking container)
            BookingContainer1.InitBookingMode(BookingContainer.BookingMode.BookingList, "", TransactionActionType.Book)
            confirmationtable.Visible = False
        End If

        ''rev:mia-25March2015 - a new library added
        B2BAgent = GetB2BAgent()
        Dim vbhelper As New Helper()
        Dim agentemail As String = vbhelper.B2BAgentEmailAddress(B2BAgent)

        If IsAgentlHasAvalidEmail(agentemail) Then
            If String.IsNullOrEmpty(emailConfirmationSentToBox.Value) Then
                emailConfirmationSentToBox.Value = agentemail
            End If
        Else
            BookingContainer1.GetInstanceOfStandardConfirmation.SendingEmailErrorMessage = "Agent email address is not specified or invalid. Please contact reservations." ''agentemail.Replace("/Multiple", "").Replace("/Empty", "")
        End If

        AddHandler BookingContainer1.GetInstanceOfStandardConfirmation.BubbleClick, AddressOf BubbleClick

    End Sub


#Region "rev:mia 25March2015 B2B Auto generated Aurora Booking Confirmation"
    Private B2BAgent As String

    Private Function GetB2BAgent() As String
        Dim b2bagentcode As String = ""

        'todo: its should be getting from dbase
        If Session("agentinformation.agentcode") IsNot Nothing Then
            b2bagentcode = Session("agentinformation.agentcode").ToString()
        Else
            If Request.Cookies("agentinformation") IsNot Nothing Then
                b2bagentcode = Request.Cookies("agentinformation")("agentcode")
            End If
        End If
        Return b2bagentcode
    End Function

    Private Function IsAgentlHasAvalidEmail(agentemail As String) As Boolean
        If agentemail.Contains("ERROR") = True Then
            ButtonSendEmail.Enabled = False
            emailConfirmationSentToBox.Disabled = True
            confirmationmessage.ForeColor = System.Drawing.Color.Red
            agentemail = "Agent email address is not specified or invalid. Please contact reservations." ''agentemail.Replace("/Multiple", "").Replace("/Empty", "").Replace("ERROR:", "")
            Return False
        Else
            ClearErrorMessage()
        End If


        Return True
    End Function

    Private Sub ClearErrorMessage()
        confirmationmessage.Text = ""
        ButtonSendEmail.Enabled = True
        emailConfirmationSentToBox.Disabled = False
    End Sub

    Private Sub BubbleClick(sender As Object, e As EventArgs)
        If (BookingContainer1.GetInstanceOfStandardConfirmation.GetBookingStatus <> "Quotation") Then
            If BookingContainer1.GetInstanceOfStandardConfirmation.IsCustomerMode Then
                confirmationtable.Visible = False
            Else
                confirmationtable.Visible = True
            End If
        Else
            confirmationtable.Visible = False
        End If

    End Sub


    Protected Sub ButtonSendEmail_Click(sender As Object, e As EventArgs)
        'make email default to the agent for both sender and receiver
        ClearErrorMessage()
        Dim email As String = ""

        'check if page is valid
        If Not Page.IsValid Then

            ButtonSendEmail.Enabled = True
            Return
        End If

        If Not String.IsNullOrEmpty(emailConfirmationSentToBox.Value) Then
            email = emailConfirmationSentToBox.Value
        Else
            ButtonSendEmail.Enabled = True
        End If


        If Not String.IsNullOrEmpty(email) Then
            If String.IsNullOrEmpty(B2BAgent) Then
                B2BAgent = GetB2BAgent()
            End If

            Dim vbhelper As New Helper()
            Dim agentemail As String = vbhelper.B2BAgentEmailAddress(B2BAgent)

            If (agentemail.Contains("@") = True) Then
                Dim splitemail() As String = agentemail.Split("@"c)
                If (Not splitemail(1).Contains(".")) Then
                    confirmationmessage.Text = "Agent email address is not specified. Please contact reservations"
                    confirmationmessage.ForeColor = System.Drawing.Color.Red
                    Exit Sub
                End If
            Else
                confirmationmessage.Text = "Agent email address is not specified. Please contact reservations"
                confirmationmessage.ForeColor = System.Drawing.Color.Red
                Exit Sub
            End If

            If IsAgentlHasAvalidEmail(agentemail) Then
                Dim content As String = BookingContainer1.GetInstanceOfStandardConfirmation.ConfirmationRawContent
                Dim subject As String = BookingContainer1.GetInstanceOfStandardConfirmation.GetSubject()
                Dim result As String = vbhelper.B2BAutoSendEmail(email, subject, content, B2BAgent)
                ButtonSendEmail.Enabled = True
                confirmationmessage.ForeColor = (If(result.Contains("OK"), System.Drawing.Color.Blue, System.Drawing.Color.Red))
                result = result.Replace("OK:", "").Replace("ERROR:", "")
                confirmationmessage.Text = result
            End If
            vbhelper = Nothing
        End If
    End Sub

#End Region

End Class