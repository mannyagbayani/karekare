<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CustomerLogin.aspx.vb" Inherits="THLAuroraWebInterface.CustomerLogin"%>
<%@ Register TagPrefix="uc1" TagName="SignIn" Src="../Controls/SignIn/SignIn.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Register" Src="../Controls/SignIn/Register.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CustomerLogin</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<FORM id="Form1" method="post" runat="server">
			<table width="100%" height="100%">
				<tr>
					<td align="center" valign="middle">
						<table id="Table2" borderColor="#999999" cellSpacing="0" cellPadding="0" border="2">
							<tr>
								<td>
									<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="300" border="0">
										<TR>
											<TD><uc1:signin id="SignIn1" runat="server"></uc1:signin></TD>
										</TR>
										<TR>
											<TD>
												<asp:Label id="lblConfirmation" runat="server" Visible="False" CssClass="warning">Confirmation details have been sent to your email address. Enter your password and click Login to proceed.</asp:Label></TD>
										</TR>
										<TR>
											<TD>
												<uc1:Register id="Register1" runat="server"></uc1:Register></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</FORM>
	</body>
</HTML>
