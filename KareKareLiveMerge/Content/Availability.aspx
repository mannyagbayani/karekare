﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Availability.aspx.cs" Inherits="Content_Availability" %>

<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Availability</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=11" />
    <link href="/css/b2b_local.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Styles.css" />

    <link href="<%= AssetBasePath %>/css/b/jcal.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<%= AssetBasePath %>/css/b2b/jquery.ui.autocomplete.css" />
    <link href="<%= AssetBasePath %>/css/b2b/bc.css" rel="stylesheet" type="text/css" />

    <%--<link href="/css/jcal.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/jquery.ui.autocomplete.css" />
    <link href="/css/bc.css" rel="stylesheet" type="text/css" />--%>
    
    <script src="<%= HTTPPrefix %>ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js" type="text/javascript"></script>
    <script src="<%= HTTPPrefix %>ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="/js/jquery.ba-bbq.min.js" type="text/javascript"></script>
    <script src="/js/bc.js?v=997" type="text/javascript"></script>
    <script src="/js/cs.js?v=997" type="text/javascript"></script>
    <script type="text/javascript">
            
            var ctrlParams = { countries :  <%= CtrlJSONObj %>};
            var enhancedUI = enableEUI();
            <%= EnhancedUI %>
            var agentData = <%= aLoginDataJSONObj %>;
            //var openHours = [{'vt':'av','cc':'au','pt':7, 'dt':17},{'vt':'ac','cc':'au','pt':0, 'dt':23},{'vt':'av','cc':'nz','pt':7, 'dt':17},{'vt':'ac','cc':'nz', 'pt':0, 'dt':23}];
			var openHours = [{'vt':'av','cc':'au','pt':7, 'dt':17, 'startTime':'10:00', 'endTime':'15:00'},{'vt':'ac','cc':'au','pt':0, 'dt':23, 'startTime':'10:00', 'endTime':'15:00'},{'vt':'av','cc':'nz','pt':7, 'dt':17, 'startTime':'10:00', 'endTime':'15:00'},{'vt':'ac','cc':'nz', 'pt':0, 'dt':23, 'startTime':'10:00', 'endTime':'15:00'},{'vt':'av','cc':'us','pt':7, 'dt':17, 'startTime':'12:30', 'endTime':'09:00'},{'vt':'ac','cc':'us','pt':0, 'dt':23, 'startTime':'12:30', 'endTime':'09:00'}];
            var state = <%= StateJSON %>;
            var csToken = <%= TokenJson %>;
           
            var baseNZAUHtmlOfBrandSelector ="";
            var baseUSHtmlOfBrandSelector ="";
            var oldBrand = ""            

             //rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT 
            var minifyCurrency = <%= minifyCurrency %>;
            var b2bParamUrl = '<%= b2bParamUrl %>'

            $(document).ready(function(){
               // debugger;
              
               init();
               csellFrame = $("#csellFrame")[0].contentWindow

               $('#brandSelector').trigger('change');//-define defaults
                hideSameDayMessage()
                //rev:mia April 16 2012
                  $('span.spanCountries').hide()
                  $('span.sectionCountriesDD').removeClass("errormsg countriesDD")
                  
                  if (crossSellData.iscrosssell == true) {
                      $('span.spanCountries').show()
                  }
                
                
                var ctrlState = getCurrentSelection(); //retrieve history and update locations
	            if(typeof ctrlState != 'undefined' && ctrlState != null)
                {
                    if(ctrlState.pb == undefined && ctrlState.db == undefined)
                    {
                        $('#pb').val(-1);
	                    $('#db').val(-1);
                    }
                    else
                    {
                        $('#pb').val(ctrlState.pb);
	                    $('#db').val(ctrlState.db);
                    }
	                refreshCombobox([$('#pb'), $('#db')]);
                }

                //rev:mia- aug.28, 2013 - addition of roadbear and US britz
                var UShasSelected = false;
                var AUNZShasSelected = false;
                $("#brandSelector option").each(function() 
                {
                    var val = $(this).val()
                    var txt = $(this).text()
                    var cls = $(this).attr("class")
                 
                    if(cls == "USbrand")
                    {

                        if(!UShasSelected)
                        {
                            if($(this).is(":selected")) baseUSHtmlOfBrandSelector = baseUSHtmlOfBrandSelector + "<option class='USbrand' value = '" + val + "' selected='selected'>"
                            else  baseUSHtmlOfBrandSelector = baseUSHtmlOfBrandSelector + "<option class='USbrand' value = '" + val + "'>"
                            UShasSelected = $(this).is(":selected");
                        }
                        else
                            baseUSHtmlOfBrandSelector = baseUSHtmlOfBrandSelector + "<option class='USbrand' value = '" + val + "'>"
                            baseUSHtmlOfBrandSelector = baseUSHtmlOfBrandSelector + txt + "</option>"
                    }
                    else
                    {
                        if(!AUNZShasSelected)  
                        {
                            if($(this).is(":selected")) baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector + "<option class='AUNZbrand' value = '" + val + "' selected='selected'>"
                            else baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector + "<option class='AUNZbrand' value = '" + val + "'>"
                            AUNZShasSelected = $(this).is(":selected");
                        }
                        else 
                            baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector + "<option class='AUNZbrand' value = '" + val + "'>"
                            baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector + txt + "</option>"
                    }
                  
                });
                
                $("#brandSelector  option.USbrand").optVisible(false);

                //var selvalue = 'Maui';
                //var optionItem;
                $("input[name='cc']").change(function() {
                    var isUS = $(this).val() == "US" && $(this).is(":checked")
                    //debugger;
                    if(oldBrand == '') oldBrand = $("#brandSelector option:selected").val()
                    if(oldBrand  != $("#brandSelector").val())  oldBrand = $("#brandSelector option:selected").val()
                    //alert( $("#brandSelector").val() + " " + oldBrand)

                    if(isUS)
                        { 
                          $("#brandSelector").html(baseUSHtmlOfBrandSelector)
                          showSameDayMessage()
                          
                        }
                    else
                        {
                            if(oldBrand != 'Z')
                            {
                                baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector.replace("selected='selected'","")
                                baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector.replace("option class='AUNZbrand' value = '" + oldBrand +"'","option class='AUNZbrand' value = '" + oldBrand +"' selected='selected'")
                            }
                            else
                            {
                              baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector.replace("selected='selected'","")
                              baseNZAUHtmlOfBrandSelector = baseNZAUHtmlOfBrandSelector.replace("option class='AUNZbrand' value = 'M'","option class='AUNZbrand' value = 'M' selected='selected'")
                            }
                            $("#brandSelector").html(baseNZAUHtmlOfBrandSelector)
                            hideSameDayMessage()
                        }
                         $("#brandSelector").next("input").val($("#brandSelector option:selected").text())
                        
                });

                 if (window.location.href.indexOf("b2bAll") != -1 && window.location.href.indexOf("&brand") == -1) {
                    submitCrossSell();
                }
            }); 


            //rev:mia- aug.28, 2013 - addition of roadbear and US britz
            $.fn.optVisible = function( show ) {
                if( show ) 
                {
                    this.filter( "span > option" ).unwrap();
                } else 
                {
                    this.filter( ":not(span > option)" ).wrap( "<span>" ).parent().hide();
                }
                return this;
            }      
    </script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35407162-1']);
        _gaq.push(['_setDomainName', 'thlrentalsb2b.com']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <style type="text/css">
        #vehicleCatalog .chargesTable td.rt
        {
            width: 240px;
        }
        
        #pdPanel.error input.smart-combobox-input {
            border-color: #DA471F !important;
            border-right: 0 none;
            border-width: 2px 0 2px 2px;            
        }
        
        /* cross sell */
        #csellFrame {            
            overflow: hidden;
            height: 2500px;
        }
        
        #catalogContainer.loading{
            background: transparent url('../../images/b2b/buttons/Continue-to-payment-submit.gif') 480px 200px no-repeat;
        }        
        
        #catalogContainerClone .loading{
            background: transparent url('../../images/b2b/buttons/Continue-to-payment-submit.gif') 480px 200px no-repeat;
        }        
        /* cross sell ends */
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="priceAvailabilityContainer">
        <div id="loadingDialog">
            <div class="ui-widget-overlay">
            </div>
            <span>loading...</span>
        </div>
        <uc1:Header ID="Header1" runat="server"></uc1:Header>
        <div id="bookingControl">
            <div id="ctrlHead">
                <span class="left"><big>Your search <em style="text-decoration:underline;"></em>:</big> </span><span class="bookingData"></span>
            </div>
            <div id="ctrlBody">
                <div id="leftPanel" style="height:475px;">
                    <div id="vehicleBrandPanel">
                        <label class="section">
                            Country</label>
                        <span class="Radios section" id="spanCountries">
                            <input type="radio" name="cc" value="AU" id="ccAU" rel="AUD" checked="checked" class="smart-radio-button" />
                            <label for="ccAU">
                                AU</label>
                            <input type="radio" name="cc" value="NZ" id="ccNZ" rel="NZD" class="smart-radio-button" />
                            <label for="ccNZ">
                                NZ</label>
                            <!--rev:mia Aug.26 20130 addition of USA-->
                            <input type="radio" name="cc" value="US" id="ccUS" rel="USD" class="smart-radio-button" />
                            <label for="ccUS">
                                US</label>
                        </span>
                        <label class="section">Brand</label>
                        <%= AgentsBrandRadios %>
                        <label for="av" class="section">
                            Vehicle type</label>
                        <span class="Radios" id="spanVehicleType">
                            <input type="radio" name="vt" value="av" id="avRadio" class="smart-radio-button"
                                rel="Campervan" checked="checked" />
                            <label for="avRadio">
                                Campervan</label>
                            <input type="radio" name="vt" value="ac" id="acRadio" rel="Car" class="smart-radio-button" />
                            <label for="acRadio">
                                Car</label>
                        </span>
                        <label for="vehicleDD" class="section">
                            Vehicle model</label>
                        <select id="vehicleDD" class="combobox long-dd">
                            <option value="0">All Models</option>
                        </select>
                    </div>
                    <div id="b2bPanel">                    
                        <label for="agentsDD" class="section">
                            Agent code</label>
                        <select id="agentsDD" class="combobox long-dd">
                        </select>                        
                        <span class="spanCountries">
                        <label for="countriesDD" class="section">
                            Driver's Licence
                        </label>
                        <select id="countriesDD" class="combobox long-dd" name="countriesDD">
                            <option value="0" selected="selected">Please select</option>
                            <option value="AU">Australia</option>
                            <option value="CA">Canada</option>
                            <option value="DK">Denmark</option>
                            <option value="FR">France</option>
                            <option value="DE">Germany</option>
                            <option value="NL">Netherlands</option>
                            <option value="NZ">New Zealand</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="UK">United Kingdom</option>
                            <option value="US">United States Of America</option>
                            <option value="">--------------------</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andora</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua & Barbados</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BO">Bolivia</option>
                            <option value="BA">Bosnia and Herzegovina</option>
                            <option value="BW">Botswana</option>
                            <option value="BV">Bouvet Island</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="BN">Brunei</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="BT">Butan</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Cental African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos (Keeling) Island</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CG">Congo</option>
                            <option value="CK">Cook Island</option>
                            <option value="CR">Costa Rica</option>
                            <option value="CI">Cote D'ivoire</option>
                            <option value="HR">Croatia</option>
                            <option value="CU">Cuba</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czech Republic</option>
                            <option value="DJ">DjiboutiI</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="TP">East Timor</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FX">France, Metropolitan</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Souther Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-Bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HM">Heard and McDonald Islands</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Lao Peoples Democratic Republic</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macau</option>
                            <option value="MK">Macedonia</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia, Federated States of</option>
                            <option value="MD">Moldova</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanamar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="AN">Netherlands Antilles</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="KP">North Korea</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn Islands</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="RE">Reunion</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russian Federation</option>
                            <option value="RW">Rwanda</option>
                            <option value="SH">Saint Helena</option>
                            <option value="KN">Saint Kitts and Nevis</option>
                            <option value="LC">Saint Lucia</option>
                            <option value="PM">Saint Pierre and Miquelon</option>
                            <option value="VC">Saint Vincent and The Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome and Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="SC">Seychelles</option>
                            <option value="SG">Singapore</option>
                            <option value="SK">Slovakia</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia</option>
                            <option value="KR">South Korea</option>
                            <option value="ES">Spain</option>
                            <option value="SL">Sri Lanka</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbardand Jan Mayen Islands</option>
                            <option value="SY">Syria</option>
                            <option value="TI">Tahiti</option>
                            <option value="TW">Taiwan</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania</option>
                            <option value="TH">Thailand</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad and Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks and Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="UY">Uruguay</option>
                            <option value="UM">US Minor Outlying Islands</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VA">Vatican City</option>
                            <option value="VE">Venezuala</option>
                            <option value="VN">Vietnam</option>
                            <option value="VG">Virgin Islands (GB)</option>
                            <option value="VI">Virgin Islands (US)</option>
                            <option value="WF">Wallis and Futuna Islands</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="YU">Yugoslavia</option>
                            <option value="ZR">Zaire</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select>
                         <span class="errormsg countriesDD  sectionCountriesDD"></span>
                        </span>
                        
                        <label class="section">
                            Promo code <small>&nbsp;(optional)</small></label>
                        <select id="packagesDD" class="combobox long-dd">
                            <option value="0">All Packages</option>
                        </select>
                    </div>
                </div>
                <div id="rightPanel">
                    <div id="locAndDates">
                        <div id="pdPanel" class="locPanel">
                            <label for="pd" class="section">
                                Pick up from</label>
                            <select id="pb" name="pb" class="locSelector combobox long-dd">
                            </select>
                            <span class="errormsg pb"></span>
                            <label class="section datelbl">
                                Pick up date</label>
                            <!-- <input type="text" id="from-date" class="fromDate datepicker" />-->
                            <span id="fromDateDiv" style="width: 100px; height: 100px;" class="fromDate"></span>
                        </div>
                        <div id="ddPanel" class="locPanel">
                            <label for="db" class="section">
                                Drop off at</label>
                            <select id="db" name="db" class="locSelector combobox long-dd">
                            </select>
                            <span class="errormsg db"></span>
                            <label class="section datelbl">
                                Drop off date</label>
                            <!--<input type="text" id="to-date" class="datepicker" />-->
                            <span id="toDateDiv" style="width: 100px; height: 100px;"></span>
                        </div>
                    </div>
                    <div id="timeMessage" style="display;block;clear:both;float:left;font-size:12px;width:440px;color:Red;">
                        <strong>Important - </strong>please specify accurate pick up and drop off times.  Pick up and  drop off outside of these times may cause a delay and impact the time spent at the branch.
                    </div>                   
                    <div id="sameDayMessage" style="display;block;clear:both;float:left;font-size:12px;width:440px;color:Red;">
                    <br />
                        Vehicle cannot be picked on the same day of arrival on an inter continental flight.
                    </div>
                    <div id="timeContainer">
                        <div class="timePanel">
                            <span class="section">
                                <label for="pt">
                                    Pick up time</label>
                                <small>&nbsp;(Optional)</small> </span>
                            <select id="pt" class="timeSelector combobox long-dd">
                                <option value="07:30">07:30 AM</option>
                                <option value="08:00">08:00 AM</option>
                                <option value="08:30">08:30 AM</option>
                                <option value="09:00">09:00 AM</option>
                                <option value="09:30">09:30 AM</option>
                                <option value="10:00" selected="selected">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">13:00 PM</option>
                                <option value="13:30">13:30 PM</option>
                                <option value="14:00">14:00 PM</option>
                                <option value="14:30">14:30 PM</option>
                                <option value="15:00">15:00 PM</option>
                                <option value="15:30">15:30 PM</option>
                                <option value="16:00">16:00 PM</option>
                                <option value="16:30">16:30 PM</option>
                                <option value="17:00">17:00 PM</option>
                            </select>
                        </div>
                        <div class="timePanel">
                            <span class="section">
                                <label for="dt">
                                    Drop off time</label><small>&nbsp;(Optional)</small></span>
                            <select id="dt" class="timeSelector combobox long-dd">
                                <option value="07:30">07:30 AM</option>
                                <option value="08:00">08:00 AM</option>
                                <option value="08:30">08:30 AM</option>
                                <option value="09:00">09:00 AM</option>
                                <option value="09:30">09:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">13:00 PM</option>
                                <option value="13:30">13:30 PM</option>
                                <option value="14:00">14:00 PM</option>
                                <option value="14:30">14:30 PM</option>
                                <option value="15:00" selected="selected">15:00 PM</option>
                                <option value="15:30">15:30 PM</option>
                                <option value="16:00">16:00 PM</option>
                                <option value="16:30">16:30 PM</option>
                                <option value="17:00">17:00 PM</option>
                            </select>
                        </div>
                    </div>
                    <div id="paxContainer">
                        <div class="paxPanel">
                            <label for="na" class="section">
                                Adults</label>
                            <select id="na" class="combobox short-dd">
                                <option value="1" selected="selected">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="paxPanel">
                            <label for="nc" class="section">
                                Children</label>
                            <select id="nc" class="combobox short-dd">
                                <option value="0" selected="selected">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div>
                            <button id="getAvailSubmit" type="button">
                                GET AVAILABILITY</button>
                            <span class="loader"></span>
                        </div>
                    </div>
                    <div id="errorPanel">
                        <span>&nbsp;</span>
                    </div>
                    <input type="hidden" id="brand" name="brand" value="b" />
                    <input type="hidden" id="sc" name="sc" value="rv" />
                    <input type="hidden" id="vtype" name="vtype" value="rv" />
                    <input type="hidden" id="pd" name="pd" value="1" />
                    <input type="hidden" id="pm" name="pm" value="8" />
                    <input type="hidden" id="py" name="py" value="2011" />
                    <input type="hidden" id="dd" name="dd" value="10" />
                    <input type="hidden" id="dm" name="dm" value="8" />
                    <input type="hidden" id="dy" name="dy" value="2011" />
                </div>
            </div>
        </div>
        <div id="availabilityFilter">
            <span class="lbl">Display:</span> <span class="Radios section">
                <input type="radio" name="grsnett" value="gross" class="smart-radio-button" id="gnGross" />
                <label for="gnGross">
                    Gross Amount</label>
                <input type="radio" name="grsnett" value="nett" class="smart-radio-button" id="gnNett" />
                <label for="gnNett">
                    Nett Amount</label>
            </span><span class="curSelector">
                <label for="currencySelector">
                    Alternative Currency</label>
                <select id="currencySelector" class="combobox long-dd">
                    <!--  Generate Dynamically by current agent<->Country of travel  -->
                </select>
            </span>
        </div>

        <div id="catalogContainer" class="catalogContainer">
            
        </div>

        <div id="catalogContainerClone" class="catalogContainer">
            <iframe runat="server" id="csellFrame" frameborder="0" src="about:blank" width="973px" height="600px" scrolling="no" /> 
        </div>
        <div id="debugPanel" style="float: left; clear: both; ">
            <%=  DebugStr %>
        </div>
    </div>
    </form>
</body>
</html>
