'--------------------------------------------------------------------------------------
'   Class:          PrinterFriendlyPageaspx
'   Description:    This page loads up the standard confirmation screen but strips
'                   out all widths over 560px inorder to make printer friendly
'--------------------------------------------------------------------------------------

Imports System.IO
Imports System.Net
Imports System.Text
Partial Class PrinterFriendlyPageaspx
    Inherits System.Web.UI.Page

   

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	'NOTE: The following placeholder declaration is required by the Web Form Designer.
	'Do not delete or move it.
	Private designerPlaceholderDeclaration As System.Object

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

	Protected WithEvents StandardConfirmation1 As StandardConfirmation

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		'Put user code to Initialize the page here

		If Not IsNothing(Request.QueryString("print")) Then
            Dim bookingId As String = Server.UrlDecode(Request.QueryString("print"))
            Dim CustomerConfirmationMode As Boolean = False
            If Request("CustomerConfirmationMode") IsNot Nothing Then
                CustomerConfirmationMode = CBool(Request("CustomerConfirmationMode"))
            End If

            ' Load the page first
            StandardConfirmation1.InitStandardConfirmation(bookingId, True, CustomerConfirmationMode)
            ' Strip out all widths over 560pxs and replace with 560px
            GetHtml()
            'Get the print box to popup
            Me.Page.RegisterStartupScript("PrintPage", "<script type=""text/javascript"">windpow.print()</script>")

        End If
	End Sub
   

	Private Sub GetHtml()
		Dim sb As New StringBuilder
		Dim sw As StringWriter = New StringWriter(sb)
		Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        MyBase.Render(hw)
		Dim pageHtml As String = sb.ToString()

		Dim i As Integer
		Dim arrC() As String = pageHtml.Split(CType(":", Char))
        For i = 0 To arrC.Length - 1
            If arrC(i).ToString.Length > 2 Then
                Dim number As String = arrC(i).ToString.Substring(0, 3)
                If IsNumeric(number) Then
                    If CInt(number) > 580 Then
                        pageHtml = pageHtml.Replace(number, "560")
                    End If 'If CInt(number) > 580 Then 

                End If  ' If IsNumeric(number) Then
            End If ' If arrC(i).ToString.Length > 2 Then
        Next

        Dim i2 As Integer
        Dim arrC2() As String = pageHtml.Split(CType("=", Char))
        For i2 = 0 To arrC2.Length - 1
            Dim number As String
            If arrC2(i2).Substring(0, 1) = """" Then
                number = arrC2(i2).ToString.Substring(0, 5)
            Else
                number = arrC2(i2).ToString.Substring(0, 3)
            End If
            If IsNumeric(number.replace("""", "")) Then
                If CInt(number.replace("""", "")) > 580 Then
                    pageHtml = pageHtml.Replace(number, """560""")
                End If

            End If
        Next
        Session("PageHtml") = pageHtml
	End Sub

	Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
		Dim pageHtml As String = CStr(Session("PageHtml"))
		writer.Write(pageHtml)
    End Sub

    'Private Sub readHtmlPage()
    '	Dim objResponse As WebResponse
    '	Dim objRequest As WebRequest
    '	Dim objRespnse As WebResponse
    '	Dim result As String
    '	Dim url As String = Me.Request.QueryString("page")		 ' & "?ignore=yes"
    '	objRequest = System.Net.HttpWebRequest.Create(url)
    '	objResponse = objRequest.GetResponse()

    '	'use UTF7 Encoding to read special charachters and the Norwegian alphabet
    '	Dim sr As New StreamReader(objResponse.GetResponseStream(), System.Text.Encoding.UTF7)
    '	result = sr.ReadToEnd().ToString
    '	'clean up StreamReader
    '	sr.Close()
    '	Session("result") = result
    '	getHtml()
    'End Sub

End Class
