﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Save.aspx.cs" Inherits="Content_Save" %>

<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Submit your Booking</title>
    
    <!--<link rel="stylesheet" type="text/css" href="<%= AssetBasePath %>/css/b2b/jquery.ui.autocomplete.css" />-->
    <link href="<%= AssetBasePath %>/css/b2b/bc.css" rel="stylesheet" type="text/css" />
    <link href="<%= AssetBasePath %>/css/b2b/configure.css" rel="stylesheet" type="text/css" />
    <!-- TODO: remove once merged -->
    <link href="<%= AssetBasePath %>/css/b2b/save.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Styles.css" />
    <%--<link href="http://secure.thlonline.com/SiteCollectionDocuments/css/Base.css?v=2.8" type="text/css" rel="stylesheet" />
    <link href="http://secure.thlonline.com/SiteCollectionDocuments/css/c/configure.css?v=2.8" type="text/css" rel="stylesheet" />--%>

    <script src="<%= HTTPPrefix %>ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
    <script src="<%= HTTPPrefix %>ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="/js/jquery.ba-bbq.min.js" type="text/javascript"></script>
    <script src="<%= HTTPPrefix %>ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/js/bc.js" type="text/javascript"></script>

  
     

    <script src="/js/configure.js?v=1.1" type="text/javascript"></script>
    <!-- REfactor into above -->
  
    <script type="text/javascript">
        var selectedExRate = { "code": "<%= SelectedCurrencyRate.ToCode %>", "rate": "<%= SelectedCurrencyRate.Rate %>" };
        var currencyStr = '<%= CurrencyStr %>';
       

        $(document).ready(function () {
       
           $('#saveBackButton').click(function()
            {
                     var configurl = $.cookie('urlForConfigurePage');
             
                     if (getUrlVars()["b2bAll"] != null) {
                           window.location.href= "/content/configure.aspx" + configurl
                     }
                     else                        
                        window.history.back();
                        
            });

               //------------------------------------------------------ 
               //rev:mia  09April2015 - added to allow multiple emails
               //------------------------------------------------------
               $.validator.addMethod(
                    "multiemails",
                     function (value, element) {
                         if (this.optional(element)) // return true on optional element
                             return true;
                         var emails = value.split(/[;,]+/); // split element by , and ;
                         valid = true;
                         for (var i in emails) {
                             value = emails[i];
                             valid = valid &&
                                     jQuery.validator.methods.email.call(this, $.trim(value), element);
                         }
                         return valid;
                     },

                   jQuery.validator.messages.multiemails
                );
                
                 $("#saveBooking").validate({
                    rules: {
                        emailConfirmationSentToBox : {required: true, multiemails:true}
                    },
                    messages: {
                        emailConfirmationSentToBox : {required: "Please enter confirmation email address", multiemails:"Please enter a valid email and  use ',' or ';' for multiple email address"}
                    }
                 });
            //------------------------------------------------------

            initSaveBooking();
            calculatePrice(currencyStr, selectedExRate.code, selectedExRate.rate);

            // added by Nimesh on 13th Feb 2015 for selection change for timeslots
            var disableAll = <%= DisableAll.ToString().ToLower() %>;
            if (!disableAll) {
                jQuery('.timeslots.available').click(function () {
                        
                        jQuery('.timeslots.available').removeClass('selected');
                        jQuery('.timeslots.available').find('.availability').each(function () {
                            jQuery(this).text(jQuery(this).text().replace('SELECTED', 'AVAILABLE'));
                        });
                        
                        jQuery(this).toggleClass('selected');
                        jQuery(this).find('.availability').text(jQuery(this).find('.availability').text().replace('AVAILABLE', 'SELECTED'));
                        
                    });
            }
            else
            {
                jQuery('#slotsDetail').hide();
            }
        });
    </script>

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35407162-1']);
        _gaq.push(['_setDomainName', 'thlrentalsb2b.com']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <style type="text/css">
        .saveQuote .hasDisc .PriceDetailsList .prods .pdp
        {
            width: 304px !important;
        }
           #emailConfirmationSentTo span label.error {
                color: #F00;
                width: 220px !important;
                margin-left: 10px;
            }

         
           #emailConfirmationSentTo span {
            float: left;
            clear: both;
            display: block;
            margin: 10px 0px 10px 30px;
            width: 800px;
            height: 50px;
        }

    </style>          
</head>
<body>
    <form id="saveBooking" action="" onsubmit="return false;">
        <div id="priceAvailabilityContainer">
            <uc1:Header ID="Header1" runat="server"></uc1:Header>
            <div id="configureContainer">
                <div class="section breadCrumb quote <%= IsAlternativeAvailability ? "aa" : "" %>"></div>
                <h2>2. Submit Your Booking</h2>
                <div id="configurePanel" class="saveQuote">
                    <%= ConfigureDisclaimer %>
                    <div id="topHeader">
                        <span class="currencyPanel"><small>Showing Alternative Rates in:</small>
                            <label><%= SelectedCurrencyRate.ToCode %></label>
                        </span>
                    </div>
                    <div id="vehicleTopPanel" class="section">
                        <div class="VehicleHeader">
                            <a class='VehicleTitle PopUp' href="http://<%= VehicleContentPage %>"><%= VehicleName %></a>
                            <h4>Travel details:&nbsp;</h4>
                            <%= LocationsPanel %>
                        </div>
                        <div class="VehicleImages">
                            <span class="left">
                                <img src="<%= VehicleInclusionPath %>" alt="<%= VehicleName %> inclusions" title="<%= VehicleName %> inclusions" />
                            </span>
                            <span class="right">
                                <a class='VehicleTitle PopUp' href="http://<%= VehicleContentPage %>">
                                    <img src="<%= VehicleImagePath %>" title="<%= VehicleName %>" alt="<%= VehicleName %>" />
                                </a>
                            </span>
                        </div>
                    </div>
                    <div id="vehicleChargeDetails" class="section <%= (HasDiscountedDays ? "hasDisc" : "") %>">
                        <div class="title">
                            <h4>Charges Description:&nbsp;</h4>
                        </div>
                        <span id="ChargeHeader">
                            <span class="Title">
                                <small>Vehicle Charge</small>
                            </span>
                        </span>
                        <%= PriceDetailsTable %>
                    </div>
                    <div id="slotsDetail">
                        <div class="title">
                            <h4>Select your pickup time:&nbsp;</h4>
                        </div>
                        <p style="padding-left:32px;">Please tell us when you'll be arriving so we can ensure you have the best experience when picking up your campervan.</p>
                        <div id="pickuptimes">
                            <div id="currentday">
                                <span class="date"><%= PickUpDayMonthStr %></span>
                                <span class="location"><%= PickUpLocationStr %></span>
                            </div>
                            <div id="times">
                                <%=SlotsPanel %>
                            </div>

                        </div>
                    </div>
                    <div id="customerDetails" class="newBooking">
                        <div class="title">
                            <h4>Customer Details:&nbsp;</h4>
                        </div>
                        <div id="bookingFormContainer">
                            <big>Personal details: (required)</big>
                            <input type="hidden" name="addtob" value="F" id="addtob" />
                            <span class="Radios section">
                                <span class="tab selected">
                                    <label rel="F">New Booking</label>
                                </span>
                                <span class="fill" style="width: 10px;"></span>
                                <span class="tab">
                                    <label rel="T">Add to existing booking</label>
                                </span>
                                <span class="fill" style="width: 571px;"></span>
                            </span>
                            <div id="customerForm" class="form">
                                <ul>
                                    <li>
                                        <p>
                                            <label for="customerTitleDD">Title:</label>
                                            <select name="customerTitleDD" id="customerTitleDD">
                                                <option value="Mr">Mr</option>
                                                <option value="Mrs">Mrs</option>
                                                <option value="Ms">Ms</option>
                                                <option value="Miss">Miss</option>
                                                <option value="Master">Master</option>
                                                <option value="Dr">Dr</option>
                                            </select>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="customerFirstName">First Name:*</label>
                                            <input class="required ClickTaleSensitive" minlength="2" name="customerFirstName" id="customerFirstName" onblur="this.value = this.value.replace(/[^a-zA-Z-/ 0-9 \n]+/g,'')" />
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="customerLastName">Last Name:*</label>
                                            <input class="required ClickTaleSensitive" minlength="2" name="customerLastName" id="customerLastName" onblur="this.value = this.value.replace(/[^a-zA-Z-/ 0-9 \n]+/g,'')" />
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="custAgentRef">Agent Ref:*</label>
                                            <input class="required ClickTaleSensitive" minlength="2" name="custAgentRef" id="custAgentRef" onblur="this.value = this.value.replace(/[^a-zA-Z-/ 0-9 \n]+/g,'')" />
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <span>Note: Please enter your customer's email address carefully as this will be the address we send your confirmation to.</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="emailAddressTxt">Email address:</label>
                                            <input class="email ClickTaleSensitive" name="emailAddressTxt" id="emailAddressTxt" />
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="confirmEmailAddressTxt">Confirm address:</label>
                                            <input class="email ClickTaleSensitive" name="confirmEmailAddressTxt" id="confirmEmailAddressTxt" />
                                        </p>
                                    </li>
                                    <li style="display: none;">
                                        <p>
                                            <label for="telephoneTxt1">Telephone:</label>
                                            <span class="PhoneNumber">
                                                <span class="number">
                                                    <small>country code</small>
                                                    <input class="number" size="10" name="telephoneTxt1" id="telephoneTxt1" />
                                                </span>
                                                <span class="number">
                                                    <small>area code</small>
                                                    <input class="number" size="10" name="telephoneTxt2" id="telephoneTxt2" />
                                                </span>
                                                <span class="number">
                                                    <small>number</small>
                                                    <input class="number ClickTaleSensitive" size="10" name="telephoneTxt3" id="telephoneTxt3" />
                                                </span>
                                            </span>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <span>Any special requests or comments relating to your customer's upcoming hire?</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="customerNoteTxt">Enter here:</label>
                                            <textarea onblur="this.value = this.value.replace(/[^a-zA-Z 0-9 \n]+/g,'')" rows="5" cols="50" class="ClickTaleSensitive" name="customerNoteTxt" id="customerNoteTxt"></textarea>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <div id="addToBookingForm" class="form">
                                <ul>
                                    <li>
                                        <p>
                                            <label for="bookingNo">Booking Num:*</label>
                                            <input class="ClickTaleSensitive" minlength="2" name="bookingNo" id="bookingNo" />
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="agentRef">AgentRef:*</label>
                                            <input class="ClickTaleSensitive" minlength="2" name="agentRef" id="agentRef" onblur="this.value = this.value.replace(/[^a-zA-Z-/ 0-9 \n]+/g,'')" />
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <span>Any special requests or comments relating to your customers upcoming hire?</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <label for="customerNoteTx1t">Enter here:</label>
                                            <textarea onblur="this.value = this.value.replace(/[^a-zA-Z 0-9 \n]+/g,'')" rows="5" cols="50" class="ClickTaleSensitive" name="customerNoteTxt1" id="customerNoteTxt1"></textarea>
                                        </p>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <%--<div id="slotDetails">
                            <%= SlotPanel %>
                        </div>--%>

                        <!--rev:mia 11March2015 B2B Auto generated Aurora Booking Confirmation-->
                        <div id="emailConfirmationSentTo" style='display:<%= (IsQN == true ) ? "none" : "block" %>'>
                             <span>Confirmation to be send to:*
                               &nbsp;&nbsp;&nbsp; <input class='<%= (IsQN == true ) ? "" : "required" %>' name="emailConfirmationSentToBox" id="emailConfirmationSentToBox"  style="width:200px;" value="<%= B2BAgentValidEmail %>"/>
                            </span>
                        </div>
                        <div id="termsAndConditions">
                            <span>Link to <a href="<%= TandCLink %>" class="PopUp">Rental Agreement</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="configureUpdatePanel" class="section">
                <span class="total">
                    <span class="labels">
                        <label>Payable to agent:</label>
                        <!--rev:mia April 1 2014 Payable to supplier messages-->
                        <label><%= GetMessage("PayableToSupplerDays") %>:</label>
                    </span>
                    <span class="price" title="<%= PackageCode %>">
                        <small id="totalAmount" class="exRate pta" rel="<%= pta %>"><%= pta %></small>
                        <small id="payAtPickUp" class="exRate pap" rel="<%= pap %>"><%= pap %></small>
                    </span>
                </span>
                <span class="TaxMsg ">
                    <%--<small>Total includes all taxes (GST)</small>--%>
                    <small><%= TaxMessage %></small>
                </span>
                <span class="Submit">
                    <%--<a class="BackBtn" href="javascript:history.back();" id="saveBackButton">Back</a>--%>
                    <a class="BackBtn" id="saveBackButton">Back</a>
                    <input id="confrmBooking" type="submit" onclick="submitBooking(this);" value="SAVE" />
                </span>
            </div>
        </div>           
        <input type="hidden" id="B2BAgentEmailResultHidden" value="<%= B2BAgent %>" />
       <%-- <input type="hidden" id="HiddenConfigureURL" value="im HiddenConfigureURL" />--%>
        
    </form>
</body>
</html>
