<%@ Register TagPrefix="uc1" TagName="WebServiceDown" Src="../Controls/SignIn/WebServiceDown.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AgentLogin.aspx.vb" Inherits="THLAuroraWebInterface.Login"%>
<%@ Register TagPrefix="uc1" TagName="SignIn" Src="../Controls/SignIn/SignIn.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-35407162-1']);
            _gaq.push(['_setDomainName', 'thlrentalsb2b.com']);
            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
    </script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="middle" align="center">
						<table id="Table1" borderColor="#999999" cellSpacing="0" cellPadding="0" border="2">
							<tr>
								<td>
									<uc1:WebServiceDown id="WebServiceDown1" runat="server"></uc1:WebServiceDown></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
