<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BookingNote.aspx.vb" Inherits="THLAuroraWebInterface.BookingNote"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add a Note</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
	
		var iLoopCounter = 1;
		var iMaxLoop = 5;
		var iIntervalId;
		
		function BeginPageLoad() {
			Message.innerText=" Saving -- Please Wait "  ;
			Message2.innerText=" This window will automatically close once saved."
			iIntervalId = window.setInterval("iLoopCounter=UpdateProgress(iLoopCounter, iMaxLoop)", 500);
		}
	
		function EndPageLoad() {
		alert("End");
			window.clearInterval(iIntervalId);
			Message.innerText=""  ;
			Progress.innerText = "";
		}
	
		function UpdateProgress(iCurrentLoopCounter, iMaximumLoops) {
		
			iCurrentLoopCounter += 1;
			
			if (iCurrentLoopCounter <= iMaximumLoops) {
				Progress.innerText += ".";
				return iCurrentLoopCounter;
			}
			else {
				Progress.innerText = "";
				return 1;
			}
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" style="WIDTH: 248px; HEIGHT: 192px" cellSpacing="1" cellPadding="1"
				width="248" border="0">
				<TR>
					<TD class="StdConfirmationHeading">Add a Note:</TD>
				</TR>
				<TR>
					<TD><asp:textbox id="txtNote" runat="server" Height="109px" Width="296px" MaxLength="254" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
				<TR>
					<TD align="right">
						<asp:button id="btnSave" runat="server" Width="80px" Text="Save"></asp:button><asp:button id="btnCancel" runat="server" Width="80px" Text="Cancel"></asp:button></TD>
				</TR>
				<TR>
					<TD align="left" class="warning">
						<span class="warning" id="Message"></span><span class="warning" id="Progress"></span>
					</TD>
				<TR>
					<TD align="left" class="warning">
						<span class="warning" id="Message2"></span>
						<br>
						<asp:Label id="lblError" runat="server"></asp:Label></TD>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
