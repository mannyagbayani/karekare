'--------------------------------------------------------------------------------------
'   Class:          CustomerLogin
'   Description:    Logs customer on as well as allowing for customer registration
'--------------------------------------------------------------------------------------

Partial Class CustomerLogin
    Inherits System.Web.UI.Page
    Protected WithEvents Signin1 As SignIn
    Protected WithEvents Register1 As Register


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        '    General.MaintainSessionObjects(General.PageName.CustomerLogin, Session)

    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here

        If Not Page.IsPostBack Then
            General.ClearSession(Session)
        End If

        Session("TypeOfUser") = "customer"

        Dim host As String = Request.Url.GetLeftPart(UriPartial.Authority)
        Dim applicationPath As String = Request.Url.Authority

        ' Determine what site the user is currently looking at
        If applicationPath.ToLower.StartsWith("b") Then
            ' Simple but works well
            Session("Company") = "britz"
        ElseIf applicationPath.ToLower.StartsWith("m") Then
            Session("Company") = "maui"
        Else
            ' If neither b or  m then it must be running in developement as localhost
            ' Default to maui
            Session("Company") = "maui"
        End If

        ' If a return url is specified in the querystring pass it to the sigin control to use
        If Not IsNothing(Request.QueryString("returnURL")) Then
            Signin1.RedirectURL = Request.QueryString("returnURL") & "?No=" & Request.QueryString("No") & "&type=" & Request.QueryString("type")
        End If

        If Not IsPostBack Then

            lblConfirmation.Visible = False
            Register1.Visible = True
            Signin1.Visible = True
        End If

    End Sub

    Private Sub Register1_NewUserCreated(ByVal email As String, ByVal password As String) Handles Register1.NewUserCreated
        'A new user has been registered, proceed to log them in.
        Signin1.Email = email
        Signin1.Password = password
        ' Signin1.Login()
        lblConfirmation.Visible = True
        Register1.Visible = False
    End Sub
End Class
