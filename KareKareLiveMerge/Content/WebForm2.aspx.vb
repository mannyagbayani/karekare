Imports System.IO
Imports System.Net
Imports System.Text
Imports TelecomNZ.Common.Library
Partial Class WebForm2
	Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	'NOTE: The following placeholder declaration is required by the Web Form Designer.
	'Do not delete or move it.
	Private designerPlaceholderDeclaration As System.Object

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region


	Function readHtmlPage() As String
		Dim objResponse As WebResponse
		Dim objRequest As WebRequest
		Dim objRespnse As WebResponse
		Dim result As String
		objRequest = System.Net.HttpWebRequest.Create(Me.Page.Request.UrlReferrer)
		objResponse = objRequest.GetResponse()
	


		'use UTF7 Encoding to read special charachters and the Norwegian alphabet
		Dim sr As New StreamReader(objResponse.GetResponseStream(), System.Text.Encoding.UTF7)
		result = sr.ReadToEnd().ToString
		'clean up StreamReader
		sr.Close()
		Return result
	End Function

	'Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
	'	Dim sb As New StringBuilder
	'	Dim sw As StringWriter = New StringWriter(sb)
	'	Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
	'	MyBase.Render(hw)
	'	Dim pageHtml As String = sb.ToString()

	'	writer.Write(pageHtml)
	'End Sub

	Private Sub getHtml()
		Dim sb As New StringBuilder
		Dim sw As StringWriter = New StringWriter(sb)
		Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
		MyBase.Render(hw)
		Dim pageHtml As String = sb.ToString()
		'pageHtml = pageHtml.Replace("746px", "560px")
		Dim i As Integer
		Dim arrC() As String = pageHtml.Split(CType(":", Char))
		For i = 0 To arrC.Length - 1
			Dim number As String = arrC(i).ToString.Substring(0, 3)
			If IsNumeric(number) Then
				If CInt(number) > 560 Then
					pageHtml = pageHtml.Replace(number, "560")
				End If

			End If
		Next
		Dim i2 As Integer
		Dim arrC2() As String = pageHtml.Split(CType("=", Char))
		For i2 = 0 To arrC.Length - 1
			Dim number As String = arrC2(i2).ToString.Substring(1, 4)
			number = number.Replace("""", "")
			If IsNumeric(number) Then
				If CInt(number) > 560 Then
					pageHtml = pageHtml.Replace(number, "560")
				End If

			End If
		Next
		'	pageHtml = pageHtml.Replace("745", "560")
		Session("PageHtml") = pageHtml
		'Response.Redirect("testpage.aspx")
	End Sub



	Private Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		'Label1.Text = readHtmlPage()
		'getHtml()
		'getHtml()
	End Sub

	Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
		'getHtml()
		Dim url As String = "testpage.aspx?page=" & Me.Request.Url.ToString
		UIScripts.NewWindowSimple(Me.Page, url, 570, 800, 0, 0)
	End Sub
End Class
