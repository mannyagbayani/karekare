#Region "Notes"
'Generic page for all aspx pages
'Karel Jordaan 5July2006 Added serverside validation 
#End Region
Partial Class THL_PageBase
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Serverside validation
        Page.Validate()
        '' experimental
        If Not Request.Path.Contains("MaintainAgentUsers.aspx") Then
            If Not Page.IsValid() Then Throw New Exception("Page is not valid")
        End If

    End Sub

End Class
