﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using THL.Booking.DAL;
using System.Xml;
using System.Xml.Linq;

public partial class Content_Save : System.Web.UI.Page
{
    /// <summary>
    /// Get the Exchange Rate for the Selected Agent's Request
    /// </summary>
    public ExchangeRate SelectedCurrencyRate
    {
        get
        {
            DataAccess dal = new DataAccess();
            AgentLoginData aLoginData = dal.GetAgentData((SourceType)(Session["POS"]));
            string bookingCurCode = CurrencyManager.GetCurrencyForCountryCode(CurrentRequest.CountryCode).ToString();
            ExchangeRate exRate = new ExchangeRate();
            if (!string.IsNullOrEmpty(Request.Params["cur"]) && !string.IsNullOrEmpty(Request.Params["rate"]))//passed forward from selection
                exRate = SelectedAgent.GetExRateForCodes(bookingCurCode, Request.Params["cur"].ToUpper());
            else//this agents default
                exRate = SelectedAgent.GetExRateForCodes(bookingCurCode, (!aLoginData.HasCurrencyOption || String.IsNullOrEmpty(SelectedAgent.DefaultCurrency)) ? bookingCurCode : SelectedAgent.DefaultCurrency);
            return exRate;
        }
    }



    /// <summary>
    /// Disclaimer Message
    /// </summary>
    public string ConfigureDisclaimer;
    public bool DisableAll { get; set; }

    public string PriceDetailsTable
    {
        get;
        set;
    }


    /// <summary>
    /// Vehicle Thumb Image within central assets
    /// </summary>
    public string VehicleImagePath
    {
        get
        {
            string vehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(CurrentRequest.CountryCode, _availabilityItem.Brand, CurrentRequest.VehicleType, _availabilityItem.VehicleCode, Content_Save.AssetBasePath, false) + "-ClearCut-144.jpg";
            return vehicleImagePath;
        }
    }

    /// <summary>
    /// Vehicle Inclusions (Features) within central assets
    /// </summary>
    public string VehicleInclusionPath
    {
        get
        {
            string vehicleInclusionPath = AvailabilityHelper.GetVehicleImageBasePath(CurrentRequest.CountryCode, _availabilityItem.Brand, CurrentRequest.VehicleType, _availabilityItem.VehicleCode, Content_Save.AssetBasePath, true) + "-InclusionIcons.gif";
            return vehicleInclusionPath;
        }
    }


    /// <summary>
    /// Vehicle Content and Features MOSS Pages location on parent site
    /// </summary>
    public string VehicleContentPage
    {
        get
        {
            THLBrands containerBrand = THLBrands.MAC;
            string VehicleContentLink = ContentManager.GetVehicleLink(_availabilityItem.Brand, CurrentRequest.CountryCode, _availabilityItem.VehicleCode, containerBrand);
            if (VehicleContentLink != null && VehicleContentLink.Length > 0) VehicleContentLink = VehicleContentLink.Replace("&amp;", "&");
            return VehicleContentLink;
        }
    }

    public string VehicleName
    {
        get
        {
            return THLBrand.GetNameForBrand(_availabilityItem.Brand) + " " + _availabilityItem.VehicleName;
        }
    }

    public string PackageCode
    {
        get
        {
            return _availabilityItem.PackageCode;
        }
    }

    public string LocationsPanel
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            ApplicationData appData = ApplicationManager.GetApplicationData();
            sb.Append(@"
            <div class=""Locations"">
                <span><b>Pick up:</b> " + appData.GetLocationStringForCtrlCode(CurrentRequest.PickUpBranch) + ", " + _availabilityItem.PickUpDate.ToString("dddd, dd MMMM yyyy HH:mm tt") + @"</span> 
                <span><b>Drop off:</b> " + appData.GetLocationStringForCtrlCode(CurrentRequest.DropOffBranch) + ", " + _availabilityItem.DropOffDate.ToString("dddd, dd MMMM yyyy HH:mm tt") + @"</span>
            </div>
            ");
            return sb.ToString();
        }
    }

    public string TandCLink
    {
        get { return "http://" + AvailabilityHelper.NormalizeURL(_availabilityItem.TandCLink); }
    }


    public string CurrencyStr
    {
        get
        {
            return CurrencyManager.GetCurrencyForCountryCode(CurrentRequest.CountryCode).ToString();
        }
    }

    public bool HasDiscountedDays
    {
        get { return AvailabilityHelper.HasDiscountedDays(_availabilityItem.GetRateBands()); }
    }

    public bool IsAlternativeAvailability { get { return _availabilityItem.IsAlternative; } }


    public string Testing { get; set; }
    /// <summary>
    /// Secure or Not
    /// </summary>
    public string HTTPPrefix { get { return ApplicationManager.HTTPPrefix; } }


    public static string AssetBasePath
    {
        get { return ApplicationManager.HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"]; }
    }



    AvailabilityItem _availabilityItem;

    public bool IsGrossRequest;

    public AgentLoginData AgentData;
    AgentDescription SelectedAgent;
    public AvailabilityRequest CurrentRequest;

    public decimal pta = 0.0m, pap = 0.0m;

    string prdcode = "";
    string papMode = "";

    public string TaxMessage { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        B2BSessionCache sessionCache = SessionManager.GetSessionData().B2BCache;
        string avpId = Request.Params["avp"];
        prdcode = Request.Params["prdId"];
        papMode = Request.Params["pap"];
        if (!B2BAvailabilityHelper.SessionExpired && sessionCache != null && !string.IsNullOrEmpty(avpId) && SessionManager.GetSessionData().HasBookingData)
        {
            AuroraBooking booking = SessionManager.GetSessionData().BookingData;
            AgentData = HttpContext.Current.Session["AgentData"] as AgentLoginData;
            CurrentRequest = SessionManager.GetSessionData().AvailabilityRequestData;
            SelectedAgent = AgentData.GetAgentForCode(CurrentRequest.AgentCode.ToUpper());
            ExchangeRate rate = SelectedCurrencyRate;
            _availabilityItem = sessionCache.GetAvailabilityForAVP(avpId);

            if (_availabilityItem == null) _availabilityItem = sessionCache.GetIncItem(avpId);//try in Inclusives, TODO: formal interface
            decimal ehiTotal = 0.0m;
            PriceDetailsTable = B2BAvailabilityDisplayer.RenderB2BPriceDetails(_availabilityItem, ref pta, ref pap);

            string renderInsurance = RenderInsurance(booking.GetBookingProducts(ProductType.ExtraHireItem), CurrencyManager.GetCurrencyForCountryCode(CurrentRequest.CountryCode).ToString(), _availabilityItem.GetHirePeriod(), ref ehiTotal);
            if (!string.IsNullOrEmpty(renderInsurance))
            {
                string priceDetailsTable = PriceDetailsTable;
                priceDetailsTable = priceDetailsTable.Replace(@"<!--<tr><td>manny</td></tr>-->", renderInsurance + @"<!--<tr><td>mannytest</td></tr>-->");
                PriceDetailsTable = priceDetailsTable;
                pta = pta + ehiTotal;
                ehiTotal = 0.0m;
            }

            PriceDetailsTable += RenderExtraHireTable(booking.GetBookingProducts(ProductType.ExtraHireItem), CurrencyManager.GetCurrencyForCountryCode(CurrentRequest.CountryCode).ToString(), _availabilityItem.GetHirePeriod(), ref ehiTotal);
            pap = pap + ehiTotal;
            PriceDetailsTable += RenderTotals(pta, pap);

            IsGrossRequest = (CurrentRequest as B2BAvailabilityRequest).b2bParams.isGross;
            PackageManager pm = new PackageManager(_availabilityItem, ApplicationManager.GetApplicationData(), IsGrossRequest);
            ConfigureDisclaimer = ContentManager.GetConfigureB2BMessage(IsGrossRequest, booking.Status);
            IsQN = (booking.Status.ToString() == "Quote" ? true : false);

            /*Added by Nimesh On 23rd Sept to display different message for tax for US based bookings*/
            try
            {
                PickUpLocationStr = CurrentRequest.PickUpBranch;// booking.PickUpBranch; // CurrentRequest.PickUpBranch;
                PickUpDayMonthStr = _availabilityItem.PickUpDate.ToString("dd MMM"); // booking.CheckOutTime.ToString("dd MMM"); // CurrentRequest.PickUpDate.ToString("dd MMM");

                if (CurrentRequest.CountryCode.ToString().ToUpper().Equals("US"))
                    //{
                    /*Start of Change*/
                    TaxMessage = "* Pre-paid items include US sales tax. Add local sales tax for items paid onsite";
                /*End of Change*/
                //}
                else
                    TaxMessage = "Total includes all taxes (GST)";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            
            /*End Added by Nimesh On 23rd Sept to display different message for tax for US based bookings*/

            /*added by nimesh on 12th Feb 15 for slots*/
            if (booking.Status.Equals(BookingStatus.Confirmed) || booking.Status.Equals(BookingStatus.Provisional))
            {
                SlotsPanel = GetAvailableSlots(avpId, string.Empty, string.Empty, string.Empty);
                if (string.IsNullOrEmpty(SlotsPanel))
                {
                    DisableAll = true;
                }
            }
            else
                DisableAll = true;
            /*end added by nimesh on 12th Feb 15 for slots*/





            //rev:mia 24March2015 - B2B Auto generated Aurora Booking Confirmation
            GetAgentEmail();
        }
    }

    public string GetAvailableSlots(string avpID, string bookingId, string sNewPickupDate, string sNewPickupLocation )
    {
        StringBuilder sb = new StringBuilder();
        string otaBase = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraOTABase"];
        Aurora_OTA otaProxy = new Aurora_OTA(otaBase);

        //var res = otaProxy.GetAvailableSlot("", avpID, "", sNewPickupDate,sNewPickupLocation);
        var res = otaProxy.GetAvailableSlot(null, avpID, null,false, false, null, null, null, null,null, null);
        if (res == null) return "";
        string slotData = res.OuterXml;
        string retString = "Success";
        //slotData = string.Empty;
        if ((slotData != null && slotData != string.Empty) && slotData.IndexOf("AvailableSlots") >= 0)
        {

            XmlDocument slotXml = new XmlDocument();
            slotXml.LoadXml(slotData);
            //slotXml.GetXElement();
            var result = (from c in slotXml.GetXElement().Descendants("Slot")
                          select c
                     ).ToList();
            int rowCount = result.Count();
            //var widthPercentage = Math.Round(Decimal.Parse(100.ToString()) / rowCount, 0);
            int widthPercentage = 100 / rowCount;
            string fontSize = "18px";
            if (rowCount > 6)
                fontSize = "16px";
            foreach (XElement slot in result)
            {
                string slotDesc = slot.Element("SlotDesc").Value;
                string slotId = slot.Element("SlotId").Value;
                string allocatedNumbers = slot.Element("AllocatedNumbers") == null ? "0" : slot.Element("AllocatedNumbers").Value;
                string bookedNumbers = slot.Element("Bookednumbers") == null ? "0" : slot.Element("Bookednumbers").Value;
                string availableNumbers = slot.Element("AvailableNumbers") == null ? "0" : slot.Element("AvailableNumbers").Value;

                string isSlotAvailable = slot.Element("IsSlotAvailable") == null ? "0" : slot.Element("IsSlotAvailable").Value;

                string isAllDaySlot = slot.Element("IsAllDaySlot") == null ? "0" : slot.Element("IsAllDaySlot").Value;
                string isSelected = slot.Element("IsSelected").Value;

                string[] slotTimes = slotDesc.Split('-');
                string[] timeSplit1 = slotTimes[0].Split(' ');
                string[] timeSplit2 = slotTimes[1].Trim().Split(' ');

                string availUnavail = isSlotAvailable.Equals("1") ? "available" : "unavailable";
                bool selected = isSelected.Equals("1") ? true : false;
                string availUnavailText = availUnavail.ToUpper();
                string ava = " (" + (int.Parse(allocatedNumbers) - int.Parse(bookedNumbers)).ToString() + ")";
                if (int.Parse(availableNumbers) <= 0)
                {
                    isSlotAvailable = "0";
                    isSelected = "0";
                    availUnavail = "unavailable";
                    selected = false;
                    availUnavailText = "UNAVAILABLE";
                }
                if (selected)
                {
                    availUnavail += " selected";
                    availUnavailText = "SELECTED";
                }
                //else
                //    if (!availUnavail.Equals("unavailable") && isDevEnv)
                //        availUnavailText += ava;

                sb.Append("<div class='timeslots " + availUnavail + "' id='slot" + slotId + "' style='width:" + widthPercentage.ToString() + "%'>");
                sb.Append("<div class='slots'>");
                sb.Append("<div class='time left' style='font-size:" + fontSize + "'>" + timeSplit1[0] + "<span class='ampm'>" + timeSplit1[1] + "</span></div>");
                sb.Append("<div class='arrow'><span class='arrowhover'>&nbsp;</span></div>");
                sb.Append("<div class='time right' style='font-size:" + fontSize + "'>" + timeSplit2[0] + "<span class='ampm'>" + timeSplit2[1] + "</span></div>");
                sb.Append("<div class='availability'>" + availUnavailText + "</div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }

        }
        else
        {
            return null;
        }
        return sb.ToString();
    }

    public string SlotsPanel { get; set; }

    public string PickUpDayMonthStr { get; set; }
    public string PickUpLocationStr { get; set; }


    public string RenderExtraHireTable(AuroraProduct[] products, string currencyStr, int hirePeriod, ref decimal total)
    {
        StringBuilder sb = new StringBuilder();
        if (products != null && products.Length > 0)
        {
            sb.Append(@"
                <table class=""chargesTable prods"">                    
                    <tbody>            
            ");
            foreach (AuroraProduct product in products)
            {
                decimal prodTotal = product.DailyRate * (product.UOM == UOMType.Day ? hirePeriod : 1) * product.SelectedAmount > product.MaxCharge && product.MaxCharge > 0 ? product.MaxCharge : product.DailyRate * (product.UOM == UOMType.Day ? hirePeriod : 1) * product.SelectedAmount;

                if (prdcode != product.Code)
                {
                    total += prodTotal;
                    sb.Append(@"
                    <tr>
                        <td class=""rt"">" + product.SelectedAmount + " x " + product.Name + @"</td>
                        <td class=""hp"">" + (product.UOM == UOMType.Day ? hirePeriod + " days(s)" : "") + @"</td>
                        <td rel=""" + product.DailyRate.ToString("F") + @""" class=""pdp"">" + currencyStr + " " + product.DailyRate.ToString("C") + @"</td>
                        <td class=""pta"">&nbsp;</td>
                        <td rel=""" + prodTotal.ToString("F") + @""" class=""pap exRate"">" + prodTotal.ToString("F") + @"</td>                        
                    </tr>
                ");
                }
                else
                {
                    if (papMode != "ptaMode")
                    {
                        total += prodTotal;
                        sb.Append(@"
                            <tr>
                                <td class=""rt"">" + product.SelectedAmount + " x " + product.Name + @"</td>
                                <td class=""hp"">" + (product.UOM == UOMType.Day ? hirePeriod + " days(s)" : "") + @"</td>
                                <td rel=""" + product.DailyRate.ToString("F") + @""" class=""pdp"">" + currencyStr + " " + product.DailyRate.ToString("C") + @"</td>
                                <td class=""pta"">&nbsp;</td>
                                <td rel=""" + prodTotal.ToString("F") + @""" class=""pap exRate"">" + prodTotal.ToString("F") + @"</td>                        
                            </tr>
                        ");
                    }
                }

            }
        }
        sb.Append(@"</tbody></table>");
        return sb.ToString();
    }

    public string RenderTotals(decimal agentCharge, decimal pickUpCharge)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"
            <table class=""chargesTable prods"">                    
                <tbody>
                    <tr class=""total"">
                    <td class=""rt"">Total</td>
                    <td class=""hp"">&nbsp;</td>
                    <td class=""pdp"">&nbsp;</td>
                    <td rel=""" + agentCharge.ToString("F") + @""" class=""pta exRate"">" + agentCharge.ToString("F") + @"</td>
                    <td rel=""" + pickUpCharge.ToString("F") + @""" class=""pap exRate"">" + pickUpCharge.ToString("F") + @"</td>                    
                    </tr>
                </tbody>
            </table>
        ");
        return sb.ToString();
    }

    private string RenderInsurance(AuroraProduct[] products, string currencyStr, int hirePeriod, ref decimal total)
    {
        StringBuilder sb = new StringBuilder();
        if (products != null && products.Length > 0 && papMode == "ptaMode")
        {

            foreach (AuroraProduct product in products)
            {
                decimal prodTotal = product.DailyRate * (product.UOM == UOMType.Day ? hirePeriod : 1) * product.SelectedAmount > product.MaxCharge && product.MaxCharge > 0 ? product.MaxCharge : product.DailyRate * (product.UOM == UOMType.Day ? hirePeriod : 1) * product.SelectedAmount;

                if (prdcode == product.Code && papMode == "ptaMode")
                {
                    total += prodTotal;
                    sb.Append(@"
                            <tr>
                                <td>" + product.SelectedAmount + " x " + product.Name + @"</td>
                                <td class=""hp"">" + (product.UOM == UOMType.Day ? hirePeriod + " days(s)" : "") + @"</td>
                                <td rel=""" + product.DailyRate.ToString("F") + @""" class='pdp exRate'>" + currencyStr + " " + product.DailyRate.ToString("C") + @"</td>
                                <td class='dpd'>&nbsp;</td>
                                <td rel=""" + prodTotal.ToString("F") + @""" class='pta exRate'>" + prodTotal.ToString("F") + @"</td>                        
                            </tr>
                            ");
                }

            }
        }

        return sb.ToString();
    }

    //rev:mia April 1 2014 Payable to supplier messages
    public string GetMessage(string message)
    {
        string vt = (CurrentRequest as B2BAvailabilityRequest).b2bParams.vt.ToString();
        return THLAuroraWebInterface.Config.GetPayablePickupMessage(message, vt);
    }


#region B2B Auto generated Aurora Booking Confirmation
    
    public string B2BAgent;
    public string B2BAgentValidEmail;

    bool IsAgentHasAvalidEmail(string agentemail)
    {
        if (agentemail.Contains("ERROR") == true)
            return false;
        else
        {
            B2BAgentValidEmail = agentemail;
            return true;
        }
    }

    string GetB2BAgent()
    {
        string b2bagentcode = "";

        //todo: its should be getting from dbase
        if (Session["agentinformation.agentcode"] != null)
            b2bagentcode = Session["agentinformation.agentcode"].ToString();
        else
        {
            if (Request.Cookies["agentinformation"] != null)
            {
                b2bagentcode = Request.Cookies["agentinformation"]["agentcode"];
            }
        }
        return b2bagentcode;
    }

    bool GetAgentEmail()
    {
        THL.Booking.PaymentManager pm = new PaymentManager();
        string agentemail = pm.GetB2BAgentEmailAddress(GetB2BAgent());
        B2BAgent = agentemail;
        return IsAgentHasAvalidEmail(agentemail);
    }

       
    public bool IsQN
    {
        get;
        set;
    }

    #endregion

}

