<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FooterImage" Src="../Controls/Graphics/FooterImage.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EditMyDetails.aspx.vb" Inherits="THLAuroraWebInterface.EditMyDetails"%>
<%@ Register TagPrefix="uc1" TagName="UserDetails" Src="../Controls/UserDetails/UserDetails.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>EditMyDetails</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
	</HEAD>
	<body background='<%=BackgroundURL()%>'>
		<form runat="server" id="form1">
			<table id="Table1" borderColor="#999999" cellSpacing="0" cellPadding="0" width="751" border="0"
				style="WIDTH: 751px; HEIGHT: 63px">
				<tr>
					<td>
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="746" border="0" style="WIDTH: 746px; HEIGHT: 37px">
							<TR>
								<TD vAlign="top">
									<uc1:Header id="Header1" runat="server"></uc1:Header></TD>
							</TR>
							<TR>
								<TD width="100%">
									<uc1:UserDetails id="UserDetails1" runat="server"></uc1:UserDetails></TD>
							</TR>
							<TR>
								<TD width="100%">
									<asp:Label id="lblLoadError" runat="server" CssClass="warning"></asp:Label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</table>
			<uc1:FooterImage id="FooterImage1" runat="server"></uc1:FooterImage>
		</form>
	</body>
</HTML>
