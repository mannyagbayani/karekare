'--------------------------------------------------------------------------------------
'   Class:          Pre_ ' Not to sure what happend to the rest of the name
'   Description:    Allows logged in cutsomer to register their details for 
'                   a particular reservation. This is not 100% working, needs testing
'                   and for the actual booking id to be passed to it.
'--------------------------------------------------------------------------------------

Partial Class Pre_
    Inherits System.Web.UI.Page

    Protected WithEvents PreRegister1 As PreRegister
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        '   General.MaintainSessionObjects(General.PageName.PreRegistration, Session)

    End Sub

#End Region



    Public Property BackgroundURL() As String
        Get
            Return CStr(viewstate("BackgroundURL"))
        End Get
        Set(ByVal Value As String)
            viewstate("BackgroundURL") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        If Not Page.IsPostBack Then
            Me.BackgroundURL = General.BackGroundImageURL(Session)

            Dim bookingId As String = Request.QueryString("No")
            ' PreRegister1.InitPreRegister(Server.UrlDecode(bookingId))

            PreRegister1.InitPreRegister("W3050407/1")

        End If
    End Sub

End Class
