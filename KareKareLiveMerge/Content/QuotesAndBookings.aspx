<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BookingContainer" Src="../Controls/Booking/BookingContainer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BookingList" Src="../Controls/Booking/BookingList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FooterImage" Src="../Controls/Graphics/FooterImage.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="QuotesAndBookings.aspx.vb"
    Inherits="THLAuroraWebInterface.QuotesAndBookings" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>QuotesAndBookings</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    

    <%--<link href="http://secure.thlonline.com/SiteCollectionDocuments/css/Base.css?v=2.8" type="text/css" rel="stylesheet" />
    <link href="http://secure.thlonline.com/SiteCollectionDocuments/css/c/configure.css?v=2.8" type="text/css" rel="stylesheet" />--%>
    <link href="<%= AssetBasePath %>/css/b2b/configure.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" type="text/css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.js" type="text/javascript"></script>
     <style type="text/css">
        .normal {
            font-weight: normal;
            font-size: 12px;
            color: #333;
            line-height: normal;
            font-style: normal;
            font-family: Arial,Helvetica,sans-serif;
            text-decoration: none;
        }
    </style>
</head>
<body background='<%=BackgroundURL()%>'>
    <form id="Form1" method="post" runat="server">
        <table id="Table1" bordercolor="#999999" cellspacing="0" cellpadding="0" width="746"
            border="0">
            <tr>
                <td>
                    <table id="Table2" cellspacing="0" cellpadding="0" width="746" border="0">
                        <tr>
                            <td valign="top">
                                <uc1:Header ID="Header1" runat="server"></uc1:Header>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <uc1:BookingContainer ID="BookingContainer1" runat="server"></uc1:BookingContainer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--rev:mia 11March2015 B2B Auto generated Aurora Booking Confirmation-->
         <table width="746" id="confirmationtable" runat="server" visible="false">
        <tr align="right">
            <td>
                    <label class="normal">
                    Confirmation to be send to:* &nbsp;
                    </label>
                    <input class="email" name="emailConfirmationSentToBox" id="emailConfirmationSentToBox" runat="server" style="width:200px;" />
                    <asp:button id="ButtonSendEmail" runat="server" text="Send to" width="70px" 
                    onclick="ButtonSendEmail_Click"></asp:button>
                     
             <asp:RequiredFieldValidator ID="emailRequiredValidator" runat="server" 
                        ControlToValidate="emailConfirmationSentToBox" Display="Dynamic" 
                        ErrorMessage="Email is required" EnableClientScript="False" Enabled="true"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="emailExpressionValidator" runat="server" 
                        ControlToValidate="emailConfirmationSentToBox" Display="Dynamic" 
                        ErrorMessage="RegularExpressionValidator" 
                        ValidationExpression="(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*;\s*|\s*$))*" 
                            EnableClientScript="False" Enabled="true">Email is in invalid format.</asp:RegularExpressionValidator>
                     
            </td>
        </tr>
        <tr align="right">
            <td>
                        <asp:Label runat="server" ID="confirmationmessage"></asp:Label>
            </td>
        </tr>
    </table>

        <uc1:FooterImage ID="FooterImage1" runat="server"></uc1:FooterImage>
    </form>
    <script type="text/javascript">
        function ValidateEmailAddress(e) {
            var retValue = false;
            var message = '';
            var sEmail = $('#BookingContainer1_CancelBooking1_emailAddressTxt').val().trim();
            if (sEmail == 0) {
                //retValue = false;
                //message = 'Please enter email address';
                retValue = true;

            }
            else {
                var allEmails = sEmail.split(';');
                for (x = 0; x < allEmails.length; x++) {
                    if (ValidateEmail(allEmails[x].trim())) {
                        retValue = true;
                    }
                    else {
                        message = allEmails[x] + ' is not a valid email address'
                        retValue = false; break;
                    }

                }
            }
            //alert('before return');
            $('#BookingContainer1_CancelBooking1_lblErrorMessage').text(message)
            return retValue;
            
        }
        function ValidateEmail($email) {
            //var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return emailReg.test($email);
        }
    </script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35407162-1']);
        _gaq.push(['_setDomainName', 'thlrentalsb2b.com']);
        _gaq.push(['_trackPageview']);

        (function () {

            _gaq.push(['_trackEvent', 'QuotesAndBookingsPage', 'logged', document.getElementById('BookingContainer1_BookingList1_ddlAgents').options[1].value]);

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</body>
</html>
