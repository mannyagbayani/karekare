﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class Content_Confirm : System.Web.UI.Page
{
    private string B2BAgent;

    
    

    protected void Page_Load(object sender, EventArgs e)
    {
       AgentLoginData aLoginData = Session["AgentData"] as AgentLoginData;
        AvailabilityRequest CurrentRequest = SessionManager.GetSessionData().AvailabilityRequestData;
        bool IsGrossRequest = aLoginData.GrossDefaultOn || (CurrentRequest as B2BAvailabilityRequest).b2bParams.isGross;        
        string bookingId = Request.Params["bid"];// "W6761706/1";
        StandardConfirmation1.InitStandardConfirmation(bookingId, false, IsGrossRequest);

        //-----------------------------------------------------------------
        //rev:mia 11March2015 B2B Auto generated Aurora Booking Confirmation
        //-----------------------------------------------------------------
        B2BAgent = GetB2BAgent();
        THL.Booking.PaymentManager pm = new PaymentManager();
        string agentemail = pm.GetB2BAgentEmailAddress(B2BAgent);
        if (IsAgentlHasAvalidEmail(agentemail))
        {
            if (string.IsNullOrEmpty(emailConfirmationSentToBox.Value))
                emailConfirmationSentToBox.Value = agentemail;
        }
        else
            StandardConfirmation1.SendingEmailErrorMessage = "Agent email address is not specified or invalid. Please contact reservations.";// agentemail.Replace("/Multiple", "").Replace("/Empty", "");


        pm = null;
        
        StandardConfirmation1.BubbleClick += new EventHandler(BubbleClick);



        if (StandardConfirmation1.IsCustomerMode)
            confirmationtable.Visible = false;
        else
            confirmationtable.Visible = true;
        //-----------------------------------------------------------------


    }

    #region rev:mia 11March2015 B2B Auto generated Aurora Booking Confirmation
    
    private void BubbleClick(object sender, EventArgs e)
    {
        if (StandardConfirmation1.GetBookingStatus != "Quotation")
        {
            if (StandardConfirmation1.IsCustomerMode)
                confirmationtable.Visible = false;
            else
                confirmationtable.Visible = true;
        }
        else
            confirmationtable.Visible = false;
    }

    bool IsAgentlHasAvalidEmail(string agentemail)
    {
        if (agentemail.Contains("ERROR") == true)
        {
            ButtonSendEmail.Enabled = false;
            emailConfirmationSentToBox.Disabled = true;
            confirmationmessage.ForeColor = System.Drawing.Color.Red;
            //agentemail = agentemail.Replace("/Multiple", "").Replace("/Empty", "").Replace("ERROR:", "");
            agentemail = "Agent email address is not specified or invalid. Please contact reservations"; //agentemail.Replace("ERROR:", "");
          
            
            return false;
        }
        else
            ClearErrorMessage();


        return true;
    }

    void ClearErrorMessage()
    {
        confirmationmessage.Text = "";
        ButtonSendEmail.Enabled = true;
        emailConfirmationSentToBox.Disabled = false;
    }

    protected void ButtonSendEmail_Click(object sender, EventArgs e)
    {
        //make email default to the agent for both sender and receiver
        ClearErrorMessage();
        string email = "";

        //check if page is valid
        if (!Page.IsValid)
        {

            ButtonSendEmail.Enabled = true;
            return;
        }

        if (!string.IsNullOrEmpty(emailConfirmationSentToBox.Value))
            email = emailConfirmationSentToBox.Value;
        else
            ButtonSendEmail.Enabled = true;


        if (!string.IsNullOrEmpty(email))
        {
            if (string.IsNullOrEmpty(B2BAgent))
                B2BAgent = GetB2BAgent();


            if ((email.Contains("@") == true))
            {
                string[] splitemail = email.Split('@');
                if ((!splitemail[1].Contains(".")))
                {
                    confirmationmessage.Text = "Agent email address is not specified. Please contact reservations";
                    confirmationmessage.ForeColor = System.Drawing.Color.Red;
                    return;
                }
            }
            else
            {
                confirmationmessage.Text = "Agent email address is not specified. Please contact reservations";
                confirmationmessage.ForeColor = System.Drawing.Color.Red;
                return;
            }

            THL.Booking.PaymentManager pm = new PaymentManager();
            string agentemail = pm.GetB2BAgentEmailAddress(B2BAgent);
            if (IsAgentlHasAvalidEmail(agentemail))
            {
                string content = StandardConfirmation1.ConfirmationRawContent;
                string subject = StandardConfirmation1.GetSubject();
                string result = pm.SendConfirmationEmail(email, subject, content, B2BAgent);


                string bookingId = Request.Params["bid"];// "W6761706/1";
                StandardConfirmation1.InitStandardConfirmation(bookingId, false, StandardConfirmation1.IsCustomerMode);
                ButtonSendEmail.Enabled = true;
                confirmationmessage.ForeColor = (result.Contains("OK") ? System.Drawing.Color.Blue : System.Drawing.Color.Red);
                result = result.Replace("OK:", "").Replace("ERROR:", "");
                confirmationmessage.Text = result;
            }
            pm = null;
        }
    }

    string GetB2BAgent()
    {
        string b2bagentcode = "";

        //todo: its should be getting from dbase
        if (Session["agentinformation.agentcode"] != null)
            b2bagentcode = Session["agentinformation.agentcode"].ToString();
        else
        {
            if (Request.Cookies["agentinformation"] != null)
            {
                b2bagentcode = Request.Cookies["agentinformation"]["agentcode"];
            }
        }
        return b2bagentcode;
    }

    string GetAgentEmailAddressCookieOrSession()
    {
        string email = "";
        try
        {
            //check firt the session
            if (Session["agentinformation.emailaddress"] != null)
                email = Session["agentinformation.emailaddress"].ToString();
            else
            {
                if (Request.Cookies["agentinformation"] != null)
                {
                    email = Request.Cookies["agentinformation"]["emailaddress"];
                }
            }


        }
        catch (Exception ex)
        {
            return "";
        }

        return email;
    }
    #endregion

    

}