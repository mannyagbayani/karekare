<%@ Register TagPrefix="uc1" TagName="PreRegister" Src="../Controls/Pre-Register/PreRegister.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Pre-Registration.aspx.vb" Inherits="THLAuroraWebInterface.Pre_"%>
<%@ Register TagPrefix="uc1" TagName="FooterImage" Src="../Controls/Graphics/FooterImage.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Pre-Registration</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
	</HEAD>
	<body background='<%=BackgroundURL()%>'>
		<table id="Table1" borderColor="#999999" cellSpacing="0" cellPadding="0" width="746" border="0">
			<tr>
				<td>
					<form id="Form1" method="post" runat="server">
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="746" border="0">
							<TR>
								<TD>
									<uc1:Header id="Header1" runat="server"></uc1:Header></TD>
							</TR>
							<TR>
								<TD>
									<uc1:PreRegister id="PreRegister1" runat="server"></uc1:PreRegister></TD>
							</TR>
						</TABLE>
					</form>
				</td>
			</tr>
		</table>
		<uc1:FooterImage id="FooterImage1" runat="server"></uc1:FooterImage>
	</body>
</HTML>
