﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BookingReports.aspx.vb" Inherits="THLAuroraWebInterface.BookingReports" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Booking Reports</title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=8" />

    <link rel="stylesheet" type="text/css" href="../Styles.css" />
    <link href="<%= AssetBasePath %>/css/b/jcal.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<%= AssetBasePath %>/css/b2b/jquery.ui.autocomplete.css" />
    <link href="<%= AssetBasePath %>/css/b2b/bc.css" rel="stylesheet" type="text/css" />
    <link href="/css/b2b_local.css" rel="stylesheet" type="text/css" />
    <link href="/css/bookingReports.css" rel="stylesheet" type="text/css" />

    <script src="<%= HttpPrefix%>ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js" type="text/javascript"></script>
    <script src="<%= HttpPrefix%>ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="/js/jquery.ba-bbq.min.js" type="text/javascript"></script>
    <script src="/js/common.js" type="text/javascript"></script>
    <script src="/js/bookingReports.js" type="text/javascript"></script>
    <script src="/js/reportHelper.js" type="text/javascript"></script>
    <script src="/js/report.js" type="text/javascript"></script>
    <script src="/js/bookingReportsData.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var reports = bookingReportsData.getJson();
            reportHelper.initialise($('#report'), $('#reportParams'));
            bookingReports.initialise('http://tdc-sql-006/ReportServer/Pages/ReportViewer.aspx');
            bookingReports.updateReports(reports);
        });
    </script>
</head>
<body>
    <uc1:Header ID="Header1" runat="server"></uc1:Header>
    <label for="report" class="left">Report: </label>
    <select id="report" class="left">
    </select>

    <div id="reportParams"></div>
    
    <button id="getReport" type="button" class="left">View Report</button>
    <div id="reportingDiv" style="width: 100%; height: 100%;">
        <iframe id="reportIframe" style="width: 100%; height: 100%;" src=""></iframe>
    </div>
</body>
</html>
