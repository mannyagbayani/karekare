﻿Imports System.Net
Imports System.Text
Imports System.Xml
Imports THLAuroraWebInterface.WebServices
Imports MSXML



Public Class TestWebService
    Inherits System.Web.UI.Page

    Dim serviceUrl As String = "http://localhost/THLRentals.OTA.Utils/Aurora_OTA.asmx"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim request As HttpWebRequest = CType(HttpWebRequest.Create(serviceUrl), HttpWebRequest)

        CallWebMethod("http://localhost/THLRentals.OTA.Utils/Aurora_OTA.asmx", "VehRes")
        'request.Method = "Post"

        'request.ContentType = "application/x-www-form-urlencoded"

        'Dim en As Encoding = Encoding.GetEncoding("iso-8859-1")
        'Dim doc As New XmlDocument



        'doc.LoadXml(testString.ToString())
        'Dim rawXml As String = doc.OuterXml






        'Dim receivingxmldoc As FileWebResponse

        ' ''Dim otaBase As String = System.Web.Configuration.WebConfigurationManager.AppSettings("AuroraOTABase")
        ' ''Dim otaProxy As New Aurora_OTAV3(otaBase)

        ' ''otaProxy.VehRes(doc)
        ' ''XmlService ser = new XmlService();  //Your web srevice..

        ''ReceivingXmlDoc = ser.SampelXmlMethod(SendingXmlDoc);

        ' ''// you need to encode your Xml before you assign it to your parameter
        ' ''// the POST parameter name is myxmldata
        'Try


        '    Dim requestText As String = String.Format("myxmldata={0}", HttpUtility.UrlEncode(rawXml, en))
        '    Dim requeststream As Stream = request.GetRequestStream()
        '    Dim requestwriter As StreamWriter = New StreamWriter(requeststream, en)
        '    receivingxmldoc = CType(request.GetResponse(), FileWebResponse)

        '    requestwriter.Write(requestText)

        '    requestwriter.Close()
        'Catch ex As Exception
        '    Dim abc As String = ex.InnerException.Message
        'End Try



    End Sub

    Public Function CallWebMethod(webServiceURL As String, webMethod As String) As String
        Try
            Dim _requestData As Byte() = Me.CreateHttpRequestData()

            Dim uri As String = Convert.ToString(webServiceURL & Convert.ToString("/")) & webMethod
            Dim _httpRequest As HttpWebRequest = DirectCast(HttpWebRequest.Create(uri), HttpWebRequest)
            _httpRequest.Method = "POST"
            _httpRequest.KeepAlive = False
            _httpRequest.ContentType = "application/x-www-form-urlencoded"
            _httpRequest.ContentLength = _requestData.Length
            _httpRequest.Timeout = 30000
            Dim _httpResponse As HttpWebResponse = Nothing
            Dim _response As String = String.Empty

            _httpRequest.GetRequestStream().Write(_requestData, 0, _requestData.Length)
            _httpResponse = DirectCast(_httpRequest.GetResponse(), HttpWebResponse)
            Dim _baseStream As System.IO.Stream = _httpResponse.GetResponseStream()
            Dim _responseStreamReader As New System.IO.StreamReader(_baseStream)
            _response = _responseStreamReader.ReadToEnd()
            _responseStreamReader.Close()

            Return _response
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Private Function CreateHttpRequestData() As Byte()
        Dim _sbParameters As New StringBuilder()
        'For Each param As String In dic.Keys
        '    _sbParameters.Append(param)
        '    'key => parameter name 
        '    _sbParameters.Append("="c)
        '    _sbParameters.Append(dic(param))
        '    'key value 
        '    _sbParameters.Append("&"c)
        'Next
        '_sbParameters.Remove(_sbParameters.Length - 1, 1)

        Dim testString As New StringBuilder
        testString.Append("<?xml version=""1.0"" encoding=""UTF-8""?>")
        testString.Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
        testString.Append("<soap:Body>")
        testString.Append("<VehRes xmlns=""http://www.thlrentals.com/Aurora/OTA"">")
        testString.Append("<OTA_VehResRQ Version=""1.002"" TimeStamp=""2015-03-06T21:26:53.790+10:00"" Target=""Production"" EchoToken=""5E1EF979219D43D29D6987E61478ADBA"" xmlns=""http://www.opentravel.org/OTA/2003/05"">")
        testString.Append("<POS>")
        testString.Append("<Source AgentSine=""JONATHONP"" AgentDutyCode=""Yes~Yes~G~G~No"" ERSP_UserID=""JONATHONP"">")
        testString.Append("<RequestorID Type=""22"" ID=""DRIVEMEL""/>")
        testString.Append("</Source>")
        testString.Append("</POS>")
        testString.Append("<VehResRQCore Status=""Available"">")
        testString.Append("<VehRentalCore PickUpDateTime=""2015-10-07T08:00:00.000+11:00"" ReturnDateTime=""2015-10-12T08:00:00.000+11:00"">")
        testString.Append("<PickUpLocation LocationCode=""SYD""/>")
        testString.Append("<ReturnLocation LocationCode=""SYD""/>")
        testString.Append("</VehRentalCore>")
        testString.Append("<Customer>")
        testString.Append("<Primary>")
        testString.Append("<PersonName>")
        testString.Append("<GivenName>Jonathon</GivenName>")
        testString.Append("<MiddleName>NA</MiddleName>")
        testString.Append("<Surname>Test</Surname>")
        testString.Append("<NameTitle>NA</NameTitle>")
        testString.Append("</PersonName>")
        testString.Append("<Telephone PhoneNumber=""+61 555 555 555"" FormattedInd=""false"" DefaultInd=""false""/>")
        testString.Append("<Email>savemore@drivenow.com.au</Email>")
        testString.Append("<Address FormattedInd=""false"" DefaultInd=""false"">")
        testString.Append("<StreetNmbr PO_Box=""NA""/>")
        testString.Append("<AddressLine>NA</AddressLine>")
        testString.Append("<AddressLine>NA</AddressLine>")
        testString.Append("<CityName>NA</CityName>")
        testString.Append("<PostalCode>NA</PostalCode>")
        testString.Append("</Address>")
        testString.Append("<CitizenCountryName Code=""AU""/>")
        testString.Append("</Primary>")
        testString.Append("</Customer>")
        testString.Append("<VendorPref CompanyShortName=""Mighty""/>")
        testString.Append("<TPA_Extensions ReserveType=""Book""/>")
        testString.Append("</VehResRQCore>")
        testString.Append("<VehResRQInfo>")
        testString.Append("<RentalPaymentPref>")
        testString.Append("<PaymentCard CardType=""1"" CardNumber=""#masked#"" CardCode=""CA"" ExpireDate=""0117"">")
        testString.Append("<CardHolderName>test card</CardHolderName>")
        testString.Append("</PaymentCard>")
        testString.Append("</RentalPaymentPref>")
        testString.Append("<Reference Type=""1"" ID=""NA"" ID_Context=""L9XDHY""/>")
        testString.Append("</VehResRQInfo>")
        testString.Append("</OTA_VehResRQ>")
        testString.Append("</VehRes>")
        testString.Append("</soap:Body>")
        testString.Append("</soap:Envelope>")


        Dim nimstring As StringBuilder = getXMLString()
        Dim encoding As New UTF8Encoding()

        Return encoding.GetBytes(nimstring.ToString())

    End Function

    


    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
       

        '// Write the response XML to an XML file
        'TextWriter tw = new StreamWriter("C:\\XML\\response.xml");
        'tw.WriteLine(strSOAPResponse);
        'tw.Close();

        Dim objXmlHTTP As New MSXML.XMLHTTPRequest

        Dim xmlDoc As New System.Xml.XmlDocument
        Dim strSOAPResponse As String
        Try

        
        
            xmlDoc.LoadXml(getXMLString.ToString())
            'xmlDoc.LoadXml(getXMLFromActualRequest.ToString())

        objXmlHTTP.open("POST", serviceUrl, False, Nothing, Nothing)
            'objXmlHTTP.setRequestHeader("SOAPAction:", serviceUrl)
        objXmlHTTP.send(xmlDoc.InnerXml)
            strSOAPResponse = objXmlHTTP.responseText.ToString()
        Catch ex As Exception
            Dim exceptionMessage As String = ex.ToString
        End Try


    End Sub

    Private Function getXMLFromActualRequest() As StringBuilder
        Dim returnString As New StringBuilder
        returnString.Append("<?xml version=""1.0"" encoding=""UTF-8""?><soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Body><VehRes xmlns=""http://www.thlrentals.com/Aurora/OTA"">")
        returnString.Append("<OTA_VehResRQ Version=""1.002"" TimeStamp=""2015-03-09T12:17:48.981+10:00"" Target=""Production"" EchoToken=""610EC62274E541918459A2E0ADD856F6"" xmlns=""http://www.opentravel.org/OTA/2003/05"" > ")
        returnString.Append("<POS>")
        returnString.Append("<Source AgentSine=""JONATHONP"" AgentDutyCode=""Yes~Yes~G~G~No"" ERSP_UserID=""JONATHONP"" > ")
        returnString.Append("<RequestorID Type=""22"" ID=""DRIVEMEL""/>")
        returnString.Append("</Source>")
        returnString.Append("</POS>")
        returnString.Append("<VehResRQCore Status=""Available"">")
        returnString.Append("<VehRentalCore PickUpDateTime=""2015-10-19T07:00:00.000+10:00"" ReturnDateTime=""2015-10-26T07:00:00.000+10:00"">")
        returnString.Append("<PickUpLocation LocationCode=""BNE""/>")
        returnString.Append("<ReturnLocation LocationCode=""BNE""/>")
        returnString.Append("</VehRentalCore>")
        returnString.Append("<Customer>")
        returnString.Append("<Primary>")
        returnString.Append("<PersonName>")
        returnString.Append("<GivenName>Jonathon</GivenName>")
        returnString.Append("<MiddleName>NA</MiddleName>")
        returnString.Append("<Surname>Test</Surname>")
        returnString.Append("<NameTitle>NA</NameTitle>")
        returnString.Append("</PersonName>")
        returnString.Append("<Telephone PhoneNumber=""+61 555 555 555"" FormattedInd=""false"" DefaultInd=""false""/>")
        returnString.Append("<Email>savemore@drivenow.com.au</Email>")
        returnString.Append("<Address FormattedInd=""false"" DefaultInd=""false"">")
        returnString.Append("<StreetNmbr PO_Box=""NA""/>")
        returnString.Append("<AddressLine>NA</AddressLine>")
        returnString.Append("<AddressLine>NA</AddressLine>")
        returnString.Append("<CityName>NA</CityName>")
        returnString.Append("<PostalCode>NA</PostalCode>")
        returnString.Append("</Address>")
        returnString.Append("<CitizenCountryName Code=""AU""/>")
        returnString.Append("</Primary>")
        returnString.Append("</Customer>")
        returnString.Append("<VendorPref CompanyShortName=""Britz""/>")
        returnString.Append("<TPA_Extensions ReserveType=""Book""/>")
        returnString.Append("</VehResRQCore>")
        returnString.Append("<VehResRQInfo>")
        returnString.Append("<RentalPaymentPref>")
        returnString.Append("<PaymentCard CardType=""1"" CardNumber=""#masked#"" CardCode=""CA"" ExpireDate=""0117"">")
        returnString.Append("<CardHolderName>test card</CardHolderName>")
        returnString.Append("</PaymentCard>")
        returnString.Append("</RentalPaymentPref>")
        returnString.Append("<Reference Type=""1"" ID=""NA"" ID_Context=""3XFBPM""/>")
        returnString.Append("</VehResRQInfo>")
        returnString.Append("</OTA_VehResRQ>")
        returnString.Append("</VehRes></soap:Body></soap:Envelope>")
        Return returnString
    End Function
    Private Function getXMLString() As StringBuilder
        Dim returnString As New StringBuilder
        returnString.Append("<?xml version=""1.0""?>")
        returnString.Append("<Data>")
        returnString.Append("<RentalDetails>")
        returnString.Append("<AvpId>F85E8D101B714B5DAF6BDC6A523C9EB7</AvpId>")
        returnString.Append("<AgentReference>Test</AgentReference>")
        returnString.Append("<QuantityRequested>1</QuantityRequested>")
        returnString.Append("<Status>KK</Status>")
        returnString.Append("<CheckOutDateTime>07-Oct-2015 10:00</CheckOutDateTime>")
        returnString.Append("<CheckOutLocationCode></CheckOutLocationCode>")
        returnString.Append("<CheckInDateTime>12-Oct-2015 15:00</CheckInDateTime>")
        returnString.Append("<CheckInLocationCode></CheckInLocationCode>")
        returnString.Append("<QuantityRequested />")
        returnString.Append("<CountryCode>NONE</CountryCode>")
        returnString.Append("<Note></Note>")
        returnString.Append("<UserCode>USR8</UserCode>")
        returnString.Append("<ConfirmationHeaderText></ConfirmationHeaderText>")
        returnString.Append("<ConfirmationFooterText>View the Generic Terms &amp; Conditions () for your rental.</ConfirmationFooterText>")
        returnString.Append("<GetConfXML>False</GetConfXML>")
        returnString.Append("<IsRequestBooking>False</IsRequestBooking>")
        returnString.Append("<SelectedSlot></SelectedSlot>")
        returnString.Append("<SlotId>0</SlotId>")
        returnString.Append("</RentalDetails>")
        returnString.Append("<CustomerData>")
        returnString.Append("<Title>Mr</Title>")
        returnString.Append("<FirstName>Jonathon</FirstName>")
        returnString.Append("<LastName>Test</LastName>")
        returnString.Append("<Email></Email>")
        returnString.Append("<PhoneNumber></PhoneNumber>")
        returnString.Append("<CountryOfResidence>AU</CountryOfResidence>")
        returnString.Append("</CustomerData>")
        returnString.Append("</Data>")
        Return returnString
    End Function

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
       
        Try
            Dim strURL As String = "http://otautils.thlonline.com/Aurora_OTA.asmx"
            'Dim strURL As String = serviceUrl
            'go find the raw XML

            'load the XML
            Dim xmlDoc As New System.Xml.XmlDocument()
            xmlDoc.LoadXml(getXMLFromActualRequest.ToString())
            Dim request As HttpWebRequest = DirectCast(WebRequest.Create(strURL), HttpWebRequest)


            'set the properties
            request.Method = "POST"
            request.ContentType = "text/xml"
            request.Timeout = 30 * 1000
            'open the pipe?
            Dim request_stream As Stream = request.GetRequestStream()
            'write the XML to the open pipe (e.g. stream)
            xmlDoc.Save(request_stream)
            'CLOSE THE PIPE !!! Very important or next step will time out!!!!
            request_stream.Close()

            'get the response from the webservice
            Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
            Dim r_stream As Stream = response.GetResponseStream()

            'convert it
            Dim response_stream As New StreamReader(r_stream, System.Text.Encoding.GetEncoding("utf-8"))
            Dim sOutput As String = response_stream.ReadToEnd()

            'display it
            Dim responseString As String = sOutput


            'clean up!
            response_stream.Close()
        Catch ex As Exception
            Dim errMessage As String = ex.Message
        End Try



    End Sub
End Class