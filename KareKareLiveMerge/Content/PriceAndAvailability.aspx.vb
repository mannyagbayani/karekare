'--------------------------------------------------------------------------------------
'   Class:          PriceAndAvailability
'   Description:    Allows user to get the price and availability of a vehicle based
'                   on their selections. This page can be neterd directly from outside the 
'                   app. Settings are retreived from querystring if that is the case.
'--------------------------------------------------------------------------------------

Partial Class PriceAndAvailability
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        'General.MaintainSessionObjects(General.PageName.PriceAndAvailability, Session)

    End Sub

#End Region

    Protected WithEvents Availability1 As Availability
    Protected WithEvents Bookingcontainer1 As BookingContainer
    Protected WithEvents LocationAndTime1 As LocationAndTime
    Public RS As String

    Public Property BackgroundURL() As String
        Get
            Return CStr(viewstate("BackgroundURL"))
        End Get
        Set(ByVal Value As String)
            viewstate("BackgroundURL") = Value
        End Set
    End Property

    ' Customer Confirmation Changes
    Public WriteOnly Property GrossModeSelected() As Boolean
        Set(ByVal value As Boolean)
            Session("GrossModeSelected") = value
        End Set
    End Property

    Public WriteOnly Property NettModeSelected() As Boolean
        Set(ByVal value As Boolean)
            Session("NettModeSelected") = value
        End Set
    End Property
    ' Customer Confirmation Changes

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        ' three scenarios
        '1 - User has come form a maui or britz website and want to make a booking, 
        '       They create an availability matrix but have to log in before making a boooking
        '2-the user has proccedded to log in and have been returned here to make the booking
        '3-The customer or agent want to get vehicle avaiability from within the site
        If Not IsPostBack Then

            Dim country As String
            Dim agent As String
            Dim category As String
            Dim brand As String
            Dim customerPOS(0) As SourceType
            Dim mRequestorId As New UniqueID_Type

            'hide rows that don't need to be shown
            AvailabilityRow.Visible = False
            BookingContainerRow.Visible = False

            Me.BackgroundURL = General.BackGroundImageURL(Session)

            ' See if this page was requested from an external site
            If Not IsNothing(Request.QueryString("agent")) Then


                agent = Request.QueryString("agent")

                category = Request.QueryString("category")
                brand = Request.QueryString("brand")
                country = Request.QueryString("country")

                If IsNothing(Session("POS")) Then
                    customerPOS(0) = New SourceType
                    customerPOS(0).AgentSine = "anonymous"
                    customerPOS(0).ERSP_UserID = "anonymous"
                    mRequestorId.ID = "WEBMAUI"
                    mRequestorId.Type = "22"
                    customerPOS(0).RequestorID = mRequestorId
                    Session("POS") = customerPOS(0)
                End If

                'Populate location and time control
                LocationAndTime1.InitLocationAndTime()
                LocationAndTime1.SetupScreen(country, category, brand)
                'Display available vehicles based on info from querystring
                LocationAndTime1.GetAvailability()
                Availability1.Visible = True

                ' See if the user has just logged in and been returned back to this page
            ElseIf Not IsNothing(Request.QueryString("No")) AndAlso Not IsNothing(Request.QueryString("type")) Then

                waitingMessageRow.Visible = False
                Dim bookingtype As TransactionActionType = CType(Request.QueryString("type"), TransactionActionType)
                ' Need to load location and time even thouhg it won't be shown but it can be accessed from 
                ' clicking th back button in make booking.
                LocationAndTime1.InitLocationAndTime()
                ' Make booking
                ' Gross mode selected = Customer mode
                Bookingcontainer1.InitBookingMode(BookingContainer.BookingMode.MakeBooking, Request.QueryString("No"), bookingtype, Availability1.GrossModeSelected)
                LocationAndTime1.Visible = False
                Bookingcontainer1.Visible = True
                LocationAndTimeRow.Visible = False
                AvailabilityRow.Visible = False
                BookingContainerRow.Visible = True

            Else
                'Just show the default location and time

                LocationAndTime1.Visible = True
                LocationAndTime1.InitLocationAndTime()
                Bookingcontainer1.Visible = False
                LocationAndTimeRow.Visible = True
                AvailabilityRow.Visible = False
                BookingContainerRow.Visible = False
            End If
        End If

    End Sub

    Private Sub Availability1_BookClicked(ByVal ID As String) Handles Availability1.BookClicked
        ' An available quote has been selected, now make booking 

        ' Hide the row that contains the "Please wait message", mucks up formatting otherwise
        waitingMessageRow.Visible = False

        checkLoginStatus(ID, TransactionActionType.Book)
        Availability1.Visible = False
        LocationAndTime1.Visible = False
        Bookingcontainer1.Visible = True
        Bookingcontainer1.InitBookingMode(BookingContainer.BookingMode.MakeBooking, ID, TransactionActionType.Book, Availability1.GrossModeSelected)
        LocationAndTimeRow.Visible = False
        AvailabilityRow.Visible = False
        BookingContainerRow.Visible = True
    End Sub


    Private Sub Availability1_BookProvisionalClicked(ByVal ID As String) Handles Availability1.BookProvisionalClicked
        ' An available quote has been selected, now make booking

        ' Hide the row that contains the "Please wait message"
        waitingMessageRow.Visible = False
        checkLoginStatus(ID, TransactionActionType.Hold)
        Availability1.Visible = False
        LocationAndTime1.Visible = False
        Bookingcontainer1.Visible = True
        Bookingcontainer1.InitBookingMode(BookingContainer.BookingMode.MakeBooking, ID, TransactionActionType.Hold, Availability1.GrossModeSelected)
        LocationAndTimeRow.Visible = False
        AvailabilityRow.Visible = False
        BookingContainerRow.Visible = True
    End Sub

    Private Sub Availability1_SaveQuoteClicked(ByVal ID As String) Handles Availability1.SaveQuoteClicked
        ' An available quote has been selected, now make booking

        ' Hide the row that contains the "Please wait message"
        waitingMessageRow.Visible = False

        checkLoginStatus(ID, TransactionActionType.Quote)
        Availability1.Visible = False
        LocationAndTime1.Visible = False
        Bookingcontainer1.Visible = True
        Bookingcontainer1.InitBookingMode(BookingContainer.BookingMode.MakeBooking, ID, TransactionActionType.Quote, Availability1.GrossModeSelected)
        LocationAndTimeRow.Visible = False
        AvailabilityRow.Visible = False
        BookingContainerRow.Visible = True
    End Sub

    Private Sub LocationAndTime1_GetAvailabilityClicked(ByVal category As String, ByVal type As String, ByVal pickUpLocation As String, ByVal returnLocation As String, ByVal pickUpDateTime As Date, ByVal returnDateTime As Date, ByVal promoCode As String, ByVal agentCode As String, ByVal SizeCode As String, ByVal VehicleCode As String) Handles LocationAndTime1.GetAvailabilityClicked

        ' Customer Confirmation Changes
        ' zap the previous gross / nett settings
        GrossModeSelected = False
        NettModeSelected = False
        ' Customer Confirmation Changes

        ' Load availability matrix, based on user selected  info

        ' Show the row that contains the "Please wait message"
        waitingMessageRow.Visible = True

        Availability1.InitAvailabilityData(category, type, pickUpLocation, returnLocation, pickUpDateTime, returnDateTime, promoCode, agentCode, SizeCode, VehicleCode)
        Availability1.Visible = True
        AvailabilityRow.Visible = True
        BookingContainerRow.Visible = False

    End Sub
   

    Private Sub checkLoginStatus(ByVal Id As String, ByVal bookingtype As TransactionActionType)
        ' This is used to see if a user is logged in as they can get this far without doing so
        ' store quoteid in session. If not logged in redirect them to login, but pass quote id
        ' so they can make booking when they return

        If IsNothing(Session("POS")) Then
            Response.Redirect("AgentLogin.aspx?returnURL=PriceAndAvailability.aspx&No=" & Id & "&type=" & bookingtype)
        Else
            Dim userPos() As SourceType = New SourceType() {CType(Session("POS"), SourceType)}
            If userPos(0).AgentSine = "anonymous" Then
                Response.Redirect("CustomerLogin.aspx?returnURL=PriceAndAvailability.aspx&No=" & Id & "&type=" & bookingtype)
            End If
        End If
    End Sub


    Private Sub Bookingcontainer1_BackToAvailability() Handles Bookingcontainer1.BackToAvailability
        ' The user clicked the back button when viewing the booking summary
        ' show location and time and vehicle availability

        ' Showthe row that contains the "Please wait message"
        waitingMessageRow.Visible = True

        Availability1.Visible = True
        LocationAndTime1.Visible = True
        Bookingcontainer1.Visible = False

        LocationAndTimeRow.Visible = True
        AvailabilityRow.Visible = True
        BookingContainerRow.Visible = False
    End Sub
End Class
