<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="B2BOptions.aspx.vb" Inherits="THLAuroraWebInterface.B2BOptions" %>

<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FooterImage" Src="../Controls/Graphics/FooterImage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="B2BCurrencyOptions" Src="~/Controls/B2BOptions/B2BCurrencyOptions.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>B2B Options</title>
    <link rel="stylesheet" type="text/css" href="../Styles.css">
    <script src="../Controls/B2BOptions/DefaultAndSelection.js" type="text/javascript"></script>
    <style type="text/css">
    .CleanGrid 
    {
    	border:none;
    }
    
    .CleanGrid TD
    {
    	border:none;
    }
    
    </style>
</head>
<body background='<%=BackgroundURL()%>'>
    <form runat="server" id="form2">
    <table id="Table1" bordercolor="#999999" cellspacing="0" cellpadding="0" width="751"
        border="0" style="width: 751px; height: 63px" class="B2BOptions">
        <tr>
            <td>
                <table id="Table2" cellspacing="0" cellpadding="0" width="746" border="0" style="width: 746px;
                    height: 37px">
                    <tr>
                        <td valign="top">
                            <uc1:Header ID="Header1" runat="server"></uc1:Header>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <uc1:B2BCurrencyOptions id="objB2BCurrencyOptions" runat="server">
                            </uc1:B2BCurrencyOptions>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <asp:Label ID="lblLoadError" runat="server" CssClass="warning"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <uc1:FooterImage ID="FooterImage1" runat="server"></uc1:FooterImage>
    </form>
</body>
</html>
