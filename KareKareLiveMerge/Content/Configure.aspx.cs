﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using THL.Booking;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml;

public partial class Content_Configure : System.Web.UI.Page
{

    /// <summary>
    /// Get the Exchange Rate for the Selected Agent's Request
    /// </summary>
    public ExchangeRate SelectedCurrencyRate
    {
        get
        {
            DataAccess dal = new DataAccess();
            AgentLoginData aLoginData = dal.GetAgentData((SourceType)(Session["POS"]));
            string bookingCurCode = CurrencyManager.GetCurrencyForCountryCode(CurrentRequest.CountryCode).ToString();
            ExchangeRate exRate = new ExchangeRate();
            if (!string.IsNullOrEmpty(Request.Params["cur"]) && !string.IsNullOrEmpty(Request.Params["rate"]))//passed forward from selection
            {
                if (!aLoginData.HasCurrencyOption || String.IsNullOrEmpty(SelectedAgent.DefaultCurrency))
                {
                    exRate = SelectedAgent.GetExRateForCodes(bookingCurCode, bookingCurCode);
                }
                else
                {
                    if (Request.Params["curOff"] != null)
                        exRate = SelectedAgent.GetExRateForCodes(bookingCurCode, Request.Params["curOff"].ToUpper());
                    else
                        exRate = SelectedAgent.GetExRateForCodes(bookingCurCode, Request.Params["cur"].ToUpper());
                }
            }
            else//this agents default
                exRate = SelectedAgent.GetExRateForCodes(bookingCurCode, (!aLoginData.HasCurrencyOption || String.IsNullOrEmpty(SelectedAgent.DefaultCurrency)) ? bookingCurCode : SelectedAgent.DefaultCurrency);
            return exRate;
        }
    }

    public AvailabilityRequest CurrentRequest;

    public string TaxMessage { get; set; }

    public AgentLoginData AgentData;

    /// <summary>
    /// Secure or Not
    /// </summary>
    public string HTTPPrefix { get { return ApplicationManager.HTTPPrefix; } }


    public static string AssetBasePath
    {
        get { return ApplicationManager.HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"]; }
    }

    AvailabilityItem _availabilityItem;

    /// <summary>
    /// Add Availability Request context awareness
    /// </summary>
    public bool IsAlternativeAvailability { get { return _availabilityItem.IsAlternative; } }

    public string VehicleName
    {
        get
        {
            return THLBrand.GetNameForBrand(_availabilityItem.Brand) + " " + _availabilityItem.VehicleName;
        }

    }

    public string PackageCode
    {
        get
        {
            return _availabilityItem.PackageCode;
        }
    }

    /// <summary>
    /// Vehicle Content and Features MOSS Pages location on parent site
    /// </summary>
    public string VehicleContentPage
    {
        get
        {
            THLBrands containerBrand = THLBrands.MAC;
            string VehicleContentLink = ContentManager.GetVehicleLink(_availabilityItem.Brand, CurrentRequest.CountryCode, _availabilityItem.VehicleCode, containerBrand);
            if (VehicleContentLink != null && VehicleContentLink.Length > 0) VehicleContentLink = VehicleContentLink.Replace("&amp;", "&");
            return VehicleContentLink;
        }
    }

    public string CurrencyPrefix
    {
        get
        {
            return CurrencyManager.GetCurrencyForCountryCode(CurrentRequest.CountryCode).ToString() + " " + "$";
            //TODO: implement CurrencyManager.GetCurrencySymbolForCode();
        }
    }

    /// <summary>
    /// Vehicle Thumb Image within central assets
    /// </summary>
    public string VehicleImagePath
    {
        get
        {
            string vehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(CurrentRequest.CountryCode, _availabilityItem.Brand, CurrentRequest.VehicleType, _availabilityItem.VehicleCode, Content_Configure.AssetBasePath, false) + "-ClearCut-144.jpg";
            return vehicleImagePath;
        }
    }

    /// <summary>
    /// Vehicle Inclusions (Features) within central assets
    /// </summary>
    public string VehicleInclusionPath
    {
        get
        {
            string vehicleInclusionPath = AvailabilityHelper.GetVehicleImageBasePath(CurrentRequest.CountryCode, _availabilityItem.Brand, CurrentRequest.VehicleType, _availabilityItem.VehicleCode, Content_Configure.AssetBasePath, true) + "-InclusionIcons.gif";
            return vehicleInclusionPath;
        }
    }

    /// <summary>
    /// Vehicle Charges location on SharePoint parent site
    /// </summary>
    public string ChargesURL
    {
        get
        {
            THL.Booking.VehicleType contentResourceVehicleType = CurrentRequest.VehicleType;
            if (_availabilityItem.VehicleCode.ToLower().Contains("pfmr")) contentResourceVehicleType = THL.Booking.VehicleType.Campervan;//Added exception 08/01/10 for PFMR
            string vehicleChargeContentURL = ContentManager.GetLinkElementForBrand(_availabilityItem.Brand, contentResourceVehicleType, CurrentRequest.CountryCode, "VehicleCharges", CurrentRequest.PickUpDate, THLBrands.MAC).Content;
            if (vehicleChargeContentURL != null && vehicleChargeContentURL.Length > 0) vehicleChargeContentURL = vehicleChargeContentURL.Replace("&amp;", "&");
            return vehicleChargeContentURL;
        }
    }

    public string PriceDetails;
    public AvailabilityItem AvailabilityItemElm
    {
        get { return _availabilityItem; }
    }

    public string InusranceOptionsStr, InsuranceProductJSON;
    public string ExtraHiresTable, ExtraHireItemsJSON, FerryPanel, FerryJSON;

    public string PriceDetailsTable;

    public decimal pta;

    public decimal pap;

    AgentDescription SelectedAgent;

    public bool BonusPackAvailable;

    public bool IsGrossRequest;

    AvailabilityItem _bonusPackAvailability;

    public bool hasDiscountDays;

    public bool HasDiscountedDays
    {
        get { return AvailabilityHelper.HasDiscountedDays(_availabilityItem.GetRateBands()); }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        B2BSessionCache sessionCache = SessionManager.GetSessionData().B2BCache;
        string avpId = Request.Params["avp"];


        if (!B2BAvailabilityHelper.SessionExpired && sessionCache != null && !string.IsNullOrEmpty(avpId))
        {
            AgentData = HttpContext.Current.Session["AgentData"] as AgentLoginData;
            CurrentRequest = SessionManager.GetSessionData().AvailabilityRequestData;

            if (Request.Url.ToString().IndexOf("/b2b/") != -1)
            {
                if (Request.Url.ToString().IndexOf("&brd") != -1)
                    Session["b2basync"] = Request.Url.ToString().Substring(Request.Url.ToString().IndexOf("/b2b/"));
                else
                    Session["b2basync"] = null;
            }
            else
            {
                Session["b2basync"] = null;
            }

            try
            {
                //note: from MAC site, there is no IsGross defined in the current request
                //      to solve this problem, i put this in url
                IsGrossRequest = (CurrentRequest as B2BAvailabilityRequest).b2bParams.isGross;


            }
            catch (Exception ex)
            {
                if (Request.Url.ToString().Contains("&IsGross") == true)
                    IsGrossRequest = Convert.ToBoolean(Request.Params["IsGross"]);
            }

            try
            {
                SelectedAgent = AgentData.GetAgentForCode(CurrentRequest.AgentCode.ToUpper());
            }
            catch (Exception agentCode)
            {
                if (Request.Url.ToString().Contains("&ac") == true)
                    SelectedAgent = AgentData.GetAgentForCode(Request.Params["ac"]);
            }

            try
            {
                ExchangeRate rate = SelectedCurrencyRate;
            }
            catch (Exception currency)
            {
            }



            try
            {
                _availabilityItem = sessionCache.GetAvailabilityForAVP(avpId);
            }
            catch (Exception agentCode)
            {
                if (Request.Url.ToString().Contains("&avp") == true)
                    _availabilityItem = sessionCache.GetAvailabilityForAVP(Request.Params["avp"]);

            }


            PackageManager pm = new PackageManager(_availabilityItem, ApplicationManager.GetApplicationData(), IsGrossRequest);
            _hasBonusPack = (Request.Params["inc"] == "1");

            if (_hasBonusPack)//Swap with Inc Availability
            {
                XmlDocument xDoc = new XmlDocument();
                string incPackXML = pm.GetIncPackageXML();
                if (!string.IsNullOrEmpty(incPackXML))
                {
                    xDoc.LoadXml("<data>" + pm.GetIncPackageXML() + "</data>");
                    _availabilityItem = AvailabilityHelper.FillFromXML(xDoc, CurrentRequest.Brand);
                    sessionCache.AddIncItem(_availabilityItem);
                }
                //store in cache:  
            }

            PriceDetailsTable = B2BAvailabilityDisplayer.RenderB2BPriceDetails(_availabilityItem, ref pta, ref pap);

            //Insurance
            _bonusPackAvailability = pm.GetInclusiveVehicleCharges(_availabilityItem.AVPID);

            BonusPackAvailable = (_bonusPackAvailability != null);//does this package has an inclusive option

            InsuranceProduct[] insuranceItems = pm.GetInsuranceItems(avpId);
            InsuranceProductDisplayer ipd = new InsuranceProductDisplayer(insuranceItems, _availabilityItem, CurrencyStr, CurrentRequest);

            string AllInclusivePanel = ipd.RenderAllInlusivePanel(avpId, _bonusPackAvailability, InclusiveMode, pm.InclusiveName, pm.InclusiveDailyRate, pm.InclusiveEstimatedTotal, pm.HasDTR, _availabilityItem.VehicleCode, pm);

            //-----------------------------------------------------------------------------------------------------
            //rev:mia APRIL 4 2013  - PART OF APCOVER ENHANCEMENT. 
            //-----------------------------------------------------------------------------------------------------    
            if (string.IsNullOrEmpty(AllInclusivePanel))
            {
                if (System.Web.HttpContext.Current.Request["prdId"] != null)
                {
                    string papmode = System.Web.HttpContext.Current.Request["pap"];
                    if (papmode == "papMode")
                        AllInclusivePanel = "<input type='hidden' id='HiddenHasCustomerCharge' name=HasCustomerChargeHidden value='pap' />";
                    else
                        AllInclusivePanel = "<input type='hidden' id='HiddenHasCustomerCharge' name='HasCustomerChargeHidden' value='pta' />";

                    AllInclusivePanel = AllInclusivePanel.Replace("name='HasCustomerChargeHidden'", "name='" + System.Web.HttpContext.Current.Request["prdId"].ToString() + "'");
                }
            }
            //-----------------------------------------------------------------------------------------------------    

            //-----------------------------------------------------------------------------------------------------
            //rev:mia APRIL 4 2013  - PART OF APCOVER ENHANCEMENT. 
            //-----------------------------------------------------------------------------------------------------    
            try
            {
                if (_bonusPackAvailability != null)
                    IsCustomerChargeHidden.Value = (_bonusPackAvailability.GetBonusPackCharge().IsCustomerCharge ? "pap" : "pta");
            }
            catch (Exception ex)
            { IsCustomerChargeHidden.Value = string.Empty; }
            //-----------------------------------------------------------------------------------------------------    

            InusranceOptionsStr = AllInclusivePanel + ipd.Render(InclusiveMode);
            InsuranceProductJSON = ipd.RenderJSON(_hasBonusPack && (_bonusPackAvailability != null));

            hasDiscountDays = AvailabilityHelper.HasDiscountedDays(_availabilityItem.GetRateBands());

            //Extra Hires
            ExtraHireItem[] EHItems = pm.GetExtraHireItems(avpId);
            ExtraHireItemsDisplayer ehd = new ExtraHireItemsDisplayer(EHItems, _availabilityItem.GetHirePeriod(), CurrentRequest, _availabilityItem);
            ExtraHiresTable = ehd.RenderExtraHirePanel();
            ExtraHireItemsJSON = ehd.RenderExtraHireJSON();
            AllInclusiveJSON = ipd.RenderAllInlusiveJSON();

            //Ferry
            FerryProduct[] ferryProducts = pm.GetFerryProducts(avpId);
            FerryProductDisplayer fpd = new FerryProductDisplayer(ferryProducts, CurrentRequest.PickUpDate, CurrentRequest.DropOffDate, CurrencyStr);
            FerryPanel = fpd.RenderFerryPanel();
            FerryJSON = fpd.RenderJSON();

            ConfigureDisclaimer = ContentManager.GetConfigureB2BMessage(IsGrossRequest, BookingStatus.Created);

            /*Added by Nimesh On 23rd Sept to display different message for tax for US based bookings*/
            try
            {
                if (CurrentRequest.CountryCode.ToString().ToUpper().Equals("US"))
                    //{
                    /*Start of Change*/
                    TaxMessage = "* Pre-paid items include US sales tax. Add local sales tax for items paid onsite.";
                /*End of Change*/

                //}
                else
                    TaxMessage = "Total includes all taxes (GST)";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }

            /*End Added by Nimesh On 23rd Sept to display different message for tax for US based bookings*/
        }
    }

    bool _hasBonusPack = false;

    public string VehicleJSON
    {
        get
        {
            return new VehicleDisplayer().VehicleChargesJSON(_availabilityItem.EstimatedTotal, _availabilityItem.EstimatedTotal + ((_bonusPackAvailability != null && _bonusPackAvailability.GetBonusPackCharge() != null) ? _bonusPackAvailability.GetBonusPackCharge().ProductPrice : 0), _hasBonusPack && (_bonusPackAvailability != null));
        }
    }

    public string CurrencyStr
    {
        get
        {
            return CurrencyManager.GetCurrencyForCountryCode(CurrentRequest.CountryCode).ToString();
        }
    }

    public int OneWayFee
    {
        get
        {
            return 0;//TODO:
        }
    }

    /// <summary>
    /// Availability Item JSON Representation
    /// </summary>
    public string AvailabilityJSON
    {
        get
        {
            string availabilityJSON;
            if (InclusiveMode)//render configure in IsInclusive mode
                availabilityJSON = new AvailabilityResponseDisplayer().RenderAvailabilityRowJSON(_availabilityItem, _bonusPackAvailability.GetBonusPackCharge().ProductPrice);
            else
                availabilityJSON = new AvailabilityResponseDisplayer().RenderAvailabilityRowJSON(_availabilityItem, 0);
            return availabilityJSON;
        }
    }

    public bool InclusiveMode
    {
        get
        {
            return (_hasBonusPack && (_bonusPackAvailability != null));
        }
    }

    /// <summary>
    /// User Entered this page in Inclusive mode (from URL)
    /// </summary>
    public bool SelectedInclusive
    {
        get { return _availabilityItem.IsInclusive; }
    }

    public string AllInclusiveJSON
    {
        get;
        set;
    }


    public string LocationsPanel
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            ApplicationData appData = ApplicationManager.GetApplicationData();
            sb.Append(@"
            <div class=""Locations"">
                <span><b>Pick up:</b> " + appData.GetLocationStringForCtrlCode(CurrentRequest.PickUpBranch) + ", " + _availabilityItem.PickUpDate.ToString("dddd, dd MMMM yyyy HH:mm tt") + @"</span> 
                <span><b>Drop off:</b> " + appData.GetLocationStringForCtrlCode(CurrentRequest.DropOffBranch) + ", " + _availabilityItem.DropOffDate.ToString("dddd, dd MMMM yyyy HH:mm tt") + @"</span>
            </div>
            ");
            return sb.ToString();
        }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Submit(string confParams, string avp, string bookingType, string selectedSlot, string slotId)
    {

        try
        {

            SessionManager.GetSessionData().AddExtraHires(confParams);
            string response = string.Empty;
            B2BSessionCache sessionCache = SessionManager.GetSessionData().B2BCache;
            AvailabilityItem availabilityItem = sessionCache.GetAvailabilityForAVP(avp);
            if (availabilityItem == null) availabilityItem = sessionCache.GetIncItem(avp);//try in Inclusives, TODO: formal interface
            if (!B2BAvailabilityHelper.SessionExpired)
            {
                try
                {
                    SessionData sessionData = SessionManager.GetSessionData();
                    //TODO: remove this to Save step
                    string phoneStr = "000";//TODO: next step telephoneTxt1 + telephoneTxt2 + telephoneTxt3;
                    bool phoneNumberValid = PaymentHelper.ValidatePhoneNumber(ref phoneStr);
                    Customer customerData = new Customer(Customer.GetTitleTypeForString("customerTitleDD"), "customerFirstName", "customerLastName", "reuben.hais@thlonline.com", phoneStr);//Will be updated next step
                    ExtraHireItem[] ehiItems = AvailabilityHelper.GetItemsForParamsStr(confParams);
                    phoneStr = new Random().Next(213213, 943423423).ToString();
                    BookingStatus bookingstatus = AuroraBooking.GetBookingTypeForString(bookingType);
                    string quoteId = string.Empty, status = string.Empty, msg = string.Empty;

                    
                    //rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
                    string[] custInfoParams = confParams.Split(':');
                    string emailaddressConfirmation = "";
                    string b2bagentemailresult = "";
                    try
                    {
                        if(custInfoParams[9] != null)  emailaddressConfirmation = custInfoParams[9];
                        if (custInfoParams[10] != null) b2bagentemailresult = custInfoParams[10];
                    }
                    catch (Exception)
                    { }

                    if (!(bookingstatus == BookingStatus.Created))
                    {
                        //rev:mia April 16 2012
                        try
                        {
                            if (sessionData.AvailabilityRequestData.CountryOfResidence != null)
                                customerData.CountryOfresidenceCode = sessionData.AvailabilityRequestData.CountryOfResidence;

                        }
                        catch (Exception)
                        { }
                    
                        //first timer
                        AuroraBooking booking = new AuroraBooking(customerData, ehiItems);
                        booking.Status = bookingstatus;
                        booking.AVPID = avp;
                        booking.CheckOutTime = availabilityItem.PickUpDate;
                        booking.CheckInTime = availabilityItem.DropOffDate;
                        booking.UserCode = sessionData.CurrentAgent;
                        booking.AgentReference = string.Empty;

                        try
                        {
                            //rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
                            if(!string.IsNullOrEmpty(emailaddressConfirmation))
                                 booking.EmailAddressConfirmation = emailaddressConfirmation;

                        }
                        catch (Exception)
                        { }
                        

                        sessionData.UpdateBookingData(booking);
                        status = "success";
                    }
                    else
                    {
                        AuroraBooking booking = SessionManager.GetSessionData().BookingData;
                        THL.Booking.PaymentManager pm = new PaymentManager();
                        //string[] custInfoParams = confParams.Split(':');
                        booking.CustomerInfo = new Customer(Customer.GetTitleTypeForString(custInfoParams[1]), custInfoParams[2], custInfoParams[3], custInfoParams[4], custInfoParams[5]);
                        
                        //rev:mia April 16 2012
                        try
                        {
                            if (sessionData.AvailabilityRequestData.CountryOfResidence != null)
                                booking.CustomerInfo.CountryOfresidenceCode = sessionData.AvailabilityRequestData.CountryOfResidence;

                        }
                        catch (Exception)
                        { }

                        try
                        {
                            //rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
                            if (!string.IsNullOrEmpty(emailaddressConfirmation))
                                booking.EmailAddressConfirmation = emailaddressConfirmation;

                        }
                        catch (Exception)
                        { }


                        booking.BookingNote = custInfoParams[6];
                        booking.AgentReference = custInfoParams[8];
                        // Added by Nimesh on 13th Feb 2015 for slot Data
                        booking.SlotId = slotId;
                        booking.SelectedSlot = selectedSlot;

                        // End Added by Nimesh on 13th Feb 2015 for slot Data
                        if (custInfoParams[0].ToLower().Equals("t"))
                        {
                            booking.ExistingBookingNumber = custInfoParams[7];
                            booking.AgentReference = custInfoParams[8];
                        }
                        else
                            booking.ExistingBookingNumber = string.Empty;

                        string auroraResponse = pm.CreateQuote(ref booking, 0);
                        if (auroraResponse.IndexOf("uoteId") > 0)
                            quoteId = auroraResponse.Split(':')[1];
                        if (quoteId.Equals("0"))
                        {
                            status = "failed";
                            msg = SessionManager.GetSessionData().LastError.Replace("\"", "").Replace("'", "").Replace("\n", "");
                        }
                        else
                        {
                            status = "success";

                            try
                            {
                                //rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
                                if (!string.IsNullOrEmpty(b2bagentemailresult)  )
                                {
                                    if (b2bagentemailresult.Contains("ERROR") == false && sessionData.BookingData.Status.ToString() != "Quote")
                                        pm.B2BAutoSendEmail(quoteId, booking.EmailAddressConfirmation);
                                }
                            }
                            catch (Exception ex)
                            { }
                            
                        }

                    }
                    return "[{'status':'" + status + "', 'quoteId':'" + quoteId + "','avp':'" + avp + "', 'msg':'" + msg + "'}]";
                }
                catch (Exception ex)
                {
                    THLDebug.LogError(ErrorTypes.Application, "CreateBooking.MakePayment", "{error:" + ex.Message + "}", "paymentFailed");
                    return "[{'error':'failed to create quote, session expired', 'code':'11'}]";
                }
            }
            else
                HttpContext.Current.Response.StatusCode = 404;
            return response;
        }
        catch (Exception ex)
        {
            string msg = ex.Message;
            THLDebug.LogError(ErrorTypes.Application, "Configure.Submit", msg, "{confParams:'" + confParams + "', avp:'" + avp + "', bookingType:'" + bookingType + "'}");
            return "[{'msg':'failed to process your request, session expired','status':'failed', 'code':'12'}]";
        }
    }

    public string ConfigureDisclaimer;


    //rev:mia April 1 2014 Payable to supplier messages
    public string GetMessage(string message)
    {
        string vt = (CurrentRequest as B2BAvailabilityRequest).b2bParams.vt.ToString();
        return THLAuroraWebInterface.Config.GetPayablePickupMessage(message, vt);
    }


}
