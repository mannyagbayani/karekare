<%@ Register TagPrefix="uc1" TagName="UserDetails" Src="../Controls/UserDetails/UserDetails.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FooterImage" Src="../Controls/Graphics/FooterImage.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MaintainAgentUsers.aspx.vb" Inherits="THLAuroraWebInterface.MaintainAgentUsers"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MaintainAgentUsers</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="../Styles.css">
	</HEAD>
	<body background='<%=BackgroundURL()%>'>
		<form id="Form1" method="post" runat="server">
			<table id="Table3" borderColor="#999999" cellSpacing="0" cellPadding="0" width="746" border="0">
				<tr>
					<td>
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="750" border="0">
							<TR>
								<TD vAlign="top">
									<uc1:Header id="Header1" runat="server"></uc1:Header></TD>
							</TR>
							<TR>
								<TD width="100%" vAlign="top">
									<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="750" border="0" class="normal">
										<TR>
											<TD class="CellHeading2" width="115">Select user to modify</TD>
											<TD class="CellHeading2" width="176">
												<asp:DropDownList id="ddlUser" runat="server" CssClass="normalddl" AutoPostBack="True" Width="176px"></asp:DropDownList></TD>
											<TD class="CellHeading2">
												<asp:Button id="btnModify" runat="server" Text="Modify" CausesValidation="False"></asp:Button>&nbsp;
												<asp:Button id="btnDelete" runat="server" Text="Delete" CausesValidation="False"></asp:Button>&nbsp;
												<asp:Button id="btnNew" runat="server" Text="Create" CausesValidation="False"></asp:Button></TD>
										</TR>
										<TR>
											<TD></TD>
											<TD>
												<asp:Label id="lblMessage" runat="server"></asp:Label></TD>
											<TD></TD>
										</TR>
									</TABLE>
									<uc1:UserDetails id="UserDetails1" runat="server" Visible="False"></uc1:UserDetails>
								</TD>
							</TR>
							<TR>
								<TD width="100%" vAlign="top">
									<asp:Label id="lblLoadError" runat="server" CssClass="warning"></asp:Label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</table>
			<uc1:FooterImage id="FooterImage1" runat="server"></uc1:FooterImage>
		</form>
	</body>
</HTML>
