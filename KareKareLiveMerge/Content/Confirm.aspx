﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Confirm.aspx.cs" Inherits="Content_Confirm" %>
<%@ Register TagPrefix="uc1" TagName="StandardConfirmation" Src="../Controls/Booking/StandardConfirmation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Booking Confirmation</title>
    <link rel="stylesheet" type="text/css" href="../Styles.css" />
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35407162-1']);
        _gaq.push(['_setDomainName', 'thlrentalsb2b.com']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <style type="text/css">
        .normal {
            font-weight: normal;
            font-size: 12px;
            color: #333;
            line-height: normal;
            font-style: normal;
            font-family: Arial,Helvetica,sans-serif;
            text-decoration: none;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
   
    <div>
        <uc1:Header ID="Header1" runat="server"></uc1:Header>
        <uc1:StandardConfirmation ID="StandardConfirmation1" runat="server"></uc1:StandardConfirmation>
    </div>

    <!--rev:mia 11March2015 B2B Auto generated Aurora Booking Confirmation-->
    <table width="746" id="confirmationtable" runat="server">
        <tr align="right">
            <td>
                    <label class="normal">
                    Confirmation to be send to:* &nbsp;
                    </label>
                    <input class="email" name="emailConfirmationSentToBox" id="emailConfirmationSentToBox" runat="server" style="width:200px;" />
                    <asp:button id="ButtonSendEmail" runat="server" text="Send to" width="70px" 
                    onclick="ButtonSendEmail_Click" ></asp:button>
                     
                    <asp:RequiredFieldValidator ID="emailRequiredValidator" runat="server" 
                        ControlToValidate="emailConfirmationSentToBox" Display="Dynamic" 
                        ErrorMessage="Email is required" EnableClientScript="False" Enabled="true"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="emailExpressionValidator" runat="server" 
                        ControlToValidate="emailConfirmationSentToBox" Display="Dynamic" 
                        ErrorMessage="RegularExpressionValidator" 
                        ValidationExpression="(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*;\s*|\s*$))*" 
                            EnableClientScript="False" Enabled="true">Email is in invalid format.</asp:RegularExpressionValidator>
                     
            </td>
        </tr>
        <tr align="right">
            <td>
                        <asp:Label runat="server" ID="confirmationmessage"></asp:Label>
            </td>
        </tr>
    </table>
     
    </form>
</body>
</html>
