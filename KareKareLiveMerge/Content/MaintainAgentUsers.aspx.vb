'--------------------------------------------------------------------------------------
'   Class:          MaintainAgentUsers
'   Description:    Allows super user to maintain other agent user details
'--------------------------------------------------------------------------------------

Partial Class MaintainAgentUsers
    'Inherits System.Web.UI.Page
    Inherits THL_PageBase
    Protected WithEvents Userdetails1 As UserDetails
    Protected Proxy As New AuroraOTAProxy.Aurora_OTA



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        ' General.MaintainSessionObjects(General.PageName.MaintainAgentUsers, Session)

        If IsNothing(Session("POS")) Then
            'redirect to redirect page
            Response.Redirect("AccessDenied.aspx")
        End If
    End Sub

#End Region
    Public Property BackgroundURL() As String
        Get
            Return CStr(viewstate("BackgroundURL"))
        End Get
        Set(ByVal Value As String)
            viewstate("BackgroundURL") = Value
        End Set
    End Property


    Private Property UserData() As String
        Get
            Return CStr(Session("UserData"))
        End Get
        Set(ByVal Value As String)
            Session("UserData") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to Initialize the page here
        lblMessage.Text = ""
        lblLoadError.Text = ""
        If Not Page.IsPostBack Then
            Me.BackgroundURL = General.BackGroundImageURL(Session)

            Try
                If GetUserData() = True Then
                    PopulateUserDropDownList()
                    CheckDelete()
                    ' If there are users to modify initialise user details control
                    If Not IsNothing(ddlUser.SelectedItem) Then
                        Userdetails1.Visible = True
                        Userdetails1.InitModify(ddlUser.SelectedItem.Value)
                    Else
                        Userdetails1.Visible = True
                    End If
                End If

            Catch ex As Exception
                lblLoadError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
            End Try

        End If
    End Sub

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click
        ' Prepare to modify a user
        Userdetails1.Visible = True
        Userdetails1.InitModify(ddlUser.SelectedItem.Value)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If ddlUser.Items.Count = 0 Then
            lblMessage.Text = "No users to delete"
        Else
            If IsNothing(ddlUser.SelectedItem) Then
                lblMessage.Text = "Please select a User to delete"
            Else
                'do delete via web service
                Userdetails1.Visible = False
                If Userdetails1.InitDelete(ddlUser.SelectedItem.Value) = True Then
                    CheckDelete()
                Else
                    Userdetails1.Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ' Set up user details control fo adding a new user
        Userdetails1.Visible = True
        Userdetails1.InitNew()
    End Sub

    Private Sub CheckDelete()
        If (ddlUser.Items.Count > 0) Then
            btnDelete.Attributes("onclick") = "javascript:return " & "confirm('Are you sure you want to delete?');"
        Else
            btnDelete.Attributes("onclick") = "javascript:return(true);"
        End If
    End Sub


    Private Function GetUserData() As Boolean
        Dim success As Boolean
        Dim RQ As New THL_RetUsersRQ
        Dim RS As New THL_RetUsersRS

        RQ.POS = New SourceType() {CType(Session("POS"), SourceType)} 'only pos required in request

        Try
            RS = Proxy.RetUsers(RQ) 'all user data retrieved based on user passed in pos

        Catch ex As Exception
            lblLoadError.Text &= "An error occurred. Please quote the following code when reporting this error: " & ExceptionManager.Publish(ex)
        End Try


        If IsNothing(RS.Errors) Then
            Me.UserData = General.SerializeRetUsersRSObject(RS)
            success = True
        Else
            'errors occurred
            Dim errorIndex As Integer
            For errorIndex = 0 To RS.Errors.Length - 1
                lblLoadError.Text &= RS.Errors(errorIndex).Tag & "<br>"
            Next
            success = False
        End If
        Return success
    End Function

    Private Sub PopulateUserDropDownList()
        ddlUser.Items.Clear()
        Dim RS As THL_RetUsersRS
        RS = CType(General.DeserializeRetUsersRSObject(Me.UserData, RS), THL_RetUsersRS)

        Dim i As Integer = 0
        Dim newListItem As ListItem
        For i = 0 To RS.Users.Length - 1
            newListItem = New ListItem
            newListItem.Text = RS.Users(i).FirstName & " " & RS.Users(i).LastName
            newListItem.Value = RS.Users(i).UserId
            ddlUser.Items.Add(newListItem)
        Next
    End Sub

    Private Sub ddlUser_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlUser.SelectedIndexChanged
        Userdetails1.Visible = True
        Userdetails1.InitModify(ddlUser.SelectedItem.Value)
    End Sub

    Private Sub Userdetails1_CancelClicked() Handles Userdetails1.CancelClicked
        Userdetails1.Visible = False
    End Sub

    Private Sub Userdetails1_SaveClicked1(ByVal userId As String) Handles Userdetails1.SaveClicked
        Userdetails1.Visible = False
        ' Get latest data for user dropdownlist
        GetUserData()
        PopulateUserDropDownList()
        ddlUser.ClearSelection()

        Try
            ddlUser.Items.FindByValue(userId).Selected = True
        Catch ex As Exception
            ddlUser.Items(0).Selected = True
        End Try
    End Sub

    Private Sub Userdetails1_DeletePerformed() Handles Userdetails1.DeletePerformed
        Userdetails1.Visible = False
        ' Get latest data for user dropdownlist
        GetUserData()
        PopulateUserDropDownList()
    End Sub
End Class



