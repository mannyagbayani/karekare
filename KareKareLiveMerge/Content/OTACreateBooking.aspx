<%@ Page Language="vb" AutoEventWireup="false" Codebehind="OTACreateBooking.aspx.vb" Inherits="THLAuroraWebInterface.OTACreateBooking" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Booking</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P><asp:label id="lblErrors" runat="server" ForeColor="Red" Font-Bold="True"></asp:label>
				<TABLE id="Table1" style="WIDTH: 656px; HEIGHT: 145px" cellSpacing="1" cellPadding="1"
					width="656" border="0">
					<TR>
						<TD align="center" colSpan="4">---- PERSONEL INFORMATION ----</TD>
					</TR>
					<TR>
						<TD>Title:</TD>
						<TD><asp:dropdownlist id="drpTitle" runat="server">
								<asp:ListItem Value="Mr.">Mr.</asp:ListItem>
								<asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
								<asp:ListItem Value="Miss">Miss.</asp:ListItem>
							</asp:dropdownlist></TD>
						<td>Last Name:</td>
						<td><asp:textbox id="txtLastName" Runat="server">Shukla</asp:textbox></td>
					</TR>
					<tr>
						<TD>First Name:</TD>
						<TD><asp:textbox id="txtFirstName" Runat="server">Rajesh</asp:textbox></TD>
						<TD>Middle Name:</TD>
						<TD><asp:textbox id="txtMiddleName" Runat="server">Kumar</asp:textbox></TD>
					</tr>
					<tr>
						<td>Phone Number:</td>
						<td><asp:textbox id="txtPhoneNumber" Runat="server">6241231</asp:textbox></td>
						<td>Email Address:</td>
						<td><asp:textbox id="txtEmail" Runat="server">shuklark@hotmail.com</asp:textbox></td>
					</tr>
					<tr>
						<td>Country of Citizen:</td>
						<td colSpan="3"><asp:textbox id="txtCountry" Runat="server">NZ</asp:textbox></td>
					</tr>
					<tr>
						<td align="center" colSpan="4">
							<hr>
						</td>
					</tr>
					<TR>
						<td align="center" colSpan="4">---- VEHICLE INFORMATION ----</td>
					</TR>
					<TR>
						<TD>Brand:</TD>
						<TD colSpan="3"><asp:dropdownlist id="drpBrand" runat="server" Width="154px">
								<asp:ListItem Value="Maui">Maui</asp:ListItem>
								<asp:ListItem Value="BRITZ">Britz</asp:ListItem>
								<asp:ListItem Value="BACKPACKER">BackPacker</asp:ListItem>
								<asp:ListItem Value="EXPLOREMORE">Explore More</asp:ListItem>
							</asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD>Booking number:</TD>
						<TD colSpan="3"><asp:textbox id="txtBooNum" runat="server"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<P>Check-out date:</P>
						</TD>
						<TD><asp:textbox id="txtCkoDate" runat="server">05/07/2006 08:00</asp:textbox></TD>
						<TD>Check_Out Location:</TD>
						<TD><asp:textbox id="txtCkoLoc" runat="server" Width="48px">AKL</asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<P>Check-in date:</P>
						</TD>
						<TD><asp:textbox id="txtckiDate" runat="server">05/08/2006 08:00</asp:textbox></TD>
						<TD>Check_In Location:</TD>
						<TD><asp:textbox id="txtCkiLoc" runat="server" Width="48px">CHC</asp:textbox></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 18px">Vehicle code:</TD>
						<TD style="HEIGHT: 18px" colSpan="3"><asp:dropdownlist id="drpVehicle" runat="server" Width="154px">
								<asp:ListItem Value="2BM">2BM</asp:ListItem>
								<asp:ListItem Value="2BTSM">2BTSM</asp:ListItem>
								<asp:ListItem Value="4BM">4BM</asp:ListItem>
								<asp:ListItem Value="6MB">6MB</asp:ListItem>
								<asp:ListItem Value="2BB">2BB</asp:ListItem>
								<asp:ListItem Value="2BTSB">2BTSB</asp:ListItem>
								<asp:ListItem Value="4BB">4BB</asp:ListItem>
								<asp:ListItem Value="6BB">6BB</asp:ListItem>
								<asp:ListItem Value="2BHMN">2BHMN</asp:ListItem>
								<asp:ListItem Value="4BMR">4BMR</asp:ListItem>
								<asp:ListItem Value="2BGX">2BGX</asp:ListItem>
							</asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD>Promotional code:</TD>
						<TD colSpan="3"><asp:textbox id="txtPkgCode" runat="server">STDM</asp:textbox></TD>
					</TR>
					<tr>
						<td align="center" colSpan="4">
							<hr>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="4">---- INSURENCE INFORMATION ----</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:radiobutton id="StdInsurence" Runat="server" GroupName="Insurence" Checked="True"></asp:radiobutton>Standard 
							Excess (Bond Applies)</td>
						<td>Included in Rate</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:radiobutton id="ER1" Runat="server" GroupName="Insurence"></asp:radiobutton>Excess 
							Reduction 1
						</td>
						<td>20.00 per day</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:radiobutton id="ER2" Runat="server" GroupName="Insurence"></asp:radiobutton>Excess 
							Reduction 2
						</td>
						<td>38.00 per day</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:radiobutton id="PrePkg" Runat="server" GroupName="Insurence"></asp:radiobutton>Maui 
							Premium Package</td>
						<td>50.00 per day</td>
					</tr>
					<tr>
						<td align="center" colSpan="4">
							<hr>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="4">---- ADDITIONAL PRODUCTS AND SERVICES ----</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:dropdownlist id="drpBabyCapsuleQty" Runat="server">
								<asp:ListItem Value="0">0</asp:ListItem>
								<asp:ListItem Value="1">1</asp:ListItem>
								<asp:ListItem Value="2">2</asp:ListItem>
							</asp:dropdownlist>Baby Capsule (recommended infants 0-6 months)</td>
						<td>25.00 each</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:dropdownlist id="drpBabySeatQty" Runat="server">
								<asp:ListItem Value="0">0</asp:ListItem>
								<asp:ListItem Value="1">1</asp:ListItem>
								<asp:ListItem Value="2">2</asp:ListItem>
							</asp:dropdownlist>Baby Seat (recommended children 0-2 years)
						</td>
						<td>25.00 each</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:dropdownlist id="drpBoostSeatQty" Runat="server">
								<asp:ListItem Value="0">0</asp:ListItem>
								<asp:ListItem Value="1">1</asp:ListItem>
								<asp:ListItem Value="2">2</asp:ListItem>
							</asp:dropdownlist>Booster Seat (recommended children 3-5 years)
						</td>
						<td>25.00 each</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:dropdownlist id="drpTableQty" Runat="server">
								<asp:ListItem Value="0">0</asp:ListItem>
								<asp:ListItem Value="1">1</asp:ListItem>
								<asp:ListItem Value="2">2</asp:ListItem>
							</asp:dropdownlist>Camping Table(s)</td>
						<td>22.00 each</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:dropdownlist id="drpChairQty" Runat="server">
								<asp:ListItem Value="0">0</asp:ListItem>
								<asp:ListItem Value="1">1</asp:ListItem>
								<asp:ListItem Value="2">2</asp:ListItem>
							</asp:dropdownlist>Camping Chair(s)</td>
						<td>12.00 each</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:dropdownlist id="drpCamPackQty" Runat="server">
								<asp:ListItem Value="0">0</asp:ListItem>
								<asp:ListItem Value="1">1</asp:ListItem>
								<asp:ListItem Value="2">2</asp:ListItem>
							</asp:dropdownlist>Camping Pack(2 Person)</td>
						<td>20.00 per day</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:dropdownlist id="drpTentQty" Runat="server">
								<asp:ListItem Value="0">0</asp:ListItem>
								<asp:ListItem Value="1">1</asp:ListItem>
								<asp:ListItem Value="2">2</asp:ListItem>
							</asp:dropdownlist>Tent (4 Person)</td>
						<td>75.00 each</td>
					</tr>
					<tr>
						<td align="center" colSpan="4">
							<hr>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="4">---- CARD INFORMATION ----</td>
					</tr>
					<TR>
						<TD>Card type:</TD>
						<TD><asp:dropdownlist id="txtCardType" runat="server" Width="154px">
								<asp:ListItem Value="2">VISA</asp:ListItem>
								<asp:ListItem Value="7">MASTERCARD</asp:ListItem>
								<asp:ListItem Value="0">AMERICAN EXPRESS</asp:ListItem>
							</asp:dropdownlist></TD>
						<TD>Amount:</TD>
						<TD><asp:textbox id="txtPmtAmt" Runat="server" Width="64px">300.00</asp:textbox></TD>
					</TR>
					<TR>
						<TD>Card number:</TD>
						<TD colSpan="3"><asp:textbox id="txtCardNumber" runat="server" Width="160px">4111111111111111</asp:textbox></TD>
					</TR>
					<TR>
						<TD>Expiry date (mm/yy):</TD>
						<TD colSpan="3"><asp:textbox id="txtExpiryMn" runat="server" Width="32px">01</asp:textbox>&nbsp;/
							<asp:textbox id="txtExpiryYr" runat="server" Width="32px">07</asp:textbox></TD>
					</TR>
					<TR>
						<TD>Card holder name:</TD>
						<TD colSpan="3"><asp:textbox id="txtCardHolderName" runat="server" Width="208px">Rajesh Shukla</asp:textbox></TD>
					</TR>
					<TR>
						<TD>Note:</TD>
						<TD colSpan="3" height="100"><asp:textbox id="txtNoteText" runat="server" Width="336px" TextMode="MultiLine" Height="100px"></asp:textbox></TD>
					</TR>
					<tr>
						<td colSpan="2"><asp:button id="txtCreateBooking" Runat="server" Text="Create Booking"></asp:button><asp:button id="Button2" runat="server" Width="200px" Text="Availability Only"></asp:button></td>
					</tr>
				</TABLE>
			</P>
		</form>
	</body>
</HTML>
