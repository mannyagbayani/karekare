<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Error.aspx.vb" Inherits="THLAuroraWebInterface._Error"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Error</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td valign="top" align="center">
						<br>
						<table width="500" border="0" cellpadding="10">
							<tr>
								<td class="ErrorHeading"><STRONG>An error has occured</STRONG></td>
							</tr>
							<tr>
								<td>
									<br>
									<asp:Label id="lblErrorMessage" runat="server" ForeColor="Red">error message</asp:Label>
									<br>
									<br>
									<br>
									Please quote the following code when reporting this error:&nbsp;
									<br>
									<br>
									<asp:Label id="lblErrorNumber" runat="server" ForeColor="Red"></asp:Label>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
