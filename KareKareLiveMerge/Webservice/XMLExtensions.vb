﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Xml
Imports System.Xml.Linq
Imports System.Runtime.CompilerServices

''' <summary>
''' Summary description for XMLExtensions
''' </summary>

Module XMLExtensions

    <Extension()>
    Public Function GetXElement(node As XmlNode) As XElement
        Dim xDoc As New XDocument()
        Using xmlWriter As XmlWriter = xDoc.CreateWriter()
            node.WriteTo(xmlWriter)
        End Using
        Return xDoc.Root
    End Function

    <Extension()>
    Public Function GetXmlNode(element As XElement) As XmlNode
        Using xmlReader As XmlReader = element.CreateReader()
            Dim xmlDoc As New XmlDocument()
            xmlDoc.Load(xmlReader)
            Return xmlDoc
        End Using
    End Function
End Module
