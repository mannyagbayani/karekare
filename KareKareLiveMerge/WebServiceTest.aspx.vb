Imports System.Xml.Serialization
Imports System.IO
'Imports tst.localhost
'Imports THLRentals.OTA.Utils
Partial Class WebForm1
	Inherits System.Web.UI.Page
	Protected Proxy As New AuroraOTAProxy.Aurora_OTA

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	'NOTE: The following placeholder declaration is required by the Web Form Designer.
	'Do not delete or move it.
	Private designerPlaceholderDeclaration As System.Object

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		'Put user code to Initialize the page here
	End Sub

	Private Sub btnPing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPing.Click
		Dim RQ As New OTA_PingRQ
		Dim RS As New OTA_PingRS


		RQ.EchoData = "Aurora_OTA_Ping"							'M 
		RQ.EchoToken = System.Guid.NewGuid().ToString()			'O
		RQ.SequenceNmbr = CStr(1)								 'O
		RQ.Target = OTA_VehAvailRateRQTarget.Test				 'M
		RQ.TimeStamp = Date.Now									'M
		RQ.Version = CType(1.0, Decimal)						'M
		RS = Proxy.Ping(RQ)
		lblPing.Text = ("Ping took " & RS.TimeStamp.Subtract(RQ.TimeStamp).TotalMilliseconds & " msecs")
	End Sub

	Private Sub btnVehicle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVehicle.Click
		' Outline the required contents and usage for the VehAvailRateRQ message
		Dim RQ As New OTA_VehAvailRateRQ

		' Set request header data
		RQ.Version = CType(1.0, Decimal)						 'M
		RQ.TimeStamp = Date.Now																	 'M
		RQ.Target = OTA_VehAvailRateRQTarget.Test								 'M
		RQ.EchoToken = "ECHOTHIS"																 'O
		RQ.MaxResponses = CStr(3)											'O

		' Set requestor details
		RQ.POS = New SourceType() {New SourceType}
		RQ.POS(0).ERSP_UserID = "Security Token"									'M Generic TNZ Token | Agent user token | Customer token
		RQ.POS(0).RequestorID = New UniqueID_Type
		RQ.POS(0).RequestorID.ID = "WEBMAUI"											'M The TNZ web agent code for Direct bookings | The selected agent code for Agent bookings
		RQ.POS(0).RequestorID.Type = CStr(22)								'M 22=ERSP
		RQ.POS(0).AgentSine = "Agent user code"									 'O Required for Agent bookings. Will be validated to match the token above

		' Set rental locations/dates
		RQ.VehAvailRQCore = New VehicleAvailRQCoreType
		Dim vrc As New VehicleRentalCoreType
		vrc.PickUpDateTime = New Date(2003, 10, 5, 8, 0, 0)							 'M
		vrc.ReturnDateTime = New Date(2003, 10, 18, 17, 0, 0)						 'M
		vrc.PickUpLocation = New LocationType("DRW", "Darwin")						'M
		vrc.ReturnLocation = New LocationType("ASP", "Alice Springs")		 'M
		RQ.VehAvailRQCore.VehRentalCore = vrc

		' Set vehicle request details
		RQ.VehAvailRQCore.VehPrefs = New VehiclePrefType() {New VehiclePrefType}
		RQ.VehAvailRQCore.Status = InventoryStatusType.Available																				'M Only "Available" may be specified
		RQ.VehAvailRQCore.VehPrefs(0).VehType = New VehicleTypeType(VehicleCategoryEnum.Motorhome4WD)	 'M
        RQ.VehAvailRQCore.VehPrefs(0).VehClass = New VehicleClassType(VehicleSizeEnum.TwoBerth)     'M
		RQ.VehAvailRQCore.VehPrefs(0).TypePref = PreferLevelType.Only																	 'O Defaults to Preferred
		RQ.VehAvailRQCore.VehPrefs(0).ClassPref = PreferLevelType.Only																	'O Defaults to Preferred

		' The following two lines show how a promotion code should be passed in the request
		'RQ.VehAvailRQCore.RateQualifier = New RateQualifierType()
		'RQ.VehAvailRQCore.RateQualifier.PromotionCode = "SKIWI"         'O

		Dim RS As OTA_VehAvailRateRS
		RS = Proxy.VehAvailRate(RQ)
		lblVehicle.Text = (RS.VehAvailRSCore.VehVendorAvails(0).VehAvails(0).VehAvailCore.Vehicle.VehMakeModel.Name)

		'	MsgBox(RS.VehAvailRSCore.VehVendorAvails(0).VehAvails(0).VehAvailCore.Vehicle.PictureURL)
	End Sub
End Class
