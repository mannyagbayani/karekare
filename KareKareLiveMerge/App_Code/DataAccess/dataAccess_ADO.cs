﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Common;
namespace THL.Booking {


/// <summary>
/// Summary description for dataAccess_ADO
/// </summary>
public partial class DataAccess
{
	protected string insertedRowIdentity; //Current Inserted Key

        /// <summary>
        /// Connects to a SQL BackUp database, TODO: implement 
        /// </summary>
        /// <param name="bookingInfo">Current Booking Information Instance</param>
        /// <returns>Identity Generated From BackUp DB</returns>
        public string SubmitToDRDB(AuroraBooking bookingInfo)
        {
            //TODO: ADO.NET Call with INSERT on a DB at the same Session DB cluster.
            //BEGIN TRAN
            //DECLARE @sRetBooNum VARCHAR(64)
            //Exec [dbo].[WEBP_CreateFailedInfo]
            //@sRequestXML      VARCHAR(MAX),
            //@sErrorMsg        VARCHAR(MAX),     
            //@sAddPrgmName     VARCHAR(128),
            //@sTransStatus     VARCHAR(128),     -- Added RKS:16SEP09 to handle DPS timeout issues //TODO: add support for failed force booking (diff types) DPSFAIL for failed trans
            //@sRetBooNum       VARCHAR(64) OUTPUT
            //  SELECT @sRetBooNum
            //COMMIT TRAN                        
            //-----------Insert/Update Starts
            try
            {
                string failSafeConStr = ConfigurationManager.ConnectionStrings["failsafedbB"].ConnectionString;                
                insertedRowIdentity = "0";
                SqlDataSource mySource = new SqlDataSource();
                mySource.ConnectionString = failSafeConStr;
                mySource.InsertCommand = "WEBP_CreateFailedInfo";
                mySource.InsertCommandType = SqlDataSourceCommandType.StoredProcedure;
                
                mySource.InsertParameters.Add("sRequestXML", bookingInfo.ToXML().OuterXml);
                mySource.InsertParameters.Add("sErrorMsg","connection");
                mySource.InsertParameters.Add("sAddPrgmName", "WebFrontEnd");
                mySource.InsertParameters.Add("sTransStatus", null );                
                
                mySource.Inserted += new SqlDataSourceStatusEventHandler(mySource_Inserted);
                mySource.Inserting += new SqlDataSourceCommandEventHandler(On_Inserting);

                THLDebug.LogEvent(EventTypes.DBRequest, "Failsafe call on: sRequestXML:" + bookingInfo.ToXML().OuterXml);
                
                try
                {
                    mySource.Insert();
                }
                catch (SqlException ex)
                {
                    string errorMsg = ex.Message;
                    THLDebug.LogError(ErrorTypes.Database, "DAL:SubmitFalsafe", "{err:" + errorMsg + "}", "{err:" + errorMsg + "}");
                    return "0";
                }                
                return insertedRowIdentity;//effected rows should be 1                
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                //log here
                return "0";
            }            
        }

        private void On_Inserting(Object sender, SqlDataSourceCommandEventArgs e)
        {
            SqlParameter insertedKey = new SqlParameter("@sRetBooNum", SqlDbType.VarChar, 64);
            insertedKey.Direction = ParameterDirection.Output;
            e.Command.Parameters.Add(insertedKey);
        }

        void mySource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            try
            {
                DbCommand command = e.Command;
                insertedRowIdentity = command.Parameters["@sRetBooNum"].Value.ToString();
            }
            catch (SqlException ex)
            {
                string errorMsg = ex.Message;
                THLDebug.LogError(ErrorTypes.Database, "DataAccess.Source_Inserted", ex.Message, string.Empty);
                insertedRowIdentity = "0";
            }
        }
    }
}
