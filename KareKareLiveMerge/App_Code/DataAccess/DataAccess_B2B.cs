﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Configuration;

namespace THL.Booking
{

    /// <summary>
    /// Data Access Layer Functionality for B2B Web Service calls
    /// </summary>
    public partial class DataAccess
    {
        
        /// <summary>
        /// Get Agent Login Data based on the logged user SourceType 
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        public AgentLoginData GetAgentData(SourceType st)
        {
            string otaBase = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraOTABase"];
            THL.Booking.DAL.Aurora_OTA wsProxy = new THL.Booking.DAL.Aurora_OTA(otaBase);
            THL.Booking.DAL.THL_RetUsersRQ rs = new THL.Booking.DAL.THL_RetUsersRQ();
            THL.Booking.DAL.THL_RetUsersRQ rur = new THL.Booking.DAL.THL_RetUsersRQ();
            rur.POS = new THL.Booking.DAL.SourceType[1];
            rur.POS[0] = new THL.Booking.DAL.SourceType();
            rur.POS[0].AgentSine = st.AgentSine;
            rur.POS[0].AgentDutyCode = st.AgentDutyCode;
            rur.POS[0].ERSP_UserID = st.ERSP_UserID;
            rur.POS[0].RequestorID = new THL.Booking.DAL.UniqueID_Type();
            rur.POS[0].RequestorID.ID = st.RequestorID.ID;
            rur.POS[0].RequestorID.Type = st.RequestorID.Type;
            THL.Booking.DAL.THL_RetUsersRS urs = wsProxy.RetUsers(rur);
            
            AgentLoginData agentData = new AgentLoginData(st);
            bool hasCurrency = true;

            if (urs != null)//has response from WS
            {
                StringBuilder sb = new StringBuilder();//debug line

                //the login user should always be the first node within the userinfo
                DAL.UserInfoType currentUserInfo = urs.Users.FirstOrDefault();
                if (currentUserInfo != null)
                {
                    agentData.DefaultAgentCode = currentUserInfo.DefAgentCode;
                    DAL.CurrOptionsTypeAgent[] currOptions = currentUserInfo.CurrOptions;
                    AgentDescription currentAgentDesc;

                    // iterate through the user info node to get all related information to the 
                    // login and agent code.
                    //
                    // userinfo -> currOptions -> agent code -> exchangerates -> curr
                    //
                    foreach (string agentStr in currentUserInfo.Agents)
                    {
                        currentAgentDesc = new AgentDescription();
                        string[] agentParams = agentStr.Split('|');
                        //agent info
                        currentAgentDesc.ID = agentParams[0];
                        currentAgentDesc.Name = agentParams[1];
    
                        //get currency options relate to this agent code
                        if (currOptions != null)
                        {
                            var currentAgent = currOptions.Where(c => c.code == currentAgentDesc.ID);
                            if (currentAgent != null)
                            {
                                //agent default currency
                                currentAgentDesc.DefaultCurrency = currentAgent.Select(c => c.@default).FirstOrDefault();
                                string currencyString = currentAgent.Select(c => c.currList).FirstOrDefault();
                                //agent currency list
                                currentAgentDesc.CurrencyList = String.IsNullOrEmpty(currencyString) ? new string[0] : currencyString.Split(',');
                                //agent currency rate, it needs to be associated with the agent code as same currency
                                //convertion can map to different agent code.
                                var agentCurrencyRates = urs.ExchangeRates.Where(e => e.Code == currentAgentDesc.ID).Select(c => c.Curr).FirstOrDefault();
                                if (agentCurrencyRates != null)
                                {
                                    var agentSelectedCurrencyRates = agentCurrencyRates.Where(c => currentAgentDesc.CurrencyList.Contains(c.ToCode));
                                    if (agentSelectedCurrencyRates != null)
                                    {
                                        List<ExchangeRate> crs = new List<ExchangeRate>();
                                        foreach (THL.Booking.DAL.ExchangeRatesTypeAgentCurr currencyRate in agentSelectedCurrencyRates)
                                        {
                                            ExchangeRate cr = new ExchangeRate();
                                            cr.FromCode = currencyRate.FromCode;
                                            cr.ToCode = currencyRate.ToCode;
                                            Decimal.TryParse(currencyRate.Rate, out cr.Rate);
                                            crs.Add(cr);
                                        }
                                        currentAgentDesc.CurrencyRates = crs;
                                    }
                                }
                                else
                                {
                                    //rev:mia 28April2015 - NO DEFAULT CURRENCY FOUND
                                    hasCurrency = false;
                                }
                            }
                        }
                        else
                        {
                            //rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT 
                            //NO DEFAULT CURRENCY FOUND
                            hasCurrency = false;

                        }
                        agentData.AddAgentDescription(currentAgentDesc);
                    }
                }

                //get packages
                DAL.THL_PriceAndAvailDataRQ prcDataRq = new DAL.THL_PriceAndAvailDataRQ();
                prcDataRq.POS = new DAL.SourceType[1];
                prcDataRq.POS[0] = rur.POS[0];
                DAL.THL_PriceAndAvailDataRS priceAndAvailRes = wsProxy.PriceAndAvailData(prcDataRq);
                List<DAL.PackageType> pkgList = new List<DAL.PackageType>();
                for (int i = 0; i < priceAndAvailRes.PriceAndAvailDataCore.PickupAndDropOffLocations.Length; i++)
                {
                    DAL.CountryType currentCountry = priceAndAvailRes.PriceAndAvailDataCore.PickupAndDropOffLocations[i];
                    for (int j = 0; j < currentCountry.Brands.Length; j++)
                    {
                        foreach (DAL.PackageType pkg in currentCountry.Brands[j].Package)
                        {
                            pkg.VehType += "," + currentCountry.Brands[j].Code + "," + currentCountry.Code;//Add the Brand to vehicle type 
                            pkgList.Add(pkg);
                        }
                    }
                }
                agentData.AddPackages(pkgList.ToArray());
                //get packages ends            
            }

            if (!hasCurrency)
            {
                
                //rev:01May2015 - When page is first load, AU is the default selected country
                string countrycode = "AUD";
                
                for (int i = 0; i <= agentData.agents.ToList().Count-1; i++)
                {
                    if (agentData.agents[i].DefaultCurrency == null)
                    {
                        List<THL.Booking.ExchangeRate> crs = new List<THL.Booking.ExchangeRate>();
                        THL.Booking.ExchangeRate currencyRates = new THL.Booking.ExchangeRate();
                        currencyRates.FromCode = countrycode;
                        currencyRates.ToCode = countrycode;
                        currencyRates.Rate = 1;
                        crs.Add(currencyRates);

                        agentData.agents[i].CurrencyRates = crs;
                        agentData.agents[i].DefaultCurrency = countrycode;
                        agentData.agents[i].CurrencyList = new string[1] { countrycode };
                    }
                }
            }

            return agentData;
        }



        /// <summary>
        /// Get AvailabilityResponse Instance for a supplied AvailabilityRequest object
        /// </summary>
        /// <param name="availabilityRequest"></param>
        /// <returns>AvailabilityResponse instance of Aurora</returns>
        public AvailabilityResponse GetB2BAvailability(B2BAvailabilityRequest availabilityRequest)
        {

            DateTime start, end;//Debug Code

            //Normalize the Brand
            availabilityRequest.Brand = AvailabilityHelper.NormaliseBrandString(availabilityRequest.Brand);

            //try {//TODO: implement exception handling
            AvailabilityResponse availabilityResponse = new AvailabilityResponse(availabilityRequest.AgentCode);//TODO: pass site (agentCode) here once MAC/AirNZ defined
            

            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraAvailabilityURL"];

            string auroraAvailabilityParamsStr = availabilityRequest.GenerateAuroraRequestURL() + "&AgnisInclusiveProduct=True&sPromoCode="; //rev:17Oct2014 - added to fix PromoCode in Phoenix

            start = DateTime.Now;
            string stringXml = GetStringForURL(auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr);
            end = DateTime.Now;

            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{AvailabilityRequest:" + (end - start) + "}");//Debug Line
            try
            {

                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
                THLDebug.LogXML("/availability/availability_" + DateTime.Now.Ticks, responseXML);

                //--debug code
                HttpContext.Current.Session["debugStr"] = responseXML.InnerXml;//debug line
                HttpContext.Current.Session["requestURL"] = auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr;//debug line
                //--debug code

                availabilityResponse.FillFromXML(responseXML, availabilityRequest.Brand);
                //debug line comes here: get serialised version of availabilityRequest + availabilityresponse and an xml dump of responseXML
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAvailability", ex.Message, "{'requestParams':'" + auroraAvailabilityParamsStr + "'}");
                availabilityResponse = null;
            }
            return availabilityResponse;
        }
        /*Methods added by Nimesh on 20th Mar 2015 to get email and send confirmation from PhoenixWebService*/
        public string getAgentEmail(string agentName)
        {
            string retValue = string.Empty;
            string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
            string passingParameters = "B2BAgentEmailAddress?agentCode=" + agentName;
            string requestURL = auroraBaseURL + "/" + passingParameters;
            try
            {
                string stringXml = GetStringForURL(requestURL);
                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
            }
            catch (Exception)
            {
                
                throw;
            }
            return retValue;

        }
        /*Ends - Methods added by Nimesh on 20th Mar 2015 to get email and send confirmation from PhoenixWebService*/
        public AlternativeAvailabilityItem[] GetB2BAlternateAvailability(B2BAltAvailabilityRequest availabilityRequest)
        {

            //add cross sell multiple brands support
            string vehicleBrand = AvailabilityHelper.NormaliseBrandString(availabilityRequest.Brand);

            List<AlternativeAvailabilityItem> altItemsList = new List<AlternativeAvailabilityItem>();
            DateTime start, end;//Debug Code
            //try {//TODO: implement exception handling
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"] + "/GetAlternateAvailability";
            string agentCode = availabilityRequest.AgentCode;
            string auroraAvailabilityParamsStr = availabilityRequest.GenerateAuroraRequestURL();
            string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
            string requestURL = auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr;  //"http://pap-app-002/PhoenixWS%20Alternate/service.asmx/GetAlternateAvailability";//?VehicleCode=bp6b&CountryCode=NZ&Brand=P&CheckOutZoneCode=chc&CheckOutDateTime=22-Aug-2011 11:00am&CheckInZoneCode=chc&CheckInDateTime=19-Sep-2011 11:00am&AgentCode=B2CNZ&PackageCode=&IsVan=True&NumberOfAdults=1&NoOfChildren=0&isBestBuy=True&countryOfResidence=nz&DisplayMode=1,2,3,4,5&Istestmode=False";
            AvailabilityResponse availabilityResponse = new AvailabilityResponse(agentCode);//TODO: pass site (agentCode) here once MAC/AirNZ defined
            try
            {
                start = DateTime.Now;
                string stringXml = GetStringForURL(requestURL);
                end = DateTime.Now;
                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
                XmlNodeList altAvailNodes = responseXML.SelectNodes("data/AvailableRate");
                if (altAvailNodes != null && altAvailNodes.Count > 0)
                {
                    int index = 0;
                    foreach (XmlNode altNode in altAvailNodes)
                    {
                        AlternativeAvailabilityItem aaItem = new AlternativeAvailabilityItem(altNode, availabilityRequest.VehicleModel);
                        aaItem.SortOrder = index++;

                        altItemsList.Add(aaItem);
                    }
                }
                else
                {
                    THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAlternateAvailability", "Alternate Availability Results is empty", "{'requestURL':'" + requestURL + "'}");
                    return null;
                }
                THLDebug.LogXML("/availability/availability_" + DateTime.Now.Ticks, responseXML);
                List<AlternativeAvailabilityItem> noAvailItems = new List<AlternativeAvailabilityItem>();
                foreach (AlternativeAvailabilityItem ai in altItemsList)
                    if (ai.AvailabilityType != AvailabilityItemType.Available)
                        noAvailItems.Add(ai);
                foreach (AlternativeAvailabilityItem ai in noAvailItems)
                    altItemsList.Remove(ai);
                AlternativeAvailabilityItem[] aaItems = altItemsList.Concat(noAvailItems).ToArray();
                if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{Alternate Availability Request:" + (end - start) + "}");//Debug Line
                return aaItems;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAlternateAvailability", msg, "{'requestURL':'" + requestURL + "'}");
                return null;
            }
        }


        private static bool LogADebutNote()
        {
            try
            {
                string logdebug = System.Web.Configuration.WebConfigurationManager.AppSettings["LogDebugNote"];
                return bool.Parse(logdebug);
               
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static bool LogDebugNotes(string message)
        {
            if (!LogADebutNote())
            {
                return false;
            }
            string logPath = System.Web.Configuration.WebConfigurationManager.AppSettings["logPath"];
            string timeStamp = DateTime.Now.ToString("ddMMyyyy");
            System.IO.StreamWriter sw = System.IO.File.AppendText(string.Concat(logPath, "DebugNote_", (timeStamp + ".txt")));
            try
            {
                sw.WriteLine((DateTime.Now.ToString("HHmmss") + (" : " + message)));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                sw.Close();
            }
        }


    }
}