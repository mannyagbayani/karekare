﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Net;
using System.Text;
using System.Collections.Specialized;
namespace THL.Booking
{
    /// <summary>
    /// Get Availability response Collection from Aurora for a provided AvailabilityRequest
    /// </summary>
    public partial class DataAccess
    {
        
        /// <summary>
        /// Get AvailabilityResponse Instance for a supplied AvailabilityRequest object
        /// </summary>
        /// <param name="availabilityRequest"></param>
        /// <returns>AvailabilityResponse instance of Aurora</returns>
        public AvailabilityResponse GetAvailability(AvailabilityRequest availabilityRequest)
        {
            DateTime start, end;//Debug Code

            //Normalize the Brand
            availabilityRequest.Brand = AvailabilityHelper.NormaliseBrandString(availabilityRequest.Brand);

            //try {//TODO: implement exception handling
            AvailabilityResponse availabilityResponse = new AvailabilityResponse("m");//TODO: pass site (agentCode) here once MAC/AirNZ defined
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraAvailabilityURL"];

            //rev:17Oct2014 - added to fix PromoCode in Phoenix and AgnisInclusiveProduct=True
            string auroraAvailabilityParamsStr = availabilityRequest.GenerateAuroraRequestURL() + "&IsGross=True&AgnisInclusiveProduct=true&sPromoCode=";//TODO: encapsulate and differ B2B/B2C
            
            start = DateTime.Now;
            string stringXml = GetStringForURL(auroraAvailabilityURL +  "?" + auroraAvailabilityParamsStr);
            end = DateTime.Now;
                        
            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{AvailabilityRequest:" + (end - start) + "}");//Debug Line
            try
            {

                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
                THLDebug.LogXML("/availability/availability_" + DateTime.Now.Ticks, responseXML);

                //--debug code
                HttpContext.Current.Session["debugStr"] = responseXML.InnerXml;//debug line
                HttpContext.Current.Session["requestURL"] = auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr;//debug line
                //--debug code

                availabilityResponse.FillFromXML(responseXML, availabilityRequest.Brand);
                //debug line comes here: get serialised version of availabilityRequest + availabilityresponse and an xml dump of responseXML
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAvailability", ex.Message, "{'requestParams':'" + auroraAvailabilityParamsStr + "'}");
                availabilityResponse = null;
            }
            return availabilityResponse;
        }

        /// <summary>
        /// Get AvailabilityResponse Instance for a supplied AvailabilityRequest object
        /// </summary>
        /// <param name="availabilityRequest"></param>
        /// <returns>AvailabilityResponse instance of Aurora</returns>
        public AvailabilityResponse GetB2BAvailability(AvailabilityRequest availabilityRequest, AgentLoginData ag)
        {
            DateTime start, end;//Debug Code

            //Normalize the Brand
            availabilityRequest.Brand = AvailabilityHelper.NormaliseBrandString(availabilityRequest.Brand);

            //try {//TODO: implement exception handling
            AvailabilityResponse availabilityResponse = new AvailabilityResponse("m");//TODO: pass site (agentCode) here once MAC/AirNZ defined
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraAvailabilityURL"];

            //rev:17Oct2014 - added to fix PromoCode in Phoenix
            string auroraAvailabilityParamsStr = availabilityRequest.GenerateAuroraRequestURL() + "&IsGross=" + (ag.GrossDefaultOn ? "True" : "False") + "&AgnisInclusiveProduct=True&sPromoCode=";//TODO: encapsulate and differ B2B/B2C

            start = DateTime.Now;
            string stringXml = GetStringForURL(auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr);
            end = DateTime.Now;

            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{AvailabilityRequest:" + (end - start) + "}");//Debug Line
            try
            {

                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
                THLDebug.LogXML("/availability/availability_" + DateTime.Now.Ticks, responseXML);

                //--debug code
                HttpContext.Current.Session["debugStr"] = responseXML.InnerXml;//debug line
                HttpContext.Current.Session["requestURL"] = auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr;//debug line
                //--debug code

                availabilityResponse.FillFromXML(responseXML, availabilityRequest.Brand);
                //debug line comes here: get serialised version of availabilityRequest + availabilityresponse and an xml dump of responseXML
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAvailability", ex.Message, "{'requestParams':'" + auroraAvailabilityParamsStr + "'}");
                availabilityResponse = null;
            }
            return availabilityResponse;
        }


        /// <summary>
        /// get an Aurora Package for the supplied identifier (AVPID)
        /// TODO: performance test and consider consuming as an XMLDocument
        /// </summary>
        /// <param name="avpId">AVPID</param>
        /// <returns>Aurora Package</returns>
        public AuroraPackage GetPackageForAvpId(string avpId, ApplicationData appData, bool IsGross)
        {


            string isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings["IsTestMode"];
            
            DateTime start, end;
            
            //try {//TODO: implement exception handling
            AuroraPackage ap = new AuroraPackage(avpId);
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraPackageURL"];

            start = DateTime.Now;
            string stringXml = GetStringForURL(auroraAvailabilityURL + "?avpId=" + avpId + "&IsGross="+ IsGross + "&IsTestMode="+ isTestMode);//TODO: define config options
            end = DateTime.Now;
            
            XmlDocument responseXML = new XmlDocument();
            responseXML.LoadXml(stringXml);
            
            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{PackageForAVPRequest:" + (end - start)+ "}");//Debug Line
            //--debug code
            THLDebug.LogXML("/package/package_" + avpId, responseXML);
            HttpContext.Current.Session["debugStr"] = responseXML.InnerXml;//debug line
            HttpContext.Current.Session["requestURL"] = auroraAvailabilityURL + "?avpId=" + avpId;//debug line
            //--debug code

            ap.LoadFromXML(responseXML, appData);
            return ap;
        }

        /// <summary>
        /// Submit a booking to Aurora using the WS Proxy Class
        /// TODO: performance test and consider consuming as an XMLDocument 
        /// </summary>
        /// <param name="bookingInfo"></param>
        /// <returns></returns>
        public string SubmitBookingToAurora(ref AuroraBooking bookingInfo)
        {

            DateTime start = DateTime.Now, end;//debug line 

            //try {//TODO: implement exception handling
            XmlDocument bookingXml = bookingInfo.ToXML();
            PhoenixWebService webService = new PhoenixWebService();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];
            
            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;                
            }
            
            XmlNode bookingResponse = null;
            try
            {
                bookingResponse = webService.CreateBookingWithExtraHireItemAndPayment(bookingXml);
                string auroraError = string.Empty;
                bool hasError = AuroraBooking.GetBookingError(bookingResponse, ref auroraError);
                if (hasError) SessionManager.GetSessionData().LastError = auroraError;
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DA.SubmitBookingToAurora", ex.Message, "{}");
                return null;
            }
                        
            end = DateTime.Now;
                     

            //--------------------added for location retrieval 20/10/09            
             XmlNode bookingPickUpElm = bookingResponse.SelectSingleNode("//BookingDetails/CkoAddress");
             XmlNode bookingDropOffElm = bookingResponse.SelectSingleNode("//BookingDetails/CkiAddress");
             if (bookingPickUpElm != null && bookingDropOffElm != null)
             {
                 bookingInfo.PickUpStr = bookingPickUpElm.InnerText;
                 bookingInfo.DropOffStr = bookingDropOffElm.InnerText;
             }
            //--------------------added for location retrieval
            
            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{CreateBookingToAurora:" + (end - start) + "}");//Debug Line
            
            //--debug code
            string timeStamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            THLDebug.LogBookingXML("/booking/response_" + timeStamp, bookingResponse);
            THLDebug.LogBookingXML("/booking/request_" + timeStamp, bookingInfo.ToXML());
            //HttpContext.Current.Session["debugStr"] = bookingResponse.OuterXml;//debug line
            //--debug code

            XmlNode bookingReponseIdElm = bookingResponse.SelectSingleNode("Booking/ReturnedBookingNumber");
            string bookingId = (bookingReponseIdElm != null ? bookingReponseIdElm.InnerText : null);
            if (bookingId != null)
                SessionManager.BookingStatus = BookingStatus.BookingSuccessful;
            return bookingId;
        }
        
         
        /// <summary>
        /// Submit a Confirmed Quote to Aurora
        /// </summary>
        /// <param name="quote">Quote Information</param>
        /// <param name="bookingInfo">Booking Reference</param>
        /// <returns></returns>
        public string SubmitConfirmedQuoteToAurora(AuroraQuote quote, ref AuroraBooking bookingInfo)
        { 
             
            DateTime start = DateTime.Now, end;//debug line 

            //try {//TODO: implement exception handling
            XmlDocument bookingXml = null;// bookingInfo.ToXML();
            PhoenixWebServiceV2 webService = new PhoenixWebServiceV2();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }

            XmlNode bookingResponse = null;
            try
            {
                //bookingResponse = webService.CreateBookingWithExtraHireItemAndPayment(bookingXml);
                bookingXml = quote.SerializeQuote();
                if(bookingXml != null)
                    bookingResponse = webService.ModifyBooking(bookingXml);
                else 
                {//cannot serialize the quote
                    THLDebug.LogError(ErrorTypes.Application, "DA.SubmitConfirmedQuoteToAurora", "failed to serialize quote", "{quoteId:'"+ quote.BookingID +"'}");
                    return null;
                }

            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DA.SubmitConfirmedQuoteToAurora", ex.Message, "{}");
                return null;
            }

            end = DateTime.Now;


            //--------------------added for location retrieval 20/10/09            
            XmlNode bookingPickUpElm = bookingResponse.SelectSingleNode("//BookingDetails/CkoAddress");
            XmlNode bookingDropOffElm = bookingResponse.SelectSingleNode("//BookingDetails/CkiAddress");
            if (bookingPickUpElm != null && bookingDropOffElm != null)
            {
                bookingInfo.PickUpStr = bookingPickUpElm.InnerText;
                bookingInfo.DropOffStr = bookingDropOffElm.InnerText;
            }
            //--------------------added for location retrieval
              
            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{CreateBookingToAurora:" + (end - start) + "}");//Debug Line

            //--debug code
            string timeStamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            THLDebug.LogBookingXML("/booking/response_" + timeStamp, bookingResponse);
            THLDebug.LogBookingXML("/booking/request_" + timeStamp, bookingXml.FirstChild);
            //HttpContext.Current.Session["debugStr"] = bookingResponse.OuterXml;//debug line
            //--debug code

            XmlNode bookingReponseIdElm = bookingResponse.SelectSingleNode("Booking/ReturnedBookingNumber");
            string bookingId = (bookingReponseIdElm != null ? bookingReponseIdElm.InnerText : null);
            if (bookingId != null)
                SessionManager.BookingStatus = BookingStatus.BookingSuccessful;
            return bookingId;
        }



        /// <summary>
        /// Force a partial bookin into Aurora with No Payment information, used by Email Form functionality
        /// </summary>
        /// <param name="bookingRequestXML">Booking XML Structure</param>
        /// <returns>Generated Booking ID</returns>
        public string ForceBookingToAurora(XmlDocument bookingRequestXML)
        {
            string generatedBookingID = string.Empty;

            DateTime start = DateTime.Now, end;//debug line 

            //try {//TODO: implement exception handling
            
            PhoenixWebService webService = new PhoenixWebService();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }
            XmlNode bookingResponse = webService.ForceCreateBooking(bookingRequestXML);
            end = DateTime.Now;
            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{ForceCreateBookingToAurora:" + (end - start) + "}");//Debug Line

            //--debug code
            string timeStamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            THLDebug.LogBookingXML("/booking/wl_response_" + timeStamp, bookingResponse);
            THLDebug.LogBookingXML("/booking/wl_request_" + timeStamp, bookingRequestXML.FirstChild);
            
            HttpContext.Current.Session["debugStr"] = bookingResponse.OuterXml;//debug line
            //--debug code

            XmlNode bookingReponseIdElm = bookingResponse.SelectSingleNode("Booking/ReturnedBookingNumber");
            string bookingId = (bookingReponseIdElm != null ? bookingReponseIdElm.InnerText : null);
            return bookingId;
        }
        
        /// <summary>
        /// Get a Quoted Booking for a provided Aurora Rental number
        /// </summary>
        /// <param name="rentalId">Aurora Generated Rental Number</param>
        /// <returns>AuroraBooking Instance representing the Quote (no payment info)</returns>
        public AuroraQuote GetQuoteForRentalId(string rentalNumber)
        {
            string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
            //Use Rest compatible instead of SOAP
            string quoteData = GetStringForURL(auroraBaseURL + "/RetrieveBooking?RentalNumber=" + rentalNumber);//TODO: consider loading an XML structure once service sealed
            string quotePackageData = GetStringForURL(auroraBaseURL + "/GetInsuranceAndExtraHireItemsForRentalQuote?BookingReference=" + rentalNumber + "&ReferenceId=" + rentalNumber);
            //--

            if (quoteData != null && quoteData != string.Empty && quotePackageData != null && quotePackageData != string.Empty && quoteData.Contains("RentalInfo"))
            {               
                XmlDocument quoteXml = new XmlDocument();
                quoteXml.LoadXml(quoteData);
                XmlDocument quotePackageXml = new XmlDocument();
                quotePackageXml.LoadXml(quotePackageData);

                THLDebug.LogQuoteInfo(rentalNumber.Replace('/', '-'), quoteXml, quotePackageXml);//Log for debug
                AuroraQuote quote = new AuroraQuote(quoteXml, quotePackageXml);//init quote with package and selected products info           
                //TODO: log
                return quote;
            }
            else
            {
                THLDebug.LogEvent(EventTypes.DBRequest, "{DataAccess.GetQuoteForRentalId:'no data for " + rentalNumber + "}");
                return null;
            }
        }
        
        /// <summary>
        /// Query Aurora WS for Alternate Availability of a given Availability Request
        /// TODO: move sorting and required types (DisplayMode) into config if the y will change frequently 
        /// </summary>
        /// <returns>AlternativeAvailabilityItem collection representation of the optional results</returns>
        public AlternativeAvailabilityItem[] GetAlternateAvailability(AvailabilityRequest availabilityRequest)
        {
            //add cross sell multiple brands support
            string vehicleBrand = AvailabilityHelper.NormaliseBrandString(availabilityRequest.Brand);
            
            
            List<AlternativeAvailabilityItem> altItemsList = new List<AlternativeAvailabilityItem>();
            DateTime start, end;//Debug Code
            //try {//TODO: implement exception handling
            AvailabilityResponse availabilityResponse = new AvailabilityResponse("m");//TODO: pass site (agentCode) here once MAC/AirNZ defined
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"] + "/GetAlternateAvailability";
            string agentCode = availabilityRequest.AgentCode;
            string auroraAvailabilityParamsStr = string.Empty;
            StringBuilder sb = new StringBuilder();//Construct Request Params
            sb.Append("Brand=" + /* swapped for CSell: availabilityRequest.Brand */ vehicleBrand + "&");
            sb.Append("VehicleCode=" + availabilityRequest.VehicleModel + "&");
            sb.Append("CountryCode=" + availabilityRequest.CountryCode + "&");
            sb.Append("CheckOutZoneCode=" + availabilityRequest.PickUpBranch + "&");
            sb.Append("CheckOutDateTime=" + availabilityRequest.PickUpDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("CheckInZoneCode=" + availabilityRequest.DropOffBranch + "&");
            sb.Append("CheckInDateTime=" + availabilityRequest.DropOffDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("AgentCode=" + agentCode + "&");
            sb.Append("PackageCode=" + availabilityRequest.PackageCode + "&");
            sb.Append("IsVan=" + (availabilityRequest.VehicleType == VehicleType.Car ? "False" : "True") + "&");
            sb.Append("NumberOfAdults=" + availabilityRequest.NumberOfAdults + "&");
            sb.Append("NoOfChildren=" + availabilityRequest.NumberOfChildren + "&");
            sb.Append("isBestBuy=True&");
            sb.Append("countryOfResidence=" + availabilityRequest.CountryOfResidence + "&");
            //if pick & drop are same no 3,6 (switch loc)
            string orderStr = (System.Web.Configuration.WebConfigurationManager.AppSettings["AltAvailOrder"] != null ? System.Web.Configuration.WebConfigurationManager.AppSettings["AltAvailOrder"] : "1,2,4,5");

            sb.Append("DisplayMode=" + (!availabilityRequest.PickUpBranch.Equals(availabilityRequest.DropOffBranch) ? "3,6," : string.Empty) + orderStr + "&");//TODO: move to config
            sb.Append("Istestmode=False&");//TODO: read from config
            auroraAvailabilityParamsStr = sb.ToString();

            string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
            string requestURL = auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr;  //"http://pap-app-002/PhoenixWS%20Alternate/service.asmx/GetAlternateAvailability";//?VehicleCode=bp6b&CountryCode=NZ&Brand=P&CheckOutZoneCode=chc&CheckOutDateTime=22-Aug-2011 11:00am&CheckInZoneCode=chc&CheckInDateTime=19-Sep-2011 11:00am&AgentCode=B2CNZ&PackageCode=&IsVan=True&NumberOfAdults=1&NoOfChildren=0&isBestBuy=True&countryOfResidence=nz&DisplayMode=1,2,3,4,5&Istestmode=False";
            
            try
            {
                start = DateTime.Now;
                string stringXml = GetStringForURL(requestURL);
                end = DateTime.Now;                
                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
                XmlNodeList altAvailNodes = responseXML.SelectNodes("data/AvailableRate");
                if (altAvailNodes != null && altAvailNodes.Count > 0)
                {
                    int index = 0;
                    foreach (XmlNode altNode in altAvailNodes)
                    {
                        AlternativeAvailabilityItem aaItem = new AlternativeAvailabilityItem(altNode, availabilityRequest.VehicleModel);
                        aaItem.SortOrder = index++;
                        altItemsList.Add(aaItem);
                    }
                }
                else
                {
                    THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAlternateAvailability", "Alternate Availability Results is empty", "{'requestURL':'" + requestURL + "'}");
                    return null;
                }
                THLDebug.LogXML("/availability/availability_" + DateTime.Now.Ticks, responseXML);
                List<AlternativeAvailabilityItem> noAvailItems = new List<AlternativeAvailabilityItem>();
                foreach (AlternativeAvailabilityItem ai in altItemsList)
                    if (ai.AvailabilityType != AvailabilityItemType.Available)
                        noAvailItems.Add(ai);
                foreach (AlternativeAvailabilityItem ai in noAvailItems)
                    altItemsList.Remove(ai);
                AlternativeAvailabilityItem[] aaItems = altItemsList.Concat(noAvailItems).ToArray();
                if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{Alternate Availability Request:" + (end - start) + "}");//Debug Line
                return aaItems;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAlternateAvailability", msg, "{'requestURL':'" + requestURL + "'}");
                return null;
            }
        }

        //-----------------------------------------------------------------------------------------------------
        //rev:mia MAY 8 2013  - PART OF APCOVER ENHANCEMENT. 
        //                     - this will extract the Availabilty XML when doing forcing of a package
        //-----------------------------------------------------------------------------------------------------
        public string GetForceInsuranceAvpId(string avpId, string productcode, bool IsGross)
        {

            InsuranceProduct[] ins = null;

            string isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings["IsTestMode"];
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraPackageURL"];

            //start = DateTime.Now;
            string stringXml = GetStringForURL(auroraAvailabilityURL + "?avpId=" + avpId + "&IsGross=" + IsGross + "&IsTestMode=" + isTestMode);//TODO: define config options
            //end = DateTime.Now;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(stringXml);


            XmlNodeList insuranceNodes = xmlDoc.SelectNodes("data/ConfigurationProducts/Insurence/Product");
                        
            //-----------------------------------------------------------------------------------------------------
            if (insuranceNodes != null && insuranceNodes.Count > 0)//has insurance items
            {
                foreach (XmlNode insuranceNode in insuranceNodes)
                {
                    XmlNodeList requiredActionsCollection = insuranceNode.SelectNodes("RequiredActions/Action");
                    if (requiredActionsCollection != null && requiredActionsCollection.Count > 0)
                    {

                        NameValueCollection nvc = new NameValueCollection();
                        foreach (XmlNode requiredNore in requiredActionsCollection)
                        {
                            string actionType = requiredNore.Attributes["code"].Value;
                            string actionTarget = requiredNore.Attributes["PrdCode"].Value;
                            if (actionType == "DISPLAYWHEN" && actionTarget == productcode)
                            {
                                return insuranceNode.Attributes["Code"].Value;
                            }

                        }//foreach (XmlNode requiredNore in requiredActionsCollection)

                    }//if (requiredActionsCollection != null && requiredActionsCollection.Count > 0)

                }//foreach (XmlNode insuranceNode in insuranceNodes)

            }//if (insuranceNodes != null && insuranceNodes.Count > 0)//has insurance items

            return string.Empty;
        }
        //-----------------------------------------------------------------------------------------------------


        //rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
        public string B2BAutoSendEmail(string rentalid,string emailaddress)
        {

            if (rentalid.Contains("/") == true)
            {
                rentalid = rentalid.Replace("/", "-");
            }
                    
            PhoenixWebService webService = new PhoenixWebService();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }

            string wsResponse = "";
            try
            {
                //if emailaddress is empty, then use agent email address
                wsResponse = webService.B2BCreateAndSendconfirmation(rentalid, emailaddress, (string.IsNullOrEmpty(emailaddress) ? false : true));
                string auroraError = string.Empty;
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DA.B2BAutoSendEmail", ex.Message, "{}");
                return "ERROR:" + ex.Message ;
            }
                       

            return wsResponse;
            
        }

        public string B2BAutoSendEmail(string emailaddress, string subject, string content, string b2bagent)
        {

            if (string.IsNullOrEmpty(content))      return "Error: Confirmation Html is empty";
            if (string.IsNullOrEmpty(emailaddress)) return "Error: Email is empty";


            PhoenixWebService webService = new PhoenixWebService();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }

            string wsResponse = "";
            try
            {
                wsResponse = webService.B2BSendConfirmation(emailaddress, subject, content, b2bagent);
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DA.B2BAutoSendEmail", ex.Message, "{}");
                return "ERROR:" + ex.Message;
            }


            return wsResponse;

        }

        public string B2BAgentEmailAddress(string b2bagent)
        {

            
            PhoenixWebService webService = new PhoenixWebService();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }

            string wsResponse = "";
            try
            {
                wsResponse = webService.B2BAgentEmailAddress(b2bagent);
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DA.B2BAgentEmailAddress", ex.Message, "{}");
                return "ERROR:" + ex.Message;
            }
            return wsResponse;
        }
    }
}
