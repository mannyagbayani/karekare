﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections.Generic;


namespace THL.Booking
{
    public enum PaymentProvider
    {
        DPS = 1
    }

    public class DPSAccount
    {
        public DPSAccount(string agentCode, string username, string password, string currencyStr)
        {
            AgentAccount = agentCode;
            PostUser = username;
            PostPassword = password;
            switch (currencyStr)
            { 
                case "AUD":
                    Currency = CurrencyCode.AUD;
                    break;
                case "NZD":
                    Currency = CurrencyCode.NZD;
                    break;
            }
        }

        public string AgentAccount;
        public CurrencyCode Currency;
        public string PostUser;
        public string PostPassword;        
    }

    public struct DPSResponse
    {
        public bool SuccessStatus;
        public string ResponseText;
        public string HelpText;
        public string DPSTransactionReference;
        public string AuthCode;
        public bool IsAuthorised;
    }

    /// <summary>
    /// Payment DataAccess Functionality via DPS 
    /// </summary>
    public partial class DataAccess
    {
        /// <summary>
        /// Initial the HTTP DataAceess Layer from config
        /// </summary>
        public DataAccess()
        {
            useProxy = (System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"] != null);
            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            dpsServiceURI = @System.Web.Configuration.WebConfigurationManager.AppSettings["DPSServiceURL"];
        }
        
        string dpsServiceURI;
        bool useProxy;

        string dPSResponseText;
        public string DPSResponseText
        {
            get { return dPSResponseText; }
        }

        /// <summary>
        /// Create a payment using the selected payment provider
        /// TOOD: future implementation could include additional payment providers
        /// </summary>
        /// <param name="txn"></param>
        /// <returns></returns>
        public bool CreatePayment(ref Transaction txn, DPSAccount dpsAccount)
        {           
            try
            {//TODO: catch log and inform calling layer with false, add timeout/no connectivity support              
                               
                decimal submittedAmount = (txn.Type == TransactionType.Purchase ? txn.Amount : 1.00m);
                // form the PXPost Xml message
                StringWriter sw = new StringWriter();
                XmlTextWriter xtw = new XmlTextWriter(sw);
                xtw.WriteStartElement("Txn");
                
                //xtw.WriteElementString("PostUsername", "ExploreMoreSSLDev");
                //xtw.WriteElementString("PostPassword", "5fce9598");

                //---- Added RH 29/10/09 for resolving Auth limit in AU acct
                string authAcctParamStr = System.Web.Configuration.WebConfigurationManager.AppSettings["DPSAuthCreds"];
                string[] authAcctParams = authAcctParamStr.Split(':');
                bool isAuthTrn = (txn.Type == TransactionType.Auth);
                
                if (isAuthTrn && authAcctParams != null && authAcctParams.Length == 3)
                {//force Auth from Web.Config
                    string AuthUName = authAcctParams[0];
                    string AuthPass = authAcctParams[1];
                    string AuthCurrency = authAcctParams[2];                    
                    xtw.WriteElementString("PostUsername", AuthUName);
                    xtw.WriteElementString("PostPassword", AuthPass);
                    xtw.WriteElementString("InputCurrency", AuthCurrency);
                }
                else
                {//regular payment/auth
                    xtw.WriteElementString("PostUsername", dpsAccount.PostUser);
                    xtw.WriteElementString("PostPassword", dpsAccount.PostPassword);
                    xtw.WriteElementString("InputCurrency", dpsAccount.Currency.ToString());  
                }
                //----------------------------------------------------------
                //xtw.WriteElementString("PostUsername", dpsAccount.PostUser); //TODO: remove once bypass removed
                //xtw.WriteElementString("PostPassword", dpsAccount.PostPassword); //TODO: remove once bypass removed              
                xtw.WriteElementString("CardHolderName", txn.CardHolderName);
                xtw.WriteElementString("CardNumber", txn.CreditCardNumber);
                xtw.WriteElementString("Amount", submittedAmount.ToString("#0.00"));
                xtw.WriteElementString("DateExpiry", txn.ExpiryDate);
                xtw.WriteElementString("Cvc2", "");
                //xtw.WriteElementString("InputCurrency", dpsAccount.Currency.ToString()); //TODO: remove once bypass removed
                xtw.WriteElementString("TxnType", txn.Type.ToString());
                xtw.WriteElementString("TxnId", "");
                xtw.WriteElementString("MerchantReference", txn.BookingReference);
                xtw.WriteElementString("TxnData1", txn.TextLines != null && txn.TextLines[0] != null ? txn.TextLines[0] : "");
                xtw.WriteElementString("TxnData2", txn.TextLines != null && txn.TextLines[1] != null ? txn.TextLines[1] : "");
                xtw.WriteElementString("TxnData3", txn.TextLines != null && txn.TextLines[2] != null ? txn.TextLines[2] : "");
                xtw.WriteEndElement();
                xtw.Close();

                DateTime start = DateTime.Now, end;//debug Line
                
                // Send the Xml message to PXPost
                bool success = false;
                WebRequest wrq = WebRequest.Create(dpsServiceURI);
                wrq.Method = "POST";
                wrq.ContentType = "application/x-www-form-urlencoded";

                if (useProxy == true)
                {
                    WebProxy localProxy = new WebProxy();
                    localProxy.Address = new Uri(HTTPProxy);
                    wrq.Proxy = localProxy;
                }

                byte[] b = Encoding.ASCII.GetBytes(sw.ToString());
                wrq.ContentLength = b.Length;
                Stream s = wrq.GetRequestStream();
                s.Write(b, 0, b.Length);
                s.Close();

                // Check the response
                WebResponse wrs = wrq.GetResponse();
                if (wrs != null) //has response
                {
                    StreamReader sr = new StreamReader(wrs.GetResponseStream());
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(sr.ReadToEnd().Trim());
                    if (xd.SelectSingleNode("/Txn/Success") != null)
                    {//succesful transaction
                        success = (xd.SelectSingleNode("/Txn/Success").InnerText == "1");                        
                        string reCo = xd.SelectSingleNode("/Txn/ReCo").InnerText;//dps internal (use Authorized/Success)
                        txn.DPSResponseText = xd.SelectSingleNode("/Txn/ResponseText").InnerText;//
                        txn.DPSHelpText = xd.SelectSingleNode("/Txn/HelpText").InnerText;
                        string authorized = xd.SelectSingleNode("/Txn/Transaction/Authorized").InnerText;//0/1 if authorized or not
                        txn.TransactionCode = xd.SelectSingleNode("/Txn/Transaction/DpsTxnRef").InnerText;//if approved could use for refund (no card needed) 
                        txn.AutorizationCode = xd.SelectSingleNode("/Txn/Transaction/AuthCode").InnerText;
                        if (!success)
                        {
                            dPSResponseText = "Failed to create payment (" + txn.DPSResponseText + ")";                            
                        }                       
                        // further error handling code should go here
                        if (THLDebug.EventLogOn)//Debug Code, Enable For Debug DPS Calls
                        {
                            XmlDocument reqDoc = new XmlDocument();
                            reqDoc.LoadXml(sw.ToString());
                            THLDebug.LogXML("/dps/dpsrequest_" + DateTime.Now.Millisecond, reqDoc); 
                            THLDebug.LogXML("/dps/dpsResponse_" + DateTime.Now.Millisecond, xd);                           
                        }
                    }
                    else
                    {
                        dPSResponseText = "payment failed (code 1011)";
                        success = false;
                    }
                }
                end = DateTime.Now;
                if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{CreateDPSPayment:" + (end - start) + "}");//Debug Line

                return success;
            }
            catch (Exception ex)
            {
                dPSResponseText = "payment failed (code 1012)";
                string err = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, err, "DPSPAyment", "{msg:" + err + "}");
                return false;
            }
        }

        /// <summary>
        /// Get Available DPS Accounts from config
        /// </summary>
        /// <returns></returns>
        public DPSAccount[] GetDPSAccounts() 
        {
            List<DPSAccount> accountsList = new List<DPSAccount>();
            string dpsDataPath = @System.Web.Configuration.WebConfigurationManager.AppSettings["DPSData"];
            XmlDocument accountsXML = GetXMLFromFile(dpsDataPath);           
            XmlNodeList dpsAccountNodes = accountsXML.SelectNodes("//account");
            if (dpsAccountNodes != null && dpsAccountNodes.Count > 0)
            {
                foreach (XmlNode xmlActNode in dpsAccountNodes)
                {
                    string username = xmlActNode.Attributes["name"].Value;
                    string password = xmlActNode.Attributes["password"].Value;
                    string agentCode = xmlActNode.Attributes["agent"].Value;
                    string currencyStr = xmlActNode.Attributes["currency"].Value;
                    DPSAccount currentAct = new DPSAccount(agentCode, username, password, currencyStr);
                    accountsList.Add(currentAct);
                }
                return accountsList.ToArray();
            }
            else
            {
                return null; 
            }         
           
        }
    }
}