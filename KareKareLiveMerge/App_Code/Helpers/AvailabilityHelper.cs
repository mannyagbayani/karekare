﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;

namespace THL.Booking
{

    /// <summary>
    /// Summary description for AvailabilityHelper
    /// </summary>
    public class AvailabilityHelper
    {

        public AvailabilityHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static NameValueCollection GetCollectionForQueryString(string queryString)
        {
            NameValueCollection nvc = new NameValueCollection();
            string[] queryStringParams = queryString.Split('&');
            foreach (string paramStr in queryStringParams)
            {
                string[] pair = paramStr.Split('=');
                nvc.Add(pair[0], pair[1]);
            }
            return nvc;
        }

        public static string NormalizeParamsString(string rawString)
        {
            return rawString.Replace("%20", " ");
        }

        /// <summary>
        /// Test Collection for required incoming Aurora params
        /// </summary>
        /// <param name="paramsCollection"></param>
        /// <returns>Required Check Status</returns>
        public static bool HasRequiredParams(NameValueCollection paramsCollection)
        {//"Brand=X&CountryCode=NZ&VehicleCode=&CheckOutZoneCode=AK2&CheckOutDateTime=15-JUN-2009%2010:00&CheckInZoneCode=Ak2&CheckInDateTime=15-JUL-2009%2010:00&NoOfAdults=2&NoOfChildren=0&AgentCode=B2CNZ&PackageCode=&IsVan=True&IsBestBuy=True&CountryOfResidence=NZ&IsTestMode=false";
            DateTime parseTester;
            if (paramsCollection["Brand"] != null &&
                paramsCollection["CountryCode"] != null /* && (paramsCollection["CountryCode"].ToUpper().Equals("NZ") || paramsCollection["CountryCode"].ToUpper().Equals("AU"))*/ &&
                paramsCollection["CheckOutZoneCode"] != null &&//TODO: validate
                paramsCollection["CheckInZoneCode"] != null && //TODO: validate
                paramsCollection["NoOfAdults"] != null &&
                paramsCollection["IsVan"] != null &&//TODO: validate
                paramsCollection["IsBestBuy"] != null &&
                DateTime.TryParse(paramsCollection["CheckOutDateTime"], out parseTester) &&
                DateTime.TryParse(paramsCollection["CheckInDateTime"], out parseTester)
                )
                return true;
            else
                return false;
        }

        /// <summary>
        /// Mapper from ctrl params to Aurora collection, TODO: Fork by Ctrl Type, current horizontal
        /// </summary>
        /// <param name="BPAECollection"></param>
        /// <returns></returns>
        public static NameValueCollection CtrlToAuroraCollectionMapper(NameValueCollection CtrlParamsCollection)
        {
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in CtrlParamsCollection.AllKeys)
            {
                switch (key)
                {
                    case "controlBrand":
                        nvc.Add("Brand", CtrlParamsCollection[key]);
                        break;
                    case "vt":
                        nvc.Add("IsVan", (CtrlParamsCollection[key].ToUpper().Contains("AV") || CtrlParamsCollection[key].ToUpper().Contains("RV") ? "True" : "False"));
                        break;
                    case "ctrlSearchBox_dropVehicleType":
                        //TODO: check
                        break;
                    case "countryCode":
                        nvc.Add("CountryCode", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_calPickUp_DateText":
                        nvc.Add("CheckOutDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(CtrlParamsCollection["ctrlSearchBox_calPickUp_DateText"], CtrlParamsCollection["ctrlSearchBox_dropPickUpTime"]));
                        break;
                    case "ctrlSearchBox_calDropOff_DateText":
                        nvc.Add("CheckInDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(CtrlParamsCollection["ctrlSearchBox_calDropOff_DateText"], CtrlParamsCollection["ctrlSearchBox_dropDropOffTime"]));
                        break;
                    case "ctrlSearchBox_dropChildren":
                        nvc.Add("NoOfChildren", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropAdults":
                        nvc.Add("NoOfAdults", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropDropOffLocation":
                        nvc.Add("CheckInZoneCode", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropPickUpLocation":
                        nvc.Add("CheckOutZoneCode", CtrlParamsCollection[key]);
                        break;
                    case "countryOfResidence":
                        nvc.Add("CountryOfResidence", CtrlParamsCollection[key]);
                        break;
                    case "vehicleCode":
                        nvc.Add("VehicleCode", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropVendor":
                        nvc.Add("CompanyCode", CtrlParamsCollection[key]);
                        break;

                    case "pc":
                        nvc.Add("PackageCode", CtrlParamsCollection[key]);
                        break;


                }
            }
            nvc.Add("IsTestMode", "False");//TODO: from config
            nvc.Add("IsBestBuy", "True");//TODO: from config
            return nvc;
        }

        /// <summary>
        /// Old Control Params Mapper
        /// </summary>
        /// <param name="CtrlParamsCollection"></param>
        /// <returns></returns>
        public static NameValueCollection OldControlCollectionMapper(NameValueCollection CtrlParamsCollection)
        {
            string brandStr = string.Empty;
            NameValueCollection nvc = new NameValueCollection();

            foreach (string key in CtrlParamsCollection.AllKeys)
            {
                switch (key)
                {
                    case "vc":
                        if (CtrlParamsCollection["vc"] != string.Empty)
                        {
                            brandStr = THLBrand.GetBrandCharForBPAECode(CtrlParamsCollection[key]);
                            nvc.Add("Brand", brandStr);
                        }
                        break;
                    case "brand":
                        nvc.Add("Brand", CtrlParamsCollection[key].ToUpper());
                        break;
                    case "vtype":
                        nvc.Add("IsVan", (CtrlParamsCollection[key].ToUpper() == "AV" || CtrlParamsCollection[key].ToUpper() == "RV" ? "True" : "False"));
                        break;

                    case "vt"://test
                        nvc.Add("IsVan", (CtrlParamsCollection[key].ToUpper() == "AV" || CtrlParamsCollection[key].ToUpper() == "RV" ? "True" : "False"));
                        break;

                    case "ctrlSearchBox_dropVehicleType":
                        //TODO: check
                        break;
                    case "cc":
                        nvc.Add("CountryCode", CtrlParamsCollection[key]);
                        break;
                    case "pd":
                        //pd=23&pm=8&py=2009&pt=09:00&                    
                        nvc.Add("CheckOutDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(DateTimeHelper.LeadingZerosPadding(CtrlParamsCollection["pd"], 2) + "-" + DateTimeHelper.NumberToMonth(CtrlParamsCollection["pm"]) + "-" + CtrlParamsCollection["py"], CtrlParamsCollection["pt"]));
                        break;
                    case "dd":
                        //dd=18&dm=9&dy=2009&dt=09:00&
                        nvc.Add("CheckInDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(DateTimeHelper.LeadingZerosPadding(CtrlParamsCollection["dd"], 2) + "-" + DateTimeHelper.NumberToMonth(CtrlParamsCollection["dm"]) + "-" + CtrlParamsCollection["dy"], CtrlParamsCollection["dt"]));
                        break;
                    case "nc":
                        nvc.Add("NoOfChildren", CtrlParamsCollection[key]);
                        break;
                    case "na":
                        nvc.Add("NoOfAdults", CtrlParamsCollection[key]);
                        break;
                    case "pb":
                        nvc.Add("CheckOutZoneCode", ConvertBPAEToAuroraLocationCode(CtrlParamsCollection[key]));
                        break;
                    case "db":
                        nvc.Add("CheckInZoneCode", ConvertBPAEToAuroraLocationCode(CtrlParamsCollection[key]));
                        break;
                    case "cr":
                        nvc.Add("CountryOfResidence", CtrlParamsCollection[key]);
                        break;
                    case "vh":
                        nvc.Add("VehicleCode", ConvertBPAEToAuroraVehicleCode(CtrlParamsCollection[key]));
                        break;
                    case "ch":
                        nvc.Add("ChannelCode", CtrlParamsCollection[key]);
                        break;
                    case "pc":
                        nvc.Add("PackageCode", CtrlParamsCollection[key]);
                        break;
                    //TODO:
                    //vc=ma&
                    //dd=18&
                    //dm=9&
                    //dy=2009&
                    //dt=09:00&
                    //vt=rv&
                    //vh=               
                }
            }
            nvc.Add("IsTestMode", "False");//TODO: from config
            nvc.Add("IsBestBuy", "True");//TODO: from config
            return nvc;
        }

        public static CountryCode GetCountryCodeForString(string countryCodeString)
        {
            CountryCode code = CountryCode.NONE;
            switch (countryCodeString.ToLower())
            {
                case "nz":
                    code = CountryCode.NZ;
                    break;
                case "au":
                    code = CountryCode.AU;
                    break;
                case "us":
                    code = CountryCode.US;
                    break;
            }
            return code;
        }

        public static string GetCountryName(CountryCode code)
        {
            string countryName = string.Empty;
            switch (code)
            {
                case CountryCode.NZ:
                    countryName = "New Zealand";
                    break;
                case CountryCode.AU:
                    countryName = "Australia";
                    break;
                //TODO: Add MAC Countries
            }
            return countryName;
        }

        /// <summary>
        /// remove any prefix from incoming URLs
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string NormalizeURL(string url)
        {
            url = url.Replace("http://", string.Empty);
            url = url.Replace("https://", string.Empty);
            return url;
        }

        public static string NVCToQueryString(NameValueCollection nvc)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in nvc.AllKeys)
                sb.Append(key + "=" + nvc[key] + "&");
            return sb.ToString().TrimEnd('&');
        }

        public static bool HasDiscountedDays(AvailabilityItemChargeRateBand[] rateBands)
        {
            foreach (AvailabilityItemChargeRateBand rateBand in rateBands)
            {
                if (rateBand.OriginalRate > rateBand.DiscountedRate)
                    return true;
            }
            return false;
        }

        public static bool HasFreeDays(AvailabilityItemChargeRateBand[] rateBands)
        {
            foreach (AvailabilityItemChargeRateBand rateBand in rateBands)
            {
                if (rateBand.IncludesFreeDay > 0)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Map Aurora Type to Booking Type
        /// </summary>
        /// <param name="vehicleTypeStr"></param>
        /// <returns></returns>
        public static VehicleType GetVehicleTypeForAuroraString(string vehicleTypeStr)
        {
            VehicleType vehicleType = VehicleType.Both;
            switch (vehicleTypeStr)
            {
                case "av":
                    vehicleType = VehicleType.Campervan;
                    break;
                case "ac":
                    vehicleType = VehicleType.Car;
                    break;
            }
            return vehicleType;
        }


        /// <summary>
        /// Map VehicleType to Aurora vehicle StrAurora Type to Booking Type
        /// </summary>
        /// <param name="vehicleTypeStr"></param>
        /// <returns></returns>
        public static string GetAuroraVehicleStrForVehicleType(VehicleType vehicleType)
        {
            string vehicleTypeStr = "both";
            switch (vehicleType)
            {
                case VehicleType.Campervan:
                    vehicleTypeStr = "av";
                    break;
                case VehicleType.Car:
                    vehicleTypeStr = "ac";
                    break;
            }
            return vehicleTypeStr;
        }

        public static string ConvertBPAEToAuroraVehicleCode(string bpaeVehicleCode)//aucvbz.2BB -> auavb.2BB
        {
            if (!bpaeVehicleCode.Contains("cv") && !bpaeVehicleCode.Contains("rv")) return bpaeVehicleCode;//not bpae

            string countryStr = bpaeVehicleCode.Substring(0, 2);
            string vehicleTypeStr = bpaeVehicleCode.Substring(2, 2);
            string brandStr = bpaeVehicleCode.Substring(4, 2);
            string vehicleAuroraCodeStr = bpaeVehicleCode.Split('.')[1];
            return countryStr + (vehicleTypeStr == "cv" ? "av" : "ac") + THLBrand.GetBrandCharForBPAECode(brandStr) + "." + vehicleAuroraCodeStr;
        }

        public static string ConvertBPAEToAuroraLocationCode(string bpaeLocationCode)
        {
            NameValueCollection bpaeMapper = ApplicationManager.GetApplicationData().BPAEMapper;
            if (bpaeMapper[bpaeLocationCode] != null)
                return bpaeMapper[bpaeLocationCode].ToUpper();
            return bpaeLocationCode;
        }

        public static String ConstructQueryString(NameValueCollection parameters)
        {
            List<String> items = new List<String>();
            foreach (String name in parameters)
                items.Add(String.Concat(name, "=", parameters[name]));
            return String.Join("&", items.ToArray());
        }


        /// <summary>
        /// Generate an Availability Ration string for Reporting purposes
        /// </summary>
        /// <param name="availabilityResponse"></param>
        /// <returns></returns>
        static string availabilityRate(AvailabilityResponse availabilityResponse, AvailabilityRequest availabilityRequest)
        {
            string rateStr = string.Empty;
            try
            {
                if (availabilityResponse.GetAvailabilityItems(string.Empty) != null)
                {
                    int availCounter = 0, nonAvailCounter = 0;
                    foreach (AvailabilityItem aItem in availabilityResponse.GetAvailabilityItems(string.Empty))
                    {
                        if (aItem.AvailabilityType == AvailabilityItemType.Available)//TODO: include on Request?
                            availCounter++;
                        else
                            nonAvailCounter++;
                    }
                    //-- Enhance reporting here 12/08/10 (add FinWeek and locations on top of availability
                    //  00/10/42/SYD/SYD                    
                    string resultStr = availCounter.ToString().PadLeft(2, '0') + "/"
                        + (availCounter + nonAvailCounter).ToString().PadLeft(2, '0') + "/"
                        + DateTimeHelper.FinWeekForDate(availabilityRequest.PickUpDate) + "/"
                        + availabilityRequest.PickUpBranch.ToUpper() + "/"
                        + availabilityRequest.DropOffBranch.ToUpper();
                    return resultStr;
                    //--------------------------- Enhancment ends 
                    //return availCounter + "/" + (availCounter + nonAvailCounter);
                }
                else
                {
                    return "n/a";
                }
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Presentation, "AvailabilityHelper.AvailabilityRate", ex.Message, "{error:'failed to generating availability ratio'}");
                return "n/a";
            }
        }

        public static AnalyticsEventTrackingItem[] GetTrackingEventsForAvailResponse(AvailabilityResponse availabilityResponse, AvailabilityRequest availabilityRequest)
        {
            try
            {

                string availRateStr = availabilityRate(availabilityResponse, availabilityRequest);
                int availCounter = 0;
                string[] availParams = availRateStr.Split('/');
                if (availParams != null && availParams.Length > 0 && availParams[0] != "n")
                {
                    int.TryParse(availParams[0], out  availCounter);
                }
                AnalyticsEventTrackingItem aet = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "Availability", availRateStr, availCounter);
                //AnalyticsEventTrackingItem leadTimeTrack = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "Lead",

                return new AnalyticsEventTrackingItem[] { aet };
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Presentation, "AvailabilityHelper.GetTrackingEventsForAvailResponse", ex.Message, "{error:'failed to generating availability tracking item'}");
                return null;
            }
        }


        public static AnalyticsEventTrackingItem[] GetTrackingEventsForAltAvailResponse(AvailabilityResponse availabilityResponse, AvailabilityRequest availabilityRequest)
        {
            try
            {
                List<AnalyticsEventTrackingItem> aetList = new List<AnalyticsEventTrackingItem>();
                string availRateStr = availabilityRate(availabilityResponse, availabilityRequest);
                int availCounter = 0;
                //string[] availParams = availRateStr.Split('/');
                //if (availParams != null && availParams.Length > 0 && availParams[0] != "n")
                //{
                //    int.TryParse(availParams[0], out  availCounter);
                //}
                foreach (AvailabilityItem item in availabilityResponse.GetAvailabilityItems(string.Empty))
                {
                    AlternativeAvailabilityItem aaItem = (AlternativeAvailabilityItem)(item);
                    availRateStr += "/" + aaItem.Type.ToString();
                    AnalyticsEventTrackingItem aet = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "AltAvailability", availRateStr, availCounter);
                    aetList.Add(aet);
                }
                //AnalyticsEventTrackingItem aet = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "AltAvailability", availRateStr, availCounter);
                //AnalyticsEventTrackingItem leadTimeTrack = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "Lead",
                return aetList.ToArray();
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Presentation, "AvailabilityHelper.GetTrackingEventsForAltAvailResponse", ex.Message, "{error:'failed to generating availability tracking item'}");
                return null;
            }
        }




        /// <summary>
        /// Get UOM Enum object for Aurora attribute string
        /// </summary>
        /// <param name="uomString"></param>
        /// <returns></returns>
        public static UOMType UOMFromString(string uomString)
        {
            UOMType oumType = UOMType.None;
            switch (uomString.ToUpper())
            {
                case "DAY":
                    oumType = UOMType.Day;
                    break;
                case "24HR":
                    oumType = UOMType.H24;
                    break;
            }
            return oumType;
        }

        /// <summary>
        /// Return a central Library reference for the provider request and Brand/Vehicle Context
        /// </summary>
        /// <param name="request">The Users Request</param>
        /// <param name="appDomain">The Domain to reference this path to</param>
        /// <param name="assetsURL">base TLD of the resources site</param>
        /// <returns></returns>
        public static string GetCentralLibPath(AvailabilityRequest request, THLDomain appDomain, string assetsURL)
        {
            try
            {
                string BrandChar = appDomain.BrandChar.ToString();
                string BrandCountryAssetPath = THLBrand.GetNameForBrand(appDomain.Brand) + "/" + AvailabilityHelper.GetCountryName(appDomain.Country).Replace(" ", "-"); //"Britz/Australia"

                if (appDomain.IsAggregator)//added for AltAvail Aggregator support 25/05/2011,TODO: define Aggregator assets default currenttly NZ
                    BrandCountryAssetPath = "MAC" + "/" + (request.CountryCode == CountryCode.NONE ? "New-Zealand" : AvailabilityHelper.GetCountryName(request.CountryCode).Replace(" ", "-"));
                if (appDomain.Brand == THLBrands.AIRNZ)//TODO: define convention, and default state when bo country defined now on: "New-Zealand"
                    BrandCountryAssetPath = "AirNZ" + "/" + (request.CountryCode == CountryCode.NONE ? "New-Zealand" : AvailabilityHelper.GetCountryName(request.CountryCode).Replace(" ", "-"));

                string VehicleTypeStr = (request.VehicleType == VehicleType.Car ? "Cars" : "Campervans");
                string VehicleImagePath = assetsURL + "/CentralLibraryImages/" + BrandCountryAssetPath + "/" + VehicleTypeStr;
                VehicleImagePath = VehicleImagePath.Replace(' ', '-');
                return VehicleImagePath;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: define logging requirements and implement
                return string.Empty;//TODO: defualt path?
            }
        }

        public static string GetVehicleImageBasePath(CountryCode countryCode, THLBrands brand, VehicleType vType, string vCode, string CDNBase, bool domainVariation)
        {
            if (domainVariation)
            {
                THLDomain appDomain = ApplicationManager.GetDomainForURL(HttpContext.Current.Request.Url.Host);

                //rev:mia check it its not nil
                if (appDomain != null)
                {
                    if (appDomain.Country == CountryCode.NONE || appDomain.IsAggregator)
                    {
                        return GetAggregatorImageBasePath(countryCode, brand, vType, vCode, CDNBase);
                    }
                }
                
            }
            return GetBrandImageBasePath(countryCode, brand, vType, vCode, CDNBase);
        }

        public static string GetBrandImageBasePath(CountryCode countryCode, THLBrands brand, VehicleType vType, string vCode, string CDNBase)
        {
            string brandCountryAssetPath = String.Format("{0}/{1}", THLBrand.GetNameForBrand(brand).Replace(" ", "-"), GetCountryName(countryCode).Replace(" ", "-"));
            string vehicleTypeInString = (vType == VehicleType.Car ? "Cars" : "Campervans");
            string vehicleImageNameConvension = countryCode + GetAuroraVehicleStrForVehicleType(vType) + THLBrand.GetBrandChar(brand) + "." + vCode;
            return String.Format("{0}/CentralLibraryImages/{1}/{2}/{3}/{3}", CDNBase, brandCountryAssetPath, vehicleTypeInString, vehicleImageNameConvension, vehicleImageNameConvension);
        }

        public static string GetAggregatorImageBasePath(CountryCode countryCode, THLBrands brand, VehicleType vType, string vCode, string CDNBase)
        {
            string aggregatorCountryAssetPath = String.Format("MAC/{0}", GetCountryName(countryCode).Replace(" ", "-"));
            string vehicleTypeInString = (vType == VehicleType.Car ? "Cars" : "Campervans");
            string vehicleImageNameConvension = countryCode + GetAuroraVehicleStrForVehicleType(vType) + THLBrand.GetBrandChar(brand) + "." + vCode;
            return String.Format("{0}/CentralLibraryImages/{1}/{2}/{3}/{3}", CDNBase, aggregatorCountryAssetPath, vehicleTypeInString, vehicleImageNameConvension, vehicleImageNameConvension);
        }

        /// <summary>
        /// Get the vehicles convension
        /// </summary>
        /// <param name="countryCode"></param>
        /// <param name="brand"></param>
        /// <param name="vType"></param>
        /// <param name="vCode"></param>
        /// <returns></returns>
        public static string GetVehicleConvension(CountryCode countryCode, THLBrands brand, VehicleType vType, string vCode)
        {
            string vehicleeConvension = countryCode + GetAuroraVehicleStrForVehicleType(vType) + THLBrand.GetBrandChar(brand) + "." + vCode;
            return vehicleeConvension;
        }


        /// <summary>
        /// Get Alternate Availability Request URL parameters
        /// </summary>
        /// <param name="availabilityRequest"></param>
        /// <returns></returns>
        public static string GetAlternateAvailabilityRequestURL(AvailabilityRequest availabilityRequest)
        {

            //add cross sell multiple brands support
            string vehicleBrand = availabilityRequest.Brand.ToUpper();

            StringBuilder sb = new StringBuilder();
            sb.Append("Brand=" +  /* swapped for CSell: availabilityRequest.Brand */ vehicleBrand + "&");
            sb.Append("VehicleCode=" + availabilityRequest.VehicleModel + "&");
            sb.Append("CountryCode=" + availabilityRequest.CountryCode + "&");
            sb.Append("CheckOutZoneCode=" + availabilityRequest.PickUpBranch + "&");
            sb.Append("CheckOutDateTime=" + availabilityRequest.PickUpDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("CheckInZoneCode=" + availabilityRequest.DropOffBranch + "&");
            sb.Append("CheckInDateTime=" + availabilityRequest.DropOffDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("AgentCode=" + availabilityRequest.AgentCode + "&");
            sb.Append("PackageCode=" + availabilityRequest.PackageCode + "&");
            sb.Append("IsVan=" + (availabilityRequest.VehicleType == VehicleType.Car ? "False" : "True") + "&");
            sb.Append("NumberOfAdults=" + availabilityRequest.NumberOfAdults + "&");
            sb.Append("NoOfChildren=" + availabilityRequest.NumberOfChildren + "&");
            sb.Append("isBestBuy=True&");
            sb.Append("countryOfResidence=" + availabilityRequest.CountryOfResidence + "&");
            //if pick & drop are same no 3,6 (switch loc)
            sb.Append("DisplayMode=1,2,4,5" + (!availabilityRequest.PickUpBranch.Equals(availabilityRequest.DropOffBranch) ? ",3,6" : string.Empty) + "&");//TODO: move to config
            sb.Append("Istestmode=False&");//TODO: read from config
            return sb.ToString();
        }

        /// <summary>
        /// Normalise brand input to Aurora Format
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static string NormaliseBrandString(string brand)
        {

            string brandsStr = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(brand))
                {
                    brandsStr = brand.Length > 1 ? brand.Substring(0, 1) : brand;
                    brandsStr = brandsStr.ToUpper();
                }
                //rev:mia Sept 18, 2013 - change Z to ZZZ
                if (brandsStr.Equals("C") || brandsStr.Equals("ZZZ"))                    brandsStr = string.Empty;
                //THLDebug.LogEvent(EventTypes.MTierStatus, "Normalized Brand from " + brand + " -> " + brandsStr);  
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "AvailabilityHelper.NormaliseBrandString", ex.Message, "{brand:\"" + brand + "\"");
            }
            return brandsStr;
        }

        /// <summary>
        /// Sort Availability Items based on a sorting request
        /// </summary>
        /// <param name="items"></param>
        /// <param name="sortOrder"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static AvailabilityItem[] SortAvailabilityItems(AvailabilityItem[] items, string sortBy, string sortDirection)
        {
            //TODO: sort items
            switch (sortBy)
            {
                case "bubble":
                    BubbleSort(ref items, sortDirection);
                    break;
                case "package":
                    packageSort(ref items, sortDirection);
                    break;
            }
            return items;
        }

        /// <summary>
        /// Sort by package and its match its inclusive option 
        /// </summary>
        /// <param name="items"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static void packageSort(ref AvailabilityItem[] items, string direction)
        {

            List<AvailabilityItem> existingItems = new List<AvailabilityItem>(items);
            List<AvailabilityItem> newItems = new List<AvailabilityItem>(items);
            foreach (AvailabilityItem item in existingItems)
            {
                //Foreach existing
                //match to the rest of the list based on packagename like
            }            //pop  
            items = newItems.ToArray();
        }



        // sort the items of an array using bubble sort
        public static void BubbleSort(ref AvailabilityItem[] items, string direction)
        {
            if (items == null || items.Length < 2)
                return;

            var groups = items.GroupBy(n => n.VehicleCode);
            List<AvailabilityItem> sortedItems = new List<AvailabilityItem>();
            foreach (var group in groups)
            {
                foreach (var item in group)
                {
                    sortedItems.Add(item);
                }
            }

            if (direction.ToLower() == "desc")
            {
                sortedItems.Reverse();
            }

            items = sortedItems.ToArray();
        }


        /// <summary>
        /// Get Extra hires from Ajax parameter string 
        /// </summary>
        /// <param name="paramStr"></param>
        /// <returns></returns>
        public static ExtraHireItem[] GetItemsForParamsStr(string paramStr)
        {
            //extraHireItems[i].id + ":" + extraHireItems[i].numItems + ":" + extraHireItems[i].name + ":" + extraHireItems[i].maxCharge + ":" +  extraHireItems[i].dailyRate + ":" + promptQuant
            try
            {
                List<ExtraHireItem> ehiList = new List<ExtraHireItem>();
                string[] ehis = paramStr.TrimEnd(',').Split(',');
                foreach (string ehiParam in ehis)
                {
                    string[] itemParam = ehiParam.Split(':');
                    ExtraHireItem current = new ExtraHireItem(itemParam[1]);
                    int selectedQty = 0, maxCharge = 0;
                    decimal dailyRate;
                    bool propmpQuant;

                    decimal.TryParse(itemParam[5], out dailyRate);
                    bool.TryParse(itemParam[6], out propmpQuant);
                    int.TryParse(itemParam[4], out maxCharge);
                    int.TryParse(itemParam[2], out selectedQty);
                    current.SelectedAmount = selectedQty;
                    current.MaxCharge = maxCharge;
                    current.Name = itemParam[3];
                    current.DailyRate = dailyRate;
                    current.UOM = itemParam[7].ToLower().Equals("day") ? UOMType.Day : UOMType.None;
                    current.PromptQuantity = propmpQuant;
                    //-----------------------------------------------------------------------------------------------------
                    //rev:mia MAY 4 2013  - PART OF APCOVER ENHANCEMENT. ADDED
                    //                        'CODE' 
                    //----------------------------------------------------------------------------------------------------- 
                    try
                    {
                        current.Code = itemParam[8];
                    }
                    catch (Exception exCode) { }
                    //----------------------------------------------------------------------------------------------------- 

                    ehiList.Add(current);
                }
                return ehiList.ToArray();
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, ex.Message, "GetItemsForParamsStr", "{'paramStr':'" + paramStr + "'}");
                return null;
            }

        }



        /// <summary>
        /// Fill the Response Object from Aurora response XML
        /// </summary>
        /// <param name="responseXml">Aurora Response XML Document</param>
        /// <param name="brandCode">Querying Brand (used for error rows TODO:remove this once error comes with branding</param>
        /// <returns></returns>
        public static AvailabilityItem FillFromXML(XmlDocument responseXml, string brandCode)
        {//TODO: remove brandCode and enable 
            XmlNodeList avalabilityRowXml = responseXml.SelectNodes("data/AvailableRate");
            List<AvailabilityItem> availabilityElementsList = new List<AvailabilityItem>();
            bool success = true;
            foreach (XmlNode avalabilityRowXmlNode in avalabilityRowXml)//Each Availability Package <availability>
            {
                string avp, packageCode, packageDesc, brandCodeStr, packageComment, packageTCUrl, packageInfoURL, cki, cko;
                bool isLumpSum;
                decimal depositAmount = 0, depositPercentage = 0, estimatedTotal = 0, payableToAgent = 0, payableAtPickup = 0; ;
                //try {
                XmlNode packageNode = avalabilityRowXmlNode.SelectSingleNode("Package");//load package Info
                if (packageNode.Attributes["AvpId"] == null)
                {
                    AvailabilityItem ai = new AvailabilityItem();//string.Empty, packageCode, packageDesc, brandCodeStr,string.Empty,string.Empty,string.Empty,0.0m, 0.0m, false,string.Empty,string.Empty,0.0m,null);
                    ai.Brand = THLBrand.GetBrandForString(packageNode.Attributes["BrandCode"].Value);//enable this instead: ai.Brand = packageNode.Attributes["BrandCode"].Value;
                    XmlNode errorsNode = avalabilityRowXmlNode.SelectSingleNode("Errors");//load package Info
                    bool errorsLoaded = loadErrorMessages(ref ai, errorsNode);
                    ai.BlockingRuleMessage = errorsNode.SelectSingleNode("BlockingRuleMessage").Attributes["Message"].Value;
                    ai.ErrorMessage = errorsNode.SelectSingleNode("ErrorMessage").Attributes["Message"].Value;
                    XmlNode chargeCollection = avalabilityRowXmlNode.SelectSingleNode("Charge/Det");//load Charges
                    if (chargeCollection != null && chargeCollection.Attributes["PrdCode"].Value != null)
                        ai.VehicleCode = chargeCollection.Attributes["PrdCode"].Value;
                    if (chargeCollection != null && chargeCollection.Attributes["PrdName"].Value != null)
                        ai.VehicleName = chargeCollection.Attributes["PrdName"].Value;
                    availabilityElementsList.Add(ai);
                }
                else
                {
                    avp = packageNode.Attributes["AvpId"].Value;
                    packageCode = packageNode.Attributes["PkgCode"].Value;
                    packageDesc = packageNode.Attributes["PkgDesc"].Value;
                    brandCodeStr = packageNode.Attributes["BrandCode"].Value;
                    packageComment = packageNode.Attributes["PkgComment"].Value;
                    packageTCUrl = packageNode.Attributes["PkgTCURL"].Value;
                    packageInfoURL = packageNode.Attributes["PkgInfoURL"].Value;

                    isLumpSum = (packageNode.Attributes["IsLumpSum"].Value.Equals("Yes") ? true : false);
                    decimal.TryParse(packageNode.Attributes["EstimatedTotal"].Value, out estimatedTotal);
                    decimal.TryParse(packageNode.Attributes["PayableToAgent"].Value, out payableToAgent);
                    decimal.TryParse(packageNode.Attributes["PayableAtPickup"].Value, out payableAtPickup);

                    //get pickUp/DropOff locations
                    XmlNode locationXml = avalabilityRowXmlNode.SelectSingleNode("Location");//load Charges
                    cki = "";// locationXml.SelectSingleNode("Cki").InnerText;
                    cko = "";// locationXml.SelectSingleNode("Cko").InnerText;             
                    // catch (log and set success = false                
                    success = decimal.TryParse(packageNode.Attributes["DepositAmt"].Value, out depositAmount);
                    if (packageNode.Attributes["DepositPercentage"] != null)//TODO define optionals here
                        success = decimal.TryParse(packageNode.Attributes["DepositPercentage"].Value.Replace('%', ' '), out depositPercentage);
                    //try
                    XmlNodeList chargeCollection = avalabilityRowXmlNode.SelectNodes("Charge/Det");
                    decimal dailyAvgRate = 0;
                    List<AvailabilityItemChargeRow> aicList = new List<AvailabilityItemChargeRow>();

                    string vehicleMsg = string.Empty;

                    bool hasBonusPack = false;
                    decimal adminFee = 0.0m;

                    foreach (XmlNode chargeXml in chargeCollection)
                    {// Each Charge <Det>                                        
                        decimal.TryParse(chargeXml.Attributes["AvgRate"].Value, out dailyAvgRate);
                        AvailabilityItemChargeRow aic = new AvailabilityItemChargeRow();
                        aic.LoadFromXml(chargeXml);
                        aicList.Add(aic);
                        if (aic.AdminFee > 0) adminFee = aic.AdminFee;
                        if (aic.IsBonusPack) hasBonusPack = true;
                        if (chargeXml.Attributes["ProductInfo"] != null && chargeXml.Attributes["ProductInfo"].Value != string.Empty)
                            vehicleMsg = chargeXml.Attributes["ProductInfo"].Value;
                    }
                    AvailabilityItem ai = new AvailabilityItem(avp, packageCode, packageDesc, brandCodeStr, packageComment, packageTCUrl, packageInfoURL, depositAmount, depositPercentage, isLumpSum, cko, cki, dailyAvgRate, aicList.ToArray());
                    ai.AdminFee = adminFee;

                    ai.DisplayPromoTile = (packageNode.Attributes["DispTile"] != null && packageNode.Attributes["DispTile"].Value == "Yes");

                    ai.IsInclusive = !string.IsNullOrEmpty(packageNode.Attributes["PkgIsInclusive"].Value) && packageNode.Attributes["PkgIsInclusive"].Value.ToUpper().Equals("YES");

                    //NRMA Loyalty card support
                    ai.DiplayLoyaltyCard = (packageNode.Attributes["DispLoylCrdFld"] != null && packageNode.Attributes["DispLoylCrdFld"].Value == "Yes");

                    ai.PayableAtPickup = payableAtPickup;

                    ai.PayableToAgent = payableToAgent;

                    //Added for B2B enhancment gross support
                    if (packageNode.Attributes["CanGetGross"] != null && !string.IsNullOrEmpty(packageNode.Attributes["CanGetGross"].Value) && packageNode.Attributes["CanGetGross"].Value.ToUpper().Equals("YES"))
                        ai.CanGetGross = true;

                    //031109 
                    XmlNode onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYAU']");//TODO: get this not by code from Aurora
                    if (onewayFeeXMLNode == null)
                        onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYNZ']");//TODO: get this not by code from Aurora

                    if (onewayFeeXMLNode == null)
                        onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYE']");//TODO: get this not by code from Aurora

                    if (onewayFeeXMLNode == null)
                        onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWQ']");//TODO: get this not by code from Aurora

                    decimal onewayFee = 0.0m;
                    if (onewayFeeXMLNode != null && onewayFeeXMLNode.Attributes["Price"] != null)
                        decimal.TryParse(onewayFeeXMLNode.Attributes["Price"].Value, out onewayFee);

                    ai.OneWayFeeComponent = onewayFee;
                    //031109

                    ai.AvailabilityType = AvailabilityItemType.Available;
                    XmlNode errorsNode = avalabilityRowXmlNode.SelectSingleNode("Errors");//load package Info
                    bool errorsLoaded = errorsNode != null &&  loadErrorMessages(ref ai, errorsNode);

                    ai.IncludesBonusPack = hasBonusPack;
                    ai.VehicleMsgText = vehicleMsg;
                    ai.EstimatedTotal = estimatedTotal;

                    availabilityElementsList.Add(ai);
                }
            }
            AvailabilityItem[] availabilityElements = availabilityElementsList.ToArray();
            return availabilityElements[0];//on successful parse return true
        }


        /// <summary>
        /// Update Error Messages and set Error status type.
        /// </summary>
        /// <param name="availabilityItem"></param>
        /// <param name="errorNode"></param>
        /// <returns></returns>
        static bool loadErrorMessages(ref AvailabilityItem availabilityItem, XmlNode errorNode)
        {
            //<ErrorMessage Allow='true|false' Message='systemMsgStr'/>
            //<BlockingRuleMessage Allow='true|false' Message='MsgStr'/>
            bool hasAVPID = (availabilityItem.AVPID != null && availabilityItem.AVPID.Length > 0);
            bool allowOnBlockingRule = false, allowOnError = false;
            string errMsg = errorNode.SelectSingleNode("ErrorMessage").Attributes["Message"].Value;
            string blockingMsg = errorNode.SelectSingleNode("BlockingRuleMessage").Attributes["Message"].Value;
            bool.TryParse(errorNode.SelectSingleNode("ErrorMessage").Attributes["Allow"].Value, out allowOnError);
            bool.TryParse(errorNode.SelectSingleNode("BlockingRuleMessage").Attributes["Allow"].Value, out allowOnBlockingRule);
            bool hasErrMessage = (errMsg != null && errMsg.Length > 0);
            bool hasBlockingMessage = (blockingMsg != null && blockingMsg.Length > 0);
            int availabilityItemFlags = 0;
            availabilityItemFlags = (hasBlockingMessage ? availabilityItemFlags | 1 : availabilityItemFlags);
            availabilityItemFlags = (hasErrMessage ? availabilityItemFlags | 2 : availabilityItemFlags);
            availabilityItemFlags = (allowOnError ? availabilityItemFlags | 4 : availabilityItemFlags);
            availabilityItemFlags = (allowOnBlockingRule ? availabilityItemFlags | 8 : availabilityItemFlags);
            availabilityItemFlags = (hasAVPID ? availabilityItemFlags | 16 : availabilityItemFlags);
            switch (availabilityItemFlags)
            {
                case 16:
                    availabilityItem.AvailabilityType = AvailabilityItemType.Available;
                    break;
                case 1:
                    availabilityItem.AvailabilityType = AvailabilityItemType.BlockingRule;
                    break;
                case 6:
                    availabilityItem.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                    break;
                case 2:
                    availabilityItem.AvailabilityType = AvailabilityItemType.SystemMessage;
                    break;
                case 25:
                    availabilityItem.AvailabilityType = AvailabilityItemType.OnRequest;
                    break;
                case 9://Added for supporting Thrifty response case :
                    //<BlockingRuleMessage Allow="true" Message="Msg"/>
                    //  <ErrorMessage Allow="" Message=""/>
                    availabilityItem.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                    break;
            }
            return true;//Exp Handle and return status on failure
        }

    }
    
}