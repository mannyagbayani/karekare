﻿using System;
using System.Text.RegularExpressions;
using System.Globalization;

namespace THL.Booking
{

    public sealed class PaymentHelper
    {

        public static bool ValidatePhoneNumber(ref string phoneNumber)
        {
            if (phoneNumber != null && !Regex.Match(phoneNumber, @"^[1-9]\d{2}-[1-9]\d{2}-\d{4}$" ).Success )
            {
                 return false;
            }
            else
            {
                if (!phoneNumber.StartsWith("+"))
                    phoneNumber = "+" + phoneNumber;
                return true;
            }
        }        
        
        public static bool ValidateCard(CreditCardType cardType, string cardNumber)
        {
            byte[] number = new byte[16]; // number to validate

            // Remove non-digits
            int len = 0;
            for (int i = 0; i < cardNumber.Length; i++)
            {
                if (char.IsDigit(cardNumber, i))
                {
                    if (len == 16) return false; // number has too many digits
                    number[len++] = byte.Parse(cardNumber[i].ToString());
                }
            }

            // Validate based on card type, first if tests length, second tests prefix
            switch (cardType)
            {
                case CreditCardType.MASTERCARD:
                    if (len != 16)
                        return false;
                    if (number[0] != 5 || number[1] == 0 || number[1] > 5)
                        return false;
                    break;
                case CreditCardType.VISA:
                    if (len != 16 && len != 13)
                        return false;
                    if (number[0] != 4)
                        return false;
                    break;
                case CreditCardType.AMEX:
                    if (len != 15)
                        return false;
                    if (number[0] != 3 || (number[1] != 4 && number[1] != 7))
                        return false;
                    break;
                /* Future Reference 
                case CreditCardType.BankCard:
                    if(len != 16)
                       return false;
                    if(number[0] != 5 || number[1] != 6 || number[2] > 1)
                       return false;
                    break;

                 case CreditCardType.Discover:
                    if(len != 16)
                       return false;
                    if(number[0] != 6 || number[1] != 0 || number[2] != 1 || number[3] != 1)
                       return false;
                    break;

                 case CreditCardType.DinersClub:
                    if(len != 14)
                       return false;
                    if(number[0] != 3 || (number[1] != 0 &amp;&amp; number[1] != 6 &amp;&amp; number[1] != 8)
                       || number[1] == 0 &amp;&amp; number[2] > 5)
                       return false;
                    break;

                 case CreditCardType.JCB:
                    if(len != 16 &amp;&amp; len != 15)
                       return false;
                    if(number[0] != 3 || number[1] != 5)
                       return false;
                    break;
                */
            }
            // Use Luhn Algorithm to validate
            int sum = 0;
            for (int i = len - 1; i >= 0; i--)
            {
                if (i % 2 == len % 2)
                {
                    int n = number[i] * 2;
                    sum += (n / 10) + (n % 10);
                }
                else
                    sum += number[i];
            }
            return (sum % 10 == 0);
        }

        /// <summary>
        /// Attempt to return a currency symbol for a provided Currency Code
        /// </summary>
        /// <param name="curStr"></param>
        /// <returns>Currency Symbol or Empty string on failure </returns>
        public static string GetCurrencySymbolForISO(string curStr)
        {          
            string curChar = string.Empty;
            try
            {                
                string[] exceptionCCodes  = {"xp","fj","pg","sb","lk","to","vu","ws"};//westpac specific non standard 2 chr codes return blank
                foreach (string expCode in exceptionCCodes)
                    if (expCode.Equals(curStr.Substring(0, 2).ToLower())) return string.Empty;
                if (curStr.Equals("EUR")) {
                    curChar = "€";
                    return curChar;
                }
                RegionInfo regInfo = new RegionInfo(curStr.Substring(0, 2));


                curChar = (regInfo != null && regInfo.CurrencySymbol.Length == 1 ? regInfo.CurrencySymbol : string.Empty);//ignore non single char symbols
                        
                
                return curChar;
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                THLDebug.LogError(ErrorTypes.Application, "GetCurrencySymbolForISO", errorMsg, "{curStr:'"+ curStr +"'}");
                return curChar;
            }
        }

        /// <summary>
        /// Convert a decimal value to integer 
        /// </summary>
        /// <param name="argument"></param>
        /// <param name="callingContext">allow future support for biz context (e.g. rounding methods and enavble exp error logs</param>
        /// <returns></returns>
        public static int DecimalToInt(decimal argument, string callingContext)
        {
            int IntValue;// Convert the argument to an int value.           
            
            try
            {
                //decimal rounded = decimal.ro
                IntValue = decimal.ToInt32(argument);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;//TODO: error log using msg and errorContext params 
                IntValue = 0;
            }
            return IntValue; 
        }
    }
}