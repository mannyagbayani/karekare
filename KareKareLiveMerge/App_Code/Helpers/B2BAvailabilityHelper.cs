﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking 
{


    /// <summary>
    /// Helper Methods for B2B Availability Functionality 
    /// </summary>
    public class B2BAvailabilityHelper
    {
	    public B2BAvailabilityHelper()
	    {
		
	    }

        //TODO: move this into the session manager class
        public static bool SessionExpired
        {
            //definition of session expiry
            get
            {  
                return (HttpContext.Current.Session["POS"] == null);
            }
        }


        public static string GetAssetsURL()
        {
            string assetBasePath = System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
            return assetBasePath;
        }


        /// <summary>
        /// Filter out the relevat packages for the provided brand (within Agent Package collection)
        /// </summary>
        /// <param name="packages"></param>
        /// <param name="brandStr"></param>
        /// <returns></returns>
        public static DAL.PackageType[] GetPackagesForBrand(DAL.PackageType[] packages, string brandStr)
        {
            brandStr = brandStr.ToUpper();
            List<DAL.PackageType> pkgList = new System.Collections.Generic.List<DAL.PackageType>();
            foreach (DAL.PackageType pkg in packages)
            {
                string[] pkgParams = pkg.VehType.Split(',');
                if (pkgParams[1].Equals(brandStr))
                    pkgList.Add(pkg);
            }
            return pkgList.ToArray();
        }

        public static THLPackage[] GetPackagesForCountryBrand(THLPackage[] packages, string brandStr, string countryStr)
        {
            brandStr = brandStr.ToUpper();
            countryStr = countryStr.ToUpper();
            List<THLPackage> pkgList = new List<THLPackage>();
            foreach (THLPackage pkg in packages)
            {
                if (pkg.brand.Equals(brandStr) && pkg.country.Equals(countryStr))
                    pkgList.Add(pkg);
            }
            return pkgList.ToArray();
        }
    }
}