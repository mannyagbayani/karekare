﻿using System;
using System.Text;

namespace THL.Booking
{

    public enum CountryCode
    {
        NONE = 0,
        AU = 1,
        NZ = 2,
        US = 3,
        CA = 4
    }

    /// <summary>
    /// Branch Entity, Consider refactoring
    /// </summary>
    public class Branch : Coordinate
    {
        public string BranchCode;
        public string OpeningHours;
        public String Name;
        public int MarkerId;
        public string ThumbImageURL;
        public string Description;
        public string PhoneNumber;
        public string FreePhoneNumber;
        public string FaxNumber;
        public string Address;
        public string Address2;
        public string DistanceFromAirport;
        public string BranchTransfers;
        public string City;
        public string CountryCode;//TODO: deprecate
        public int CityId;
        public decimal Lon;
        public decimal Lat;
        public int Type;//1= cars, 2=campers, 3=both;
        public bool PickUpOnly;
        public VehicleType VehicleTypeAvailability;
        public CountryCode country;
        public THLBrands[] brands;
        public string ZoneCode;
        public string ZoneName;
        
        public Branch(Coordinate coord)
        {
            //Lon = coord.Lon;
            //Lat = coord.Lat;        
        }

        public Branch()
        {

        }

        public string GetAddress() 
        {
            string addressStr = string.Empty;
            addressStr += (Address != null && Address.Length > 0 ? Address : string.Empty);
            addressStr += (Address2 != null && Address2.Length > 0 ? ", " + Address2 : string.Empty);
            addressStr += (ZoneName != null && ZoneName.Length > 0 ? "  " + ZoneName : string.Empty);
            return addressStr;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append("Name:'"+ BranchCode +"',");
            sb.Append("Code:'"+ Name +"'");
            sb.Append("}");
            return sb.ToString();
        }

    }
}