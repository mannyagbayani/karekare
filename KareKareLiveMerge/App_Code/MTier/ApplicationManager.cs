﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// Application Level Data and Configuration Info Manager
    /// </summary>
    public class ApplicationManager
    {
        public static ApplicationData GetApplicationData()
        {
            //TODO: verify and init if not initialised..
            bool debugOn = false;
            bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["AppCacheOff"], out debugOn);
            if (debugOn)
                return new ApplicationData();
            else
                return (ApplicationData)HttpContext.Current.Application["ApplicationData"];        
        }
        
        public ApplicationManager()
        {

        }

        /// <summary>
        /// return a Cached DPS account for a provided AgentCode
        /// </summary>
        /// <param name="agentCode"></param>
        /// <returns></returns>
        public static DPSAccount GetDPSAccountForAgentCode(string agentCode, CountryCode countryCode)
        {
            ApplicationData appData = GetApplicationData();
            if(agentCode.Equals("AIRNZB2C"))
                agentCode = (countryCode == CountryCode.NZ ? "B2CNZ" : "B2CAU" );//TODO: from config

            if (agentCode.Equals("MACCOM"))
                agentCode = (countryCode == CountryCode.NZ ? "B2CNZ" : "B2CAU");//TODO: define MAC payment rules
            
            return appData.GetDPSAccountForAgentCode(agentCode);
        }


        /// <summary>
        /// Get Cached Content Elements
        /// </summary>
        /// <returns></returns>
        public static ContentElement[] GetContentElements()
        { 
            ApplicationData appData = GetApplicationData();
            return appData.ContentElements;
        }

        /// <summary>
        /// Get MaC Specific Cached Content Elements added for aggregator support (21/12/09)
        /// </summary>
        /// <returns></returns>
        public static ContentElement[] GetMACContentElements()
        {
            ApplicationData appData = GetApplicationData();
            return appData.MACContentElements;
        }

        /// <summary>
        /// Return the parend domain for a cached THL Domain
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        public static string GetParentSiteForBrand(THLBrands brand, CountryCode country)
        { 
            ApplicationData appData = GetApplicationData();
            foreach (THLDomain domain in appData.Domains)
            {
                if (domain.Brand == brand && (country == CountryCode.NONE || domain.Country == country))
                    return domain.ParentSite;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Cached Affeliate Element
        /// </summary>
        /// <returns></returns>
        public static Affiliate GetAffeliateForCode(string code)
        {
            ApplicationData appData = GetApplicationData();
            return appData.GetAffiliateForCode(code);
        }
        
        public static CurrencyManager GetCurrencyManager()
        {
            CurrencyManager current = (CurrencyManager)HttpContext.Current.Application["CurrencyManager"];
            if (current != null && current.LastUpdated.AddDays(-1) > DateTime.Now)            
                return current;
            else
                return new CurrencyManager(CurrencyConversionProvider.BNZ, CurrencyCode.NZD);
        } 

        /// <summary>
        /// Get Vehicles
        /// </summary>
        /// <returns></returns>
        public static Vehicle[] GetVehicles()
        {
            ApplicationData appData = GetApplicationData();
            return appData.Vehicles;
        }

        public static Vehicle[] GetMacVehicles()
        {
            ApplicationData appData = GetApplicationData();
            return appData.MacVehicles;
        }
        
        public static TrackingData GetTrackingElementForDomainEvent(string domainName, string eventName)
        {
            ApplicationData appData = GetApplicationData();
            return appData.GetTrackingElement(domainName, eventName);
        }


        public static string HTTPPrefix
        {
            get
            {
                bool isSecure = false;
                bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out isSecure);
                return (isSecure ? "https://" : "http://");            
            }
        }

        /// <summary>
        /// Return the THL domain instance for the oprovided URL 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static THLDomain GetDomainForURL(string url)
        {
            ApplicationData appData = GetApplicationData();
            foreach (THLDomain domain in appData.Domains)
            {
                if (domain.Name.Contains( url) || domain.Name.Contains("www." + url))
                    return domain;            
            }
            return null;
        }

    }
}