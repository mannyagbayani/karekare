﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Xml;
using System.Text;

namespace THL.Booking
{

    /// <summary>
    /// Introduced to handle the new Fee webservice section returned within RetrieveBooking for quotes
    /// TODO: refactor into model structure once finalised
    /// </summary>
    public class BookingFee
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Class { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal GrossCalcPercentage { get; set; }
        public bool IsPercentageFee { get; set; }
        public int HirePeriod { get; set; }
        public string BpdId { get; set; }
        public bool Loaded { get; set; }

        public BookingFee(XmlNode FeeXMLNode)
        {
            Loaded = false;
            try
            {
                decimal grossAmt = 0.0m;
                Name = FeeXMLNode.Attributes["Name"].Value;
                ShortName = FeeXMLNode.Attributes["ShortName"].Value;
                Class = FeeXMLNode.Attributes["Class"].Value;
                //TODO: can fees by 0.0 otherwise set loaded to false
                decimal.TryParse(FeeXMLNode.Attributes["GrossAmount"].Value, out grossAmt);
                decimal feePercentage = 0.0m;
                IsPercentageFee = (decimal.TryParse(FeeXMLNode.Attributes["GrossCalcPercentage"].Value, out feePercentage) && feePercentage > 0);
                GrossAmount = grossAmt;
                GrossCalcPercentage = feePercentage;
                BpdId = FeeXMLNode.Attributes["BpdId"].Value;
                Loaded = true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: log failed fee load from web service data.
                Loaded = false;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{Name:" + Name);
            sb.Append(",ShortName:" + ShortName);
            sb.Append(",Class:" + Class);
            sb.Append(",GrossAmount:" + GrossAmount);
            sb.Append(",BpdId:" + BpdId);
            sb.Append("}");
            return sb.ToString();
        }
    }

    /// <summary>
    /// Quote Response Status
    /// </summary>
    public enum QuoteStatus { 
        Expired = 0,
        NoAvailability = 1,
        Retrieved = 2    
    }

    /// <summary>
    /// Aurora Quote Class for prebooked quotes
    /// </summary>
    public class AuroraQuote
    {
        public string DebugStr;

        public string PickUpLoc, DropOffLoc;

        private string bookingID;
        /// <summary>
        /// The Quotes ID
        /// </summary>
        public string BookingID { get { return bookingID; } }
        
        private AuroraBooking bookingInfo;

        /// <summary>
        /// Encapsulated Booking Instance for backward compatibility
        /// </summary>
        public AuroraBooking BookingData
        {
            get { return bookingInfo; }
            set { bookingInfo = value; }
        }

        /// <summary>
        /// Successful Transaction data
        /// </summary>
        public Transaction transaction { get; set; }

        public string CurrencyCode { get; set; }

        public string VehicleName;
        public ExtraHireItem[] SelectedExtraHireItems { get; set; }
        public ExtraHireItem[] AddedExtraHireItems { get; set; }
        public ExtraHireItem[] AvailableExtraHireItems { get; set; }

        public FerryProduct[] FerryProducts { get; set; }
        public string FerryNote { get; set; }

        public CreditCardElement[] AvailableCards { get; set; }

        public decimal VehicleCharge { get; set; }

        //TODO: encapsulate and process once rate bands returned from WS
        public XmlNode RentalInfoNode;

        public InsuranceProduct[] SelectedInsurance { get; set; }

        public InsuranceProduct[] AddedInsurance { get; set; }

        public bool InclusiveSelected { get {             
                bool inclusiveSelected = ( SelectedInsurance != null && SelectedInsurance.Length > 0 && SelectedInsurance[0].IsInclusive);
                //TODO: check added ins as well for inclusive here..
                if (AddedInsurance != null && AddedInsurance.Length == 1 && AddedInsurance[0].IsInclusive) inclusiveSelected = true;
                return inclusiveSelected;       
            } 
        }

        public InsuranceProduct[] AvailableInsurance { get; set; }

        /// <summary>
        /// Vehicle Related Fees
        /// </summary>
        public BookingFee[] EnforcedFees { get; set; }
         
        /// <summary>
        /// Customer Information
        /// </summary>
        Customer quotedCustomer;

        /// <summary>
        /// Return admin fee component percentage if present in this quote
        /// </summary>
        public decimal AdminFee
        {
            get
            {
                foreach (BookingFee fee in EnforcedFees)
                    if (fee.ShortName.Equals("STADMIN")) return fee.GrossCalcPercentage;
                return 0.0m;
            }
        }

        /// <summary>
        /// Initiate an Avaliability Request object for new searches and request info data
        /// </summary>
        AvailabilityRequest availRequest;

        /// <summary>
        /// Returns the Request that generated this quote
        /// </summary>
        public AvailabilityRequest Request
        {
            get { return availRequest; }
        }

        public Customer GetCustomerData()
        {
            return bookingInfo.CustomerInfo;
        }

        public string AgentCode { get; set; }

        public DateTime ExpiryDate { get; set; }

        public string TaCLink { get; set; } 

        public XmlNode RateBandsXML;//This could be refactored and encapsulated once finalised

        public QuoteStatus Status { get; set; }

        /// <summary>
        /// Get The original Quotes Booking Products
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        public AuroraProduct[] GetBookingProducts(ProductType productType)
        {
            return bookingInfo.GetBookingProducts(productType);
        }

        /// <summary>
        /// The Required deposit for securing the booking
        /// </summary>
        public decimal RequiredDeposit { get; set; }

        public decimal RequiredDepositPer { get; set; }

        public AuroraQuote(XmlDocument selectedQuoteItems, XmlDocument quotePackageXml)
        {
            string countryCode = string.Empty;
            XmlNode rentalInfoXML = selectedQuoteItems.SelectSingleNode("//RentalInfo/Rental");
            if (rentalInfoXML != null)
            {                
                //Load the Availability Request from The Retrieve Quote Service data(for regenerating the request) 
                availRequest = new AvailabilityRequest();
                availRequest.LoadFromQuoteRentalInfo(selectedQuoteItems);

                EnforcedFees = GetRentalFeesForXMLNodes(selectedQuoteItems.SelectNodes("//Fee/InsuranceOption"));
                RentalInfoNode = rentalInfoXML;//This needs to be broken into rate bands
                bookingID = rentalInfoXML.Attributes["BookingNumber"].Value;
                DateTime expiryDate = DateTime.Now.AddDays(7);//TODO: biz logic for default if any, uses the provided below
                DateTime.TryParse(rentalInfoXML.Attributes["RntExpiryDate"].Value, out expiryDate);
                ExpiryDate = expiryDate;
                CurrencyCode = rentalInfoXML.Attributes["CurrencyCode"] != null ? rentalInfoXML.Attributes["CurrencyCode"].Value : string.Empty;
                availRequest.VehicleName = rentalInfoXML.Attributes["VehicleName"] != null ? rentalInfoXML.Attributes["VehicleName"].Value : string.Empty;
                VehicleName = availRequest.VehicleName;//TODO: refactor?
                decimal vehicleCharge = 0.0m;
                decimal.TryParse(rentalInfoXML.Attributes["VehicleCharge"].Value, out vehicleCharge);
                VehicleCharge = vehicleCharge;
                AgentCode = rentalInfoXML.Attributes["AgentCode"].Value;
                countryCode = rentalInfoXML.Attributes["CountryCode"].Value;

                PickUpLoc = rentalInfoXML.Attributes["PickupLocName"].Value;
                DropOffLoc = rentalInfoXML.Attributes["DropOffLocName"].Value;

                TaCLink = rentalInfoXML.Attributes["PackageTandCURL"].Value;

                decimal requiredDepositPer = 0.2m, requiredDeposit = 0.0m;//define defaults?
                if (decimal.TryParse(rentalInfoXML.Attributes["DepositPercentage"].Value.Replace("%", ""), out requiredDepositPer))
                    RequiredDepositPer = requiredDepositPer;
                if (decimal.TryParse(rentalInfoXML.Attributes["DepositAmt"].Value, out requiredDeposit))
                    RequiredDeposit = requiredDeposit;

            }//TODO: if this fails inform stack
            Status = getQuoteStatus(selectedQuoteItems);

            if (Status != QuoteStatus.Retrieved)
                return;//Do not load further

            //load customer data into bookingInfo            
            string customerTitle = selectedQuoteItems.SelectSingleNode("//CustomerDetails/Title").InnerText;
            string customerFName = selectedQuoteItems.SelectSingleNode("//CustomerDetails/FirstName").InnerText;
            string customerLName = selectedQuoteItems.SelectSingleNode("//CustomerDetails/LastName").InnerText;
            string customerEmail = selectedQuoteItems.SelectSingleNode("//CustomerDetails/Email").InnerText;
            string customerPhone = selectedQuoteItems.SelectSingleNode("//CustomerDetails/PhDayNumber").InnerText;
            quotedCustomer = new Customer(Customer.GetTitleTypeForString(customerTitle), customerFName, customerLName, customerEmail, customerPhone);

            //Load Insurance todo: this need to be customised to handle the quote package attributes as web service is different schema
            SelectedInsurance = GetInsuranceOptionsForXMLNodes(selectedQuoteItems.SelectNodes("//InsuranceOptions/InsuranceOption"), true);
            AvailableInsurance = GetInsuranceOptionsForXMLNodes(quotePackageXml.SelectNodes("//Insurence/Product"), false);//get available insurance for this package
             
            //Load Extra Hires
            SelectedExtraHireItems = GetHireItemsForXMLNodes(selectedQuoteItems.SelectNodes("//ExtraHireItems/ExtraHireItem"), false);
            AvailableExtraHireItems = GetHireItemsForXMLNodes(quotePackageXml.SelectNodes("//ExtraHireItems/Product"), true);//Load possible extra hires for this relevant package          

            AvailableCards = getAvailableCards(quotePackageXml.SelectNodes("data/ConfigurationProducts/Surcharge/Product"));

            RateBandsXML = selectedQuoteItems.SelectSingleNode("//RentalInfo/RateBands");//TODO: create a rate band collection + Discounts for later consumption by Displayer
            
            bookingInfo = new AuroraBooking(quotedCustomer, SelectedExtraHireItems);//TODO: consider refactoring           
            bookingInfo.CountryCode = AvailabilityHelper.GetCountryCodeForString(countryCode);
            //Load Ferry Products            
            FerryProducts = getFerryProducts(quotePackageXml.SelectNodes("data/ConfigurationProducts/FerryCrossing/Product"));
            FerryProductDisplayer fpd = new FerryProductDisplayer(FerryProducts, Request.PickUpDate, Request.DropOffDate, this.CurrencyCode);            
        }

        /// <summary>
        /// This Method assumes a web service response without the <Error> Node is a Valid and Available response
        /// based on the provided WS schema, additional options will need updateting here
        /// </summary>
        /// <param name="quoteXml"></param>
        /// <returns></returns>
        public QuoteStatus getQuoteStatus(XmlDocument quoteXml)
        {
            QuoteStatus status = QuoteStatus.Retrieved;
            XmlNode errorMessageNode = quoteXml.SelectSingleNode("//Error/Details");
            if (errorMessageNode != null && !String.IsNullOrEmpty(errorMessageNode.InnerText))
            {
                switch (errorMessageNode.InnerText)
                {
                    case "This quote has expired":
                        status = QuoteStatus.Expired;
                        break;
                    case "This quote cannot be processed due to vehicle unavailability"://TODO: get exact text
                        status = QuoteStatus.NoAvailability;
                        break;
                    default:
                        status = QuoteStatus.NoAvailability;
                        break;
                }                
            }
            return status;        
        }


        public bool ModifyQuote(NameValueCollection requestParams, string ferryNote, NameValueCollection ferryTimes)
        {           
            StringBuilder sb = new StringBuilder();//debug remove
            List<ExtraHireItem> selectedHiresList = new List<ExtraHireItem>();
            List<InsuranceProduct> selectedInsProductsList = new List<InsuranceProduct>();
            Dictionary<string, ExtraHireItem> availHireDic = new List<ExtraHireItem>(AvailableExtraHireItems).ToDictionary(p => p.SapId);
            foreach (string key in requestParams.AllKeys)
            {
                if (key.Contains("ehi") && !requestParams[key].Equals("0"))
                {
                    string currentSap = key.Split('_')[1];
                    if (availHireDic.ContainsKey(currentSap))
                    {
                        int itemCounter = 0;
                        string counterStr = (requestParams[key].ToLower().Equals("on") ? "1" : requestParams[key]);//handle cboxes
                        int.TryParse(counterStr, out itemCounter);//TODO: handle failure if required
                        ExtraHireItem currentHire = new ExtraHireItem(//Cloning method needed
                            availHireDic[currentSap].Code,
                            availHireDic[currentSap].Name, availHireDic[currentSap].DailyRate, availHireDic[currentSap].UOM,
                            availHireDic[currentSap].GrossAmount, availHireDic[currentSap].MaxCharge,
                            availHireDic[currentSap].SapId, availHireDic[currentSap].DisplayOrder,
                            availHireDic[currentSap].PromptQuantity, availHireDic[currentSap].MinAge, availHireDic[currentSap].MaxAge);
                        currentHire.HirePeriod = (currentHire.HirePeriod == 0 ? 1 : currentHire.HirePeriod);//handle hireporiod 0
                        currentHire.Quantity = itemCounter;
                        currentHire.GrossAmount = (currentHire.UOM == UOMType.Day) ? itemCounter * currentHire.GrossAmount * currentHire.HirePeriod : itemCounter * currentHire.GrossAmount;
                        currentHire.GrossAmount = currentHire.GrossAmount > currentHire.MaxCharge && currentHire.MaxCharge > 0 ? currentHire.MaxCharge : currentHire.GrossAmount;
                        currentHire.TotalCharge = currentHire.DailyRate;
                        selectedHiresList.Add(currentHire);
                    }
                }
                else if (key.Equals("insuranceOption") || key.Contains("ipi_"))
                {
                    
                    List<string> insItems = new List<string>();
                    
                    if (SelectedInsurance != null && SelectedInsurance.Length == 1 && SelectedInsurance[0].IsInclusive)//TODO: if inclusive is selected add it mannually here,biz logic at the front
                        selectedInsProductsList.Add(SelectedInsurance[0]);                    
                    
                    if (requestParams[key].Contains(","))
                        insItems = new List<string>(requestParams[key].Split(','));
                    else
                        insItems.Add(requestParams[key]);
                    
                    foreach (string insStr in insItems.ToArray())
                    {
                        
                        foreach (InsuranceProduct ins in AvailableInsurance)
                        {
                            string currentSap = insStr.Split('_')[1];
                            if (ins.SapId.Equals(currentSap))
                                selectedInsProductsList.Add(ins);
                        }
                    }                   
                }
            }
            AddedExtraHireItems = selectedHiresList.ToArray();//TODO: consider dedicating a collction for this (support added/removed on submit)
            AddedInsurance = selectedInsProductsList.ToArray();//TODO: consider dedicating a collction for this (support added/removed on submit)

            //Reset Pax Conunters for ferry products
            if (FerryProducts.Length != 0 && FerryProducts[1] != null && FerryProducts[1].FerryData.PersonRateCollection != null && FerryProducts[1].FerryData.PersonRateCollection.Length == 3)
            {
                FerryProducts[0].Quantity = 0;
                FerryProducts[1].FerryData.PersonRateCollection[0].NumPassengers = 0;
                FerryProducts[1].FerryData.PersonRateCollection[1].NumPassengers = 0;
                FerryProducts[1].FerryData.PersonRateCollection[2].NumPassengers = 0;
            }

            StringBuilder noteBuilder = new StringBuilder();
            if (ferryTimes != null && ferryTimes.Count > 0 && !String.IsNullOrEmpty(ferryNote))
            {//add a ferry components
                FerryProduct VehicleProduct = FerryProducts[0];
                
                FerryProduct PaxProduct = null;
                Dictionary<string, PersonRateInfo> paxData = new Dictionary<string, PersonRateInfo>();
                NameValueCollection origins = new NameValueCollection();
                if (FerryProducts.Length > 1)
                {
                    PaxProduct = FerryProducts[1];//PersonRateInfo
                    paxData = new List<PersonRateInfo>(FerryProducts[1].FerryData.PersonRateCollection).ToDictionary(p => p.PersonInfoType.ToString());
                }               
                
                if (!String.IsNullOrEmpty(ferryNote))
                {
                    string[] ferryParams = ferryNote.TrimEnd(',').Split(',');                   
                    foreach (string ferryParam in ferryParams)//TODO:
                    {//ferry note:
                        int counter = 0;
                        string[] productLine = ferryParam.Split('_');
                        string origin = productLine[0];
                        if (ferryTimes[origin] != null)
                            origins[origin] = origin;
                        string dest = productLine[1];
                        string paxType = productLine[2];
                        string currentSap = productLine[3].Split(':')[0];
                        int.TryParse(productLine[3].Split(':')[1], out counter);
                        paxData[paxType].NumPassengers = paxData[paxType].NumPassengers + counter;
                        noteBuilder.Append(counter + " " + (origin.Equals("pct") ? "Picton to Wellington " : "Wellington to Picton ") +  paxType + ", ");
                        string route = (origin.Equals("pct") ? "pct-wlg" : "wlg-pct");
                        //noteBuilder.Append(paxType + "s:" + counter + "("+ route +"),");
                    
                    }
                    FerryProducts[0].Quantity = origins.Count;
                    FerryProducts[1].FerryData.PersonRateCollection = paxData.Values.ToArray();
                }//(WLG-PCT crossing date :16032011 9:00(Adults:1, Childs:2, Infants:3)PCT-WLG crossing date :16032011 9:00(Adults:4, Childs:5, Infants:6))</
                foreach (string org in origins.AllKeys)//biz logic in model, TODO: refactor
                {
                    switch (org)
                    {
                        case "wlg":
                            noteBuilder.Append(" One Vehicle Ferry from Wellington to Picton at " + ferryTimes["wlg"] + ",");
                            //noteBuilder.Append("WLG-PCT crossing date :" + ferryTimes["wlg"] + ",");
                            FerryNote += " One Vehicle Ferry from Wellington to Picton at " + ferryTimes["wlg"] + ",";
                            break;
                        case "pct":
                            noteBuilder.Append(" One Vehicle Ferry from Wellington to Picton at " + ferryTimes["pct"] + ",");
                            //noteBuilder.Append("PCT-WLG crossing date :" + ferryTimes["pct"] + ",");
                            FerryNote += " One Vehicle Ferry from Wellington to Picton at " + ferryTimes["pct"] + ",";
                            break;
                    }
                }

                string paxNote = string.Empty;
                foreach (PersonRateInfo person in FerryProducts[1].FerryData.PersonRateCollection)
                {
                    if (person.NumPassengers > 0)
                        paxNote += person.NumPassengers + " " + person.PersonInfoType + " Crossings ,";
                }
                if (!String.IsNullOrEmpty(FerryNote))
                    FerryNote += paxNote;
            }

            FerryNote = noteBuilder.ToString().TrimEnd(',');
            return true;
        }
        
        ExtraHireItem[] GetHireItemsForXMLNodes(XmlNodeList ehiXMLNodes, bool quoteMode)
        {
            List<ExtraHireItem> ehiList = new List<ExtraHireItem>();
            if (quoteMode)
            {
                foreach (XmlNode ehiNode in ehiXMLNodes)
                {
                    bool promptForQuantity = false;
                    string extraHireSapId = ehiNode.Attributes["SapId"].Value;
                    string extraHireName = ehiNode.Attributes["Name"].Value;
                    string noteText = (ehiNode.Attributes["PrdInfo"] != null ? ehiNode.Attributes["PrdInfo"].Value : string.Empty);
                    decimal dailyRate = 0.0m, grossAmt = 0.0m, maxCharge = 0.0m;
                    int displayOrder = 0, minAge = 0, maxAge = 0;
                    UOMType ehiUOMType = (ehiNode.Attributes["UOM"] != null ? AvailabilityHelper.UOMFromString(ehiNode.Attributes["UOM"].Value) : UOMType.None);
                    promptForQuantity = ehiNode.Attributes["PromptQty"] != null && ehiNode.Attributes["PromptQty"].Value.ToUpper().Equals("YES");
                    decimal.TryParse(ehiNode.Attributes["dailyRate"].Value, out dailyRate);
                    decimal.TryParse(ehiNode.Attributes["GrossAmt"].Value, out grossAmt);
                    decimal.TryParse(ehiNode.Attributes["MaxCharge"].Value, out maxCharge);
                    int.TryParse(ehiNode.Attributes["DisplayOrder"].Value, out displayOrder);
                    int.TryParse(ehiNode.Attributes["MinAge"].Value, out minAge);
                    int.TryParse(ehiNode.Attributes["MaxAge"].Value, out minAge);
                    ExtraHireItem ehi = new ExtraHireItem(extraHireSapId, extraHireName, dailyRate, ehiUOMType, grossAmt, maxCharge, extraHireSapId, displayOrder, promptForQuantity, minAge, maxAge);
                    ehi.Note = noteText;
                    ehiList.Add(ehi);
                }
                ehiList.Sort(delegate(ExtraHireItem ehi1, ExtraHireItem ehi2) { return ehi1.DisplayOrder.CompareTo(ehi2.DisplayOrder); });
            }
            else //Load Selected Extra Hires
            {
                foreach (XmlNode ehiNode in ehiXMLNodes)
                {
                    bool promptForQuantity = false;
                    string extraHireSapId = ehiNode.Attributes["SapId"].Value;
                    string extraHireName = ehiNode.Attributes["Name"].Value;
                    string noteText = (ehiNode.Attributes["PrdInfo"] != null ? ehiNode.Attributes["PrdInfo"].Value : string.Empty);
                    decimal dailyRate = 0.0m, grossAmt = 0.0m, maxCharge = 0.0m;
                    int displayOrder = 0, minAge = 0, maxAge = 0, quantity = 0;
                    UOMType ehiUOMType = (ehiNode.Attributes["UOM"] != null ? AvailabilityHelper.UOMFromString(ehiNode.Attributes["UOM"].Value) : UOMType.None);
                    bool.TryParse(ehiNode.Attributes["PromptForQuantity"].Value, out promptForQuantity);
                    decimal.TryParse(ehiNode.Attributes["Rate"].Value, out dailyRate);
                    decimal.TryParse(ehiNode.Attributes["GrossAmount"].Value, out grossAmt);
                    decimal.TryParse(ehiNode.Attributes["MaxCharge"].Value, out maxCharge);
                    int.TryParse(ehiNode.Attributes["DisplayOrder"].Value, out displayOrder);
                    int.TryParse(ehiNode.Attributes["MinAge"].Value, out minAge);
                    int.TryParse(ehiNode.Attributes["MaxAge"].Value, out minAge);
                    int.TryParse(ehiNode.Attributes["Quantity"].Value, out quantity);
                    ExtraHireItem ehi = new ExtraHireItem(extraHireSapId, extraHireName, dailyRate, ehiUOMType, grossAmt, maxCharge, extraHireSapId, displayOrder, promptForQuantity, minAge, maxAge);
                    ehi.BpdId = (ehiNode.Attributes["BpdId"] != null && !String.IsNullOrEmpty(ehiNode.Attributes["BpdId"].Value) ? ehiNode.Attributes["BpdId"].Value : string.Empty);
                    ehi.Quantity = quantity;
                    ehi.Note = noteText;
                    ehiList.Add(ehi);
                }
                ehiList.Sort(delegate(ExtraHireItem ehi1, ExtraHireItem ehi2) { return ehi1.DisplayOrder.CompareTo(ehi2.DisplayOrder); });
            }
            return ehiList.ToArray();
        }

        /// <summary>
        /// Process Web Service XML Node list into Insurance Product Array
        /// </summary>
        /// <param name="insXMLNodes"></param>
        /// <param name="isQuoteService"></param>
        /// <returns></returns>
        InsuranceProduct[] GetInsuranceOptionsForXMLNodes(XmlNodeList insXMLNodes, bool isQuoteService)
        {
            List<InsuranceProduct> insList = new List<InsuranceProduct>();
            if (isQuoteService)
            {
                foreach (XmlNode insNode in insXMLNodes)
                {
                    string waivedItems = (insNode.SelectSingleNode("Inclusive") != null && !string.IsNullOrEmpty(insNode.SelectSingleNode("Inclusive").InnerText) ? insNode.SelectSingleNode("Inclusive").InnerText : string.Empty);
                    bool promptForQuantity = false, isInclusive = false;
                    string extraHireSapId = insNode.Attributes["SapId"].Value;
                    string extraHireCode = insNode.Attributes["ShortName"].Value;
                    string extraHireName = insNode.Attributes["Name"].Value;
                    decimal dailyRate = 0.0m, grossAmt = 0.0m, maxCharge = 0.0m, hirePeriod = 0.0m;
                    int displayOrder = 0, minAge = 0, maxAge = 0, priceRating = 0;
                    UOMType ehiUOMType = (insNode.Attributes["UOM"] != null ? AvailabilityHelper.UOMFromString(insNode.Attributes["UOM"].Value) : UOMType.None);
                    bool.TryParse(insNode.Attributes["PromptForQuantity"].Value, out promptForQuantity);
                    if (insNode.Attributes["PrdIsInclusive"] != null) 
                        isInclusive = insNode.Attributes["PrdIsInclusive"].Value.ToUpper().Equals("TRUE");//possibly parse bool                                          
                    string noteText = (insNode.Attributes["NoteText"] != null ? insNode.Attributes["NoteText"].Value : string.Empty);
                    int.TryParse(insNode.Attributes["Rating"].Value, out priceRating);
                    decimal.TryParse(insNode.Attributes["ProductHirePeriod"].Value, out hirePeriod);
                    decimal.TryParse(insNode.Attributes["Rate"].Value, out dailyRate);
                    decimal.TryParse(insNode.Attributes["GrossAmount"].Value, out grossAmt);
                    decimal.TryParse(insNode.Attributes["MaxCharge"].Value, out maxCharge);
                    int.TryParse(insNode.Attributes["DisplayOrder"].Value, out displayOrder);
                    int.TryParse(insNode.Attributes["MinAge"].Value, out minAge);
                    int.TryParse(insNode.Attributes["MaxAge"].Value, out maxAge);
                    InsuranceProduct insItem = new InsuranceProduct(extraHireCode, extraHireName, dailyRate, ehiUOMType, grossAmt, maxCharge, extraHireSapId, displayOrder, promptForQuantity, minAge, maxAge);
                    insItem.BpdId = (insNode.Attributes["BpdId"] != null && !String.IsNullOrEmpty(insNode.Attributes["BpdId"].Value) ? insNode.Attributes["BpdId"].Value : string.Empty);

                    XmlNodeList actionNodes = insNode.SelectNodes("RequiredActions/Action");
                    if (actionNodes != null && actionNodes.Count > 0)//has action dependencies
                    {
                        insItem.RequiredActions = new NameValueCollection();
                        foreach (XmlNode xmlNode in actionNodes)//<Action code="DISPLAYWHEN" PrdCode="ER2" />                         
                            insItem.RequiredActions.Add(xmlNode.Attributes["PrdCode"].Value, xmlNode.Attributes["code"].Value);
                    }
                    insItem.IsInclusive = isInclusive;
                    insItem.Note = noteText;
                    insItem.HirePeriod = hirePeriod;

                    if (isInclusive && insNode.SelectSingleNode("Note") != null && insNode.SelectSingleNode("Note").Attributes["NteDesc"] != null)
                        noteText = insNode.SelectSingleNode("Note").Attributes["NteDesc"].Value;//Inclusive is embedded node ;o(

                    insItem.Rating = priceRating;
                    insItem.BpdId = (insNode.Attributes["BpdId"] != null && !String.IsNullOrEmpty(insNode.Attributes["BpdId"].Value) ? insNode.Attributes["BpdId"].Value : string.Empty);
                    insItem.WaivedProducts = waivedItems;
                    insList.Add(insItem);
                }
                insList.Sort(delegate(InsuranceProduct ins1, InsuranceProduct ins2) { return ins1.Rating.CompareTo(ins2.Rating); });
                return insList.ToArray();
            }
            else
            {
                foreach (XmlNode insNode in insXMLNodes)
                {
                    bool promptForQuantity = false, isInclusive = false;
                    string waivedItems = (insNode.SelectSingleNode("Inclusive") != null && !string.IsNullOrEmpty(insNode.SelectSingleNode("Inclusive").InnerText) ? insNode.SelectSingleNode("Inclusive").InnerText : string.Empty);
                    string extraHireSapId = insNode.Attributes["SapId"].Value;
                    string extraHireCode = insNode.Attributes["Code"].Value;
                    string extraHireName = insNode.Attributes["Name"].Value;
                    decimal dailyRate = 0.0m, grossAmt = 0.0m, maxCharge = 0.0m, hirePeriod = 0.0m;
                    int displayOrder = 0, minAge = 0, maxAge = 0, priceRating = 0;
                    UOMType ehiUOMType = (insNode.Attributes["UOM"] != null ? AvailabilityHelper.UOMFromString(insNode.Attributes["UOM"].Value) : UOMType.None);
                    bool.TryParse(insNode.Attributes["PromptQty"].Value, out promptForQuantity);
                    if (insNode.Attributes["IsInclusive"] != null) isInclusive = insNode.Attributes["IsInclusive"].Value.ToUpper().Equals("YES");
                    decimal.TryParse(insNode.Attributes["dailyRate"].Value, out dailyRate);
                    decimal.TryParse(insNode.Attributes["GrossAmt"].Value, out grossAmt);
                    decimal.TryParse(insNode.Attributes["MaxCharge"].Value, out maxCharge);
                    decimal.TryParse(insNode.Attributes["HirePeriod"].Value, out hirePeriod);
                    int.TryParse(insNode.Attributes["DisplayOrder"].Value, out displayOrder);
                    int.TryParse(insNode.Attributes["MinAge"].Value, out minAge);
                    int.TryParse(insNode.Attributes["MaxAge"].Value, out maxAge);
                    int.TryParse(insNode.Attributes["Rating"].Value, out priceRating);
                    InsuranceProduct insItem = new InsuranceProduct(extraHireCode, extraHireName, dailyRate, ehiUOMType, grossAmt, maxCharge, extraHireSapId, displayOrder, promptForQuantity, minAge, maxAge);
                    string noteText = (insNode.Attributes["NteDesc"] != null ? insNode.Attributes["NteDesc"].Value : string.Empty);
                    if (isInclusive && insNode.SelectSingleNode("Note") != null && insNode.SelectSingleNode("Note").Attributes["NteDesc"] != null)
                        noteText = insNode.SelectSingleNode("Note").Attributes["NteDesc"].Value;//Inclusive is embedded node ;o(
                    XmlNodeList actionNodes = insNode.SelectNodes("RequiredActions/Action");
                    if (actionNodes != null && actionNodes.Count > 0)//has action dependencies
                    {
                        insItem.RequiredActions = new NameValueCollection();
                        foreach (XmlNode xmlNode in actionNodes)//<Action code="DISPLAYWHEN" PrdCode="ER2" />                         
                            insItem.RequiredActions.Add(xmlNode.Attributes["PrdCode"].Value, xmlNode.Attributes["code"].Value);
                    }
                    insItem.IsInclusive = isInclusive;
                    insItem.Note = noteText;
                    insItem.Rating = priceRating;
                    insItem.HirePeriod = hirePeriod;
                    insItem.WaivedProducts = waivedItems;
                    insList.Add(insItem);
                }
                insList.Sort(delegate(InsuranceProduct ins1, InsuranceProduct ins2) { return ins1.Rating.CompareTo(ins2.Rating); });
                //TODO: map dependencies                
                return insList.ToArray();
            }
        }


        BookingFee[] GetRentalFeesForXMLNodes(XmlNodeList feeXMLNodes)
        {
            List<BookingFee> feesList = new List<BookingFee>();
            foreach (XmlNode feeXml in feeXMLNodes)
            {
                BookingFee quoteFee = new BookingFee(feeXml);
                feesList.Add(quoteFee);
            }
            return feesList.ToArray();
        }
        
        /// <summary>
        /// Return the Quotes ferry products
        /// </summary>
        /// <param name="ferryNodes"></param>
        /// <returns></returns>
        FerryProduct[] getFerryProducts(XmlNodeList ferryNodes)
        {
            string code, name;
            int minAge, maxAge;
            decimal grossAmt, maxCharge, dailyRate;
            bool success;
            int displayOrder;
            List<FerryProduct> ferryProductsList = new List<FerryProduct>();
            if (ferryNodes != null && ferryNodes.Count > 0)//has ferrys
            {
                foreach (XmlNode ferryNode in ferryNodes)
                {
                    code = ferryNode.Attributes["Code"].Value;
                    name = ferryNode.Attributes["Name"].Value;
                    if (code.Contains("PAX"))//TODO: missing naming convention
                    {
                        success = int.TryParse(ferryNode.Attributes["DisplayOrder"].Value, out  displayOrder);
                        DateTime rateStart = DateTime.Now, rateEnd = DateTime.Now;//TODO: get from Aurora
                        XmlNodeList personRateNodes = ferryNode.SelectNodes("//PersonRate");
                        List<PersonRateInfo> personRateList = new List<PersonRateInfo>();
                        foreach (XmlNode personRateNode in personRateNodes)
                        {
                            decimal prrRate = 0;
                            PersonRateInfo currentRate = new PersonRateInfo();
                            currentRate.PersonId = personRateNode.Attributes["PrrId"].Value;
                            string paxTypeStr = personRateNode.Attributes["PaxType"].Value;
                            currentRate.PersonInfoType = PersonRateInfo.GetTypeForAuroraString(paxTypeStr);
                            success = decimal.TryParse(personRateNode.Attributes["PrrRate"].Value, out  prrRate);
                            currentRate.Rate = prrRate;
                            currentRate.SapId = personRateNode.Attributes["SapId"].Value;
                            personRateList.Add(currentRate);
                        }
                        PersonRateInfo[] rateCollection = personRateList.ToArray();
                        FerryCrossingData fcd = new FerryCrossingData(code, name, rateStart, rateEnd, rateCollection);//todo: from xml
                        FerryProduct currentFerryPrd = new FerryProduct(code, name, fcd);
                        ferryProductsList.Add(currentFerryPrd);
                    }
                    else if (code.Contains("VEH"))//TODO: missing naming convention requires Biz Logic Flag
                    {
                        string sapId = ferryNode.Attributes["SapId"].Value;
                        success = int.TryParse(ferryNode.Attributes["DisplayOrder"].Value, out  displayOrder);
                        success = int.TryParse(ferryNode.Attributes["MinAge"].Value, out  minAge);
                        success = int.TryParse(ferryNode.Attributes["MaxAge"].Value, out  maxAge);
                        success = decimal.TryParse(ferryNode.Attributes["GrossAmt"].Value, out  grossAmt);
                        success = decimal.TryParse(ferryNode.Attributes["MaxCharge"].Value, out maxCharge);
                        success = decimal.TryParse(ferryNode.Attributes["dailyRate"].Value, out dailyRate);
                        bool promptForQty = (ferryNode.Attributes["PromptQty"].Value == "Yes");
                        UOMType uom = (ferryNode.Attributes["UOM"].Value == "DAY") ? UOMType.Day : UOMType.None;
                        DateTime rateStart = DateTime.Now, rateEnd = DateTime.Now;
                        FerryProduct currentFerryPrd = new FerryProduct(code, name, dailyRate, uom, grossAmt, maxCharge, sapId, displayOrder, promptForQty, minAge, maxAge);
                        ferryProductsList.Add(currentFerryPrd);
                    }
                }                
            }//end load Ferry
            return ferryProductsList.ToArray();
        }
        
        /// <summary>
        /// Get the Available C/C for this Quote
        /// </summary>
        /// <param name="creditCardNodes"></param>
        /// <returns></returns>
        CreditCardElement[] getAvailableCards(XmlNodeList creditCardNodes)
        {
            //XmlNodeList creditCardNodes = xmlDoc.SelectNodes("data/ConfigurationProducts/Surcharge/Product");
            ApplicationData appData = ApplicationManager.GetApplicationData();
            if (creditCardNodes != null && creditCardNodes.Count > 0)
            {
                List<CreditCardElement> cardElmList = new List<CreditCardElement>();
                foreach (XmlNode ccNode in creditCardNodes)
                {
                    decimal currentSurcharge = 0m;
                    string currentCode = ccNode.Attributes["Code"].Value;
                    string currentName = appData.GetCardNameForCode(currentCode);
                    decimal.TryParse(ccNode.Attributes["SurchargePer"].Value.Replace('%', ' '), out currentSurcharge);
                    if (currentCode != null && currentCode != string.Empty && currentName != null && currentName != string.Empty)
                    {
                        CreditCardElement currentCardData = new CreditCardElement(currentCode, currentName, currentSurcharge);
                        cardElmList.Add(currentCardData);
                    }
                }
                return cardElmList.ToArray();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the Quotes total sum
        /// </summary>
        /// <returns></returns>
        public decimal GetQuoteTotal()
        {
            decimal totalQuote = VehicleCharge;
            int hirePeriod = Request.GetHirePeriod();
            foreach (ExtraHireItem product in AddedExtraHireItems)
                totalQuote += product.GrossAmount;
            foreach (InsuranceProduct product in SelectedInsurance)
                totalQuote += product.GrossAmount;
            //Ferry if any
            if (FerryProducts.Length == 2 && FerryProducts[0].Quantity > 0)
            {
                for (int i = 0; i < FerryProducts[0].Quantity; i++)
                    totalQuote += FerryProducts[0].GrossAmount;
                foreach (PersonRateInfo person in FerryProducts[1].FerryData.PersonRateCollection)
                    if (person.NumPassengers > 0)
                        totalQuote += (person.Rate * person.NumPassengers);
            }
            foreach (BookingFee fee in EnforcedFees)
                totalQuote += (fee.IsPercentageFee ? (totalQuote * fee.GrossCalcPercentage / 100) : fee.GrossAmount);
            return totalQuote;
        }
               

        /// <summary>
        /// Serialize a Quote into XML representation
        /// </summary>
        /// <returns></returns>
        public XmlDocument SerializeQuote()
        {
            string customerNote = bookingInfo.CustomerInfo.Note + (!String.IsNullOrEmpty(FerryNote) ? " " + FerryNote : string.Empty);           
            StringBuilder sb = new StringBuilder();
            try
            {                
                sb.Append(@"
<Data xmlns="""">
    <RentalDetails>
		");
                sb.Append("<RentalId>" + bookingID + "-1</RentalId>");
                sb.Append(@"	
        <AgentReference>" + AgentCode + @"</AgentReference> 
        <CountryCode>" + bookingInfo.CountryCode + @"</CountryCode>
        <Note>" + customerNote + @"</Note>
        <UserCode>" + AgentCode + @"</UserCode>
        <GetConfXML>True</GetConfXML>
		<IsRequestBooking>False</IsRequestBooking>
        <SelectedSlot></SelectedSlot>
        <SlotId>0</SlotId>
        <ConfirmQuote>True</ConfirmQuote> 		
		<AddProductList>
        ");
                foreach (ExtraHireItem ehi in getAddedProducts())
                    sb.Append("<Product SapId='" + ehi.SapId + "' Qty='" + (ehi.PromptQuantity ? ehi.Quantity : 1)+ "' UsageOn='" + Request.PickUpDate.ToString("dd/MM/yyyy") + "' PrdShortName='" + ehi.Name + "'/>");
                foreach(InsuranceProduct ins in getAddedInsurance())
                    if (!ins.Code.ToUpper().Equals("ER0"))
                        sb.Append("<Product SapId='" + ins.SapId + "' UsageOn='" + Request.PickUpDate.ToString("dd/MM/yyyy") + "' Qty='1' PrdShortName='" + ins.Name + "'/>");               
                sb.Append(@"
        </AddProductList>
		<RemoveProductList>
                ");
                foreach (ExtraHireItem ehi in getRemovedProducts())
                    sb.Append("<Product BpdId='" + ehi.BpdId + "' />");                
                foreach(InsuranceProduct ins in getRemovedInsurance())
                    if(!ins.Code.ToUpper().Equals("ER0"))
                        sb.Append("<Product BpdId='" + ins.BpdId + "' />");
                if (InclusiveSelected && getWaivedProducts().Count > 0)//added biz logic to support removing of auto add products 
                {
                    foreach (string waivedItemCode in getWaivedProducts())
                    {
                        Dictionary<string, BookingFee> feesDic = new List<BookingFee>(EnforcedFees).ToDictionary(p => p.ShortName);
                        if(feesDic.ContainsKey(waivedItemCode))
                            sb.Append("<Product BpdId='" + feesDic[waivedItemCode].BpdId + "' />");                   
                    }
                }               
                sb.Append(@"					
		</RemoveProductList>              			
	</RentalDetails>
                ");                
                sb.Append(@"
	<CreditCardDetails>
		<CardType>" + bookingInfo.PaymentInformation.CardType.ToString() + @"</CardType>
		<CreditCardNumber>" + bookingInfo.PaymentInformation.CardNumber  +  @"</CreditCardNumber>
		<CreditCardName>" +   bookingInfo.PaymentInformation.CardHolderName +  @"</CreditCardName>
		<ApprovalRef>" + AgentCode + @"</ApprovalRef>
		<ExpiryDate>" +   bookingInfo.PaymentInformation.ExpiryDate +  @"</ExpiryDate>
		<PaymentType>R</PaymentType>
		<PaymentAmount>" +   bookingInfo.PaymentInformation.PaymentAmount + @"</PaymentAmount>
		<PaymentSurchargeAmount>" + bookingInfo.PaymentInformation.PaymentSurchargeAmount + @"</PaymentSurchargeAmount>
	</CreditCardDetails>
	            ");
                sb.Append(@"
    <DPSData> 
		<DPSTxnRef>" + (transaction != null && !String.IsNullOrEmpty(transaction.TransactionCode) ? transaction.TransactionCode : string.Empty) + @"</DPSTxnRef>
		<DPSAuthCode>" + (transaction != null && !String.IsNullOrEmpty(transaction.AutorizationCode) ? transaction.AutorizationCode : string.Empty) + @"</DPSAuthCode>
	</DPSData>
	<AddPayment>True</AddPayment>
	<DoCheckOut>False</DoCheckOut>
	<PrintRentalAgreement>False</PrintRentalAgreement>	
</Data>
                ");
                XmlDocument responseDoc = new XmlDocument();
                responseDoc.LoadXml(sb.ToString());
                return responseDoc;
            } 
            catch (Exception ex) 
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.Application, "AuroraQuote.Serialize", msg, (sb != null && !String.IsNullOrEmpty(sb.ToString()) ? sb.ToString() : string.Empty ));
                return null;
            }
        }

        /// <summary>
        /// Get The required security deposit for the current Quote
        /// </summary>
        /// <returns>Currently 20%, TODO: get from Aurora once defined</returns>
        public decimal GetRequiredDeposit()
        {
            return RequiredDeposit;
            //return (VehicleCharge * 0.2m);//TODO: reciece from Aurora once returned from WS
        }
        
        private ExtraHireItem[] getRemovedProducts()
        {
            List<ExtraHireItem> removedProductsList = new List<ExtraHireItem>();
            Dictionary<string, ExtraHireItem> AddedHiresDic = new List<ExtraHireItem>(AddedExtraHireItems).ToDictionary(p => p.SapId);
            foreach (ExtraHireItem oldEhi in SelectedExtraHireItems)
            {
                if (!AddedHiresDic.Keys.Contains(oldEhi.SapId))
                    removedProductsList.Add(oldEhi);
            }
            return removedProductsList.ToArray();
        }
        
        /// <summary>
        /// Return Only Extra Hires that have been added to the booking from the original quote
        /// </summary>
        /// <returns></returns>
        private ExtraHireItem[] getAddedProducts()
        {
            List<ExtraHireItem> addedProductsList = new List<ExtraHireItem>();
            Dictionary<string, ExtraHireItem> SelectedHiresDic = new List<ExtraHireItem>(SelectedExtraHireItems).ToDictionary(p => p.SapId);
            foreach (ExtraHireItem addedEhi in AddedExtraHireItems)
            {//for each added product  
                if (SelectedHiresDic.Keys.Contains(addedEhi.SapId))
                {
                    if (SelectedHiresDic[addedEhi.SapId].Quantity != addedEhi.Quantity)
                        addedProductsList.Add(addedEhi);
                }
                else 
                {
                    addedProductsList.Add(addedEhi);//newlly added one
                }
            }
            return addedProductsList.ToArray();
        }
        
        private InsuranceProduct[] getAddedInsurance() {
            List<InsuranceProduct> addedInsPrdList = new List<InsuranceProduct>();
            Dictionary<string, InsuranceProduct> SelectedInsDic = new List<InsuranceProduct>(SelectedInsurance).ToDictionary(p => p.SapId);
            foreach (InsuranceProduct addedIns in AddedInsurance)
            {//for each added product  
                if (!SelectedInsDic.Keys.Contains(addedIns.SapId))
                    addedInsPrdList.Add(addedIns);
            }
            return addedInsPrdList.ToArray();        
        }

        /// <summary>
        /// Get the removed Insurance items since the last quote request
        /// </summary>
        /// <returns></returns>
        private InsuranceProduct[] getRemovedInsurance()
        {
            List<InsuranceProduct> removedInsPrdList = new List<InsuranceProduct>();
            Dictionary<string, InsuranceProduct> addedInsDic = new List<InsuranceProduct>(AddedInsurance).ToDictionary(p => p.SapId);
            foreach (InsuranceProduct selectedIns in SelectedInsurance)
            {//for each added product  
                if (!addedInsDic.Keys.Contains(selectedIns.SapId))                    
                    removedInsPrdList.Add(selectedIns);
            }
            return removedInsPrdList.ToArray();
        }
        
        /// <summary>
        /// Aggregate and sum the current quote total
        /// </summary>
        /// <returns>Decimal calue of total quote</returns>
        public decimal CurrentTotal() 
        {
            decimal totalQuote = VehicleCharge;            
            foreach (ExtraHireItem product in AddedExtraHireItems)
                totalQuote += product.GrossAmount;
            foreach (InsuranceProduct product in AddedInsurance)
                totalQuote += product.GrossAmount;
            if (FerryProducts.Length == 2 && FerryProducts[0].Quantity > 0)
            {
                for (int i = 0; i < FerryProducts[0].Quantity; i++)
                    totalQuote += FerryProducts[0].GrossAmount;                
                foreach (PersonRateInfo person in FerryProducts[1].FerryData.PersonRateCollection)
                    if (person.NumPassengers > 0)
                        totalQuote += (person.Rate * person.NumPassengers);                    
            }            
            List<string> waivedFeeCodes = getWaivedProducts();
            foreach (BookingFee fee in EnforcedFees)
                if (!waivedFeeCodes.Contains(fee.ShortName))
                    totalQuote += (fee.IsPercentageFee ? (totalQuote * fee.GrossCalcPercentage / 100) : fee.GrossAmount);
            return totalQuote;
        }
        
        /// <summary>
        /// Get the WaiveItems on the selected inclusive item, TODO: refactor into Quote Class if used otherplaces
        /// </summary>
        /// <returns></returns>
        public List<string> getWaivedProducts()
        {
            List<string> waivedItems = new List<string>();
            InsuranceProduct incItem = null;
            if (InclusiveSelected && SelectedInsurance.Length == 1)
                incItem = SelectedInsurance[0];
            if (incItem == null)//added after quote submit
                incItem = AddedInsurance != null && AddedInsurance.Length > 0 ? AddedInsurance[0] : null;
            if (incItem != null)//has inc
                waivedItems = new List<string>(incItem.WaivedProducts.TrimEnd(',').Split(','));
            return waivedItems;
        }

        /// <summary>
        /// Get The qutoes Rate Bands
        /// </summary>
        /// <returns></returns>
        public AvailabilityItemChargeRateBand[] GetRateBands()
        {
            AvailabilityItemChargeRateBand[] ratebands = null;
            /*
            <RateBands>
                <Bands TotalSaving="709.80">
                    <Band From="Mar 16 2011 10:00AM" To="Mar 31 2011 10:00AM" HirePeriod="16" OriginalRate="137.00" DiscountedRate="116.45" GrossAmt="1863.20" IncludesFreeDay="0" /> 
                    <Band From="Apr 1 2011 10:00AM" To="Apr 20 2011 3:00PM" HirePeriod="20" OriginalRate="127.00" DiscountedRate="107.95" GrossAmt="2159.00" IncludesFreeDay="0" /> 
                </Bands>
                <Discounts>
                    <Discount CodCode="LHD" CodDesc="Long Hire Discount" Discount="15.00" IsPercentage="Yes" DiscountDescription="35" /> 
                </Discounts>
            </RateBands>
            */
            XmlNodeList RateBandsElm = RateBandsXML.SelectNodes("Bands/Band");
            List<AvailabilityItemChargeRateBand> rbList = new List<AvailabilityItemChargeRateBand>();
            if (RateBandsElm != null)
            {
                foreach (XmlNode rateBandNode in RateBandsElm)
                {//foreach <Band> node
                    AvailabilityItemChargeRateBand aiRateBand = new AvailabilityItemChargeRateBand();
                    aiRateBand.LoadFromXML(rateBandNode);
                    rbList.Add(aiRateBand);
                }
                ratebands = rbList.ToArray();
            }
            //TODO: exp handle
            return ratebands;
        }
    }
}