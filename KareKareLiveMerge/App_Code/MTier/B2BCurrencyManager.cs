﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace THL.Booking
{

    //TODO: refactor and consider encapsulation
    [DataContract]
    public struct ExchangeRate
    {
        [DataMember]
        public string FromCode;
        [DataMember]
        public string ToCode;
        [DataMember]
        public decimal Rate;
    }
        
    /// <summary>
    /// Summary description for B2BCurrencyManager
    /// </summary>
    public class B2BCurrencyManager
    {
        public B2BCurrencyManager()
        {
            
        }


        public static string GetAgentCurrenciesInJson(ExchangeRate[] rates)
        {
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(ExchangeRate[]));
            ser.WriteObject(stream1, rates);
            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);
            return sr.ReadToEnd();
        }

    }
}