﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;

namespace THL.Booking
{
    //'pb':'AKL', 'db':'AKL', 'pd' : '01-12-2011', 'dd':'15-12-2011', 'pt':'10:00', 'dt':'15:00', 'na':2, 'nc':3, 'cc':'NZ', 'brand':'b', 'vt':'av', 'model':'all', 'ac' :'ARARAT', 'pc':'FLEXB'
    [DataContract]
    public class B2BRequestParams
    { 
        [DataMember]
        public string pb;
        
        [DataMember]
        public string db;

        [DataMember]
        public string pd;

        [DataMember]
        public string dd;
        
        [DataMember]
        public string pt;

        [DataMember]
         public string dt;

        [DataMember]
        public string na;
        
        [DataMember]
        public string nc;
        
        [DataMember]
        public string cc;
        
        [DataMember]
        public string brand;
        
        [DataMember]
        public string vt;

        [DataMember]
        public string model;

        [DataMember]
        public string ac;

        [DataMember]
        public string pc;

        [DataMember]
        public bool isGross;

        [DataMember]
        public double requestTimeInMil;

        [DataMember]
        public RequestTypes requestType;

        [DataContract(Name = "RequestType")]
        public enum RequestTypes
        {
            [EnumMember]
            Availability = 1,
            [EnumMember]
            AltAvailability = 2
        }

        //rev:mia april 16 2012
        [DataMember]
        public string ctyResidence;

        /// <summary>
        /// Convert from incoming date string to DateTime Object
        /// </summary>
        /// <returns></returns>
        public static DateTime ParseDate(string dateString, string timeStr) 
        {
            DateTime date = new DateTime();//TODO: define default and inform caller.
            try
            {                
                string dateTimeStr = dateString + " " + timeStr;
                System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
                string format = "d-M-yyyy HH:mm";
                date = DateTime.ParseExact(dateTimeStr, format, provider);
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "B2BAvailabilityRequest.Parse", ex.Message, "{'dateString':'" + dateString + "','timeStr':'" + timeStr + "'}");
            }
            return date;
        }    
    }
    /// <summary>
    /// Extend Availability Requst to Support B2B requirements
    /// </summary>
    public class B2BAvailabilityRequest : AvailabilityRequest
    {
        public B2BAvailabilityRequest() { }

        //protected B2BRequestParams b2bParams;
        public B2BRequestParams b2bParams;

        /// <summary>
        /// Used to identify the request in a Cacheable environment
        /// </summary>
        public string CacheKey { get; set; } 

        public override string AgentCode{get;set;}

        public string VehicleCodeString{
            get
            {
                if (VehicleModel != null && VehicleModel.IndexOf(".") > 0)//TODO: define required state
                    return VehicleModel.Split('.')[1];
                return VehicleModel;
            }
        }

        public enum RequestType
        {
            Availability,
            AltAvailability
        }

        public bool IsGross { get; set; }
        
        public B2BAvailabilityRequest(string json, AgentLoginData agentData)
        {
            
            int na = 1, nc = 0;
            
            b2bParams = new B2BRequestParams(); 
            JavaScriptSerializer js = new JavaScriptSerializer();
            b2bParams = js.Deserialize<B2BRequestParams>(json);

            int.TryParse(b2bParams.na, out na);
            int.TryParse(b2bParams.nc, out nc);


            //test stub
            CountryCode = AvailabilityHelper.GetCountryCodeForString(b2bParams.cc);
            DropOffBranch = b2bParams.db;
            PickUpBranch = b2bParams.pb;
            DropOffDate = B2BRequestParams.ParseDate(b2bParams.dd, b2bParams.dt);
            PickUpDate = B2BRequestParams.ParseDate(b2bParams.pd, b2bParams.pt);
            AgentCode = b2bParams.ac;
            PackageCode = b2bParams.pc;
            Brand = b2bParams.brand;
            NumberOfAdults = na;
            NumberOfChildren = nc;
            VehicleModel = b2bParams.model;
            VehicleType = (b2bParams.vt.ToLower().Equals("ac") ? VehicleType.Car : VehicleType.Campervan);
            IsGross = b2bParams.isGross;//TODO: 
            CacheKey = json;

            //rev:mia april 16 2012
            CountryOfResidence = b2bParams.ctyResidence;
        }


        /// <summary>
        /// Format the Availability Request for Web Service request
        /// </summary>
        /// <returns>GET Params String</returns>
        public override string GenerateAuroraRequestURL()
        {
            string isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings["IsTestMode"];

            StringBuilder sb = new StringBuilder();
            sb.Append("Brand=" + Brand.ToUpper() + "&"); //TODO: get the availability requests brands collection..
            sb.Append("CountryCode=" + CountryCode + "&");
            sb.Append("VehicleCode=" + VehicleCodeString + "&");
            sb.Append("CheckOutZoneCode=" + PickUpBranch + "&");
            sb.Append("CheckOutDateTime=" + PickUpDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("CheckInZoneCode=" + DropOffBranch + "&");
            sb.Append("CheckInDateTime=" + DropOffDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("NoOfAdults=" + NumberOfAdults + "&");
            sb.Append("NoOfChildren=" + NumberOfChildren + "&");
            sb.Append("AgentCode=" + (AgentCode != null ? AgentCode.Replace(",", "") : string.Empty) + "&");
            sb.Append("PackageCode=" + (PackageCode != null ? PackageCode.Replace(",", "") : string.Empty) + "&");
            sb.Append("IsVan=" + (VehicleType == VehicleType.Campervan ? "True" : "False") + "&");
            sb.Append("IsBestBuy=True&");
            sb.Append("CountryOfResidence=" + (CountryOfResidence != null ? CountryOfResidence.ToUpper() : string.Empty) + "&");
            sb.Append("IsTestMode=" + isTestMode + "&");
            sb.Append("IsGross="+IsGross);

            //rev:mia July 3, 2014 - change to TRUE if its coming from B2B
            //utm_campaign=CrossLink,&hso=true&hgo=true&gno
            //try
            //{
            //    string urlReferrer = System.Web.HttpContext.Current.Request.UrlReferrer.OriginalString;
            //    if (urlReferrer.IndexOf("utm_campaign=CrossLink") != -1
            //        && urlReferrer.IndexOf("&hso") != -1
            //        && urlReferrer.IndexOf("&hgo") != -1
            //        && urlReferrer.IndexOf("&gno") != -1
            //        && urlReferrer.IndexOf("&json") != -1)
            //    {
            //        sb.Append("&AgnisInclusiveProduct=false");
            //    }
            //    else
            //        sb.Append("&AgnisInclusiveProduct=true");
            //}
            //catch (Exception exUrlReferrer)
            //{
            //    sb.Append("&AgnisInclusiveProduct=true");
            //}
            
            

            
            return sb.ToString();
        }

        
    }

    public class B2BAltAvailabilityRequest : B2BAvailabilityRequest
    {
        public B2BAltAvailabilityRequest(string json, AgentLoginData agentData):base(json, agentData){}

        public override string GenerateAuroraRequestURL()
        {
            string isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings["IsTestMode"];
            StringBuilder sb = new StringBuilder();//Construct Request Params
            sb.Append("Brand=" + Brand.ToUpper() + "&");
            sb.Append("VehicleCode=" + VehicleCodeString + "&");
            sb.Append("CountryCode=" + CountryCode + "&");
            sb.Append("CheckOutZoneCode=" + PickUpBranch + "&");
            sb.Append("CheckOutDateTime=" + PickUpDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("CheckInZoneCode=" + DropOffBranch + "&");
            sb.Append("CheckInDateTime=" + DropOffDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("AgentCode=" + (AgentCode != null ? AgentCode.Replace(",", "") : string.Empty) + "&");
            sb.Append("PackageCode=" + (PackageCode != null ? PackageCode.Replace(",", "") : string.Empty) + "&");
            sb.Append("IsVan=" + (VehicleType == VehicleType.Campervan ? "True" : "False") + "&");
            sb.Append("NumberOfAdults=" + NumberOfAdults + "&");
            sb.Append("NoOfChildren=" + NumberOfChildren + "&");
            sb.Append("isBestBuy=True&");
            sb.Append("countryOfResidence=" + CountryOfResidence + "&");
            //if pick & drop are same no 3,6 (switch loc)
            string orderStr = (System.Web.Configuration.WebConfigurationManager.AppSettings["AltAvailOrder"] != null ? System.Web.Configuration.WebConfigurationManager.AppSettings["AltAvailOrder"] : "1,2,4,5");
            sb.Append("DisplayMode=" + (!PickUpBranch.Equals(DropOffBranch) ? "3,6," : string.Empty) + orderStr + "&");
            sb.Append("Istestmode="+isTestMode+"&");
            sb.Append("IsGross=" + IsGross);
            return sb.ToString();
        }
    }
}