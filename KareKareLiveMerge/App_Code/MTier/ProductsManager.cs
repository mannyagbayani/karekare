﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;


namespace THL.Booking
{

    /// <summary>
    /// Manager for Aurora Product collection handling  
    /// </summary>
    public class PackageManager
    {

        AuroraPackage package;


        public string InclusiveName { get { return package.InclusiveName; } }
        public decimal InclusiveDailyRate { get { return package.InclusiveDayPrice; } }
        public decimal InclusiveEstimatedTotal { get { return package.InclusiveEstimatedTotal; } }
        public decimal DTRPrice { get { return package.DTRCharge; } }
       // public string InclusiveNote {  }
        public bool HasDTR { get { return package.HasDTR; } }

        /// <summary>
        /// Added to support Road Charges title from WS
        /// </summary>
        public string RoadChargeTitle { get { return package.DTRTitle; } }

        public string GetInclusiveAvpId() {
            if (package.HasInclusivePackage)
            {
                return package.GetInclusiveItemAvpID();
            }
            else 
                return null;
        }


        public bool GetInclusiveOneWayFeeWaived()
        {
            if (package.HasInclusivePackage)
            {
                return package.InclusiveAvailItem.WaiveInclusiveOneWayFee;
            }
            else
                return false;
        }

        public decimal OneWayFeeComponent
        {
            get { return package.InclusiveAvailItem.OneWayFeeComponent; }
        }

        /// <summary>
        /// Merge a product definition with a supplied partially loaded booked product
        /// </summary>
        /// <param name="PrototypeProduct">The retrurned Package information product</param>
        /// <param name="targetProduct">The Booked product with quantity and id</param>
        /// <returns></returns>
        public static AuroraProduct MergeProducts(AuroraProduct prototypeProduct, ref AuroraProduct targetProduct)
        {
            AuroraProduct mergedProduct = new AuroraProduct(prototypeProduct.Code, 
                prototypeProduct.Name, 
                prototypeProduct.DailyRate, 
                prototypeProduct.UOM, 
                prototypeProduct.GrossAmount, 
                prototypeProduct.MaxCharge, 
                prototypeProduct.SapId, 
                prototypeProduct.DisplayOrder, 
                prototypeProduct.PromptQuantity, 
                prototypeProduct.MinAge, 
            prototypeProduct.MinAge);
            mergedProduct.Type = prototypeProduct.Type;
            mergedProduct.SelectedAmount = targetProduct.SelectedAmount;
            return mergedProduct;
        }



        bool packageLoaded = false;
        public bool PackageLoaded
        {
            get { return packageLoaded; }
        }
        
        AvailabilityItem availabilityItem;
        
        public PackageManager(AvailabilityItem _availabilityItem, ApplicationData appData, bool isGrossRequest)
        {
            availabilityItem = _availabilityItem;
            DataAccess dal = new DataAccess();
            package = dal.GetPackageForAvpId(_availabilityItem.AVPID, appData, isGrossRequest);//is gross should be passed based on session
            packageLoaded = (package != null);
        }

        public string GetIncPackageXML()
        {
            return package.IncPackageXml; 
        }

        public CreditCardElement[]  GetAvailableCards(string avpId) {//possible cache by AVPiD point
            return package.AvailableCardType;
        }
        
        
        public ExtraHireItem[] GetExtraHireItems(string avpId)
        {
            return package.GetExtraHireItems();
        }

        public InsuranceProduct[] GetInsuranceItems(string avpId)
        {
            return package.GetInsuranceProducts();
        }

        public FerryProduct[] GetFerryProducts(string avpId)
        { 
            FerryProduct[] ferrycollection = package.GetFerryProducts();

            if (packageLoaded && ferrycollection != null && ferrycollection.Length > 0)
                return ferrycollection;
            else
                return null;
        }

        public PersonRateInfo[] PersonRateInfo(string avpId)
        {
            return package.GetFerryRateList();
        }
        
        public AuroraProduct[] GetAllProducts(string avpId)
        {
            List<AuroraProduct> products = new List<AuroraProduct>();
            products.AddRange(package.GetExtraHireItems());
            products.AddRange(package.GetInsuranceProducts());
            if(package.GetFerryProducts() != null)
                products.AddRange(package.GetFerryProducts());
            return products.ToArray();
        }

        public AvailabilityItem GetInclusiveVehicleCharges(string avpId)
        {
            return package.InclusiveAvailItem;            
        }

        public InsuranceProduct GetInclusiveProduct(string avpId)
        {
            return package.GetBonusPack();
        }

        /// <summary>
        /// Get The Specified Package Admin Fee
        /// </summary>
        /// <param name="avpId">Future use for supporting multiple cache of avpIds/packages info per session</param>
        /// <returns></returns>
        public decimal GetAdminFee(string avpId)//future support could include by AvpId
        {
            return package.AdminFee;
        }

    }
}