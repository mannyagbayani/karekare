﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{


    /// <summary>
    /// Analytics tracking Event Data Structure
    /// </summary>
    public class AnalyticsEventTrackingItem
    {
        public AnalyticsEventTrackingItem(string category, string action, string label, int value)
        {
            Category = category;
            Action = action;
            Label = label;
            Value = value;
            //TODO: proper encapsulation once finalised
        }

        public string Category;//TODO: proper encapsulation once finalised
        public string Action;//TODO: proper encapsulation once finalised
        public string Label;//TODO: proper encapsulation once finalised
        public int Value;//TODO: proper encapsulation once finalised
    }


    public enum TrackingProvider
    {
        Custom = 0,
        DecideDNA = 1,
        GoogleAnalytics = 2,
        GoogleAdWords = 3
    }

    /// <summary>
    /// Tracking Data Structure
    /// </summary>
    public class TrackingData
    {
        TrackingProvider _provider;
        string _code;
        string _id;
        string _event;
        string _domain;

        public string Code
        {
            get { return _code; }
        }
                
        public string ID
        {
            get { return _id; }
        }

        public string Event
        {
            get { return _event; }
        }

        public string Domain
        {
            get { return _domain; }
        }

        public TrackingData(TrackingProvider provider, string id, string code, string eventName, string domainName)
        {
            _provider = provider;
            _id = id;
            _code = code;
            _event = eventName;
            _domain = domainName;
        }
    }
}