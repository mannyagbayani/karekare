﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{

    public enum PersonType {
        None = 0,
        Adult = 1,
        Child = 2,
        Infant = 3        
    }

    public class PersonRateInfo {

        public PersonType PersonInfoType;
        public string PersonId;
        public string SapId;        
        public decimal Rate;
        public int NumPassengers;

        public static PersonType GetTypeForAuroraString(string typeStr)
        {
            PersonType personType = PersonType.None;
            switch (typeStr)
            { 
                case "Adult":
                    personType = PersonType.Adult;
                    break;
                case "Child":
                    personType = PersonType.Child;
                    break;
                case "Infant":
                    personType = PersonType.Infant;
                    break;
            }
            return personType;            
        }
    
        
    
    }
    
    
    /// <summary>
    /// Ferry Crossing Data Structure
    /// </summary>
    public class FerryCrossingData
    {

        public string Code;

        public string Name;

        public DateTime StartTime;

        public DateTime EndTime;

        PersonRateInfo[] personRates;

        public PersonRateInfo[] PersonRateCollection {
            get { return personRates;  }  
            set { personRates = value;  } 
        }


        public FerryCrossingData(string _code, string _name, DateTime _startTime, DateTime _endTime, PersonRateInfo[] _personRateInfo)
        {
            Code = _code;
            Name = _name;
            StartTime = _startTime;
            EndTime = _endTime;
            personRates = _personRateInfo;
        }

        
    }
}