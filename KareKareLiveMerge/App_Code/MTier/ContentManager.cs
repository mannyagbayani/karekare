﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Text;

namespace THL.Booking
{

    public enum ContentType
    {
        Link = 0,
        HTML = 1
    }

    /// <summary>
    /// A Content Entity Representation 
    /// </summary>
    public struct ContentElement {
        public ContentType ContentType;
        public VehicleType VehicleType;
        public THLBrands Brand;
        public string Code;
        public string Content;
        public DateTime StartTime;
        public DateTime EndTime;
        public CountryCode Country;
    
        public override string ToString()
        {
            return Code + " : " + Content;        
        }
    
    }

    /// <summary>
    /// Content Management Funcionality 
    /// </summary>
    public class ContentManager
    {

        ContentElement[] contentElements;
        
        public ContentManager()
        {
            
        }

        /// <summary>
        /// Return a Vehicle Content Link for a given Vehicle Code
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="countryCode"></param>
        /// <param name="vehicleCode"></param>
        /// <returns></returns>
        public static string GetVehicleLink(THLBrands brand, CountryCode countryCode, string vehicleCode, THLBrands containerBrand)
        {
            // Swapped for AirNZ treatment 28062011
            //if (containerBrand == THLBrands.MAC)
            //{
            //    return getMACVehicleLink(countryCode, vehicleCode, brand);
            //}


            Vehicle[] cacheVehicles = ApplicationManager.GetVehicles();

            if (containerBrand == THLBrands.AIRNZ || containerBrand == THLBrands.MAC)//Air NZ no brand match
            {
                foreach (Vehicle vehicle in cacheVehicles)
                {
                    string[] currentVehicleParams = vehicle.Code.Split('.');
                    string auroraCode = (currentVehicleParams.Length > 1 ? currentVehicleParams[1] : currentVehicleParams[0]);
                    if (auroraCode.Equals(vehicleCode) && (vehicle.Country.ToUpper() == countryCode.ToString().ToUpper()))
                        return vehicle.ContentURL;
                }
                return string.Empty;
            }                                    
            
            foreach (Vehicle vehicle in cacheVehicles)//All THL Brands match
            {
                string[] currentVehicleParams =  vehicle.Code.Split('.');
                string auroraCode = (currentVehicleParams.Length > 1 ? currentVehicleParams[1] : currentVehicleParams[0]);
                if (auroraCode.Equals(vehicleCode) && vehicle.Brand == brand && (vehicle.Country.ToUpper() == countryCode.ToString().ToUpper()))
                    return vehicle.ContentURL;
            }
            return string.Empty;
        }
        
        /// <summary>
        /// Used for retrieving mac vehicle links deprecated and refactored 12/09
        /// </summary>
        /// <param name="countryCode"></param>
        /// <param name="vehicleCode"></param>
        /// <returns></returns>
        static string getMACVehicleLink(CountryCode countryCode, string vehicleCode, THLBrands vehicleBrand)
        {
            Vehicle[] cacheVehicles = ApplicationManager.GetMacVehicles();
            foreach (Vehicle vehicle in cacheVehicles)
            {
                string auroraCode = vehicle.Code;
                if (auroraCode.Contains(".")) 
                    auroraCode = auroraCode.Split('.')[1];//take the Aurora portion only                
                if (auroraCode == vehicleCode && vehicle.Brand == vehicleBrand && vehicle.Country == countryCode.ToString().ToUpper() )
                    return vehicle.ContentURL;
            }           
            return "www.motorhomesandcars.com";//TODO: define the default vehicle page for MaC
        }

        public static ContentElement GetLinkElementForBrand(THLBrands brand, VehicleType vehicleType, CountryCode countryCode, string contentCode, DateTime pickUpDate, THLBrands containerBrand)
        {                    
            
            //added non THL owned brand exception 18/12/09
            bool isMaC = HttpContext.Current.Request.Url.Host.ToLower().Contains("motorhomesandcars");//TODO: get from containerBrand..
            if (isMaC)
            {

                ContentElement[] macCacheElements = ApplicationManager.GetMACContentElements();
                foreach (ContentElement elm in macCacheElements)
                {
                    if ((elm.StartTime < pickUpDate) && (elm.EndTime > pickUpDate) && (contentCode == elm.Code) && (elm.VehicleType == vehicleType) && (elm.VehicleType == vehicleType) && (brand == elm.Brand) && (countryCode == elm.Country))
                    {
                        ContentElement selected = elm;
                        selected.ContentType = ContentType.Link;
                        return selected;
                    }
                }
                //no match go ahead
                ContentElement macContentElm = new ContentElement();
                macContentElm.ContentType = ContentType.Link;
                macContentElm.Content = "insertMACContentLinkHere";//TODO Cache Query Here..
                return macContentElm;
            }
            //added non THL owned brand exception ends 18/12/09
            
            ContentElement contentElm = new ContentElement();
            contentElm.ContentType = ContentType.Link;

            ContentElement[] cacheElements = ApplicationManager.GetContentElements();
            
            foreach (ContentElement elm in cacheElements)
            {
                if ((elm.StartTime < pickUpDate) && (elm.EndTime > pickUpDate) && (contentCode == elm.Code) && (elm.VehicleType == vehicleType) && (elm.VehicleType == vehicleType) && (brand == elm.Brand) && (countryCode == elm.Country))
                {
                    ContentElement selected = elm;
                    selected.ContentType = ContentType.Link;
                    return selected;
                }
            }
            //---app level caching by brand comes here
            //TODO: this should be read from ContentStrings XML
            return contentElm;//TODO: consider token
        }

        /// <summary>
        /// Parse a content Element XML Node
        /// </summary>
        /// <param name="xmlNode">The Content Context to parse(Link/HTML)</param>
        /// <returns></returns>
        static ContentElement getLinkForXMLNode(XmlNode xmlNode, ContentType context)
        {            
            System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
            string format = "dd/MM/yy";            
            
            ContentElement contentElm = new ContentElement();
            contentElm.Content = "..";//TODO: consider token 
            contentElm.ContentType = ContentType.Link;
            //<link code="FerryCrossing" URL="http://www.maui.co.nz/ferry-ma-NZ.aspx"/>
            if (xmlNode.Attributes["code"] != null)
                contentElm.Code = xmlNode.Attributes["code"].Value;
            if (xmlNode.Attributes["url"] != null)
                contentElm.Content= xmlNode.Attributes["url"].Value;
            contentElm.VehicleType = VehicleType.Both;
            if (xmlNode.Attributes["type"] != null)
            {
                switch (xmlNode.Attributes["type"].Value)
                { 
                    case "ac":
                        contentElm.VehicleType = VehicleType.Car;
                        break;
                    case "av":
                        contentElm.VehicleType = VehicleType.Campervan;
                        break;                   
                }               
            }
            contentElm.StartTime = (xmlNode.Attributes["startDate"] != null ? DateTime.ParseExact(xmlNode.Attributes["startDate"].Value, format, provider) : DateTime.ParseExact("01/01/00", format, provider));
            contentElm.EndTime = (xmlNode.Attributes["endDate"] != null ? DateTime.ParseExact(xmlNode.Attributes["endDate"].Value, format, provider) : DateTime.ParseExact("01/01/2050", "dd/MM/yyyy", provider));
            return contentElm;
        }

        public static ContentElement GetMessageForCode(THLBrands brand, CountryCode countryCode, string code)
        { 
            
            ContentElement[] cacheElements = ApplicationManager.GetContentElements();

            ContentElement contentElm = new ContentElement();
            contentElm.ContentType = ContentType.HTML;
            foreach (ContentElement currentElm in cacheElements)
            {
                if ((currentElm.Code == code) && (brand == currentElm.Brand) && (countryCode == currentElm.Country))
                {
                    contentElm.Code = code;
                    contentElm.Content = currentElm.Content;
                }
            }
            
            //Globals Here
            switch (code.ToUpper())
            {
                case "BOOKINGCOMPLETED":
                    contentElm.Content = "This booking has already been completed.";
                    break;
                case "SESSIONEXPIRED":
                    contentElm.Content = "Booking Information has expired please search again.";
                    break;
            }           
            return contentElm;          
        }

        public static string GetConfigureB2BMessage(bool isGross, BookingStatus bookingStatus)
        {
            StringBuilder sb = new StringBuilder();
            switch (bookingStatus)
            {
                case BookingStatus.Provisional:
                    sb.Append("IMPORTANT NOTE: This is a PROVISIONAL BOOKING only, you MUST confirm this booking within 48 hours or it will be cancelled automatically. All current Provisional Bookings can be  found on the Quotes and Bookings page.<br />");
                    break;
                case BookingStatus.Confirmed:
                    sb.Append(string.Empty);
                    break;
                case BookingStatus.Quote:
                    sb.Append("Your saved quote will be valid for 1 week from today. Vehicle availability will be re-checked before your booking can be confirmed.<br />");
                    break;
            }
            string quotedText = sb.ToString();
            //if (isGross) Gross/Nett Ref removed
            //    sb.Append("<br />Prices displayed below are gross prices.");
            //else
            //    sb.Append("<br />Prices displayed below are nett prices.");
            if (!string.IsNullOrEmpty(quotedText))
                quotedText = "<div id=\"quoteDisclaimer\"><span>" + quotedText + "</span></div>";
            return quotedText;
        }
    }
}