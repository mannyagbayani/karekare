﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace THL.Booking
{
    
    /// <summary>
    /// Availability Response Structure, the product of a Cache Manager Request
    /// </summary>
    public class B2BResponseCacheItem
    {

        public B2BResponseCacheItem(AvailabilityItem[] items, bool isAlternativeResponse, string cacheKey)
        {
            IsAlternativeResponse = isAlternativeResponse;
            if (isAlternativeResponse)
            {
                _alternativeAvailabilityItems = items as AlternativeAvailabilityItem[];
            }
            else
            {
                _availabilityItems = items;
            }
            CacheKey = cacheKey;
            creationTime = DateTime.Now;
        }

        /// <summary>
        /// The Request Hashed value that represents the request parameters
        /// </summary>
        public string CacheKey { get; set; }

        /// <summary>
        /// Flag to determine if the response is an Alternative Availability Collection
        /// </summary>
        public bool IsAlternativeResponse { get; set; }

        /// <summary>
        /// The time stamp of this availability response, allows expiry of old Aurora availability collections
        /// </summary>
        private DateTime creationTime;

        public TimeSpan CacheAge { 
            get { return (DateTime.Now - creationTime);  } 
        }

        /// <summary>
        /// The returned Alternative Availability items
        /// </summary>
        AlternativeAvailabilityItem[] _alternativeAvailabilityItems;

        public AlternativeAvailabilityItem[] AltAvailabilityItems
        {
            get { return _alternativeAvailabilityItems; }
        }
        /// <summary>
        /// The returned Availability items
        /// </summary>
        AvailabilityItem[] _availabilityItems;

        public AvailabilityItem[] AvailabilityItems
        {
            get { return _availabilityItems; }
        }

    }

    /// <summary>
    /// Cache Managment functionality for B2B Booking logic, Cache and manage availability requests
    /// </summary>
    public partial class B2BSessionCache
    {
        
        List<AvailabilityItem>  inclusiveItems = new List<AvailabilityItem>();

        public int AddIncItem(AvailabilityItem availabilityItem)
        {
            inclusiveItems.Add(availabilityItem);
            return inclusiveItems.Count;
        }
        
        public AvailabilityItem GetIncItem(string avpId)
        {
            foreach (AvailabilityItem item in inclusiveItems)
                if (item.AVPID == avpId)
                    return item;
            return null;
        }

        
        public B2BResponseCacheItem GetLatestItem()
        {
            if (_responseCache != null && _responseCache.Length > 0)
            {
                var latest =
                    from res in _responseCache
                    orderby res.CacheAge
                    select res;
                return latest.Take<B2BResponseCacheItem>(1).ToArray()[0];
            }
            else
                return null;
        }
        

        /// <summary>
        /// Retrun a unique hash value based on the requests parameters
        /// </summary>
        /// <param name="request"></param>
        /// <returns>unique hash string</returns>
        public static string GetHashKeyForRequest(B2BAvailabilityRequest request)
        {
            //TODO: add additional parameter to the raw key (such as package, agent etc. on requirements)
            string raw = request.PickUpBranch + ":" + request.PickUpDate + ":" + request.DropOffBranch + ":" + request.DropOffDate + ":" + request.VehicleType + ":" + request.VehicleModel + ":" + request.AgentCode + ":" + request.Brand + ":"+ request.PackageCode + ":" + request.NumberOfAdults + ":" + request.NumberOfChildren + ":"  +  request.IsGross;
            
            //rev:mia april 16 2012
            raw = raw + ":" + request.CountryOfResidence;

            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(raw));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                    sBuilder.Append(data[i].ToString("x2"));              
                return sBuilder.ToString();
            }
        }

        /// <summary>
        /// Get The configured cache age in minutes from config, defualt to five if app settings entry is missing or not integer
        /// </summary>
        public static int GetMaxCachAge {
            get
            {
                string cacheAgeStr = System.Web.Configuration.WebConfigurationManager.AppSettings["AvailabilityCacheAge"];
                int cacheAgeMinute = 5;//default if no config defined.
                int.TryParse(cacheAgeStr, out cacheAgeMinute);
                return cacheAgeMinute;
            }
        }

        public string CurrentCahcheKey { get; set; }

        /// <summary>
        /// The Cached Availability entries 
        /// </summary>
        private B2BResponseCacheItem[] _responseCache;
        
        /// <summary>
        /// Get Availability Items for a given AvailabilityRequest instance, this method is a cache point
        /// and will ensure cached RAM return when possible
        /// TODO: define maxage and cast Alternate avaialbility (repository exists)
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Availablity results either from cache or dal if miss</returns>
        public AvailabilityItem[] GetAvailabilityResponse(B2BAvailabilityRequest request)
        {
            bool enableCache = true;//debug line, TODO: will come from application context and available by config or agent level
            TimeSpan maxAge = new TimeSpan(0, 15, 0);

            if (enableCache && _responseCache != null)
            {
                string requiredKey = B2BSessionCache.GetHashKeyForRequest(request);
                //Does this request exist in cache?
                for (int i = 0; i < _responseCache.Length; i++)
                {
                    if (_responseCache[i].CacheAge.Minutes >= B2BSessionCache.GetMaxCachAge)//expired, remove
                    {
                        List<B2BResponseCacheItem> cacheList = new List<B2BResponseCacheItem>(_responseCache);
                        cacheList.RemoveAt(i--);
                        _responseCache = cacheList.ToArray();
                    } else if (!_responseCache[i].IsAlternativeResponse && requiredKey.Equals(_responseCache[i].CacheKey))//enable alternative option once implemented
                    {
                        return _responseCache[i].AvailabilityItems;//Same Availability type same requst key Cache Match
                    }
                }
            }
            //else request from dal and append to cache            
            DataAccess dal = new DataAccess();
            AvailabilityResponse resp = dal.GetB2BAvailability(request);
            AvailabilityItem[] items = resp.GetAvailabilityItems(string.Empty);
            string key = B2BSessionCache.GetHashKeyForRequest(request);
            CurrentCahcheKey = key;
            B2BResponseCacheItem response = new B2BResponseCacheItem(items, false, key);
            List<B2BResponseCacheItem> cItems = _responseCache == null ? new List<B2BResponseCacheItem>() : new List<B2BResponseCacheItem>(_responseCache);
            cItems.Add(response);
            _responseCache = cItems.ToArray();
            return items;       
        }

        /// <summary>
        /// Get Availability Items for a given AvailabilityRequest instance, this method is a cache point
        /// and will ensure cached RAM return when possible
        /// TODO: define maxage and cast Alternate avaialbility (repository exists)
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Availablity results either from cache or dal if miss</returns>
        public AlternativeAvailabilityItem[] GetAltAvailabilityResponse(B2BAltAvailabilityRequest request)
        {
            bool enableCache = true;//debug line, TODO: will come from application context and available by config or agent level
            TimeSpan maxAge = new TimeSpan(0, 15, 0);

            if (enableCache && _responseCache != null)
            {
                string requiredKey = B2BSessionCache.GetHashKeyForRequest(request);
                //Does this request exist in cache?
                for (int i = 0; i < _responseCache.Length; i++)
                {
                    if (_responseCache[i].CacheAge.Minutes >= B2BSessionCache.GetMaxCachAge)//expired, remove
                    {
                        List<B2BResponseCacheItem> cacheList = new List<B2BResponseCacheItem>(_responseCache);
                        cacheList.RemoveAt(i--);
                        _responseCache = cacheList.ToArray();
                    }
                    else if (_responseCache[i].IsAlternativeResponse && requiredKey.Equals(_responseCache[i].CacheKey))//enable alternative option once implemented
                    {
                        return _responseCache[i].AltAvailabilityItems;//Same Availability type same requst key Cache Match
                    }
                }
            }
            //else request from dal and append to cache            
            DataAccess dal = new DataAccess();
            AlternativeAvailabilityItem[] items =  dal.GetB2BAlternateAvailability(request);
            string key = B2BSessionCache.GetHashKeyForRequest(request);
            CurrentCahcheKey = key;
            B2BResponseCacheItem response = new B2BResponseCacheItem(items, true, key);
            List<B2BResponseCacheItem> cItems = _responseCache == null ? new List<B2BResponseCacheItem>() : new List<B2BResponseCacheItem>(_responseCache);
            cItems.Add(response);
            _responseCache = cItems.ToArray();
            return items;
        }
        /// <summary>
        /// Get a cached availability based on its AvpID
        /// TODO: implement for Alternative Availability
        /// </summary>
        /// <param name="avpId"></param>
        /// <param name="IsAlternative"></param>
        /// <returns></returns>
        public AvailabilityItem GetAvailabilityForAVP(string avpId)
        {           
            for (int i = 0; i < _responseCache.Length; i++)
            {
                if (!_responseCache[i].IsAlternativeResponse && _responseCache[i].AvailabilityItems != null && _responseCache[i].AvailabilityItems.Length > 0)
                {
                    foreach (AvailabilityItem item in _responseCache[i].AvailabilityItems)
                        if (!string.IsNullOrEmpty(item.AVPID) && item.AVPID.Equals(avpId))
                        {
                            item.IsAlternative = false;
                            return item;
                        }
                }
                else if (_responseCache[i].IsAlternativeResponse && _responseCache[i].AltAvailabilityItems != null && _responseCache[i].AltAvailabilityItems.Length > 0)
                {
                    foreach (AlternativeAvailabilityItem item in _responseCache[i].AltAvailabilityItems)
                        if (!string.IsNullOrEmpty(item.AVPID) && item.AVPID.Equals(avpId))
                        {
                            item.IsAlternative = true;
                            return item;
                        }
                }
            }           
            return null;
        }


        /// <summary>
        /// Quote the Availability Cache State in JSON 
        /// </summary>
        /// <returns></returns>
        public string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (_responseCache != null && _responseCache.Length > 0)
            {
                foreach (B2BResponseCacheItem cacheItem in _responseCache)
                {
                    sb.Append("{\"cacheKey\":\"" + cacheItem.CacheKey + "\",\"cacheAge\":\"" + cacheItem.CacheAge + "\",[");
                    int i = 0;
                    foreach (AvailabilityItem item in cacheItem.AvailabilityItems)
                    {
                        sb.Append("{\"avpId\":\"" + item.AVPID + " \"}");
                        if (++i != cacheItem.AvailabilityItems.Length)
                            sb.Append(",");
                    }
                    sb.Append("]},");
                }
                //TODO: the same for Alternate availabilty

            }
            else
                sb.Append("{\"msg\":\"availability cache is empty\"}");
            return sb.ToString().TrimEnd(',');
        }
    }
}