﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace THL.Booking
{

    public enum TransactionType
    {
        Purchase = 0,
        Auth = 1
    }
    
    /// <summary>
    /// Credit Card Payment/Authrization Transaction Structure
    /// </summary>
    public class Transaction
    {
        string err;
        public string ErrorMessage
        {
            get { return err; }
        }

        string bookingReference;
        
        public string BookingReference
        {
            get { return bookingReference; }
        }
        
        string dpsResponseText;
        public string DPSResponseText
        {
            get { return dpsResponseText; }
            set { dpsResponseText = value; }
        }

        string dpsHelpText;
        public string DPSHelpText
        {
            get { return dpsHelpText; }
            set { dpsHelpText = value; }
        }

        decimal amount;
        /// <summary>
        /// Transaction Total Amount
        /// </summary>
        public decimal Amount
        {
            get { return amount; }
        }

        /// <summary>
        /// Returned DPS Authorization Code
        /// </summary>
        string authCode;
        public string AutorizationCode
        {
            get { return authCode; }
            set { authCode = value; }
        }

        string creditCardNumber;
        /// <summary>
        /// Transaction Credit Card Number
        /// </summary>
        public string CreditCardNumber
        {
            get { return creditCardNumber; }
        }

        string cardHolderName;
        /// <summary>
        /// Transaction C/C holder name
        /// </summary>
        public string CardHolderName
        {
            get { return cardHolderName; }
        }

        /// <summary>
        /// Transaction C/C Expiry Date
        /// </summary>
        string expiryDate;
        public string ExpiryDate
        {
            get { return expiryDate; }
        }

        string transactionCode;
        /// <summary>
        /// DPS Transaction Code
        /// </summary>
        public string TransactionCode
        {
            get { return transactionCode; }
            set { transactionCode = value; }
        }

        string[] textLines;
        /// <summary>
        /// Transaction description Text Lines
        /// </summary>
        public string[] TextLines
        {
            get { return textLines; }
            set { textLines = value; }
        }

        /// <summary>
        /// The Generated Booking ID for a successful transaction
        /// </summary>
        string auroraBookingId;
        public string AuroraBookingId
        {
            get { return auroraBookingId; }
            set { auroraBookingId = value; }
        }


        TransactionType transactionType;
        public TransactionType Type
        {
            get { return transactionType; }
        }
                

        public Transaction(decimal _amount, string _cardNumber, string _cardHolderName, string _expiryDate, string _bookingReference, TransactionType _type)
        {
            err = string.Empty;
            amount = _amount;
            creditCardNumber = _cardNumber;
            cardHolderName = _cardHolderName;
            expiryDate = _expiryDate;
            bookingReference = _bookingReference;
            transactionType = _type;
        }


        public Transaction(AuroraBooking bookingData)
        { 
            //TODO: collect financial data from booking data object and init THIS
        }



        /// <summary>
        /// Validate a given Credit Card number
        /// </summary>
        /// <param name="creditCardNumber"></param>
        /// <returns></returns>
        public static bool ValidateCreditCardNumber(string creditCardNumber)
        {
            return true;//deprecated, TODO: consider refactoring
            /*
            creditCardNumber = Regex.Replace(creditCardNumber, @"[^0-9]", "");
            int cardSize = creditCardNumber.Length;
            if (cardSize >= 13 && cardSize <= 16)
            {
                int odd = 0;
                int even = 0;
                char[] cardNumberArray = new char[cardSize];
                cardNumberArray = creditCardNumber.ToCharArray();
                Array.Reverse(cardNumberArray, 0, cardSize);
                for (int i = 0; i < cardSize; i++)
                {
                    if (i % 2 == 0)
                    {
                        odd += (Convert.ToInt32(cardNumberArray.GetValue(i)) - 48);
                    }
                    else
                    {
                        int temp = (Convert.ToInt32(cardNumberArray[i]) - 48) * 2;
                        if (temp > 9)
                        {
                            temp = temp - 9;
                        }
                        even += temp;
                    }
                }
                if ((odd + even) % 10 == 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
            */ 
        }

        /// <summary>
        /// Validate a given Expiry date (MMYY) string
        /// </summary>
        /// <param name="expiryDate"></param>
        /// <returns></returns>
        public static bool ValidateExpiryDate(string expiryDate)
        {
            int month, year;
            if (expiryDate.Length == 4 && int.TryParse(expiryDate.Substring(0, 2), out month) && int.TryParse(expiryDate.Substring(2, 2), out year))
            {//parsed values succefully 
                return (month > 0 && month <= 12 && year < 99);//TODO: consider range for year
            }
            else
                return false;
        }
    }
}