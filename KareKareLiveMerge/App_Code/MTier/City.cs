﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;


namespace THL.Booking {

    /// <summary>
    /// City Instance for Aurora Master XML City Entity
    /// </summary>
    public class City
    {

        private string _cityName;
        public string Name
        {
            get { return _cityName;  }
        }

        private string _cityCode;
        public string Code
        {
            get { return _cityCode; }
        }

        public City()
	    {
		    
	    }


        public bool LoadFromXml(XmlNode cityNode) 
        {

            try// to parse this <city code="ADL" name="Adelaide">
            {
                _cityCode = cityNode.Attributes["code"].Value;
                _cityName = cityNode.Attributes["name"].Value;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;//TODO: on loging = true
                return false;
            }
            return true;
        }
    
    }
}