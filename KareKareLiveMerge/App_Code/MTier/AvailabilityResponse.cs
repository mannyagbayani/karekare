﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking
{
    /// <summary>
    /// Availability response items collection
    /// </summary>
    public class AvailabilityResponse
    {
        string requstingAgentCode;
        public string AgentCode
        {
            get { return requstingAgentCode; }
        }

        
        /// <summary>
        /// Added for Alternate Availability Support 
        /// </summary>
        /// <param name="item">The Alternative Item to Add</param>
        public void SetAvailabilityItems(AvailabilityItem[] items)
        {          
            availabilityElements = items;
        }

        public void AddAltAvailabilityItems(AlternativeAvailabilityItem[] items)
        {
            List<AvailabilityItem> aitem = availabilityElements.ToList();
            aitem.RemoveAll(IsAltAvailability);
            aitem.AddRange(items);
            availabilityElements = aitem.ToArray();
        }

        bool IsAltAvailability(object item)
        {
            return (item.GetType() == typeof(AlternativeAvailabilityItem));
        }

        AvailabilityItem[] availabilityElements;
        
        public AvailabilityItem[] GetAvailabilityItems(string sortOrder) {
            //if (sortOrder == "price")
            //{
            //    var sorted = from s in availabilityElements
            //               orderby s.GetTotalPrice()
            //               select s;

            //    return sorted.ToArray();
            //}           
            return availabilityElements;
        }

        /// <summary>
        /// Added for B2B support with sorting capabilities
        /// </summary>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public AvailabilityItem[] GetAvailabilityItems(string sortOrder, int maxResult)
        {
            //if (sortOrder == "price")
            //{
            //    var sorted = from s in availabilityElements
            //               orderby s.GetTotalPrice()
            //               select s;

            //    return sorted.ToArray();
            //}           
            return availabilityElements;
        }



        /// <summary>
        /// Get a specific Availability Item for a provided AVP Id
        /// </summary>
        /// <param name="avpId">Aurora temporary AVPID</param>
        /// <returns></returns>
        public AvailabilityItem GetAvailabilityItem(string avpId)
        {
            foreach(AvailabilityItem aItem in availabilityElements) 
            {
                if (aItem.AVPID == avpId)
                    return aItem;
            }
            return null;
        }

        /// <summary>
        /// Update Data of a defined Availability Item
        /// Added for alternate availability support
        /// </summary>
        /// <param name="avpId">Identifier of the updated Item</param>
        /// <param name="updatedItem">The Updated Item Data</param>
        /// <returns></returns>
        public bool UpdateAvailabilityItem(string avpId, AvailabilityItem updatedItem)
        {
            for(int i=0; i< availabilityElements.Length; i++)           
            {
                if (availabilityElements[i].AVPID == avpId)
                    availabilityElements[i] = updatedItem;
                return true;
            }
            return false;
        }

        /// <summary>
        /// The Responses Currency
        /// </summary>
        public CurrencyCode Currency;
        
        /// <summary>
        /// Used For Querying availability item when no Backend AVP Exists 
        /// </summary>
        /// <param name="vehicleCode"></param>
        /// <returns></returns>
        public AvailabilityItem GetAvailabilityItemForVehicleCode(string vehicleCode)
        {
            foreach (AvailabilityItem aItem in availabilityElements)
            {
                if (aItem.VehicleCode == vehicleCode)
                    return aItem;
            }
            return null;        
        }
        
        public AvailabilityResponse(string _requstingAgentCode)
        {
            requstingAgentCode = _requstingAgentCode;
        }
                
        /// <summary>
        /// Fill the Response Object from Aurora response XML
        /// </summary>
        /// <param name="responseXml">Aurora Response XML Document</param>
        /// <param name="brandCode">Querying Brand (used for error rows TODO:remove this once error comes with branding</param>
        /// <returns></returns>
        public bool FillFromXML(XmlDocument responseXml, string brandCode) {//TODO: remove brandCode and enable 
            XmlNodeList avalabilityRowXml = responseXml.SelectNodes("data/AvailableRate");
            List<AvailabilityItem> availabilityElementsList = new List<AvailabilityItem>();
            bool success = true;
            foreach (XmlNode avalabilityRowXmlNode in avalabilityRowXml)//Each Availability Package <availability>
            {
                string avp, packageCode, packageDesc, brandCodeStr, packageComment, packageTCUrl, packageInfoURL,cki,cko;
                bool isLumpSum;
                decimal depositAmount = 0, depositPercentage = 0, estimatedTotal = 0, payableToAgent = 0, payableAtPickup = 0; ;                
                //try {
                XmlNode packageNode = avalabilityRowXmlNode.SelectSingleNode("Package");//load package Info
                if (packageNode.Attributes["AvpId"] == null) {                    
                    AvailabilityItem ai = new AvailabilityItem();//string.Empty, packageCode, packageDesc, brandCodeStr,string.Empty,string.Empty,string.Empty,0.0m, 0.0m, false,string.Empty,string.Empty,0.0m,null);
                    ai.Brand = THLBrand.GetBrandForString(packageNode.Attributes["BrandCode"].Value);//enable this instead: ai.Brand = packageNode.Attributes["BrandCode"].Value;
                    XmlNode errorsNode = avalabilityRowXmlNode.SelectSingleNode("Errors");//load package Info

                    bool errorsLoaded = loadErrorMessages(ref ai, errorsNode);

                    ai.BlockingRuleMessage = errorsNode.SelectSingleNode("BlockingRuleMessage").Attributes["Message"].Value;
                    ai.ErrorMessage = errorsNode.SelectSingleNode("ErrorMessage").Attributes["Message"].Value;
                    XmlNode chargeCollection = avalabilityRowXmlNode.SelectSingleNode("Charge/Det");//load Charges
                    if (chargeCollection != null && chargeCollection.Attributes["PrdCode"].Value != null)
                        ai.VehicleCode =  chargeCollection.Attributes["PrdCode"].Value;
                    if (chargeCollection != null && chargeCollection.Attributes["PrdName"].Value != null)
                        ai.VehicleName = chargeCollection.Attributes["PrdName"].Value;                   
                    availabilityElementsList.Add(ai);
                }
                else
                {                                    
                    avp = packageNode.Attributes["AvpId"].Value;
                    packageCode = packageNode.Attributes["PkgCode"].Value;
                    packageDesc = packageNode.Attributes["PkgDesc"].Value;
                    brandCodeStr = packageNode.Attributes["BrandCode"].Value;
                    packageComment = packageNode.Attributes["PkgComment"].Value;
                    packageTCUrl = packageNode.Attributes["PkgTCURL"].Value;
                    packageInfoURL = packageNode.Attributes["PkgInfoURL"].Value;

                    isLumpSum = (packageNode.Attributes["IsLumpSum"].Value.Equals("Yes") ? true : false);
                    decimal.TryParse(packageNode.Attributes["EstimatedTotal"].Value, out estimatedTotal);
                    decimal.TryParse(packageNode.Attributes["PayableToAgent"].Value, out payableToAgent);
                    decimal.TryParse(packageNode.Attributes["PayableAtPickup"].Value, out payableAtPickup);
                    
                    //get pickUp/DropOff locations
                    XmlNode locationXml = avalabilityRowXmlNode.SelectSingleNode("Location");//load Charges
                    cki = "";// locationXml.SelectSingleNode("Cki").InnerText;
                    cko = "";// locationXml.SelectSingleNode("Cko").InnerText;             
                    // catch (log and set success = false                
                    success = decimal.TryParse(packageNode.Attributes["DepositAmt"].Value, out depositAmount);
                    if (packageNode.Attributes["DepositPercentage"] != null)//TODO define optionals here
                        success = decimal.TryParse(packageNode.Attributes["DepositPercentage"].Value.Replace('%', ' '), out depositPercentage);
                    //try
                    XmlNodeList chargeCollection = avalabilityRowXmlNode.SelectNodes("Charge/Det");
                    decimal dailyAvgRate = 0;
                    List<AvailabilityItemChargeRow> aicList = new List<AvailabilityItemChargeRow>();
                    
                    string vehicleMsg = string.Empty;

                    bool hasBonusPack = false; 
                    decimal adminFee = 0.0m;

                    string flexCode = string.Empty;

                    foreach (XmlNode chargeXml in chargeCollection)
                    {// Each Charge <Det>                                        

                        if (chargeXml.Attributes["FlxNum"] != null && !string.IsNullOrEmpty(chargeXml.Attributes["FlxNum"].Value))
                            flexCode = chargeXml.Attributes["FlxNum"].Value;
                        
                        decimal.TryParse(chargeXml.Attributes["AvgRate"].Value, out dailyAvgRate);
                        AvailabilityItemChargeRow aic = new AvailabilityItemChargeRow();
                        aic.LoadFromXml(chargeXml);
                        aicList.Add(aic);
                        if (aic.AdminFee > 0) adminFee = aic.AdminFee;
                        if (aic.IsBonusPack) hasBonusPack = true;
                        if (chargeXml.Attributes["ProductInfo"] != null && chargeXml.Attributes["ProductInfo"].Value != string.Empty)
                            vehicleMsg = chargeXml.Attributes["ProductInfo"].Value;
                    }
                    AvailabilityItem ai = new AvailabilityItem(avp, packageCode, packageDesc, brandCodeStr, packageComment, packageTCUrl, packageInfoURL, depositAmount, depositPercentage, isLumpSum, cko, cki, dailyAvgRate, aicList.ToArray());
                    ai.AdminFee = adminFee;

                    ai.DisplayPromoTile = (packageNode.Attributes["DispTile"] != null && packageNode.Attributes["DispTile"].Value == "Yes");

                    ai.FlexCode = flexCode;

                    ai.IsInclusive = !string.IsNullOrEmpty(packageNode.Attributes["PkgIsInclusive"].Value) && packageNode.Attributes["PkgIsInclusive"].Value.ToUpper().Equals("YES");
                    
                    //Added Description for agent reference, 28/11/2011
                    ai.PackageDescription = packageNode.Attributes["PkgDesc"] != null && !string.IsNullOrEmpty(packageNode.Attributes["PkgDesc"].Value) ? packageNode.Attributes["PkgDesc"].Value : string.Empty;

                    ai.PromoText = packageNode.Attributes["PromoText"] != null && !string.IsNullOrEmpty(packageNode.Attributes["PromoText"].Value) ? packageNode.Attributes["PromoText"].Value : string.Empty;

                    ai.PackageName = packageComment;//TODO: swap to Package Name once populated

                    //NRMA Loyalty card support
                    ai.DiplayLoyaltyCard = (packageNode.Attributes["DispLoylCrdFld"] != null && packageNode.Attributes["DispLoylCrdFld"].Value == "Yes");

                    ai.PayableAtPickup = payableAtPickup;

                    ai.PayableToAgent = payableToAgent;

                    //Added for B2B enhancment gross support
                    if (packageNode.Attributes["CanGetGross"] != null && !string.IsNullOrEmpty(packageNode.Attributes["CanGetGross"].Value) && packageNode.Attributes["CanGetGross"].Value.ToUpper().Equals("YES"))
                        ai.CanGetGross = true;
                    
                    //031109 
                    XmlNode onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYAU']");//TODO: get this not by code from Aurora
                    if(onewayFeeXMLNode == null)
                        onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYNZ']");//TODO: get this not by code from Aurora
                   
                    if (onewayFeeXMLNode == null)
                        onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYE']");//TODO: get this not by code from Aurora

                    if (onewayFeeXMLNode == null)
                        onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWQ']");//TODO: get this not by code from Aurora
                   
                    decimal onewayFee = 0.0m; 
                    if (onewayFeeXMLNode != null && onewayFeeXMLNode.Attributes["Price"] != null)
                        decimal.TryParse(onewayFeeXMLNode.Attributes["Price"].Value, out onewayFee);
                    
                    ai.OneWayFeeComponent = onewayFee;
                    //031109

                    ai.AvailabilityType = AvailabilityItemType.Available;
                    XmlNode errorsNode = avalabilityRowXmlNode.SelectSingleNode("Errors");//load package Info
                    bool errorsLoaded = loadErrorMessages(ref ai, errorsNode);                    
                    
                    ai.IncludesBonusPack = hasBonusPack;
                    ai.VehicleMsgText = vehicleMsg;
                    ai.EstimatedTotal = estimatedTotal;
                    
                    availabilityElementsList.Add(ai);
                }
            }
            availabilityElements = availabilityElementsList.ToArray();
            return success ;//on successful parse return true
        }

        /// <summary>
        /// Update Error Messages and set Error status type.
        /// </summary>
        /// <param name="availabilityItem"></param>
        /// <param name="errorNode"></param>
        /// <returns></returns>
        bool loadErrorMessages(ref AvailabilityItem availabilityItem , XmlNode errorNode)
        {
            //<ErrorMessage Allow='true|false' Message='systemMsgStr'/>
            //<BlockingRuleMessage Allow='true|false' Message='MsgStr'/>
            bool hasAVPID = (availabilityItem.AVPID != null && availabilityItem.AVPID.Length > 0);             
            bool allowOnBlockingRule = false, allowOnError = false;
            string errMsg = errorNode.SelectSingleNode("ErrorMessage").Attributes["Message"].Value;
            string blockingMsg = errorNode.SelectSingleNode("BlockingRuleMessage").Attributes["Message"].Value;
            bool.TryParse(errorNode.SelectSingleNode("ErrorMessage").Attributes["Allow"].Value, out allowOnError);
            bool.TryParse(errorNode.SelectSingleNode("BlockingRuleMessage").Attributes["Allow"].Value, out allowOnBlockingRule);
            bool hasErrMessage = (errMsg != null && errMsg.Length > 0);
            bool hasBlockingMessage = (blockingMsg != null && blockingMsg.Length > 0);
            int availabilityItemFlags = 0;
            availabilityItemFlags = (hasBlockingMessage ? availabilityItemFlags | 1 : availabilityItemFlags);
            availabilityItemFlags = (hasErrMessage ? availabilityItemFlags | 2 : availabilityItemFlags);
            availabilityItemFlags = (allowOnError ? availabilityItemFlags | 4 : availabilityItemFlags);
            availabilityItemFlags = (allowOnBlockingRule ? availabilityItemFlags | 8 : availabilityItemFlags);
            availabilityItemFlags = (hasAVPID ? availabilityItemFlags | 16 : availabilityItemFlags);
            switch (availabilityItemFlags)
            { 
                case 16:
                    availabilityItem.AvailabilityType = AvailabilityItemType.Available;
                    break;
                case 1:
                    availabilityItem.AvailabilityType = AvailabilityItemType.BlockingRule;
                    break;
                case 6:
                    availabilityItem.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                    break;
                case 2:
                    availabilityItem.AvailabilityType = AvailabilityItemType.SystemMessage;
                    break;
                case 25:
                    availabilityItem.AvailabilityType = AvailabilityItemType.OnRequest;
                    break;
                case 9://Added for supporting Thrifty response case :
                    //<BlockingRuleMessage Allow="true" Message="Msg"/>
                    //  <ErrorMessage Allow="" Message=""/>
                    availabilityItem.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                    break;
            }            
            return true;//Exp Handle and return status on failure
        }
    }
}