﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// Booking Domain Representation
    /// </summary>
    public class THLDomain
    {

        THLBrands _brand;
        public THLBrands Brand
        {
            get {return _brand; }        
        }

        public char BrandChar
        {
            get { return THLBrand.GetBrandChar(_brand); }
        }
        
        bool _isAggregator;
        public bool IsAggregator
        {
            get { return _isAggregator; }
        }

        string _name;
        public string Name
        {
            get { return _name; }
        }

        public string ParentSite
        {
            get { return "http://www" +  _name.Substring(_name.IndexOf('.')); }         
        }

        CountryCode _country;
        public CountryCode Country
        {
            get { return _country; }
            set { _country = value; }
        }

        string _agentCode;
        public string AgentCode
        {
            get { return _agentCode;  }
        }


        string _uaCode;
        public string UACode
        { 
            get { return _uaCode; } 
        }


        public THLDomain(string name, THLBrands brand, CountryCode country, string agentCode, bool isAggregator, string uaCode)
        {
            _name  = name;
            _brand = brand;
            _country = (isAggregator ? CountryCode.NONE : country);
            _agentCode = agentCode;
            _isAggregator = isAggregator;
            _uaCode = uaCode;
        }

        public override string ToString()
        {
            return "<domain><name>"+ _name +"</name><brand>"+ _brand +"</brand><country>"+ _country +"</country><agentcode>"+ _agentCode +"</agentcode><isaggregator>"+ _isAggregator +"</isaggregator></domain>";        
        }
    }
}