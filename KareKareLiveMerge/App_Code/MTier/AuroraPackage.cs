﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections.Specialized;
using System.Xml.Serialization;
using System.IO;
using System.Text;
namespace THL.Booking
{
    /// <summary>
    /// Aurora Package Representation returned from package query on AVP ID identifier 
    /// </summary>
    public class AuroraPackage
    {

        /// <summary>
        /// Added for supporting caching of associative Inclusive Data
        /// </summary>
        public string IncPackageXml { get; set; }

        /// <summary>
        /// Aurora Identifier
        /// </summary>
        private string avpId;
        public string AvpID { 
            get { return avpId; }
        }


        private ExtraHireItem[] extraHireItems;
        public ExtraHireItem[] GetExtraHireItems()
        {
            return extraHireItems;
        }

        public AuroraPackage(string _avpId)
        {
            avpId = _avpId;
        }

        /// <summary>
        /// Added to support Road Charge Title from Web Service
        /// </summary>
        public string DTRTitle;

        private InsuranceProduct[] insuranceOptions;
        public InsuranceProduct[] GetInsuranceProducts()
        {
            return insuranceOptions;
        }

        public InsuranceProduct GetBonusPack()
        {
            foreach (InsuranceProduct insurance in insuranceOptions)
            {
                if (insurance.IsInclusive)
                    return insurance; 
            }
            return null;
        }
        
        private FerryProduct[] ferryOptions;

        public PersonRateInfo[] GetFerryRateList()
        {
            List<PersonRateInfo> ratesList = new List<PersonRateInfo>();
            foreach (FerryProduct ferryProduct in ferryOptions)
            {
                if(ferryProduct.FerryData != null && ferryProduct.FerryData.PersonRateCollection != null)
                    ratesList.AddRange(ferryProduct.FerryData.PersonRateCollection); 
            }
            //TODO: merge passenger types..

            return ratesList.ToArray();
        }
        
        public FerryProduct[] GetFerryProducts()
        {
            return ferryOptions;
        }
                
        public string InclusiveAVPId;
        
        public string GetInclusiveItemAvpID()
        {
            return (InclusiveAvailItem != null ?  InclusiveAvailItem.AVPID : null);
        }

        CreditCardElement[] availableCardType;
        public CreditCardElement[] AvailableCardType
        { 
            get { return availableCardType; }
        }

        public decimal AdminFee = 0.0m;
        public decimal DTRCharge = 0.0m;

        bool _hasDTR = false;
        public bool HasDTR 
        {
            get { return _hasDTR; }
        }

        public AvailabilityItem InclusiveAvailItem;

        //------added to handle inclusive data at package
        public decimal InclusiveDayPrice;
        public decimal InclusiveEstimatedTotal;
        public string InclusiveName;
        //-----------------------------------------------

        private bool hasInclusivePackage;
        public bool HasInclusivePackage
        {
            get { return hasInclusivePackage; }
        }

        public bool LoadFromXML(XmlDocument xmlDoc, ApplicationData appData)
        {
            bool IsGrossRequest = false;
            decimal NettAmt = 0.0m;
            decimal NettDailyRate = 0.0m;
            string productIdInUrl  = string.Empty;

            try
            {
                AvailabilityRequest CurrentRequest = SessionManager.GetSessionData().AvailabilityRequestData;
                IsGrossRequest = (CurrentRequest as B2BAvailabilityRequest).b2bParams.isGross;

                if(System.Web.HttpContext.Current.Request["prdId"] != null)
                {
                    productIdInUrl = System.Web.HttpContext.Current.Request["prdId"];
                }
            }
            catch (Exception ex)
            { }
            


            XmlNodeList productNodes = xmlDoc.SelectNodes("data/ConfigurationProducts/ExtraHireItems/Product");
            List<ExtraHireItem> extraHiresList = new List<ExtraHireItem>();
            //load ExtraHireItems
            string code, name, sapId, info;
            int displayOrder, minAge, maxAge;
            decimal grossAmt, dailyRate, maxCharge;
            UOMType uom;
            bool promptForQty;
            bool success = true;

            XmlNode incPackageXML = xmlDoc.SelectSingleNode("//InclusiveRate/AvailableRate");
            IncPackageXml = incPackageXML != null ? incPackageXML.OuterXml : string.Empty;
            
            //TODO: try catch and set success
            foreach (XmlNode ehiNode in productNodes)
            {
                code = ehiNode.Attributes["Code"].Value;
                name = ehiNode.Attributes["Name"].Value;

                info = ehiNode.Attributes["PrdInfo"].Value;
                
                sapId = ehiNode.Attributes["SapId"].Value;
                success = int.TryParse(ehiNode.Attributes["DisplayOrder"].Value, out  displayOrder);
                success = int.TryParse(ehiNode.Attributes["MinAge"].Value, out  minAge);
                success = int.TryParse(ehiNode.Attributes["MaxAge"].Value, out  maxAge);
                success = decimal.TryParse(ehiNode.Attributes["GrossAmt"].Value, out  grossAmt);
                success = decimal.TryParse(ehiNode.Attributes["MaxCharge"].Value, out maxCharge);
                success = decimal.TryParse(ehiNode.Attributes["dailyRate"].Value, out dailyRate);
                promptForQty = (ehiNode.Attributes["PromptQty"].Value == "Yes");

                if (ehiNode.Attributes["IsDTR"] != null && ehiNode.Attributes["IsDTR"].Value.Equals("Yes"))
                {//mine DTR and exclude

                    DTRTitle = (ehiNode.Attributes["Name"] != null ? ehiNode.Attributes["Name"].Value : "Road User Charge Recovery Fee");

                    decimal.TryParse(ehiNode.Attributes["IsDTR"].Value, out DTRCharge);
                    _hasDTR = true;                    
                    continue;
                }
                switch (ehiNode.Attributes["UOM"].Value)
                {
                    case null:
                        uom = UOMType.None;
                        break;
                    case "DAY":
                        uom = UOMType.Day;
                        break;
                    case "24HR":
                        uom = UOMType.Day;
                        break;
                    default:
                        uom = UOMType.None;
                        break;
                }
                //uom = (ehiNode.Attributes["UOM"].Value == "DAY") ? UOMType.Day;
                ExtraHireItem currentEhi = new ExtraHireItem(code, name, dailyRate, uom, grossAmt, maxCharge, sapId, displayOrder, promptForQty, minAge, maxAge);
                
                currentEhi.ProductInfo = info;

                extraHiresList.Add(currentEhi);
            }
            extraHireItems = extraHiresList.ToArray();
            //end load Extra Hire Items

            //load Ferry Information, TODO: define convention on ferry products.. 
            XmlNodeList ferryNodes = xmlDoc.SelectNodes("data/ConfigurationProducts/FerryCrossing/Product");
            List<FerryProduct> ferryProductsList = new List<FerryProduct>();
            if (ferryNodes != null && ferryNodes.Count > 0)//has ferrys
            {             
                foreach (XmlNode ferryNode in ferryNodes)
                {
                    code = ferryNode.Attributes["Code"].Value;
                    name = ferryNode.Attributes["Name"].Value;
                    if (code.Contains("PAX"))//TODO: missing naming convention
                    {
                        success = int.TryParse(ferryNode.Attributes["DisplayOrder"].Value, out  displayOrder);
                        DateTime rateStart = DateTime.Now, rateEnd = DateTime.Now;//TODO: get from Aurora
                        XmlNodeList personRateNodes = ferryNode.SelectNodes("//PersonRate");
                        List<PersonRateInfo> personRateList = new List<PersonRateInfo>(); 
                        foreach (XmlNode personRateNode in personRateNodes) 
                        {
                            decimal prrRate = 0;
                            PersonRateInfo currentRate = new PersonRateInfo();
                            currentRate.PersonId = personRateNode.Attributes["PrrId"].Value;
                            string paxTypeStr = personRateNode.Attributes["PaxType"].Value;
                            currentRate.PersonInfoType = PersonRateInfo.GetTypeForAuroraString(paxTypeStr);
                            success = decimal.TryParse(personRateNode.Attributes["PrrRate"].Value, out  prrRate);
                            currentRate.Rate = prrRate;
                            currentRate.SapId = personRateNode.Attributes["SapId"].Value;
                            personRateList.Add(currentRate);
                        }
                        PersonRateInfo[] rateCollection = personRateList.ToArray();
                        FerryCrossingData fcd = new FerryCrossingData(code, name, rateStart, rateEnd, rateCollection);//todo: from xml
                        FerryProduct currentFerryPrd = new FerryProduct(code, name, fcd);
                        ferryProductsList.Add(currentFerryPrd);
                    }
                    else if (code.Contains("VEH"))//TODO: missing naming convention requires Biz Logic Flag
                    {                        
                        sapId = ferryNode.Attributes["SapId"].Value;
                        success = int.TryParse(ferryNode.Attributes["DisplayOrder"].Value, out  displayOrder);
                        success = int.TryParse(ferryNode.Attributes["MinAge"].Value, out  minAge);
                        success = int.TryParse(ferryNode.Attributes["MaxAge"].Value, out  maxAge);
                        success = decimal.TryParse(ferryNode.Attributes["GrossAmt"].Value, out  grossAmt);
                        success = decimal.TryParse(ferryNode.Attributes["MaxCharge"].Value, out maxCharge);
                        success = decimal.TryParse(ferryNode.Attributes["dailyRate"].Value, out dailyRate);
                        promptForQty = (ferryNode.Attributes["PromptQty"].Value == "Yes");
                        uom = (ferryNode.Attributes["UOM"].Value == "DAY") ? UOMType.Day : UOMType.None;
                        DateTime rateStart = DateTime.Now, rateEnd = DateTime.Now;
                        FerryProduct currentFerryPrd = new FerryProduct(code, name, dailyRate, uom, grossAmt, maxCharge, sapId, displayOrder, promptForQty, minAge, maxAge);
                        ferryProductsList.Add(currentFerryPrd);
                    }                
                }
                ferryOptions = ferryProductsList.ToArray();
            }//end load Ferry
            
            //Load Insurance         
            XmlNodeList insuranceNodes = xmlDoc.SelectNodes("data/ConfigurationProducts/Insurence/Product");
            List<InsuranceProduct> insuarnceProductsList = new List<InsuranceProduct>();
            bool isInclusive;

            //-----------------------------------------------------------------------------------------------------
            //rev:mia MAY 8 2013  - PART OF APCOVER ENHANCEMENT. 
            //-----------------------------------------------------------------------------------------------------
            string ProductCodeWithrequiredAction = string.Empty;
            string ProductCodeWithInclusive = string.Empty;
            //-----------------------------------------------------------------------------------------------------
            if (insuranceNodes != null && insuranceNodes.Count > 0)//has insurance items
            {             
                foreach (XmlNode insuranceNode in insuranceNodes) 
                {   
                    code = insuranceNode.Attributes["Code"].Value;
                    name = insuranceNode.Attributes["Name"].Value;
                    success = int.TryParse(insuranceNode.Attributes["DisplayOrder"].Value, out  displayOrder);
                    sapId = insuranceNode.Attributes["SapId"].Value;
                    success = int.TryParse(insuranceNode.Attributes["DisplayOrder"].Value, out  displayOrder);
                    success = int.TryParse(insuranceNode.Attributes["MinAge"].Value, out  minAge);
                    success = int.TryParse(insuranceNode.Attributes["MaxAge"].Value, out  maxAge);
                    success = decimal.TryParse(insuranceNode.Attributes["GrossAmt"].Value, out  grossAmt);
                    success = decimal.TryParse(insuranceNode.Attributes["MaxCharge"].Value, out maxCharge);
                    success = decimal.TryParse(insuranceNode.Attributes["dailyRate"].Value, out dailyRate);
                    isInclusive = (insuranceNode.Attributes["IsInclusive"].Value == "Yes");
                    
                    promptForQty = (insuranceNode.Attributes["PromptQty"].Value == "Yes");                    
                    uom = (insuranceNode.Attributes["UOM"].Value == "DAY") ? UOMType.Day : UOMType.None;

                    XmlNode excessReductionNode = insuranceNode.SelectSingleNode("Excess");
                    if (excessReductionNode != null)//dig the age params from the excess reduction sub node 
                    {
                        success = int.TryParse(excessReductionNode.Attributes["MinAge"].Value, out  minAge);
                        success = int.TryParse(excessReductionNode.Attributes["MaxAge"].Value, out  maxAge);
                        code = code + "_" + excessReductionNode.Attributes["Code"].Value;//differ ER0s workaround
                    }

                    InsuranceProduct currentInsurance = new InsuranceProduct(code, name, dailyRate, uom, grossAmt, maxCharge, sapId, displayOrder, promptForQty, minAge, maxAge);
                    currentInsurance.IsInclusive = isInclusive;
                    XmlNodeList requiredActionsCollection = insuranceNode.SelectNodes("RequiredActions/Action");
                    if (requiredActionsCollection != null && requiredActionsCollection.Count > 0)
                    {
                        //-----------------------------------------------------------------------------------------------------
                        //rev:mia MAY 9 2013  - PART OF APCOVER ENHANCEMENT. 
                        //                    - add the netAmount...today 
                        //                    - Today is the Bday of my Father.
                        //-----------------------------------------------------------------------------------------------------
                        try
                        {
                            if (insuranceNode.Attributes["NettAmt"] != null && code == productIdInUrl)
                            {
                                success = decimal.TryParse(insuranceNode.Attributes["NettAmt"].Value, out  NettAmt);
                                success = decimal.TryParse(insuranceNode.Attributes["NettDailyRate"].Value, out  NettDailyRate);
                            }
                        }
                        catch (Exception ex)
                        { }

                        NameValueCollection nvc = new NameValueCollection();
                        foreach(XmlNode requiredNore in requiredActionsCollection)
                        {
                            string actionType = requiredNore.Attributes["code"].Value;
                            string actionTarget = requiredNore.Attributes["PrdCode"].Value;
                            nvc.Add(actionTarget, actionType);
                        }
                        currentInsurance.RequiredActions = nvc;
                    }
                    insuarnceProductsList.Add(currentInsurance);
                }
                insuranceOptions = insuarnceProductsList.ToArray();
            }//end load Insurance Options
            
            //-- has bonus pack (inclusive rate) load it into --
            XmlNode inclusiveRateXML = xmlDoc.SelectSingleNode("data/InclusiveRate/AvailableRate");
            List<AvailabilityItemChargeRow> aicList = new List<AvailabilityItemChargeRow>();
            if (inclusiveRateXML != null)
            {
                hasInclusivePackage = true;
                //TODO: Load inclusive package into AvailabilityItem
                XmlNode packageNode = inclusiveRateXML.SelectSingleNode("Package");
                //InclusivePackage = packageNode.ToString();
                InclusiveAVPId = packageNode.Attributes["AvpId"].Value;

                //load package info for inclusive:
                string packageCode = packageNode.Attributes["PkgCode"].Value;
                string packageDesc = packageNode.Attributes["PkgDesc"].Value;
                string brandCodeStr = packageNode.Attributes["BrandCode"].Value;
                string packageComment = packageNode.Attributes["PkgComment"].Value;
                string packageTCUrl = packageNode.Attributes["PkgTCURL"].Value;
                string packageInfoURL = packageNode.Attributes["PkgInfoURL"].Value;
                bool isLumpSum = (packageNode.Attributes["IsLumpSum"].Value.Equals("Yes") ? true : false);
                decimal estimatedTotal;
                decimal.TryParse(packageNode.Attributes["EstimatedTotal"].Value, out estimatedTotal);
                               
                XmlNode premPackChargeNode =  inclusiveRateXML.SelectSingleNode("//Det[@IsInclusive='Yes']");
                if(premPackChargeNode != null)
                {
                    decimal.TryParse(premPackChargeNode.Attributes["Price"].Value, out InclusiveEstimatedTotal) ;                    
                    InclusiveName = premPackChargeNode.Attributes["PrdName"].Value ;
                    decimal.TryParse(premPackChargeNode.Attributes["AvgRate"].Value, out InclusiveDayPrice);
                }

                XmlNode chargeCollection = inclusiveRateXML.SelectSingleNode("Charge");//load Charges
                decimal dailyAvgRate = 0;
                //List<AvailabilityItemChargeRow> aicList = new List<AvailabilityItemChargeRow>();

                bool hasInclusive = false;
                decimal adminFee = 0.0m;

                string CurrCode = string.Empty;
                int HirePeriod = 0;
                
                foreach (XmlNode chargeXml in chargeCollection)
                {// Each Charge <Det>                                        
                    decimal.TryParse(chargeXml.Attributes["AvgRate"].Value, out dailyAvgRate);
                    AvailabilityItemChargeRow aic = new AvailabilityItemChargeRow();
                    aic.LoadFromXml(chargeXml);
                    if (aic.IsBonusPack) hasInclusive = true;
                    if (aic.AdminFee > 0) adminFee = aic.AdminFee;
                    aicList.Add(aic);

                    //-----------------------------------------------------------------------------------------------------
                    //rev:mia APRIL 4 2013  - PART OF APCOVER ENHANCEMENT. 
                    //-----------------------------------------------------------------------------------------------------
                    CurrCode = aic.CurrencyCode;
                    HirePeriod = aic.HirePeriod;
                   
                }

                //-----------------------------------------------------------------------------------------------------
                //rev:mia APRIL 4 2013  - PART OF APCOVER ENHANCEMENT. 
                //-----------------------------------------------------------------------------------------------------
                if (string.IsNullOrEmpty(productIdInUrl) == false)
                {
                    string prdcode = productIdInUrl;//System.Web.HttpContext.Current.Request["prdId"];
                    var pack = from option in insuranceOptions
                               where option.RequiredActions != null && option.Code == prdcode
                               select option;

                
                    AvailabilityItemChargeRow aics = null;
                    string SapID = string.Empty;
                    int DisplayOrder = 0;
                    bool PromptForQty = false;
                    int MinAge  = 0;
                    int MaxAge = 0;
                    string CurrentInfo = " ";

                    foreach (InsuranceProduct result in pack)
                    {
                        aics = new AvailabilityItemChargeRow();
                        aics.ProductCode = result.Code;//="BONUSPAC" 
                        aics.ProductName = result.Name;//="Bonus Pack" 

                        if (IsGrossRequest  == true)
                        {
                            aics.AverageDailyRate = result.DailyRate;
                            aics.ProductPrice = result.GrossAmount;
                        }
                        else
                        {
                            aics.AverageDailyRate = NettDailyRate;
                            aics.ProductPrice = NettAmt;
                        }
                        
                        aics.IsVehicle = false;
                        aics.CurrencyCode = CurrCode;
                        aics.HirePeriod = HirePeriod;
                        

                        if (result.UOM == UOMType.None)
                            aics.UnitOfMeasurement = "";
                        else
                        {
                            if (result.UOM == UOMType.Day)
                                aics.UnitOfMeasurement = "Day";
                            else
                                aics.UnitOfMeasurement = "H24";
                        }

                        SapID = result.SapId;
                        DisplayOrder = result.DisplayOrder;
                        PromptForQty = result.PromptQuantity;

                        MinAge = result.MinAge;
                        MaxAge = result.MaxAge;
                        CurrentInfo = (string.IsNullOrEmpty(result.ProductInfo) ? " " : result.ProductInfo);
                        var included = aicList.Find(x => x.ProductCode == result.Code);
                        if(included == null)  aicList.Add(aics);
                    }

                    if (aics != null)
                    {

                        ExtraHireItem addedEhi = new ExtraHireItem(aics.ProductCode, aics.ProductName, aics.AverageDailyRate, (aics.UnitOfMeasurement == "" ? UOMType.None : UOMType.Day), aics.ProductPrice, aics.ProductPrice, SapID, 0, PromptForQty, MinAge, MaxAge);
                        addedEhi.HirePeriod = HirePeriod;
                        addedEhi.ProductInfo = CurrentInfo;
                        extraHiresList.Add(addedEhi);
                        extraHireItems = extraHiresList.ToArray();
                    }

                }
                //---------------------------------------------------------------

                InclusiveAvailItem = new AvailabilityItem(InclusiveAVPId, packageCode, packageDesc, brandCodeStr, packageComment, packageTCUrl, packageInfoURL, /*depositAmount*/0, /*depositPercentage*/0, isLumpSum, "cko", "cki", dailyAvgRate, aicList.ToArray());
                InclusiveAvailItem.AdminFee = adminFee;
                InclusiveAvailItem.IncludesBonusPack = true;
                InclusiveAvailItem.EstimatedTotal = estimatedTotal;
                InclusiveAvailItem.AvailabilityType = AvailabilityItemType.Available;

                //consider if one way fee should be waived: ONEWAYAU--------------------------------
                decimal inclusiveOneWayFee = 0.0m;
                XmlNode onewayFeeXMLNode = inclusiveRateXML.SelectSingleNode("//Det[@PrdCode='ONEWAYAU']");//TODO: get this not by code

                if (onewayFeeXMLNode == null)//nz? TODO: this need a type
                    onewayFeeXMLNode = inclusiveRateXML.SelectSingleNode("//Det[@PrdCode='ONEWAYNZ']");

                if (onewayFeeXMLNode == null)//nz? TODO: this need a type
                    onewayFeeXMLNode = inclusiveRateXML.SelectSingleNode("//Det[@PrdCode='ONEWAYQ']");

                if (onewayFeeXMLNode != null && onewayFeeXMLNode.Attributes["Price"] != null)
                    decimal.TryParse(onewayFeeXMLNode.Attributes["Price"].Value, out inclusiveOneWayFee);
                
                InclusiveAvailItem.WaiveInclusiveOneWayFee = (inclusiveOneWayFee == 0);//added 03/11
                InclusiveAvailItem.OneWayFeeComponent = inclusiveOneWayFee;//TODO: get the 
                //-------------------------------------------end ONEWAYAU ---------------------------

                //get the note string
                XmlNode noteXML = inclusiveRateXML.SelectSingleNode("//Det[@IsInclusive='Yes']/Note");//load note for inclusive package
                InclusiveAvailItem.Note = (noteXML != null && noteXML.Attributes["NteDesc"] != null ? noteXML.Attributes["NteDesc"].Value : string.Empty);
                //end get the note string
            }
            else
            {
                //-----------------------------------------------------------------------------------------------------
                //rev:mia MAY 8 2013  - PART OF APCOVER ENHANCEMENT. 
                //                    - Forcing a product w/c is new for APCOVER  
                //-----------------------------------------------------------------------------------------------------
                if (string.IsNullOrEmpty(productIdInUrl) == false)
                {
                    bool included = false;
                    string prdcode = productIdInUrl;//System.Web.HttpContext.Current.Request["prdId"];
                    var pack = from option in insuranceOptions
                               where option.RequiredActions != null && option.Code == prdcode
                               select option;



                    foreach (InsuranceProduct result in pack)
                    {
                        included = true;
                    }//foreach (InsuranceProduct result in pack)

                    if (included)
                    {
                        // ChangeUrl(ProductCodeWithrequiredAction);

                        AvailabilityItemChargeRow aics = null;
                        string SapID = string.Empty;
                        int DisplayOrder = 0;
                        bool PromptForQty = false;
                        int MinAge = 0;
                        int MaxAge = 0;
                        string CurrentInfo = " ";


                        foreach (InsuranceProduct result in pack)
                        {
                            aics = new AvailabilityItemChargeRow();
                            aics.ProductCode = result.Code;//="BONUSPAC" 
                            aics.ProductName = result.Name;//="Bonus Pack" 
                            
                            if (IsGrossRequest == true)
                            {
                                aics.AverageDailyRate = result.DailyRate;
                                aics.ProductPrice = result.GrossAmount;
                            }
                            else
                            {
                                aics.AverageDailyRate = NettDailyRate;
                                aics.ProductPrice = NettAmt;
                            }
                            aics.IsVehicle = false;
                            // aics.CurrencyCode = CurrCode;
                            // aics.HirePeriod = HirePeriod;
                            //aics.AverageDailyRate = result.DailyRate;

                            if (result.UOM == UOMType.None)
                                aics.UnitOfMeasurement = "";
                            else
                            {
                                if (result.UOM == UOMType.Day)
                                    aics.UnitOfMeasurement = "Day";
                                else
                                    aics.UnitOfMeasurement = "H24";
                            }

                            SapID = result.SapId;
                            DisplayOrder = result.DisplayOrder;
                            PromptForQty = result.PromptQuantity;

                            MinAge = result.MinAge;
                            MaxAge = result.MaxAge;
                            CurrentInfo = (string.IsNullOrEmpty(result.ProductInfo) ? " " : result.ProductInfo);
                            var includedInList = aicList.Find(x => x.ProductCode == result.Code);
                            if (includedInList == null) aicList.Add(aics);
                        }

                        if (aics != null)
                        {

                            ExtraHireItem addedEhi = new ExtraHireItem(aics.ProductCode, aics.ProductName, aics.AverageDailyRate, (aics.UnitOfMeasurement == "" ? UOMType.None : UOMType.Day), aics.ProductPrice, aics.ProductPrice, SapID, 0, PromptForQty, MinAge, MaxAge);
                            //addedEhi.HirePeriod = HirePeriod;
                            addedEhi.ProductInfo = CurrentInfo;
                            extraHiresList.Add(addedEhi);
                            extraHireItems = extraHiresList.ToArray();
                        }
                    }

                }
                //-----------------------------------------------------------------------------------------------------

                hasInclusivePackage = false;
            }
            //--------------------

            XmlNodeList creditCardNodes = xmlDoc.SelectNodes("data/ConfigurationProducts/Surcharge/Product");
            if (creditCardNodes != null && creditCardNodes.Count > 0) {
                List<CreditCardElement> cardElmList = new List<CreditCardElement>();
                foreach (XmlNode ccNode in creditCardNodes)
                { 
                    decimal currentSurcharge = 0m;
                    string  currentCode = ccNode.Attributes["Code"].Value;                    
                    string currentName = appData.GetCardNameForCode(currentCode);
                    decimal.TryParse(ccNode.Attributes["SurchargePer"].Value.Replace('%',' '), out currentSurcharge);
                    if (currentCode != null && currentCode != string.Empty && currentName != null && currentName != string.Empty)
                    {
                        CreditCardElement currentCardData = new CreditCardElement(currentCode, currentName, currentSurcharge);
                        cardElmList.Add(currentCardData);
                    }
                }
                availableCardType = cardElmList.ToArray();
            }
            return success;
        }

        private void ChangeUrl(string newvalue)
        {
            // reflect to readonly property 
            System.Reflection.PropertyInfo isReadOnly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

            // make collection editable 
            isReadOnly.SetValue(HttpContext.Current.Request.QueryString, false, null);

            // add
            HttpContext.Current.Request.QueryString.Add("prdId", newvalue);

            // modify 
            //HttpContext.Current.Request.QueryString.Set("prdId", newvalue);
            HttpContext.Current.Request.QueryString.Remove("forceId");

            // make collection readonly again 
            isReadOnly.SetValue(HttpContext.Current.Request.QueryString,  true , null); 
        }
    }
   
}