﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking {

    /// <summary>
    /// RateBand Structure for Availability Item response from Aurora
    /// </summary>
    public struct AvailabilityItemChargeRateBand
    {

        public DateTime FromDate;

        public DateTime ToDate;

        public decimal OriginalRate;

        public decimal DiscountedRate;

        public int HirePeriod;

        public decimal GrossAmount;

        public int IncludesFreeDay;//integer?

        public bool IsCustomerCharge;
        //public AvailabilityItemRateBand()
	    //{
		    //
		    // TODO: Add constructor logic here
		    //
	    //}

        public bool LoadFromXML(XmlNode xmlNode) 
        {
            //TODO: try {
            bool success = true;
            string fromStr = xmlNode.Attributes["From"].Value;
            string toStr = xmlNode.Attributes["To"].Value;
            FromDate = DateTime.Parse(fromStr);
            ToDate = DateTime.Parse(toStr);
            if(xmlNode.Attributes["IncludesFreeDay"] != null)
                success = int.TryParse(xmlNode.Attributes["IncludesFreeDay"].Value, out IncludesFreeDay);
            //} catch (log and set success = false);
            success = decimal.TryParse(xmlNode.Attributes["OriginalRate"].Value, out OriginalRate);
            success = decimal.TryParse(xmlNode.Attributes["DiscountedRate"].Value, out DiscountedRate);
            success = int.TryParse(xmlNode.Attributes["HirePeriod"].Value, out HirePeriod);
            success = decimal.TryParse(xmlNode.Attributes["GrossAmt"].Value, out GrossAmount);
            return success;
        }
    }
}