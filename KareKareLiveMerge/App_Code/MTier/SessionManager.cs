﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// Handle and Manage Session State information for a booking process user.
    /// </summary>
    public class SessionManager
    {
        
        static public SessionData GetSessionData() 
        {
            if ((SessionData)HttpContext.Current.Session["SessionData"] == null)
                HttpContext.Current.Session["SessionData"] = new SessionData(DateTime.Now);
            return (SessionData)HttpContext.Current.Session["SessionData"]; 
        }
        
        /// <summary>
        /// The Status of The Current Booking
        /// </summary>
        static public BookingStatus BookingStatus
        {
            get { return (BookingStatus)HttpContext.Current.Session["BookingStatus"]; }
            set { HttpContext.Current.Session["BookingStatus"] = value;  }
        }

        //External Agent reference
        static public string AgentReference
        {
            get { return (HttpContext.Current.Session["AgentReference"] != null ? (string)HttpContext.Current.Session["AgentReference"] : string.Empty); }
            set { HttpContext.Current.Session["AgentReference"] = value; }
        }
                
        static public string AgentCode
        {
            get { return (String)HttpContext.Current.Session["AgentCode"]; }
            set { HttpContext.Current.Session["AgentCode"] = value; }
        }
        
        /// <summary>
        /// The Booking Id of the completed booking
        /// </summary>
        static public string CompletedBookingID
        {
            get { return (string)HttpContext.Current.Session["BookingId"]; }
            set { HttpContext.Current.Session["BookingId"] = value; }
        }

        /// <summary>
        /// Get The Current session Quote, Added for Retrieve Quote Support 
        /// </summary>
        static public AuroraQuote CurrentQuote
        {
            get { return (AuroraQuote)HttpContext.Current.Session["CurrentQuote"]; }
            set { HttpContext.Current.Session["CurrentQuote"] = value; }        
        }
    

        public SessionManager()
        {
            HttpContext.Current.Session["BookingStatus"] = BookingStatus.Created;
        }
    }
}