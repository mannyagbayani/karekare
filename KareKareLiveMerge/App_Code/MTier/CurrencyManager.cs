﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

/// <summary>
/// Currency Conversion and Caclulation Functionality Manager
/// </summary>

namespace THL.Booking
{

    public enum CurrencyConversionProvider 
    { 
        BNZ = 0    
    }

    public enum CurrencyCode
    { 
        AUD = 0,
        CAD = 1,
        CNY = 2,
        EUR = 3,
        GBR = 4,
        HKD = 5,
        JPY = 6,
        SGD = 7,
        ZAR = 8,
        KRW = 9,
        CHF = 10,
        USD = 11,
        NZD = 12
    }
    
    public struct Currency 
    {
        public string CountryName;
        public string CountryCode;
        public decimal IndicativeRate;
        public decimal LastActualRate;
    }
    
    public class CurrencyManager
    {


        public decimal ConvertToNZD(CountryCode countryCode, decimal value)
        {
            if (value == 0) return 0;
            string country = countryCode.ToString();
            decimal convertedRate = 1;
            foreach (Currency currentCurrency in currencyRates)
            {
                if (currentCurrency.CountryCode.Contains(country))//TODO: conversion of country code to currency code comes here..
                    return (value /(currentCurrency.IndicativeRate));
            }
            return convertedRate;
        }


        /// <summary>
        /// Provide a Currency Code for a given Country Code
        /// </summary>
        /// <param name="countryCode">Required Country</param>
        /// <returns></returns>
        public static CurrencyCode GetCurrencyForCountryCode(CountryCode countryCode)
        {
            CurrencyCode currencyCode = CurrencyCode.NZD; //TODO: define default if required.
            switch (countryCode)
            { 
                case(CountryCode.NZ):
                    currencyCode = CurrencyCode.NZD;
                    break;
                case(CountryCode.AU):
                    currencyCode = CurrencyCode.AUD;
                    break;
                case (CountryCode.US):
                    currencyCode = CurrencyCode.USD;
                    break;
                case(CountryCode.CA):
                    currencyCode = CurrencyCode.CAD;
                    break;
            }
            return currencyCode;
        }

        /// <summary>
        /// Provide a Currency Code for a given Country Code
        /// </summary>
        /// <param name="countryCode">Required Country</param>
        /// <returns></returns>
        public static CurrencyCode GetCurrencyForCountryCode(string countryCode)
        {
            CurrencyCode currencyCode = CurrencyCode.NZD; //TODO: define default if required.
            switch (countryCode.ToUpper())
            {
                case ("NZ"):
                    currencyCode = CurrencyCode.NZD;
                    break;
                case ("AU"):
                    currencyCode = CurrencyCode.AUD;
                    break;
                case ("US"):
                    currencyCode = CurrencyCode.USD;
                    break;
                case ("CA"):
                    currencyCode = CurrencyCode.CAD;
                    break;
            }
            return currencyCode;
        }   




        Currency[] currencyRates;
        public Currency[] CurrencyRates
        {
            get { return currencyRates; }
        }

        /// <summary>
        /// Age of this Currency Rates Set
        /// </summary>
        public DateTime LastUpdated;
        
        /// <summary>
        /// True on successful load of Currency Rates from 3rd Party provider feed
        /// </summary>
        bool loadStatus = false;
        public bool LoadStatus 
        {
            get { return loadStatus; }
        }


        CurrencyCode defaultCurrency;
        public CurrencyCode DefaultCurrency 
        {
            get { return defaultCurrency;  }
        }

        public CurrencyManager(CurrencyConversionProvider provider, CurrencyCode _defaultCurrency)
        {
            defaultCurrency = _defaultCurrency;
            
            string currenctConvertorURL = System.Web.Configuration.WebConfigurationManager.AppSettings["CurrencyConversionURL"];
            //DataAccess da = new DataAccess();
            //string stringXml = da.GetStringForURL(currenctConvertorURL);            
            //XmlDocument responseXML = new XmlDocument();
            //responseXML.LoadXml(stringXml);

            XmlDocument responseXML = ApplicationManager.GetApplicationData().CurrencyXML;
            
            bool successStatus = false;
            switch (provider) 
            { 
                case CurrencyConversionProvider.BNZ:
                    successStatus = LoadFromBNZXML(responseXML);
                    break;            
            }           
            
        }

        /// <summary>
        /// BNZ specific initial logic
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private bool LoadFromBNZXML(XmlDocument xmlDoc) 
        {
            List<Currency> currencyList = new List<Currency>();            
            XmlNodeList countryNodes = xmlDoc.SelectNodes("content/country");
            foreach (XmlNode countryNode in countryNodes) 
            {
                decimal indicative, lastActual;
                Currency currency = new Currency();
                currency.CountryCode = countryNode.Attributes["code"].Value;
                currency.CountryName = countryNode.Attributes["name"].Value;
                XmlNodeList ratesNodes = countryNode.ChildNodes;
                loadStatus = decimal.TryParse(ratesNodes[0].InnerText, out indicative);
                loadStatus = decimal.TryParse(ratesNodes[1].InnerText, out lastActual);
                currency.IndicativeRate = indicative;
                currency.LastActualRate = lastActual;
                currencyList.Add(currency);
            }
            currencyRates = currencyList.ToArray();
            return false; 
        }

        public decimal GetNZExchangeRate(decimal NZDAmount, CurrencyCode currencyCode) {
            foreach (Currency currency in currencyRates) {
                if (currency.CountryCode.Equals(currencyCode.ToString())) 
                {
                    return (currency.IndicativeRate * NZDAmount);
                }
            }
            return 0m;
        }

    }
}