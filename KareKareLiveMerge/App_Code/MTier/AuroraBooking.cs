﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;

namespace THL.Booking
{
    public enum BookingStatus
    {
        Created = 0,
        Proccessing = 1,
        PaymentSuccessful = 2,
        BookingSuccessful = 3,
        Quote = 4,
        Provisional = 5,
        Confirmed = 6
    }


    /// <summary>
    /// Aurora Booking Instance with Extra Products and User Payment Infromation  
    /// </summary>
    public class AuroraBooking
    {

        public static bool GetBookingError(XmlNode bookingResponse, ref string errorMessage)
        {
            //TODO: 
            if (bookingResponse != null && bookingResponse.SelectSingleNode("//Message") != null && !string.IsNullOrEmpty(bookingResponse.SelectSingleNode("//Message").InnerText) && (bookingResponse.SelectSingleNode("//Message").InnerText.IndexOf("Warning") > 0 || bookingResponse.SelectSingleNode("//Message").InnerText.IndexOf("Error") > 0))
            {
                errorMessage = bookingResponse.SelectSingleNode("//Message").InnerText;
                return true;
            }
            else
            {
                if (bookingResponse != null && bookingResponse.SelectSingleNode("//Message") != null && !string.IsNullOrEmpty(bookingResponse.SelectSingleNode("//Message").InnerText) && (bookingResponse.SelectSingleNode("//Message").InnerText.IndexOf("Object") > -1 || bookingResponse.SelectSingleNode("//Message").InnerText.IndexOf("Unable") > -1))
                {
                    errorMessage = "An error has occurred,please try again";
                    return true;
                }
                else
                    return false;
            }
        }

        public string ExistingBookingNumber { get; set; }

        /// <summary>
        /// Return the Booking Request Type based on code string
        /// </summary>
        /// <param name="bookingTypeStr"></param>
        /// <returns></returns>
        public static BookingStatus GetBookingTypeForString(string bookingTypeStr)
        {
            BookingStatus status = BookingStatus.Created;
            switch (bookingTypeStr.ToLower())
            {
                case "qn":
                    status = BookingStatus.Quote;
                    break;
                case "kk":
                    status = BookingStatus.Confirmed;
                    break;
                case "nn":
                    status = BookingStatus.Provisional;
                    break;
            }
            return status;
        }



        public static string GetBookingStatuString(BookingStatus status)
        {
            string statusStr = "QN";//TODO: define default
            switch (status)
            {
                case BookingStatus.Quote:
                    statusStr = "QN";
                    break;
                case BookingStatus.Confirmed:
                    statusStr = "KK";
                    break;
                case BookingStatus.Provisional:
                    statusStr = "NN";
                    break;
            }
            return statusStr;
        }



        AvailabilityItemType availabilityType;
        /// <summary>
        /// The Availability Status of this booking (Available or OnRequest) 
        /// </summary>
        public AvailabilityItemType AvailabilityType
        {
            get { return availabilityType; }
            set { availabilityType = value; }
        }


        bool _requiresLoyaltyNumber;

        public bool RequiresLoyaltyNumber//Added for NRMA Loyalty support
        {
            get { return _requiresLoyaltyNumber; }
            set { _requiresLoyaltyNumber = value; }
        }


        string _termsAndConsLink;

        public string BookingTermAndConditionsLink
        {
            get { return _termsAndConsLink; }
            set { _termsAndConsLink = value; }
        }

        public bool WaiveOneWayFee;
        public decimal OneWayFeeComponent;

        string _packageCode;
        /// <summary>
        /// The Bookings Package Code
        /// </summary>
        public string PackageCode
        {
            get { return _packageCode; }
            set { _packageCode = value; }
        }

        string _channelCode;
        public string ChannelCode
        {
            get { return _channelCode; }
            set { _channelCode = value; }
        }

        BookingStatus status;
        /// <summary>
        /// The Current status off the booking
        /// </summary>
        public BookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public bool PayedFull;
        public decimal SurchargePercentage;
        public decimal AdminFee;

        bool bonusPackSelected;
        public bool BonusPackSelected
        {
            get { return bonusPackSelected; }
            set
            {
                bonusPackSelected = value;
            }
        }

        CreditCardData _paymentInfo;
        public CreditCardData PaymentInformation
        {
            get { return _paymentInfo; }
        }

        /*Added by Nimesh on 13th Feb 2015 for Slots*/
        public string SlotId { get; set; }
        public string SelectedSlot { get; set; }
        /*End Added by Nimesh on 13th Feb 2015 for Slots*/
        /// <summary>
        /// Add Principal Customers details and related Credit Card Information
        /// </summary>
        /// <param name="cardData"></param>
        /// <returns></returns>
        public bool AddPaymentInfo(CreditCardData cardData)
        {
            _paymentInfo = cardData;
            _customer = cardData.Customer;
            //TODO: booking related validation logic goes here if needed
            return true; // TODO: set status to true if needed.
        }

        Transaction _transaction;
        public bool AddTransactionData(Transaction transaction)
        {
            _transaction = transaction;
            //TODO: validate transaction information and commit to the Booking object
            return true;
        }

        public Decimal GetBookingPriceTotal()
        {


            ApplicationData appData = ApplicationManager.GetApplicationData();

            decimal calulatedTotal = _vehicleCharges;
            foreach (AuroraProduct product in _selectedProducts)
            {
                calulatedTotal += product.SelectedAmount;
            }
            //TODO: get Admin Fees and add them here..
            decimal surCharge = (_paymentInfo != null ? appData.GetSurchargeForCard(_paymentInfo.CardType) * 0.01m : 1);
            calulatedTotal = calulatedTotal + (calulatedTotal * surCharge);
            return calulatedTotal;
        }

        AuroraProduct[] _selectedProducts;

        /// <summary>
        /// Flag This Booking as Is Inclusive 
        /// </summary>
        public void SetInclusive(PackageManager pm)
        {
            List<AuroraProduct> productList = new List<AuroraProduct>();
            if (_selectedProducts != null && _selectedProducts.Length > 0)
            {
                foreach (AuroraProduct product in _selectedProducts)
                    if (product.Type != ProductType.Insurance)
                        productList.Add(product);//TODO: consider other inclusive restriction if needed here..               
            }
            //Test insert an Inclusive place holder
            InsuranceProduct incItem = new InsuranceProduct("code", pm.InclusiveName, pm.InclusiveDailyRate, UOMType.Day, pm.InclusiveEstimatedTotal, 0, string.Empty, 0, false, 0, 0);
            incItem.SelectedAmount = 1;
            productList.Add(incItem); //Test                        
            //Test insert an Inclusive place holder
            _selectedProducts = productList.ToArray();
        }

        bool paxMerged = false;

        /// <summary>
        /// Returns all booked products that matches the required product type
        /// </summary>
        /// <param name="productType">ProductType.All for any</param>
        /// <returns></returns>
        public AuroraProduct[] GetBookingProducts(ProductType productType)
        {
            if (!paxMerged)
            {
                FerryProduct[] ferryItems = getVehiclePaxCharges();

                List<AuroraProduct> mergedProducts = new List<AuroraProduct>();
                if (_selectedProducts != null)
                    mergedProducts.AddRange(_selectedProducts);
                if (ferryItems != null)
                    mergedProducts.AddRange(ferryItems);

                paxMerged = true;
                _selectedProducts = mergedProducts.ToArray();
            }
            if (_selectedProducts != null && _selectedProducts.Length > 0)
            {
                if (productType == ProductType.All)
                    return _selectedProducts;
                else
                {//filter products
                    List<AuroraProduct> matchedProducts = new List<AuroraProduct>();
                    foreach (AuroraProduct product in _selectedProducts)
                    {
                        if (product.Type == productType)
                            matchedProducts.Add(product);
                    }
                    return matchedProducts.ToArray();
                }
            }//end has products
            else
                return null;
        }

        /// <summary>
        /// Returns a collection of Ferry Pax Entries
        /// </summary>
        /// <returns></returns>
        FerryProduct[] getVehiclePaxCharges()
        {
            Dictionary<PersonType, FerryProduct> ferryDic = new Dictionary<PersonType, FerryProduct>();

            if (passengers == null) return null;

            foreach (PersonRateInfo passenger in passengers)
            {
                PersonType currentType = passenger.PersonInfoType;
                if (ferryDic.Keys.Contains(currentType))
                {
                    ferryDic[currentType].SelectedAmount += passenger.NumPassengers;
                }
                else
                {
                    FerryProduct product = new FerryProduct(passenger.SapId);
                    product.SelectedAmount = passenger.NumPassengers;
                    product.GrossAmount = getPaxRate(passenger.PersonInfoType);
                    product.Name = passenger.PersonInfoType.ToString();//work around..
                    ferryDic.Add(currentType, product);
                }
            }
            return ferryDic.Values.ToArray();
        }

        /// <summary>
        /// Get the Total Non Vehicle Charges
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        public decimal GetTotalNonVehicleCharges(ProductType productType)
        {
            decimal totalPrice = 0;
            foreach (AuroraProduct product in _selectedProducts)
            {
                if (product.Type == productType || productType == ProductType.All)
                {
                    totalPrice += product.GrossAmount * product.SelectedAmount;
                }
            }
            return totalPrice;
        }

        string _avpID;

        public string AVPID
        {
            get { return _avpID; }
            set { _avpID = value; }
        }

        string _inclusiveAvpID;

        public string InclusiveAvpID
        {
            get { return _inclusiveAvpID; }
            set { _inclusiveAvpID = value; }
        }

        InsuranceProduct _inclusivePack;
        public InsuranceProduct InclusivePack
        {
            get { return _inclusivePack; }
            set { _inclusivePack = value; }
        }


        string _agentReference;
        public string AgentReference
        {
            get { return _agentReference; }
            set { _agentReference = value; }
        }

        int _quantity;

        string _status;//kk etc.

        THLBrands _brand;
        public THLBrands Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }

        DateTime _checkOutTime;

        public DateTime CheckOutTime
        {
            get { return _checkOutTime; }
            set { _checkOutTime = value; }
        }

        DateTime _checkInTime;

        public DateTime CheckInTime
        {
            get { return _checkInTime; }
            set { _checkInTime = value; }
        }

        string _checkOutLocationCode;

        public string PickUpBranch
        {
            get { return _checkOutLocationCode; }
            set { _checkOutLocationCode = value; }
        }

        string _checkInLocationCode;

        public string DropOffBranch
        {
            get { return _checkInLocationCode; }
            set { _checkInLocationCode = value; }
        }

        public string PickUpStr;
        public string DropOffStr;

        CountryCode _countryCode;
        public CountryCode CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        string _vehicleCode;

        public string VehicleCode
        {
            get { return _vehicleCode; }
            set { _vehicleCode = value; }
        }

        string _note;//added by customer during booking..

        public string BookingNote
        {
            get { return _note; }
            set { _note = value; }
        }

        string _userCode;
        public string UserCode
        {
            get { return _userCode; }
            set { _userCode = value; }
        }

        Customer _customer;

        /// <summary>
        /// Get The Bookings Customer Data
        /// </summary>
        public Customer CustomerInfo
        {
            get { return _customer; }
            set { _customer = value; }
        }

        int _hirePeriod;
        public int HirePeriod
        {
            get { return _hirePeriod; }
        }


        /// <summary>
        /// Added for Initializing a booking instance out of a previous quote
        /// </summary>
        public AuroraBooking(Customer customer, ExtraHireItem[] extraHires)
        {
            _customer = customer;
            _selectedProducts = extraHires;
        }

        /// <summary>
        /// Initial from submitted HTTP Parameters
        /// </summary>
        /// <param name="incomingParamsCollection"></param>
        public AuroraBooking(NameValueCollection incomingParamsCollection, string avpId, AvailabilityItem availabilityInformation)
        {
            status = BookingStatus.Created;
            _avpID = avpId;
            _vehicleCharges = availabilityInformation.EstimatedTotal;
            _hirePeriod = availabilityInformation.GetHirePeriod();
            int currentAmount = 0;
            ferryParams = new NameValueCollection();
            bool success;
            foreach (string key in incomingParamsCollection.AllKeys)
            {
                if (key.Equals("insuranceOption"))//found Insurance
                {
                    if (incomingParamsCollection[key].Contains("_"))//insurance
                    {//Expecting this: "ipi_26834D52-9546-4135-9215-DFD139BB124A,ipi_94052247-96FB-42B7-ADA3-E50736CD984B"                        
                        foreach (string insParamStr in incomingParamsCollection[key].Split(','))//on this: "ipi_26834D52-9546-4135-9215-DFD139BB124A"
                        {
                            string sapId = insParamStr.Split('_')[1];
                            if (sapId.Equals(string.Empty))
                                continue;//only process real values (not an ER0);
                            InsuranceProduct insProduct = new InsuranceProduct(sapId);//add amount if future require so (currently only one needed) 
                            insProduct.SelectedAmount = 1;//only one insurance
                            AddProduct(insProduct);
                        }
                    }
                }
                if (key.Contains("ehi_"))//found Extra Hire Item
                {
                    string sapId = key.Split('_')[1];
                    if (incomingParamsCollection[key].Equals("on"))
                        currentAmount = 1;
                    else
                        success = int.TryParse(incomingParamsCollection[key], out currentAmount);
                    if (currentAmount > 0)
                    {
                        ExtraHireItem ehiProduct = new ExtraHireItem(sapId);
                        ehiProduct.SelectedAmount = currentAmount;
                        AddProduct(ehiProduct);//TODO: add amount
                    }
                }
                if (key.Equals("paxCounter") || (key.Contains("wlg") && !incomingParamsCollection[key].Equals("0")))
                {//TODO: remove biz logic out
                    ferryParams.Add(key, incomingParamsCollection[key]);//add into the ferry bucket
                }
                if (key.Equals("pickUpLoc"))
                {
                    _checkOutLocationCode = incomingParamsCollection[key];
                }
                if (key.Equals("dropOffLoc"))
                {
                    _checkInLocationCode = incomingParamsCollection[key];
                }
            }
            processFerryParams();//process the ferry bucket
        }

        NameValueCollection ferryParams;
        string ferryPaxCollection;
        PersonRateInfo[] passengers;

        /// <summary>
        /// Serialized ferry information
        /// </summary>
        public string GetFerryInfo()
        {
            int numAdults = 0;
            StringBuilder sb = new StringBuilder();
            if (int.TryParse(ferryParams["wlg_pct_Adult"], out numAdults) && numAdults > 0)
            {
                sb.Append("WLG-PCT crossing date :" + ferryParams["crossingDate_wlg_pct"] + " " + ferryParams["crossingTime_wlg_pct"]);
                sb.Append("(Adults:" + ferryParams["wlg_pct_Adult"] + ", Childs:" + ferryParams["wlg_pct_Child"] + ", Infants:" + ferryParams["wlg_pct_Infant"] + ")");
            }
            if (int.TryParse(ferryParams["pct_wlg_Adult"], out numAdults) && numAdults > 0)
            {
                sb.Append("PCT-WLG crossing date :" + ferryParams["crossingDate_pct_wlg"] + " " + ferryParams["crossingTime_pct_wlg"]);
                sb.Append("(Adults:" + ferryParams["pct_wlg_Adult"] + ", Childs:" + ferryParams["pct_wlg_Child"] + ", Infants:" + ferryParams["pct_wlg_Infant"] + ")");
            }
            return (sb.Length > 0 ? "(" + sb.ToString() + ")" : string.Empty);
        }


        private void processFerryParams()
        {
            List<PersonRateInfo> passengersList = new List<PersonRateInfo>();
            StringBuilder bucketBuilder = new StringBuilder();
            //get the selected ferry crossings (has a wlg_pct_CEA1CBED-3692-430D-A132-697A2FC48302 component)
            List<string> crossingKeys = new List<string>();
            foreach (string key in ferryParams.AllKeys)
            {
                if (key.IndexOf('-') > 0)//has SapId GUID component 
                    crossingKeys.Add(key);
            }

            foreach (string ferryStr in crossingKeys.ToArray())//for each crossing item itterate through all key and match relevant
            {
                string[] currentFerryParams = ferryStr.Split('_');
                string srcCode = currentFerryParams[0];
                string destCode = currentFerryParams[1];
                string vehSapId = currentFerryParams[2];
                FerryProduct currentFerry = new FerryProduct(vehSapId);
                currentFerry.SelectedAmount = 1;//one per direction.. test this 
                AddProduct(currentFerry);
                //try to match srcCode_destCode in all other and create an info bucket

                foreach (string str in ferryParams.Keys)
                {
                    if (str.Contains(srcCode + "_" + destCode) && !str.Equals(ferryStr))//crossingDate_wlg_pct:01092009,crossingTime_wlg_pct:9:00,wlg_pct_Adult:1,wlg_pct_Child:2,wlg_pct_Infant:3
                    {
                        bucketBuilder.Append(str + ":" + ferryParams[str] + ",");
                        string[] keyParams = str.Split('_');
                        if (keyParams.Length == 3 && keyParams[2] != null)
                        {
                            PersonRateInfo currentPerson = new PersonRateInfo();
                            switch (keyParams[2])
                            {
                                case "Adult":
                                    currentPerson.PersonInfoType = PersonType.Adult;
                                    break;
                                case "Child":
                                    currentPerson.PersonInfoType = PersonType.Child;
                                    break;
                                case "Infant":
                                    currentPerson.PersonInfoType = PersonType.Infant;
                                    break;
                                default:
                                    currentPerson.PersonInfoType = PersonType.None;
                                    break;
                            }
                            int currentPassengersCount = 0;
                            int.TryParse(ferryParams[str], out currentPassengersCount);
                            currentPerson.NumPassengers = currentPassengersCount;
                            if (currentPerson.PersonInfoType != PersonType.None)
                                passengersList.Add(currentPerson);
                        }
                    }
                }
            }
            passengers = passengersList.ToArray();
        }

        /// <summary>
        /// Base Vehicle Charges from Package
        /// </summary>
        decimal _vehicleCharges;

        public bool AddProduct(AuroraProduct product)
        {
            List<AuroraProduct> productsList;
            if (_selectedProducts == null)
                productsList = new List<AuroraProduct>();
            else
                productsList = new List<AuroraProduct>(_selectedProducts);
            productsList.Add(product);
            _selectedProducts = productsList.ToArray();
            return true;//TODO: ExpHandle
        }

        public bool AddProducts(AuroraProduct[] products)
        {
            List<AuroraProduct> productsList;
            if (_selectedProducts == null)
                productsList = new List<AuroraProduct>();
            else
                productsList = new List<AuroraProduct>(_selectedProducts);
            productsList.AddRange(products);
            _selectedProducts = productsList.ToArray();
            return true;//TODO: ExpHandle
        }

        PersonRateInfo[] paxRates;//Ferry rates collection
        decimal getPaxRate(PersonType personType)
        {
            foreach (PersonRateInfo currentRate in paxRates)
            {
                if (currentRate.PersonInfoType == personType)
                    return currentRate.Rate;
            }
            return 0;
        }

        public bool UpdateProductDefinitions(PackageManager packageManager, string avpId)
        {
            //Load Prices into the selected booked products
            try
            {
                if (_selectedProducts != null && _selectedProducts.Length > 0)
                {
                    for (int i = 0; i < _selectedProducts.Length; i++)// (AuroraProduct bookedProduct in _selectedProducts)
                    {
                        foreach (AuroraProduct packageProduct in packageManager.GetAllProducts(avpId))
                        {
                            if (_selectedProducts[i].SapId.Equals(packageProduct.SapId))
                            {
                                _selectedProducts[i] = PackageManager.MergeProducts(packageProduct, ref _selectedProducts[i]);
                            }
                        }
                    }
                    paxRates = packageManager.PersonRateInfo(avpId);

                }
                return true;//TODO: exp handle
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                //TODO: log.. 
                return false;
            }
        }

        public XmlDocument ToXML()
        {

            bool addToExisting = !string.IsNullOrEmpty(ExistingBookingNumber);


            string bookingStatus = AuroraBooking.GetBookingStatuString(status);

            string passedAgentCode = _agentReference;
            string userCode = SessionManager.GetSessionData().CurrentAgent;

            XmlDocument xmlDoc = new XmlDocument();

            //// Create the xml document containe
            //XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            XmlDeclaration dec = xmlDoc.CreateXmlDeclaration("1.0", null, null);
            xmlDoc.AppendChild(dec);// Create the root element
            XmlElement rootElm = xmlDoc.CreateElement("Data");

            XmlElement rentalDetElm = xmlDoc.CreateElement("RentalDetails");

            XmlElement avpidElm = xmlDoc.CreateElement("AvpId");//<AvpId>8F56EAF196D94E62B217573DD81C7C5D</AvpId>
            avpidElm.InnerText = (bonusPackSelected ? _inclusiveAvpID : _avpID);
            rentalDetElm.AppendChild(avpidElm);

            if (addToExisting)//adding this booking to an existing
            {
                XmlElement rentalIdElm = xmlDoc.CreateElement("AddBookingNumber");//<AvpId>8F56EAF196D94E62B217573DD81C7C5D</AvpId>
                rentalIdElm.InnerText = ExistingBookingNumber;
                rentalDetElm.AppendChild(rentalIdElm);
            }

            XmlElement agentElm = xmlDoc.CreateElement("AgentReference");//<AgentReference>AGENTREF1234</AgentReference>
            agentElm.InnerText = passedAgentCode;
            rentalDetElm.AppendChild(agentElm);

            XmlElement quantityElm = xmlDoc.CreateElement("QuantityRequested");//<QuantityRequested>1</QuantityRequested>
            quantityElm.InnerText = "1";//TODO:
            rentalDetElm.AppendChild(quantityElm);

            XmlElement statusElm = xmlDoc.CreateElement("Status");//<Status>KK</Status>
            statusElm.InnerText = bookingStatus;
            rentalDetElm.AppendChild(statusElm);

            XmlElement checkOutElm = xmlDoc.CreateElement("CheckOutDateTime");//<CheckOutDateTime>15/10/2009 10:00</CheckOutDateTime>
            checkOutElm.InnerText = _checkOutTime.ToString("dd-MMM-yyyy HH:mm");
            rentalDetElm.AppendChild(checkOutElm);

            XmlElement checkOutLocElm = xmlDoc.CreateElement("CheckOutLocationCode");//<CheckOutLocationCode>AKL</CheckOutLocationCode>
            checkOutLocElm.InnerText = _checkOutLocationCode;
            rentalDetElm.AppendChild(checkOutLocElm);

            XmlElement checkInElm = xmlDoc.CreateElement("CheckInDateTime");//<CheckInDateTime>15/11/2009 10:00</CheckInDateTime>
            checkInElm.InnerText = _checkInTime.ToString("dd-MMM-yyyy HH:mm");
            rentalDetElm.AppendChild(checkInElm);

            XmlElement checkInLocElm = xmlDoc.CreateElement("CheckInLocationCode");//<CheckInLocationCode>AKL</CheckInLocationCode>
            checkInLocElm.InnerText = _checkOutLocationCode;
            rentalDetElm.AppendChild(checkInLocElm);

            rentalDetElm.AppendChild(xmlDoc.CreateElement("QuantityRequested"));

            XmlElement productListElm = xmlDoc.CreateElement("ProductList");

            if (_selectedProducts != null && _selectedProducts.Length > 0)
            {

                foreach (AuroraProduct product in _selectedProducts)
                {
                    if (product.SapId != null && product.SapId != string.Empty)
                    {
                        XmlElement productElm = xmlDoc.CreateElement("Product");
                        productElm.SetAttribute("SapId", product.SapId);
                        productElm.SetAttribute("Qty", product.SelectedAmount.ToString());
                        productElm.SetAttribute("PrdShortName", product.Name);
                        productListElm.AppendChild(productElm);
                    }
                }
                rentalDetElm.AppendChild(productListElm);//Add Products
            }
            XmlElement countryCodeElm = xmlDoc.CreateElement("CountryCode");//<CountryCode>NZ</CountryCode>
            countryCodeElm.InnerText = _countryCode.ToString();
            rentalDetElm.AppendChild(countryCodeElm);

            XmlElement noteElm = xmlDoc.CreateElement("Note");//<Note>This is a note to be added to the booking</Note>
            noteElm.InnerText = _note; //no ferry GetFerryInfo();
            rentalDetElm.AppendChild(noteElm);

            XmlElement userCodeElm = xmlDoc.CreateElement("UserCode");//<UserCode>B2CNZ</UserCode>
            userCodeElm.InnerText = _userCode;
            rentalDetElm.AppendChild(userCodeElm);

            XmlElement confirmHeaderElm = xmlDoc.CreateElement("ConfirmationHeaderText");//<ConfirmationHeaderText>Header Text Goes Here</ConfirmationHeaderText>
            confirmHeaderElm.InnerText = "";//TODO: from App ContentManager
            rentalDetElm.AppendChild(confirmHeaderElm);

            XmlElement confirmFooterElm = xmlDoc.CreateElement("ConfirmationFooterText");//<ConfirmationFooterText>Footer Text Goes Here</ConfirmationFooterText>
            confirmFooterElm.InnerText = "View the " + Brand + " Terms & Conditions (" + _termsAndConsLink + ") for your rental.";
            rentalDetElm.AppendChild(confirmFooterElm);

            XmlElement getConfElm = xmlDoc.CreateElement("GetConfXML");//<GetConfXML>True</GetConfXML>
            getConfElm.InnerText = "False";
            rentalDetElm.AppendChild(getConfElm);

            XmlElement isReqfElm = xmlDoc.CreateElement("IsRequestBooking");//<IsRequestBooking>True</IsRequestBooking>
            isReqfElm.InnerText = (availabilityType == AvailabilityItemType.OnRequest ? "True" : "False");
            rentalDetElm.AppendChild(isReqfElm);

            // Added by Nimesh on 28th Jan 2015 for passing empty selected slot and slot id
            XmlElement slotInElm = xmlDoc.CreateElement("SelectedSlot");
            slotInElm.InnerText = SelectedSlot;
            rentalDetElm.AppendChild(slotInElm);

            XmlElement slotIdInElm = xmlDoc.CreateElement("SlotId");
            slotIdInElm.InnerText = string.IsNullOrEmpty(SlotId) ? "0" : SlotId; //SlotId;
            rentalDetElm.AppendChild(slotIdInElm);
            // End Added by Nimesh on 28th Jan 2015 for passing empty selected slot and slot id

            rootElm.AppendChild(rentalDetElm);

            if (_customer != null)
            {//Add Customer Information
                XmlElement customerElm = xmlDoc.CreateElement("CustomerData");

                XmlElement custTitleElm = xmlDoc.CreateElement("Title");//<Title>Mr.</Title>
                custTitleElm.InnerText = _customer.Title.ToString();
                customerElm.AppendChild(custTitleElm);

                XmlElement custFNameElm = xmlDoc.CreateElement("FirstName");//<FirstName>Shoel</FirstName>
                custFNameElm.InnerText = _customer.FirstName;
                customerElm.AppendChild(custFNameElm);

                XmlElement custLNameElm = xmlDoc.CreateElement("LastName");//<LastName>Palli</LastName>
                custLNameElm.InnerText = _customer.LastName;
                customerElm.AppendChild(custLNameElm);

                XmlElement custEMailElm = xmlDoc.CreateElement("Email");//<Email>RAJESH.SHUKLA@THLONLINE.COM</Email>
                custEMailElm.InnerText = _customer.EMail;
                customerElm.AppendChild(custEMailElm);

                XmlElement custPhoneElm = xmlDoc.CreateElement("PhoneNumber");//<PhoneNumber>+6493364237</PhoneNumber>
                custPhoneElm.InnerText = _customer.PhoneNumber;
                customerElm.AppendChild(custPhoneElm);

                if (_customer.AgentMembershipNumber != null && _customer.AgentMembershipNumber != string.Empty)
                {//addded for NRMA support 22/06/10
                    XmlElement custLoyaltyElm = xmlDoc.CreateElement("LoyaltyCardNumber");//<LoyaltyCardNumber>111-Card-222</LoyaltyCardNumber>
                    custLoyaltyElm.InnerText = _customer.AgentMembershipNumber;
                    customerElm.AppendChild(custLoyaltyElm);
                }

                XmlElement custCoRElm = xmlDoc.CreateElement("CountryOfResidence");//<CountryOfResidence>DK</CountryOfResidence>
                custCoRElm.InnerText = _customer.CountryOfresidenceCode;
                customerElm.AppendChild(custCoRElm);

                rootElm.AppendChild(customerElm);
            }
            if (_paymentInfo != null && _transaction != null)
            {
                XmlElement ccElm = xmlDoc.CreateElement("CreditCardDetails");//<CreditCardDetails>

                XmlElement cTypeElm = xmlDoc.CreateElement("CardType");//<CardType>VISA</CardType>
                cTypeElm.InnerText = _paymentInfo.CardType.ToString();
                ccElm.AppendChild(cTypeElm);

                XmlElement cNumberElm = xmlDoc.CreateElement("CreditCardNumber");//<CreditCardNumber>1111111111111111</CreditCardNumber>
                cNumberElm.InnerText = _paymentInfo.CardNumber.ToString();
                ccElm.AppendChild(cNumberElm);

                XmlElement cNameElm = xmlDoc.CreateElement("CreditCardName");//<CreditCardName>Dave Davis</CreditCardName>
                cNameElm.InnerText = _paymentInfo.CardHolderName;
                ccElm.AppendChild(cNameElm);

                XmlElement cTransRefElm = xmlDoc.CreateElement("ApprovalRef");//<ApprovalRef>123</ApprovalRef>
                cTransRefElm.InnerText = _paymentInfo.GeneratedBookingReference();
                ccElm.AppendChild(cTransRefElm);

                XmlElement cExpiryElm = xmlDoc.CreateElement("ExpiryDate");//<ExpiryDate>12/12</ExpiryDate>
                cExpiryElm.InnerText = (bookingStatus == "WL" ? _paymentInfo.FormattedExpiryDate : _paymentInfo.ExpiryDate);
                ccElm.AppendChild(cExpiryElm);

                XmlElement cAmountElm = xmlDoc.CreateElement("PaymentAmount");//<PaymentAmount>1000.00</PaymentAmount>
                cAmountElm.InnerText = _transaction.Amount.ToString("F");
                ccElm.AppendChild(cAmountElm);

                XmlElement cSurchargeElm = xmlDoc.CreateElement("PaymentSurchargeAmount");//<PaymentSurchargeAmount>2.00</PaymentSurchargeAmount>
                cSurchargeElm.InnerText = _paymentInfo.PaymentSurchargeAmount.ToString("F");
                ccElm.AppendChild(cSurchargeElm);

                rootElm.AppendChild(ccElm);//</CreditCardDetails>

                XmlElement dpsElm = xmlDoc.CreateElement("DPSData");//<DPSData>

                XmlElement dpsAuthElm = xmlDoc.CreateElement("DPSAuthCode");//<DPSAuthCode>DPSAUTH</DPSAuthCode>
                dpsAuthElm.InnerText = _transaction.AutorizationCode;
                dpsElm.AppendChild(dpsAuthElm);

                XmlElement dpsTransRefElm = xmlDoc.CreateElement("DPSTxnRef");//<DPSTxnRef>DPSTXNREF</DPSTxnRef>
                dpsTransRefElm.InnerText = _transaction.TransactionCode;
                dpsElm.AppendChild(dpsTransRefElm);
                rootElm.AppendChild(dpsElm);//</DPSData>

                //</Info>
                XmlElement infoElm = xmlDoc.CreateElement("Info");//<Info>
                XmlElement vCodeElm = xmlDoc.CreateElement("VehicleCode");
                vCodeElm.InnerText = _vehicleCode;//<VehicleCode>4BBXS</VehicleCode>
                infoElm.AppendChild(vCodeElm);
                XmlElement packageCodeElm = xmlDoc.CreateElement("PackageCode");
                packageCodeElm.InnerText = _packageCode;
                infoElm.AppendChild(packageCodeElm);
                rootElm.AppendChild(infoElm);//</Info>
            }

            xmlDoc.AppendChild(rootElm);
            return xmlDoc;
        }

        /// <summary>
        /// Added to support saving and retrieval of Quotes, Removes all Ferry Items from booking 
        /// </summary>
        /// <returns>True is removed any items</returns>
        public bool removeFerryProducts()
        {
            List<AuroraProduct> filteredCollection = new List<AuroraProduct>(_selectedProducts);
            bool removedItems = false;
            List<AuroraProduct> removedItemsList = new List<AuroraProduct>();

            foreach (AuroraProduct product in filteredCollection)
            {
                if (product.Type == ProductType.FerryCrossing) removedItemsList.Add(product);
                removedItems = true;
            }
            if (removedItems == true)
            {
                foreach (AuroraProduct prd in removedItemsList)
                    filteredCollection.Remove(prd);
                _selectedProducts = filteredCollection.ToArray();
            }
                return removedItems;        
        }
        
        #region rev:mia: 11March2015-B2B Auto generated Aurora Booking Confirmation
        private string _EmailAddressConfirmation;
        public string EmailAddressConfirmation
        {
            get { return _EmailAddressConfirmation; }
            set { _EmailAddressConfirmation = value; }
        }
        #endregion
        

    }
}
