﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace THL.Booking
{
    /// <summary>
    /// Represents Session information for a booking user
    /// </summary>
    public class SessionData
    {

        /// <summary>
        /// Session level Error Protocol for Backend errors
        /// </summary>
        public string LastError { get; set; }

        /// <summary>
        /// The Last requests Agent Code
        /// </summary>
        public string CurrentAgent { get; set; }
        
        /// <summary>
        /// Extra Hires Collection, consider refactoring into collection
        /// This will need refactoring for Retrieve functionality
        /// </summary>
        private string _extraHires;

        string ExtraHires { get { return _extraHires; } }

        public bool AddExtraHires(string paramsStr)
        {
            _extraHires = paramsStr;
            return true;//TODO: define protocol(i.e. counter/status).
        }
        
        private bool hasRequestData;
        public bool HasRequestData
        {
            get { return (availabilityRequestData != null); }//consoider validation of object
        }

        private bool hasResponseData;
        public bool HasResponseData
        {
            get { return (availabilityResponseData != null); }//consoider validation of Response object
        }
        
        AvailabilityRequest availabilityRequestData;
        public AvailabilityRequest AvailabilityRequestData {
            get { return availabilityRequestData; }//consoider validation of Requset object
            set 
            {
                availabilityRequestData = value;
                hasRequestData = true;
            }
        } 

        AvailabilityResponse availabilityResponseData;
        public AvailabilityResponse AvailabilityResponseData {
            get { return availabilityResponseData; }
            set { availabilityResponseData = value; }
        }

        DateTime creationTime;
        public DateTime CreationTime {
            get { return creationTime; }
        }
        
        AuroraBooking _bookingData;
        public bool UpdateBookingData(AuroraBooking bookingData)
        {
            //TODO: merge into existig data, currently overright
            _bookingData = bookingData;            
            return true;//TODO: exp handle
        }

        public AuroraBooking BookingData {
            get { return _bookingData;  }
        }

        public bool HasBookingData { 
            get { return (_bookingData != null); /*TODO: add data vaerfication here..*/ }
        }
        
        public SessionData(DateTime createdOn)
        {
            creationTime = createdOn;
        }

        public CreditCardElement[] availableCards;
        public CreditCardElement[] AvailableCards
        {
            get { return availableCards; }
            set { availableCards = value; }
        }

        public bool HasSurecharge()
        { 
            bool hasSurcharge = false;
            foreach(CreditCardElement cardInfo in availableCards)
            {
                if (cardInfo.Surcharge > 0)
                    return true;
            }
            return hasSurcharge;
        }

        //refactoring 25/01/10
        public bool HasCountryOfResidence = false;
        string countryOfResidence = "AU";//test here string.Empty;        
        public string CountryOfResidence {
            get { return countryOfResidence; }
            set { 
                countryOfResidence = value.ToUpper();//TODO: consider ISO validation here
                HasCountryOfResidence = true;
            }
        }
        //end refactoring 25/01/10
       
        /// <summary>
        /// Added to support availabiliy response caching for B2B at Session level
        /// </summary>
        private B2BSessionCache _b2bCache;
        
        /// <summary>
        /// B2B Session level Cache Support
        /// </summary>
        public B2BSessionCache B2BCache
        {
            get
            {
                if (_b2bCache != null)
                    return _b2bCache;
                else
                {
                    _b2bCache = new B2BSessionCache();
                    return _b2bCache;
                }
            }
            set { 
                _b2bCache = value;
                //TODO: this is a potential cache init point if required
            }        
        }
        




    }
}