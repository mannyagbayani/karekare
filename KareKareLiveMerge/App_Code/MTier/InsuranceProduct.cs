﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

namespace THL.Booking
{
    /// <summary>
    /// Aurora Insurance Item Product
    /// </summary>
    public class InsuranceProduct : AuroraProduct
    {

        bool isInclusive;
        public bool IsInclusive
        {
            get { return isInclusive;  }
            set { isInclusive = value; }
        }

        NameValueCollection requiredActions;
        public NameValueCollection RequiredActions
        {
            get { return requiredActions;  }
            set { requiredActions = value; }
        }
        
        public InsuranceProduct(string _code, string _name, decimal _dailyRate, UOMType _uom, decimal _grossAmt, decimal _maxCharge, string _sapId, int _displayOrder, bool promptForQty, int _minAge, int _maxAge)
            : base(_code, _name, _dailyRate, _uom, _grossAmt, _maxCharge, _sapId, _displayOrder, promptForQty, _minAge, _maxAge)
        {
            productType = ProductType.Insurance;            
        }

        int rating;
        /// <summary>
        /// Support Insurance pricing level sorting and managment
        /// </summary>
        public int Rating 
        {
            get { return rating;  }
            set { rating = value; }
        }

        /// <summary>
        /// Added to support Inclusive Products and fee waivers (Save Quote)
        /// </summary>
        public string WaivedProducts { get; set; }


        public InsuranceProduct(string _sapId)
        {
            sapId = _sapId;
            productType = ProductType.Insurance;
        }
    }
}