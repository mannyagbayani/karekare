﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using THL.Booking;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace THL.Booking
{

    public static class SerializationExtensions
    {
        /// <summary>
        /// Serializes the object to a JSON formatted string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The object being converted to JSON.</param>
        /// <returns></returns>
        public static string ToJSON<T>(this  T obj)
        {
            MemoryStream stream = new MemoryStream();

            try
            {
                //serialize data to a stream, then to a JSON string
                DataContractJsonSerializer jsSerializer = new DataContractJsonSerializer(typeof(T));
                jsSerializer.WriteObject(stream, obj);

                return Encoding.UTF8.GetString(stream.ToArray());
            }
            finally
            {
                stream.Close();
                stream.Dispose();
            }
        }
    }
    [DataContract]
    public struct PriceDetail
    {
        [DataMember]
        public decimal pta;
        [DataMember]
        public decimal pap;
    }


    [DataContract]
    public struct AgentDescription
    {
        [DataMember]
        public string ID;
        [DataMember]
        public string Name;
        [DataMember]
        public string DefaultCurrency;
        [DataMember]
        public string[] CurrencyList;
        [DataMember]
        public List<ExchangeRate> CurrencyRates { get; set; }


        public ExchangeRate GetExRateForCodes(string fromCode, string toCode)
        {
            
            //if (fromCode == "USD")
            //{
            //    ExchangeRate ex = new ExchangeRate();
            //    ex.FromCode = fromCode;
            //    ex.ToCode = fromCode;//offset back to requested country
            //    ex.Rate = 1.0m;
            //    return ex;
            //}

            //rev:mia june 16, 2014 - injection of code to display Gross  and NET AMOUNT 
             if (fromCode.Contains(",") == true)
                fromCode = fromCode.Split(new char[]{','})[0].Trim();

            if (toCode.Contains(",") == true)
                toCode = toCode.Split(new char[] { ',' })[0].Trim();


            try
            {
                //rev:mia sept 20 2013 -- added britz-usa
                if (fromCode == toCode || fromCode == "USD")//agent has no default or none provided
                {
                    ExchangeRate ex = new ExchangeRate();
                    ex.FromCode = fromCode;
                    ex.ToCode = fromCode;//offset back to requested country
                    ex.Rate = 1.0m;
                    return ex;
                }

                else
                {
                    var exRate =
                    from ex in CurrencyRates
                    where ex.FromCode.Equals(fromCode) && ex.ToCode.Equals(toCode)
                    select ex;
                    return exRate.First<ExchangeRate>();
                }
            }
            catch (Exception)
            {
                //rev:mia 01May2015 - NO DEFAULT CURRENCY FOUND
                ExchangeRate currencyRates = new ExchangeRate();
                currencyRates.FromCode = fromCode;
                currencyRates.ToCode = fromCode;
                currencyRates.Rate = 1;
                return currencyRates;
            }
        }

    }

    [DataContract]
    public class THLCountry
    {
        [DataMember]
        public bool selected;
        [DataMember]
        public string code;
        [DataMember]
        public string name;
        
        [DataMember]
        public B2BBrand[] brands;

        public int AddBrand(B2BBrand brand)
        {
            if (brands == null)
                brands = new B2BBrand[] { brand };
            else
            {
                List<B2BBrand> brandLst = new List<B2BBrand>(brands);
                brandLst.Add(brand);
                brands = brandLst.ToArray();
            }
            return brands.Length;
        }

        /// <summary>
        /// Get a countries brand
        /// </summary>
        /// <param name="brandStr">requested brands code</param>
        /// <returns>THL Brand</returns>
        public B2BBrand GetBrand(string brandStr)
        {
            if (brands == null)
                return null;
            foreach(B2BBrand brand in brands)
                if(brand.code.Equals(brandStr))
                    return brand;
            return null;
        
        }



        [DataMember]
        public THLBranch[] locations;

        public THLBranch GetBranch(string code)
        {
            code = code.ToLower();
            if (locations == null) return null;
            foreach (THLBranch branch in locations)
                if (branch.code.ToLower().Equals(code))
                    return branch;
            return null;        
        }
    }


    [DataContract]
    public class THLBranch
    {

        public THLBranch(string _code, string _name, VehicleType _type)
        {
            name = _name;
            code = _code;
            vehicleType = _type;        
        }
        
        [DataMember]
        public string code;
        [DataMember]
        public string name;

        [DataMember]
        public VehicleType vehicleType;

        [DataMember]
        public string Type { 
            get { return vehicleType.ToString();/* JSON converter */ }
            set { vehicleType = VehicleType.Both;/* TODO: swap str to type */ }
        }

        [DataMember]
        public string BrandStr = string.Empty;

    }


    [DataContract]
    public class B2BBrand
    {
        [DataMember]
        public string name;
        
        [DataMember]
        public string code;
        
        [DataMember]
        public bool selected;
        
        [DataMember]
        public THLPackage[] packages;

        [DataMember]
        public THLVehicle[] vehicles;

    }


    [DataContract]
    public class THLVehicle
    {

        public THLVehicle(string _code, string _name, VehicleType _type)
        {
            code = _code;
            name = _name;
            type = _type;        
        }
        
        [DataMember]
        public string code;

        [DataMember]
        public string name;

        [DataMember]
        public VehicleType type;

        [DataMember]
        public int MaxPax;


        public CountryCode Country { get; set; }

        public char Brand { get; set; }
    
    }


    /// <summary>
    /// TODO: refactor and encapsulate, used to consume the old B2B OTA WS
    /// </summary>
    [DataContract]
    public class THLPackage
    {

        public THLPackage(string _code, string _name, bool _isInc, string _vt, string _from, string _to, bool _isSp)
        {
            name = _name;
            code = _code;
            isInc = _isInc;
            vt = _vt;
            from = _from;
            to = _to;
            isSp = _isSp;
        }

        public THLPackage(DAL.PackageType _package)
        {
            code = _package.Code;
            name = code + " - " + _package.Name;
            string[] packageParams = _package.VehType.Split(',');
            vt = packageParams[0];
            brand = packageParams[1];
            country = packageParams[2];

            from = _package.TravelFrom;
            to = _package.TravelTo;

        }


        [DataMember]
        public string name;
        [DataMember]
        public string code;
        [DataMember]
        public bool isInc;
        [DataMember]
        public string vt;
        [DataMember]
        public string from;
        [DataMember]
        public string to;
        [DataMember]
        public bool isSp;

        [DataMember]
        public string country;
        [DataMember]
        public string brand;
       

    }

    [DataContract]
    public class AgentLoginData
    {
        /// <summary>
        /// Load the Agents Configuration params from session login
        /// </summary>
        /// <param name="st"></param>
        void ProccessAgentDutyCode(SourceType st)
        {
            try
            {
                string[] agentDutyParams = st.AgentDutyCode.Split('~');
                HasGrossNetOptions = agentDutyParams[1].ToUpper().Equals("YES");
                GrossDefaultOn = agentDutyParams[3].ToUpper().Equals("G");  
                GrossNetOptions = agentDutyParams[2].ToUpper();
                HasCurrencyOption = agentDutyParams[4].ToUpper().Equals("YES");

                Sine = st.AgentSine;

            }
            catch (Exception ex)
            {
                //TODO: refine biz requirements on failed parsing, strict at the moment
                string msg = ex.Message;
                GrossDefaultOn = false;
                GrossNetOptions = "G";
                HasCurrencyOption = false;                
                THLDebug.LogError(ErrorTypes.Application, "AgentLoginData.ProccessAgentDutyCode", msg, "{agentDuty:'" + st.AgentDutyCode + "'}");            
            }
        }

        /// <summary>
        /// Login User
        /// </summary>
        [DataMember]
        public string Sine { get; set; }



        public AgentLoginData(SourceType st)
        {
            ProccessAgentDutyCode(st);        
        }
        /// <summary>
        /// Default Agent Code
        /// </summary>
        [DataMember]
        public string DefaultAgentCode { get; set; }//G,N

        /// <summary>
        /// Is User on Groos View as default 
        /// </summary>
        [DataMember]
        public bool GrossDefaultOn { get; set; }

        /// <summary>
        /// The Possible Gross/Nett options
        /// </summary>
        [DataMember]
        public string GrossNetOptions { get; set; }//G,N

        [DataMember]
        public bool HasGrossNetOptions { get; set; }
        /// <summary>
        /// Can the agent view alternative ForEx
        /// </summary>
        [DataMember]
        public bool HasCurrencyOption { get; set; }

        /// <summary>
        /// Currency Associated with this agent
        /// </summary>


        /*public bool AddExchangeRate(ExchangeRate rate)
        {
            try
            {
                List<ExchangeRate> exList;
                if (CurrencyRates == null)
                    exList = new List<ExchangeRate>();
                else
                    exList = new List<ExchangeRate>(CurrencyRates);
                exList.Add(rate);
                CurrencyRates = exList.ToArray();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: Log
                return false;
            }
        }*/

        [DataMember]
        public AgentDescription[] agents { get; set; }

        /// <summary>
        /// Returned a requested agent for a given agent code
        /// </summary>
        /// <param name="agentCode"></param>
        /// <returns></returns>
        public AgentDescription GetAgentForCode(string agentCode)
        {
            foreach(AgentDescription agent in agents)
                if(agent.ID.Equals(agentCode))
                    return agent;
            AgentDescription nullAgent = new AgentDescription();
            nullAgent.ID = string.Empty; //TODO: define miss protocol
            return nullAgent;
        }

        public bool AddAgentDescription(AgentDescription agent)
        {
            try
            {
                List<AgentDescription> agList;
                if (agents == null)
                    agList = new List<AgentDescription>();
                else
                    agList = new List<AgentDescription>(agents);
                agList.Add(agent);
                agents = agList.ToArray();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: Log
                return false;
            }
        }

        public THLPackage[] packages { get; set; }

        public int AddPackages(DAL.PackageType[] _packages)
        {
            int count = 0;
            List<THLPackage> pkgList = new List<THLPackage>();
            foreach (DAL.PackageType pkg in _packages)
            {
                THLPackage currentPkg = new THLPackage(pkg);
                pkgList.Add(currentPkg);
                count++;
            }
            packages = pkgList.ToArray();
            return count;
        }

    }
}