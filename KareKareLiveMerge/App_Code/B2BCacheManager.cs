﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using THL.Booking;

namespace THL.Booking
{

    public enum VehicleType
    {
        Car = 0,
        Campervan = 1,
        Both = 2,
        None = 3
    }

    public partial class B2BCacheManager//Added a refactored segment for cacheability at B2BCacheManager.cs
    {

        THLCountry[] countries;

        public THLCountry GetCountry(string code)
        {
            if (countries == null) return null;
            foreach (THLCountry country in countries)
                if (country.code.ToLower().Equals(code.ToLower())) return country;
            return null;
        }

        public THLCountry[] Countries { get { return countries; } }

        string localDataPath;
        bool LoadSuccess { get; set; }

        public B2BCacheManager()
        {
            localDataPath = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"];
            LoadSuccess = load();
        }


        private bool load()
        {
            //load the 3 brands
            //rev:mia- oct.16, 2012 - addition of Alpha and Econo
            //rev:mia- aug.27, 2013 - addition of roadbear and US britz
            char[] brandChars = { 'b', 'm', 'p', 'y', 'q', 'a', 'e', 'u', 'z', 'r' };
            List<THLCountry> countryList = new List<THLCountry>();
            foreach (char brandChar in brandChars)
            {
                string brandDataPath = localDataPath + "/" + brandChar + ".xml";
                XmlDocument xml = new XmlDocument();
                xml.Load(brandDataPath);
                XmlNodeList countriesXMLList = xml.SelectNodes("countries/country");
                string currentCountryCode = string.Empty;
                foreach (XmlNode country in countriesXMLList)
                {
                    List<THLBranch> branchesList = new List<THLBranch>();
                    string currentCode = country.Attributes["code"].Value.ToLower();
                    THLCountry currentCountry = GetCountry(currentCode);
                    if (currentCountry == null)
                    {
                        currentCountry = new THLCountry();
                        currentCountry.code = country.Attributes["code"].Value;
                        currentCountry.name = country.Attributes["name"].Value;
                        List<THLCountry> clst = (countries == null) ? new List<THLCountry>() : new List<THLCountry>(countries);
                        clst.Add(currentCountry);
                        countries = clst.ToArray();
                    }

                    foreach (XmlNode branchNode in country.SelectNodes("branches/branch"))
                    {
                        VehicleType vt;
                        switch (branchNode.Attributes["type"].Value)
                        {
                            case "av":
                                vt = VehicleType.Campervan;
                                break;
                            case "ac":
                                vt = VehicleType.Car;
                                break;
                            case "all":
                                vt = VehicleType.Both;
                                break;
                            default:
                                vt = VehicleType.Both;
                                //TODO: log no service type branch and handle here
                                break;
                        }
                        string branchCode = branchNode.Attributes["value"].Value;
                        //THLBranch currentBranch = currentCountry.GetBranch(branchCode);

                        //if (currentBranch == null)
                        //{ load for all brands
                        THLBranch currentBranch = new THLBranch(branchNode.Attributes["value"].Value, branchNode.Attributes["name"].Value, vt);
                        currentBranch.BrandStr = brandChar.ToString().ToUpper();
                        List<THLBranch> blst = currentCountry.locations == null ? new List<THLBranch>() : new List<THLBranch>(currentCountry.locations);
                        blst.Add(currentBranch);
                        currentCountry.locations = blst.ToArray();
                        //}                      
                    }

                    List<THLVehicle> vehicleList = new List<THLVehicle>();
                    foreach (XmlNode vehicleNode in country.SelectNodes("vehicles/vehicle"))
                    {
                        VehicleType vt = (vehicleNode.Attributes["type"].Value == "ac" ? VehicleType.Car : VehicleType.Campervan);
                        int maxPassengers = 0;
                        int.TryParse(vehicleNode.Attributes["maxPassengers"].Value, out maxPassengers);
                        THLVehicle current = new THLVehicle(vehicleNode.Attributes["value"].Value, vehicleNode.Attributes["name"].Value, vt);//TODO: exp handle
                        current.Brand = brandChar;
                        current.Country = AvailabilityHelper.GetCountryCodeForString(currentCountry.code);
                        current.MaxPax = maxPassengers;
                        vehicleList.Add(current);
                    }

                    if (currentCountry.GetBrand(brandChar.ToString()) == null)
                    {
                        B2BBrand currentBrand = new B2BBrand();
                        currentBrand.code = brandChar.ToString();//normalise to char
                        currentBrand.vehicles = vehicleList.ToArray();
                        currentCountry.AddBrand(currentBrand);

                    }
                    //currentCountry.GetBrand(brandChar.ToString()).vehicles = vehicleList.ToArray();
                }
            }
            //TODO: exp handle and return false on failure
            return true;
        }

        /// <summary>
        /// Merge Agents Data with Location/Brand Cache
        /// </summary>
        /// <param name="countries"></param>
        /// <returns></returns>
        public THLCountry[] GetCountriesForAgent(AgentLoginData agentData)
        {
            //rev:mia- oct.16, 2012 - addition of Alpha and Econo
            //rev:mia- aug.27, 2013 - addition of roadbear and US britz
            string[] brands = new string[] { "B", "M", "P", "Y", "Q", "A", "E", "U", "Z", "R" }; //TODO: refactor

            int counter = 0;
            foreach (THLCountry country in countries)
            {
                if (counter++ == 0) country.selected = true;//TODO: define biz logic for the default/selected country                
                List<B2BBrand> b2bBrandsList = new List<B2BBrand>();
                int brandCounter = 0;
                foreach (string brandStr in brands)
                {

                    B2BBrand currentBrand = new B2BBrand();
                    currentBrand.selected = (brandCounter++ == 0);//TODO: define selected brand biz logic first for now
                    currentBrand.code = brandStr;
                    currentBrand.name = THLBrand.GetNameForBrand(brandStr);
                    currentBrand.packages = B2BAvailabilityHelper.GetPackagesForCountryBrand(agentData.packages, brandStr, country.code);

                    if (country.code == "US" && (brandStr == "Z" || brandStr == "R"))
                    {
                        currentBrand.vehicles = country.GetBrand(brandStr.ToLower()).vehicles;//pass brands vehicles forward, TODO: normalise params                  
                    }
                    else
                    {
                        if (country.code != "US" && brandStr != "Z" && brandStr != "R")
                        {
                            currentBrand.vehicles = country.GetBrand(brandStr.ToLower()) != null ? country.GetBrand(brandStr.ToLower()).vehicles : null;//pass brands vehicles forward, TODO: normalise params                  
                            //currentBrand.vehicles = country.GetBrand(brandStr.ToLower()).vehicles;//pass brands vehicles forward, TODO: normalise params                  
                        }
                    }
                    b2bBrandsList.Add(currentBrand);
                }
                country.brands = b2bBrandsList.ToArray();
            }
            return countries;
        }
    }
}