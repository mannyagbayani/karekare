﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using THL.Booking;

/// <summary>
/// Summary description for Content_Availability_Displayer
/// </summary>
public partial class Content_Availability_Displayer : System.Web.UI.Page
{
    public Content_Availability_Displayer() { }
    public static string RenderPriceDetails(AvailabilityItem availabilityRow)
    {
        int colSpans = 4;

        StringBuilder sb = new StringBuilder();
        bool hasFreeDays = false;
        AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
        AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
        bool hasDiscount = false, hasPercentageDiscount = false;

        foreach (AvailabilityItemChargeDiscount discount in aidc)
        {
            //if (discount.IsPercentage) hasDiscount = true;// 04/08/09 only consider percentage discount ,TODO: dive into all ratebands           
            if (discount.IsPercentage) hasPercentageDiscount = true;
        }

        hasDiscount = AvailabilityHelper.HasDiscountedDays(availabilityRateBands);


        colSpans = (hasDiscount || hasFreeDays ? colSpans : --colSpans);

        if (availabilityRateBands != null && availabilityRateBands.Length > 0)
            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands) //order?
                if (chargeRateRow.IncludesFreeDay > 0)
                    hasFreeDays = true;
        sb.Append("<div class='PriceDetailsList " + (hasFreeDays ? string.Empty : "NoFreeDays") + (hasDiscount ? string.Empty : " NoDiscount") + "'>");
        sb.Append("<table class='chargesTable'>");
        sb.Append("<thead>");
        sb.Append("<tr>");
        sb.Append("<th class='RateTH'>Product Description</th>");
        sb.Append("<th class='FreeTH'><span>Free Days</span><span rel='freeDays' class='popup' style='display:none;'>[?]</span></th>");
        sb.Append("<th class='HireTH'>Hire Days</th>");
        sb.Append("<th class='PerTH'>Per Day Price</th>");
        sb.Append(!hasDiscount ? "" : "<th class='DiscTH' " + (hasDiscount ? "" : "style='display:none;'") + "><span>Discounted Day Price</span>" + (hasPercentageDiscount ? "<span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>" : string.Empty) + "</th>");
        sb.Append("<th class='TotalTH'>Total</th>");
        sb.Append("</tr>");
        sb.Append("</thead>");
        sb.Append("<tbody>");

        bool displayTotal = false;
        decimal longHireDiscount = availabilityRow.GetLongHireDiscount();
        decimal totalCharge = availabilityRow.GetTotalPrice();

        if (availabilityRateBands != null)//has several rate bands
        {
            if (availabilityRateBands.Length > 1)
                displayTotal = true;
            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
            {
                string freeDaysStr = string.Empty;
                if (chargeRateRow.IncludesFreeDay > 0)
                {
                    freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                    hasFreeDays = true;
                }
                string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                sb.Append("<tr>");
                sb.Append("<td class='rt'>" + fromDateStr + " to " + toDateStr + "</td>");
                sb.Append("<td class='fd'>" + freeDaysStr + "</td>");
                sb.Append("<td class='hp'>" + chargeRateRow.HirePeriod + " day(s)</td>");
                sb.Append("<td class='pdp'>$" + chargeRateRow.OriginalRate.ToString("F") + "</td>");
                sb.Append(!hasDiscount ? "" : "<td class='dpd' " + (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F") + "</td>");
                sb.Append("<td>$" + chargeRateRow.GrossAmount.ToString("F") + "</td>");
                sb.Append("</tr>");
            }
        }
        else //single rate band, TODO: revisit formating
        {
            AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
            sb.Append("<tr>");
            sb.Append("<td class='rt'>" + vehicleRow.ProductName + "</td>");
            sb.Append("<td class='fd'>" + /* free days? */  "</td>");
            sb.Append("<td class='hp'>" + vehicleRow.HirePeriod + " day(s)</td>");
            sb.Append("<td class='pdp'>" + /*chargeRateRow.OriginalRate.ToString("F") +*/ "</td>");
            sb.Append("<td class='dpd' " /*+ (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F")*/ + "</td>");
            sb.Append("<td>$" + vehicleRow.ProductPrice + "</td>");
            sb.Append("</tr>");
        }

        AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
        if (aCharges != null && aCharges.Length > 0)
        {
            displayTotal = true;
            foreach (AvailabilityItemChargeRow charge in aCharges)
            {
                if (charge.IsBonusPack || charge.AdminFee > 0) continue;

                if (charge.HirePeriod > 1)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + charge.ProductName + "</td>");
                    sb.Append("<td class='hp'>" + charge.HirePeriod + " day(s)</td>");
                    sb.Append("<td class='pdp'>$" + charge.AverageDailyRate.ToString("F") + "</td>");
                    if (hasDiscount) sb.Append("<td class='pdp'>" + "&nbsp;" + "</td>");
                    if (hasFreeDays) sb.Append("<td class='dpd'>" + "&nbsp;" + "</td>");
                    sb.Append("<td>" + (charge.ProductPrice > 0 ? "$" + charge.ProductPrice : "included") + "</td>");
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr rel='" + charge.ProductCode + "'>");
                    sb.Append("<td colspan='" + (colSpans + (hasFreeDays ? 1 : 0)) + "'>" + charge.ProductName + "</td>");
                    sb.Append("<td>" + (charge.ProductPrice == 0m ? "included" : "$" + charge.ProductPrice.ToString("F")) + "</td>");
                    sb.Append("</tr>");
                }
            }

        }


        decimal adminFeeFactor = 1.0m;
        if (availabilityRow.AdminFee > 0 && availabilityRow.AdminFee > 0)
        {
            adminFeeFactor = 1.0m + (availabilityRow.AdminFee / 100);
            sb.Append("<tr>");
            sb.Append("<td colspan='" + (colSpans + (hasFreeDays ? 1 : 0)) + "'>" + (availabilityRow.AdminFee).ToString("F") + "% Administration fee</td>");
            sb.Append("<td>$" + (availabilityRow.EstimatedTotal * (availabilityRow.AdminFee / 100)).ToString("F") + "</td>");
            sb.Append("</tr>");
        }

        if (displayTotal)
        {
            colSpans = 3 + (hasDiscount ? 1 : 0) + (hasFreeDays ? 1 : 0);
            sb.Append("<tr class='total'>");
            sb.Append("<td class='TotalTD' colspan='" + colSpans + "'>Total</td>");
            sb.Append("<td>$" + (availabilityRow.EstimatedTotal * adminFeeFactor).ToString("N") + "</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");
        sb.Append(getDiscountPopUp(availabilityRow));
        sb.Append("</div>");
        string result = sb.ToString();
        return result;
    }

    private static string getDiscountPopUp(AvailabilityItem availabilityRow)
    {
        AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
        if (aidc == null || aidc.Length == 0)
            return null;
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='DiscountedDaysPopUp bubbleBody'>");
            sb.Append("<h3>Discounted Day Price</h3>");
            sb.Append("<table>");
            sb.Append("<tbody>");
            decimal totalDis = 0m;
            string discountUnitDisplay = string.Empty;
            foreach (AvailabilityItemChargeDiscount aid in aidc)
            {
                if (aid.IsPercentage)
                {
                    totalDis += aid.DisPercentage;//TODO: Condider summing discount totals on non percentage as well
                    discountUnitDisplay = aid.DisPercentage + "%";
                }
                else
                {
                    discountUnitDisplay = "$" + aid.DisPercentage;
                    continue;
                }
                sb.Append("<tr>");
                sb.Append("<td>" + aid.CodDesc + "</td>");
                sb.Append("<td>" + discountUnitDisplay + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("<tr class='total' rel='" + totalDis + "'>");
            sb.Append("<td>Total Discount</td>");
            sb.Append("<td>" + totalDis + "%</td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("<span class='note'>(off standard per day rates)</span>");
            sb.Append("</div>");
            return sb.ToString();
        }
    }

    public static string RenderAvailabilityRow(AvailabilityItem availabilityRow, AvailabilityRequest availabilityRequest)
    {
        decimal adminFeeFactor = 1.0m;//use for adding Admin Fee if applicable
        if (availabilityRow.AdminFee > 0)
            adminFeeFactor = 1.0m + (availabilityRow.AdminFee / 100);

        string httpPrefix = "http://";//content pages are always HTTP

        string assetsURL = httpPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];

        string vehicleContentLink = ContentManager.GetVehicleLink(availabilityRow.Brand, availabilityRequest.CountryCode, availabilityRow.VehicleCode, availabilityRow.Brand);

        char brandChar = THLBrand.GetBrandChar(availabilityRow.Brand);

        string vehicleName = availabilityRow.VehicleName;
        string vehicleCode = availabilityRow.VehicleCode;
        string packageName = availabilityRow.PackageCode;
        string vehicleTypeStr = (availabilityRequest.VehicleType == THL.Booking.VehicleType.Car ? "Cars" : "Campervans");

        //Image Convention
        THLDomain appDomain = ApplicationManager.GetDomainForURL(HttpContext.Current.Request.Url.Host);

        string vehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(availabilityRequest.CountryCode, availabilityRow.Brand, availabilityRequest.VehicleType, vehicleCode, assetsURL, false);
        string inclusionImagePath = AvailabilityHelper.GetVehicleImageBasePath(availabilityRequest.CountryCode, availabilityRow.Brand, availabilityRequest.VehicleType, vehicleCode, assetsURL, true);

        string vehicleInclusionImage = String.Format("<img src='{0}-InclusionIcons.gif' border='0' title='{1}' />", inclusionImagePath, availabilityRow.Brand);
        string vehicleClearCut72Image = String.Format("<img src='{0}-ClearCut-72.jpg' border='0' title='{1}' />", vehicleImagePath, vehicleCode);
        string vehicleClearCut144Image = String.Format("<img src='{0}-ClearCut-144.jpg' border='0' title='{1}' />", vehicleImagePath, vehicleCode);
        string noAvailMsg = "Search online to find the following<br/>alternative travel options for this vehicle:<br /><b>Reverse route, +/- 10 days, Shorter hire.</b>";
        string noAvailLink = "javascript:preloadPage(\"vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brandChar + "\")";
        string noAvailTitle = "Unavailable - Search Alternative Availability";
        bool shortLead = false;
        if (!string.IsNullOrEmpty(availabilityRow.ErrorMessage) && availabilityRow.ErrorMessage.Contains("DISP24HR"))//swap above if short leadtime detected
        {//less then 24hr lead
            noAvailMsg = "Let our 24hr res team find a vehicle for you<br />We can search all the big brands:<br /><b>Britz, Maui, Backpacker, Explore More</b>";
            //noAvailTitle = (containerSite == THLBrands.AIRNZ ? "Bookings cannot be made within 24 hours" : "Travel is within 24 hours - Contact Us");
            //noAvailLink = AppDomain.ParentSite + "/callus";
            //TODO: consider this : ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "CrossSale").Content for cross sell
            shortLead = true;//phone and no alt avail
        }
        else if (!string.IsNullOrEmpty(availabilityRow.ErrorMessage) && availabilityRow.ErrorMessage.Contains("DISPRateCalc"))
        {//weekly mantenance case
            noAvailMsg = "Let our 24hr res team find a vehicle for you<br />We can search all the big brands:<br /><b>Britz, Maui, Backpacker, Explore More</b>";
            noAvailTitle = "We are undergoing scheduled maintenance, will be back 12.15 NZST";
            //noAvailLink = AppDomain.ParentSite + "/callus";
            //TODO: consider this : ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "CrossSale").Content for cross sell
            shortLead = true;//phone and no alt avail            
        }

        //add additional tokens here

        StringBuilder sb = new StringBuilder();
        if (availabilityRow.AvailabilityType == AvailabilityItemType.Available || availabilityRow.AvailabilityType == AvailabilityItemType.OnRequest)
        {
            sb.Append("<div class='VehicleItem'>");
            sb.Append("<img src='" + assetsURL + "/images/" + brandChar + "/icons/rowLogo.gif' title='" + availabilityRow.Brand + "' class='RowLogo PopUp' />");
            sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='VehicleThumb PopUp'>");
            sb.Append(vehicleClearCut72Image);
            sb.Append("</a>");
            sb.Append("<div class='VehicleFeatures'>");
            sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='PopUp'>" + THLBrand.GetNameForBrand(availabilityRow.Brand) + " " + vehicleName + "</a>");
            sb.Append(vehicleInclusionImage);
            sb.Append("</div>");
            sb.Append("<div class='PriceDescription'>");
            sb.Append("<a href='Configure.aspx?avp=" + availabilityRow.AVPID + "' class='" + (availabilityRow.AvailabilityType == AvailabilityItemType.Available ? "Avail" : "OnRequest") + "'>" + (availabilityRow.AvailabilityType == AvailabilityItemType.Available ? "AVAILABLE NOW" : "ON REQUEST") + "</a>");
            if (availabilityRow.DisplayPromoTile)
                sb.Append("<span><img src='" + assetsURL + "/CentralLibraryImages/" + THLBrand.GetNameForBrand(availabilityRow.Brand) + "/Promotions/" + availabilityRow.PackageCode + ".png' title='Promotion' /></span>");//TODO: file convention on img src for Promos goes here                
            sb.Append("</div>");
            sb.Append("<div class='TotalPrice' title='" + packageName + "'>");
            //sb.Append(getForeignListElm(availabilityRow.EstimatedTotal * adminFeeFactor));
            sb.Append("<a class='ShowPriceList'>Price Details</a>");
            sb.Append("</div>");
            sb.Append("<a href='Configure.aspx?avp=" + availabilityRow.AVPID + "' class='SelectBtn'>Select</a>");
            sb.Append("</div>");
            sb.Append(RenderPriceDetails(availabilityRow));
        }
        else
        {
            bool isBlocking = false;
            if (availabilityRow.AvailabilityType == AvailabilityItemType.BlockingRuleTaC)
            {
                isBlocking = true;
            }

            string errMsg = availabilityRow.BlockingRuleMessage + availabilityRow.ErrorMessage;
            sb.Append("<div class='VehicleItem " + (isBlocking ? "Blocking" : string.Empty) + "'>");
            sb.Append("<img src='" + assetsURL + "/images/" + brandChar + "/icons/rowLogo.gif' title='" + availabilityRow.Brand + "' class='RowLogo' />");
            sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='VehicleThumb PopUp'>");
            sb.Append(vehicleClearCut72Image);
            sb.Append("</a>");
            sb.Append("<div class='VehicleFeatures'>");
            sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='PopUp'>" + THLBrand.GetNameForBrand(availabilityRow.Brand) + " " + vehicleName + "</a>");
            sb.Append(vehicleInclusionImage);
            sb.Append("</div>");
            sb.Append("<div class='Err'><span>" + errMsg + "</span></div>");
            sb.Append("<div class='Limited'><div class='Top'><span><a href='" + noAvailLink + "' " + (shortLead ? "class='PopUp'" : string.Empty) + ">" + noAvailTitle + "</a></span><span class='Contact'>" + noAvailMsg + "</span></div>");

            if (!shortLead)
                sb.Append("<div class='AALink LeftLnk'><a class='aAvail' href='" + noAvailLink + "'>Search Alternate Availability</a></div>");

            sb.Append("</div>");
            sb.Append("</div>");
        }
        return sb.ToString();
    }

}