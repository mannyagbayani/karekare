﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections;
using THLAuroraWebInterface;
namespace THL.Booking
{

    /// <summary>
    /// Displayer Functionality for B2B availability
    /// </summary>
    public class B2BAvailabilityDisplayer
    {

        AvailabilityItem _item;

        public B2BAvailabilityDisplayer(AvailabilityItem availabilityItem)
        {
            _item = availabilityItem;
        }

        /// <summary>
        /// pap = payable at pick up
        /// pta = payable to agent
        /// isCustomerCharge = pap
        /// </summary>
        /// <param name="availabilityRow"></param>
        /// <returns></returns>
        public static string RenderB2BPriceDetails(AvailabilityItem availabilityRow, ref decimal pta, ref decimal pap)
        {
            int colSpans = 7;

            StringBuilder sb = new StringBuilder();
            bool hasFreeDays = false;
            AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
            AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
            bool hasDiscount = false, hasPercentageDiscount = false;
            foreach (AvailabilityItemChargeDiscount discount in aidc)
            {
                //if (discount.IsPercentage) hasDiscount = true;// 04/08/09 only consider percentage discount ,TODO: dive into all ratebands           
                if (discount.IsPercentage) hasPercentageDiscount = true;
            }

            hasDiscount = AvailabilityHelper.HasDiscountedDays(availabilityRateBands);

            hasFreeDays = AvailabilityHelper.HasFreeDays(availabilityRateBands);

            colSpans -= Convert.ToInt16(!hasFreeDays) + Convert.ToInt16(!hasDiscount);

            sb.Append("<div class='PriceDetailsList " + (hasFreeDays ? string.Empty : "NoFreeDays") + (hasDiscount ? string.Empty : " NoDiscount") + "'>");
            sb.Append("<table class='chargesTable vehicle'>");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append("<th class='RateTH'>Product Description</th>");
            if (hasFreeDays) sb.Append("<th class='FreeTH'><span>Free Days</span><span rel='freeDays' class='popup' style='display:none;'>[?]</span></th>");
            sb.Append("<th class='HireTH'>Hire Days</th>");
            sb.Append("<th class='PerTH'>Per Day Price</th>");
            if (hasDiscount) sb.Append("<th class='DiscTH'><span>Discounted Day Price</span>" + (hasPercentageDiscount ? "<span rel='DiscountedDaysPopUp' class='popup' title='' onmouseover='initPopUp(this)'>[?]</span>" : string.Empty) + "</th>");
            sb.Append("<th class='PayToAgent'>Payable To Agent</th>");
            //rev:mia April 1 2014 Payable to supplier messages
            string payablemessage = Config.GetPayablePickupMessage("PayableAtPickupTextCamelCase", availabilityRow.AItemChargeCollection[0].ProductClass);
            sb.Append("<th class='PayAtPU'>" + payablemessage + @"</th>");
            //sb.Append("<th class='TotalTH'>Total</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            bool displayTotal = false;
            decimal longHireDiscount = availabilityRow.GetLongHireDiscount();
            decimal totalCharge = availabilityRow.GetTotalPrice();

            //rate bands
            if (availabilityRateBands != null)//has several rate bands
            {
                if (availabilityRateBands.Length > 1)
                    displayTotal = true;
                foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
                {
                    string freeDaysStr = string.Empty;
                    if (chargeRateRow.IncludesFreeDay > 0)
                    {
                        freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                        hasFreeDays = true;
                    }
                    string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                    string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                    sb.Append("<tr>");
                    sb.Append("<td class='rt'>" + availabilityRow.VehicleName + " " + fromDateStr + " to " + toDateStr + "</td>");
                    if (hasFreeDays) sb.Append("<td class='fd'>" + freeDaysStr + "</td>");
                    sb.Append("<td class='hp'>" + chargeRateRow.HirePeriod + " day(s)</td>");
                    sb.Append(GetPriceColumn("pdp", chargeRateRow.OriginalRate, false));
                    if (hasDiscount) sb.Append(GetPriceColumn("dpd", chargeRateRow.DiscountedRate, false));
                    sb.Append(chargeRateRow.IsCustomerCharge ? "<td class='pta'></td>" + GetPriceColumn("pap", chargeRateRow.GrossAmount, true) : GetPriceColumn("pta", chargeRateRow.GrossAmount, true) + "<td></td>");
                    //sb.Append(GetPriceColumn("subtotal", chargeRateRow.GrossAmount, false));
                    sb.Append("</tr>");
                    CalculateSubTotal(chargeRateRow.IsCustomerCharge, chargeRateRow.GrossAmount, ref pta, ref pap);
                }
            }
            else //single rate band, TODO: revisit formating
            {
                AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
                sb.Append("<tr>");
                sb.Append("<td class='rt'>" + vehicleRow.ProductName + "</td>");
                if (hasFreeDays) sb.Append("<td class='fd'>&nbsp;</td>");
                sb.Append("<td class='hp'>" + vehicleRow.HirePeriod + " day(s)</td>");
                sb.Append("<td class='pdp'>&nbsp;</td>");
                if (hasDiscount) sb.Append("<td class='dpd'>&nbsp;</td>");
                sb.Append(vehicleRow.IsCustomerCharge ? "<td></td>" + GetPriceColumn("pap", vehicleRow.ProductPrice, true) : GetPriceColumn("pta", vehicleRow.ProductPrice, true) + "<td></td>");
                //sb.Append(GetPriceColumn("subtotal", vehicleRow.ProductPrice, false));
                sb.Append("</tr>");
                CalculateSubTotal(vehicleRow.IsCustomerCharge, vehicleRow.ProductPrice, ref pta, ref pap);
            }

            //extra hire items
            AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
            if (aCharges != null && aCharges.Length > 0)
            {
                bool added = false;
                displayTotal = true;
                foreach (AvailabilityItemChargeRow charge in aCharges)
                {
                    //if (charge.IsBonusPack || charge.AdminFee > 0) continue;

                    if (charge.HirePeriod > 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + charge.ProductName + "</td>");
                        if (hasFreeDays) sb.Append("<td class='fd'>&nbsp;</td>");
                        sb.Append("<td class='hp'>" + charge.HirePeriod + " day(s)</td>");
                        sb.Append(GetPriceColumn("pdp", charge.AverageDailyRate, false));
                        if (hasDiscount) sb.Append("<td class='dpd'>" + "&nbsp;" + "</td>");
                        sb.Append(charge.IsCustomerCharge ? "<td></td>" + GetPriceColumn("pap", charge.ProductPrice, true) : GetPriceColumn("pta", charge.ProductPrice, true) + "<td></td>");
                        //sb.Append(GetPriceColumn("subtotal", charge.ProductPrice, false));
                        sb.Append("</tr>");
                        
                        if (System.Web.HttpContext.Current.Request["prdId"] != null )
                        {
                            if (System.Web.HttpContext.Current.Request["pap"] != null)
                                {
                                    if (!added)
                                    {
                                        added = true;
                                        sb.Append(@"<!--<tr><td>manny</td></tr>-->");
                                    }
                                }
                        }
                    }
                    else
                    {
                        sb.Append("<tr rel='" + charge.ProductCode + "'>");
                        sb.Append("<td colspan='" + (colSpans - 2) + "'>" + charge.ProductName + "</td>");
                        sb.Append(charge.IsCustomerCharge ? "<td></td>" + GetPriceColumn("pap", charge.ProductPrice, true) : GetPriceColumn("pta", charge.ProductPrice, true) + "<td></td>");
                        //sb.Append(GetPriceColumn("subtotal", charge.ProductPrice, false));
                        sb.Append("</tr>");
                    }
                    CalculateSubTotal(charge.IsCustomerCharge, charge.ProductPrice, ref pta, ref pap);
                }

            }

            //admin fee
            decimal adminFeeFactor = 1.0m;
            if (availabilityRow.AdminFee > 0 && availabilityRow.AdminFee > 0)
            {
                adminFeeFactor = 1.0m + (availabilityRow.AdminFee / 100);
                sb.Append("<tr>");
                sb.Append("<td colspan='" + colSpans + "'>" + (availabilityRow.AdminFee).ToString("F") + "% Administration fee</td>");
                //sb.Append(GetPriceColumn("subtotal", availabilityRow.EstimatedTotal * (availabilityRow.AdminFee / 100), false));
                sb.Append("</tr>");
            }

            //total
            if (displayTotal)
            {
                sb.Append("<tr class='total'>");
                sb.Append("<td class='TotalTD' colspan='" + (colSpans - 2) + "'>Total</td>");
                sb.Append(GetPriceColumn("pta", pta, true));
                sb.Append(GetPriceColumn("pap", pap, true));
                //sb.Append(GetPriceColumn("", availabilityRow.EstimatedTotal * adminFeeFactor, false));
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append(GetDiscountPopUp(availabilityRow));
            
            PriceDetail pd = new PriceDetail();
            pd.pap = pap;
            pd.pta = pta;
            sb.Append("<script>var vehicleB2BTotals=" + pd.ToJSON() + ";</script>");

            string result = sb.ToString();
            return result;
        }

        private static string GetPriceColumn(string colClasses, decimal price, bool hideOnZero)
        {
            string priceInCurrencyFormat = hideOnZero ? "" : "Included";
            string exRateClass = "";
            if (price > 0)
            {
                priceInCurrencyFormat = price.ToString("F");
                exRateClass = "exRate";
            }
            return String.Format("<td class='{0} {1}' rel='{2}'>{2}</td>", colClasses, exRateClass, priceInCurrencyFormat);
        }

        /// <summary>
        /// TODO: to be moved into B2BAvailabilityHelper class 
        /// Render Price Details Table for a given Availability Row Object
        /// </summary>
        /// <param name="availabilityRow"></param>
        /// <returns></returns>
        private static void CalculateSubTotal(bool isCustomerCharge, decimal price, ref decimal ptaTotal, ref decimal papTotal)
        {
            if (isCustomerCharge)
                papTotal += price;
            else
                ptaTotal += price;
        }

        /// <summary>
        /// TODO: to be moved into b2bavailabilityhelper.cs
        /// Discount PopUps 
        /// </summary>
        /// <param name="availabilityRow"></param>
        /// <returns></returns>
        private static string GetDiscountPopUp(AvailabilityItem availabilityRow)
        {
            AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
            if (aidc == null || aidc.Length == 0)
                return null;
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class='DiscountedDaysPopUp bubbleBody'>");
                sb.Append("<h3>Discounted Day Price</h3>");
                sb.Append("<table>");
                sb.Append("<tbody>");
                decimal totalDis = 0m;
                string discountUnitDisplay = string.Empty;
                foreach (AvailabilityItemChargeDiscount aid in aidc)
                {
                    if (aid.IsPercentage)
                    {
                        totalDis += aid.DisPercentage;//TODO: Condider summing discount totals on non percentage as well
                        discountUnitDisplay = aid.DisPercentage + "%";
                    }
                    else
                    {
                        discountUnitDisplay = "$" + aid.DisPercentage;
                        continue;
                    }
                    sb.Append("<tr>");
                    sb.Append("<td>" + aid.CodDesc + "</td>");
                    sb.Append("<td>" + discountUnitDisplay + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("<tr class='total' rel='" + totalDis + "'>");
                sb.Append("<td>Total Discount</td>");
                sb.Append("<td>" + totalDis + "%</td>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("<span class='note'>(off standard per day rates)</span>");
                sb.Append("</div>");
                return sb.ToString();
            }
        }

        /// <summary>
        /// TODO: refactor into Availability Helper
        /// </summary>
        /// <param name="vehicleCode"></param>
        /// <param name="counrty"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetVehicleContentLink(AvailabilityRequest request, AvailabilityItem item)
        {
            string domainName = string.Empty;
            string sitePath = string.Empty;
            string vehicleType = string.Empty;
            string vehicleCode = item.VehicleCode;
            string vehicelConvention = AvailabilityHelper.GetVehicleConvension(request.CountryCode, item.Brand, request.VehicleType, vehicleCode);

            if (request.VehicleType == THL.Booking.VehicleType.Campervan)
            {
                switch (item.Brand)
                {
                    case THLBrands.Britz:
                        sitePath = "http://www.britz.{0}/campervanhire/pages/campervanpopup.aspx?vehiclecode={1}";
                        break;
                    case THLBrands.Maui:
                        sitePath = "http://www.maui.{0}/campervanhire/pages/campervanpopup.aspx?vehiclecode={1}";    
                        break;
                    case THLBrands.Backpackers:
                        sitePath = "http://www.backpackercampervans.{0}/campervanhire/pages/campervanPopup.aspx?vehiclecode={1}";
                        break;
                    case THLBrands.Mighty:
                        sitePath = "http://www.mightycampers.{0}/campervanhire/pages/campervanPopup.aspx?vehiclecode={1}";
                        break;
                    case THLBrands.United:
                        vehicelConvention = vehicelConvention.Replace(".","");//contegro limitation
                        sitePath = "http://www.unitedcampervans.{0}/{1}";
                        break;
                    case THLBrands.Alpha:
                        vehicelConvention = vehicelConvention.Replace(".","");//contegro limitation
                        sitePath = "http://www.alphacampervans.{0}/{1}";
                        break;
                    case THLBrands.Econo:
                        vehicelConvention = vehicelConvention.Replace(".","");//contegro limitation
                        sitePath = "http://www.econocampers.{0}/{1}";
                        break;
                    case THLBrands.Kea:
                        sitePath = "http://" + (request.CountryCode.Equals(CountryCode.AU) ? "au" : "nz") + "rentals.keacampers.com/en/{1}.aspx";
                        break;
                }
            }
            else
            {
                switch (item.Brand)
                {
                    case THLBrands.Britz:
                        sitePath = "http://www.britz.{0}/carhire/pages/carpopup.aspx?vehiclecode={1}";
                        break;
                    case THLBrands.Maui:
                        sitePath = "http://www.maui.{0}/carhire/pages/carpopup.aspx?vehiclecode={1}";
                        //sitePath = "http://www.maui.{0}/THLVehicleList?popup&vehiclecode={1}";
                        break;
                    case THLBrands.Backpackers:
                        sitePath = "http://www.backpackercampervans.{0}/carhire/pages/carPopup.aspx?vehiclecode={1}";
                        break;
                }

            }


            
            string vehicleContentPage = String.Format(sitePath, (request.CountryCode.Equals(CountryCode.AU) ? "com.au" : "co.nz"), vehicelConvention);

            return vehicleContentPage;
        }

        public static AlternativeAvailabilityItem GetOriginalItemForRequest(B2BAltAvailabilityRequest request)
        {
            AlternativeAvailabilityItem item = new AlternativeAvailabilityItem(AlternativeAvailabilityType.OriginalSearch);
            item.PickUpDate = request.PickUpDate;
            item.DropOffDate = request.DropOffDate;
            item.PickUpLoc = request.PickUpBranch;
            item.DropOffLoc = request.DropOffBranch;
            item.HirePeriod = (request.DropOffDate - request.PickUpDate).Days + 1;
            return item;
        }

        /// <summary>
        /// Refactor into static helper on AvailabilityResponseDisaplayer
        /// </summary>
        /// <param name="aType"></param>
        /// <returns></returns>
        public static string GetAlternativeTypeLabel(AlternativeAvailabilityType aType)
        {
            string responseStr = string.Empty;
            switch (aType)
            {
                case AlternativeAvailabilityType.DropOffBased:
                    responseStr = "Fixed drop off date";
                    break;
                case AlternativeAvailabilityType.PickUpBased:
                    responseStr = "Fixed pick up date";
                    break;
                case AlternativeAvailabilityType.SwitchLocation:
                    responseStr = "Reverse route";
                    break;
                case AlternativeAvailabilityType.MoveForward:
                    responseStr = "Later pick up date";
                    break;
                case AlternativeAvailabilityType.MoveBackwards:
                    responseStr = "Earlier pick up date";
                    break;
                case AlternativeAvailabilityType.OneWaySwitchLocation:
                    responseStr = "Return to pick up location";
                    break;
                case AlternativeAvailabilityType.OriginalSearch:
                    responseStr = "Original Search";
                    break;
                default:
                    responseStr = "Normal Based";//TODO: define defaults and biz logic if any
                    break;
            }
            return responseStr.ToUpper();
        }

        public static string GetAvailabilityTextForType(AlternativeAvailabilityType aType)
        {
            string responseStr = string.Empty;
            switch (aType)
            {
                case AlternativeAvailabilityType.DropOffBased:
                    responseStr = "shortened your stay by up to 10 days, keeping your drop off date the same";
                    break;
                case AlternativeAvailabilityType.PickUpBased:
                    responseStr = "shortened your stay by up to 10 days, keeping your pick up date the same";
                    break;
                case AlternativeAvailabilityType.SwitchLocation:
                    responseStr = "reversed your travel route";
                    break;
                case AlternativeAvailabilityType.MoveForward:
                    responseStr = "moved your pick up day up to 10 days later";
                    break;
                case AlternativeAvailabilityType.MoveBackwards:
                    responseStr = "moved your pick up day up to 10 days earlier";
                    break;
                case AlternativeAvailabilityType.OneWaySwitchLocation:
                    responseStr = "returned  the vehicle to your pick up location";
                    break;
                case AlternativeAvailabilityType.OriginalSearch:
                    responseStr = "Original Search";
                    break;
                default:
                    responseStr = "Normal Based";//TODO: define defaults and biz logic if any
                    break;
            }
            return responseStr;
        }

    }
}