﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking {


public enum THLBrands
{
    Generic = 0,
    Maui = 1,
    Britz = 2,
    Backpackers = 3,
    ExploreMore = 4,
    MAC = 5,
    AIRNZ = 6,
    Apollo = 7,
    EasyGo = 8,
    Kea = 9,
    KiwiCampers = 10,
    Moturis = 11,
    Mighty = 12,
    United = 13,
    Alpha = 14,
    Econo = 15,
    //reV:mia sept.18 2013 - added 
    BritzUS = 16,
    RoadBear = 17
}

/// <summary>
/// THLBrand Class
/// </summary>
public class THLBrand
{
    BranchCountryData[] _countriesData = null;
        
    public static THLBrands GetBrandForString(string brandStr) {
        brandStr = brandStr.ToLower();
        THLBrands brand = THLBrands.Generic;
        switch (brandStr) 
        { 
            case "m":
                brand = THLBrands.Maui;
                break;
            case "b":
                brand = THLBrands.Britz;
                break;
            case "p":
                brand = THLBrands.Backpackers;
                break;
            case "y":
                brand = THLBrands.Mighty;
                break;
            case "u":
                brand = THLBrands.United;
                break;
            case "a":
                brand = THLBrands.Alpha;
                break;
            case "e":
                brand = THLBrands.Econo;
                break;
            case "x":
                brand = THLBrands.ExploreMore;
                break;
            case "mac":
                brand = THLBrands.MAC;
                break;
            case "c":
                brand = THLBrands.MAC;
                break;
            case "airnz":
                brand = THLBrands.AIRNZ;
                break;

            case "maccom":
                brand = THLBrands.MAC;//added for aggregator content support 15/12/09
                break;
            //case "z": //  //reV:mia sept.18 2013 - added..not use 
            //    brand = THLBrands.AIRNZ;
            //    break;
            case "z":
                brand = THLBrands.BritzUS; //  //reV:mia sept.18 2013 - added 
                break;
            case "r":
                brand = THLBrands.RoadBear; //  //reV:mia sept.18 2013 - added 
                break;
            case "w":
                brand = THLBrands.KiwiCampers;
                break;
            case "o":
                brand = THLBrands.Moturis;
                break;
            case "q":
                brand = THLBrands.Kea;
                break;            
        }
        return brand;
    }


    /// <summary>
    /// Return true on brands owned by Tourism Holdings
    /// </summary>
    /// <param name="brand"></param>
    public static bool IsTHLOwned(THLBrands brand)
    {
        switch(brand)
        {
            case THLBrands.Maui:
                return true;
            case THLBrands.Britz:
                return true;
            case THLBrands.ExploreMore:
                return true;
            case THLBrands.Backpackers:
                return true;
            case THLBrands.Mighty:
                return true;
            case THLBrands.Kea:
                return true;
            case THLBrands.United:
                return true;
            case THLBrands.Alpha:
                return true;
            case THLBrands.Econo:
                return true;
            case THLBrands.BritzUS:
                return true;  //reV:mia sept.18 2013 - added 
                break;
            case THLBrands.RoadBear:
                return true; //  //reV:mia sept.18 2013 - added 
                break;
        }
        return false;    
    }

    /// <summary>
    /// Return the branded name label for the brand char
    /// </summary>
    /// <param name="brand"></param>
    /// <returns></returns>
    public static string GetNameForBrand(string brandChar)
    {
        brandChar = brandChar.ToLower();
        string brandStr = string.Empty;

        switch (brandChar)
        {
            case "m":
                brandStr = "Maui";
                break;
            case "b":
                brandStr = "Britz";
                break;
            case "p":
                brandStr = "Backpacker";
                break;
            case "y":
                brandStr = "Mighty";
                break;
            case "q":
                brandStr = "Kea";
                break;
            case "u":
                brandStr = "United";
                break;
            case "a":
                brandStr = "Alpha";
                break;
            case "e":
                brandStr = "Econo";
                break;

            //rev:mia- aug.27, 2013 - addition of roadbear and US britz
            case "z":
                brandStr = "Britz-US";
                break;
            case "r":
                brandStr = "RoadBear";
                break;
        }
        return brandStr;
    }
    
    /// <summary>
    /// Return the user friendly label for the brand
    /// </summary>
    /// <param name="brand"></param>
    /// <returns></returns>
    public static string GetNameForBrand(THLBrands brand)
    {
        string brandStr = string.Empty;

        switch (brand)
        {
            case THLBrands.Maui:
                brandStr = "Maui";
                break;
            case THLBrands.Britz:
                brandStr = "Britz";
                break;
            case THLBrands.Backpackers:
                brandStr = "Backpacker";
                break;
            case THLBrands.Mighty:
                brandStr = "Mighty";
                break;
            case THLBrands.Kea:
                brandStr = "Kea";
                break;
            case THLBrands.United:
                brandStr = "United";
                break;
            case THLBrands.Alpha:
                brandStr = "Alpha";
                break;
            case THLBrands.Econo:
                brandStr = "Econo";
                break;
            case THLBrands.ExploreMore:
                brandStr = "Explore More";
                break;
            case THLBrands.MAC:
                brandStr = "Motorhomes and Cars";
                break;
            case THLBrands.AIRNZ:
                brandStr = "Air New Zealand Campervans";
                break;

            case THLBrands.BritzUS:
                brandStr = "Britz-US"; //  //reV:mia sept.18 2013 - added 
                break;
            case THLBrands.RoadBear:
                brandStr = "RoadBear"; //  //reV:mia sept.18 2013 - added 
                break;

        }
        return brandStr;
    }

    public static char GetBrandChar(THLBrands brand)
    {
        char brandChar = 'g';//generic
        
        switch (brand)
        {
            case THLBrands.Maui:
                brandChar = 'm';
                break;
            case THLBrands.Britz:
                brandChar = 'b';
                break;
            case THLBrands.Backpackers:
                brandChar = 'p';
                break;
            case THLBrands.Mighty:
                brandChar = 'y';
                break;
            case THLBrands.ExploreMore :                 
                brandChar = 'x';
                break;
            //case THLBrands.AIRNZ:
            //    brandChar = 'z';
            //    break;
            case THLBrands.MAC:
                brandChar = 'c';
                break;
            case THLBrands.Alpha:
                brandChar = 'a';
                break;
            case THLBrands.Econo:
                brandChar = 'e';
                break;
            case THLBrands.KiwiCampers:
                brandChar = 'w';
                break;
            case THLBrands.Kea:
                brandChar = 'q';
                break;
            case THLBrands.United:
                brandChar = 'u';
                break;
            case THLBrands.Moturis:
                brandChar = 'o';
                break;
            case THLBrands.BritzUS:// //reV:mia sept.18 2013 - added 
                brandChar = 'z';
                break;
            case THLBrands.RoadBear: //reV:mia sept.18 2013 - added 
                brandChar = 'r';
                break;
        }
        return brandChar;
    }

    /// <summary>
    /// Convert From BPAE to Aurora Code used for legacy control params
    /// </summary>
    /// <param name="BPAECode"></param>
    /// <returns></returns>
    public static string GetBrandCharForBPAECode(string BPAECode)
    {
        string brandStr = string.Empty;
        switch (BPAECode.ToLower())
        { 
            case "ma":
                brandStr = "m";
                break;
            case "bz":
                brandStr = "b";
                break;
            case "bk":
                brandStr = "p";
                break;
            case "kx":
                brandStr = "x";
                break;
            // NON THL:
            case "ca":
                brandStr = "d";
                break;
            case "ec":
                brandStr = "e";
                break;
            case "mo":
                brandStr = "o";
                break;
            case "ke":
                brandStr = "q";
                break;
            case "kc":
                brandStr = "w";
                break;
            case "gw":
                brandStr = "y";
                break;
        }
        return brandStr;
    }

    public BranchCountryData GetCountryData(string countryCode) {
        foreach (BranchCountryData country in _countriesData)
            if (country.CountryCode == countryCode)
                return country;
        return null;
    }

    void addCountryData(BranchCountryData countryData)
    {
        if (_countriesData == null)
            _countriesData = new BranchCountryData[] { countryData };
        else
        {
            List<BranchCountryData> list = new List<BranchCountryData>(_countriesData);
            list.Add(countryData);
            _countriesData = list.ToArray();
        }
    }

    Branch[] branches;
    public Branch[] Branches
    {
        get {
            return branches;        
        }        
    }
    
    Vehicle[] vehicles;

    string BrandName;
    string BrandCode;
    
    THLBrands brandType;
    public THLBrands BrandType
    { 
        get { return brandType; }
    }
    
    public THLBrand(string brandXmlPath, THLBrands _brand, bool useLocalResource)
	{
        brandType = _brand;
        List<Branch> branchesList = new List<Branch>();
        List<Vehicle> vehiclesList = new List<Vehicle>();
        XmlDocument xml = new XmlDocument();
        
        if(useLocalResource)
            xml.Load(brandXmlPath); //fs      
        else 
            xml.LoadXml(new DataAccess().GetStringForURL(brandXmlPath));//http
        
        XmlNodeList countries = xml.SelectNodes("countries/country");
        string currentCountryCode = string.Empty;
        foreach(XmlNode country in countries) {
            currentCountryCode = country.Attributes["code"].Value;
            
            //get the countries opening hours
            XmlNode openingNode = country.SelectSingleNode("openHours");
            BranchCountryData countryData = new BranchCountryData();
            if (countryData.SetBranchTimes(openingNode)) {
                countryData.CountryCode = currentCountryCode;
                countryData.SetPassengerInfo(country.SelectSingleNode("passengerInfo"));
                //removed: addCountryData(countryData);
            } //else failed to init the country data for node xml node currentCountryCode..
            //get countries max passenger info
            
            Branch currentBranch = null;
            foreach (XmlNode branchNode in country.SelectNodes("branches/branch"))
            {
                VehicleType vt;
                switch (branchNode.Attributes["type"].Value)
                {
                    case "av":
                        vt = VehicleType.Campervan;
                        break;
                    case "ac":
                        vt = VehicleType.Car;
                        break;
                    case "all":
                        vt = VehicleType.Both;
                        break;
                    default:
                        vt = VehicleType.Both;
                        //TODO: log no service type branch and handle here
                        break;
                }               
                currentBranch = new Branch();
                currentBranch.CountryCode = currentCountryCode;
                currentBranch.BranchCode = branchNode.Attributes["value"].Value;
                currentBranch.Name = branchNode.Attributes["name"].Value;
                currentBranch.VehicleTypeAvailability = vt;
                
                countryData.AddAvailability(vt);//added
                
                bool pickupOnly = false;
                if (branchNode.Attributes["pickUpOnly"] != null)
                {
                    bool.TryParse(branchNode.Attributes["pickUpOnly"].Value, out pickupOnly);
                    currentBranch.PickUpOnly = pickupOnly;
                }
                branchesList.Add(currentBranch);
            }
            addCountryData(countryData);//added            
            Vehicle currentVehicle = null;
            foreach (XmlNode vehicleNode in country.SelectNodes("vehicles/vehicle"))
            {
                VehicleType vt = (vehicleNode.Attributes["type"].Value == "ac" ? VehicleType.Car : VehicleType.Campervan);
                int maxPassengers = 0;
                int.TryParse(vehicleNode.Attributes["maxPassengers"].Value, out maxPassengers);
                currentVehicle = new Vehicle(vehicleNode.Attributes["name"].Value, vt, vehicleNode.Attributes["value"].Value, maxPassengers, currentCountryCode);
                vehiclesList.Add(currentVehicle);
            }       
        }       
        branches = branchesList.ToArray();
        vehicles = vehiclesList.ToArray();       
	}

    /// <summary>
    /// Return Brand's Vehicle Collection for a given Country.
    /// </summary>
    /// <param name="countryCode">ISO Country Code</param>
    /// <returns>Vehicle Collection</returns>
    public Vehicle[] GetVehiclesForCountry(string countryCode) { 
        List<Vehicle> vList = new List<Vehicle>();
        foreach(Vehicle vehicle in vehicles) {
            if (vehicle.Country == countryCode.ToUpper())
                vList.Add(vehicle);        
        }
        return vList.ToArray();        
    }
}
}