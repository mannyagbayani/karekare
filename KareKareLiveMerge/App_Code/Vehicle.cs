﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;




namespace THL.Booking
{

    
    /// <summary>
    /// Vehicle Structure
    /// </summary>
    public class Vehicle
    {
        /// <summary>
        /// convert XML Node into Vehicle instance
        /// </summary>
        /// <param name="xmlNode"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static Vehicle FromXML(XmlNode xmlNode, string countryCode, THLBrands brand)
        {
            //<vehicle type="ac" name="Urban Wagon" value="auacm.UWAR" maxPassengers="5" url="/contentPage/url"/>
            string name = (xmlNode.Attributes["name"] != null ? xmlNode.Attributes["name"].Value : string.Empty);
            string code = (xmlNode.Attributes["value"] != null ? xmlNode.Attributes["value"].Value : string.Empty);
            string url = (xmlNode.Attributes["url"] != null ? xmlNode.Attributes["url"].Value.Replace("http://",string.Empty) : string.Empty);
            int maxPassengers = 0;
            int.TryParse(xmlNode.Attributes["maxPassengers"].Value, out maxPassengers);
            VehicleType type = VehicleType.Both; 
            if(xmlNode.Attributes["type"] != null)
            {
                switch (xmlNode.Attributes["type"].Value)
                { 
                    case "ac":
                        type = VehicleType.Car;
                        break;
                    case "av":
                        type = VehicleType.Campervan;
                        break;                
                }
            }
            Vehicle vehicle = new Vehicle(name, type, code, maxPassengers, countryCode);
            vehicle.ContentURL = url;
            vehicle.Brand = brand;
            return vehicle;       
        }
        
        
        
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
        }
        
        private string _contentURL;
        public string ContentURL
        {
            get { return _contentURL; }
            set { _contentURL = value; }
        }

        private VehicleType _type;
        public VehicleType Type {
            get {
                return _type;
            }
        }

        private int _maxPessengers;
        public int MaxPassengers {
            get {
                return _maxPessengers;
            }        
        }

        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
        }

        private string _country;
        public string Country
        {
            get {
                return _country;
            }
        }       

        private THLBrands _brand;
        public THLBrands Brand
        {
            get { return _brand; }
            set { _brand = value;  }
        }

        public Vehicle(string name, VehicleType type, string code, int maxPassengers, string countryCode)
        {
            _name = name;
            _type = type;
            _code = code;
            _maxPessengers = maxPassengers;
            _country = countryCode;
        }
    }
}