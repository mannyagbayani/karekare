﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;
using System.Text;

public partial class UnitTest_AvailabilityCheck : System.Web.UI.Page
{

    public string Results; 

    protected void Page_Load(object sender, EventArgs e)
    {
        string na = "4";
        if (!string.IsNullOrEmpty(Request.Params["na"]))
            na = Request.Params["na"];

        StringBuilder sb = new StringBuilder();
        DateTime start = DateTime.Now;
        sb.Append("{'start':'" + start.ToString() + "',");
        DataAccess dal = new DataAccess();
        string B2BJSONParams = "{'pb':'AKL', 'db':'AKL', 'pd' : '01-02-2013', 'dd':'15-02-2013', 'pt':'10:00', 'dt':'15:00', 'na':"+ na +", 'nc':1, 'cc':'NZ', 'brand':'b', 'vt':'av', 'model':'all', 'ac' :'ARARAT', 'pc':'FLEXB'}";
        //bridge the session        
        SourceType st = new SourceType();
        st.RequestorID = new UniqueID_Type();
        st.RequestorID.ID = "ARARAT";
        st.RequestorID.Type = "22";
        st.AgentDutyCode = "Yes~Yes~G,N~G~No";
        st.AgentSine = "USR8";
        st.ERSP_UserID = "USR8";       
        AgentLoginData agentData = dal.GetAgentData(st);
               
        B2BAvailabilityRequest b2bReq = new B2BAvailabilityRequest(B2BJSONParams, agentData);

        SessionManager.GetSessionData().AvailabilityRequestData = b2bReq;//added to remember selections

        SessionManager.GetSessionData().CurrentAgent = agentData.Sine;

        //Pull items from session
        B2BSessionCache sessionCache = SessionManager.GetSessionData().B2BCache == null ? new B2BSessionCache() : SessionManager.GetSessionData().B2BCache;
        AvailabilityItem[] availabiltyItems = sessionCache.GetAvailabilityResponse(b2bReq);
        SessionManager.GetSessionData().B2BCache = sessionCache;
        availabiltyItems = AvailabilityHelper.SortAvailabilityItems(availabiltyItems, "bubble", "asc");
        SessionManager.GetSessionData().AvailabilityRequestData = b2bReq;//update the current sessions request
        //bool hasNogross = false;
        //sortedItems = RenderAvailabilityCatalog(availabiltyItems, b2bReq);
        DateTime end = DateTime.Now;
        sb.Append("'complete': '" + end.ToString() + "','length': "+ (end-start).Seconds  +",'itemCount': " + availabiltyItems.Length + ",'query': "+ B2BJSONParams + "}");
        Results = sb.ToString();        
    }
}