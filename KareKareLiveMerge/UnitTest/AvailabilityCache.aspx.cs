﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTest_AvailabilityCache : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Write("Current Max Cache Age from Config:" + B2BSessionCache.GetMaxCachAge);
        Response.Write(SessionManager.GetSessionData().B2BCache.ToString().Replace("{","<br/>{"));
    }
}