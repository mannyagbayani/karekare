﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTest_AvailabilityRequestTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string B2BJSONParams = "{'pb':'AKL', 'db':'AKL', 'pd' : '01-12-2011', 'dd':'15-12-2011', 'pt':'10:00', 'dt':'15:00', 'na':2, 'nc':3, 'cc':'NZ', 'brand':'b', 'vt':'av', 'model':'all', 'ac' :'ARARAT', 'pc':'FLEXB'}";
        B2BAvailabilityRequest b2bReq = new B2BAvailabilityRequest(B2BJSONParams, Session["AgentData"] as AgentLoginData);
        DataAccess dal = new DataAccess();
        AvailabilityResponse response = dal.GetAvailability((AvailabilityRequest)b2bReq);
        //TODO: load AvailabilityRequest 

    }
}