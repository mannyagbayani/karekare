﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using THL.Booking;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
using System.Xml;


public partial class Content_Availability : System.Web.UI.Page
{

    public string DebugStr, CtrlJSONObj, aLoginDataJSONObj;

    public string EnhancedUI;


    public string AssetBasePath;

    public AgentLoginData aLoginData;

    protected void Page_Load(object sender, EventArgs e)
    {

        EnhancedUI = (!string.IsNullOrEmpty(Request.Params["eui"]) && Request.Params["eui"] == "1") ? "true" : "false";

        AssetBasePath = /*TODO: add https support from config using B2C HTTPPrefix*/ "http://" + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
        //debug zone              
        /*
        THLCountry bc = new THLCountry();
        bc.code = "AU";
        bc.name = "Australia";
        bc.brands = new B2BBrand[] { new B2BBrand() };
        THLCountry[] countries = new THLCountry[] { bc };
        */
        //debug zone

        if (Session["POS"] != null)//has B2B valid agent
        {

            DataAccess dal = new DataAccess();
            aLoginData = dal.GetAgentData((SourceType)(Session["POS"]));
            Session["AgentData"] = aLoginData;//TODO: encapsulate into session manager..
            DebugStr = QuoteTHLLogin((SourceType)(Session["POS"]), aLoginData);
            B2BCacheManager cm = new B2BCacheManager();//TODO: from app cache 
            THLCountry[] countries = cm.GetCountriesForAgent(aLoginData);
            CtrlJSONObj = countries.ToJSON();
            aLoginDataJSONObj = aLoginData.ToJSON();
        }

    }

    /// <summary>
    /// Process Ajax requst for Availability Results
    /// </summary>
    /// <param name="B2BJSONParams"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string GetAvailabilityItems(string B2BJSONParams)
    {
        //string B2BJSONParams = "{'pb':'AKL', 'db':'AKL', 'pd' : '01-12-2011', 'dd':'15-12-2011', 'pt':'10:00', 'dt':'15:00', 'na':2, 'nc':3, 'cc':'NZ', 'brand':'b', 'vt':'av', 'model':'all', 'ac' :'ARARAT', 'pc':'FLEXB'}";
        AgentLoginData agentData = HttpContext.Current.Session["AgentData"] as AgentLoginData;
        B2BAvailabilityRequest b2bReq = new B2BAvailabilityRequest(B2BJSONParams, agentData);
        DataAccess dal = new DataAccess();
        AvailabilityResponse response = dal.GetB2BAvailability(b2bReq);

        //TODO: save latest request into Session Object (for Back->Forward) support)        
        if (response != null)
        {
            AvailabilityItem[] availabiltyItems = response.GetAvailabilityItems("", 10);
            //availabiltyItems = AvailabilityHelper.SortAvailabilityItems(availabiltyItems, "bubble", "asc");

            availabiltyItems = AvailabilityHelper.SortAvailabilityItems(availabiltyItems, "bubble", "asc");

            string sortedItems = String.Empty;
            sortedItems = RenderAvailabilityCatalog(availabiltyItems, b2bReq);
            string encodedHTML = HttpContext.Current.Server.HtmlEncode(sortedItems);
            return String.Format("<xml count='{0}'>{1}</xml>", availabiltyItems.Length, encodedHTML);
        }
        return String.Empty;
    }

    /// <summary>
    /// Debug Method, TODO: invoke on debug mode
    /// </summary>
    /// <param name="st"></param>
    /// <returns></returns>
    public string QuoteTHLLogin(SourceType st, AgentLoginData aLoginData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<br/>Session ID:" + Session["SessionId"]);
        sb.Append("<br/>POS:" + st.ToString());
        sb.Append("<br />AgentSine :" + st.AgentSine);
        sb.Append("<br />AirlineVendorID:" + st.AirlineVendorID);
        sb.Append("<br />AgentDutyCode:" + st.AgentDutyCode);
        sb.Append("<br />AirportCode:" + st.AirportCode);
        sb.Append("<br />BookingChannel:" + st.BookingChannel);
        sb.Append("<br />ERSP_UserID:" + st.ERSP_UserID);
        sb.Append("<br />FirstDepartPoint:" + st.FirstDepartPoint);
        sb.Append("<br />ISOCountry:" + st.ISOCountry);
        sb.Append("<br />ISOCurrency:" + st.ISOCurrency);
        sb.Append("<br />Position:" + st.Position);
        sb.Append("<br />PseudoCityCode:" + st.PseudoCityCode);
        sb.Append("<br />RequestorID:" + st.RequestorID);
        sb.Append("<br />Agent's Currencies</br>");

        return sb.ToString();
    }


    /// <summary>
    /// TODO: to be moved into B2BAvailabilityHelper class 
    /// </summary>
    /// <param name="availabilityItems"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    public static string RenderAvailabilityCatalog(AvailabilityItem[] availabilityItems, B2BAvailabilityRequest request)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<ul id=""vehicleCatalog"">");
        foreach (AvailabilityItem item in availabilityItems)
        {
            //if(string.IsNullOrEmpty(item.AVPID)) continue;
            //Goal:, TODO: Cars path
            //http://www.britz.com.au/campervans/pages/CampervanPopup.aspx?vehiclecode=auavb.2BB
            //http://mauiauV2.thl.com/THLVehicleList?popup&vehiclecode=auacm.ECMR
            //http://www.backpackercampervans.co.nz/campervanhire/Pages/campervanpopup.aspx?vehiclecode=nzavp.BP2BTS

            char brandChar = THLBrand.GetBrandChar(item.Brand);
            string vehicleName = item.VehicleName;
            string vehicleCode = item.VehicleCode;
            string packageName = item.PackageCode;
            string vehicleTypeStr = (request.VehicleType == THL.Booking.VehicleType.Car ? "Cars" : "Campervans");
            string assetsBaseUrl = "http://" + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];

            string vehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(request.CountryCode, item.Brand, request.VehicleType, vehicleCode, assetsBaseUrl, false);
            string inclusionImagePath = AvailabilityHelper.GetVehicleImageBasePath(request.CountryCode, item.Brand, request.VehicleType, vehicleCode, assetsBaseUrl, true);
            string vehicleInclusionImage = String.Format("<img src='{0}-InclusionIcons.gif' border='0' title='{1}' />", inclusionImagePath, item.Brand);
            string vehicleClearCut72Image = String.Format("<img src='{0}-ClearCut-72.jpg' border='0' title='{1}' />", vehicleImagePath, vehicleCode);
            string vehicleClearCut144Image = String.Format("<img src='{0}-ClearCut-144.jpg' border='0' title='{1}' />", vehicleImagePath, vehicleCode);
            string vehicleContentURL = GetVehicleContentLink(request, item);

            string noAvailMsg = "Search online to find the following<br/>alternative travel options for this vehicle:<br /><b>Reverse route, +/- 10 days, Shorter hire.</b>";
            string noAvailLink = "javascript:preloadPage(\"vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brandChar + "\")";
            string noAvailTitle = "Unavailable - Search Alternative Availability";

            bool shortLead = false;


            if (!string.IsNullOrEmpty(item.ErrorMessage) && item.ErrorMessage.Contains("DISP24HR"))//swap above if short leadtime detected
            {//less then 24hr lead
                item.ErrorMessage = "Travel is within 24 hours - Please search again";
                //b2b does not require contact us display custom error message the same as blocking message
                item.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                shortLead = true;//phone and no alt avail
            }

            else if (!string.IsNullOrEmpty(item.ErrorMessage) && item.ErrorMessage.Contains("DISPRateCalc"))
            {//weekly mantenance case
                item.ErrorMessage = "We are undergoing scheduled maintenance, will be back 12.15 NZST";
                item.AvailabilityType = AvailabilityItemType.BlockingRuleTaC;
                shortLead = true;//phone and no alt avail            
            }

            sb.Append(@"<li class=""Collapse"">");

            if (item.AvailabilityType == AvailabilityItemType.Available || item.AvailabilityType == AvailabilityItemType.OnRequest)
            {
                string priceTableHTML = RenderB2BPriceDetails(item);
                sb.Append(@"
                        <div class=""VehicleItem"">                            
                            <a class=""VehicleThumb PopUp"" href=""" + vehicleContentURL + @""">" + vehicleClearCut72Image + @"</a>
                            <div class=""VehicleFeatures"">
                                <a class=""PopUp"" href=""" + vehicleContentURL + @""">" + THLBrand.GetNameForBrand(item.Brand) + " " + item.VehicleName + @"</a>"
                                + vehicleInclusionImage +
                            @"</div>
                            <div class=""PriceDescription"">
                                <a class=""Avail"" href=""Configure.aspx?avp=" + item.AVPID + @""">AVAILABLE NOW</a>
                            </div>
                            <div title=""" + item.PackageCode + @""" class=""TotalPrice"">
                                <span rel='" + item.EstimatedTotal + @"' class=""price exRate" + (item.CanGetGross ? string.Empty : " netonly") + @""">" + item.EstimatedTotal + @"</span>
                                <a class=""ShowPriceList"">Price Details</a>
                            </div>
                            <a class=""SelectBtn"" href=""Configure.aspx?avp=" + item.AVPID + @""">Select</a>
                        </div>
                        " + RenderB2BPriceDetails(item) + @"
                    </li>
                ");
            }
            else
            {
                bool isBlocking = (item.AvailabilityType == AvailabilityItemType.BlockingRuleTaC);

                string errMsg = item.BlockingRuleMessage + item.ErrorMessage;
                sb.Append(@"
                        <div class='VehicleItem " + (isBlocking ? "Blocking" : string.Empty) + @"'>
                            <img src='" + assetsBaseUrl + "/images/" + brandChar + "/icons/rowLogo.gif' title='" + item.Brand + @"' class='RowLogo' />
                            <a href='" + vehicleContentURL + @"' class='VehicleThumb PopUp'>
                            <img border=""0"" title=""2BB"" src=""" + vehicleImagePath + @"-ClearCut-72.jpg"">
                            </a>
                            <div class='VehicleFeatures'>
                             <a class=""PopUp"" href=""" + vehicleContentURL + @""">" + THLBrand.GetNameForBrand(item.Brand) + " " + vehicleName + @"</a>"
                                + vehicleInclusionImage +
                            @"</div>
                            <div class='Err'><span>" + errMsg + @"</span></div>
                            <div class='Limited'><div class='Top'><span><a href='" + noAvailLink + "' " + (shortLead ? "class='PopUp'" : string.Empty) + ">" + noAvailTitle + "</a></span><span class='Contact'>" + noAvailMsg + "</span></div>"
                );

                if (!shortLead)
                    sb.Append("<div class='AALink LeftLnk'><a class='aAvail' href='" + noAvailLink + "'>Search Alternate Availability</a></div>");

                sb.Append("</div>");
                sb.Append("</div>");
            }
        }
        return sb.ToString();
    }


    /// <summary>
    /// TODO: to be moved into B2BAvailabilityHelper class 
    /// Render Price Details Table for a given Availability Row Object
    /// </summary>
    /// <param name="availabilityRow"></param>
    /// <returns></returns>
    public static string RenderB2BPriceDetails(AvailabilityItem availabilityRow)
    {
        int colSpans = 7;

        StringBuilder sb = new StringBuilder();
        bool hasFreeDays = false;
        AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
        AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
        bool hasDiscount = false, hasPercentageDiscount = false;

        foreach (AvailabilityItemChargeDiscount discount in aidc)
        {
            //if (discount.IsPercentage) hasDiscount = true;// 04/08/09 only consider percentage discount ,TODO: dive into all ratebands           
            if (discount.IsPercentage) hasPercentageDiscount = true;
        }

        hasDiscount = AvailabilityHelper.HasDiscountedDays(availabilityRateBands);

        hasFreeDays = AvailabilityHelper.HasFreeDays(availabilityRateBands);

        colSpans -= Convert.ToInt16(hasFreeDays) + Convert.ToInt16(hasDiscount);

        sb.Append("<div class='PriceDetailsList " + (hasFreeDays ? string.Empty : "NoFreeDays") + (hasDiscount ? string.Empty : " NoDiscount") + "'>");
        sb.Append("<table class='chargesTable'>");
        sb.Append("<thead>");
        sb.Append("<tr>");
        sb.Append("<th class='RateTH'>Product Description</th>");
        if (hasFreeDays) sb.Append("<th class='FreeTH'><span>Free Days</span><span rel='freeDays' class='popup' style='display:none;'>[?]</span></th>");
        sb.Append("<th class='HireTH'>Hire Days</th>");
        sb.Append("<th class='PerTH'>Per Day Price</th>");
        if (hasDiscount) sb.Append("<th class='DiscTH'><span>Discounted Day Price</span>" + (hasPercentageDiscount ? "<span rel='DiscountedDaysPopUp' class='popup' title='' onmouseover='initPopUp(this)'>[?]</span>" : string.Empty) + "</th>");
        sb.Append("<th class='PayToAgent'>Payable To Agent</th>");
        sb.Append("<th class='PayAtPU'>Payble At Pickup</th>");
        sb.Append("<th class='TotalTH'>Total</th>");
        sb.Append("</tr>");
        sb.Append("</thead>");
        sb.Append("<tbody>");

        bool displayTotal = false;
        decimal longHireDiscount = availabilityRow.GetLongHireDiscount();
        decimal totalCharge = availabilityRow.GetTotalPrice();

        //rate bands
        if (availabilityRateBands != null)//has several rate bands
        {
            if (availabilityRateBands.Length > 1)
                displayTotal = true;
            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
            {
                string freeDaysStr = string.Empty;
                if (chargeRateRow.IncludesFreeDay > 0)
                {
                    freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                    hasFreeDays = true;
                }
                string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                sb.Append("<tr>");
                sb.Append("<td class='rt'>" + fromDateStr + " to " + toDateStr + "</td>");
                if (hasFreeDays) sb.Append("<td class='fd'>" + freeDaysStr + "</td>");
                sb.Append("<td class='hp'>" + chargeRateRow.HirePeriod + " day(s)</td>");
                sb.Append("<td class='pdp exRate curSymbol' rel='" + chargeRateRow.OriginalRate.ToString("F") + "'>" + chargeRateRow.OriginalRate.ToString("F") + "</td>");
                if (hasDiscount) sb.Append("<td class='dpd exRate curSymbol' rel='" + chargeRateRow.DiscountedRate.ToString("F") + "'>" + chargeRateRow.DiscountedRate.ToString("F") + "</td>");
                sb.Append(chargeRateRow.IsCustomerCharge ? "<td class='pta'></td><td class='pap exRate curSymbol' rel='" + chargeRateRow.GrossAmount.ToString("F") + "'>" + chargeRateRow.GrossAmount.ToString("F") + " </td>" : "<td class='pta exRate curSymbol' rel='" + chargeRateRow.GrossAmount.ToString("F") + "'>" + chargeRateRow.GrossAmount.ToString("F") + "</td><td class='pap'></td>");
                sb.Append("<td class='exRate curSymbol' rel='" + chargeRateRow.GrossAmount.ToString("F") + "'>" + chargeRateRow.GrossAmount.ToString("F") + "</td>");
                sb.Append("</tr>");
            }
        }
        else //single rate band, TODO: revisit formating
        {
            AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
            sb.Append("<tr>");
            sb.Append("<td class='rt'>" + vehicleRow.ProductName + "</td>");
            if (hasFreeDays) sb.Append("<td class='fd'>&nbsp;</td>");
            sb.Append("<td class='hp'>" + vehicleRow.HirePeriod + " day(s)</td>");
            sb.Append("<td class='pdp'>&nbsp;</td>");
            if (hasDiscount) sb.Append("<td class='dpd'>&nbsp;</td>");
            sb.Append(vehicleRow.IsCustomerCharge ? "<td class='pta'></td><td class='pap exRate curSymbol' rel='" + vehicleRow.ProductPrice + "'>" + vehicleRow.ProductPrice + " </td>" : "<td class='pta exRate curSymbol' rel='" + vehicleRow.ProductPrice + "'>" + vehicleRow.ProductPrice + "</td><td class='pap'></td>");
            sb.Append("<td class='exRate curSymbol' rel='" + vehicleRow.ProductPrice + "'>" + vehicleRow.ProductPrice + "</td>");
            sb.Append("</tr>");
        }

        //extra hire items
        AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
        if (aCharges != null && aCharges.Length > 0)
        {
            displayTotal = true;
            foreach (AvailabilityItemChargeRow charge in aCharges)
            {
                if (charge.IsBonusPack || charge.AdminFee > 0) continue;

                if (charge.HirePeriod > 1)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + charge.ProductName + "</td>");
                    if (hasFreeDays) sb.Append("td class='fd'>&nbsp;</td>");
                    sb.Append("<td class='hp'>" + charge.HirePeriod + " day(s)</td>");
                    sb.Append("<td class='pdp exRate curSymbol' rel='" + charge.AverageDailyRate.ToString("F") + "'>" + charge.AverageDailyRate.ToString("F") + "</td>");
                    if (hasDiscount) sb.Append("<td class='dpd'>" + "&nbsp;" + "</td>");
                    sb.Append(charge.IsCustomerCharge ? "<td class='pta'></td><td class='pap exRate curSymbol' rel='" + charge.ProductPrice.ToString("F") + "'>" + charge.ProductPrice.ToString("F") + "</td>" : "<td class='pta exRate curSymbol' rel='" + charge.ProductPrice.ToString("F") + "'>" + charge.ProductPrice.ToString("F") + "</td><td class='pap'></td>");
                    sb.Append("<td class='exRate curSymbol' rel='" + (charge.ProductPrice > 0 ? charge.ProductPrice.ToString("F") : "") + "'>" + (charge.ProductPrice > 0 ? charge.ProductPrice.ToString("F") : "included") + "</td>");
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr rel='" + charge.ProductCode + "'>");
                    sb.Append("<td colspan='" + colSpans + "'>" + charge.ProductName + "</td>");
                    sb.Append(charge.IsCustomerCharge ? "<td></td><td class='pta exRate curSymbol'>" + charge.ProductPrice.ToString("F") + "</td>" : "<td class='pap exRate curSymbol'>" + charge.ProductPrice.ToString("F") + "</td><td></td>");
                    sb.Append("<td class='exRate curSymbol'>" + (charge.ProductPrice == 0m ? "included" : charge.ProductPrice.ToString("F")) + "</td>");
                    sb.Append("</tr>");
                }
            }

        }

        //admin fee
        decimal adminFeeFactor = 1.0m;
        if (availabilityRow.AdminFee > 0 && availabilityRow.AdminFee > 0)
        {
            adminFeeFactor = 1.0m + (availabilityRow.AdminFee / 100);
            sb.Append("<tr>");
            sb.Append("<td colspan='" + colSpans + "'>" + (availabilityRow.AdminFee).ToString("F") + "% Administration fee</td>");
            sb.Append("<td class='exRate curSymbol'>" + (availabilityRow.EstimatedTotal * (availabilityRow.AdminFee / 100)).ToString("F") + "</td>");
            sb.Append("</tr>");
        }

        //total
        if (displayTotal)
        {
            //colSpans = 3 + (hasDiscount ? 1 : 0) + (hasFreeDays ? 1 : 0);
            sb.Append("<tr class='total'>");
            sb.Append("<td class='TotalTD' colspan='" + colSpans + "'>Total</td>");
            sb.Append("<td class='exRate curSymbol' rel='" + (availabilityRow.EstimatedTotal * adminFeeFactor).ToString("F") + "'>" + (availabilityRow.EstimatedTotal * adminFeeFactor).ToString("F") + "</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");
        sb.Append(getDiscountPopUp(availabilityRow));
        sb.Append("</div>");
        string result = sb.ToString();
        return result;
    }

    /// <summary>
    /// TODO: to be moved into b2bavailabilityhelper.cs
    /// Discount PopUps 
    /// </summary>
    /// <param name="availabilityRow"></param>
    /// <returns></returns>
    private static string getDiscountPopUp(AvailabilityItem availabilityRow)
    {
        AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
        if (aidc == null || aidc.Length == 0)
            return null;
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='DiscountedDaysPopUp bubbleBody'>");
            sb.Append("<h3>Discounted Day Price</h3>");
            sb.Append("<table>");
            sb.Append("<tbody>");
            decimal totalDis = 0m;
            string discountUnitDisplay = string.Empty;
            foreach (AvailabilityItemChargeDiscount aid in aidc)
            {
                if (aid.IsPercentage)
                {
                    totalDis += aid.DisPercentage;//TODO: Condider summing discount totals on non percentage as well
                    discountUnitDisplay = aid.DisPercentage + "%";
                }
                else
                {
                    discountUnitDisplay = "$" + aid.DisPercentage;
                    continue;
                }
                sb.Append("<tr>");
                sb.Append("<td>" + aid.CodDesc + "</td>");
                sb.Append("<td>" + discountUnitDisplay + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("<tr class='total' rel='" + totalDis + "'>");
            sb.Append("<td>Total Discount</td>");
            sb.Append("<td>" + totalDis + "%</td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("<span class='note'>(off standard per day rates)</span>");
            sb.Append("</div>");
            return sb.ToString();
        }
    }
    /// <summary>
    /// TODO: refactor into Availability Helper
    /// </summary>
    /// <param name="vehicleCode"></param>
    /// <param name="counrty"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    public static string GetVehicleContentLink(AvailabilityRequest request, AvailabilityItem item)
    {
        string topSitePath = string.Empty;
        switch (item.Brand)
        {//TODO: fork camper/cars url path based on brand implementation, campers for now
            case THLBrands.Britz:
                topSitePath = "/campervans/pages/CampervanPopup.aspx?vehiclecode=";
                break;
            case THLBrands.Maui:
                topSitePath = "/campervans/pages/CampervanPopup.aspx?vehiclecode=";
                break;
            case THLBrands.Backpackers:
                topSitePath = "/campervanhire/Pages/campervanpopup.aspx?vehiclecode=";
                break;

        }

        string vehicleContentPage = "http://www." + item.Brand.ToString() + "." + (request.CountryCode.Equals(CountryCode.AU) ? "com.au" : "co.nz") + topSitePath + AvailabilityHelper.GetVehicleConvension(request.CountryCode, item.Brand, request.VehicleType, item.VehicleCode);

        //http://www.britz.com.au/campervans/pages/CampervanPopup.aspx?vehiclecode=auavb.2BB
        //http://mauiauV2.thl.com/THLVehicleList?popup&vehiclecode=auacm.ECMR
        //http://www.backpackercampervans.co.nz/campervanhire/Pages/campervanpopup.aspx?vehiclecode=nzavp.BP2BTS
        return vehicleContentPage;
    }

}