﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Availability.aspx.cs" Inherits="Content_Availability" %>

<%@ Register TagPrefix="uc1" TagName="Header" Src="../Controls/General/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Availability</title>
    <link rel="stylesheet" type="text/css" href="../Styles.css" />
    <link href="http://images.thl.com/css/b/jcal.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="http://jqueryui.com/themes/base/jquery.ui.autocomplete.css" />
    <link rel="stylesheet" type="text/css" href="../css/bc.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/bc.js" type="text/javascript"></script>
    <script type="text/javascript">
        //var ctrlParams = {
        var testParams = {
            countries: [
                {
                    "code": "NZ",
                    "name": "New Zealand",
                    "selected": true,
                    "brands": [
                        {
                            "code": "b",
                            "name": "Britz",
                            "selected": true,
                            "packages": [{ "code": "FLEXB", "name": "Nett Flex Rate", "isInc": false, "vt": "av", "from": "01/03/2003", "to": "01/01/2013", "isSp": false}],
                            "vehicles": [{ "code": "auavb.2BB", "name": "HiTop", "vt": "av" }, { "code": "auavb.4BBXS", "name": "Voyager", "vt": "av" }, { "code": "auavb.2BTSB", "name": "Elite", "vt": "av"}]
                        }],
                    "locations": [{ "code": "ADL", "name": "Adelaide" }, { "code": "ASP", "name": "Alice Springs" }, { "code": "BYR", "name": "Ballina - Byron Bay"}]
                }
            ]
            };

            var ctrlParams = { countries :  <%= CtrlJSONObj %>};
            var enhancedUI = <%= EnhancedUI %>;
            var agentData = <%= aLoginDataJSONObj %>;

            $(document).ready(function(){
                init();
                                                  
            });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:Header ID="Header1" runat="server"></uc1:Header>
        <div id="bookingControl">
            <div id="ctrlHead">
                <span class="left"><big>Your search:</big> </span><span class="bookingData"></span>
            </div>
            <div id="ctrlBody">
                <div id="leftPanel">
                    <div id="vehicleBrandPanel">
                        <label class="section">
                            Country</label>
                        <span class="Radios section">
                            <input type="radio" name="cc" value="AU" id="ccAU" checked="checked" class="smart-radio-button">
                            <label for="ccAU">
                                AU</label>
                            <input type="radio" name="cc" value="NZ" id="ccNZ" class="smart-radio-button">
                            <label for="ccNZ">
                                NZ</label>
                        </span>
                        <label class="section">
                            Brand</label>
                        <span class="Radios">
                            <input type="radio" name="brand" value="M" id="brandM" class="smart-radio-button">
                            <label for="brandM">
                                Maui</label>
                            <input type="radio" name="brand" value="B" id="brandB" checked="checked" class="smart-radio-button">
                            <label for="brandB">
                                Britz</label>
                        </span><span class="Radios">
                            <input type="radio" name="brand" value="P" id="brandP" class="smart-radio-button">
                            <label for="brandP">
                                Backpacker</label>
                        </span>
                        <label for="av" class="section">
                            Vehicle type</label>
                        <span class="Radios">
                            <input type="radio" name="vt" value="av" id="avRadio" class="smart-radio-button"
                                rel="Campervan" checked="checked">
                            <label for="avRadio">
                                Campervan</label>
                            <input type="radio" name="vt" value="ac" id="acRadio" rel="Car" class="smart-radio-button">
                            <label for="acRadio">
                                Car</label>
                        </span>
                        <label for="vehicleDD" class="section">
                            Vehicle model</label>
                        <select id="vehicleDD" class="combobox long-dd">
                            <option value="-1">All Vehicles</option>
                        </select>
                    </div>
                    <div id="b2bPanel">
                        <label for="agentsDD" class="section">
                            Agent code</label>
                        <select id="agentsDD" class="combobox long-dd">
                        </select>
                        <label class="section">
                            Promo code <small>&nbsp;(optional)</small></label>
                        <select id="packagesDD" class="combobox long-dd">
                        </select>
                    </div>
                </div>
                <div id="locAndDates">
                    <div id="pdPanel" class="locPanel">
                        <label for="pd" class="section">
                            Pick up from</label>
                        <select id="pb" name="pb" class="locSelector combobox long-dd">
                            <option value="-1">Pick Up Location</option>
                        </select>
                        <label class="section datelbl">
                            Pick up date</label>
                        <!-- <input type="text" id="from-date" class="fromDate datepicker" />-->
                        <span id="fromDateDiv" style="width: 100px; height: 100px;" class="fromDate"></span>
                    </div>
                    <div id="ddPanel" class="locPanel">
                        <label for="db" class="section">
                            Drop off at</label>
                        <select id="db" name="db" class="locSelector combobox long-dd">
                            <option value="-1">Drop Off Location</option>
                        </select>
                        <label class="section datelbl">
                            Drop off date</label>
                        <!--<input type="text" id="to-date" class="datepicker" />-->
                        <span id="toDateDiv" style="width: 100px; height: 100px;"></span>
                    </div>
                </div>
                <div id="timeContainer">
                    <div class="timePanel">
                        <span class="section">
                            <label for="pt">
                                Pick up time</label><small>&nbsp;(Optional)</small></span>
                        <select id="pt" class="timeSelector combobox long-dd">
                            <option value="08:00">08:00 AM</option>
                            <option value="08:30">08:30 AM</option>
                            <option value="09:00">09:00 AM</option>
                            <option value="09:30">09:30 AM</option>
                            <option value="10:00" selected="selected">10:00 AM</option>
                            <option value="10:30">10:30 AM</option>
                            <option value="11:00">11:00 AM</option>
                            <option value="11:30">11:30 AM</option>
                            <option value="12:00">12:00 PM</option>
                            <option value="12:30">12:30 PM</option>
                            <option value="13:00">13:00 PM</option>
                            <option value="13:30">13:30 PM</option>
                            <option value="14:00">14:00 PM</option>
                            <option value="14:30">14:30 PM</option>
                            <option value="15:00">15:00 PM</option>
                            <option value="15:30">15:30 PM</option>
                            <option value="16:00">16:00 PM</option>
                            <option value="16:30">16:30 PM</option>
                            <option value="17:00">17:00 PM</option>
                        </select>
                    </div>
                    <div class="timePanel">
                        <span class="section">
                            <label for="dt">
                                Drop off time</label><small>&nbsp;(Optional)</small></span>
                        <select id="dt" class="timeSelector combobox long-dd">
                            <option value="08:00">08:00 AM</option>
                            <option value="08:30">08:30 AM</option>
                            <option value="09:00">09:00 AM</option>
                            <option value="09:30">09:30 AM</option>
                            <option value="10:00">10:00 AM</option>
                            <option value="10:30">10:30 AM</option>
                            <option value="11:00">11:00 AM</option>
                            <option value="11:30">11:30 AM</option>
                            <option value="12:00">12:00 PM</option>
                            <option value="12:30">12:30 PM</option>
                            <option value="13:00">13:00 PM</option>
                            <option value="13:30">13:30 PM</option>
                            <option value="14:00">14:00 PM</option>
                            <option value="14:30">14:30 PM</option>
                            <option value="15:00" selected="selected">15:00 PM</option>
                            <option value="15:30">15:30 PM</option>
                            <option value="16:00">16:00 PM</option>
                            <option value="16:30">16:30 PM</option>
                            <option value="17:00">17:00 PM</option>
                        </select>
                    </div>
                </div>
                <div id="paxContainer">
                    <div class="paxPanel">
                        <label for="na" class="section">
                            Adults</label>
                        <select id="na" class="combobox short-dd">
                            <option value="1" selected="selected">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                    </div>
                    <div class="paxPanel">
                        <label for="nc" class="section">
                            Children</label>
                        <select id="nc" class="combobox short-dd">
                            <option value="0" selected="selected">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                    </div>
                    <div>
                        <button id="getAvailSubmit" type="button" onclick="submitRequest()">
                            GET AVAILABILITY</button>
                    </div>
                </div>
                <div id="errorPanel">
                    <span>&nbsp;</span>
                </div>
                <input type="hidden" id="brand" name="brand" value="b" />
                <input type="hidden" id="sc" name="sc" value="rv" />
                <input type="hidden" id="vtype" name="vtype" value="rv" />
                <input type="hidden" id="pd" name="pd" value="1" />
                <input type="hidden" id=" PM" name=" PM" value="8" />
                <input type="hidden" id="py" name="py" value="2011" />
                <input type="hidden" id="dd" name="dd" value="10" />
                <input type="hidden" id="dm" name="dm" value="8" />
                <input type="hidden" id="dy" name="dy" value="2011" />
            </div>
        </div>
        <div id="availabilityFilter">
            <span>Search result below</span> <span class="Radios">
                <input type="radio" name="grsnett" value="gross" class="smart-radio-button">
                <label for="grsnett">
                    Gross Amount</label>
                <input type="radio" name="grsnett" value="nett" class="smart-radio-button">
                <label>
                    Nett Amount</label>
            </span>
            <label for="currencySelector">
                Alternative Currency</label>
            <select id="currencySelector">
            </select>
        </div>
        <div id="catalogContainer">
        </div>
        <div id="debugPanel" style="float: left; clear: both;">
            <%=  DebugStr %>
        </div>
    </div>
    </form>
</body>
</html>
