Imports System.Xml
Imports THLAuroraWebInterface.WebServices

Public Class General

#Region "rev:mia Oct.18 2013 - added for logging"
    Private Shared Function LogADebutNote() As Boolean
        Try
            Return CBool(System.Web.Configuration.WebConfigurationManager.AppSettings("LogDebugNote").ToString)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function LogDebugNotes(message As String) As Boolean
        If (Not LogADebutNote()) Then Return False

        Dim logPath As String = System.Web.Configuration.WebConfigurationManager.AppSettings("logPath")
        Dim timeStamp As String = DateTime.Now.ToString("ddMMyyyy")
        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(String.Concat(logPath, "DebugNote_", timeStamp & ".txt"))
        Try
            sw.WriteLine(DateTime.Now.ToString("HHmmss") + " : " + message)
            Return True
        Catch ex As Exception
            Return False
        Finally
            sw.Close()
        End Try

    End Function
    Public Shared Function SerializeVehRetResRQObject(ByVal obj As OTA_VehRetResRQ) As String
        If (Not LogADebutNote()) Then Return ""
        Try
            Dim ser1 As XmlSerializer
            Dim w1 As StringWriter
            ser1 = New XmlSerializer(GetType(OTA_VehRetResRQ))
            w1 = New StringWriter
            ser1.Serialize(w1, obj)
            Return w1.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function SerializeVehRetResRSObject(ByVal obj As OTA_VehRetResRS) As String
        If (Not LogADebutNote()) Then Return ""
        Try
            Dim ser1 As XmlSerializer
            Dim w1 As StringWriter
            ser1 = New XmlSerializer(GetType(OTA_VehRetResRS))
            w1 = New StringWriter
            ser1.Serialize(w1, obj)
            Return w1.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function SerializeVehRetResRSObject(ByVal obj As OTA_VehRetResRSV3) As String
        If (Not LogADebutNote()) Then Return ""
        Try
            Dim ser1 As XmlSerializer
            Dim w1 As StringWriter
            ser1 = New XmlSerializer(GetType(OTA_VehRetResRS))
            w1 = New StringWriter
            ser1.Serialize(w1, obj)
            Return w1.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region


    Public Shared Function SerializeRetUsersRSObject(ByVal obj As THL_RetUsersRS) As String
        Dim ser1 As XmlSerializer
        Dim w1 As StringWriter
        ser1 = New XmlSerializer(GetType(THL_RetUsersRS))
        w1 = New StringWriter
        ser1.Serialize(w1, obj)
        Return w1.ToString
    End Function

    Public Shared Function DeserializeRetUsersRSObject(ByVal objString As String, ByVal obj As THL_RetUsersRS) As Object
        Dim ae As New System.Text.UnicodeEncoding
        Dim byteXML() As Byte = ae.GetBytes(objString)
        Dim ser1 As XmlSerializer
        ser1 = New XmlSerializer(GetType(THL_RetUsersRS))
        Dim newMemoryStream As New MemoryStream(byteXML)
        Dim userDataStream As Stream

        '  RS = CType(ser1.Deserialize(newMemoryStream), THL_RetUsersRS)
        Return CType(ser1.Deserialize(newMemoryStream), THL_RetUsersRS)
    End Function


    Public Shared Function SerializePriceAndAvailabilityRSObject(ByVal obj As THL_PriceAndAvailDataRS) As String
        Dim ser1 As XmlSerializer
        Dim w1 As StringWriter
        ser1 = New XmlSerializer(GetType(THL_PriceAndAvailDataRS))
        w1 = New StringWriter
        ser1.Serialize(w1, obj)
        Return w1.ToString
    End Function

    Public Shared Function DeserializePriceAndAvailabilityRSObject(ByVal objString As String, ByVal obj As THL_PriceAndAvailDataRS) As THL_PriceAndAvailDataRS
        Dim ae As New System.Text.UnicodeEncoding
        Dim byteXML() As Byte = ae.GetBytes(objString)
        Dim ser1 As XmlSerializer
        ser1 = New XmlSerializer(GetType(THL_PriceAndAvailDataRS))
        Dim newMemoryStream As New MemoryStream(byteXML)
        Dim userDataStream As Stream

        Return CType(ser1.Deserialize(newMemoryStream), THL_PriceAndAvailDataRS)
    End Function


    Public Shared Function SerializeVehAvailRateRSObject(ByVal obj As OTA_VehAvailRateRS) As String
        Dim ser1 As XmlSerializer
        Dim w1 As StringWriter
        ser1 = New XmlSerializer(GetType(OTA_VehAvailRateRS))
        w1 = New StringWriter
        ser1.Serialize(w1, obj)
        Return w1.ToString
    End Function

    Public Shared Function DeserializeVehAvailRateRSObject(ByVal objString As String, ByVal obj As OTA_VehAvailRateRS) As OTA_VehAvailRateRS
        Dim ae As New System.Text.UnicodeEncoding
        Dim byteXML() As Byte = ae.GetBytes(objString)
        Dim ser1 As XmlSerializer
        ser1 = New XmlSerializer(GetType(OTA_VehAvailRateRS))
        Dim newMemoryStream As New MemoryStream(byteXML)
        Dim userDataStream As Stream
        Return CType(ser1.Deserialize(newMemoryStream), OTA_VehAvailRateRS)
    End Function

    Public Sub SessionObjects()
        Dim PriceAndAvailabilityData As String
        Dim AvailableVehicleData As String
        Dim BookingDataset As String
        Dim MyDetails As String
        Dim UserData As String
    End Sub

    Public Enum PageName
        AgentLogin = 0
        CustomerLogin = 1
        EditMyDetails = 2
        MaintainAgentUsers = 3
        PreRegistration = 4
        PriceAndAvailability = 5
        QuotesAndBookings = 6
    End Enum

    Public Shared Sub MaintainSessionObjects(ByVal page As PageName, ByRef session As System.Web.SessionState.HttpSessionState)
        'clear session objects when not required
        Select Case page
            Case PageName.AgentLogin
                ClearSession(session)

            Case PageName.CustomerLogin
                ClearSession(session)

            Case PageName.EditMyDetails
                session("PriceAndAvailabilityData") = Nothing
                session("AvailableVehicleData") = Nothing
                session("BookingDataset") = Nothing
                session("UserData") = Nothing

            Case PageName.MaintainAgentUsers
                session("PriceAndAvailabilityData") = Nothing
                session("AvailableVehicleData") = Nothing
                session("BookingDataset") = Nothing
                session("MyDetails") = Nothing


            Case PageName.PreRegistration
                session("PriceAndAvailabilityData") = Nothing
                session("AvailableVehicleData") = Nothing
                session("BookingDataset") = Nothing
                session("MyDetails") = Nothing
                session("UserData") = Nothing

            Case PageName.PriceAndAvailability
                session("BookingDataset") = Nothing
                session("MyDetails") = Nothing
                session("UserData") = Nothing

            Case PageName.QuotesAndBookings
                session("PriceAndAvailabilityData") = Nothing
                session("AvailableVehicleData") = Nothing
                session("MyDetails") = Nothing
                session("UserData") = Nothing
        End Select
    End Sub

    Public Shared Sub ClearSession(ByRef session As System.Web.SessionState.HttpSessionState)
        '  session("PriceAndAvailabilityData") = Nothing
        ' session("AvailableVehicleData") = Nothing
        session("BookingDataset") = Nothing
        session("MyDetails") = Nothing
        session("UserData") = Nothing
        session("TypeOfUser") = Nothing
        session("POS") = Nothing
        session("FullName") = Nothing
        ''rev:mia sept 20 2013 -- Roadbear and Britz-US change
        session("MRU_CurrencyCode") = Nothing
        session("MRU_CurrencyMultiplier") = "0.0"
    End Sub


    Public Shared Sub LogError(ByVal message As String, ByVal errorId As String, ByVal server As System.Web.HttpServerUtility)

        Dim m_oDOM As New XmlDocument
        Dim path As String = server.MapPath("../content/ErrorLog.xml")
        m_oDOM.Load(path)
        Dim root As XmlNode = m_oDOM.DocumentElement()

        Dim newNode As System.Xml.XmlNode = m_oDOM.CreateNode(XmlNodeType.Element, "Error", "ss")
        newNode.InnerXml = "<ErrorId>" & errorId & "</ErrorId><Message>" & message & "</Message>"
        root.AppendChild(newNode)
        m_oDOM.Save(path)

        'Else
        '	Dim xmlWriter As XmlTextWriter

        '	xmlWriter = New XmlTextWriter(path, Nothing)
        '	With xmlWriter
        '		.Indentation = 2

        '		.Formatting = Formatting.Indented
        '		'Create an XML declaration for this XMl document
        '		.WriteStartDocument(False)
        '		'Add Comment
        '		.WriteComment("This XML file is used to log errors in the THLAuorora Application")
        '		'The root element is <Error>
        '		.WriteStartElement("Errors")

        '		'Write a new <Error> element
        '		.WriteStartElement("Error")
        '		.WriteElementString("ErrorId", errorId)
        '		.WriteElementString("Message", message)
        '		'close <Error> element
        '		.WriteEndElement()

        '		'close root </Error>
        '		.WriteEndDocument()
        '		'close underlying stream
        '		.Close()
        '	End With

        ''End If
    End Sub

    Public Shared Function BackGroundImageURL(ByRef session As System.Web.SessionState.HttpSessionState) As String
        Dim url As String
        If CStr(session("TypeOfUser")) = "customer" Then
            ' This is b2c
            If CStr(session("Company")) = "britz" Then
                ' Load britz image
                url = "../images/Britz-bkgrd.gif"
            Else
                ' Load maui image
                url = "../images/maui_bckgrd.gif"
            End If
        Else
            ' This is b2b, load universal image
            url = "../images/b&m_background.jpg"
        End If

        Return url
    End Function


    Public Shared Function SerializeLoginObject(ByVal obj As THL_LoginOkRQ) As String
        Dim ser1 As XmlSerializer
        Dim w1 As StringWriter
        ser1 = New XmlSerializer(GetType(THL_LoginOkRQ))
        w1 = New StringWriter
        ser1.Serialize(w1, obj)
        Return w1.ToString
    End Function

    Public Shared Function SerializeVehAvailRateRQObject(ByVal obj As OTA_VehAvailRateRQ) As String
        Dim ser1 As XmlSerializer
        Dim w1 As StringWriter
        ser1 = New XmlSerializer(GetType(OTA_VehAvailRateRQ))
        w1 = New StringWriter
        ser1.Serialize(w1, obj)
        Return w1.ToString
    End Function

End Class


