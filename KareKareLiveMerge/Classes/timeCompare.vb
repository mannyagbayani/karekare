﻿Imports System.Runtime.CompilerServices

Module TimeExtensions

    '<System.Runtime.CompilerServices.Extension> _
    <Extension()> _
    Public Function IsBetween(time As DateTime, startTime As DateTime, endTime As DateTime) As Boolean
        If time.TimeOfDay = startTime.TimeOfDay Then
            Return True
        End If
        If time.TimeOfDay = endTime.TimeOfDay Then
            Return True
        End If

        If startTime.TimeOfDay <= endTime.TimeOfDay Then
            Return (time.TimeOfDay >= startTime.TimeOfDay AndAlso time.TimeOfDay <= endTime.TimeOfDay)
        Else
            Return Not (time.TimeOfDay >= endTime.TimeOfDay AndAlso time.TimeOfDay <= startTime.TimeOfDay)
        End If
    End Function
End Module


