Imports System.Configuration
Public Class Config

	Public Enum Environment
		ENV_DEV = 1
		ENV_QA = 2
		ENV_PROD = 3
		ENV_TEST = 4
	End Enum

	Public Shared Function GetConfigValue(ByVal configSetting As String) As String
		'returns the configuration setting for the 
		'current Environment

		Dim strEnvironment As String
		strEnvironment = ConfigurationSettings.AppSettings("environment")
		Return ConfigurationSettings.AppSettings(strEnvironment & "_" & configSetting)

	End Function

	Public Shared Function GetEnvironment() As Environment
		Dim strEnvironment As String
		strEnvironment = ConfigurationSettings.AppSettings("environment")
		Select Case strEnvironment.ToLower()
			Case "prod"
				Return Environment.ENV_PROD
			Case "dev"
				Return Environment.ENV_DEV
			Case "qa"
				Return Environment.ENV_QA
			Case "test"
				Return Environment.ENV_TEST
			Case Else
				'raise an error as Web.Config must not have 
				'the relevant settings
				Throw New Exception("Unrecognised Environment")
		End Select
	End Function

	Public Shared Function GetEnvironmentString() As String
		Return ConfigurationSettings.AppSettings("environment")
	End Function

    ''rev:mia April 1, 2014 changed  "payable at pickup" to Payable to supplier
    'Private Shared Function DisplayNewSupplierMessage() As String
    '    Try
    '        If (HttpContext.Current.Request.QueryString("vt") <> Nothing) Then
    '            Dim qs As String = HttpContext.Current.Request.QueryString("vt").ToString()
    '            If (qs = "av") Then ''campervan
    '                Return ""
    '            Else
    '                Return "Default"
    '            End If
    '        Else
    '            Return "Default"
    '        End If
    '    Catch ex As Exception
    '        Return "Default"
    '    End Try

    'End Function

    'Public Shared Function GetPayablePickupMessage(key As String) As String
    '    key = String.Concat(DisplayNewSupplierMessage(), key)
    '    Return ConfigurationSettings.AppSettings(key)
    'End Function

    Public Shared Function GetPayablePickupMessage(key As String, vehicletype As String) As String
        vehicletype = vehicletype.ToLower
        key = CStr(IIf(vehicletype = "av", (String.Concat("", key)), (String.Concat("Default", key))))
        Return ConfigurationSettings.AppSettings(key)
    End Function


    Public Shared Function GetMessage(key As String) As String
        Return ConfigurationSettings.AppSettings(key)
    End Function
End Class



